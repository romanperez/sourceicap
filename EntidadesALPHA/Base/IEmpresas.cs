/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IEmpresas
    {                
        int IdEmpresa { get;set; }
		string NombreUnidad { get;set; }
		string Direccion { get;set; }
		string Telefono { get;set; }
		string CorreoElectronico { get;set; }
		string SitioWeb { get;set; }
		string RFC { get;set; }
		string CodigoPostal { get;set; }
		bool Estatus { get;set; }
                    
    }// IEmpresas ends.
}// LibEntidades.Entidades ends.
