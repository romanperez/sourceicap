using System;
namespace LibEntidades.Entidades
{
    public partial interface IInsumos
    {                
        int IdInsumo { get;set; }
		int? IdUnidad { get;set; }
		string Codigo { get;set; }
		string Serie { get;set; }
		string Descripcion { get;set; }
		int IdMoneda { get;set; }
		double Precio { get;set; }
		bool EsServicio { get;set; }
		bool Estatus { get;set; }
                    
    }// IInsumos ends.
}// LibEntidades.Entidades ends.
