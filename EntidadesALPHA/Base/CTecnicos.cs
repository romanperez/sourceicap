using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CTecnicos : CBase, ITecnicos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CTecnicos].
        /// </summary>
		public enum eFields {IdTecnico=0,IdEmpleado,IdEspecialidad,IdGrado}
		protected int _IdTecnico;
		protected int _IdEmpleado;
		protected int _IdEspecialidad;
		protected int _IdGrado;
        
				public virtual  int IdTecnico
		{
		 get{return _IdTecnico;}
		 set{_IdTecnico = value;}
		}
		public virtual  int IdEmpleado
		{
		 get{return _IdEmpleado;}
		 set{_IdEmpleado = value;}
		}
		public virtual  int IdEspecialidad
		{
		 get{return _IdEspecialidad;}
		 set{_IdEspecialidad = value;}
		}
		public virtual  int IdGrado
		{
		 get{return _IdGrado;}
		 set{_IdGrado = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CTecnicos()
		{
			_tabla = "Tecnicos";
		}
    }// CTecnicos ends.
}// LibEntidades.Entidades ends.
