using System;
namespace LibEntidades.Entidades
{
    public partial interface IOrdenesTrabajo
    {                
        int IdOrdenTrabajo { get;set; }
		string Comentarios { get;set; }
		string ComentariosCliente { get;set; }
		bool Estatus { get;set; }
		int IdContrato { get;set; }
		int IdActividad { get;set; }
		string Folio { get;set; }
		string Ubicacion { get;set; }
		DateTime FechaInicio { get;set; }
		DateTime FechaFin { get;set; }
		int IdEstatus { get;set; }
		int IdPrioridad { get;set; }
                    
    }// IOrdenesTrabajo ends.
}// LibEntidades.Entidades ends.
