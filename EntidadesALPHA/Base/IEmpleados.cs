/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IEmpleados
    {                
        int IdEmpleado { get;set; }
		int IdDepartamento { get;set; }
		string Nombre { get;set; }
		string ApellidoPaterno { get;set; }
		string ApellidoMaterno { get;set; }
		string Matricula { get;set; }
		bool Estatus { get;set; }
                    
    }// IEmpleados ends.
}// LibEntidades.Entidades ends.
