using System;
namespace LibEntidades.Entidades
{
    public partial interface ITecnicos
    {                
        int IdTecnico { get;set; }
		int IdEmpleado { get;set; }
		int IdEspecialidad { get;set; }
		int IdGrado { get;set; }
                    
    }// ITecnicos ends.
}// LibEntidades.Entidades ends.
