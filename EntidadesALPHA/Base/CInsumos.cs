using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CInsumos : CBase, IInsumos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CInsumos].
        /// </summary>
		public enum eFields {IdInsumo=0,IdUnidad,Codigo,Serie,Descripcion,IdMoneda,Precio,EsServicio,Estatus}
		protected int _IdInsumo;
		protected int? _IdUnidad;
		protected string _Codigo;
		protected string _Serie;
		protected string _Descripcion;
		protected int _IdMoneda;
		protected double _Precio;
		protected bool _EsServicio;
		protected bool _Estatus;
        
				public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  int? IdUnidad
		{
		 get{return _IdUnidad;}
		 set{_IdUnidad = value;}
		}
		public virtual  string Codigo
		{
		 get{return _Codigo;}
		 set{_Codigo = value;}
		}
		public virtual  string Serie
		{
		 get{return _Serie;}
		 set{_Serie = value;}
		}
		public virtual  string Descripcion
		{
		 get{return _Descripcion;}
		 set{_Descripcion = value;}
		}
		public virtual  int IdMoneda
		{
		 get{return _IdMoneda;}
		 set{_IdMoneda = value;}
		}
		public virtual  double Precio
		{
		 get{return _Precio;}
		 set{_Precio = value;}
		}
		public virtual  bool EsServicio
		{
		 get{return _EsServicio;}
		 set{_EsServicio = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CInsumos()
		{
			_tabla = "Insumos";
		}
    }// CInsumos ends.
}// LibEntidades.Entidades ends.
