using System;
/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CTecnicosGrados : CBase, ITecnicosGrados,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CTecnicosGrados].
        /// </summary>
		public enum eFields {IdGrado=0,Codigo,Nombre,Estatus}
		protected int _IdGrado;
		protected string _Codigo;
		protected string _Nombre;
		protected bool _Estatus;
        
				public virtual  int IdGrado
		{
		 get{return _IdGrado;}
		 set{_IdGrado = value;}
		}
		public virtual  string Codigo
		{
		 get{return _Codigo;}
		 set{_Codigo = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CTecnicosGrados()
		{
			_tabla = "TecnicosGrados";
		}
    }// CTecnicosGrados ends.
}// LibEntidades.Entidades ends.
