using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CAlmacenes : CBase, IAlmacenes,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CAlmacenes].
        /// </summary>
		public enum eFields {IdAlmacen=0,IdEmpresa,Codigo,Almacen,Estatus}
		protected int _IdAlmacen;
		protected int _IdEmpresa;
		protected string _Codigo;
		protected string _Almacen;
		protected bool _Estatus;
        
				public virtual  int IdAlmacen
		{
		 get{return _IdAlmacen;}
		 set{_IdAlmacen = value;}
		}
		public virtual  int IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  string Codigo
		{
		 get{return _Codigo;}
		 set{_Codigo = value;}
		}
		public virtual  string Almacen
		{
		 get{return _Almacen;}
		 set{_Almacen = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CAlmacenes()
		{
			_tabla = "Almacenes";
		}
    }// CAlmacenes ends.
}// LibEntidades.Entidades ends.
