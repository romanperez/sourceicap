using System;
namespace LibEntidades.Entidades
{
    public partial interface IPedidosDetalles
    {                
        int IdPedidoDetalle { get;set; }
		int IdPedido { get;set; }
		int IdInsumo { get;set; }
		double Cantidad { get;set; }
                    
    }// IPedidosDetalles ends.
}// LibEntidades.Entidades ends.
