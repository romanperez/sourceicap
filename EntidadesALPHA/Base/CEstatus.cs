using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CEstatus : CBase, IEstatus,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CEstatus].
        /// </summary>
		public enum eFields {IdEstatus=0,Clasificacion,Nombre,Estatus}
		protected int _IdEstatus;
		protected string _Clasificacion;
		protected string _Nombre;
		protected bool _Estatus;
        
				public virtual  int IdEstatus
		{
		 get{return _IdEstatus;}
		 set{_IdEstatus = value;}
		}
		public virtual  string Clasificacion
		{
		 get{return _Clasificacion;}
		 set{_Clasificacion = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CEstatus()
		{
			_tabla = "Estatus";
		}
    }// CEstatus ends.
}// LibEntidades.Entidades ends.
