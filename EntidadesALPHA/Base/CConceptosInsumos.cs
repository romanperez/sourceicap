using System;
/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CConceptosInsumos : CBase, IConceptosInsumos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CConceptosInsumos].
        /// </summary>
		public enum eFields {IdConceptoInsumo=0,IdConcepto,IdInsumo,Cantidad}
		protected int _IdConceptoInsumo;
		protected int _IdConcepto;
		protected int _IdInsumo;
		protected double _Cantidad;
        
				public virtual  int IdConceptoInsumo
		{
		 get{return _IdConceptoInsumo;}
		 set{_IdConceptoInsumo = value;}
		}
		public virtual  int IdConcepto
		{
		 get{return _IdConcepto;}
		 set{_IdConcepto = value;}
		}
		public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  double Cantidad
		{
		 get{return _Cantidad;}
		 set{_Cantidad = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CConceptosInsumos()
		{
			_tabla = "ConceptosInsumos";
		}
    }// CConceptosInsumos ends.
}// LibEntidades.Entidades ends.
