using System;
namespace LibEntidades.Entidades
{
    public partial interface IMovimientos
    {                
      int IdMovimiento { get;set; }
		int IdEstatus { get;set; }
		DateTime Fecha { get;set; }
		int IdAlmacen { get;set; }
		int IdUsuario { get;set; }
		bool Estatus { get;set; }
                    
    }// IMovimientos ends.
}// LibEntidades.Entidades ends.
