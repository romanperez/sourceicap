using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CInsumosExistencias : CBase, IInsumosExistencias,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CInsumosExistencias].
        /// </summary>
		public enum eFields {IdExistencia=0,IdAlmacen,IdInsumo,Cantidad,Estatus}
		protected int _IdExistencia;
		protected int _IdAlmacen;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected bool _Estatus;
        
				public virtual  int IdExistencia
		{
		 get{return _IdExistencia;}
		 set{_IdExistencia = value;}
		}
		public virtual  int IdAlmacen
		{
		 get{return _IdAlmacen;}
		 set{_IdAlmacen = value;}
		}
		public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  double Cantidad
		{
		 get{return _Cantidad;}
		 set{_Cantidad = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CInsumosExistencias()
		{
			_tabla = "InsumosExistencias";
		}
    }// CInsumosExistencias ends.
}// LibEntidades.Entidades ends.
