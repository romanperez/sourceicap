using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CPedidosDetalles : CBase, IPedidosDetalles,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CPedidosDetalles].
        /// </summary>
		public enum eFields {IdPedidoDetalle=0,IdPedido,IdInsumo,Cantidad}
		protected int _IdPedidoDetalle;
		protected int _IdPedido;
		protected int _IdInsumo;
		protected double _Cantidad;
        
				public virtual  int IdPedidoDetalle
		{
		 get{return _IdPedidoDetalle;}
		 set{_IdPedidoDetalle = value;}
		}
		public virtual  int IdPedido
		{
		 get{return _IdPedido;}
		 set{_IdPedido = value;}
		}
		public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  double Cantidad
		{
		 get{return _Cantidad;}
		 set{_Cantidad = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CPedidosDetalles()
		{
			_tabla = "PedidosDetalles";
		}
    }// CPedidosDetalles ends.
}// LibEntidades.Entidades ends.
