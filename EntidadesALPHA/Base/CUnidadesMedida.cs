using System;
/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CUnidadesMedida : CBase, IUnidadesMedida,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CUnidadesMedida].
        /// </summary>
		public enum eFields {IdUnidad=0,Unidad,Estatus}
		protected int _IdUnidad;
		protected string _Unidad;
		protected bool _Estatus;
        
				public virtual  int IdUnidad
		{
		 get{return _IdUnidad;}
		 set{_IdUnidad = value;}
		}
		public virtual  string Unidad
		{
		 get{return _Unidad;}
		 set{_Unidad = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CUnidadesMedida()
		{
			_tabla = "UnidadesMedida";
		}
    }// CUnidadesMedida ends.
}// LibEntidades.Entidades ends.
