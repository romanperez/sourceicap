using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class COTConceptos : CBase, IOTConceptos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ COTConceptos].
        /// </summary>
		public enum eFields {IdOTConcepto=0,IdOrdenTrabajo,IdContratoDetalle,CantidadSolicitada,CantidadProgramada,CantidadEjecutada,Estatus}
		protected int _IdOTConcepto;
		protected int _IdOrdenTrabajo;
		protected int _IdContratoDetalle;
		protected double _CantidadSolicitada;
		protected double _CantidadProgramada;
		protected double _CantidadEjecutada;
		protected bool _Estatus;
        
				public virtual  int IdOTConcepto
		{
		 get{return _IdOTConcepto;}
		 set{_IdOTConcepto = value;}
		}
		public virtual  int IdOrdenTrabajo
		{
		 get{return _IdOrdenTrabajo;}
		 set{_IdOrdenTrabajo = value;}
		}
		public virtual  int IdContratoDetalle
		{
		 get{return _IdContratoDetalle;}
		 set{_IdContratoDetalle = value;}
		}
		public virtual  double CantidadSolicitada
		{
		 get{return _CantidadSolicitada;}
		 set{_CantidadSolicitada = value;}
		}
		public virtual  double CantidadProgramada
		{
		 get{return _CantidadProgramada;}
		 set{_CantidadProgramada = value;}
		}
		public virtual  double CantidadEjecutada
		{
		 get{return _CantidadEjecutada;}
		 set{_CantidadEjecutada = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public COTConceptos()
		{
			_tabla = "OTConceptos";
		}
    }// COTConceptos ends.
}// LibEntidades.Entidades ends.
