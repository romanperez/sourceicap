/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IConceptosInsumos
    {                
        int IdConceptoInsumo { get;set; }
		int IdConcepto { get;set; }
		int IdInsumo { get;set; }
		double Cantidad { get;set; }
                    
    }// IConceptosInsumos ends.
}// LibEntidades.Entidades ends.
