using System;
namespace LibEntidades.Entidades
{
    public partial interface IPedidos
    {                
        int IdPedido { get;set; }
		int IdContrato { get;set; }
		string Pedido { get;set; }
		string Descripcion { get;set; }
		DateTime FechaSolicitado { get;set; }
		DateTime FechaLlegada { get;set; }
		string Estatus { get;set; }
                    
    }// IPedidos ends.
}// LibEntidades.Entidades ends.
