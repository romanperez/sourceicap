/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IContratosDetalles
    {                
        int IdContratoDetalle { get;set; }
		bool Estatus { get;set; }
		int IdContrato { get;set; }
		int IdConcepto { get;set; }
		int IdInsumo { get;set; }
		int Partida { get;set; }
		double Cantidad { get;set; }
		double Precio { get;set; }
                    
    }// IContratosDetalles ends.
}// LibEntidades.Entidades ends.
