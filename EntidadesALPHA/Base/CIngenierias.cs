using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CIngenierias : CBase, IIngenierias,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CIngenierias].
        /// </summary>
		public enum eFields {IdIngenieria=0,IdProyecto,Ingenieria,Fecha,Estatus}
		protected int _IdIngenieria;
		protected int _IdProyecto;
		protected string _Ingenieria;
		protected DateTime _Fecha;
		protected bool _Estatus;
        
				public virtual  int IdIngenieria
		{
		 get{return _IdIngenieria;}
		 set{_IdIngenieria = value;}
		}
		public virtual  int IdProyecto
		{
		 get{return _IdProyecto;}
		 set{_IdProyecto = value;}
		}
		public virtual  string Ingenieria
		{
		 get{return _Ingenieria;}
		 set{_Ingenieria = value;}
		}
		public virtual  DateTime Fecha
		{
		 get{return _Fecha;}
		 set{_Fecha = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CIngenierias()
		{
			_tabla = "Ingenierias";
		}
    }// CIngenierias ends.
}// LibEntidades.Entidades ends.
