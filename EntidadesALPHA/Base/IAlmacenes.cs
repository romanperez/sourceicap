using System;
namespace LibEntidades.Entidades
{
    public partial interface IAlmacenes
    {                
        int IdAlmacen { get;set; }
		int IdEmpresa { get;set; }
		string Codigo { get;set; }
		string Almacen { get;set; }
		bool Estatus { get;set; }
                    
    }// IAlmacenes ends.
}// LibEntidades.Entidades ends.
