/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{
    public abstract class CBase
    {
        protected string _tabla;
		  protected bool _hasError;
        protected int _delete;
        protected string _error;        
        /// <summary>
        /// Especifica si la entidad esta seleccionada para ser eliminada.
        /// </summary>
        public virtual int IsDelete
        {
            get { return _delete; }
        }
        /// <summary>
        /// Especifica el error al procesar el objeto.
        /// </summary>  
        public virtual string Error
        {
            get { return (string.IsNullOrEmpty(_error)) ? "" : _error.Trim(); }
            set { _error = value; }
        }
        /// <summary>
        /// Indica si el objeto tiene error.
        /// </summary>
        public virtual bool HasError
        {
            get { return (string.IsNullOrEmpty(_error) ? false : true); }
            set { _hasError = value; }
        }
		  public abstract string[] GetCampos();
    }// CBase ends.
}// @@NombreProyecto@ ends.

