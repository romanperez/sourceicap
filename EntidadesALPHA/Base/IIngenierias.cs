using System;
namespace LibEntidades.Entidades
{
    public partial interface IIngenierias
    {                
        int IdIngenieria { get;set; }
		int IdProyecto { get;set; }
		string Ingenieria { get;set; }
		DateTime Fecha { get;set; }
		bool Estatus { get;set; }
                    
    }// IIngenierias ends.
}// LibEntidades.Entidades ends.
