/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IUnidadesMedida
    {                
        int IdUnidad { get;set; }
		string Unidad { get;set; }
		bool Estatus { get;set; }
                    
    }// IUnidadesMedida ends.
}// LibEntidades.Entidades ends.
