using System;
namespace LibEntidades.Entidades
{
    public partial interface IActividades
    {                
        int IdActividad { get;set; }
		int? IdEmpresa { get;set; }
		string Actividad { get;set; }
		string Descripcion { get;set; }
		bool Estatus { get;set; }
                    
    }// IActividades ends.
}// LibEntidades.Entidades ends.
