using System;
namespace LibEntidades.Entidades
{
    public partial interface IOTPersonal
    {                
        int IdOTPersonal { get;set; }
		int IdOrdenTrabajo { get;set; }
		int IdTecnico { get;set; }
		bool Estatus { get;set; }
                    
    }// IOTPersonal ends.
}// LibEntidades.Entidades ends.
