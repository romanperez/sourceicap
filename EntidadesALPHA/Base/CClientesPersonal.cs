using System;
/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CClientesPersonal : CBase, IClientesPersonal,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CClientesPersonal].
        /// </summary>
		public enum eFields {IdPersonalCliente=0,IdCliente,Nombre,ApellidoPaterno,ApellidoMaterno,Puesto,Telefono,Estatus}
		protected int _IdPersonalCliente;
		protected int _IdCliente;
		protected string _Nombre;
		protected string _ApellidoPaterno;
		protected string _ApellidoMaterno;
		protected string _Puesto;
		protected string _Telefono;
		protected bool _Estatus;
        
				public virtual  int IdPersonalCliente
		{
		 get{return _IdPersonalCliente;}
		 set{_IdPersonalCliente = value;}
		}
		public virtual  int IdCliente
		{
		 get{return _IdCliente;}
		 set{_IdCliente = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  string ApellidoPaterno
		{
		 get{return _ApellidoPaterno;}
		 set{_ApellidoPaterno = value;}
		}
		public virtual  string ApellidoMaterno
		{
		 get{return _ApellidoMaterno;}
		 set{_ApellidoMaterno = value;}
		}
		public virtual  string Puesto
		{
		 get{return _Puesto;}
		 set{_Puesto = value;}
		}
		public virtual  string Telefono
		{
		 get{return _Telefono;}
		 set{_Telefono = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CClientesPersonal()
		{
			_tabla = "ClientesPersonal";
		}
    }// CClientesPersonal ends.
}// LibEntidades.Entidades ends.
