/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface ITecnicosGrados
    {                
        int IdGrado { get;set; }
		string Codigo { get;set; }
		string Nombre { get;set; }
		bool Estatus { get;set; }
                    
    }// ITecnicosGrados ends.
}// LibEntidades.Entidades ends.
