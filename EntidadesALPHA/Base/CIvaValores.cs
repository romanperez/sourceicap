using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CIvaValores : CBase, IIvaValores,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CIvaValores].
        /// </summary>
		public enum eFields {IdIva=0,Porcentaje,FechaInicio,FechaFin,Estatus}
		protected int _IdIva;
		protected double _Porcentaje;
		protected DateTime _FechaInicio;
		protected DateTime _FechaFin;
		protected bool _Estatus;
        
				public virtual  int IdIva
		{
		 get{return _IdIva;}
		 set{_IdIva = value;}
		}
		public virtual  double Porcentaje
		{
		 get{return _Porcentaje;}
		 set{_Porcentaje = value;}
		}
		public virtual  DateTime FechaInicio
		{
		 get{return _FechaInicio;}
		 set{_FechaInicio = value;}
		}
		public virtual  DateTime FechaFin
		{
		 get{return _FechaFin;}
		 set{_FechaFin = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CIvaValores()
		{
			_tabla = "IvaValores";
		}
    }// CIvaValores ends.
}// LibEntidades.Entidades ends.
