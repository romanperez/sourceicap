using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CProyectos : CBase, IProyectos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CProyectos].
        /// </summary>
		public enum eFields {IdProyecto=0,IdCotizacion,Proyecto,Descripcion,Ubicacion,FechaInicial,FechaFinal,IdEstatus}
		protected int _IdProyecto;
		protected int _IdCotizacion;
		protected string _Proyecto;
		protected string _Descripcion;
		protected string _Ubicacion;
		protected DateTime _FechaInicial;
		protected DateTime _FechaFinal;
		protected int _IdEstatus;
        
				public virtual  int IdProyecto
		{
		 get{return _IdProyecto;}
		 set{_IdProyecto = value;}
		}
		public virtual  int IdCotizacion
		{
		 get{return _IdCotizacion;}
		 set{_IdCotizacion = value;}
		}
		public virtual  string Proyecto
		{
		 get{return _Proyecto;}
		 set{_Proyecto = value;}
		}
		public virtual  string Descripcion
		{
		 get{return _Descripcion;}
		 set{_Descripcion = value;}
		}
		public virtual  string Ubicacion
		{
		 get{return _Ubicacion;}
		 set{_Ubicacion = value;}
		}
		public virtual  DateTime FechaInicial
		{
		 get{return _FechaInicial;}
		 set{_FechaInicial = value;}
		}
		public virtual  DateTime FechaFinal
		{
		 get{return _FechaFinal;}
		 set{_FechaFinal = value;}
		}
		public virtual  int IdEstatus
		{
		 get{return _IdEstatus;}
		 set{_IdEstatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CProyectos()
		{
			_tabla = "Proyectos";
		}
    }// CProyectos ends.
}// LibEntidades.Entidades ends.
