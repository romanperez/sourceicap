using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CMonedas : CBase, IMonedas,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CMonedas].
        /// </summary>
		public enum eFields {IdMoneda=0,Moneda,Estatus}
		protected int _IdMoneda;
		protected string _Moneda;
		protected bool _Estatus;
        
				public virtual  int IdMoneda
		{
		 get{return _IdMoneda;}
		 set{_IdMoneda = value;}
		}
		public virtual  string Moneda
		{
		 get{return _Moneda;}
		 set{_Moneda = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CMonedas()
		{
			_tabla = "Monedas";
		}
    }// CMonedas ends.
}// LibEntidades.Entidades ends.
