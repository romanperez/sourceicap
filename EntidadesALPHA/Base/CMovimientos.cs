using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CMovimientos : CBase, IMovimientos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CMovimientos].
        /// </summary>
		public enum eFields {IdMovimiento=0,IdEstatus,Fecha,IdAlmacen,IdUsuario,Estatus}
		protected int _IdMovimiento;
		protected int _IdEstatus;
		protected DateTime _Fecha;
		protected int _IdAlmacen;
		protected int _IdUsuario;
		protected bool _Estatus;
        
				public virtual  int IdMovimiento
		{
		 get{return _IdMovimiento;}
		 set{_IdMovimiento = value;}
		}
		public virtual  int IdEstatus
		{
		 get{return _IdEstatus;}
		 set{_IdEstatus = value;}
		}
		public virtual  DateTime Fecha
		{
		 get{return _Fecha;}
		 set{_Fecha = value;}
		}
		public virtual  int IdAlmacen
		{
		 get{return _IdAlmacen;}
		 set{_IdAlmacen = value;}
		}
		public virtual  int IdUsuario
		{
		 get{return _IdUsuario;}
		 set{_IdUsuario = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CMovimientos()
		{
			_tabla = "Movimientos";
		}
    }// CMovimientos ends.
}// LibEntidades.Entidades ends.
