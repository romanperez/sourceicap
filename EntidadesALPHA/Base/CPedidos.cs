
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CPedidos : CBase, IPedidos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CPedidos].
        /// </summary>
		public enum eFields {IdPedido=0,IdContrato,Pedido,Descripcion,FechaSolicitado,FechaLlegada,Estatus}
		protected int _IdPedido;
		protected int _IdContrato;
		protected string _Pedido;
		protected string _Descripcion;
		protected DateTime _FechaSolicitado;
		protected DateTime _FechaLlegada;
		protected string _Estatus;
        
				public virtual  int IdPedido
		{
		 get{return _IdPedido;}
		 set{_IdPedido = value;}
		}
		public virtual  int IdContrato
		{
		 get{return _IdContrato;}
		 set{_IdContrato = value;}
		}
		public virtual  string Pedido
		{
		 get{return _Pedido;}
		 set{_Pedido = value;}
		}
		public virtual  string Descripcion
		{
		 get{return _Descripcion;}
		 set{_Descripcion = value;}
		}
		public virtual  DateTime FechaSolicitado
		{
		 get{return _FechaSolicitado;}
		 set{_FechaSolicitado = value;}
		}
		public virtual  DateTime FechaLlegada
		{
		 get{return _FechaLlegada;}
		 set{_FechaLlegada = value;}
		}
		public virtual  string Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CPedidos()
		{
			_tabla = "Pedidos";
		}
    }// CPedidos ends.
}// LibEntidades.Entidades ends.
