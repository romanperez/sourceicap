using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class COrdenesTrabajo : CBase, IOrdenesTrabajo,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ COrdenesTrabajo].
        /// </summary>
		public enum eFields {IdOrdenTrabajo=0,Comentarios,ComentariosCliente,Estatus,IdContrato,IdActividad,Folio,Ubicacion,FechaInicio,FechaFin,IdEstatus,IdPrioridad}
		protected int _IdOrdenTrabajo;
		protected string _Comentarios;
		protected string _ComentariosCliente;
		protected bool _Estatus;
		protected int _IdContrato;
		protected int _IdActividad;
		protected string _Folio;
		protected string _Ubicacion;
		protected DateTime _FechaInicio;
		protected DateTime _FechaFin;
		protected int _IdEstatus;
		protected int _IdPrioridad;
        
				public virtual  int IdOrdenTrabajo
		{
		 get{return _IdOrdenTrabajo;}
		 set{_IdOrdenTrabajo = value;}
		}
		public virtual  string Comentarios
		{
		 get{return _Comentarios;}
		 set{_Comentarios = value;}
		}
		public virtual  string ComentariosCliente
		{
		 get{return _ComentariosCliente;}
		 set{_ComentariosCliente = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
		public virtual  int IdContrato
		{
		 get{return _IdContrato;}
		 set{_IdContrato = value;}
		}
		public virtual  int IdActividad
		{
		 get{return _IdActividad;}
		 set{_IdActividad = value;}
		}
		public virtual  string Folio
		{
		 get{return _Folio;}
		 set{_Folio = value;}
		}
		public virtual  string Ubicacion
		{
		 get{return _Ubicacion;}
		 set{_Ubicacion = value;}
		}
		public virtual  DateTime FechaInicio
		{
		 get{return _FechaInicio;}
		 set{_FechaInicio = value;}
		}
		public virtual  DateTime FechaFin
		{
		 get{return _FechaFin;}
		 set{_FechaFin = value;}
		}
		public virtual  int IdEstatus
		{
		 get{return _IdEstatus;}
		 set{_IdEstatus = value;}
		}
		public virtual  int IdPrioridad
		{
		 get{return _IdPrioridad;}
		 set{_IdPrioridad = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public COrdenesTrabajo()
		{
			_tabla = "OrdenesTrabajo";
		}
    }// COrdenesTrabajo ends.
}// LibEntidades.Entidades ends.
