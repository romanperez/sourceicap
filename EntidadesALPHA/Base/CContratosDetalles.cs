using System;
/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CContratosDetalles : CBase, IContratosDetalles,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CContratosDetalles].
        /// </summary>
		public enum eFields {IdContratoDetalle=0,Estatus,IdContrato,IdConcepto,IdInsumo,Partida,Cantidad,Precio}
		protected int _IdContratoDetalle;
		protected bool _Estatus;
		protected int _IdContrato;
		protected int _IdConcepto;
		protected int _IdInsumo;
		protected int _Partida;
		protected double _Cantidad;
		protected double _Precio;
        
				public virtual  int IdContratoDetalle
		{
		 get{return _IdContratoDetalle;}
		 set{_IdContratoDetalle = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
		public virtual  int IdContrato
		{
		 get{return _IdContrato;}
		 set{_IdContrato = value;}
		}
		public virtual  int IdConcepto
		{
		 get{return _IdConcepto;}
		 set{_IdConcepto = value;}
		}
		public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  int Partida
		{
		 get{return _Partida;}
		 set{_Partida = value;}
		}
		public virtual  double Cantidad
		{
		 get{return _Cantidad;}
		 set{_Cantidad = value;}
		}
		public virtual  double Precio
		{
		 get{return _Precio;}
		 set{_Precio = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CContratosDetalles()
		{
			_tabla = "ContratosDetalles";
		}
    }// CContratosDetalles ends.
}// LibEntidades.Entidades ends.
