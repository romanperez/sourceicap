/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface ICotizacionesDetalles
    {                
        int IdCotizacionDetalle { get;set; }
		int IdCotizacion { get;set; }
		int IdIConcepto { get;set; }
		int IdInsumo { get;set; }
		double? Cantidad { get;set; }
		double Precio { get;set; }
		double PorcentajeDescuento { get;set; }
		double DescuentoMonto { get;set; }
                    
    }// ICotizacionesDetalles ends.
}// LibEntidades.Entidades ends.
