using System;
namespace LibEntidades.Entidades
{
    public partial interface IMonedas
    {                
        int IdMoneda { get;set; }
		string Moneda { get;set; }
		bool Estatus { get;set; }
                    
    }// IMonedas ends.
}// LibEntidades.Entidades ends.
