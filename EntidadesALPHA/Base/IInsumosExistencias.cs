using System;
namespace LibEntidades.Entidades
{
    public partial interface IInsumosExistencias
    {                
        int IdExistencia { get;set; }
		int IdAlmacen { get;set; }
		int IdInsumo { get;set; }
		double Cantidad { get;set; }
		bool Estatus { get;set; }
                    
    }// IInsumosExistencias ends.
}// LibEntidades.Entidades ends.
