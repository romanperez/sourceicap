using System;
namespace LibEntidades.Entidades
{
    public partial interface IProyectos
    {                
        int IdProyecto { get;set; }
		int IdCotizacion { get;set; }
		string Proyecto { get;set; }
		string Descripcion { get;set; }
		string Ubicacion { get;set; }
		DateTime FechaInicial { get;set; }
		DateTime FechaFinal { get;set; }
		int IdEstatus { get;set; }
                    
    }// IProyectos ends.
}// LibEntidades.Entidades ends.
