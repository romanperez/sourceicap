using System;
/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CClientes : CBase, IClientes,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CClientes].
        /// </summary>
		public enum eFields {IdCliente=0,RazonSocial,NombreComercial,Rfc,Telefono,Direccion,CodigoPostal,Estatus}
		protected int _IdCliente;
		protected string _RazonSocial;
		protected string _NombreComercial;
		protected string _Rfc;
		protected string _Telefono;
		protected string _Direccion;
		protected string _CodigoPostal;
		protected bool _Estatus;
        
				public virtual  int IdCliente
		{
		 get{return _IdCliente;}
		 set{_IdCliente = value;}
		}
		public virtual  string RazonSocial
		{
		 get{return _RazonSocial;}
		 set{_RazonSocial = value;}
		}
		public virtual  string NombreComercial
		{
		 get{return _NombreComercial;}
		 set{_NombreComercial = value;}
		}
		public virtual  string Rfc
		{
		 get{return _Rfc;}
		 set{_Rfc = value;}
		}
		public virtual  string Telefono
		{
		 get{return _Telefono;}
		 set{_Telefono = value;}
		}
		public virtual  string Direccion
		{
		 get{return _Direccion;}
		 set{_Direccion = value;}
		}
		public virtual  string CodigoPostal
		{
		 get{return _CodigoPostal;}
		 set{_CodigoPostal = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CClientes()
		{
			_tabla = "Clientes";
		}
    }// CClientes ends.
}// LibEntidades.Entidades ends.
