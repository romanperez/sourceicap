using System;
/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CConceptos : CBase, IConceptos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CConceptos].
        /// </summary>
		public enum eFields {IdConcepto=0,IdEmpresa,Concepto,Descripcion,Estatus}
		protected int _IdConcepto;
		protected int? _IdEmpresa;
		protected string _Concepto;
		protected string _Descripcion;
		protected bool _Estatus;
        
				public virtual  int IdConcepto
		{
		 get{return _IdConcepto;}
		 set{_IdConcepto = value;}
		}
		public virtual  int? IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  string Concepto
		{
		 get{return _Concepto;}
		 set{_Concepto = value;}
		}
		public virtual  string Descripcion
		{
		 get{return _Descripcion;}
		 set{_Descripcion = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CConceptos()
		{
			_tabla = "Conceptos";
		}
    }// CConceptos ends.
}// LibEntidades.Entidades ends.
