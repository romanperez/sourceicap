/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IDepartamentos
    {                
        int IdDepartamento { get;set; }
		int IdEmpresa { get;set; }
		string Codigo { get;set; }
		string Nombre { get;set; }
		bool Estatus { get;set; }
                    
    }// IDepartamentos ends.
}// LibEntidades.Entidades ends.
