/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IConceptos
    {                
        int IdConcepto { get;set; }
		int? IdEmpresa { get;set; }
		string Concepto { get;set; }
		string Descripcion { get;set; }
		bool Estatus { get;set; }
                    
    }// IConceptos ends.
}// LibEntidades.Entidades ends.
