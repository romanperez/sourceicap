using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CMovimientosDetalles : CBase, IMovimientosDetalles,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CMovimientosDetalles].
        /// </summary>
		public enum eFields {IdMovimientoDetalle=0,IdMovimiento,IdInsumo,Cantidad,Precio,Observacion}
		protected int _IdMovimientoDetalle;
		protected int _IdMovimiento;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected double _Precio;
		protected string _Observacion;
        
				public virtual  int IdMovimientoDetalle
		{
		 get{return _IdMovimientoDetalle;}
		 set{_IdMovimientoDetalle = value;}
		}
		public virtual  int IdMovimiento
		{
		 get{return _IdMovimiento;}
		 set{_IdMovimiento = value;}
		}
		public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  double Cantidad
		{
		 get{return _Cantidad;}
		 set{_Cantidad = value;}
		}
		public virtual  double Precio
		{
		 get{return _Precio;}
		 set{_Precio = value;}
		}
		public virtual  string Observacion
		{
		 get{return _Observacion;}
		 set{_Observacion = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CMovimientosDetalles()
		{
			_tabla = "MovimientosDetalles";
		}
    }// CMovimientosDetalles ends.
}// LibEntidades.Entidades ends.
