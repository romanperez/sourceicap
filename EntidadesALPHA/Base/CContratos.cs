using System;
/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CContratos : CBase, IContratos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CContratos].
        /// </summary>
		public enum eFields {IdContrato=0,IdEmpresa,IdIngenieria,Contrato,Aprobado,Estatus}
		protected int _IdContrato;
		protected int _IdEmpresa;
		protected int _IdIngenieria;
		protected string _Contrato;
		protected bool _Aprobado;
		protected bool _Estatus;
        
				public virtual  int IdContrato
		{
		 get{return _IdContrato;}
		 set{_IdContrato = value;}
		}
		public virtual  int IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  int IdIngenieria
		{
		 get{return _IdIngenieria;}
		 set{_IdIngenieria = value;}
		}
		public virtual  string Contrato
		{
		 get{return _Contrato;}
		 set{_Contrato = value;}
		}
		public virtual  bool Aprobado
		{
		 get{return _Aprobado;}
		 set{_Aprobado = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CContratos()
		{
			_tabla = "Contratos";
		}
    }// CContratos ends.
}// LibEntidades.Entidades ends.
