/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IContratos
    {                
        int IdContrato { get;set; }
		int IdEmpresa { get;set; }
		int IdIngenieria { get;set; }
		string Contrato { get;set; }
		bool Aprobado { get;set; }
		bool Estatus { get;set; }
                    
    }// IContratos ends.
}// LibEntidades.Entidades ends.
