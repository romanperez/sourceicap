/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IClientes
    {                
        int IdCliente { get;set; }
		string RazonSocial { get;set; }
		string NombreComercial { get;set; }
		string Rfc { get;set; }
		string Telefono { get;set; }
		string Direccion { get;set; }
		string CodigoPostal { get;set; }
		bool Estatus { get;set; }
                    
    }// IClientes ends.
}// LibEntidades.Entidades ends.
