using System;
/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CEmpresas : CBase, IEmpresas,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CEmpresas].
        /// </summary>
		public enum eFields {IdEmpresa=0,NombreUnidad,Direccion,Telefono,CorreoElectronico,SitioWeb,RFC,CodigoPostal,Estatus}
		protected int _IdEmpresa;
		protected string _NombreUnidad;
		protected string _Direccion;
		protected string _Telefono;
		protected string _CorreoElectronico;
		protected string _SitioWeb;
		protected string _RFC;
		protected string _CodigoPostal;
		protected bool _Estatus;
        
				public virtual  int IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  string NombreUnidad
		{
		 get{return _NombreUnidad;}
		 set{_NombreUnidad = value;}
		}
		public virtual  string Direccion
		{
		 get{return _Direccion;}
		 set{_Direccion = value;}
		}
		public virtual  string Telefono
		{
		 get{return _Telefono;}
		 set{_Telefono = value;}
		}
		public virtual  string CorreoElectronico
		{
		 get{return _CorreoElectronico;}
		 set{_CorreoElectronico = value;}
		}
		public virtual  string SitioWeb
		{
		 get{return _SitioWeb;}
		 set{_SitioWeb = value;}
		}
		public virtual  string RFC
		{
		 get{return _RFC;}
		 set{_RFC = value;}
		}
		public virtual  string CodigoPostal
		{
		 get{return _CodigoPostal;}
		 set{_CodigoPostal = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CEmpresas()
		{
			_tabla = "Empresas";
		}
    }// CEmpresas ends.
}// LibEntidades.Entidades ends.
