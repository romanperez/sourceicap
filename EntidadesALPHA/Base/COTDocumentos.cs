using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class COTDocumentos : CBase, IOTDocumentos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ COTDocumentos].
        /// </summary>
		public enum eFields {IdOTDocumentos=0,IdOrdenTrabajo,IdTipoDocumento,RutaDocumento,Estatus}
		protected int _IdOTDocumentos;
		protected int _IdOrdenTrabajo;
		protected int _IdTipoDocumento;
		protected int _RutaDocumento;
		protected bool _Estatus;
        
				public virtual  int IdOTDocumentos
		{
		 get{return _IdOTDocumentos;}
		 set{_IdOTDocumentos = value;}
		}
		public virtual  int IdOrdenTrabajo
		{
		 get{return _IdOrdenTrabajo;}
		 set{_IdOrdenTrabajo = value;}
		}
		public virtual  int IdTipoDocumento
		{
		 get{return _IdTipoDocumento;}
		 set{_IdTipoDocumento = value;}
		}
		public virtual  int RutaDocumento
		{
		 get{return _RutaDocumento;}
		 set{_RutaDocumento = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public COTDocumentos()
		{
			_tabla = "OTDocumentos";
		}
    }// COTDocumentos ends.
}// LibEntidades.Entidades ends.
