using System;
/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CCotizacionesDetalles : CBase, ICotizacionesDetalles,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CCotizacionesDetalles].
        /// </summary>
		public enum eFields {IdCotizacionDetalle=0,IdCotizacion,IdIConcepto,IdInsumo,Cantidad,Precio,PorcentajeDescuento,DescuentoMonto}
		protected int _IdCotizacionDetalle;
		protected int _IdCotizacion;
		protected int _IdIConcepto;
		protected int _IdInsumo;
		protected double? _Cantidad;
		protected double _Precio;
		protected double _PorcentajeDescuento;
		protected double _DescuentoMonto;
        
				public virtual  int IdCotizacionDetalle
		{
		 get{return _IdCotizacionDetalle;}
		 set{_IdCotizacionDetalle = value;}
		}
		public virtual  int IdCotizacion
		{
		 get{return _IdCotizacion;}
		 set{_IdCotizacion = value;}
		}
		public virtual  int IdIConcepto
		{
		 get{return _IdIConcepto;}
		 set{_IdIConcepto = value;}
		}
		public virtual  int IdInsumo
		{
		 get{return _IdInsumo;}
		 set{_IdInsumo = value;}
		}
		public virtual  double? Cantidad
		{
		 get{return _Cantidad;}
		 set{_Cantidad = value;}
		}
		public virtual  double Precio
		{
		 get{return _Precio;}
		 set{_Precio = value;}
		}
		public virtual  double PorcentajeDescuento
		{
		 get{return _PorcentajeDescuento;}
		 set{_PorcentajeDescuento = value;}
		}
		public virtual  double DescuentoMonto
		{
		 get{return _DescuentoMonto;}
		 set{_DescuentoMonto = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CCotizacionesDetalles()
		{
			_tabla = "CotizacionesDetalles";
		}
    }// CCotizacionesDetalles ends.
}// LibEntidades.Entidades ends.
