using System;
namespace LibEntidades.Entidades
{
    public partial interface ICotizaciones
    {                
        int IdCotizacion { get;set; }
		double Total { get;set; }
		double PorcentajeDescuento { get;set; }
		double DescuentoMonto { get;set; }
		int IdEmpresa { get;set; }
		int IdCliente { get;set; }
		int IdIva { get;set; }
		DateTime Fecha { get;set; }
		double Exito { get;set; }
		double Importe { get;set; }
		double SubTotal { get;set; }
		double Iva { get;set; }
                    
    }// ICotizaciones ends.
}// LibEntidades.Entidades ends.
