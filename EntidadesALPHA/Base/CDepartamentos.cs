using System;
/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CDepartamentos : CBase, IDepartamentos,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CDepartamentos].
        /// </summary>
		public enum eFields {IdDepartamento=0,IdEmpresa,Codigo,Nombre,Estatus}
		protected int _IdDepartamento;
		protected int _IdEmpresa;
		protected string _Codigo;
		protected string _Nombre;
		protected bool _Estatus;
        
				public virtual  int IdDepartamento
		{
		 get{return _IdDepartamento;}
		 set{_IdDepartamento = value;}
		}
		public virtual  int IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  string Codigo
		{
		 get{return _Codigo;}
		 set{_Codigo = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CDepartamentos()
		{
			_tabla = "Departamentos";
		}
    }// CDepartamentos ends.
}// LibEntidades.Entidades ends.
