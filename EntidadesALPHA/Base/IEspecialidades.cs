using System;
namespace LibEntidades.Entidades
{
    public partial interface IEspecialidades
    {                
      int IdEspecialidad { get;set; }
		int IdEmpresa { get;set; }
		string Codigo { get;set; }
		string Nombre { get;set; }
		bool Estatus { get;set; }
                    
    }// IEspecialidades ends.
}// LibEntidades.Entidades ends.
