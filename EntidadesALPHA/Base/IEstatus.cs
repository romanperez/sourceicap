using System;
namespace LibEntidades.Entidades
{
    public partial interface IEstatus
    {                
        int IdEstatus { get;set; }
		string Clasificacion { get;set; }
		string Nombre { get;set; }
		bool Estatus { get;set; }
                    
    }// IEstatus ends.
}// LibEntidades.Entidades ends.
