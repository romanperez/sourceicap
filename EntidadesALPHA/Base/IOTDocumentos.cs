using System;
namespace LibEntidades.Entidades
{
    public partial interface IOTDocumentos
    {                
        int IdOTDocumentos { get;set; }
		int IdOrdenTrabajo { get;set; }
		int IdTipoDocumento { get;set; }
		int RutaDocumento { get;set; }
		bool Estatus { get;set; }
                    
    }// IOTDocumentos ends.
}// LibEntidades.Entidades ends.
