using System;
/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CEmpleados : CBase, IEmpleados,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CEmpleados].
        /// </summary>
		public enum eFields {IdEmpleado=0,IdDepartamento,Nombre,ApellidoPaterno,ApellidoMaterno,Matricula,Estatus}
		protected int _IdEmpleado;
		protected int _IdDepartamento;
		protected string _Nombre;
		protected string _ApellidoPaterno;
		protected string _ApellidoMaterno;
		protected string _Matricula;
		protected bool _Estatus;
        
				public virtual  int IdEmpleado
		{
		 get{return _IdEmpleado;}
		 set{_IdEmpleado = value;}
		}
		public virtual  int IdDepartamento
		{
		 get{return _IdDepartamento;}
		 set{_IdDepartamento = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  string ApellidoPaterno
		{
		 get{return _ApellidoPaterno;}
		 set{_ApellidoPaterno = value;}
		}
		public virtual  string ApellidoMaterno
		{
		 get{return _ApellidoMaterno;}
		 set{_ApellidoMaterno = value;}
		}
		public virtual  string Matricula
		{
		 get{return _Matricula;}
		 set{_Matricula = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CEmpleados()
		{
			_tabla = "Empleados";
		}
    }// CEmpleados ends.
}// LibEntidades.Entidades ends.
