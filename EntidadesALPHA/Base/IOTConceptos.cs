using System;
namespace LibEntidades.Entidades
{
    public partial interface IOTConceptos
    {                
        int IdOTConcepto { get;set; }
		int IdOrdenTrabajo { get;set; }
		int IdContratoDetalle { get;set; }
		double CantidadSolicitada { get;set; }
		double CantidadProgramada { get;set; }
		double CantidadEjecutada { get;set; }
		bool Estatus { get;set; }
                    
    }// IOTConceptos ends.
}// LibEntidades.Entidades ends.
