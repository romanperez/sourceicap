using System;
namespace LibEntidades.Entidades
{
    public partial interface IOTPersonalCliente
    {                
        int IdOTPersonalCliente { get;set; }
		int IdOrdenTrabajo { get;set; }
		int IdEstatus { get;set; }
		int IdPersonalCliente { get;set; }
		bool Estatus { get;set; }
                    
    }// IOTPersonalCliente ends.
}// LibEntidades.Entidades ends.
