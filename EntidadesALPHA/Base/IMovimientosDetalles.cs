using System;
namespace LibEntidades.Entidades
{
    public partial interface IMovimientosDetalles
    {                
        int IdMovimientoDetalle { get;set; }
		int IdMovimiento { get;set; }
		int IdInsumo { get;set; }
		double Cantidad { get;set; }
		double Precio { get;set; }
		string Observacion { get;set; }
                    
    }// IMovimientosDetalles ends.
}// LibEntidades.Entidades ends.
