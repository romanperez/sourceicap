using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class COTPersonal : CBase, IOTPersonal,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ COTPersonal].
        /// </summary>
		public enum eFields {IdOTPersonal=0,IdOrdenTrabajo,IdTecnico,Estatus}
		protected int _IdOTPersonal;
		protected int _IdOrdenTrabajo;
		protected int _IdTecnico;
		protected bool _Estatus;
        
				public virtual  int IdOTPersonal
		{
		 get{return _IdOTPersonal;}
		 set{_IdOTPersonal = value;}
		}
		public virtual  int IdOrdenTrabajo
		{
		 get{return _IdOrdenTrabajo;}
		 set{_IdOrdenTrabajo = value;}
		}
		public virtual  int IdTecnico
		{
		 get{return _IdTecnico;}
		 set{_IdTecnico = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public COTPersonal()
		{
			_tabla = "OTPersonal";
		}
    }// COTPersonal ends.
}// LibEntidades.Entidades ends.
