using System;
/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CEspecialidades : CBase, IEspecialidades,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CEspecialidades].
        /// </summary>
		public enum eFields {IdEspecialidad=0,IdEmpresa,Codigo,Nombre,Estatus}
		protected int _IdEspecialidad;
		protected int _IdEmpresa;
		protected string _Codigo;
		protected string _Nombre;
		protected bool _Estatus;
        
				public virtual  int IdEspecialidad
		{
		 get{return _IdEspecialidad;}
		 set{_IdEspecialidad = value;}
		}
		public virtual  int IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  string Codigo
		{
		 get{return _Codigo;}
		 set{_Codigo = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CEspecialidades()
		{
			_tabla = "Especialidades";
		}
    }// CEspecialidades ends.
}// LibEntidades.Entidades ends.
