using System;
using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class COTPersonalCliente : CBase, IOTPersonalCliente,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ COTPersonalCliente].
        /// </summary>
		public enum eFields {IdOTPersonalCliente=0,IdOrdenTrabajo,IdEstatus,IdPersonalCliente,Estatus}
		protected int _IdOTPersonalCliente;
		protected int _IdOrdenTrabajo;
		protected int _IdEstatus;
		protected int _IdPersonalCliente;
		protected bool _Estatus;
        
				public virtual  int IdOTPersonalCliente
		{
		 get{return _IdOTPersonalCliente;}
		 set{_IdOTPersonalCliente = value;}
		}
		public virtual  int IdOrdenTrabajo
		{
		 get{return _IdOrdenTrabajo;}
		 set{_IdOrdenTrabajo = value;}
		}
		public virtual  int IdEstatus
		{
		 get{return _IdEstatus;}
		 set{_IdEstatus = value;}
		}
		public virtual  int IdPersonalCliente
		{
		 get{return _IdPersonalCliente;}
		 set{_IdPersonalCliente = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public COTPersonalCliente()
		{
			_tabla = "OTPersonalCliente";
		}
    }// COTPersonalCliente ends.
}// LibEntidades.Entidades ends.
