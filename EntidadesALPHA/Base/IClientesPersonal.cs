/*LAVM 04/11/2016 11:37:26 a. m.*/
namespace LibEntidades.Entidades
{
    public partial interface IClientesPersonal
    {                
        int IdPersonalCliente { get;set; }
		int IdCliente { get;set; }
		string Nombre { get;set; }
		string ApellidoPaterno { get;set; }
		string ApellidoMaterno { get;set; }
		string Puesto { get;set; }
		string Telefono { get;set; }
		bool Estatus { get;set; }
                    
    }// IClientesPersonal ends.
}// LibEntidades.Entidades ends.
