using System;
/*LAVM 04/11/2016 11:37:27 a. m.*/
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CCotizaciones : CBase, ICotizaciones,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CCotizaciones].
        /// </summary>
		public enum eFields {IdCotizacion=0,Total,PorcentajeDescuento,DescuentoMonto,IdEmpresa,IdCliente,IdIva,Fecha,Exito,Importe,SubTotal,Iva}
		protected int _IdCotizacion;
		protected double _Total;
		protected double _PorcentajeDescuento;
		protected double _DescuentoMonto;
		protected int _IdEmpresa;
		protected int _IdCliente;
		protected int _IdIva;
		protected DateTime _Fecha;
		protected double _Exito;
		protected double _Importe;
		protected double _SubTotal;
		protected double _Iva;
        
				public virtual  int IdCotizacion
		{
		 get{return _IdCotizacion;}
		 set{_IdCotizacion = value;}
		}
		public virtual  double Total
		{
		 get{return _Total;}
		 set{_Total = value;}
		}
		public virtual  double PorcentajeDescuento
		{
		 get{return _PorcentajeDescuento;}
		 set{_PorcentajeDescuento = value;}
		}
		public virtual  double DescuentoMonto
		{
		 get{return _DescuentoMonto;}
		 set{_DescuentoMonto = value;}
		}
		public virtual  int IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  int IdCliente
		{
		 get{return _IdCliente;}
		 set{_IdCliente = value;}
		}
		public virtual  int IdIva
		{
		 get{return _IdIva;}
		 set{_IdIva = value;}
		}
		public virtual  DateTime Fecha
		{
		 get{return _Fecha;}
		 set{_Fecha = value;}
		}
		public virtual  double Exito
		{
		 get{return _Exito;}
		 set{_Exito = value;}
		}
		public virtual  double Importe
		{
		 get{return _Importe;}
		 set{_Importe = value;}
		}
		public virtual  double SubTotal
		{
		 get{return _SubTotal;}
		 set{_SubTotal = value;}
		}
		public virtual  double Iva
		{
		 get{return _Iva;}
		 set{_Iva = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CCotizaciones()
		{
			_tabla = "Cotizaciones";
		}
    }// CCotizaciones ends.
}// LibEntidades.Entidades ends.
