using System;
namespace LibEntidades.Entidades
{ 
	
	public abstract partial class CActividades : CBase, IActividades,IEntidad	
    {
        /// <summary>
        /// Lista de propiedades clase [ CActividades].
        /// </summary>
		public enum eFields {IdActividad=0,IdEmpresa,Actividad,Descripcion,Estatus}
		protected int _IdActividad;
		protected int? _IdEmpresa;
		protected string _Actividad;
		protected string _Descripcion;
		protected bool _Estatus;
        
				public virtual  int IdActividad
		{
		 get{return _IdActividad;}
		 set{_IdActividad = value;}
		}
		public virtual  int? IdEmpresa
		{
		 get{return _IdEmpresa;}
		 set{_IdEmpresa = value;}
		}
		public virtual  string Actividad
		{
		 get{return _Actividad;}
		 set{_Actividad = value;}
		}
		public virtual  string Descripcion
		{
		 get{return _Descripcion;}
		 set{_Descripcion = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
	
		
		public override string[] GetCampos()
		{
			return Enum.GetNames(typeof(eFields));
		}
		public CActividades()
		{
			_tabla = "Actividades";
		}
    }// CActividades ends.
}// LibEntidades.Entidades ends.
