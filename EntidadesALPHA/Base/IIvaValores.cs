using System;
namespace LibEntidades.Entidades
{
    public partial interface IIvaValores
    {                
        int IdIva { get;set; }
		double Porcentaje { get;set; }
		DateTime FechaInicio { get;set; }
		DateTime FechaFin { get;set; }
		bool Estatus { get;set; }
                    
    }// IIvaValores ends.
}// LibEntidades.Entidades ends.
