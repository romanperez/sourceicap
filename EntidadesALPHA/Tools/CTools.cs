using System;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Web.Script.Serialization;//System.Web.Extensions
using System.Configuration;
namespace  LibEntidades.Entidades
{
    public partial class CTools
    {
        public const string APP_SETTING_PATH_FILE = "appPathFile";
        public const string NAME_FILE_CONFIG = @"\CConfig.xml";
        public const string ERR_NO_CONFIG_PATH = "La ruta del archivo de configuracion es nula.";
        public const string ERR_NO_EXIST_CONFIG_PATH = "No existe el archivo [{0}] de configuracion.";

        public static string ToXML<clase>(clase entidad, System.Text.Encoding encoding)
        {
            XmlSerializer xml = new XmlSerializer(typeof(clase), " ");
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms,((encoding == null)?  System.Text.Encoding.UTF8 : encoding));
            xml.Serialize(sw,entidad);
            ms.Position = 0;
            StreamReader sr = new StreamReader(ms);
            return sr.ReadToEnd();          
        }
        public static string ToJSON<clase>(clase entidad)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Serialize(entidad);
        }
        public static string XmlWithoutEncoding(string sXml)
        {
            return sXml.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "").
                        Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
        }
        public static clase XmlToObject<clase>(string entidad)
        {
            XmlSerializer xml = new XmlSerializer(typeof(clase), " ");
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);
            sw.Write(entidad);
            sw.Flush();
            ms.Position = 0;
            return (clase)xml.Deserialize(ms);
        }
        public static clase JsonToObject<clase>(string entidad)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return (clase)jsonSerializer.Deserialize(entidad, typeof(clase));
        }        
        public static string GetPathConfigFile()
        {            
            return ConfigurationManager.AppSettings[CTools.APP_SETTING_PATH_FILE].ToString();
        }       
        public static void ValidaDatos(string path)
        {            
            if (string.IsNullOrEmpty(path))
            {
                throw new Exception(ERR_NO_CONFIG_PATH);
            }
            if (!System.IO.File.Exists(path + NAME_FILE_CONFIG))
            {
                throw new Exception(string.Format(ERR_NO_EXIST_CONFIG_PATH, path + NAME_FILE_CONFIG));
            }
        }
        public static string GetFileContent(string path)
        {
            StringBuilder sbContenido = new StringBuilder("");
            if (System.IO.File.Exists(path))
            {
                string[] errores = System.IO.File.ReadAllLines(path);
                foreach (string line in errores) { sbContenido.Append(line); }
                return sbContenido.ToString();
            }
            return String.Empty;
        }
        public static void SaveObject<Entidad>(string path,Entidad objeto,MyEnum.eFormato Formato,bool append,System.Text.Encoding encoding)
        {
            string ObjetoSerializado = String.Empty;
            switch (Formato)
            {
                case MyEnum.eFormato.Xml:
                    ObjetoSerializado = CTools.ToXML<Entidad>(objeto, encoding);
                    break;
                case MyEnum.eFormato.Json:
                    ObjetoSerializado = CTools.ToJSON<Entidad>(objeto);
                    break;
            }
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path, append))
            {
                sw.Write(ObjetoSerializado);
            }            
        }
        public static Entidad FileToObjectFromPath<Entidad>(string path, MyEnum.eFormato Formato)
        {
            string contenido = CTools.GetFileContent(path);
            if (String.IsNullOrEmpty(contenido))
            {
                return default(Entidad);               
            }
            return CTools.FileToObject<Entidad>(contenido, Formato);            
        }
        public static Entidad FileToObject<Entidad>(string EntidadString, MyEnum.eFormato Formato)
        {
            Entidad objeto = default(Entidad);
            switch (Formato)
            {
                case MyEnum.eFormato.Xml:
                    objeto = CTools.XmlToObject<Entidad>(EntidadString);
                    break;
                case MyEnum.eFormato.Json:
                    objeto = CTools.JsonToObject<Entidad>(EntidadString);
                    break;
            }
            return objeto;
        }       
        public static string HtmlEncode (string texto)
        {
            return System.Web.HttpUtility.HtmlEncode(texto);
        }
        public static string HtmlDecode(string texto)
        {
            return System.Web.HttpUtility.HtmlDecode(texto);
        }
    }
}
