using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{
    public class CCriterio
    {
        private const string OP_EQUALS = "=";
        public enum eTipo { Where = 0, Order }
        public enum eTipoWhere { And = 0, Or }
        public enum eTipoOrder { Asc = 0, Desc }
        public eTipo Tipo { set; get; }
        public string Propiedad { set; get; }
        public string Operador { set; get; }
        public object Valor { set; get; }

        public eTipoOrder TipoOrder { set; get; }
        public eTipoWhere TipoWhere { set; get; }
        public CCriterio(string propiedad, string operador, object valor, eTipoWhere tipoWhere)
        {
            Propiedad = propiedad;
            Operador = operador;
            Valor = valor;
            Tipo = eTipo.Where;
            TipoWhere = tipoWhere;
        }
        public CCriterio(string propiedad, string operador, object valor)
        {
            Propiedad = propiedad;
            Operador = operador;
            Valor = valor;
            Tipo = eTipo.Where;
            TipoWhere = eTipoWhere.And;
        }
        public CCriterio(string propiedad, object valor, eTipoWhere tipoWhere)
        {
            Propiedad = propiedad;
            Operador = OP_EQUALS;
            Valor = valor;
            Tipo = eTipo.Where;
            TipoWhere = tipoWhere;
        }
        public CCriterio(string propiedad, object valor)
        {
            Propiedad = propiedad;
            Operador = OP_EQUALS;
            Valor = valor;
            Tipo = eTipo.Where;
            TipoWhere = eTipoWhere.And;
        }
        public CCriterio(string propiedad, eTipoOrder TipOrder)
        {
            Propiedad = propiedad;
            Operador = String.Empty;
            Valor = null;
            Tipo = eTipo.Order;
            TipoOrder = TipOrder;
        }
        public CCriterio(string propiedad)
        {
            Propiedad = propiedad;
            Operador = String.Empty;
            Valor = null;
            Tipo = eTipo.Order;
            TipoOrder = eTipoOrder.Asc;
        }
    }// CCriterio ends.
}// LibEntidades.Entidades ends.

