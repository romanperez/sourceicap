/*LAVM 04/11/2016 11:37:31 a. m.*/
namespace LibEntidades.Entidades
{
    public class MyEnum
    {
        /// <summary>
        /// Todos[Si exsite un error al guardar la lista no se guarda ninguno.]
        /// Correctos[Se guardan aquellos elementos que no generaron error y se descartan los cambios de los que se genero error.]
        /// </summary>
        public enum eSave { Todos = 0, Correctos }
        /// <summary>
        /// Indica las operaciones que provienen del grid para editar los datos.
        /// </summary>
        public enum eOpGrd { Del = 0, Edit, New }
        /// <summary>
        /// Formato para Parsear el objeto a texto.
        /// </summary>
        public enum eFormato { Xml = 0, Json }        
    }
}

