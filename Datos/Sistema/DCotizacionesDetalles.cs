using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:10 p. m.*/
namespace Datos
{

	public partial class DCotizacionesDetalles : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_CotizacionesDetalles]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CCotizacionesDetalles();
		}
		public DCotizacionesDetalles()
			: base()
		{
			Inicializa();
		}
		public DCotizacionesDetalles(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "CotizacionesDetalles";
		}
		/// <summary>
		/// Procesa el objeto ICotizacionesDetalles via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			ICotizacionesDetalles IEntity = Entity as ICotizacionesDetalles;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdCotizacionDetalle", IEntity.IdCotizacionDetalle);
			_Connection.AddParam("@IdCotizacion", IEntity.IdCotizacion);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@Partida", IEntity.Partida);
			_Connection.AddParam("@Cantidad", IEntity.Cantidad);			 
			_Connection.AddParam("@Precio",IEntity.Precio);
			_Connection.AddParam("@Ubicacion", IEntity.Ubicacion);
			_Connection.AddParam("@Concepto", IEntity.Concepto);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);		
			/*_Connection.AddParam("@PorcentajeDescuento", IEntity.PorcentajeDescuento);
			_Connection.AddParam("@DescuentoMonto", IEntity.DescuentoMonto);*/
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdCotizacionDetalle = Convert.ToInt32(_Connection.GetParam("@IdCotizacionDetalle").ToString());

		}
	}//  DCotizacionesDetalles ends.
}// LibDatos.Entidades ends.
