using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 21/02/2017 09:48:13 a. m.*/
namespace Datos
{
    
    public partial class   DCotizacionesDetallesInsumos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_CotizacionesDetallesInsumos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CCotizacionesDetallesInsumos();
	}		
        public DCotizacionesDetallesInsumos():base()
	{
	  Inicializa();
	}
	public DCotizacionesDetallesInsumos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "CotizacionesDetallesInsumos";
	} 
	/// <summary>
        /// Procesa el objeto ICotizacionesDetallesInsumos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  ICotizacionesDetallesInsumos IEntity = Entity as ICotizacionesDetallesInsumos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdInsumoCotizacion",IEntity.IdInsumoCotizacion); 
				 _Connection.AddParam("@IdCotizacion", IEntity.IdCotizacion);
				 _Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
				 _Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
				 _Connection.AddParam("@Cantidad", IEntity.Cantidad);
				 _Connection.AddParam("@Costo", IEntity.Costo);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdInsumoCotizacion = Convert.ToInt32( _Connection.GetParam("@IdInsumoCotizacion").ToString());
 			
	}	
    }//  DCotizacionesDetallesInsumos ends.
}// LibDatos.Entidades ends.
