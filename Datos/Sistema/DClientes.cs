using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:09 p. m.*/
namespace Datos
{

	public partial class DClientes : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Clientes]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CClientes();
		}
		public DClientes()
			: base()
		{
			Inicializa();
		}
		public DClientes(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Clientes";			
		}
		/// <summary>
		/// Procesa el objeto IClientes via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IClientes IEntity = Entity as IClientes;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdCliente", IEntity.IdCliente);
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@RazonSocial", IEntity.RazonSocial);
			_Connection.AddParam("@NombreComercial", IEntity.NombreComercial);
			_Connection.AddParam("@Rfc", IEntity.Rfc);
			_Connection.AddParam("@Telefono", IEntity.Telefono);
			_Connection.AddParam("@Direccion", IEntity.Direccion);
			_Connection.AddParam("@CodigoPostal", IEntity.CodigoPostal);
			_Connection.AddParam("@Estatus", IEntity.Estatus);

			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdCliente = Convert.ToInt32(_Connection.GetParam("@IdCliente").ToString());

		}
	}//  DClientes ends.
}// LibDatos.Entidades ends.
