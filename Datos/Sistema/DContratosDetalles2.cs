using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 30/03/2017 03:46:57 p. m.*/
namespace Datos
{
	public partial class DContratosDetalles2 : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_ContratosDetalles2]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CContratosDetalles2();
		}
		public DContratosDetalles2()
			: base()
		{
			Inicializa();
		}
		public DContratosDetalles2(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected virtual void Inicializa()
		{
			_Tabla = "ContratosDetalles2";
		}
		/// <summary>
		/// Procesa el objeto IContratosDetalles2 via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IContratosDetalles2 IEntity = Entity as IContratosDetalles2;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContratosDetalles", IEntity.IdContratosDetalles);
			_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@Partida", IEntity.Partida);
            ///El precio que se almacena ya contienen el aumento de la utilidad es decir precio del concepto + el % utilidad del contrato.
			_Connection.AddParam("@Precio", IEntity.Precio);
			/*_Connection.AddParam("@PorcentajeDescuento", IEntity.PorcentajeDescuento);
			_Connection.AddParam("@DescuentoMonto", IEntity.DescuentoMonto);*/
			_Connection.AddParam("@Cantidad", IEntity.Cantidad);
			_Connection.AddParam("@Ubicacion", IEntity.Ubicacion);
			_Connection.AddParam("@Concepto", IEntity.Concepto);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@EsAdicional", IEntity.EsAdicional);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdContratosDetalles = Convert.ToInt32(_Connection.GetParam("@IdContratosDetalles").ToString());
		}
	}//  DContratosDetalles2 ends.
	public class DvwContratosDetalles2:DContratosDetalles2
	{
		public const string VIEW_CONTRATOS_DETLLES2 = "vwContratosDetalles2";
		public DvwContratosDetalles2()
			: base()
		{
			Inicializa();
		}
		protected override void Inicializa()
		{
			_Tabla = DvwContratosDetalles2.VIEW_CONTRATOS_DETLLES2;
		}
		public override IEntidad BuildEntity()
		{
			return new CvwContratosDetalles2();
		}
	}
}// LibDatos.Entidades ends.