using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:10 p. m.*/
namespace Datos
{
	public partial class DContratos : DBase
	{
		//private const string SP_SAVE = "[dbo].[prc_Contratos]";
		private const string SP_SAVE = "[dbo].[prc_Contratos2]";
		
		protected const string SP_CONTRATO_DETALLES = "[dbo].[prc_contrato_detalles]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CContratos2();
		}
		public DContratos()
			: base()
		{
			Inicializa();
		}
		public DContratos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Contratos2";
		}
		/// <summary>
		/// Procesa el objeto IContratos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{			
			//IContratos IEntity = Entity as IContratos;
			//_Connection.Query = SP_SAVE;
			//_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			//_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContrato", IEntity.IdContrato);
			//_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			//_Connection.AddParam("@IdProyecto", IEntity.IdProyecto);
			//_Connection.AddParam("@Contrato", IEntity.Contrato);
			//_Connection.AddParam("@Aprobado", IEntity.Aprobado);
			//_Connection.AddParam("@Estatus", IEntity.Estatus);
			//_Connection.AddParam("@IdUsuario", IEntity.IdUsuario); /*Se necesita el id de usuario para identificar quien realiza la salida y el pedido de materiales*/
			//_Connection.AddParam("@Delete_Entity", this.Delete);
			//_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			//_Connection.NonQuery();
			//IEntity.IdContrato = Convert.ToInt32(_Connection.GetParam("@IdContrato").ToString());


			IContratos2 IEntity = Entity as IContratos2;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@SubTotal", IEntity.SubTotal);
			_Connection.AddParam("@Total", IEntity.Total);
			_Connection.AddParam("@PorcentajeDescuento", IEntity.PorcentajeDescuento);
			_Connection.AddParam("@DescuentoMonto", IEntity.DescuentoMonto);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdCliente", IEntity.IdCliente);
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@IdIva", IEntity.IdIva);
			_Connection.AddParam("@IdTipoCambio", IEntity.IdTipoCambio);
			_Connection.AddParam("@IdMoneda", IEntity.IdMoneda);
			_Connection.AddParam("@IdPedido", IEntity.IdPedido);
			_Connection.AddParam("@Aprobado", IEntity.Aprobado);
			_Connection.AddParam("@Contrato", IEntity.Contrato);
			_Connection.AddParam("@Fecha", IEntity.Fecha);
			_Connection.AddParam("@IdUsuario", IEntity.IdUsuario); /*Se necesita el id de usuario para identificar quien realiza la salida y el pedido de materiales*/
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdContrato = Convert.ToInt32(_Connection.GetParam("@IdContrato").ToString());
			SaveDetail(IEntity);
		}
		public IEntidad CreaContratoDetalle()
		{
			return new CContratosDetalles();
		}
		public IEntidad CreaContratoDetalleInsumo()
		{
			return new CContratosDetallesInsumos();
		}
		public IEnumerable<IContratosDetalles> GetConceptos(IContratos contrato)
		{
			try
			{
				OpenConexion();
				_Connection.Query = SP_CONTRATO_DETALLES;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdContrato", contrato.IdContrato);
				_Connection.AddParam("@IdProyecto", contrato.IdProyecto);
				_Connection.AddParam("@IdOperacion", 1);
				_Connection.NonQuery();
				return FillEntidades(_Connection.GetReader(), CreaContratoDetalle).Select(x => x as IContratosDetalles);
			}
			finally
			{
				CloseConnection();
			}
		}
		public IEnumerable<IContratosDetallesInsumos> GetInsumos(IContratos contrato)
		{
			try
			{
				OpenConexion();
				_Connection.Query = SP_CONTRATO_DETALLES;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdContrato", contrato.IdContrato);
				_Connection.AddParam("@IdProyecto", contrato.IdProyecto);
				_Connection.AddParam("@IdOperacion", 2);
				_Connection.NonQuery();
				return FillEntidades(_Connection.GetReader(), CreaContratoDetalleInsumo).Select(x => x as IContratosDetallesInsumos);
			}
			finally
			{
				CloseConnection();
			}
		}
		protected virtual void SaveDetail(IContratos2 Entity)
		{			
			if(Entity == null) return;
			if(Entity.Conceptos != null && Entity.Conceptos.Count() > 0)
			{
				foreach(IContratosDetalles2 detalle in Entity.Conceptos)
				{
					DContratosDetalles2 _ddetail = new DContratosDetalles2(_Connection);
					detalle.IdContrato = Entity.IdContrato;
					if(detalle.IdContratosDetalles < 0)
					{
						detalle.IdContratosDetalles = detalle.IdContratosDetalles * -1;
						_ddetail.Delete = 1;					
					}
					_ddetail.Save(detalle);					
				}
			}
		}
	}//  DContratos ends.
}// LibDatos.Entidades ends.