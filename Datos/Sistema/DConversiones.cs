using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 06/04/2017 03:52:29 p. m.*/
namespace Datos
{

	public partial class DConversiones : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Conversiones]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CConversiones();
		}
		public DConversiones()
			: base()
		{
			Inicializa();
		}
		public DConversiones(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Conversiones";
		}
		/// <summary>
		/// Procesa el objeto IConversiones via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IConversiones IEntity = Entity as IConversiones;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdConversion", IEntity.IdConversion);
			_Connection.AddParam("@IdMonedaOrigen", IEntity.IdMonedaOrigen);
			_Connection.AddParam("@IdMonedaDestino", IEntity.IdMonedaDestino);
			_Connection.AddParam("@Operador", IEntity.Operador);

			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdConversion = Convert.ToInt32(_Connection.GetParam("@IdConversion").ToString());

		}
	}//  DConversiones ends.
}// LibDatos.Entidades ends.
