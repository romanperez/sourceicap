using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 30/01/2017 02:07:45 p. m.*/
namespace Datos
{
	
	public partial class DTipoCambios : DBase
	{		
		private const string SP_SAVE = "[dbo].[prc_TipoCambios]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CTipoCambios();
		}
		public DTipoCambios()
			: base()
		{
			Inicializa();
		}
		public DTipoCambios(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "TipoCambios";
		}
		/// <summary>
		/// Procesa el objeto ITipoCambios via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			ITipoCambios IEntity = Entity as ITipoCambios;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdTipoCambio", IEntity.IdTipoCambio);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdMonedaOrigen", IEntity.IdMonedaOrigen);
			_Connection.AddParam("@IdMonedaDestino", IEntity.IdMonedaDestino);
			_Connection.AddParam("@FechaInicio", IEntity.FechaInicio);
			_Connection.AddParam("@FechaFin", IEntity.FechaFin);
			_Connection.AddParam("@Valor", IEntity.Valor);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdTipoCambio = Convert.ToInt32(_Connection.GetParam("@IdTipoCambio").ToString());
		}
	}//  DTipoCambios ends.
}// LibDatos.Entidades ends.
