using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 19/06/2017 05:00:01 p. m.*/
namespace Datos
{
    
    public partial class   DContratosCotizaciones:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_ContratosCotizaciones]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CContratosCotizaciones();
	}		
        public DContratosCotizaciones():base()
	{
	  Inicializa();
	}
	public DContratosCotizaciones(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "ContratosCotizaciones";
	} 
	/// <summary>
        /// Procesa el objeto IContratosCotizaciones via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IContratosCotizaciones IEntity = Entity as IContratosCotizaciones;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContratoCotizacion",IEntity.IdContratoCotizacion); 
				 _Connection.AddParam("@IdContrato", IEntity.IdContrato);
				 _Connection.AddParam("@IdCotizacion", IEntity.IdCotizacion);
				 _Connection.AddParam("@Fecha", IEntity.Fecha);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdContratoCotizacion = Convert.ToInt32( _Connection.GetParam("@IdContratoCotizacion").ToString());
 			
	}	
    }//  DContratosCotizaciones ends.
}// LibDatos.Entidades ends.
