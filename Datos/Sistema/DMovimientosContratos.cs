using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 23/06/2017 01:59:22 p. m.*/
namespace Datos
{
	public partial class DMovimientosContratos : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_MovimientosContratos]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CMovimientosContratos();
		}
		public DMovimientosContratos()
			: base()
		{
			Inicializa();
		}
		public DMovimientosContratos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "MovimientosContratos";
		}
		/// <summary>
		/// Procesa el objeto IMovimientosContratos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IMovimientosContratos IEntity = Entity as IMovimientosContratos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdMovimientoContrato", IEntity.IdMovimientoContrato);
			_Connection.AddParam("@IdProyecto", IEntity.IdProyecto);
			_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@IdMovimiento", IEntity.IdMovimiento);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
			_Connection.AddParam("@IdPedidoDetalle", IEntity.IdPedidoDetalle);
			_Connection.AddParam("@IdMovimientoDetalle", IEntity.IdMovimientoDetalle);
			_Connection.AddParam("@IdMovimientoTraspaso", IEntity.IdMovimientoTraspaso);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdMovimientoContrato = Convert.ToInt32(_Connection.GetParam("@IdMovimientoContrato").ToString());
		}
	}//  DMovimientosContratos ends.
}// LibDatos.Entidades ends.
