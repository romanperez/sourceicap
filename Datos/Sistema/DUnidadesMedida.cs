using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:15 p. m.*/
namespace Datos
{
	public partial class DUnidadesMedida : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_UnidadesMedida]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CUnidadesMedida();
		}
		public DUnidadesMedida()
			: base()
		{
			Inicializa();
		}
		public DUnidadesMedida(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "UnidadesMedida";
		}
		/// <summary>
		/// Procesa el objeto IUnidadesMedida via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IUnidadesMedida IEntity = Entity as IUnidadesMedida;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@Unidad", IEntity.Unidad);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdUnidad = Convert.ToInt32(_Connection.GetParam("@IdUnidad").ToString());
		}
	}//  DUnidadesMedida ends.
}// LibDatos.Entidades ends.
