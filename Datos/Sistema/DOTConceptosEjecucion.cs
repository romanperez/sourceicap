using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 05/05/2017 01:04:02 p. m.*/
namespace Datos
{

	public partial class DOTConceptosEjecucion : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_OTConceptosEjecucion]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new COTConceptosEjecucion();
		}
		public DOTConceptosEjecucion()
			: base()
		{
			Inicializa();
		}
		public DOTConceptosEjecucion(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "OTConceptosEjecucion";
		}
		/// <summary>
		/// Procesa el objeto IOTConceptosEjecucion via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IOTConceptosEjecucion IEntity = Entity as IOTConceptosEjecucion;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdOTConceptoEjecucion", IEntity.IdOTConceptoEjecucion);
			_Connection.AddParam("@IdOTConcepto", IEntity.IdOTConcepto);
			_Connection.AddParam("@CantidadProgramada", IEntity.CantidadProgramada);
			_Connection.AddParam("@CantidadEjecutada", IEntity.CantidadEjecutada);
			_Connection.AddParam("@Fecha", IEntity.Fecha);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdOTConceptoEjecucion = Convert.ToInt32(_Connection.GetParam("@IdOTConceptoEjecucion").ToString());
		}
	}//  DOTConceptosEjecucion ends.
}// LibDatos.Entidades ends.
