using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:10 p. m.*/
namespace Datos
{

	public partial class DContratosDetalles : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_ContratosDetalles]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CContratosDetalles();
		}
		public DContratosDetalles()
			: base()
		{
			Inicializa();
		}
		public DContratosDetalles(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "ContratosDetalles";
		}
		/// <summary>
		/// Procesa el objeto IContratosDetalles via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IContratosDetalles IEntity = Entity as IContratosDetalles;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContratoDetalle", IEntity.IdContratoDetalle);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@Partida", IEntity.Partida);
			_Connection.AddParam("@Precio", IEntity.Precio);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdContratoDetalle = Convert.ToInt32(_Connection.GetParam("@IdContratoDetalle").ToString());
		}
	}//  DContratosDetalles ends.
}// LibDatos.Entidades ends.
