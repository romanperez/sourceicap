using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{

    public partial class DIngenierias : DBase
    {
        private const string SP_SAVE = "[dbo].[prc_Ingenierias]";
        public override Entidades.IEntidad BuildEntity()
        {
            return new CIngenierias();
        }
        public DIngenierias()
            : base()
        {
            Inicializa();
        }
        public DIngenierias(IConexion cnn)
            : base(cnn)
        {
            Inicializa();
        }
        protected void Inicializa()
        {
            _Tabla = "Ingenierias";
        }
        /// <summary>
        /// Procesa el objeto IIngenierias via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
        protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
        {
            IIngenierias IEntity = Entity as IIngenierias;
            _Connection.Query = SP_SAVE;
            _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
            _Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdIngenieria", IEntity.IdIngenieria);
            _Connection.AddParam("@IdProyecto", IEntity.IdProyecto);
            _Connection.AddParam("@Ingenieria", IEntity.Ingenieria);
            _Connection.AddParam("@Fecha", IEntity.Fecha);
            _Connection.AddParam("@Estatus", IEntity.Estatus);
            _Connection.AddParam("@Delete_Entity", this.Delete);
            _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
            _Connection.NonQuery();
            IEntity.IdIngenieria = Convert.ToInt32(_Connection.GetParam("@IdIngenieria").ToString());

        }
    }//  DIngenierias ends.
}// LibDatos.Entidades ends.
