using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:09 p. m.*/
namespace Datos
{

	public partial class DConceptosInsumos : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_ConceptosInsumos]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CConceptosInsumos();
		}
		public DConceptosInsumos()
			: base()
		{
			Inicializa();
		}
		public DConceptosInsumos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "ConceptosInsumos";
		}
		/// <summary>
		/// Procesa el objeto IConceptosInsumos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IConceptosInsumos IEntity = Entity as IConceptosInsumos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdConceptoInsumo", IEntity.IdConceptoInsumo);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
			_Connection.AddParam("@Cantidad", IEntity.Cantidad);
			_Connection.AddParam("@CostoInsumo", IEntity.CostoInsumo);
			_Connection.AddParam("@IdMoneda", IEntity.IdMoneda);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdConceptoInsumo = Convert.ToInt32(_Connection.GetParam("@IdConceptoInsumo").ToString());
		}
	}//  DConceptosInsumos ends.
}// LibDatos.Entidades ends.
