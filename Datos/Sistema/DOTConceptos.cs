using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
namespace Datos
{
	public partial class DOTConceptos : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_OTConceptos]";
		private const string SP_GET_CONCEPTOS = "[dbo].[prc_get_conceptos_ot]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new COTConceptos();
		}
		public DOTConceptos()
			: base()
		{
			Inicializa();
		}
		public DOTConceptos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "OTConceptos";
		}
		/// <summary>
		/// Procesa el objeto IOTConceptos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IOTConceptos IEntity = Entity as IOTConceptos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdOTConcepto", IEntity.IdOTConcepto);
			_Connection.AddParam("@IdOrdenTrabajo", IEntity.IdOrdenTrabajo);
			_Connection.AddParam("@IdContratosDetalles", IEntity.IdContratosDetalles);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);			
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdOTConcepto = Convert.ToInt32(_Connection.GetParam("@IdOTConcepto").ToString());
		}
		public IEnumerable<IOTConceptos> GetConceptos(string ids)
		{
			try
			{				
				int data = 0;
				List<COTConceptos> conceptos = new List<COTConceptos>();
				List<CContratosDetallesInsumos2> insumos = new List<CContratosDetallesInsumos2>();
				OpenConexion();
				_Connection.Query = SP_GET_CONCEPTOS;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@OrdenesTrabajo", ids);
				IDataReader drDatos = _Connection.GetReader();
				while(_Connection.HasRows(drDatos) || data < 2)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								conceptos.Add(GetEntity(drDatos, new COTConceptos()) as COTConceptos);
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CContratosDetallesInsumos2()) as CContratosDetallesInsumos2);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 2) ends.
				if(conceptos != null && conceptos.Count() > 0)
				{
					if(insumos != null && insumos.Count() > 0)
					{
						conceptos.ForEach(c =>
						{
							IEnumerable<CContratosDetallesInsumos2> res = insumos.Where(i => i.IdContratosDetalles == c.IdContratosDetalles);
							if(res != null && res.Count() > 0)
							{
								c.Insumos = res.ToList();
							}
						});
					}
				}//if ends.
				return conceptos;
			}
			finally
			{
				CloseConnection();
			}			
		}
	}//  DOTConceptos ends.
}// LibDatos.Entidades ends.