using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 03/10/2017 11:24:14 a. m.*/
namespace Datos
{
	public partial class DEstimaciones : DBase
	{
        protected const string SP_SAVE = "[dbo].[prc_Estimaciones]";
        protected const string PRC_RPT_ESTIMACION_CANTIDADES = "[dbo].[prc_rpt_estimacion_cantidades]";
        protected const string PRC_RPT_ESTIMACION_RESUMEN_ECONOMICO= "[dbo].[prc_rpt_estimacion_resumen_economico]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CEstimaciones();
		}
        public  Entidades.IEntidad BuildEntityResumenEconomico()
        {
            return new CEstimacionesResumenEconomico();
        }
		public DEstimaciones()
			: base()
		{
			Inicializa();
		}
		public DEstimaciones(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected virtual void Inicializa()
		{
			_Tabla = "Estimaciones";
		}
		/// <summary>
		/// Procesa el objeto IEstimaciones via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IEstimaciones IEntity = Entity as IEstimaciones;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdEstimacion", IEntity.IdEstimacion);
			_Connection.AddParam("@IdContratosDetalles", IEntity.IdContratosDetalles);
            _Connection.AddParam("@NumeroEstimacion", IEntity.NumeroEstimacion);
            _Connection.AddParam("@FechaInicio", IEntity.FechaInicio);
            _Connection.AddParam("@FechaFin", IEntity.FechaFin);
			_Connection.AddParam("@CantidadEstimada", IEntity.CantidadEstimada);
			_Connection.AddParam("@ImporteEstimado", IEntity.ImporteEstimado);
            _Connection.AddParam("@ImporteDeductivo", IEntity.ImporteDeductivo);
            //_Connection.AddParam("@PorcentajeAmortizacionAnticipo", IEntity.PorcentajeAmortizacionAnticipo);      
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdEstimacion = Convert.ToInt32(_Connection.GetParam("@IdEstimacion").ToString());
		}
        public IEnumerable<IEstimaciones> ReporteEstimacionesCantidades(IEstimaciones IEntity)
        {
            try
            {
                OpenConexion();
                _Connection.Query = PRC_RPT_ESTIMACION_CANTIDADES;
                _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                _Connection.AddParam("@IdContrato", IEntity.IdContrato);
                _Connection.AddParam("@NumeroEstimacion", IEntity.NumeroEstimacion);
                _Connection.NonQuery();
                IEnumerable<IEntidad> result = FillEntidades(_Connection.GetReader());
                if (result != null && result.Count() > 0)
                {
                    return result.Select(x => x as IEstimaciones);
                }
                return null;
            }
            finally
            {
                CloseConnection();
            }
        }
        public IEnumerable<IEstimacionesResumenEconomico> ReporteEstimacionesResumenEconomico(IEstimacionesResumenEconomico IEntity)
        {
            try
            {
                //prc_rpt_estimacion_resumen_economico
                OpenConexion();
                _Connection.Query = PRC_RPT_ESTIMACION_RESUMEN_ECONOMICO;
                _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                _Connection.AddParam("@IdContrato", IEntity.IdContrato);
                _Connection.AddParam("@IdEstimacionIni", IEntity.IdEstimacion);
                _Connection.AddParam("@IdEstimacionFin", IEntity.NumeroEstimacion);
                _Connection.AddParam("@PorcentajeAnticipo", IEntity.PorcentajeAmortizacionAnticipo);
                _Connection.NonQuery();
                IEnumerable<IEntidad> result = FillEntidades(_Connection.GetReader(),this.BuildEntityResumenEconomico);
                if (result != null && result.Count() > 0)
                {
                    return result.Select(x => x as IEstimacionesResumenEconomico);
                }
                return null;
            }
            finally
            {
                CloseConnection();
            }
        }
	}//  DEstimaciones ends.
    public class DvwEstimaciones : DEstimaciones
    {
        public const string VIEW_ESTIMACIONES = "vwEstimaciones";
        public DvwEstimaciones()
            : base()
        {
            Inicializa();
        }
        protected override void Inicializa()
        {
            _Tabla = DvwEstimaciones.VIEW_ESTIMACIONES;
        }
        public override IEntidad BuildEntity()
        {
            return new CEstimaciones();
        }
    }
}// LibDatos.Entidades ends.