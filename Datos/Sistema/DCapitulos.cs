using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 30/01/2017 02:07:45 p. m.*/
namespace Datos
{
    
    public partial class   DCapitulos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_Capitulos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CCapitulos();
	}		
        public DCapitulos():base()
	{
	  Inicializa();
	}
	public DCapitulos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "Capitulos";
	} 
	/// <summary>
        /// Procesa el objeto ICapitulos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  ICapitulos IEntity = Entity as ICapitulos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdCapitulo",IEntity.IdCapitulo); 
				 _Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
				 _Connection.AddParam("@Capitulo", IEntity.Capitulo);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdCapitulo = Convert.ToInt32( _Connection.GetParam("@IdCapitulo").ToString());
 			
	}	
    }//  DCapitulos ends.
}// LibDatos.Entidades ends.
