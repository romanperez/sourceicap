using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 02/06/2017 11:10:20 a. m.*/
namespace Datos
{

	public partial class DCotizacionesPersonalCliente : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_CotizacionesPersonalCliente]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CCotizacionesPersonalCliente();
		}
		public DCotizacionesPersonalCliente()
			: base()
		{
			Inicializa();
		}
		public DCotizacionesPersonalCliente(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "CotizacionesPersonalCliente";
		}
		/// <summary>
		/// Procesa el objeto ICotizacionesPersonalCliente via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			ICotizacionesPersonalCliente IEntity = Entity as ICotizacionesPersonalCliente;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdCotizacionPersonalCliente", IEntity.IdCotizacionPersonalCliente);
			_Connection.AddParam("@IdCotizacion", IEntity.IdCotizacion);
			_Connection.AddParam("@IdEstatus", IEntity.IdEstatus);
			_Connection.AddParam("@IdPersonalCliente", IEntity.IdPersonalCliente);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdCotizacionPersonalCliente = Convert.ToInt32(_Connection.GetParam("@IdCotizacionPersonalCliente").ToString());
		}
	}//  DCotizacionesPersonalCliente ends.
}// LibDatos.Entidades ends.
