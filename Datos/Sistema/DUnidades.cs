using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 21/12/2016 12:08:31 p. m.*/
namespace Datos
{
    
    public partial class   DUnidades:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_Unidades]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CUnidades();
	}		
        public DUnidades():base()
	{
	  Inicializa();
	}
	public DUnidades(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "Unidades";
	} 
	/// <summary>
        /// Procesa el objeto IUnidades via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IUnidades IEntity = Entity as IUnidades;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdUnidad",IEntity.IdUnidad); 
				 _Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
				 _Connection.AddParam("@Unidad", IEntity.Unidad);
				 _Connection.AddParam("@Direccion", IEntity.Direccion);
				 _Connection.AddParam("@Telefono", IEntity.Telefono);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdUnidad = Convert.ToInt32( _Connection.GetParam("@IdUnidad").ToString());
 			
	}	
    }//  DUnidades ends.
}// LibDatos.Entidades ends.
