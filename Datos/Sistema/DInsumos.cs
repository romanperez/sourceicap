using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{

	public partial class DInsumos : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Insumos]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CInsumos();
		}
		public DInsumos()
			: base()
		{
			Inicializa();
		}
		public DInsumos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Insumos";
		}
		/// <summary>
		/// Procesa el objeto IInsumos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IInsumos IEntity = Entity as IInsumos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdInsumo", IEntity.IdInsumo);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdFamilia", IEntity.IdFamilia);			 
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@Codigo", IEntity.Codigo);
			_Connection.AddParam("@Serie", IEntity.Serie);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@IdMoneda", IEntity.IdMoneda);
			_Connection.AddParam("@EsServicio", IEntity.EsServicio);
			if(IEntity.EsServicio)
			{
				_Connection.AddParam("@Costo", IEntity.Costo);
			}			
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdInsumo = Convert.ToInt32(_Connection.GetParam("@IdInsumo").ToString());

		}
	}//  DInsumos ends.
}// LibDatos.Entidades ends.
