using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:15 p. m.*/
namespace Datos
{
	public partial class DTecnicosGrados : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_TecnicosGrados]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CTecnicosGrados();
		}
		public DTecnicosGrados()
			: base()
		{
			Inicializa();
		}
		public DTecnicosGrados(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "TecnicosGrados";
		}
		/// <summary>
		/// Procesa el objeto ITecnicosGrados via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			ITecnicosGrados IEntity = Entity as ITecnicosGrados;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdGrado", IEntity.IdGrado);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@Codigo", IEntity.Codigo);
			_Connection.AddParam("@Nombre", IEntity.Nombre);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdGrado = Convert.ToInt32(_Connection.GetParam("@IdGrado").ToString());
		}
	}//  DTecnicosGrados ends.
}// LibDatos.Entidades ends.
