using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{
    
    public partial class   DInsumosExistencias:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_InsumosExistencias]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CInsumosExistencias();
	}		
        public DInsumosExistencias():base()
	{
	  Inicializa();
	}
	public DInsumosExistencias(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "InsumosExistencias";
	} 
	/// <summary>
        /// Procesa el objeto IInsumosExistencias via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IInsumosExistencias IEntity = Entity as IInsumosExistencias;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdExistencia",IEntity.IdExistencia); 
				 _Connection.AddParam("@IdAlmacen", IEntity.IdAlmacen);
				 _Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
				 _Connection.AddParam("@Cantidad", IEntity.Cantidad);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdExistencia = Convert.ToInt32( _Connection.GetParam("@IdExistencia").ToString());
 			
	}	
    }//  DInsumosExistencias ends.
}// LibDatos.Entidades ends.
