using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{

	public partial class DMenus : DBase
	{
		protected const string SP_SAVE = "[dbo].[prc_Menus]";
		protected const string SP_GET_MENU_BY_PERFIL = "[dbo].[prc_get_menu_by_perfil]";

		public override Entidades.IEntidad BuildEntity()
		{
			return new CMenus();
		}
		public DMenus()
			: base()
		{
			Inicializa();
		}
		public DMenus(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Menus";
		}
		/// <summary>
		/// Procesa el objeto IMonedas via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(eCommit tipo, IEntidad Entity)
		{
			IMenus IEntity = Entity as IMenus;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdMenu", IEntity.IdMenu);
			_Connection.AddParam("@Menu", IEntity.Menu);			
			_Connection.AddParam("@Titulo", IEntity.Titulo);			
			_Connection.AddParam("@IdMenuPadre", IEntity.IdMenuPadre);			
			_Connection.AddParam("@Imagen", IEntity.Imagen);
			_Connection.AddParam("@ImagenTab", IEntity.ImagenTab);			
			_Connection.AddParam("@Url", IEntity.Url);
			_Connection.AddParam("@Orden", IEntity.Orden);
			_Connection.AddParam("@Parametros", IEntity.Parametros);
			_Connection.AddParam("@Estatus", IEntity.Estatus);            
            _Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdMenu = Convert.ToInt32(_Connection.GetParam("@IdMenu").ToString());
		}
		protected void SpGetMenus()
		{
			_Connection.Query = SP_GET_MENU_BY_PERFIL;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
		}
		protected void BuildSpGetMenusByPerfil(IPerfiles perfil)
		{
			SpGetMenus();
			_Connection.AddParam("@IdPerfil", perfil.IdPerfil);
		}		
		public virtual IEnumerable<IMenus> GetMenuByPerfil(IPerfiles perfil)
		{			
			//if(perfil == null) return null;
			List<IMenus> Menus= null;
			try
			{
				Menus = new List<IMenus>();
				OpenConexion();
				if(perfil == null)
				{
					SpGetMenus();
				}
				else
				{
					BuildSpGetMenusByPerfil(perfil);
				}
				
				IDataReader drDatos = _Connection.GetReader();
				while(drDatos.Read())
				{
					IMenus mnu = GetEntity(drDatos, new CMenus())as IMenus;
					Menus.Add(mnu);
				}			
			}
			finally
			{
				CloseConnection();
			}
			return Menus;
		}
	}//  DMonedas ends.
}// LibDatos.Entidades ends.
