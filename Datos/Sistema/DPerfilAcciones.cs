using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 04/01/2017 10:37:35 a. m.*/
namespace Datos
{
    
    public partial class   DPerfilAcciones:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_PerfilAcciones]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CPerfilAcciones();
	}		
        public DPerfilAcciones():base()
	{
	  Inicializa();
	}
	public DPerfilAcciones(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "PerfilAcciones";
	} 
	/// <summary>
        /// Procesa el objeto IPerfilAcciones via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IPerfilAcciones IEntity = Entity as IPerfilAcciones;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdPerfilAcciones",IEntity.IdPerfilAcciones); 
				 _Connection.AddParam("@IdAccion", IEntity.IdAccion);
				 _Connection.AddParam("@IdPerfil", IEntity.IdPerfil);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdPerfilAcciones = Convert.ToInt32( _Connection.GetParam("@IdPerfilAcciones").ToString());
 			
	}	
    }//  DPerfilAcciones ends.
}// LibDatos.Entidades ends.
