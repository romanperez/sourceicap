using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:15 p. m.*/
namespace Datos
{
	public partial class DTecnicos : DBase
	{
		protected const string SP_SAVE = "[dbo].[prc_Tecnicos]";
		protected const string SP_TECNICOS_TOP = "[dbo].[prc_get_Tecnicos_top]";

		
		public override Entidades.IEntidad BuildEntity()
		{
			return new CTecnicos();
		}
		public DTecnicos()
			: base()
		{
			Inicializa();
		}
		public DTecnicos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Tecnicos";
		}
		/// <summary>
		/// Procesa el objeto ITecnicos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{			
			foreach(IEspecialidades especialidad in ((ITecnicos)Entity).Especialidades)
			{
				ITecnicos IEntity = this.BuildEntity() as ITecnicos;
				IEntity.IdEmpresa = ((ITecnicos)Entity).IdEmpresa;
				IEntity.IdEmpleado = ((ITecnicos)Entity).IdEmpleado;
				IEntity.IdTecnico = especialidad.IdTecnico;
				IEntity.IdEspecialidad = especialidad.IdEspecialidad;
				IEntity.IdGrado = especialidad.IdGrado;
				if(IEntity.IdTecnico < 0)
				{
					IEntity.IdTecnico = IEntity.IdTecnico * -1;
					this.Delete = 1;
				}				
				_Connection.Query = SP_SAVE;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdTecnico", IEntity.IdTecnico);
				_Connection.AddParam("@IdEmpleado", IEntity.IdEmpleado);
				_Connection.AddParam("@IdEspecialidad", IEntity.IdEspecialidad);				
				_Connection.AddParam("@IdGrado", IEntity.IdGrado);
				_Connection.AddParam("@Delete_Entity", this.Delete);
				_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
				_Connection.NonQuery();
				IEntity.IdTecnico = Convert.ToInt32(_Connection.GetParam("@IdTecnico").ToString());
			}			
		}
		public IEnumerable<ITecnicos> CustomTop(int top)
		{
			IEnumerable<ITecnicos> tecnicosTop = null;
			try
			{
				IEnumerable<IEntidad> tecnicosTopAux = null;
				IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				IPropsWhere condicion = _Where.Where(a => a.NombreCampo == "NombreEmpleado").FirstOrDefault();
				IPropsWhere especialidad = _Where.Where(a => a.NombreCampo == "IdEspecialidad").FirstOrDefault();
				if(empresa == null || condicion == null || especialidad == null) return null;	
				OpenConexion();
				_Connection.Query = SP_TECNICOS_TOP;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);				
				_Connection.AddParam("@Top", top);
				_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor.ToString()));
				_Connection.AddParam(SqlDbType.NVarChar, -1, ParameterDirection.Input, "@Condicion", condicion.Valor);
				_Connection.AddParam("@IdEspecialidad", Convert.ToInt32(especialidad.Valor.ToString()));
				_Connection.NonQuery();
				tecnicosTopAux = FillEntidades(_Connection.GetReader());
				if(tecnicosTopAux.Any())
				{
					tecnicosTop = tecnicosTopAux.Select(x => x as ITecnicos);
				}
			}
			finally
			{
				CloseConnection();
			}
			return tecnicosTop;
		}
	}//  DTecnicos ends.
}// LibDatos.Entidades ends.