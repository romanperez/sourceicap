using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 08/12/2016 05:26:51 p. m.*/
namespace Datos
{
    
    public partial class   DSisLogErrores:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_SIS_LOG_ERRORES]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CSisLogErrores();
	}		
        public DSisLogErrores():base()
	{
	  Inicializa();
	}
	public DSisLogErrores(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "SIS_LOG_ERRORES";
	} 
	/// <summary>
        /// Procesa el objeto ISisLogErrores via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  ISisLogErrores IEntity = Entity as ISisLogErrores;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.BigInt, ParameterDirection.InputOutput, "@LogErrorId",IEntity.Log_Error_Id); 
				 _Connection.AddParam("@LogFecha", IEntity.Log_Fecha);
				 _Connection.AddParam("@LogArchivo", IEntity.Log_Archivo);
				 _Connection.AddParam("@LogFuncion", IEntity.Log_Funcion);
				 _Connection.AddParam("@LogLinea", IEntity.Log_Linea);
				 _Connection.AddParam("@LogColumna", IEntity.Log_Columna);
				 _Connection.AddParam("@LogPila", IEntity.Log_Pila);
				 _Connection.AddParam("@LogError", IEntity.Log_Error);
				 _Connection.AddParam("@LogInfo", IEntity.Log_Info);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.Log_Error_Id = Convert.ToInt32( _Connection.GetParam("@LogErrorId").ToString());
 			
	}	
    }//  DSisLogErrores ends.
}// LibDatos.Entidades ends.
