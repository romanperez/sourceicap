using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 21/12/2016 12:08:30 p. m.*/
namespace Datos
{
    
    public partial class   DDepartamentos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_Departamentos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CDepartamentos();
	}		
        public DDepartamentos():base()
	{
	  Inicializa();
	}
	public DDepartamentos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "Departamentos";
	} 
	/// <summary>
        /// Procesa el objeto IDepartamentos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IDepartamentos IEntity = Entity as IDepartamentos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdDepartamento",IEntity.IdDepartamento); 
				 _Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
				 _Connection.AddParam("@Codigo", IEntity.Codigo);
				 _Connection.AddParam("@Nombre", IEntity.Nombre);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdDepartamento = Convert.ToInt32( _Connection.GetParam("@IdDepartamento").ToString());
 			
	}	
    }//  DDepartamentos ends.
}// LibDatos.Entidades ends.
