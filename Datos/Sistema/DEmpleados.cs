using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:11 p. m.*/
namespace Datos
{
	public partial class DEmpleados : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Empleados]";
		private const string SP_GET_ALL_TECNICOS = "[dbo].[prc_get_Tecnicos]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CEmpleados();
		}
		public DEmpleados()
			: base()
		{
			Inicializa();
		}
		public DEmpleados(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Empleados";
		}
		/// <summary>
		/// Procesa el objeto IEmpleados via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{			
			IEmpleados IEntity = Entity as IEmpleados;			
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdEmpleado", IEntity.IdEmpleado);
			_Connection.AddParam("@IdDepartamento", IEntity.IdDepartamento);
			_Connection.AddParam("@IdGrado", IEntity.IdGrado);
			_Connection.AddParam("@Nombre", IEntity.Nombre);
			_Connection.AddParam("@ApellidoPaterno", IEntity.ApellidoPaterno);
			_Connection.AddParam("@ApellidoMaterno", IEntity.ApellidoMaterno);
			_Connection.AddParam("@Matricula", IEntity.Matricula);
			_Connection.AddParam("@Email", IEntity.Email);
			_Connection.AddParam("@Telefono", IEntity.Telefono);
			_Connection.AddParam("@Celular", IEntity.Celular);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdEmpleado = Convert.ToInt32(_Connection.GetParam("@IdEmpleado").ToString());			
		}		
		public virtual IPagina<Entity> GetAllTecnicos<Entity>(int Pagina) where Entity : IEntidad, new()
		{			
			IPagina<Entity> pagina = new DPagina<Entity>(Pagina, 0);
			try
			{
				IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				OpenConexion();
				_Connection.Query = SP_GET_ALL_TECNICOS;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdPagina", Pagina);
				_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor));
				FillEntidades(pagina, _Connection.GetReader());
				if(pagina.RegistrosPP <= 0) pagina.RegistrosPP = (pagina != null) ? pagina.Contenido.Count() : 0;
			}
			finally
			{
				CloseConnection();
			}
			return pagina;
		}
		protected override string BuildCampos(string[] camposOmitir, string[] camposSelect, string separador)
		{
			string all= base.BuildCampos(camposOmitir, camposSelect, separador);
			all = all + separador + " dbo.fnGradosByEmpleado(Empleados.IdEmpleado) as Grado";
			return all ;
		}
	}//  DEmpleados ends.
}// LibDatos.Entidades ends.
