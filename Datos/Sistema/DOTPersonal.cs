using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:14 p. m.*/
namespace Datos
{
    
    public partial class   DOTPersonal:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_OTPersonal]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new COTPersonal();
	}		
        public DOTPersonal():base()
	{
	  Inicializa();
	}
	public DOTPersonal(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "OTPersonal";
	} 
	/// <summary>
        /// Procesa el objeto IOTPersonal via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IOTPersonal IEntity = Entity as IOTPersonal;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdOTPersonal",IEntity.IdOTPersonal); 
				 _Connection.AddParam("@IdOrdenTrabajo", IEntity.IdOrdenTrabajo);
				 _Connection.AddParam("@IdTecnico", IEntity.IdTecnico);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdOTPersonal = Convert.ToInt32( _Connection.GetParam("@IdOTPersonal").ToString());
 			
	}	
    }//  DOTPersonal ends.
}// LibDatos.Entidades ends.
