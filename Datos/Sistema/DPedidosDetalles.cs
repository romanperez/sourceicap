using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:14 p. m.*/
namespace Datos
{
	public partial class DPedidosDetalles : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_PedidosDetalles]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CPedidosDetalles();
		}
		public DPedidosDetalles()
			: base()
		{
			Inicializa();
		}
		public DPedidosDetalles(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "PedidosDetalles";
		}
		/// <summary>
		/// Procesa el objeto IPedidosDetalles via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IPedidosDetalles IEntity = Entity as IPedidosDetalles;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdPedidoDetalle", IEntity.IdPedidoDetalle);
			_Connection.AddParam("@IdPedido", IEntity.IdPedido);
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
			_Connection.AddParam("@Cantidad", IEntity.Cantidad);
			_Connection.AddParam("@CantidadEnviada", IEntity.CantidadEnviada);
			_Connection.AddParam("@CantidadRecibida", IEntity.CantidadRecibida);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdPedidoDetalle = Convert.ToInt32(_Connection.GetParam("@IdPedidoDetalle").ToString());
		}
	}//  DPedidosDetalles ends.
}// LibDatos.Entidades ends.