using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 21/02/2017 09:48:13 a. m.*/
namespace Datos
{
    
    public partial class   DContratosDetallesInsumos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_ContratosDetallesInsumos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CContratosDetallesInsumos();
	}		
        public DContratosDetallesInsumos():base()
	{
	  Inicializa();
	}
	public DContratosDetallesInsumos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "ContratosDetallesInsumos";
	} 
	/// <summary>
        /// Procesa el objeto IContratosDetallesInsumos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IContratosDetallesInsumos IEntity = Entity as IContratosDetallesInsumos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdInsumoContrato",IEntity.IdInsumoContrato); 
				 _Connection.AddParam("@IdContrato", IEntity.IdContrato);
				 _Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
				 _Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
				 _Connection.AddParam("@Cantidad", IEntity.Cantidad);
				 _Connection.AddParam("@Costo", IEntity.Costo);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdInsumoContrato = Convert.ToInt32( _Connection.GetParam("@IdInsumoContrato").ToString());
 			
	}	
    }//  DContratosDetallesInsumos ends.
}// LibDatos.Entidades ends.
