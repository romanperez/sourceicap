using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:08 p. m.*/
namespace Datos
{
	public partial class DActividades : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Actividades]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CActividades();
		}
		public DActividades()
			: base()
		{
			Inicializa();
		}
		public DActividades(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Actividades";
		}
		/// <summary>
		/// Procesa el objeto IActividades via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IActividades IEntity = Entity as IActividades;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdActividad", IEntity.IdActividad);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdEspecialidad", IEntity.IdEspecialidad);
			_Connection.AddParam("@IdConceptoClasificacion", IEntity.IdConceptoClasificacion);
			_Connection.AddParam("@Actividad", IEntity.Actividad);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@ProcedimientoSeguridad", IEntity.ProcedimientoSeguridad);
			_Connection.AddParam("@AnalisisRiesgo", IEntity.AnalisisRiesgo);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdActividad = Convert.ToInt32(_Connection.GetParam("@IdActividad").ToString());
		}
	}//  DActividades ends.
}// LibDatos.Entidades ends.