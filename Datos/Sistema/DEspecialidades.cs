using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:11 p. m.*/
namespace Datos
{

	public partial class DEspecialidades : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Especialidades]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CEspecialidades();
		}
		public DEspecialidades()
			: base()
		{
			Inicializa();
		}
		public DEspecialidades(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Especialidades";
		}
		/// <summary>
		/// Procesa el objeto IEspecialidades via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IEspecialidades IEntity = Entity as IEspecialidades;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdEspecialidad", IEntity.IdEspecialidad);
			_Connection.AddParam("@IdGrado", IEntity.IdGrado);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@Codigo", IEntity.Codigo);
			_Connection.AddParam("@Nombre", IEntity.Nombre);
			_Connection.AddParam("@Estatus", IEntity.Estatus);

			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdEspecialidad = Convert.ToInt32(_Connection.GetParam("@IdEspecialidad").ToString());

		}
	}//  DEspecialidades ends.
}// LibDatos.Entidades ends.
