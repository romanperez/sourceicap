using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{

	public partial class DMonedas : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Monedas]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CMonedas();
		}
		public DMonedas()
			: base()
		{
			Inicializa();
		}
		public DMonedas(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Monedas";
		}
		/// <summary>
		/// Procesa el objeto IMonedas via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IMonedas IEntity = Entity as IMonedas;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdMoneda", IEntity.IdMoneda);			 
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@Moneda", IEntity.Moneda);
			_Connection.AddParam("@Codigo", IEntity.Codigo);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdMoneda = Convert.ToInt32(_Connection.GetParam("@IdMoneda").ToString());
		}
	}//  DMonedas ends.
}// LibDatos.Entidades ends.
