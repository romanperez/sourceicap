using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 16/11/2016 05:55:02 p. m.*/
namespace Datos
{
    ///<summary>
	///Catalogo de usuarios para el inicio de sesion.
	///</summary>
    public partial class   DUsuarios:DBase
    {
		   protected const string SP_SAVE = "[dbo].[prc_Usuarios]";
			protected const string SP_GET_USER = "[dbo].[prc_get_user_credentials]";
			protected IUsuarios _userValidar;
			public override Entidades.IEntidad BuildEntity()
			{
				return new CUsuarios();
			}		
			public DUsuarios():base()
			{
				Inicializa();
			}
			public DUsuarios(IConexion cnn): base(cnn)
			{
				Inicializa();
			}
			protected void Inicializa()
			{
				_Tabla = "Usuarios";
			} 
			/// <summary>
         /// Procesa el objeto IUsuarios via stored procedure.
         /// </summary>
         /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
         /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
			protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
			{
				IUsuarios IEntity = Entity as IUsuarios;
				_Connection.Query = SP_SAVE;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdUsuario",IEntity.IdUsuario); 
				_Connection.AddParam("@IdPerfil", IEntity.IdPerfil);
				_Connection.AddParam("@Usuario", IEntity.Usuario);
				_Connection.AddParam("@Clave", IEntity.Clave);
				_Connection.AddParam("@FechaRegistro", IEntity.FechaRegistro);
				_Connection.AddParam("@IdEmpleado", IEntity.IdEmpleado);
				_Connection.AddParam("@Delete_Entity", this.Delete);
				_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
				_Connection.NonQuery();
				IEntity.IdUsuario = Convert.ToInt32( _Connection.GetParam("@IdUsuario").ToString()); 			
			}
			
			protected void BuildSpGetUserByLoggin()
			{
				_Connection.Query = SP_GET_USER;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@Usuario", _userValidar.Usuario);
				_Connection.AddParam("@Clave", _userValidar.Clave);
			}
			public virtual IUsuarios GetUserByLoggin(IUsuarios Loggin) 
			{
				int data = 0;
				if(Loggin == null) return null;
				IUsuarios Usuario = null;
				try
				{					
					IEmpleados Empleado = null;
					IPerfiles Perfil = null;
					IDepartamentos Departamento = null;
					IUnidades unidad = null;
					IEmpresas empresa = null;
					OpenConexion();
					_userValidar = Loggin;
					BuildSpGetUserByLoggin();					
					IDataReader drDatos = _Connection.GetReader();
					while(_Connection.HasRows(drDatos) || data<=5)
					{
						while(drDatos.Read())
						{
							switch(data)
							{
								case 0:
									Usuario = new CUsuarios();
									Usuario = GetEntity(drDatos, Usuario) as IUsuarios;
									Usuario.Clave = String.Empty;
									break;
								case 1:
									Empleado = new CEmpleados();
									Empleado = GetEntity(drDatos, Empleado) as IEmpleados;
									break;
								case 2:
									Perfil = new CPerfiles();
									Perfil = GetEntity(drDatos, Perfil) as IPerfiles;
									break;
								case 3:
									Departamento = new CDepartamentos();
									Departamento = GetEntity(drDatos, Departamento) as IDepartamentos;
									break;
								case 4:
									unidad = new CUnidades();
									unidad = GetEntity(drDatos, unidad) as IUnidades;
									break;
								case 5:
									empresa = new CEmpresas();
									empresa = GetEntity(drDatos, empresa) as IEmpresas;
									break;
							}
						}
						data++;
						drDatos.NextResult();
					}
					if(Usuario != null)
					{
						Usuario.Data = new CUsuariosData();
						Usuario.Data.Departamento = Departamento as CDepartamentos;
						Usuario.Data.Empleado = Empleado as CEmpleados;
						Usuario.Data.Empresa = empresa as CEmpresas;
						Usuario.Data.Perfil = Perfil as CPerfiles;
						Usuario.Data.Unidad = unidad as CUnidades;
					}
				}
				finally
				{
					CloseConnection();
				}
				return Usuario;
					 
			}
			
    }//  DUsuarios ends.
}// LibDatos.Entidades ends.
