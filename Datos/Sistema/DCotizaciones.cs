using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:10 p. m.*/
namespace Datos
{
	public partial class DCotizaciones : DBase
	{
		public const string VW_COTIZACIONES = "vwCotizaciones";
		/// <summary>
		/// Decimales para el redondeo.
		/// </summary>
		public const int NUM_DECIMALES = 2;
		public enum eGetIvaTipo
		{
			eIva =1,eTipoCambio
		}
		private const string SP_SAVE = "[dbo].[prc_Cotizaciones]";
		private const string SP_INDEX_COTIZACIONES = "[dbo].[prc_index_cotizaciones]";
		private const string SP_GET_IVA_TIPO_CAMBIO = "[dbo].[GetIvaTipoCambio]";
		private const string SP_INSUMOS_COTIZACION = "[dbo].[sp_insumos_cotizacion]";
		private const string SP_COTIZACION_DETALLE = "[dbo].[prc_cotizacion_detalles]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CCotizaciones();
		}
		public DCotizaciones()
			: base()
		{
			Inicializa();
		}
		public DCotizaciones(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Cotizaciones";
		}
		/// <summary>
		/// Procesa el objeto ICotizaciones via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{			
			ICotizaciones IEntity = Entity as ICotizaciones;
			
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdCotizacion", IEntity.IdCotizacion);
			_Connection.AddParam("@IdUsuario", IEntity.IdUsuario);
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@IdMoneda", IEntity.IdMoneda);
			_Connection.AddParam("@Cotizacion", IEntity.Cotizacion); 
			_Connection.AddParam("@Total", IEntity.Total);
			_Connection.AddParam("@PorcentajeDescuento", IEntity.PorcentajeDescuento);
			_Connection.AddParam("@DescuentoMonto", IEntity.DescuentoMonto);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdCliente", IEntity.IdCliente);
			_Connection.AddParam("@IdIva", IEntity.IdIva);
			_Connection.AddParam("@IdTipoCambio", IEntity.IdTipoCambio);
			_Connection.AddParam("@Fecha", IEntity.Fecha);
			_Connection.AddParam("@Exito", IEntity.Exito);			
			_Connection.AddParam("@SubTotal", IEntity.SubTotal);
			_Connection.AddParam("@Utilidad", IEntity.Utilidad);
			_Connection.AddParam("@Solicitud", IEntity.Solicitud);
			_Connection.AddParam("@ProyectoInicial", IEntity.ProyectoInicial);
			_Connection.AddParam("@DescripcionTitulo", IEntity.DescripcionTitulo);
			_Connection.AddParam("@DescripcionCompleta", IEntity.DescripcionCompleta);
			_Connection.AddParam("@DepartamentoAtencion", IEntity.DepartamentoAtencion);
			_Connection.AddParam("@FechaEntrega", IEntity.FechaEntrega); 
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdCotizacion = Convert.ToInt32(_Connection.GetParam("@IdCotizacion").ToString());			
			SaveDetail(Entity as ICotizaciones);			
			SaveDetailInsumos(Entity as ICotizaciones);
			if(this.Delete != 1)
			{
				SavePersonalCliente(IEntity);
			}
		}
		protected virtual void SavePersonalCliente(ICotizaciones cotizacion)
		{
			DCotizacionesPersonalCliente pc = new DCotizacionesPersonalCliente();
			if(cotizacion.PersonalClienteAtencion != null && cotizacion.PersonalClienteAtencion.Count>0)
			{

				cotizacion.PersonalClienteAtencion.ForEach(x =>
				{
					if(x != null)
					{

						x.IdCotizacion = cotizacion.IdCotizacion;
						if(x.IdCotizacionPersonalCliente < 0)
						{
							x.IdCotizacionPersonalCliente = x.IdCotizacionPersonalCliente * -1;
							pc.Delete = 1;
						}
						pc.Save(x);
					}
				});
			}
			if(cotizacion.PersonalClienteContacto != null && cotizacion.PersonalClienteContacto.Any())
			{
				cotizacion.PersonalClienteContacto.ForEach(x =>
				{
					if(x != null)
					{
						x.IdCotizacion = cotizacion.IdCotizacion;
						if(x.IdCotizacionPersonalCliente < 0)
						{
							x.IdCotizacionPersonalCliente = x.IdCotizacionPersonalCliente * -1;
							pc.Delete = 1;
						}
						pc.Save(x);
					}
				});
			}
		}
		protected virtual void SaveDetailInsumos(ICotizaciones Entity)
		{
			try
			{
				string error = String.Empty;
				OpenConexion();
				_Connection.Query = SP_INSUMOS_COTIZACION;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdCotizacion", Entity.IdCotizacion);				
				_Connection.NonQuery();				
			}
			finally
			{
				CloseConnection();
			}
		}
		protected virtual void SaveDetail(ICotizaciones Entity)
		{
			bool eliminar = false;
			if(Entity == null) return;
			if(Entity.Conceptos != null && Entity.Conceptos.Count() > 0)
			{
				foreach(ICotizacionesDetalles detalle in Entity.Conceptos)
				{
					DCotizacionesDetalles _ddetail = new DCotizacionesDetalles(_Connection);
					detalle.IdCotizacion = Entity.IdCotizacion;
					if(detalle.IdCotizacionDetalle < 0)
					{
						detalle.IdCotizacionDetalle = detalle.IdCotizacionDetalle * -1;
						_ddetail.Delete = 1;
						eliminar = true;
					}
					_ddetail.Save(detalle);
					if(eliminar)
					{
						detalle.IdCotizacionDetalle = detalle.IdCotizacionDetalle * -1;
					}
				}
			}
		}
		/*protected virtual void SaveDetailInsumos(ICotizaciones Entity)
		{
			if(Entity == null) return;
			int[] iConceptos = Entity.Conceptos.Where(c => c.IdCotizacionDetalle > 0).Select(x => x.IdConcepto).ToArray();
			if(Entity.Insumos != null && Entity.Insumos.Count() > 0)
			{				
				foreach(ICotizacionesDetallesInsumos detalle in Entity.Insumos.Where(x=>iConceptos.Contains(x.IdConcepto)))
				{
					DCotizacionesDetallesInsumos _ddetail = new DCotizacionesDetallesInsumos(_Connection);
					detalle.IdCotizacion = Entity.IdCotizacion;
					detalle.Cantidad=detalle.Cantidad*
					_ddetail.Save(detalle);
				}
			}
		}*/
		//public virtual int GetValorIvaTipoCambio(eGetIvaTipo tipo, ICotizaciones cotizacion,int?IdMonedaOrigen,int?IdMonedaDestino)
		//{
		//	int Resultado = 0;
		//	try
		//	{
		//		OpenConexion();
		//		_Connection.Query = SP_GET_IVA_TIPO_CAMBIO;
		//		_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
		//		_Connection.AddParam("@Tipo", (int)tipo);
		//		_Connection.AddParam("@IdEmpresa", cotizacion.IdEmpresa);
		//		_Connection.AddParam("@Fecha", cotizacion.Fecha);
		//		if(IdMonedaOrigen != null)
		//		{
		//			_Connection.AddParam("@IdMonedaOrigen", IdMonedaOrigen);
		//		}
		//		if(IdMonedaDestino != null)
		//		{
		//			_Connection.AddParam("@IdMonedaDestino", IdMonedaDestino);
		//		}				
		//		_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdResultado");
		//		_Connection.AddParam(SqlDbType.Float, ParameterDirection.InputOutput, "@Valor");
		//		_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@Resultado");
		//		_Connection.NonQuery();
		//		Resultado = Convert.ToInt32(_Connection.GetParam("@Resultado").ToString());
		//		if(Resultado == 1)
		//		{
		//			switch(tipo)
		//			{
		//				case eGetIvaTipo.eIva:
		//					cotizacion.IdIva = Convert.ToInt32(_Connection.GetParam("@IdResultado").ToString());
		//					cotizacion.Iva = Convert.ToDouble(_Connection.GetParam("@Valor").ToString());
		//					break;
		//				case eGetIvaTipo.eTipoCambio:
		//					cotizacion.IdTipoCambio = Convert.ToInt32(_Connection.GetParam("@IdResultado").ToString());
		//					cotizacion.Valor = Convert.ToDouble(_Connection.GetParam("@Valor").ToString());
		//					break;
		//			}
		//		}
		//	}
		//	finally
		//	{
		//		CloseConnection();
		//	}
		//	return Resultado;
		//}
		protected override void FillEntidades<Entity>(IPagina<Entity> pagina, IDataReader drDatos)
		{
			List<Entity> lstEntities = new List<Entity>();
			try
			{
				while(drDatos.Read())
				{
					Entity entidad = (Entity)BuildEntity();
					if(pagina.Paginas <= 0)
					{
						pagina.Paginas = Convert.ToInt32(drDatos[DPaginaConst.CAMPO_PAGINAS].ToString());
					}
					Entity EntityAux = (Entity)GetEntity(drDatos, entidad);
					((ICotizaciones)EntityAux).Usuario = GetEntity(drDatos, new CUsuarios()) as CUsuarios;
					lstEntities.Add(EntityAux);
				}
				pagina.Contenido = lstEntities;
			}
			finally
			{
				if(drDatos != null)
				{
					if(!drDatos.IsClosed) drDatos.Close();
				}
			}			 
		}
		public virtual IPagina<Entity> GetIndexCotizaciones<Entity>(int Pagina) where Entity : IEntidad, new()
		{
			IPagina<Entity> pagina = new DPagina<Entity>(Pagina, 0);
			try
			{
				IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				IPropsWhere unidad = _Where.Where(a => a.NombreCampo == "Unidades.IdUnidad").FirstOrDefault();
				IPropsWhere cotizacion = _Where.Where(a => a.NombreCampo == "Cotizaciones.IdCotizacion").FirstOrDefault();
				OpenConexion();
				_Connection.Query = SP_INDEX_COTIZACIONES;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdPagina", Pagina);
				if(empresa != null)
				{
					_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor));
				}
				if(unidad != null)
				{
					_Connection.AddParam("@IdUnidad", Convert.ToInt32(unidad.Valor));
				}				
				if(cotizacion != null)
				{
					_Connection.AddParam("@IdCotizacion", Convert.ToInt32(cotizacion.Valor));
				}
				FillEntidades(pagina, _Connection.GetReader());
				if(pagina.RegistrosPP <= 0) pagina.RegistrosPP = (pagina != null) ? pagina.Contenido.Count() : 0;
			}
			finally
			{
				CloseConnection();
			}
			return pagina;
		}
		public virtual ICotizaciones GetDetalleCotizacion(ICotizaciones cotizacion)
		{			
			try
			{				
				List<CCotizacionesDetalles> conceptos = new List<CCotizacionesDetalles>();
				List<CCotizacionesDetallesInsumos> insumos = new List<CCotizacionesDetallesInsumos>();
				int data = 0;
				OpenConexion();
				_Connection.Query = SP_COTIZACION_DETALLE;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdCotizacion", cotizacion.IdCotizacion);								
				IDataReader drDatos = _Connection.GetReader();
				while(_Connection.HasRows(drDatos) || data < 2)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								conceptos.Add(GetEntity(drDatos, new CCotizacionesDetalles()) as CCotizacionesDetalles);								
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CCotizacionesDetallesInsumos()) as CCotizacionesDetallesInsumos);	
								break;							
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 2) ends.
				if(conceptos != null && conceptos.Count() > 0)
				{
					if(insumos != null && insumos.Count() > 0)
					{
						conceptos.ForEach(c =>
						{
							IEnumerable<CCotizacionesDetallesInsumos> res = insumos.Where(i => i.IdConcepto == c.IdConcepto);
							if(res != null && res.Count() > 0)
							{
								c.Insumos = res.ToList();
							}
						});
					}
				}//if ends.
				cotizacion.Conceptos = conceptos;
			}//try ends
			finally
			{
				CloseConnection();
			}
			return cotizacion;
		}
	}//  DCotizaciones ends.
}// LibDatos.Entidades ends.
