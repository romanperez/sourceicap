using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 26/01/2017 04:27:10 p. m.*/
namespace Datos
{
    
    public partial class   DFamilias:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_Familias]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CFamilias();
	}		
        public DFamilias():base()
	{
	  Inicializa();
	}
	public DFamilias(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "Familias";
	} 
	/// <summary>
        /// Procesa el objeto IFamilias via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IFamilias IEntity = Entity as IFamilias;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdFamilia",IEntity.IdFamilia); 
				 _Connection.AddParam("@Familia", IEntity.Familia);
				 _Connection.AddParam("@Descripcion", IEntity.Descripcion);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);
				 _Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdFamilia = Convert.ToInt32( _Connection.GetParam("@IdFamilia").ToString());
 			
	}	
    }//  DFamilias ends.
}// LibDatos.Entidades ends.
