using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:09 p. m.*/
namespace Datos
{

	public partial class DConceptos : DBase
	{
		protected const string SP_SAVE = "[dbo].[prc_Conceptos]";
		protected const string SP_PRECIO_CONCEPTO = "[dbo].[sp_obtiene_precio_concepto]";
		//protected const string PRC_ACTUALIZA_COSTOS = "[dbo].[prc_actualiza_costo_insumos_concepto]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CConceptos();
		}
		public DConceptos()
			: base()
		{
			Inicializa();
		}
		public DConceptos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Conceptos";
		}
		/// <summary>
		/// Procesa el objeto IConceptos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IConceptos IEntity = Entity as IConceptos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdCapitulo", IEntity.IdCapitulo);
			_Connection.AddParam("@IdUnidadMedida", IEntity.IdUnidadMedida);
			_Connection.AddParam("@IdConceptoClasificacion", IEntity.IdConceptoClasificacion);
			_Connection.AddParam("@Precio", IEntity.Precio);
			_Connection.AddParam("@Concepto", IEntity.Concepto);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@Fecha", IEntity.Fecha);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdConcepto = Convert.ToInt32(_Connection.GetParam("@IdConcepto").ToString());
			if(this.Delete != 1)
			{
				SaveDetail(IEntity);
				/*string error = String.Empty;
				OpenConexion();
				_Connection.Query = SP_PRECIO_CONCEPTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
				_Connection.AddParam(SqlDbType.VarChar,-1,ParameterDirection.Output, "@SinTipoCambio");				
				_Connection.NonQuery();
				error = _Connection.GetParam("@SinTipoCambio").ToString();
				if(!String.IsNullOrEmpty(error))
				{
					IEntity.IdConcepto = -1;
					IEntity.Concepto = error;
				}*/
			}		
		}
		public virtual void ActualizaPrecio(IConceptos concepto)
		{
			if(concepto == null || concepto.Precio <= 0) return;
			OpenConexion();
			_Connection.Query = SP_PRECIO_CONCEPTO;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam("@IdConcepto", concepto.IdConcepto);
			_Connection.AddParam("@Precio", concepto.Precio);
			//_Connection.AddParam(SqlDbType.VarChar, -1, ParameterDirection.Output, "@SinTipoCambio");
			_Connection.NonQuery();
		}
		protected virtual void SaveDetail(IEntidad Entity)
		{
			if(Entity == null) return;
			IConceptos concepto = Entity as IConceptos;
			if(concepto.Insumos != null && concepto.Insumos.Count() > 0)
			{
				foreach(IConceptosInsumos detail in concepto.Insumos)
				{
					DConceptosInsumos _ddetail = new DConceptosInsumos(_Connection);
					detail.IdConcepto = concepto.IdConcepto;
					if(detail.IdConceptoInsumo < 0)
					{
						detail.IdConceptoInsumo = detail.IdConceptoInsumo * -1;
						_ddetail.Delete = 1;
					}					
					_ddetail.Save(detail);
				}
			}
		}
	}//  DConceptos ends.
}// LibDatos.Entidades ends.
