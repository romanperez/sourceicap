using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:15 p. m.*/
namespace Datos
{

	public partial class DProyectos : DBase
	{
		protected const string SP_SAVE = "[dbo].[prc_Proyectos]";
		protected const string SP_GET_ALL_PROYECTOS = "[dbo].[prc_get_proyectos]";
		protected const string SP_GENERA_SALIDA_PROYECTO = "[dbo].[prc_genera_pedidoSalida_ByProyecto]";
		protected const string SP_INSUMOS_BY_PROYECTO = "[dbo].[prc_InsumosByProyecto]";
		protected const string SP_INSUMOS_BY_PROYECTO_FECHA = "[dbo].[prc_InsumosByProyecto_fecha]";
		protected const string SP_IMPORTES_PROYECTO = "[dbo].[prc_ImportesProyecto]";
        protected const string SP_CONCEPTOSPORCONTRATO = "[dbo].[spReporteConceptosPorContrato]";
		protected const string SP_CONCEPTOSPORCONTRATO_INSTALADO = "[dbo].[spReporteConceptosPorContratoInstalado]";
        protected const string SP_CONCEPTOSPORCONTRATO_SOLICITADOINSTALADO = "[dbo].[spReporteConceptosPorContratosSolicitadoInstalado]";
        protected const string SP_CONCEPTOSPORCONTRATO_SOLICITADOESTADO = "[dbo].[spReporteConceptosPorContratoSolicitadoEstado]";
        protected const string SP_CONCEPTOSPORCONTRATO_SOLICITADOAREA = "[dbo].[spReporteConceptosPorContratoArea]";
        protected const string SP_CONCEPTOSPORCONTRATO_INSTALADOAREA = "[dbo].[spReporteConceptosPorUbicacionInstalado]";
        protected const string SP_CONCEPTOSPORCONTRATO_SOLICITADOESTADOAREA = "[dbo].[spReporteConceptosPorContratoAreaSolicitadoEstado]";
        protected const string SP_CONCEPTOS_SOLICITADOINSTALADOAREA = "[dbo].[spReporteConceptosSolicitadoInstaladoArea]";
		  protected const string SP_ESTIMACIONES_CONTRATO = "[dbo].[spReporteEstimacionesPorContrato]";

		public override Entidades.IEntidad BuildEntity()
		{
			return new CProyectos();
		}
		public DProyectos()
			: base()
		{
			Inicializa();
		}
		public DProyectos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Proyectos";
		}
		/// <summary>
		/// Procesa el objeto IProyectos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IProyectos IEntity = Entity as IProyectos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdProyecto", IEntity.IdProyecto);
			_Connection.AddParam("@IdCliente", IEntity.IdCliente);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@IdCotizacion", IEntity.IdCotizacion);
			_Connection.AddParam("@IdEstatus", IEntity.IdEstatus);
			_Connection.AddParam("@Proyecto", IEntity.Proyecto);
			_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@Ubicacion", IEntity.Ubicacion);
			_Connection.AddParam("@FechaInicial", IEntity.FechaInicial);
			_Connection.AddParam("@FechaFinal", IEntity.FechaFinal);			
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdProyecto = Convert.ToInt32(_Connection.GetParam("@IdProyecto").ToString());	
			if(this.Delete != 1)
			{
				SaveProyectosContratos(IEntity);			
			}		   
		}
		public void SaveProyectosContratos(IProyectos IEntity)
		{
			if(IEntity.Contratos == null || IEntity.Contratos.Count() <= 0)
				return;
			foreach(IProyectoContratos contrato in IEntity.Contratos)
				{
					DProyectoContratos _ddetail = new DProyectoContratos(_Connection);
					contrato.IdProyecto = IEntity.IdProyecto;
					if(contrato.IdProyectoContrato < 0)
					{
						contrato.IdProyectoContrato = contrato.IdProyectoContrato * -1;
						_ddetail.Delete = 1;						
					}
					_ddetail.Save(contrato);					
				}			
		}
		protected IEntidad GetPlantillaCostos()
		{
			return new CProyectosCostos();
		}
		public virtual void MainCostosProyectos(IEnumerable<IProyectos> Proyectos)
		{
			try
			{
				OpenConexion();
				SetCostosProyectos(Proyectos);
			}
			finally
			{
				CloseConnection();
			}
		}
		protected virtual void SetCostosProyectos(IEnumerable<IProyectos> Proyectos)
		{
			Proyectos.ToList().ForEach(x =>
			{
				_Connection.Query = SP_IMPORTES_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", x.IdProyecto);
				IEnumerable<IEntidad> Costos = FillEntidades(_Connection.GetReader(), GetPlantillaCostos);
				if(Costos != null && Costos.Count() > 0)
				{
					x.CostosGlobales = Costos.Select(x2 => x2 as CProyectosCostos).ToList();
				}
			});			
		}
		public virtual IEnumerable<IEntidad> SetCostosGlobal()// where Entity : IEntidad, new()
		{
			string swhere = String.Empty;
			IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
			IPropsWhere unidad = _Where.Where(a => a.NombreCampo == "Unidades.IdUnidad").FirstOrDefault();
			IPropsWhere where = _Where.Where(a => a.NombreCampo != "Empresas.IdEmpresa" && a.NombreCampo != "Unidades.IdUnidad").FirstOrDefault();
			if(where != null)
			{
				swhere = ToWhere(where);
			}
			_Connection.Query = SP_GET_ALL_PROYECTOS;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);			
			_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor));
			if(unidad != null)
			{
				_Connection.AddParam("@IdUnidad", Convert.ToInt32(unidad.Valor));
			}
			if(!String.IsNullOrEmpty(swhere))
			{
				_Connection.AddParam("@Where", swhere);
			}			
			IEnumerable<IEntidad> res= FillEntidades(_Connection.GetReader());
			if(res != null && res.Count() > 0)
			{
				res.ToList().ForEach(x =>
				{
					((IProyectos)x).EstatusProyecto = CProyectos.PROYECTO_RSMN_COSTOS;
				});
				return res;//.Select(x=> ((Entity)x));
			}
			return null;
		}
		public virtual IPagina<Entity> GetProyectos<Entity>(int Pagina,bool permiso) where Entity : IEntidad, new()
		{			
			IPagina<Entity> pagina = new DPagina<Entity>(Pagina, 0);
			try
			{
				string swhere = String.Empty;
				IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				IPropsWhere unidad = _Where.Where(a => a.NombreCampo == "Unidades.IdUnidad").FirstOrDefault();
				IPropsWhere where = _Where.Where(a => a.NombreCampo != "Empresas.IdEmpresa" && a.NombreCampo != "Unidades.IdUnidad").FirstOrDefault();
				if(where != null)
				{
					swhere = ToWhere(where);
				}
				OpenConexion();
				_Connection.Query = SP_GET_ALL_PROYECTOS;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				if(Pagina>=0)
				{
					_Connection.AddParam("@IdPagina", Pagina);
				}				
				_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor));
				if(unidad!=null)
				{
					_Connection.AddParam("@IdUnidad", Convert.ToInt32(unidad.Valor));
				}				
				if(!String.IsNullOrEmpty(swhere))
				{
					_Connection.AddParam("@Where", swhere);
				}				
				FillEntidades(pagina, _Connection.GetReader());
				if(pagina.RegistrosPP <= 0) pagina.RegistrosPP = (pagina != null) ? pagina.Contenido.Count() : 0;
				if(pagina != null && pagina.Contenido != null && pagina.Contenido.Count() > 0 && permiso)
				{
					SetCostosProyectos(pagina.Contenido as IEnumerable<IProyectos>);
					if(Pagina > 0)
					{
						IEnumerable<IEntidad> costosGlobales = SetCostosGlobal();
						if(costosGlobales != null && costosGlobales.Count() > 0)
						{
							SetCostosProyectos(costosGlobales.Select(x => x as IProyectos));
							costosGlobales.ToList().ForEach(x =>
							{
								((List<Entity>)pagina.Contenido).Add((Entity)x);
							});
						}
					}
				}
			}
			finally
			{
				CloseConnection();
			}
			return pagina;
		}
		public void SolicitarInsumosByProyecto(IProyectos proyecto, int IdUsuario)
		{
			try
			{
				if(proyecto == null || proyecto.IdProyecto <= 0 || IdUsuario <= 0) return;
				OpenConexion();
				_Connection.Query = SP_GENERA_SALIDA_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", proyecto.IdProyecto);
				_Connection.AddParam("@IdUsuario", IdUsuario);
				_Connection.NonQuery();				
			}
			finally
			{
				CloseConnection();
			}
		}
		public IEnumerable<IvwContratos2> InsumosByProyecto(IProyectos proyecto)
		{
			try
			{
				int data = 0;
				if(proyecto == null || proyecto.IdProyecto <= 0) return null;
				OpenConexion();
				_Connection.Query = SP_INSUMOS_BY_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", proyecto.IdProyecto);
				IDataReader drDatos = _Connection.GetReader();
				List<IvwContratos2> contratos = new List<IvwContratos2>();
				List<IContratosDetalles2> conceptos = new List<IContratosDetalles2>();
				List<IContratosDetallesInsumos2> insumos = new List<IContratosDetallesInsumos2>();

				while(_Connection.HasRows(drDatos) || data < 3)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								contratos.Add(GetEntity(drDatos, new CvwContratos2()) as IvwContratos2);
								break;
							case 1:
								conceptos.Add(GetEntity(drDatos, new CContratosDetalles2()) as IContratosDetalles2);
								break;
							case 2:
								insumos.Add(GetEntity(drDatos, new CContratosDetallesInsumos2()) as IContratosDetallesInsumos2);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 3) ends.
				if(conceptos != null && insumos != null)
				{
					conceptos.ForEach(c =>
					{
						c.Insumos = insumos.Where(i => i.IdContratosDetalles == c.IdContratosDetalles).Select(x=> x as CContratosDetallesInsumos2).ToList();
					});
				}
				if(contratos != null && conceptos != null)
				{
					contratos.ForEach(co =>
					{
						co.Conceptos = conceptos.Where(c => c.IdContrato == co.IdContrato).Select(x => x as CContratosDetalles2).ToList();
					});
				}				
				return contratos;				
			}
			finally
			{
				CloseConnection();
			}
		}
		public IEnumerable<IContratosDetalles2> InsumosByProyectoFecha(int IdProyecto, DateTime FechaInicial, DateTime FechaFinal)
		{
			try
			{
				int data = 0;
				if( IdProyecto <= 0) return null;
				OpenConexion();
				_Connection.Query = SP_INSUMOS_BY_PROYECTO_FECHA;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto",IdProyecto);
				_Connection.AddParam("@FechaInicial", FechaInicial);
				_Connection.AddParam("@FechaFinal", FechaFinal);
				IDataReader drDatos = _Connection.GetReader();				
				List<IContratosDetalles2> conceptos = new List<IContratosDetalles2>();
				List<IContratosDetallesInsumos2> insumos = new List<IContratosDetallesInsumos2>();

				while(_Connection.HasRows(drDatos) || data < 3)
				{
					while(drDatos.Read())
					{
						switch(data)
						{							
							case 0:
								conceptos.Add(GetEntity(drDatos, new CContratosDetalles2()) as IContratosDetalles2);
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CContratosDetallesInsumos2()) as IContratosDetallesInsumos2);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 3) ends.
				if(conceptos != null && insumos != null)
				{
					conceptos.ForEach(c =>
					{
						c.Insumos = insumos.Where(i => i.IdContratosDetalles == c.IdContratosDetalles).Select(x => x as CContratosDetallesInsumos2).ToList();
					});
				}
				return conceptos;
			}
			finally
			{
				CloseConnection();
			}
		}
		protected IContratosDetalles2 ToContratoDetalles(IDataReader drDatos)
		{
			IContratosDetalles2 iContrato = new CContratosDetalles2();
			iContrato.Contrato = drDatos.GetValue(drDatos.GetOrdinal("Contrato")).ToString();
			iContrato.Concepto = drDatos.GetValue(drDatos.GetOrdinal("Concepto")).ToString();
			iContrato.Descripcion = drDatos.GetValue(drDatos.GetOrdinal("Descripcion")).ToString();
			iContrato.UnidadMedida = drDatos.GetValue(drDatos.GetOrdinal("UnidadMedida")).ToString();
			iContrato.Cantidad = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Base")));
			iContrato.CantidadProgramada = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Adicional")));
			return iContrato;
		}
        public IEnumerable<IContratosDetalles2> ConceptosPorContratos(int IdProyecto, string contratos)
        {
            try
            {
                if (IdProyecto <= 0) return null;
                OpenConexion();
                _Connection.Query = SP_CONCEPTOSPORCONTRATO;
                _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                _Connection.AddParam("@ProyectoId", IdProyecto);
                _Connection.AddParam("@Contratos", contratos);
                IDataReader drDatos = _Connection.GetReader();
                List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
              while(drDatos.Read())
				  {
					  listaResultado.Add(ToContratoDetalles(drDatos));
              }
              return listaResultado;
            }
            finally
            {
                CloseConnection();
            }
        }
		  public IEnumerable<IContratosDetalles2> ConceptosPorContratosInstalado(int IdProyecto, string contratos)
		  {
			  try
			  {
				  if(IdProyecto <= 0) return null;
				  OpenConexion();
				  _Connection.Query = SP_CONCEPTOSPORCONTRATO_INSTALADO;
				  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				  _Connection.AddParam("@IdProyecto", IdProyecto);
				  _Connection.AddParam("@Contratos", contratos);
				  IDataReader drDatos = _Connection.GetReader();
				  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
				  while(drDatos.Read())
				  {
					  listaResultado.Add(ToContratoDetalles(drDatos));
				  }
				  return listaResultado;
			  }
			  finally
			  {
				  CloseConnection();
			  }
		  }

          public IEnumerable<IContratosDetalles2> ConceptosPorContratosSolicitadoInstalado(int proyectoId, string contratos, DateTime fechaInicial, DateTime fechaFinal)
          {
              try
              {
                  if (proyectoId <= 0) return null;
                  OpenConexion();
                  _Connection.Query = SP_CONCEPTOSPORCONTRATO_SOLICITADOINSTALADO;
                  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                  _Connection.AddParam("@ProyectoId", proyectoId);
                  _Connection.AddParam("@Contratos", contratos);
                  _Connection.AddParam("@FechaInicial", fechaInicial);
                  _Connection.AddParam("@FechaFinal", fechaFinal);
                  IDataReader drDatos = _Connection.GetReader();
                  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
                  while (drDatos.Read())
                  {
                      IContratosDetalles2 iContrato = new CContratosDetalles2();
                      iContrato.Contrato = drDatos.GetValue(drDatos.GetOrdinal("Contrato")).ToString();
                      iContrato.Concepto = drDatos.GetValue(drDatos.GetOrdinal("Concepto")).ToString();
                      iContrato.Descripcion = drDatos.GetValue(drDatos.GetOrdinal("Descripcion")).ToString();
                      iContrato.UnidadMedida = drDatos.GetValue(drDatos.GetOrdinal("UnidadMedida")).ToString();
                      iContrato.Cantidad = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("BaseSolicitado")));
                      iContrato.CantidadProgramada = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("AdicionalSolicitado")));
                      iContrato.BaseInstalado = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("BaseInstalado")));
                      iContrato.AdicionalInstalado = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("AdicionalInstalado")));

                      listaResultado.Add(iContrato);
                  }
                  return listaResultado;
              }
              finally
              {
                  CloseConnection();
              }
          }

          public IEnumerable<IContratosDetalles2> ConceptosPorContratosSolicitadoEstado(int proyectoId, string contratos)
          {
              try
              {
                  if (proyectoId <= 0) return null;
                  OpenConexion();
                  _Connection.Query = SP_CONCEPTOSPORCONTRATO_SOLICITADOESTADO;
                  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                  _Connection.AddParam("@ProyectoId", proyectoId);
                  _Connection.AddParam("@Contratos", contratos);
                  IDataReader drDatos = _Connection.GetReader();
                  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
                  while (drDatos.Read())
                  {
                      IContratosDetalles2 iContrato = new CContratosDetalles2();
                      iContrato.Contrato = drDatos.GetValue(drDatos.GetOrdinal("Contrato")).ToString();
                      iContrato.Concepto = drDatos.GetValue(drDatos.GetOrdinal("Concepto")).ToString();
                      iContrato.Descripcion = drDatos.GetValue(drDatos.GetOrdinal("Descripcion")).ToString();
                      iContrato.UnidadMedida = drDatos.GetValue(drDatos.GetOrdinal("UnidadMedida")).ToString();
                      iContrato.Capitulo = drDatos.GetValue(drDatos.GetOrdinal("Estatus")).ToString();
                      iContrato.Cantidad = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Base")));
                      iContrato.CantidadProgramada = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Adicional")));

                      listaResultado.Add(iContrato);
                  }
                  return listaResultado;
              }
              finally
              {
                  CloseConnection();
              }
          }

          protected IContratosDetalles2 ToContratoAreaDetalles(IDataReader drDatos)
          {
              IContratosDetalles2 iContrato = new CContratosDetalles2();
              iContrato.Ubicacion = drDatos.GetValue(drDatos.GetOrdinal("Ubicacion")).ToString();
              iContrato.Concepto = drDatos.GetValue(drDatos.GetOrdinal("Concepto")).ToString();
              iContrato.Descripcion = drDatos.GetValue(drDatos.GetOrdinal("Descripcion")).ToString();
              iContrato.UnidadMedida = drDatos.GetValue(drDatos.GetOrdinal("UnidadMedida")).ToString();
              iContrato.Contrato = drDatos.GetValue(drDatos.GetOrdinal("Contrato")).ToString();
              iContrato.Cantidad = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Base")));
              iContrato.CantidadProgramada = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Adicional")));
              return iContrato;
          }
          public IEnumerable<IContratosDetalles2> ConceptosPorContratosSolicitadoArea(int proyectoId, string ubicaciones)
          {
              try
              {
                  if (proyectoId <= 0) return null;
                  OpenConexion();
                  _Connection.Query = SP_CONCEPTOSPORCONTRATO_SOLICITADOAREA;
                  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                  _Connection.AddParam("@ProyectoId", proyectoId);
                  _Connection.AddParam("@Ubicacion", ubicaciones);
                  IDataReader drDatos = _Connection.GetReader();
                  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
                  while (drDatos.Read())
                  {
                      listaResultado.Add(ToContratoAreaDetalles(drDatos));
                  }
                  return listaResultado;
              }
              finally
              {
                  CloseConnection();
              }
          }

          public IEnumerable<IContratosDetalles2> ConceptosPorContratosInstaladoArea(int proyectoId, string ubicaciones)
          {
              try
              {
                  if (proyectoId <= 0) return null;
                  OpenConexion();
                  _Connection.Query = SP_CONCEPTOSPORCONTRATO_INSTALADOAREA;
                  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                  _Connection.AddParam("@ProyectoId", proyectoId);
                  _Connection.AddParam("@Ubicaciones", ubicaciones);
                  IDataReader drDatos = _Connection.GetReader();
                  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
                  while (drDatos.Read())
                  {
                      listaResultado.Add(ToContratoAreaDetalles(drDatos));
                  }
                  return listaResultado;
              }
              finally
              {
                  CloseConnection();
              }
          }

          public IEnumerable<IContratosDetalles2> ConceptosSolicitadoEstadoArea(int proyectoId, string contratos)
          {
              try
              {
                  if (proyectoId <= 0) return null;
                  OpenConexion();
                  _Connection.Query = SP_CONCEPTOSPORCONTRATO_SOLICITADOESTADOAREA;
                  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                  _Connection.AddParam("@ProyectoId", proyectoId);
                  _Connection.AddParam("@Ubicaciones", contratos);
                  IDataReader drDatos = _Connection.GetReader();
                  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
                  while (drDatos.Read())
                  {
                      IContratosDetalles2 iContrato = new CContratosDetalles2();
                      iContrato.Ubicacion = drDatos.GetValue(drDatos.GetOrdinal("Ubicacion")).ToString();
                      iContrato.Contrato = drDatos.GetValue(drDatos.GetOrdinal("Contrato")).ToString();
                      iContrato.Concepto = drDatos.GetValue(drDatos.GetOrdinal("Concepto")).ToString();
                      iContrato.Descripcion = drDatos.GetValue(drDatos.GetOrdinal("Descripcion")).ToString();
                      iContrato.UnidadMedida = drDatos.GetValue(drDatos.GetOrdinal("UnidadMedida")).ToString();
                      iContrato.Capitulo = drDatos.GetValue(drDatos.GetOrdinal("Estatus")).ToString();
                      iContrato.Cantidad = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Base")));
                      iContrato.CantidadProgramada = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("Adicional")));

                      listaResultado.Add(iContrato);
                  }
                  return listaResultado;
              }
              finally
              {
                  CloseConnection();
              }
          }

          public IEnumerable<IContratosDetalles2> ConceptosSolicitadoInstaladoArea(int proyectoId, string ubicaciones, DateTime fechaInicial, DateTime fechaFinal)
          {
              try
              {
                  if (proyectoId <= 0) return null;
                  OpenConexion();
                  _Connection.Query = SP_CONCEPTOS_SOLICITADOINSTALADOAREA;
                  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
                  _Connection.AddParam("@ProyectoId", proyectoId);
                  _Connection.AddParam("@Ubicaciones", ubicaciones);
                  _Connection.AddParam("@FechaInicial", fechaInicial);
                  _Connection.AddParam("@FechaFinal", fechaFinal);
                  IDataReader drDatos = _Connection.GetReader();
                  List<IContratosDetalles2> listaResultado = new List<IContratosDetalles2>();
                  while (drDatos.Read())
                  {
                      IContratosDetalles2 iContrato = new CContratosDetalles2();
                      iContrato.Ubicacion = drDatos.GetValue(drDatos.GetOrdinal("Ubicacion")).ToString();
                      iContrato.Contrato = drDatos.GetValue(drDatos.GetOrdinal("Contrato")).ToString();
                      iContrato.Concepto = drDatos.GetValue(drDatos.GetOrdinal("Concepto")).ToString();
                      iContrato.Descripcion = drDatos.GetValue(drDatos.GetOrdinal("Descripcion")).ToString();
                      iContrato.UnidadMedida = drDatos.GetValue(drDatos.GetOrdinal("UnidadMedida")).ToString();
                      iContrato.Cantidad = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("BaseSolicitado")));
                      iContrato.CantidadProgramada = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("AdicionalSolicitado")));
                      iContrato.BaseInstalado = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("BaseInstalado")));
                      iContrato.AdicionalInstalado = Convert.ToDouble(drDatos.GetValue(drDatos.GetOrdinal("AdicionalInstalado")));

                      listaResultado.Add(iContrato);
                  }
                  return listaResultado;
              }
              finally
              {
                  CloseConnection();
              }
          }
			 public IEnumerable<IContratosDetalles2> EstimacionesPorContratoIndex(int IdProyecto, string contratos,DateTime Inicial,DateTime Final)
			 {
				 try
				 {
					 if(IdProyecto <= 0) return null;
					 DContratosDetalles2 contrato = new DContratosDetalles2();
					 OpenConexion();
					 _Connection.Query = SP_ESTIMACIONES_CONTRATO;
					 _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
					 _Connection.AddParam("@IdProyecto", IdProyecto);
					 _Connection.AddParam("@Contratos", contratos);
					 if(Inicial.Date.Year > 1)
					 {
						 _Connection.AddParam("@Inicial", Inicial);
					 }
					 if(Final.Date.Year > 1)
					 {
						 _Connection.AddParam("@Final", Final);
					 }
					 IDataReader drDatos = _Connection.GetReader();
					 IEnumerable<IEntidad> res = FillEntidades(drDatos,contrato.BuildEntity);
					 if(res != null && res.Count() > 0)
					 {
						 return res.Select(x => x as IContratosDetalles2);
					 }
					 return null;
				 }
				 finally
				 {
					 CloseConnection();
				 }
			 }
	}//  DProyectos ends.
}// LibDatos.Entidades ends.
