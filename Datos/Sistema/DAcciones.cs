using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 04/01/2017 10:37:35 a. m.*/
namespace Datos
{

	public partial class DAcciones : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Acciones]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CAcciones();
		}
		public DAcciones()
			: base()
		{
			Inicializa();
		}
		public DAcciones(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Acciones";
		}
		/// <summary>
		/// Procesa el objeto IAcciones via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IAcciones IEntity = Entity as IAcciones;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdAccion", IEntity.IdAccion);
			_Connection.AddParam("@AccionNombre", IEntity.AccionNombre);
			_Connection.AddParam("@ParametrosJs", IEntity.ParametrosJs);
			_Connection.AddParam("@ParametrosMvc", IEntity.ParametrosMvc);
			_Connection.AddParam("@Html", IEntity.Html);
			_Connection.AddParam("@MenuIncluir", IEntity.MenuIncluir);
		   _Connection.AddParam("@MenuOmitir", IEntity.MenuOmitir);			 
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdAccion = Convert.ToInt32(_Connection.GetParam("@IdAccion").ToString());
		}
	}//  DAcciones ends.
}// LibDatos.Entidades ends.
