using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:13 p. m.*/
namespace Datos
{

	public partial class DMovimientosDetalles : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_MovimientosDetalles]";
		public int iAlmacen
		{
			set;
			get;
		}
		public override Entidades.IEntidad BuildEntity()
		{
			return new CMovimientosDetalles();
		}
		public DMovimientosDetalles()
			: base()
		{
			Inicializa();
		}
		public DMovimientosDetalles(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "MovimientosDetalles";
		}
		/// <summary>
		/// Procesa el objeto IMovimientosDetalles via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IMovimientosDetalles IEntity = Entity as IMovimientosDetalles;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdMovimientoDetalle", IEntity.IdMovimientoDetalle);
			_Connection.AddParam("@IdAlmacen", iAlmacen);
			_Connection.AddParam("@IdMovimiento", IEntity.IdMovimiento);
			_Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
			_Connection.AddParam("@Cantidad", IEntity.Cantidad);
			_Connection.AddParam("@Precio", IEntity.Precio);
			_Connection.AddParam("@Observacion", IEntity.Observacion);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdMovimientoDetalle = Convert.ToInt32(_Connection.GetParam("@IdMovimientoDetalle").ToString());			
		}
	}//  DMovimientosDetalles ends.
}// LibDatos.Entidades ends.
