using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{

	public partial class DPermisos : DBase
	{		
		//protected string SP_PERFILES_MENU = "[dbo].[prc_PerfilMenus2]";
		//protected string SP_PERFILES_MENU_ACCIONES = "[dbo].[prc_PerfilAcciones2]";
		protected string SP_PERMISOS = "[dbo].[prc_permisos]";

		public override Entidades.IEntidad BuildEntity()
		{
			return new CPermisos();
		}
		public DPermisos(): base()
		{
			Inicializa();
		}
		public DPermisos(IConexion cnn): base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = String.Empty;
		}
		public CPermisosJs GetPermisos(int idPerfil,IEnumerable<IPropsWhere>wherePM,IEnumerable<IPropsWhere> wherePA )
		{
			CPermisosJs permisos = new CPermisosJs();
			try
			{				
				DPerfilMenus pm = new DPerfilMenus(_Connection);
				DPerfilAcciones pa = new DPerfilAcciones(_Connection);
				pm.AddWhere(wherePM.ToList());
				pa.AddWhere(wherePA.ToList());
				IEnumerable<IPerfilMenus> resPM = pm.Select(new CPerfilMenus()).Select(p=>p as IPerfilMenus);
				IEnumerable<IPerfilAcciones> resPA = pa.Select(new CPerfilAcciones()).Select(p => p as IPerfilAcciones);
				if(resPM != null && resPM.Count()>0)
				{
					List<string> array1 = new List<string>();
                    resPM.ToList().ForEach(x => array1.Add(String.Format("liTreeVer{0}|{1}", x.IdMenu.ToString(),x.IdMenu.ToString())));
					permisos.MenuVer = array1.ToArray();
				}
				if(resPA != null && resPA.Count() > 0)
				{
					List<string> array1 = new List<string>();
                    resPA.ToList().ForEach(x => array1.Add(String.Format("Permiso_{0}_{1}|{2},{3}", x.IdMenu.ToString(), x.IdAccion, x.IdMenu.ToString(), x.IdAccion)));
					permisos.MenuPermisos = array1.ToArray();
				}
				permisos.IdPerfil = idPerfil;
			}
			finally
			{
				CloseConnection();
			}	
			return permisos;
		}
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IPermisosJs permisos = Entity as IPermisosJs;
			/*INICIALIZA*/
			_Connection.Query = SP_PERMISOS;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam("@inicializa", 1);
			_Connection.AddParam("@IdPerfil", permisos.IdPerfil);	
			_Connection.NonQuery();

			if(permisos.MenuVer != null && permisos.MenuVer.Count()>0)
			{
				foreach(string permiso in permisos.MenuVer)
				{
					if(!String.IsNullOrEmpty(permiso))
					{
						int idMenu = 0;
						int.TryParse(permiso, out idMenu);
						if(idMenu > 0)
						{
							_Connection.Query = SP_PERMISOS;
							_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
							_Connection.AddParam("@inicializa", 2);	
							_Connection.AddParam("@IdPerfil", permisos.IdPerfil);
							_Connection.AddParam("@IdMenu", permiso);
							_Connection.NonQuery();
						}
					}
				}
			}
			if(permisos.MenuPermisos != null && permisos.MenuPermisos.Count() > 0)
			{
				foreach(string permiso in permisos.MenuPermisos)
				{
					if(!String.IsNullOrEmpty(permiso))
					{
						string[] data = permiso.Split(',');
						if(data != null && data.Count() >= 2)
						{
							int idMenu = 0;
							int idAccion = 0;
							int.TryParse(data[0], out idMenu);
							int.TryParse(data[1], out idAccion);
							if(idMenu > 0 && idAccion > 0)
							{
								_Connection.Query = SP_PERMISOS;
								_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
								_Connection.AddParam("@inicializa", 3);	
								_Connection.AddParam("@IdPerfil", permisos.IdPerfil);
								_Connection.AddParam("@IdMenu", idMenu);
								_Connection.AddParam("@IdAccion", idAccion);								
								_Connection.NonQuery();
							}
						}
					}
				}			
			}			
		}	
	}//  DMonedas ends.
}// LibDatos.Entidades ends.
