using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 10/04/2017 12:10:58 p. m.*/
namespace Datos
{
    
    public partial class   DClasificacionConceptos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_ClasificacionConceptos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CClasificacionConceptos();
	}		
        public DClasificacionConceptos():base()
	{
	  Inicializa();
	}
	public DClasificacionConceptos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "ClasificacionConceptos";
	} 
	/// <summary>
        /// Procesa el objeto IClasificacionConceptos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IClasificacionConceptos IEntity = Entity as IClasificacionConceptos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdConceptoClasificacion",IEntity.IdConceptoClasificacion); 
				 _Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
				 _Connection.AddParam("@Clasificacion", IEntity.Clasificacion);
				 _Connection.AddParam("@Descripicion", IEntity.Descripicion);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdConceptoClasificacion = Convert.ToInt32( _Connection.GetParam("@IdConceptoClasificacion").ToString());
 			
	}	
    }//  DClasificacionConceptos ends.
}// LibDatos.Entidades ends.
