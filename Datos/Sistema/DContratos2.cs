using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 30/03/2017 03:46:56 p. m.*/
namespace Datos
{
	public partial class DContratos2 : DBase
	{
		//public const string VIEW_CONTRATOS = "VWCONTRATOS2";
		protected const string SP_SAVE = "[dbo].[prc_Contratos2]";		
		protected const string SP_GENERA_FROM_PROYECTO = "[dbo].[prc_genera_from_proyecto]";
		protected const string SP_INDEX_CONTRATO = "[dbo].[prc_index_contratos]";
		protected const string SP_CONTRATO_DETALLES_BY_CONTRATO = "[dbo].[prc_contrato_detalles_byContrato]";
		protected const string SP_TOTAL_CONTRATO = "[dbo].[sp_total_contrato]";
		protected const string SP_TOP_CONTRATOS_SIN_PROYECTO = "[dbo].[prc_top_contratos_to_proyecto]";
		protected const string SP_OBTIENE_PRECIOS = "[dbo].[prc_get_precios_conceptos]";
		protected const string SP_COTIZACION_FROM_CONTRATO = "[dbo].[prc_genera_from_contrato]";
		protected const string SP_ESTIMACIONES_BY_CONTRATO = "[dbo].[prc_estimaciones_contrato]";


		public override Entidades.IEntidad BuildEntity()
		{
			return new CContratos2();
		}
		public DContratos2()
			: base()
		{
			Inicializa();
		}
		public DContratos2(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}		
		protected virtual void Inicializa()
		{
			_Tabla = "Contratos2";
		}
		/// <summary>
		/// Procesa el objeto IContratos2 via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IContratos2 IEntity = Entity as IContratos2;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@SubTotal", IEntity.SubTotal);
			_Connection.AddParam("@Total", IEntity.Total);
			_Connection.AddParam("@PorcentajeDescuento", IEntity.PorcentajeDescuento);
			_Connection.AddParam("@DescuentoMonto", IEntity.DescuentoMonto);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdCliente", IEntity.IdCliente);
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@IdIva", IEntity.IdIva);
			_Connection.AddParam("@IdTipoCambio", IEntity.IdTipoCambio);
			_Connection.AddParam("@IdMoneda", IEntity.IdMoneda);
			//_Connection.AddParam("@IdPedido", IEntity.IdPedido);
			//_Connection.AddParam("@Aprobado", IEntity.Aprobado);
			_Connection.AddParam("@Contrato", IEntity.Contrato);
			_Connection.AddParam("@Fecha", IEntity.Fecha);
			_Connection.AddParam("@IdUsuario", IEntity.IdUsuario); /*Se necesita el id de usuario para identificar quien realiza la salida y el pedido de materiales*/
			_Connection.AddParam("@Utilidad", IEntity.Utilidad);
			_Connection.AddParam("@IdUsuarioCostos", IEntity.IdUsuarioCostos);
			_Connection.AddParam("@MotivoModificacion", IEntity.MotivoModificacion);
            _Connection.AddParam("@Anticipo", IEntity.Anticipo);
            if (IEntity.IdPersonalCliente != null)
            {
                _Connection.AddParam("@IdPersonalCliente", IEntity.IdPersonalCliente);
            }
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdContrato = Convert.ToInt32(_Connection.GetParam("@IdContrato").ToString());
			if(this.Delete != 1)
			{
				SaveDetail(IEntity);
			}
			if(this.Delete != 1)
			{
				SaveDetailAdicionales(IEntity);
			}
			if(this.Delete != 1)
			{
				ContratoTotales(IEntity);
			}
			if(this.Delete != 1)
			{
				AsignaContratoProyecto(IEntity);
			}	
		}
		public void GeneraCotizacionFromContrato(IContratos2 contrato)
		{
			try
			{
				OpenConexion();
				_Connection.Query = SP_COTIZACION_FROM_CONTRATO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdContrato", contrato.IdContrato);
				_Connection.NonQuery();
			}
			finally
			{
				CloseConnection();
			}
		}
		public void ContratoTotales(IContratos2 contrato)
		{
			try
			{
				OpenConexion();
				_Connection.Query = SP_TOTAL_CONTRATO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdContrato", contrato.IdContrato);				
				_Connection.NonQuery();
			}
			finally
			{
				CloseConnection();
			}			
		}
		protected virtual void SaveDetail(IContratos2 Entity)
		{
			if(Entity == null) return;
			int Partida = 1;
			if(Entity.Conceptos != null && Entity.Conceptos.Count() > 0)
			{
				foreach(IContratosDetalles2 detalle in Entity.Conceptos)
				{
					DContratosDetalles2 _ddetail = new DContratosDetalles2(_Connection);
					detalle.IdContrato = Entity.IdContrato;
					detalle.EsAdicional = false;
					if(detalle.IdContratosDetalles < 0)
					{
						detalle.IdContratosDetalles = detalle.IdContratosDetalles * -1;
						_ddetail.Delete = 1;
					}
					else
					{
						detalle.Partida = Partida++;
					}
					_ddetail.Save(detalle);
				}
			}
		}
		protected virtual void AsignaContratoProyecto(IContratos2 Entity)
		{
			if(Entity == null || Entity.Proyecto == null) return;
			Entity.Proyecto.Contratos = new List<CProyectoContratos>();
			Entity.Proyecto.Contratos.Add(new CProyectoContratos()
			{
				IdContrato=Entity.IdContrato,
				Estatus=true
			});

			DProyectoContratos b = new DProyectoContratos(this._Connection);
			b.AddWhere(new CPropsWhere()
			{
				NombreCampo = "IdProyecto",
				Condicion="=",
				OmiteConcatenarTabla=true,
				Valor=Entity.Proyecto.IdProyecto,
				Prioridad=true
			});
			b.AddWhere(new CPropsWhere()
			{
				NombreCampo = "IdContrato",
				Condicion = "=",
				OmiteConcatenarTabla = true,
				Prioridad=true,
				Valor = Entity.IdContrato
			});
			IEnumerable<IEntidad> res = b.Select(new CProyectoContratos());
			if(!(res != null && res.Count()>0))
			{
				DProyectos p = new DProyectos(this._Connection);
				p.SaveProyectosContratos(Entity.Proyecto);
			}
		}
		protected virtual void SaveDetailAdicionales(IContratos2 Entity)
		{
			if(Entity == null) return;
			int Partida = 1;
			if(Entity.Adicionales != null && Entity.Adicionales.Count() > 0)
			{
				foreach(IContratosDetalles2 detalle in Entity.Adicionales)
				{
					DContratosDetalles2 _ddetail = new DContratosDetalles2(_Connection);
					detalle.IdContrato = Entity.IdContrato;
					detalle.EsAdicional = true;
					if(detalle.IdContratosDetalles < 0)
					{
						detalle.IdContratosDetalles = detalle.IdContratosDetalles * -1;
						_ddetail.Delete = 1;
					}
					else
					{
						detalle.Partida = Partida++;
					}
					_ddetail.Save(detalle);
				}
			}
		}
		public IContratos2 GeneraFromProyecto(IContratos2 contrato)
		{
			try
			{
				OpenConexion();
				_Connection.Query = SP_GENERA_FROM_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", contrato.Proyecto.IdProyecto);
				_Connection.AddParam("@Contrato", contrato.Contrato);
				_Connection.AddParam("@IdUsuario", contrato.IdUsuario);
				_Connection.NonQuery();												
			}
			finally
			{
				CloseConnection();
			}
			return contrato;
		}
		protected override IEnumerable<IEntidad> FillEntidades(IDataReader drDatos)
		{
			List<IEntidad> lstEntities = new List<IEntidad>();
			try
			{
				while(drDatos.Read())
				{
					IEntidad entidad = GetEntity(drDatos, BuildEntity());
					((IContratos2)entidad).Usuario = GetEntity(drDatos, new CUsuarios()) as CUsuarios;
					((IContratos2)entidad).Proyecto = GetEntity(drDatos, new CProyectos()) as CProyectos;
					lstEntities.Add(entidad);
				}
			}
			finally
			{
				if(drDatos != null)
				{
					if(!drDatos.IsClosed) drDatos.Close();
				}
			}
			return lstEntities;
		}
		protected override void FillEntidades<Entity>(IPagina<Entity> pagina, IDataReader drDatos)
		{
			List<Entity> lstEntities = new List<Entity>();
			try
			{
				while(drDatos.Read())
				{
					Entity entidad = (Entity)BuildEntity();
					if(pagina.Paginas <= 0)
					{
						pagina.Paginas = Convert.ToInt32(drDatos[DPaginaConst.CAMPO_PAGINAS].ToString());
					}
					Entity EntityAux = (Entity)GetEntity(drDatos, entidad);
					((IContratos2)EntityAux).Usuario = GetEntity(drDatos, new CUsuarios()) as CUsuarios;
					((IContratos2)EntityAux).Proyecto = GetEntity(drDatos, new CProyectos()) as CProyectos;
					lstEntities.Add(EntityAux);
				}
				pagina.Contenido = lstEntities;
			}
			finally
			{
				if(drDatos != null)
				{
					if(!drDatos.IsClosed) drDatos.Close();
				}
			}
		}
		public virtual IPagina<Entity> GetIndexContratos<Entity>(int Pagina) where Entity : IEntidad, new()
		{
			IPagina<Entity> pagina = new DPagina<Entity>(Pagina, 0);
			try
			{
				IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				IPropsWhere unidad = _Where.Where(a => a.NombreCampo == "Unidades.IdUnidad").FirstOrDefault();
				IPropsWhere cotizacion = _Where.Where(a => a.NombreCampo == "Contratos2.IdContrato").FirstOrDefault();
				OpenConexion();
				_Connection.Query = SP_INDEX_CONTRATO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdPagina", Pagina);
				if(empresa != null)
				{
					_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor));
				}
				if(unidad != null)
				{
					_Connection.AddParam("@IdUnidad", Convert.ToInt32(unidad.Valor));
				}
				if(cotizacion != null)
				{
					_Connection.AddParam("@IdContrato", Convert.ToInt32(cotizacion.Valor));
				}
				FillEntidades(pagina, _Connection.GetReader());
				if(pagina.RegistrosPP <= 0) pagina.RegistrosPP = (pagina != null) ? pagina.Contenido.Count() : 0;
			}
			finally
			{
				CloseConnection();
			}
			return pagina;
		}
		public virtual IContratos2 GetDetalleContrato(IContratos2 contrato)
		{
			//IContratos2 contrato = null;
			try
			{
				if(contrato ==null) return contrato;
				if(contrato.IdContrato<=0) return contrato;
				/*IPagina<CContratos2> pagina = GetIndexContratos<CContratos2>(1);
				if(pagina == null || pagina.Contenido == null || pagina.Contenido.Count() <= 0) return contrato;
				contrato = pagina.Contenido.FirstOrDefault();*/
				List<CContratosDetalles2> conceptos = new List<CContratosDetalles2>();
				List<CContratosDetallesInsumos2> insumos = new List<CContratosDetallesInsumos2>();
				int data = 0;
				OpenConexion();
				_Connection.Query = SP_CONTRATO_DETALLES_BY_CONTRATO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdContrato", contrato.IdContrato);
				IDataReader drDatos = _Connection.GetReader();
				while(_Connection.HasRows(drDatos) || data < 2)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								conceptos.Add(GetEntity(drDatos, new CContratosDetalles2()) as CContratosDetalles2);
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CContratosDetallesInsumos2()) as CContratosDetallesInsumos2);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 2) ends.
				if(conceptos != null && conceptos.Count() > 0)
				{
					if(insumos != null && insumos.Count() > 0)
					{
						conceptos.ForEach(c =>
						{
							IEnumerable<CContratosDetallesInsumos2> res = insumos.Where(i => i.IdConcepto == c.IdConcepto && i.IdContratosDetalles==c.IdContratosDetalles);
							if(res != null && res.Count() > 0)
							{
								c.Insumos = res.ToList();
							}
						});
					}
				}//if ends.
				contrato.Conceptos = conceptos;
			}//try ends
			finally
			{
				CloseConnection();
			}
			return contrato;
		}
		public IEnumerable<IContratos2> ContratosSinProyecto(IEnumerable<IPropsWhere> where)
		{
			IEnumerable<IContratos2> res = null;
			try
			{
				IPropsWhere condicion = where.Where(x => x.NombreCampo == "Contrato").FirstOrDefault();
				IPropsWhere Empresa = where.Where(x => x.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				IPropsWhere Unidad = where.Where(x => x.NombreCampo == "Unidades.IdUnidad").FirstOrDefault();

				if(condicion == null) return null;
				OpenConexion();
				_Connection.Query = SP_TOP_CONTRATOS_SIN_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdEmpresa", Empresa.Valor.ToString());
				_Connection.AddParam("@IdUnidad", Unidad.Valor.ToString());
				_Connection.AddParam("@condicion", condicion.Valor.ToString());
				_Connection.NonQuery();
				IEnumerable<IEntidad> res1 = FillEntidades(_Connection.GetReader());
				if(res1 != null && res1.Count() > 0)
				{
					res = res1.Select(x => x as IContratos2);
				}
			}
			finally
			{
				CloseConnection();
			}
			return res;
		}
		public IEnumerable<IContratosDetalles2> ObtienePrecios(IContratos2 contrato, string xml)
		{
			List<IContratosDetalles2> res = null;
			try
			{
				IDataReader drDatos = null;
				res = new List<IContratosDetalles2>();
				OpenConexion();
				_Connection.Query = SP_OBTIENE_PRECIOS;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdEmpresa", contrato.IdEmpresa);
				_Connection.AddParam("@FechaContrato", contrato.Fecha);
				_Connection.AddParam("@Utilidad", contrato.Utilidad);
				_Connection.AddParam("@XmlData", xml);
				_Connection.NonQuery();
				drDatos = _Connection.GetReader();
				while(drDatos.Read())
				{
					IContratosDetalles2 detalle = GetEntity(drDatos, new CContratosDetalles2()) as IContratosDetalles2;
					detalle.Insumos = new List<CContratosDetallesInsumos2>();
					detalle.Insumos.Add(GetEntity(drDatos, new CContratosDetallesInsumos2()) as CContratosDetallesInsumos2);
					res.Add(detalle);
				}				
			}
			finally
			{
				CloseConnection();
			}
			return res;
		}
		protected void ActualizaCostos(List<CContratosDetalles2> Conceptos, 
			                            List<CContratosDetalles2> CostosManuales,
												 bool IsAdicional)
		{
			if(Conceptos != null && Conceptos.Count > 0)
			{
				Conceptos.ForEach(c =>
				{
					IEnumerable<IContratosDetalles2> newConcepto = CostosManuales.Where(c2 => c2.IdConcepto == c.IdConcepto && c2.EsAdicional == IsAdicional);
					if(newConcepto != null && newConcepto.Count() > 0)
					{
						c.Insumos.ForEach(i =>
						{
							IEnumerable<IContratosDetallesInsumos2> res = newConcepto.FirstOrDefault().Insumos.Where(i2 => i2.IdInsumo == i.IdInsumo &&
																																						 i2.IdConcepto == i.IdConcepto);
							if(res != null && res.Count() > 0)
							{
								i.Costo = res.FirstOrDefault().Costo;
							}
						});
					}
				});
			}
		}
		protected List<CContratosDetallesInsumos2> GetInsumos(List<CContratosDetallesInsumos2> Insumos,
										  List<CContratosDetalles2> Conceptos)
		{
			if(Insumos == null)
			{
				Insumos = new List<CContratosDetallesInsumos2>();
			}
			if(Conceptos != null && Conceptos.Count > 0)
			{
				Conceptos.ForEach(c =>
				{
					if(c.Insumos != null && c.Insumos.Count > 0)
					{
						c.Insumos.ForEach(i =>
						{
							Insumos.Add(i);
						});
					}
				});
			}
			return Insumos;
		}
		public virtual void ActualizaCostosManuales(IContratos2 Entity)
		{
			List<CContratosDetallesInsumos2> allInsumos = null;
			if(Entity != null && Entity.CostosManuales != null && Entity.CostosManuales.Count > 0)
			{
				ActualizaCostos(Entity.Conceptos, Entity.CostosManuales,false);
				ActualizaCostos(Entity.Adicionales, Entity.CostosManuales,true);
				allInsumos = GetInsumos(allInsumos, Entity.Conceptos);
				allInsumos = GetInsumos(allInsumos, Entity.Adicionales);
				if(allInsumos != null && allInsumos.Count > 0)
				{
					DContratosDetallesInsumos2 dinsumo = new DContratosDetallesInsumos2();
					foreach(IContratosDetallesInsumos2 insumo in allInsumos)
					{
						dinsumo.Save(insumo);
					}
				}
			}
		}//ActualizaCostosManuales ends.
		public virtual IContratos2 EstimacionesPorContrato(IContratos2 contrato,DateTime? FechaIni,DateTime? FechaFin)
		{	
            try
			{
				if(contrato == null) return contrato;
				if(contrato.IdContrato <= 0) return contrato;				
				List<CContratosDetalles2> conceptos = new List<CContratosDetalles2>();
				List<CContratosDetallesInsumos2> insumos = new List<CContratosDetallesInsumos2>();
				List<CEstimaciones> estimaciones = new List<CEstimaciones>();
				int data = 0;
				OpenConexion();
				_Connection.Query = SP_ESTIMACIONES_BY_CONTRATO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdContrato", contrato.IdContrato);
                _Connection.AddParam("@FechaIni", FechaIni);
                _Connection.AddParam("@FechaFin", FechaFin);
				IDataReader drDatos = _Connection.GetReader();
				while(_Connection.HasRows(drDatos) || data < 3)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								conceptos.Add(GetEntity(drDatos, new CContratosDetalles2()) as CContratosDetalles2);
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CContratosDetallesInsumos2()) as CContratosDetallesInsumos2);
								break;
							case 2:
								estimaciones.Add(GetEntity(drDatos, new CEstimaciones()) as CEstimaciones);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 2) ends.
				if(conceptos != null && conceptos.Count() > 0)
				{
					if(insumos != null && insumos.Count() > 0)
					{
						conceptos.ForEach(c =>
						{							
							IEnumerable<CContratosDetallesInsumos2> res = insumos.Where(i => i.IdConcepto == c.IdConcepto && i.IdContratosDetalles == c.IdContratosDetalles);
							if(res != null && res.Count() > 0)
							{
								c.Insumos = res.ToList();
							}
						});
					}
					if(estimaciones != null && estimaciones.Count() > 0)
					{
						conceptos.ForEach(c =>
						{
							IEnumerable<CEstimaciones> res = estimaciones.Where(e =>  e.IdContratosDetalles== c.IdContratosDetalles);
							if(res != null && res.Count() > 0)
							{
								c.Estimaciones = res.OrderBy(e=>e.FechaInicio).ToList();
							}
						});
					}
				}//if ends.
				contrato.Conceptos = conceptos;
			}//try ends
			finally
			{
				CloseConnection();
			}
			return contrato;
		}


	}//  DContratos2 ends.
	public class DvwContratos2 : DContratos2
	{
		public const string VIEW_CONTRATOS2 = "vwContratos2";
		public DvwContratos2()
			: base()
		{
			Inicializa();
		}
		protected override void Inicializa()
		{
			_Tabla = DvwContratos2.VIEW_CONTRATOS2;
		}
		public override IEntidad BuildEntity()
		{
			return new CvwContratos2();
		}
	}
}// LibDatos.Entidades ends.
