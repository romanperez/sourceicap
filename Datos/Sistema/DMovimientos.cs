using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:13 p. m.*/
namespace Datos
{
	public partial class DMovimientos : DBase
	{
		protected const string SP_SAVE = "[dbo].[prc_Movimientos]";
		//protected const string SP_MOVIMIENTOS_BY_TIPO = "[dbo].[prc_Movimientos_by_tipo]";
		protected const string SP_ACTUALIZA_EXISTENCIAS = "[dbo].[sp_afecta_existencias]";
		protected const string MOVIMIENTOS_BY_PROYECTO = "[dbo].[prc_MovimientosByProyecto]";
		protected const string BUSCA_MOVIMIENTO = "[dbo].[sp_BuscaMovimiento]";

		public override Entidades.IEntidad BuildEntity()
		{
			return new CMovimientos();
		}
		public DMovimientos()
			: base()
		{
			Inicializa();
		}
		public DMovimientos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Movimientos";
		}
		/// <summary>
		/// Procesa el objeto IMovimientos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IMovimientos IEntity = Entity as IMovimientos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdMovimiento", IEntity.IdMovimiento);
			_Connection.AddParam("@Tipo", IEntity.Tipo);
			_Connection.AddParam("@Fecha", IEntity.Fecha);
			_Connection.AddParam("@IdAlmacen", IEntity.IdAlmacen);
			if(IEntity.IdAlmacenDestino > 0)
			{
				_Connection.AddParam("@IdAlmacenDestino", IEntity.IdAlmacenDestino);
			}
			_Connection.AddParam("@IdUsuario", IEntity.IdUsuario);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdMovimiento = Convert.ToInt32(_Connection.GetParam("@IdMovimiento").ToString());
			SaveDetail(Entity);
			OpenConexion();
			_Connection.Query = SP_ACTUALIZA_EXISTENCIAS;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.NonQuery();			
		}
		protected virtual void SaveDetail(IEntidad Entity)
		{
			if(Entity == null) return ;
			IMovimientos mov = Entity as IMovimientos;
			if(mov.DetalleMovimientos != null && mov.DetalleMovimientos.Count() > 0)
			{
				foreach(IMovimientosDetalles detail in mov.DetalleMovimientos)
				{
					DMovimientosDetalles _ddetail = new DMovimientosDetalles(_Connection);
					_ddetail.iAlmacen = mov.IdAlmacen;

					if(detail.IdMovimientoDetalle < 0)
					{
						detail.IdMovimientoDetalle = detail.IdMovimientoDetalle * -1;
						_ddetail.Delete = 1;							
					}
					detail.IdMovimiento = mov.IdMovimiento;
					_ddetail.Save(detail);
				}
			}
		}
		//protected void BuildSpMovimientosByTipo(int TipoMovimiento, int Pagina, IEnumerable<CPropsWhere> where)
		//{
		//	int IdEmpresa=0;
		//	int IdUnidad=0;
		//	int IdAlmacen=0;
		//	IEnumerable<IPropsWhere> aux = where.Where(x => x.NombreCampo == "Empresa");
		//	if(aux != null && aux.Count() > 0)
		//	{
		//		int.TryParse(aux.First().Valor.ToString(), out IdEmpresa);
		//	}
		//	aux = where.Where(x => x.NombreCampo == "Unidad");
		//	if(aux != null && aux.Count() > 0)
		//	{
		//		int.TryParse(aux.First().Valor.ToString(), out IdUnidad);
		//	}
		//	aux = where.Where(x => x.NombreCampo == "Almacen");
		//	if(aux != null && aux.Count() > 0)
		//	{
		//		int.TryParse(aux.First().Valor.ToString(), out IdAlmacen);
		//	}
		//	_Connection.Query = SP_MOVIMIENTOS_BY_TIPO;
		//	_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
		//	_Connection.AddParam("@IdEmpresa", IdEmpresa);
		//	_Connection.AddParam("@IdUnidad", IdUnidad);
		//	if(IdAlmacen>0)
		//	{
		//		_Connection.AddParam("@IdAlmacen", IdAlmacen);		
		//	}			
		//	_Connection.AddParam("@Tipo", TipoMovimiento);
		//	_Connection.AddParam("@Pagina", Pagina);
		//	_Connection.NonQuery();
		//}
		//public virtual IPagina<CMovimientos> GetPageMovimientosByTipo(int TipoMovimiento, int Pagina, IEnumerable<CPropsWhere> where) 
		//{
		//	IPagina<CMovimientos> pagina = new DPagina<CMovimientos>(Pagina, 0);
		//	try
		//	{
		//		OpenConexion();
		//		BuildSpMovimientosByTipo(TipoMovimiento, Pagina, where);
		//		FillEntidades(pagina, _Connection.GetReader());
		//		if(pagina.RegistrosPP <= 0) pagina.RegistrosPP = (pagina != null) ? pagina.Contenido.Count() : 0;
		//	}
		//	finally
		//	{
		//		CloseConnection();
		//	}
		//	return pagina;
		//}
		public IEnumerable<IMovimientos> BuscaMovimiento(CFiltrosMovimiento Entity)
		{
			IEnumerable<IMovimientos> result = null;
			try
			{
				OpenConexion();
				_Connection.Query = DMovimientos.BUSCA_MOVIMIENTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@Tipo",((Entity.Tipo>0)? Entity.Tipo.ToString():null));
				_Connection.AddParam("@FechaIni", Entity.FechaIni);
				_Connection.AddParam("@FechaFin", Entity.FechaFin);
				_Connection.AddParam("@Proyecto", ((!String.IsNullOrEmpty(Entity.Proyecto)) ? Entity.Proyecto : null));
				_Connection.AddParam("@Contrato", ((!String.IsNullOrEmpty( Entity.Contrato)) ? Entity.Contrato : null));
				_Connection.AddParam("@IdUsuario", ((Entity.IdUsuario > 0) ? Entity.IdUsuario.ToString() : null));
				_Connection.AddParam("@IdUnidadOrigen", ((Entity.IdUnidadOrigen > 0) ? Entity.IdUnidadOrigen.ToString() : null));
				_Connection.AddParam("@IdAlmacenOrigen", ((Entity.IdAlmacenOrigen > 0) ? Entity.IdAlmacenOrigen.ToString() : null));
				_Connection.AddParam("@IdUnidadDestino", ((Entity.IdUnidadDestino > 0) ? Entity.IdUnidadDestino.ToString() : null));
				_Connection.AddParam("@IdAlmacenDestino", ((Entity.IdAlmacenDestino > 0) ? Entity.IdAlmacenDestino.ToString() : null));
				//IDataReader drDatos = _Connection.GetReader();
				//result = new List<CMovimientos>();
				IEnumerable<IEntidad> resultado = FillEntidades(_Connection.GetReader());
				if(resultado != null && resultado.Count() > 0)
				{
					result = resultado.Select(x => x as IMovimientos);
				}
				
			}
			finally
			{
				CloseConnection();
			}
			return result;
		}
		public IEnumerable<IMovimientos> MovimientosByProyecto(IProyectos proyecto)
		{
			try				
			{				
				if(proyecto == null) return null;
				int data = 0;
				List<CMovimientos> movimientos = new List<CMovimientos>();
				List<CMovimientosDetalles> insumos = new List<CMovimientosDetalles>();
				OpenConexion();
				_Connection.Query = DMovimientos.MOVIMIENTOS_BY_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", proyecto.IdProyecto);
				IDataReader drDatos = _Connection.GetReader();
				while(_Connection.HasRows(drDatos) || data < 2)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								movimientos.Add(GetEntity(drDatos, new CMovimientos()) as CMovimientos);
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CMovimientosDetalles()) as CMovimientosDetalles);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 2) ends.
				if(movimientos != null && movimientos.Count() > 0)
				{
					if(insumos != null && insumos.Count() > 0)
					{
						movimientos.ForEach(m =>
						{
							IEnumerable<CMovimientosDetalles> res = insumos.Where(i => i.IdMovimiento == m.IdMovimiento);
							if(res != null && res.Count() > 0)
							{
								m.DetalleMovimientos = res.ToList();
							}
						});
					}
				}//if ends.
				return movimientos;
			}
			finally
			{
				CloseConnection();
			}
		}
	}//  DMovimientos ends.
}// LibDatos.Entidades ends.
