﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
namespace Datos
{
	public class CPropsWhere : IPropsWhere
	{
		public CPropsWhere()
		{
			Prioridad = false;
			OmiteConcatenarTabla = false;
		}
		public virtual bool OmiteConcatenarTabla
		{
			set;
			get;
		}
		public virtual bool OmiteParametro
		{
			set;
			get;
		}
		public virtual bool Prioridad
		{
			set;
			get;
		}
		public virtual string Operador{set;get;}
		public virtual string NombreCampo 
		{
			set;
			get;
		}
		public virtual string Condicion
		{
			set;
			get;
		}
		public virtual object Valor
		{
			set;
			get;
		}
		public virtual string Plantilla
		{
			set;
			get;
		}
		public static void AddPropList(List<IPropsWhere> propiedades, IPropsWhere propiedad)
		{
			var Lst = propiedades.Where(p => p.NombreCampo == propiedad.NombreCampo);
			if(Lst == null)    
			{
				propiedades.Add(propiedad);
				return;
			}
			if(Lst.Count() <= 0)
			{
				propiedades.Add(propiedad);
				return;
			}
			Lst.First().Condicion = propiedad.Condicion;
		}		
		public bool ValidaConsultaMVC(object Entidad)
		{
			this.Prioridad = true;
			string[] Operadores = new string[] { "=", "!=", ">", "<", ">=", "<=" ,"*"};
			string[] CondicionValor = null;
			if(Valor == null)
			{
				return false;
			}
			if(!String.IsNullOrEmpty(Valor.ToString()))
			{
				CondicionValor = Valor.ToString().Split(' ');
				if(CondicionValor.Length >= 2)
				{
					Condicion = CondicionValor[0];
					Valor = String.Empty;
					for(int c = 1; c<CondicionValor.Length;c++ )
					{
						Valor += CondicionValor[c] + " " ;
					}
					Valor = Valor.ToString().Trim();
					IEnumerable<string> existe = Operadores.Where(x => x == Condicion);
					if(existe == null || existe.Count() <= 0)
					{
						return false;
					}
					if(Condicion == "*")
					{
						Condicion = DBase.CONTENGA;
					}
				}
				else
				{
					if(String.IsNullOrEmpty(Condicion))
					{															
						return false;
					}
					
				}
			}
			else
			{
				return false;
			}			
			try
			{
				DEmpresas emp = new DEmpresas();
				emp.SetValue(Entidad, this.NombreCampo, Valor);
			}
			catch(Exception ee)
			{
				string ee2 = ee.Message;
				return false;
			}
			return true;
		}
		//public bool ValidaConsultaMVC(object Entidad)
		//{
		//	this.Prioridad = true;
		//	string []Operadores =new string[]{"=" ,"!=" ,">" ,"<", ">=" , "<="};
		//	IEnumerable<string> existe = Operadores.Where(x => x == Condicion);
		//	if(existe == null || existe.Count() <= 0)
		//	{


		//		string operador = this.Valor.ToString().Substring(0, 1);
		//		IEnumerable<string> es1digitos = Operadores.Where(x => x == operador);
		//		if(es1digitos != null && es1digitos.Count() > 0)
		//		{
		//			Condicion = Valor.ToString().Substring(0, 1);
		//			Valor = Valor.ToString().Substring(1, Valor.ToString().Length - 1);
		//		}
		//		else
		//		{
		//			operador = this.Valor.ToString().Substring(0, 2);
		//			IEnumerable<string> es2digitos = Operadores.Where(x => x == operador);
		//			if(es2digitos != null && es2digitos.Count() > 0)
		//			{
		//				Condicion = Valor.ToString().Substring(0, 2);
		//				Valor = Valor.ToString().Substring(2, Valor.ToString().Length - 2);
		//			}
		//			else
		//			{
		//				return false;
		//			}
		//		}
		//	}
		//	try
		//	{
		//		DEmpresas emp = new DEmpresas();
		//		emp.SetValue(Entidad, this.NombreCampo, Valor);
		//	}
		//	catch(Exception ee)
		//	{
		//		string ee2 = ee.Message;
		//		return false;
		//	}
		//	return true;			
		//}
		public string RemoveDot()
		{
			/*string[] campoCompuesto = NombreCampo.Split('|');
			if(campoCompuesto.Length > 1)
			{
				return (parametro )? campoCompuesto[1]: campoCompuesto[0];
			}
			else
			{
				return NombreCampo.Replace(".", "");
			}*/
			return NombreCampo.Replace(".", "");
		}
	}

}
