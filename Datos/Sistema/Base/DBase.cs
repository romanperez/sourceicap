﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Entidades;
using System.Reflection;
using System.Data.SqlClient;
using System.ComponentModel;
namespace Datos
{
	public abstract class DBase:IBase
	{
		public const int DECIMALES_OPERACIONES = 4;
		public const int DECIMALES_REDONDEO = 2;
		public const string InnerJoin = "Inner Join";
		public const string LeftJoin = "left Join";
		public const string Group_By = "Group by";
		public const string On = "On";
		public const string SELECT = "SELECT";
		public const string AND = " AND ";
		public const string CONTENGA = "contenga";
		public const string IN = "in";
		public const string BETWEEN = "between";
		protected enum eCommit
		{
			CommitFromClass = 0, CommitFromSP
		}
		protected  const string WHERE_OMITIR = "Omitir";
		protected IError _ErrorInterno;		
		protected IConexion _Connection;
		protected int _update;		  		  		 
		protected int _delete;
		protected int _new;
		protected string _Tabla;		
		protected List<IPropsWhere> _Where;
		protected string[] _camposJoin;
		protected string[] _condicionesJoin;
		public delegate IEntidad fnBuildEntity();
		public IConexion GetConection
		{
			get
			{
				return _Connection;
			}
		}
		public virtual string GroupBy
		{
			set;
			get;
		}
		public virtual string Tabla
		{
			get
			{
				return _Tabla;
			}
			set
			{
				_Tabla = value;
			}
		}
		/// <summary>
		/// Especifica si la entidad sera dada de alta.
		/// </summary>
		public virtual int Nuevo
		{
			get
			{
				return _new;
			}
			set
			{
				_new = value;
			}

		}
		/// <summary>
		/// Especifica si la entidad esta siendo modificada.
		/// </summary>
		public virtual int Update
		{
			get
			{
				return _update;
			}
			set
			{
				_update = value;
			}
		}
		/// <summary>
		/// Especifica si la entidad esta seleccionada para ser eliminada.
		/// </summary>
		public virtual int Delete
		{
			get
			{
				return _delete;
			}
			set
			{
				_delete = value;
			}
		}
		  /// <summary>
		  /// Delegado el cual contruye la llamada a un Stored Procedure.
		  /// </summary>
        protected delegate void _BuildCommandCustom();		  
		  protected DBase()
		  {
			  InicializaBase();
		  }
		  protected DBase(IConexion cnn)
		  {
			  _Connection = cnn;
			  InicializaBase();
		  }
		  protected void InicializaBase()
		  {
			  _Where = new List<IPropsWhere>();
			  _camposJoin = null;
			  _condicionesJoin = null;
		  }
#region Manejo Conexion 
       protected virtual string BuildConnectionString()
       {
           return String.Empty;
       }
   	  /// <summary>
	     /// Establece una nueva conexión.
		  /// </summary>
        protected virtual void OpenConexion()
        {
            string cnn = String.Empty;
            if (_Connection != null)
            {
                if (_Connection.Status() == ConnectionState.Open)
                {
                    return; //La conexion ha sido abierta anteriormente.
                } 
                cnn = _Connection.ConnectionString;
            }
            if (String.IsNullOrEmpty(cnn)) { cnn = BuildConnectionString(); }
            _Connection =(String.IsNullOrEmpty(cnn))? new CSqlServer() : new CSqlServer(cnn);
        }		
        /// <summary>
        /// Termina la conexión actual.
        /// </summary>
        protected void CloseConnection()
        {
            if (_Connection != null)
            {
                _Connection.CloseConection();
            }
        }
#endregion

#region Manejo de errores
		  public static IError StaticInicializaError()
		  {
			  return new DError();
		  }
		  public void ClearError()
		  {
			  _ErrorInterno = null;
			  InicializaError();
		  }
		  public virtual void InicializaError()
		  {
			  if(_ErrorInterno == null) _ErrorInterno = DBase.StaticInicializaError();
		  }
		  public virtual void SetError(string error)
		  {
			  InicializaError();
			  _ErrorInterno.Error = error;			  
		  }
		  public virtual void SetError(string error,bool append)
		  {
			  InicializaError();
			  _ErrorInterno.Error = String.Format("{0} {1}",_ErrorInterno.Error,error);
		  }
		  protected virtual void SaveQueryInfo()
		  {
			  if(_Connection != null)
			  {
				  if(!String.IsNullOrEmpty(_Connection.Query))
				  {
					  _ErrorInterno.SaveLog(new Exception( String.Format("Error Asociado {0} [{1}]", _ErrorInterno.IdError, _Connection.Query)));
				  }
			  }
		  }
		  public virtual void OnError(Exception Error)
		  {
			  InicializaError();
			  try
			  {
				  _ErrorInterno.SaveLog(Error);
				  SaveQueryInfo();
			  }//try ends.
			  catch(Exception eInesperado)
			  {
				  _ErrorInterno.Error =  eInesperado.Message;
			  }//catch ends.			 
		  }
		  public virtual IError GetError()
		  {
			  return _ErrorInterno;
		  }
#endregion
        public abstract IEntidad BuildEntity();

#region Joins
		  public virtual void SetConfigJoins(string[] campos, string[] join)
		  {
			  _camposJoin = campos;
			  _condicionesJoin = join;
		  }
		  public virtual string[] CamposJoin()
		  {
			  return _camposJoin;
		  }
		  public virtual string[] CondicionesJoin()
		  {
			  return _condicionesJoin;
		  }
#endregion
#region REFLECTION_BUILD_ENTITY
		  public static string[] GetProperties(object entity)
		  {
			 PropertyInfo[] info= entity.GetType().GetProperties();
			 List<string> p = new List<string>();
			 if(info != null && info.Count() > 0)
			 {
				 info.ToList().ForEach(x => p.Add(x.Name));
				 return p.ToArray();
			 }
			 else return null;
		  }
		  /// <summary>
        /// Utilizando Reflection recuperamos el la propiedad en un objeto [PropertyInfo] de la instancia de un objeto.
		  /// </summary>
		  /// <param name="entity">Instancia del objeto.</param>
		  /// <param name="property">Nombre de la propiedad.</param>
        /// <returns>Instancia de PropertyInfo en base a los parametros.</returns>
		  public static PropertyInfo GetProperty(object entity, string property)
		  {
			  return entity.GetType().GetProperty(property);			  
		  }
		  /// <summary>
		  /// Mediante reflection asignamos el valor a una determinada propiedad de la instancia de un objeto.
		  /// </summary>
		  /// <param name="entity">Instancia del objeto.</param>
		  /// <param name="propiedad">Nombre de la propiedad.</param>
		  /// <param name="valor">Nuevo Valor.</param>
        public virtual void SetValue(object entity,string propiedad, object valor)
		  {
			  PropertyInfo prop = GetProperty(entity, propiedad);
           if (prop == null) return;
			  Type tProp = prop.PropertyType;
           if (tProp == null) return;
			  if(tProp.IsGenericType && tProp.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
			  {
				  if(valor == null)
				  {
					  prop.SetValue(entity, null, null);
					  return;
				  }
				  tProp = new NullableConverter(prop.PropertyType).UnderlyingType;
			  }
			  prop.SetValue(entity, Convert.ChangeType(valor, tProp), null);			  
			}		 
		  /// <summary>
		  /// Utilizando Reflection recuperamos el valor de una propiedad de la instancia de un objeto.
		  /// </summary>
		  /// <param name="entity">Instancia del objeto.</param>
		  /// <param name="propiedad">Nombre de la propiedad a recuperar.</param>
		  /// <returns>Valor de la propiedad.</returns>
        public static object GetValue(object entity, string propiedad)
		  {
			  PropertyInfo pi = GetProperty(entity, propiedad);
			  if(pi == null) return null;
			  return pi.GetValue(entity, null);
		  }
		
		  /// <summary>
        /// Convertimos un IDataReader a un IEntidad utilizando reflection. (Sobreescribir[override] este metodo para un mapeo personalizado)
		  /// </summary>
        /// <param name="drDatos">Objeto IDataReader.</param>
        /// <param name="entidad">Referencia a un IEntidad.</param>		
		  /// <param name="bValidaTabla">Especifica si se valida el patron [Tabla].[Nombre].</param>
        /// <returns>IEntidad con los valores del IDataReader</returns>
		  protected virtual IEntidad GetEntity(IDataReader drDatos, IEntidad entidad)
		  {
			  string Tabla;
			  string Campo;
			  string CampoReader;			  
			  for(int iCampo = 0; iCampo < drDatos.FieldCount; iCampo++)
			  {
				  Tabla = String.Empty;
				  Campo = String.Empty;
				  CampoReader = String.Empty;
				  try
				  {
					  CampoReader = drDatos.GetName(iCampo);
					 // if(!drDatos.IsDBNull(drDatos.GetOrdinal(CampoReader)))
					//  {
					  object valueReader = drDatos[iCampo];
					  if(drDatos[iCampo] is DBNull) valueReader = null;					  					     
					  this.SetValue(entidad, CampoReader, valueReader);
					 // }					  
				  }
				  catch(Exception)
				  {
					  //OnError(new Exception(String.Format("Error al asignar la propiedad[{0}],{1}", CampoReader, eInterno.Message)));					  
				  }
			  }
			  return entidad;
		  }				        
#endregion

#region StoredProcedure
		  protected virtual IEnumerable<IEntidad> FillEntidades(IDataReader drDatos,fnBuildEntity creaEntidad)
		  {
			  List<IEntidad> lstEntities = new List<IEntidad>();
			  try
			  {
				  while(drDatos.Read())
				  {
					  IEntidad entidad = creaEntidad();
					  lstEntities.Add(GetEntity(drDatos, entidad));
				  }
			  }
			  finally
			  {
				  if(drDatos != null)
				  {
					  if(!drDatos.IsClosed) drDatos.Close();
				  }
			  }
			  return lstEntities;
		  }
		  protected virtual IEnumerable<IEntidad> FillEntidades(IDataReader drDatos)
		  {			 
			  List<IEntidad> lstEntities = new List<IEntidad>();			  
			  try
			  {				  
				  while(drDatos.Read())
				  {
					  IEntidad entidad = BuildEntity();
					  lstEntities.Add(GetEntity(drDatos, entidad));
				  }
			  }
			  finally
			  {
				  if(drDatos != null)
				  {
					  if(!drDatos.IsClosed) drDatos.Close();
				  }
			  }
			  return lstEntities;
		  }
		  protected virtual void FillEntidades<Entity>(IPagina<Entity> pagina, IDataReader drDatos)where Entity:IEntidad,new()
		  {
			  List<Entity> lstEntities = new List<Entity>();
			  try
			  {
				  while(drDatos.Read())
				  {
					  Entity entidad = (Entity)BuildEntity();
					  if(pagina.Paginas <= 0)
					  {
						  pagina.Paginas = Convert.ToInt32(drDatos[DPaginaConst.CAMPO_PAGINAS].ToString());
					  }
					  lstEntities.Add((Entity)GetEntity(drDatos,entidad));
				  }
				  pagina.Contenido = lstEntities;
			  }
			  finally
			  {
				  if(drDatos != null)
				  {
					  if(!drDatos.IsClosed) drDatos.Close();
				  }
			  }			 
		  }		
		  /// <summary>
		  /// Construye el objeto command simple sin parametros del tipo SP[Stored Procedure].
		  /// </summary>
		  /// <param name="storedProcedureName">Nombre del SP.</param>
        protected virtual void _BuildCommandSP(string storedProcedureName)
		  {			  
              _BuildCommandSP(storedProcedureName, CommandType.StoredProcedure);
		  }
		  protected virtual void _BuildCommandSP(string Query,System.Data.CommandType Tipo)
		  {
				_Connection.Query = Query;
				_Connection.CreateCommand(Tipo);
		  }          
		  /// <summary>
		  /// Ejecuta el objeto Command del objeto IConexion, para obtener una Lista de Listas de Entidades.
		  /// </summary>
		  /// <param name="storedProcedureName">Nombre del Stored Procedure.</param>
        /// <param name="buildSP">Delegado [_BuildCommandCustom] el cual realiza el llamado al sp personalizado.</param>
        /// <param name="DataTemplate">Arreglo de IBase el cual genera la entidad a mapear.</param>
		  /// <returns></returns>
        protected virtual IEnumerable<IEntidad> _GetEntitiesFromCommand(string storedProcedureName,_BuildCommandCustom buildSP)
		  {
			  IEnumerable<IEntidad> resultado;
			  try
			  {
				  OpenConexion();
				  if(buildSP == null)
				  {
					  //Construimos el objeto command por Default.
                 _BuildCommandSP(storedProcedureName);
				  }
				  else
				  {
					  //Se ejecuta el Delegado personalizado.
                 buildSP();
				  }
				  resultado = FillEntidades(_Connection.GetReader());
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return resultado;
		  }
		  protected virtual IEnumerable<IEntidad> _GetEntitiesFromCommand(string storedProcedureName)
		  {
			  return _GetEntitiesFromCommand(storedProcedureName, null);
		  }        		 				          
#endregion

#region WHERE
		  public List<IPropsWhere> Where()
		  {
			  return _Where;
		  }
		  public virtual void AddWhere(string Nombre, string Condicion)
		  {
			  IPropsWhere prop = new CPropsWhere();
			  prop.NombreCampo = Nombre;
			  prop.Condicion = Condicion;
			  AddWhere(prop);
		  }
		  public virtual void AddWhere(string Nombre, string Condicion, object Valor)
		  {
			  IPropsWhere prop = new CPropsWhere();
			  prop.NombreCampo = Nombre;
			  prop.Condicion = Condicion;
			  prop.Valor = Valor;
			  AddWhere(prop);
		  }
		  public virtual void AddWhere(IPropsWhere propiedad)
		  {
			  if(_Where == null)
			  {
				  _Where = new List<IPropsWhere>();
			  }
			  CPropsWhere.AddPropList(_Where, propiedad);
		  }
		  public virtual void AddWhere(List<IPropsWhere> propiedades)
		  {
			  if(_Where == null)
			  {
				  _Where = new List<IPropsWhere>();
			  }
			  _Where = propiedades;
		  }
		  public virtual void ClearWhere()
		  {
			  _Where = new List<IPropsWhere>();
		  }
#endregion

#region SELECT
		  protected virtual string BuildCampos(string []camposOmitir,string[] camposSelect ,string separador)
		  {
			  StringBuilder sb = new StringBuilder();
			  string[] campos = null;
			  if(camposOmitir != null && camposOmitir.Count() > 0)
			  {
				  IEnumerable<string> camposNew = from tbl in camposSelect
															 where !camposOmitir.Contains(tbl)
															 select tbl;
				  if(camposNew != null && camposNew.Count() > 0)
				  {
					  campos = camposNew.ToArray();
				  }
			  }
			  else
			  {
				  campos = camposSelect;
			  }
			  for(int c = 0; c<campos.Length; c++)
			  {				 				 
					  sb.Append(String.Format("{0}.{1}",_Tabla, campos[c]));
					  if(c < campos.Length - 1)
					  {
						sb.Append(separador);
					  }				  
			  }
			  if(_camposJoin != null)
			  {
				  for(int c = 0; c < _camposJoin.Length; c++)
				  {
					  sb.Append(String.Format(",{0}", _camposJoin[c]));					  
				  }
			  }

			  return sb.ToString();
		  }
		  protected virtual string EvaluaWhere(IPropsWhere propiedad,IEntidad entidad)
		  {
				  string where= "";
				  object values = (!propiedad.Prioridad) ? DBase.GetValue(entidad, propiedad.NombreCampo) : propiedad.Valor;
				  string campo = propiedad.NombreCampo;
				  string NombreCampo = propiedad.NombreCampo;
				  if(!propiedad.OmiteConcatenarTabla)
				  {
					  campo = String.Format(" {0}.{1}", _Tabla, propiedad.NombreCampo);
				  }
				  else
				  {
					  NombreCampo = propiedad.RemoveDot();
				  }
				  switch (propiedad.Condicion.ToLower())
				  {
					  case DBase.CONTENGA:
						  where = String.Format(" {0} like '%{1}%' ", campo, values.ToString());
							  break;
					  case DBase.IN:
							  where = String.Format(" {0} in ({1}) ", campo, propiedad.Valor);
							  break;
					  case DBase.BETWEEN:
							  object[] valores = ((object[])propiedad.Valor);
							  where = String.Format(" {0}  between {1} and {2} ", campo,valores[0],valores[1]);
							  break;
					  default:
							  where = String.Format("{0} {1} @{2}", campo, propiedad.Condicion, NombreCampo);
						  break;
				  }
				  return where;
		  }
		  protected virtual string ToWhere(IPropsWhere propiedad)
		  {
			  string where = "";
			  object values =  propiedad.Valor;
			  string campo = propiedad.NombreCampo;
			  //string NombreCampo = propiedad.NombreCampo;
			  if(!propiedad.OmiteConcatenarTabla)
			  {
				  campo = String.Format(" {0}.{1}", _Tabla, propiedad.NombreCampo);
			  }
			 /*else
			  {
				  NombreCampo = propiedad.RemoveDot();
			  }*/
			  if(String.IsNullOrEmpty(propiedad.Plantilla))
			  {
				  propiedad.Plantilla = "";
			  }
			  switch(propiedad.Condicion.ToLower())
			  {
				  case DBase.CONTENGA:
					  where = String.Format(" {0} like '%{1}%' ", campo, values.ToString());
					  break;
				  case DBase.IN:
					  where = String.Format(" {0} in ({1}) ", campo, propiedad.Valor);
					  break;
				  default:
					  where = String.Format("{0} {1} {2}", campo, propiedad.Condicion,String.Format(propiedad.Plantilla, propiedad.Valor));
					  break;
			  }
			  return where;
		  }
		  protected virtual string BuildCamposSelect(IEntidad entity)		  
		  {
			  string[] campos = entity.GetAllCampos();
			  string[] camposOmitir = entity.GetOmitirCamposSelect();			  
			  if(campos == null)
			  {
				  campos = DBase.GetProperties(entity);
				  if(campos == null)
				  {
					  throw new Exception("No se implentado la funcionalidad campos SELECT");
				  }				 
			  }
			  return BuildCampos(camposOmitir, campos, ",");
		  }
		  protected virtual string BuildWhere(IEntidad entity)
		  {
			  StringBuilder SqlWhere = new StringBuilder("");			  
			  if(_Where != null)
			  {
				  if(_Where.Where(p => p.Condicion != DBase.WHERE_OMITIR).ToList().Count() > 0)
				  {
					  _Where = _Where.Where(p => p.Condicion != DBase.WHERE_OMITIR).ToList();
					  for(int c = 0; c < _Where.Count(); c++)
					  {
						  SqlWhere.Append(EvaluaWhere(_Where[c], entity));
						  if(c < _Where.Count() - 1)
						  {
							  if(String.IsNullOrEmpty(_Where[c].Operador))
							  {
								  SqlWhere.Append(" AND ");
							  }
							  else
							  {
								  SqlWhere.Append(String.Format(" {0} ", _Where[c].Operador));
							  }
						  }
					  }
					  SqlWhere = new StringBuilder(String.Format(" WHERE {0}", SqlWhere.ToString()));
				  }
			  }
			  return SqlWhere.ToString();
		  }
		  protected virtual void RunQuery(IEntidad entity, string query, IEnumerable<IPropsWhere> where)
		  {
			  OpenConexion();
			  _Connection.Query = query;
			  _Connection.CreateCommand(System.Data.CommandType.Text);
			  foreach(CPropsWhere propiedad in where)
			  {
				  string NombreCampo =propiedad.NombreCampo;
				  if(propiedad.OmiteConcatenarTabla)
				  {
					  NombreCampo = propiedad.RemoveDot();
				  }
				  if(!propiedad.Prioridad)
				  {
					  _Connection.AddParam(String.Format("@{0}", NombreCampo), entity.GetType().GetProperty(propiedad.NombreCampo).GetValue(entity));
				  }
				  else
				  {
					  if(!propiedad.OmiteParametro)
					  {
						  _Connection.AddParam(String.Format("@{0}", NombreCampo), propiedad.Valor);
					  }
				  }
			  }			   
		  }
		  protected virtual string BuildJoins()
		  {
			  StringBuilder sb = new StringBuilder();
			  if(_camposJoin != null)
			  {
				  foreach(string s in _condicionesJoin)
				  {
					  sb.AppendLine(s);
				  }
				  return String.Format(" {0} ", sb.ToString());
			  }
			  return String.Empty;
		  }
		  public virtual IEnumerable<IEntidad> Select(int top ,IEntidad entity) 
		  {
			  IEnumerable<IEntidad> result;
			  try
			  {
				  string SqlSelect = String.Format(" {0} {1} {2} FROM {3} ",DBase.SELECT,((top>0)? "TOP " + top.ToString():""), BuildCamposSelect(entity), this.Tabla);
				  string SqlJoins = BuildJoins();
				  if(!String.IsNullOrEmpty(SqlJoins))
				  {
					  SqlSelect += SqlJoins;
				  }
				  string SqlWhere = BuildWhere(entity);
				  string Sql = String.Format("{0} {1} {2}", SqlSelect.ToString(), SqlWhere.ToString(),GroupBy);
				  RunQuery(entity, Sql, _Where);
				  result = FillEntidades(_Connection.GetReader());
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return result;
		  }
		  public virtual IEnumerable<IEntidad> Select(IEntidad entity)
		  {
			  return this.Select(0, entity);
		  }
		  public virtual IPagina<Entity> Select<Entity>(IEntidad entity, string camposKey, int Pagina,int RegistrosPP)where Entity: IEntidad ,new()
		  {
			  IPagina<Entity> pagina = new DPagina<Entity>(Pagina, RegistrosPP);
			  try
			  {
				  string SqlJoins = BuildJoins();
				  string SqlWhere = String.Format("{0} {1}", BuildWhere(entity),GroupBy);
				  string SqlQuery = pagina.BuildQuery(this._Tabla + SqlJoins, camposKey, BuildCamposSelect(entity), SqlWhere);
				  RunQuery(entity, SqlQuery, _Where);
				  FillEntidades(pagina,_Connection.GetReader());
				  if(pagina.RegistrosPP <= 0) pagina.RegistrosPP =(pagina !=null)? pagina.Contenido.Count():0;
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return pagina;
		  }
		  public virtual IPagina<Entity> Select<Entity>(IEntidad entity, string camposKey, int Pagina) where Entity : IEntidad, new()
		  {
			  return Select<Entity>(entity, camposKey, Pagina, 0);
		  }	 
#endregion

#region GUARDAR
		  protected abstract void SaveSP(eCommit tipo, IEntidad Entity);
		  public virtual void Save(IEntidad IEntity)
		  {
			  try
			  {
				  if(IEntity == null) return;
				  OpenConexion();
				  SaveSP(eCommit.CommitFromSP, IEntity);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  //return IEntity;
		  }// Save ends.		
#endregion

		#region Funciones SQL
		  public virtual double fnConvierteCantidad(int IdMonedaOrigen, int IdMonedaDestino, double cantidad, double tipocambio)
		  {
			  double valor = 0;
			  try
			  {				  
				  OpenConexion();
				  _Connection.Query = String.Format("{0} dbo.fnConvierteCantidad({1},{2},{3},{4})",DBase.SELECT,IdMonedaOrigen,IdMonedaDestino,cantidad,tipocambio);
				  _Connection.CreateCommand(System.Data.CommandType.Text);
				  double.TryParse( _Connection.ExcuteScalar().ToString(), out valor);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return valor;
		  }
		  public virtual int fnGetIdPeso(int IdEmpresa)
		  {
			  int valor = 0;
			  try
			  {
				  OpenConexion();
				  _Connection.Query = String.Format("{0} dbo.fnGetIdPeso({1})", DBase.SELECT, IdEmpresa);
				  _Connection.CreateCommand(System.Data.CommandType.Text);
				  int.TryParse(_Connection.ExcuteScalar().ToString(), out valor);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return valor;
		  }
		  /// <summary>
		  /// Devuelve el ID del tipo de cambio de acuerdo a los parametros.
		  /// </summary>
		  /// <param name="IdEmpresa">Empresa.</param>
		  /// <param name="Fecha">Fecha.</param>
		  /// <param name="IdMonedaOrigen">Moneda origen del tipo de cambio.</param>
		  /// <param name="IdMonedaDestino">Moneda destino del tipo de cambio</param>
		  /// <returns>Id del tipo de cambio.[0 => Indica que no se ha registrado un tipo de cambio.]</returns>
		  public virtual int fnGetIdValorCambio(int IdEmpresa, DateTime Fecha, int IdMonedaOrigen, int IdMonedaDestino)
		  {
			  int valor = 0;
			  try
			  {
				  string _date = String.Format("{0}-{1}-{2}", Fecha.Year, Fecha.Month, Fecha.Day);
				  OpenConexion();
				  _Connection.Query = String.Format("{0} dbo.fnGetIdValorCambio({1},'{2}',{3},{4})", DBase.SELECT, IdEmpresa, _date, IdMonedaOrigen, IdMonedaDestino);
				  _Connection.CreateCommand(System.Data.CommandType.Text);
				  int.TryParse(_Connection.ExcuteScalar().ToString(), out valor);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return valor;
		  }
		  public virtual double fnGetValorCambio(int IdEmpresa, DateTime Fecha, int IdMonedaOrigen, int IdMonedaDestino)
		  {
			  double valor = 0;
			  try
			  {
				  string _date = String.Format("{0}-{1}-{2}", Fecha.Year, Fecha.Month, Fecha.Day);
				  OpenConexion();
				  _Connection.Query = String.Format("{0} dbo.fnGetValorCambio({1},'{2}',{3},{4})", DBase.SELECT, IdEmpresa, _date, IdMonedaOrigen, IdMonedaDestino);
				  _Connection.CreateCommand(System.Data.CommandType.Text);
				  double.TryParse(_Connection.ExcuteScalar().ToString(), out valor);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return valor;
		  }
		  public virtual double fnGetValorIva(int IdEmpresa, DateTime Fecha)
		  {
			  double valor = 0;
			  try
			  {
				  string _date = String.Format("{0}-{1}-{2}", Fecha.Year, Fecha.Month, Fecha.Day);
				  OpenConexion();
				  _Connection.Query = String.Format("{0} dbo.fnGetValorIva({1},'{2}')", DBase.SELECT, IdEmpresa, _date);
				  _Connection.CreateCommand(System.Data.CommandType.Text);
				  double.TryParse(_Connection.ExcuteScalar().ToString(), out valor);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return valor;
		  }
		 public virtual int fnGetIdValorIva(int IdEmpresa, DateTime Fecha)
		  {
			  int valor = 0;
			  try
			  {
				  string _date = String.Format("{0}-{1}-{2}", Fecha.Year, Fecha.Month, Fecha.Day);
				  OpenConexion();
				  _Connection.Query = String.Format("{0} dbo.fnGetIdValorIva({1},'{2}')", DBase.SELECT, IdEmpresa, _date);
				  _Connection.CreateCommand(System.Data.CommandType.Text);
				  int.TryParse(_Connection.ExcuteScalar().ToString(), out valor);
			  }
			  finally
			  {
				  CloseConnection();
			  }
			  return valor;
		  }
         public virtual int fnValidaPeriodoEstimacion(int IdContrato, DateTime Fecha)
         {
             int valor = 0;
             try
             {
                 string _date = String.Format("{0}-{1}-{2}", Fecha.Year, Fecha.Month, Fecha.Day);                 
                 OpenConexion();
                 _Connection.Query = String.Format("{0} dbo.fnValidaPeriodoEstimacion({1},'{2}')", DBase.SELECT, IdContrato, _date);
                 _Connection.CreateCommand(System.Data.CommandType.Text);
                 int.TryParse(_Connection.ExcuteScalar().ToString(), out valor);
             }
             finally
             {
                 CloseConnection();
             }
             return valor;
         }
		#endregion		
	}//DBase Clase ends.
}
