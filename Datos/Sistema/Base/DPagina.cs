﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
namespace Datos
{
	public class DPaginaConst
	{
		public const string PAGINACION_CATALOGOS_REGISTROS = "PAGINACION_CATALOGOS_REGISTROS";
		public const string CAMPO_PAGINA = "generadorPagina";
		public const string CAMPO_PAGINAS = "generadorPaginas";
		public const int REGISTROS_PAGINA_DEFAULT = 10;
	}
	public class DPagina<Entity> : IPagina<Entity> where Entity : IEntidad, new()
	{				
		protected int _Pagina;
		protected int _Paginas;
		protected int _RegistrosPP;
		protected IEnumerable<Entity> _Contenido;
		public DPagina()
		{
			_RegistrosPP = 0;
		}
		public DPagina(int pagina,int registrosPP)
		{
			_Pagina = pagina;
			_RegistrosPP = registrosPP;
		}
		public virtual int RegistrosPP
		{
			set
			{
				_RegistrosPP = value;
			}
			get
			{
				return _RegistrosPP;
			}
		}
		public virtual int Pagina
		{
			set
			{
				_Pagina = value;
			}
			get
			{
				return _Pagina;
			}
		}
		public virtual int Paginas
		{
			set
			{
				_Paginas = value;
			}
			get
			{
				return _Paginas;
			}
		}
		public virtual IEnumerable<Entity> Contenido
		{
			set
			{
				_Contenido = value;
			}
			get
			{
				return _Contenido;
			}
		}
		protected virtual string GetCampoPagina(string campoKey)
		{
			if(_RegistrosPP <= 0)
			{
				return String.Format(" ceiling((ROW_NUMBER() OVER (ORDER BY {0} ASC))/ " +
										  " isnull((SELECT CAST(VALOR AS float) FROM Parametros WHERE NOMBRE='{1}'),{2}))  AS {3} ",
										  campoKey,
										  DPaginaConst.PAGINACION_CATALOGOS_REGISTROS,
										  DPaginaConst.REGISTROS_PAGINA_DEFAULT.ToString() + ".0",
										  DPaginaConst.CAMPO_PAGINA);
			}
			else
			{
				return String.Format(" ceiling((ROW_NUMBER() OVER (ORDER BY {0} ASC))/ " +
									  "  AS {1} ",
									  campoKey,
									  _RegistrosPP.ToString() + ".0",
									  DPaginaConst.CAMPO_PAGINA);
			}
		}
		protected virtual string GetCampoPaginas(string table, string where)
		{
			if(_RegistrosPP <= 0)
			{
				return String.Format(" (select (count(*) / isnull((SELECT CAST(VALOR AS INT) FROM Parametros WHERE NOMBRE='{0}'),{1}))+1 " +
											" from  {2} {3}) as {4}",
											DPaginaConst.PAGINACION_CATALOGOS_REGISTROS,
											DPaginaConst.REGISTROS_PAGINA_DEFAULT,
											table,
											where,
											DPaginaConst.CAMPO_PAGINAS);
			}
			else
			{
				return String.Format(" (select (count(*) /{0})+1 " +
											" from  {1} {2}) as {3}",
											_RegistrosPP,											
											table,
											where,
											DPaginaConst.CAMPO_PAGINAS);
			}
		}
		protected virtual string GetQueryBody(string query)
		{
			if(_Pagina <= 0) _Pagina = 1;
			return String.Format(" SELECT * FROM ({0}) as VW WHERE VW.{1}={2}", query, DPaginaConst.CAMPO_PAGINA, _Pagina);
		}
		public virtual string BuildQuery(string table, string campoKey, string select, string where)
		{
			string campoPagina = this.GetCampoPagina(campoKey);
			string campoPaginas = this.GetCampoPaginas(table,where);
			return this.GetQueryBody(String.Format("SELECT {0},{1},{2} from {3} {4}",campoPagina,campoPaginas,select,table,where));		
		}

	}
}
