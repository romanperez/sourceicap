using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 21/12/2016 12:08:31 p. m.*/
namespace Datos
{

	public partial class DEmpresas : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_Empresas]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CEmpresas();
		}
		public DEmpresas()
			: base()
		{
			Inicializa();
		}
		public DEmpresas(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Empresas";
		}
		/// <summary>
		/// Procesa el objeto IEmpresas via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IEmpresas IEntity = Entity as IEmpresas;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@Empresa", IEntity.Empresa);
			_Connection.AddParam("@Direccion", IEntity.Direccion);
			_Connection.AddParam("@Telefono", IEntity.Telefono);
			_Connection.AddParam("@CorreoElectronico", IEntity.CorreoElectronico);
			_Connection.AddParam("@SitioWeb", IEntity.SitioWeb);
			_Connection.AddParam("@RFC", IEntity.RFC);
			_Connection.AddParam("@CodigoPostal", IEntity.CodigoPostal);
			_Connection.AddParam("@Logo", IEntity.Logo);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdEmpresa = Convert.ToInt32(_Connection.GetParam("@IdEmpresa").ToString());
		}
	}//  DEmpresas ends.
}// LibDatos.Entidades ends.
