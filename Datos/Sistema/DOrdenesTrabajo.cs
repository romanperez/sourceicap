using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:13 p. m.*/
namespace Datos
{

	public partial class DOrdenesTrabajo : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_OrdenesTrabajo]";
		private const string SP_INDEX_ORDENES_TRABAJO = "[dbo].[prc_get_index_ordenesTrabajo]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new COrdenesTrabajo();
		}
		public DOrdenesTrabajo()
			: base()
		{
			Inicializa();
		}
		public DOrdenesTrabajo(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "OrdenesTrabajo";
		}
		/// <summary>
		/// Procesa el objeto IOrdenesTrabajo via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IOrdenesTrabajo IEntity = Entity as IOrdenesTrabajo;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdOrdenTrabajo", IEntity.IdOrdenTrabajo);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@IdProyecto", IEntity.IdProyecto);
			_Connection.AddParam("@Comentarios", IEntity.Comentarios);
			_Connection.AddParam("@ComentariosCliente", IEntity.ComentariosCliente);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			//_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@IdActividad", IEntity.IdActividad);
			_Connection.AddParam("@Folio", IEntity.Folio);
			_Connection.AddParam("@Ubicacion", IEntity.Ubicacion);
			_Connection.AddParam("@FechaInicio", IEntity.FechaInicio);
			_Connection.AddParam("@FechaFin", IEntity.FechaFin);
			_Connection.AddParam("@IdEstatus", IEntity.IdEstatus);
			_Connection.AddParam("@IdPrioridad", IEntity.IdPrioridad);
			_Connection.AddParam("@IdTipo", IEntity.IdTipo);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdOrdenTrabajo = Convert.ToInt32(_Connection.GetParam("@IdOrdenTrabajo").ToString());
			if(this.Delete != 1)
			{
				SavePersonalCliente(IEntity);
			}
			if(this.Delete != 1)
			{
				SaveContratos(IEntity);
			}
			if(this.Delete != 1)
			{
				SavePersonal(IEntity);
			}
			if(this.Delete != 1)
			{
				SaveDiagramas(IEntity);
			}
			if(this.Delete != 1)
			{
				SaveDetallesInstalacion(IEntity);
			}
			if(this.Delete != 1)
			{
				SaveOTConceptos(IEntity);
			}	
		}
		protected virtual void SavePersonalCliente(IOrdenesTrabajo ot)
		{			
			DOTPersonalCliente pc = new DOTPersonalCliente();
			if(ot.PersonalClienteAtencion != null && ot.PersonalClienteAtencion.Any())
			{

				ot.PersonalClienteAtencion.ForEach(x =>
				{
					if(x != null)
					{

						x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
						if(x.IdOTPersonalCliente < 0)
						{
							x.IdOTPersonalCliente = x.IdOTPersonalCliente * -1;
							pc.Delete = 1;
						}
						pc.Save(x);
					}
				});
			}
			if(ot.PersonalClienteContacto != null && ot.PersonalClienteContacto.Any())
			{
				ot.PersonalClienteContacto.ForEach(x =>
				{
					if(x != null)
					{
						x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
						if(x.IdOTPersonalCliente < 0)
						{
							x.IdOTPersonalCliente = x.IdOTPersonalCliente * -1;
							pc.Delete = 1;
						}
						pc.Save(x);
					}
				});
			}
		}
		protected virtual void SaveContratos(IOrdenesTrabajo ot)
		{
			if(ot.Contratos == null || ot.Contratos.Count<=0) return;
			if(ot.Contratos.Count >1) return;			
			DOTContratos contrato = new DOTContratos();
			ot.Contratos.ForEach(x =>
			{
				if(x != null)
				{
					x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
					if(x.IdContratoOrden < 0)
					{
						x.IdContratoOrden = x.IdContratoOrden * -1;
						contrato.Delete = 1;
					}
					contrato.Save(x);
				}
			});						
		}
		protected virtual void SavePersonal(IOrdenesTrabajo ot)
		{
			if(ot.OTPersonal == null || ot.OTPersonal.Count<=0) return;
			DOTPersonal personal = new DOTPersonal();			
			ot.OTPersonal.ForEach(x =>
			{
				if(x != null)
				{
					x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
					if(x.IdOTPersonal < 0)
					{
						x.IdOTPersonal = x.IdOTPersonal * -1;
						personal.Delete = 1;
					}
					personal.Save(x);
				}
			});
		}
		protected virtual void SaveDiagramas(IOrdenesTrabajo ot)
		{
			if(ot.OTDiagramas == null || ot.OTDiagramas.Count<=0) return;
			DOTDocumentos diagrama = new DOTDocumentos();
			ot.OTDiagramas.ForEach(x =>
			{
				if(x != null)
				{
					x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
					if(x.IdOTDocumentos < 0)
					{
						x.IdOTDocumentos = x.IdOTDocumentos * -1;
						diagrama.Delete = 1;
					}
					diagrama.Save(x);
				}
			});
		}
		protected virtual void SaveDetallesInstalacion(IOrdenesTrabajo ot)
		{
			if(ot.OTDetalleInstalacion == null || ot.OTDetalleInstalacion.Count<=0) return;
			DOTDocumentos detallesInstalacion = new DOTDocumentos();
			ot.OTDetalleInstalacion.ForEach(x =>
			{
				if(x != null)
				{
					x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
					if(x.IdOTDocumentos < 0)
					{
						x.IdOTDocumentos = x.IdOTDocumentos * -1;
						detallesInstalacion.Delete = 1;
					}
					detallesInstalacion.Save(x);
				}
			});
		}
		protected virtual void  SaveOTConceptos(IOrdenesTrabajo ot)
		{			
			if(ot.OTConceptos == null || ot.OTConceptos.Count<=0) return;
			DOTConceptos otConceptos = new DOTConceptos();
			ot.OTConceptos.ForEach(x =>
			{
				if(x != null)
				{
					x.IdOrdenTrabajo = ot.IdOrdenTrabajo;
					if(x.IdOTConcepto < 0)
					{
						x.IdOTConcepto = x.IdOTConcepto * -1;
						otConceptos.Delete = 1;
					}
					otConceptos.Save(x);
				}
			});
		}
		public virtual IPagina<Entity> IndexOrdenesTrabajo<Entity>(int Pagina) where Entity : IEntidad, new()
		{
			IPagina<Entity> pagina = new DPagina<Entity>(Pagina, 0);
			try
			{
				IPropsWhere empresa = _Where.Where(a => a.NombreCampo == "Empresas.IdEmpresa").FirstOrDefault();
				IPropsWhere unidad = _Where.Where(a => a.NombreCampo == "Unidades.IdUnidad").FirstOrDefault();
				OpenConexion();
				_Connection.Query = SP_INDEX_ORDENES_TRABAJO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdPagina", Pagina);
				_Connection.AddParam("@IdEmpresa", Convert.ToInt32(empresa.Valor));
				if(unidad != null)
				{
					_Connection.AddParam("@IdUnidad", Convert.ToInt32(unidad.Valor));
				}
				FillEntidades(pagina, _Connection.GetReader());
				if(pagina.RegistrosPP <= 0) pagina.RegistrosPP = (pagina != null) ? pagina.Contenido.Count() : 0;
			}
			finally
			{
				CloseConnection();
			}
			return pagina;
		}
	}//  DOrdenesTrabajo ends.
}// LibDatos.Entidades ends.
