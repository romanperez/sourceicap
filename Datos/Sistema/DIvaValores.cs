using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:12 p. m.*/
namespace Datos
{

	public partial class DIvaValores : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_IvaValores]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CIvaValores();
		}
		public DIvaValores()
			: base()
		{
			Inicializa();
		}
		public DIvaValores(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "IvaValores";
		}
		/// <summary>
		/// Procesa el objeto IIvaValores via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IIvaValores IEntity = Entity as IIvaValores;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdIva", IEntity.IdIva);
			_Connection.AddParam("@IdEmpresa", IEntity.IdEmpresa);
			_Connection.AddParam("@Porcentaje", IEntity.Porcentaje);
			_Connection.AddParam("@FechaInicio", IEntity.FechaInicio);
			_Connection.AddParam("@FechaFin", IEntity.FechaFin);
			_Connection.AddParam("@Estatus", IEntity.Estatus);

			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdIva = Convert.ToInt32(_Connection.GetParam("@IdIva").ToString());

		}
	}//  DIvaValores ends.
}// LibDatos.Entidades ends.
