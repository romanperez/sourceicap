using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 08/12/2016 05:26:51 p. m.*/
namespace Datos
{
    
    public partial class   DPerfiles:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_Perfiles]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CPerfiles();
	}		
        public DPerfiles():base()
	{
	  Inicializa();
	}
	public DPerfiles(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "Perfiles";
	} 
	/// <summary>
        /// Procesa el objeto IPerfiles via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IPerfiles IEntity = Entity as IPerfiles;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdPerfil",IEntity.IdPerfil); 
				 _Connection.AddParam("@Nombre", IEntity.Nombre);
				 _Connection.AddParam("@Descripcion", IEntity.Descripcion);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdPerfil = Convert.ToInt32( _Connection.GetParam("@IdPerfil").ToString());
 			
	}	
    }//  DPerfiles ends.
}// LibDatos.Entidades ends.
