using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 04/01/2017 10:37:35 a. m.*/
namespace Datos
{
    
    public partial class   DPerfilMenus:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_PerfilMenus]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CPerfilMenus();
	}		
        public DPerfilMenus():base()
	{
	  Inicializa();
	}
	public DPerfilMenus(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "PerfilMenus";
	} 
	/// <summary>
        /// Procesa el objeto IPerfilMenus via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IPerfilMenus IEntity = Entity as IPerfilMenus;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdPerfilModulo",IEntity.IdPerfilModulo); 
				 _Connection.AddParam("@IdPerfil", IEntity.IdPerfil);
				 _Connection.AddParam("@IdMenu", IEntity.IdMenu);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdPerfilModulo = Convert.ToInt32( _Connection.GetParam("@IdPerfilModulo").ToString());
 			
	}	
    }//  DPerfilMenus ends.
}// LibDatos.Entidades ends.
