using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 30/03/2017 03:46:57 p. m.*/
namespace Datos
{

	public partial class DContratosDetallesInsumos2 : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_ContratosDetallesInsumos2]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CContratosDetallesInsumos2();
		}
		public DContratosDetallesInsumos2()
			: base()
		{
			Inicializa();
		}
		public DContratosDetallesInsumos2(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected virtual void Inicializa()
		{
			_Tabla = "ContratosDetallesInsumos2";
		}
		/// <summary>
		/// Procesa el objeto IContratosDetallesInsumos2 via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IContratosDetallesInsumos2 IEntity = Entity as IContratosDetallesInsumos2;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdInsumoContrato", IEntity.IdInsumoContrato);
			_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			_Connection.AddParam("@IdContratosDetalles", IEntity.IdContratosDetalles);			 
			_Connection.AddParam("@IdConcepto", IEntity.IdConcepto);
			_Connection.AddParam("@IdInsumo", IEntity.IdInsumo);
			_Connection.AddParam("@Cantidad", IEntity.Cantidad);
			_Connection.AddParam("@Costo", IEntity.Costo);
			_Connection.AddParam("@IdPedido", IEntity.IdPedido);
			_Connection.AddParam("@Movimiento", IEntity.Movimiento);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdInsumoContrato = Convert.ToInt32(_Connection.GetParam("@IdInsumoContrato").ToString());
		}
	}//  DContratosDetallesInsumos2 ends.
	public class DvwContratosDetallesInsumos2 : DContratosDetallesInsumos2
	{
		public const string VIEW_CONTRATOS_DETLLES_INSUMOS_2 = "vwContratosDetallesInsumos2";
		public DvwContratosDetallesInsumos2()
			: base()
		{
			Inicializa();
		}
		protected override void Inicializa()
		{
			_Tabla = DvwContratosDetallesInsumos2.VIEW_CONTRATOS_DETLLES_INSUMOS_2;
		}
		public override IEntidad BuildEntity()
		{
			return new CvwContratosDetallesInsumos2();
		}
	}
}// LibDatos.Entidades ends.
