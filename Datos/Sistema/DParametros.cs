using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:14 p. m.*/
namespace Datos
{
    
    public partial class   DParametros:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_Parametros]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CParametros();
	}		
        public DParametros():base()
	{
	  Inicializa();
	}
	public DParametros(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "Parametros";
	} 
	/// <summary>
        /// Procesa el objeto IParametros via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IParametros IEntity = Entity as IParametros;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdParametro",IEntity.IdParametro); 
				 _Connection.AddParam("@Nombre", IEntity.Nombre);
				 _Connection.AddParam("@Valor", IEntity.Valor);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);
				 _Connection.AddParam("@Descripcion", IEntity.Descripcion);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdParametro = Convert.ToInt32( _Connection.GetParam("@IdParametro").ToString());
 			
	}	
    }//  DParametros ends.
}// LibDatos.Entidades ends.
