using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 26/04/2017 01:51:08 p. m.*/
namespace Datos
{
    
    public partial class   DOTContratos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_OTContratos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new COTContratos();
	}		
        public DOTContratos():base()
	{
	  Inicializa();
	}
	public DOTContratos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "OTContratos";
	} 
	/// <summary>
        /// Procesa el objeto IOTContratos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IOTContratos IEntity = Entity as IOTContratos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdContratoOrden",IEntity.IdContratoOrden); 
				 _Connection.AddParam("@IdOrdenTrabajo", IEntity.IdOrdenTrabajo);
				 _Connection.AddParam("@IdContrato", IEntity.IdContrato);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdContratoOrden = Convert.ToInt32( _Connection.GetParam("@IdContratoOrden").ToString());
 			
	}	
    }//  DOTContratos ends.
}// LibDatos.Entidades ends.
