using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:14 p. m.*/
namespace Datos
{
	public partial class DPedidos : DBase
	{
		protected const string SP_SAVE = "[dbo].[prc_Pedidos]";
		protected const string PEDIDOS_BY_PROYECTO = "[dbo].[prc_PedidosByProyecto]";
		protected const string PEDIDOS_BY_PROYECTO_MAIL = "[dbo].[prc_PedidosByProyectoMail]";
		protected const string EXISTENCIAS_VS_PEDIDO = "[dbo].[prc_existencias_vs_PedidoByProyecto]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new CPedidos();
		}
		public DPedidos()
			: base()
		{
			Inicializa();
		}
		public DPedidos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "Pedidos";
		}
		/// <summary>
		/// Procesa el objeto IPedidos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IPedidos IEntity = Entity as IPedidos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdPedido", IEntity.IdPedido);
			_Connection.AddParam("@IdUnidad", IEntity.IdUnidad);
			_Connection.AddParam("@IdProyecto", IEntity.IdProyecto);
			_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			//_Connection.AddParam("@IdContrato", IEntity.IdContrato);
			//_Connection.AddParam("@Pedido", IEntity.Pedido);
			//_Connection.AddParam("@Descripcion", IEntity.Descripcion);
			_Connection.AddParam("@FechaSolicitado", IEntity.FechaSolicitado);
			_Connection.AddParam("@FechaLlegada", IEntity.FechaLlegada);
			_Connection.AddParam("@Estatus", IEntity.Estatus);
			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdPedido = Convert.ToInt32(_Connection.GetParam("@IdPedido").ToString());
			if(this.Delete != 1)
			{
				SaveDetail(IEntity);
			}
		}
		protected virtual void SaveDetail(IEntidad Entity)
		{
			if(Entity == null) return;
			IPedidos pedido = Entity as IPedidos;
			if(pedido.Detalle != null && pedido.Detalle.Count() > 0)
			{
				foreach(IPedidosDetalles detail in pedido.Detalle)
				{
					DPedidosDetalles _ddetail = new DPedidosDetalles(_Connection);
					detail.IdPedido = pedido.IdPedido;
					if(detail.IdPedidoDetalle < 0)
					{
						detail.IdPedidoDetalle = detail.IdPedidoDetalle * -1;
						_ddetail.Delete = 1;
					}
					_ddetail.Save(detail);
				}
			}
		}
		public IEnumerable<IPedidos> PedidosByProyecto(IProyectos proyecto)
		{
			try
			{
				if(proyecto == null) return null;
				int data = 0;
				List<CPedidos> pedidos = new List<CPedidos>();
				List<CPedidosDetalles> insumos = new List<CPedidosDetalles>();
				OpenConexion();
				_Connection.Query = DPedidos.PEDIDOS_BY_PROYECTO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", proyecto.IdProyecto);
				IDataReader drDatos = _Connection.GetReader();
				while(_Connection.HasRows(drDatos) || data < 2)
				{
					while(drDatos.Read())
					{
						switch(data)
						{
							case 0:
								pedidos.Add(GetEntity(drDatos, new CPedidos()) as CPedidos);
								break;
							case 1:
								insumos.Add(GetEntity(drDatos, new CPedidosDetalles()) as CPedidosDetalles);
								break;
						}//switch ends.
					}//while(drDatos.Read()) ends.
					data++;
					drDatos.NextResult();
				}//while(_Connection.HasRows(drDatos) || data < 2) ends.
				if(pedidos != null && pedidos.Count() > 0)
				{
					if(insumos != null && insumos.Count() > 0)
					{
						pedidos.ForEach(p =>
						{
							IEnumerable<CPedidosDetalles> res = insumos.Where(i => i.IdPedido == p.IdPedido);
							if(res != null && res.Count() > 0)
							{
								p.Detalle = res.ToList();
							}
						});
					}
				}//if ends.
				return pedidos;
			}
			finally
			{
				CloseConnection();
			}
		}
		public IEnumerable<IPedidosDetalles> PedidosByProyectoMail(IProyectos proyecto)
		{
			try
			{
				if(proyecto == null || proyecto.IdProyecto<=0) return null;				
				List<CPedidosDetalles> insumos = new List<CPedidosDetalles>();
				OpenConexion();
				_Connection.Query = DPedidos.PEDIDOS_BY_PROYECTO_MAIL;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto", proyecto.IdProyecto);
				IDataReader drDatos = _Connection.GetReader();
				while(drDatos.Read())
				{
				  insumos.Add(GetEntity(drDatos, new CPedidosDetalles()) as CPedidosDetalles);
				}//while(drDatos.Read()) ends.
				return insumos;			
			}
			finally
			{
				CloseConnection();
			}
		}
		public IEnumerable<IPedidosDetalles> ExistenciasVSPedidoByProyecto(int IdProyecto,int IdAlmacen)
		{
			try
			{
				if(IdProyecto <= 0 || IdAlmacen<=0) return null;
				List<CPedidosDetalles> insumos = new List<CPedidosDetalles>();
				OpenConexion();
				_Connection.Query = DPedidos.EXISTENCIAS_VS_PEDIDO;
				_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
				_Connection.AddParam("@IdProyecto",IdProyecto);
				_Connection.AddParam("@IdAlmacen", IdAlmacen);
				IDataReader drDatos = _Connection.GetReader();
				while(drDatos.Read())
				{
					insumos.Add(GetEntity(drDatos, new CPedidosDetalles()) as CPedidosDetalles);
				}//while(drDatos.Read()) ends.
				return insumos;
			}
			finally
			{
				CloseConnection();
			}
		}
	}//  DPedidos ends.
}// LibDatos.Entidades ends.