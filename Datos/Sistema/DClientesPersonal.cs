using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:09 p. m.*/
namespace Datos
{
    
    public partial class   DClientesPersonal:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_ClientesPersonal]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CClientesPersonal();
	}		
        public DClientesPersonal():base()
	{
	  Inicializa();
	}
	public DClientesPersonal(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "ClientesPersonal";
	} 
	/// <summary>
        /// Procesa el objeto IClientesPersonal via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IClientesPersonal IEntity = Entity as IClientesPersonal;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdPersonalCliente",IEntity.IdPersonalCliente); 
				 _Connection.AddParam("@IdCliente", IEntity.IdCliente);
				 _Connection.AddParam("@Nombre", IEntity.Nombre);
				 _Connection.AddParam("@ApellidoPaterno", IEntity.ApellidoPaterno);
				 _Connection.AddParam("@ApellidoMaterno", IEntity.ApellidoMaterno);
				 _Connection.AddParam("@Puesto", IEntity.Puesto);
				 _Connection.AddParam("@Telefono", IEntity.Telefono);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdPersonalCliente = Convert.ToInt32( _Connection.GetParam("@IdPersonalCliente").ToString());
 			
	}	
    }//  DClientesPersonal ends.
}// LibDatos.Entidades ends.
