using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 15/11/2016 04:41:13 p. m.*/
namespace Datos
{

	public partial class DOTDocumentos : DBase
	{
		private const string SP_SAVE = "[dbo].[prc_OTDocumentos]";
		public override Entidades.IEntidad BuildEntity()
		{
			return new COTDocumentos();
		}
		public DOTDocumentos()
			: base()
		{
			Inicializa();
		}
		public DOTDocumentos(IConexion cnn)
			: base(cnn)
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Tabla = "OTDocumentos";
		}
		/// <summary>
		/// Procesa el objeto IOTDocumentos via stored procedure.
		/// </summary>
		/// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
		/// <param name="IEntity">Objeto IEntity con los cambios.</param>   
		protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
		{
			IOTDocumentos IEntity = Entity as IOTDocumentos;
			_Connection.Query = SP_SAVE;
			_Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdOTDocumentos", IEntity.IdOTDocumentos);
			_Connection.AddParam("@IdOrdenTrabajo", IEntity.IdOrdenTrabajo);
			_Connection.AddParam("@IdTipoDocumento", IEntity.IdTipoDocumento);
			_Connection.AddParam("@RutaDocumento", IEntity.RutaDocumento);
			_Connection.AddParam("@Estatus", IEntity.Estatus);

			_Connection.AddParam("@Delete_Entity", this.Delete);
			_Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));
			_Connection.NonQuery();
			IEntity.IdOTDocumentos = Convert.ToInt32(_Connection.GetParam("@IdOTDocumentos").ToString());

		}
	}//  DOTDocumentos ends.
}// LibDatos.Entidades ends.
