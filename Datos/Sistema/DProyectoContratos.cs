using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
/*Capa Datos 30/03/2017 03:46:57 p. m.*/
namespace Datos
{
    
    public partial class   DProyectoContratos:DBase
    {                
        private const string SP_SAVE = "[dbo].[prc_ProyectoContratos]";
	public override Entidades.IEntidad BuildEntity()
	{
	  return new CProyectoContratos();
	}		
        public DProyectoContratos():base()
	{
	  Inicializa();
	}
	public DProyectoContratos(IConexion cnn): base(cnn)
	{
	   Inicializa();
	}
	protected void Inicializa()
	{
	  _Tabla = "ProyectoContratos";
	} 
	/// <summary>
        /// Procesa el objeto IProyectoContratos via stored procedure.
        /// </summary>
        /// <param name="commit">Especifica si el commit lo produce el SP o bien desde NET.</param>
        /// <param name="IEntity">Objeto IEntity con los cambios.</param>   
	protected override void SaveSP(DBase.eCommit tipo, IEntidad Entity)
	{
	  IProyectoContratos IEntity = Entity as IProyectoContratos;
	  _Connection.Query = SP_SAVE;
	  _Connection.CreateCommand(System.Data.CommandType.StoredProcedure);
	  			_Connection.AddParam(SqlDbType.Int, ParameterDirection.InputOutput, "@IdProyectoContrato",IEntity.IdProyectoContrato); 
				 _Connection.AddParam("@IdProyecto", IEntity.IdProyecto);
				 _Connection.AddParam("@IdContrato", IEntity.IdContrato);
				 _Connection.AddParam("@Estatus", IEntity.Estatus);

	  _Connection.AddParam("@Delete_Entity", this.Delete);
	  _Connection.AddParam("@AutoCommit", Convert.ToInt32(tipo));	  
	  _Connection.NonQuery();
	  IEntity.IdProyectoContrato = Convert.ToInt32( _Connection.GetParam("@IdProyectoContrato").ToString());
 			
	}	
    }//  DProyectoContratos ends.
}// LibDatos.Entidades ends.
