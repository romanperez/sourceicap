using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using Entidades;
/*LAVM 01/09/2015 01:26:04 p.m.*/
namespace Datos
{
    public abstract partial class DConexion : IConexion
    {
        protected IDbConnection _connection;
        protected IDbCommand _command;
        protected IDbTransaction _transaccion;
        protected string _sql;
        protected string _pathFileConfig;
        protected string _conectionString;
        protected bool _hayTransaccion;
        public string Query
        {
            set { _sql = value; }
            get { return _sql; }
        }
        public string ConnectionString
        {
            set { _conectionString = value; }
            get { return _conectionString; }
        }
        private void BuildConnectionString(string ConnectionString)
        {
            if (String.IsNullOrEmpty(ConnectionString))
            {
                 //Entidades.CTools tool = new Entidades.CTools();
                string tipoCNN = ConfigurationManager.AppSettings["DataBaseUse"].ToString();
                // _conectionString = tool.BsdDencode(ConfigurationManager.ConnectionStrings[tipoCNN].ConnectionString.ToString());	
                _conectionString =ConfigurationManager.ConnectionStrings[tipoCNN].ConnectionString.ToString();	
                
            }
            else
            {
                _conectionString = ConnectionString;
            }

        }

        public static void SaveProperties(string servidor, string db, string user, string pass)
        {

        }


        protected DConexion(string ConnectionString)
        {
            BuildConnectionString(ConnectionString);
            _hayTransaccion = false;
        }
        protected string OnError(Exception eConexion)
        {
            throw eConexion;
        }
        public IDataReader GetReader()
        {
            return _command.ExecuteReader();
        }
        public abstract object GetParam(string Name);
        public abstract void NonQuery();
        public virtual void CreateCommand(System.Data.CommandType tipo)
        {
            _command.Connection = this._connection;
            _command.CommandText = Query;
            _command.CommandType = tipo;
            if (_hayTransaccion)
            {
                if (_transaccion != null) { _command.Transaction = _transaccion; }
            }
        }
        public void ChangeQueryCommand()
        {
            _command.CommandText = Query;
        }
        public abstract void AddParam(string Name, object Value);
        public virtual void AddParam(IDataParameter Parametro)
        {
            _command.Parameters.Add(Parametro);
        }
        public abstract void AddParam(object Type, System.Data.ParameterDirection Direction, string Name, object Value);
        public abstract void AddParam(object Type, System.Data.ParameterDirection Direction, string Name);
		  public abstract void AddParam(object Type, int longitud, System.Data.ParameterDirection Direction, string Name);
		  public abstract void AddParam(object Type, int longitud, System.Data.ParameterDirection Direction, string Name, object Value); 
        public void CloseConection()
        {
            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }
            if (_command != null)
            {
                _command.Dispose();
            }
        }
        public List<data> GetListEntities<data>()
        {
            IDataReader dr = null;
            List<data> lstResults = new List<data>();
            try
            {
                dr = GetReader();
                while (dr.Read())
                {
                    data objetoAux = Activator.CreateInstance<data>();
                    string propReader = String.Empty;
                    for (int c = 0; c < dr.FieldCount; c++)
                    {
                        propReader = dr.GetName(c);
                        IEnumerable<PropertyInfo> propR = from tbl in objetoAux.GetType().GetProperties()
                                                          where tbl.Name.ToLower() == propReader.ToLower()
                                                          select tbl;
                        if (propR.Count() > 0)
                        {
                            propR.First().SetValue(objetoAux, dr[propReader], null);
                        }
                    }
                    if (objetoAux != null)
                    {
                        lstResults.Add(objetoAux);
                    }
                }
            }
            finally
            {
                if (dr != null)
                {
                    if (!dr.IsClosed) { dr.Close(); }
                }
            }//finally ends            
            return lstResults;
        }//GetListEntities ends.
        public void BeginTran()
        {
            _transaccion = _connection.BeginTransaction();
            _hayTransaccion = true;
        }
        public void CommitTran() { _transaccion.Commit(); }
        public void RollBackTran()
        {
            try { _transaccion.Rollback(); }
            catch (Exception eRollBack) { OnError(eRollBack); }
        }
        public string DBName()
        {
            if (_connection != null)
            {
                return _connection.Database;
            }
            else
            {
                return "Sin conexion";
            }
        }
        public ConnectionState Status()
        {
            return _connection.State;
        }
        public abstract bool HasRows(IDataReader reader);
		  public virtual object ExcuteScalar()
		  {
			  return _command.ExecuteScalar();
		  }

    }
}

