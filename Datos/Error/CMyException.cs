using System;
using System.Collections.Generic;
using System.Diagnostics;
using Entidades;
namespace  Datos
{
	public class CMyException : IMyException
    {
        public int IdError { set; get; }        
        public int Columna { set; get; }
        public int Linea { set; get; }
        public string Pila { set; get; }
        public string Funcion { set; get; }
        public string Archivo { set; get; }
        public string Error { set; get; }
		  public string Info
		  {
			  set;
			  get;
		  }              
        public CMyException()
        {
           
        }               
        public string InfoToString()
        {
			  return String.Format("Error:[{0}],Pila[{1}]", Error, Pila); 			
        }
        /// <summary>
        /// Obtiene los detalles de la excepción línea del error,archivo que provocó el error, función que generó el error, etc.
        /// IMPORTANTE: Debe existir un archivo PDB asociado al assembly que provoca la excepción.
        /// </summary>
        /// <param name="ErrorEvaluar">Variable del tipo Exception a la cual se quiere conocer su detalle.</param>
        /// <param name="Procedimiento">Nombre del procedimiento que genera la excepción.</param>
        private void GetDetalleError(Exception ErrorEvaluar)
        {
            const string NO_DISPONIBLE = "No disponible.";            
            try
            {
                StackTrace SeguimientoPila = new StackTrace(ErrorEvaluar, true);
                if (SeguimientoPila != null)
                {
                    StackFrame frPila = SeguimientoPila.GetFrame(0);
                    if (frPila != null)
                    {
                        Archivo = isEmpty(frPila.GetFileName(), NO_DISPONIBLE);
                        Linea = Convert.ToInt32(frPila.GetFileLineNumber());
                        Columna =  Convert.ToInt32(frPila.GetFileColumnNumber());
                        Funcion = isEmpty(frPila.GetMethod().Name, NO_DISPONIBLE);
                        Error = ErrorEvaluar.Message;
                        Pila = ErrorEvaluar.StackTrace;
                    }//if ends.
                    else
                    {
                        Default(ErrorEvaluar, NO_DISPONIBLE);
                    }
                }//if ends.
                else
                {
                    Default(ErrorEvaluar, NO_DISPONIBLE);
                }
            }//try ends.
            catch (Exception eDetailError)
            {                
                Default(ErrorEvaluar, NO_DISPONIBLE);
                Error = Error + " Err detalles excep [" +eDetailError.Message + "]";
            }//catch ends.            
        }
        private string isEmpty(string cadena, string sdefault)
        {
            return (string.IsNullOrWhiteSpace(cadena) ? sdefault : cadena);
        }
        private void Default(Exception eDefault, string NoDisponible)
        {           
            Archivo = NoDisponible;
            Linea = 0;
            Columna = 0;
            Funcion = NoDisponible;
            Error = eDefault.Message;
            Pila = eDefault.StackTrace;
        }        
        public static CMyException ExceptionToError(Exception eInterno)
        {
            CMyException log = new CMyException();
            Exception eAux = eInterno.InnerException;
            log.GetDetalleError(eInterno);
            if (eInterno.InnerException != null)
            {
					log.Info = CMyException.ExceptionToError(eInterno.InnerException).InfoToString();
            }            
            return log;
        }
    }
}// namespace Datos.Entidades ends.
