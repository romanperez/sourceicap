using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Entidades;
using Idiomas;
using System.Configuration;
namespace  Datos
{
	public class DError : IError
    {
		 public int IdError{set;get;}
		 public string Error{set;get;} 		 
       public DError(){}		 
	   public void SaveLog(Exception error)
       {            
			string LabelErr=String.Empty;
			try
			{
				string sNewLog = string.Empty;
				CMyException log = CMyException.ExceptionToError(error);
				SaveDataBase(log);
			}
			catch(Exception er)
			{				
				try
				{
					LabelErr = IcapWeb.DErrorSaveLogBaseDatos + " {0} ";
				}
				catch(Exception){LabelErr="Existe un problema al intentar conectarse a la base de datos. {0}";}
				throw new Exception(String.Format(LabelErr, er.Message));
			}    
        } //SaveLog ends.                            
        protected void SaveDataBase (CMyException log)
        {
			  IConexion cnn = null;
				try
				{
					cnn = new CSqlServer();
					cnn.Query = "[dbo].[PRC_SAVE_LOG_ERROR]";
					cnn.CreateCommand(System.Data.CommandType.StoredProcedure);
					cnn.AddParam(System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, "@Log_Error_Id");
					cnn.AddParam("@Log_Archivo", log.Archivo);
					cnn.AddParam("@Log_Funcion", log.Funcion);
					cnn.AddParam("@Log_Linea", log.Linea);
					cnn.AddParam("@Log_Columna", log.Columna);
					cnn.AddParam("@Log_Pila", log.Pila);
					cnn.AddParam("@Log_Error", log.Error);
					cnn.AddParam("@Log_Info", log.InfoToString());
					cnn.NonQuery();
					IdError = Convert.ToInt32(cnn.GetParam("@Log_Error_Id"));
					Error = String.Format("{0} [{1}]", IdError.ToString(), log.Error);
				}
				catch(Exception eBase)
				{
					IdError = -1;
					Error = "Info in file";
					SaveFile(eBase, log as IMyException);
				}
            finally 
            {
                if (cnn != null) { cnn.CloseConection(); }
            }
		  }//SaveDataBase ends.	   
		  public void SaveFile(Exception eBase, IMyException log)
		  {
			  try
			  {

				  string fileName = String.Empty;
				  if(ConfigurationManager.AppSettings["file_log_errores"] != null)
				  {
					  fileName = ConfigurationManager.AppSettings["file_log_errores"].ToString();
					  string data = CBase._ToJson(log);
					  System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, true);
					  file.WriteLine("------");
					  file.WriteLine(data);
					  file.WriteLine("****");
					  file.WriteLine(eBase.Message);
					  file.WriteLine("------");
					  file.Close();
				  }
			  }
			  catch(Exception file)
			  {
				  throw new Exception("File error" + file.Message);
			  }
		  }
	 }//class DError ends.
}//namespace ends.

