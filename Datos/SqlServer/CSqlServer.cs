using System.Data.SqlClient;
namespace Datos
{
    public partial class CSqlServer:DConexion
    {                       
		private void BuildConnection()
        {			  
				this._connection = new SqlConnection();
            this._connection.ConnectionString = _conectionString;
            this._connection.Open();
        }
		  public CSqlServer(): base(null){BuildConnection();}
		  public CSqlServer(string ConnectionString) : base(ConnectionString)
		  {
			BuildConnection();
		  }    
        public override void NonQuery()
        {
            this._command.ExecuteNonQuery();
        }
        public override void CreateCommand(System.Data.CommandType tipo)
        {
            this._command = new SqlCommand();
            base.CreateCommand(tipo);
        }
        public override object GetParam(string Name)
        {
            return ((SqlCommand)_command).Parameters[Name].Value;
        }
        public override void AddParam(string Name, object Value)
        {
            ((SqlCommand)_command).Parameters.AddWithValue(Name, Value);
        }
        public override void AddParam(object Type, System.Data.ParameterDirection Direction, string Name, object Value)
        {
            SqlParameter parametro = BuilParameter(Type, Direction, Name);
            parametro.Value = Value;
            _command.Parameters.Add(parametro);
        }
		  public override void AddParam(object Type, int longitud, System.Data.ParameterDirection Direction, string Name)
		  {
			  SqlParameter parametro = BuilParameter(Type, Direction, Name);			  
			  parametro.Size=longitud;
			  _command.Parameters.Add(parametro);				  
		  }
		  public override void AddParam(object Type, int longitud, System.Data.ParameterDirection Direction, string Name, object Value)
		  {
			  SqlParameter parametro = BuilParameter(Type, Direction, Name);
			  parametro.Size = longitud;
			  parametro.Value = Value;
			  _command.Parameters.Add(parametro);	
		  }
        public override void AddParam(object Type, System.Data.ParameterDirection Direction, string Name)
        {
            _command.Parameters.Add(BuilParameter(Type, Direction, Name));
        }    
        private SqlParameter BuilParameter(object Type, System.Data.ParameterDirection Direction, string Name)
        {
            SqlParameter parametro = new SqlParameter();
            parametro.SqlDbType = (System.Data.SqlDbType)Type;
            parametro.Direction = Direction;
            parametro.ParameterName = Name;
            return parametro;
        }
		  public override bool HasRows(System.Data.IDataReader reader)
		  {
			  return ((SqlDataReader)reader).HasRows;
		  }		 
    }// CSqlServer ends.
}//  Datos.Entidades ends.

