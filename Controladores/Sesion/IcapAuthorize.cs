﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
namespace Controladores
{
	public class IcapAuthorize : AuthorizeAttribute
	{
		public const string ICAP_SESSION_CLAVE = "icap_web_user";
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			var authroized = base.AuthorizeCore(httpContext);
			if(!authroized)
			{				
				return false;
			}
			IUsuarios usuario = httpContext.Session[IcapAuthorize.ICAP_SESSION_CLAVE] as IUsuarios;
			if(usuario == null)
			{				
				return false;
			}
			return true;
		}
	}//IcapAuthorize ends.
}//Namespace ends.
