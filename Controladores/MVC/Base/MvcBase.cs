﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Datos;
using System.Threading;
using Idiomas;
using System.Net;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;

namespace Controladores.MVC
{
    public class MvcBase : Controller
    {
        #region MensajesMultiIdioma
        protected string _BusquedaNovalida;
        protected string _msjWebApiError;
        protected string _MVCBaseErrorWebApiResponseVacia;
        #endregion
        public string _CatalogoNombre;
        public string _ControladorNombre;
        public string _NombreToolBarView;
        protected string _ViewEdicion;
        protected string _InterfaceName;
        protected IUsuarios _Usuario;
        protected IIdioma _Idioma;
        protected IError _ErrorInterno;
        protected RestSharp.IRestResponse _ResponseWS;
        public delegate bool fnValidaFileUpload(HttpPostedFileBase file);
        protected bool HasSession
        {
            get
            {
                return (_Usuario != null) ? true : false;
            }
        }
        protected bool HasIdioma
        {
            get
            {
                return (_Idioma != null) ? true : false;
            }
        }
        public MvcBase()
        {

        }
        #region ManejoError
        public static IError OnErrorGeneral(Exception error)
        {
            IError eLog = new DError();
            try
            {
                eLog.SaveLog(error);
            }//try ends.
            catch (Exception eInesperado)
            {
                eLog.Error = eInesperado.Message;
            }//catch ends.
            return eLog;
        }
        public bool HasError()
        {
            if (_ErrorInterno != null) return true;
            else return false;
        }
        public string GetError()
        {
            if (HasError()) return _ErrorInterno.Error;
            else return String.Empty;
        }
        protected virtual void OnError(Exception error)
        {
            try
            {
                _ErrorInterno = MvcBase.OnErrorGeneral(error);
                if (_ErrorInterno != null)
                {
                    if (_ResponseWS != null)
                    {
                        string contenido = _ResponseWS.Content;
                        _ResponseWS = null;
                        OnError(String.Format("Error asociado{0} {1}", _ErrorInterno.IdError, contenido));
                    }
                }
            }
            catch (Exception fatal)
            {
                MvcBase.OnErrorGeneral(fatal);
            }
        }
        protected virtual void OnError(string error)
        {
            this.OnError(new Exception(error));
        }
        public virtual void InicializaError()
        {
            if (_ErrorInterno == null) _ErrorInterno = DBase.StaticInicializaError();
        }
        public virtual void SetError(string error)
        {
            InicializaError();
            _ErrorInterno.Error = error;
        }
        public virtual void SetError(string error, bool append)
        {
            InicializaError();
            _ErrorInterno.Error = String.Format("{0} {1}", _ErrorInterno.Error, error);
        }
        #endregion
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            try
            {
                int idIdioma = 0;
                HttpCookie CookieIcapLanguage = GetCookieIdioma();
                int.TryParse(CookieIcapLanguage.Value, out idIdioma);
                _Idioma = GetIdiomaById(idIdioma);
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(_Idioma.Codigo);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                GetUserSession();
                LoadMessagesResx();
            }
            catch (Exception eBeginExecuteCore)
            {
                OnError(eBeginExecuteCore);
            }
            return base.BeginExecuteCore(callback, state);
        }
        protected HttpCookie GetCookieIdioma()
        {
            HttpCookie CookieIcapLanguage = Request.Cookies[CIdioma.COOKIE_NAME_LANGUAGE];
            if (CookieIcapLanguage == null)
            {
                CookieIcapLanguage = new HttpCookie(CIdioma.COOKIE_NAME_LANGUAGE);
            }
            return CookieIcapLanguage;
        }
        protected void ChangeIdioma(int id)
        {
            _Idioma = GetIdiomaById(id);
            HttpCookie CookieIcapLanguage = GetCookieIdioma();
            CookieIcapLanguage.Value = _Idioma.IdIdioma.ToString();
            CookieIcapLanguage.Expires = DateTime.Now.AddDays(CIdioma.COOKIE_EXPIRES);
            Response.Cookies.Add(CookieIcapLanguage);
        }
        protected IIdioma GetIdiomaByCode(string code)
        {
            IEnumerable<IIdioma> _idiomas = CIdioma.GetAllIdiomas();
            IEnumerable<IIdioma> _selected = _idiomas.Where(i => i.Codigo == code);
            if (_selected == null || _selected.Count() <= 0)
            {
                return CIdioma.DefaultIdioma();
            }
            else
            {
                return _selected.FirstOrDefault();
            }
        }
        protected IIdioma GetIdiomaById(int id)
        {
            IEnumerable<IIdioma> _idiomas = CIdioma.GetAllIdiomas();
            IEnumerable<IIdioma> _selected = _idiomas.Where(i => i.IdIdioma == id);
            if (_selected == null || _selected.Count() <= 0)
            {
                return CIdioma.DefaultIdioma();
            }
            else
            {
                return _selected.FirstOrDefault();
            }
        }
        protected void GetUserSession()
        {

            IUsuarios usuario = System.Web.HttpContext.Current.Session[IcapAuthorize.ICAP_SESSION_CLAVE] as IUsuarios;
            if (usuario != null)
            {
                _Usuario = usuario;
            }
        }
        protected void KillSession()
        {
            System.Web.HttpContext.Current.Session[IcapAuthorize.ICAP_SESSION_CLAVE] = null;
            GetUserSession();
        }
        #region WEB_SERVICE
        public string GetBaseUrlService()
        {
            string url = String.Empty;
            try
            {
                url = MVC.MvcBase.GetBaseUrl();
            }
            catch (Exception eUrl)
            {
                OnError(eUrl);
            }
            return url;
        }
        public static string GetBaseUrl()
        {
            return ConfigurationManager.AppSettings["web_api_url_base"].ToString();

        }
        protected string CombineUrl(string url)
        {
            return String.Format("{0}{1}", GetBaseUrlService(), url);
        }
        protected void EvaluaRespuestaWebServie(RestSharp.IRestResponse response)
        {
            try
            {
                if (response != null)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string msj = response.StatusDescription;
                        SetError(String.Format("[{0}] {1}", msj, _msjWebApiError));
                    }
                    _ResponseWS = response;
                }
                else
                {
                    SetError(_MVCBaseErrorWebApiResponseVacia);
                }
            }
            catch (Exception eResponse)
            {
                OnError(eResponse);
            }
        }
        /// <summary>
        /// Realiza una peticion al servicio web [deseñada para ci¿onsumirse desde la seccion de reportes]
        /// </summary>
        /// <param name="url">url del servicio.</param>
        /// <param name="body">parametros.</param>
        /// <param name="Verb">peticion HTTP.</param>
        /// <returns></returns>
        public static RestSharp.IRestResponse BuildPeticionService(string url, string body, RestSharp.Method Verb)
        {
            RestSharp.IRestResponse response = null;
            var client = new RestSharp.RestClient(url);
            var request = new RestSharp.RestRequest(Verb);
            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/json");
            if (!String.IsNullOrEmpty(body))
            {
                request.RequestFormat = RestSharp.DataFormat.Json;
                request.AddParameter("application/json", body, RestSharp.ParameterType.RequestBody);
            }
            response = client.Execute(request);
            return response;
        }

        public RestSharp.IRestResponse BuildPeticionVerb(string url, string body, RestSharp.Method Verb)
        {
            RestSharp.IRestResponse response = null;
            try
            {
                var client = new RestSharp.RestClient(CombineUrl(url));
                var request = new RestSharp.RestRequest(Verb);
                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                if (_Idioma != null)
                {
                    request.AddHeader("accept-language", _Idioma.Codigo);
                }
                if (_Usuario != null)
                {
                    request.AddHeader("userIcap-IdUsuario", _Usuario.IdUsuario.ToString());
                    request.AddHeader("userIcap-IdPerfil", _Usuario.IdPerfil.ToString());
                    if (_Usuario.Data != null && _Usuario.Data.Empresa != null)
                    {
                        request.AddHeader("userIcap-IdEmpresa", _Usuario.Data.Empresa.IdEmpresa.ToString());
                    }
                    if (_Usuario.Data != null && _Usuario.Data.Unidad != null)
                    {
                        request.AddHeader("userIcap-IdUnidad", _Usuario.Data.Unidad.IdUnidad.ToString());
                    }
                    if (_Usuario.IdMenuCaller > 0)
                    {
                        request.AddHeader("userIcap-IdMenu", _Usuario.IdMenuCaller.ToString());
                    }
                }
                if (!String.IsNullOrEmpty(body))
                {
                    //request.Parameters.Clear();
                    request.RequestFormat = RestSharp.DataFormat.Json;
                    request.AddParameter("application/json", body, RestSharp.ParameterType.RequestBody);
                }
                response = client.Execute(request);
                EvaluaRespuestaWebServie(response);
            }
            catch (Exception ePost)
            {
                OnError(ePost);
            }
            return response;
        }
        public RestSharp.IRestResponse BuildPeticionVerb(string url, IEntidad entidad, RestSharp.Method Verb)
        {
            string body = entidad.ToJson(entidad);
            try
            {
                COrdenesTrabajo dd = entidad.ToJson<COrdenesTrabajo>(body);
            }
            catch (Exception) { };
            return BuildPeticionVerb(url, body, Verb);
        }
        public Entity BuildPeticionVerb<Entity>(string url, RestSharp.Method Verb) where Entity : new()
        {
            Entity res = new Entity();
            RestSharp.IRestResponse response = this.BuildPeticionVerb(url, Verb);
            if (response != null)
            {
                res = CBase._ToJson<Entity>(response.Content);
            }
            return res;
        }
        /// <summary>
        /// Realiza la peticion via RestSharp a la url deseada.
        /// </summary>
        /// <typeparam name="Entity">Objeto plantilla para deserealizar la respuesta</typeparam>
        /// <param name="url">URL</param>
        /// <param name="elemento">Parametros</param>
        /// <param name="Verb">POST,GET,PUT,ETC</param>
        /// <returns>Objeto respuesta</returns>
        public Entity BuildPeticionVerb<Entity>(string url, object elemento, RestSharp.Method Verb) where Entity : new()
        {
            Entity res = new Entity();
            RestSharp.IRestResponse response = this.BuildPeticionVerb(url, elemento, Verb);
            if (response != null)
            {
                res = CBase._ToJson<Entity>(response.Content);
            }
            return res;
        }
        public RestSharp.IRestResponse BuildPeticionVerb(string url, object elemento, RestSharp.Method Verb)
        {
            string body = CBase._ToJson(elemento);
            return BuildPeticionVerb(url, body, Verb);
        }
        public RestSharp.IRestResponse BuildPeticionVerb(string url, RestSharp.Method Verb)
        {
            return BuildPeticionVerb(url, String.Empty, Verb);
        }
        public string GetUrlApiService()
        {
            return GetUrlApiService(null, null);
        }
        public string GetUrlApiService(string action, string controller)
        {
            string _Action = (String.IsNullOrEmpty(action)) ? this.ControllerContext.RouteData.Values["action"].ToString() : action;
            string _Controller = (String.IsNullOrEmpty(controller)) ? this.ControllerContext.RouteData.Values["controller"].ToString() : controller;
            return String.Format("{0}/{1}", _Controller, _Action);
        }
        public string GetUrlApiService(string action, string controller, params string[] parametros)
        {
            string url = GetUrlApiService(action, controller);
            if (parametros != null && parametros.Count() > 0)
            {
                parametros.ToList().ForEach(x =>
                {
                    url = String.Format("{0}/{1}", url, x);
                });
            }
            return url;
        }
        protected virtual Entity _GetById<Entity>(int id, string url) where Entity : IEntidad, new()
        {
            Entity Entidad = new Entity();
            try
            {
                RestSharp.IRestResponse response = BuildPeticionVerb(url, RestSharp.Method.GET);
                if (response != null)
                {
                    Entidad = Entidad.ToJson<Entity>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return Entidad;
        }
        //protected virtual void _New<Entity>(Entity Entidad, string url) where Entity : IEntidad, new()
        protected virtual Entity _New<Entity>(Entity Entidad, string url) where Entity : IEntidad, new()
        {
            try
            {
                RestSharp.IRestResponse response = BuildPeticionVerb(url, Entidad, RestSharp.Method.POST);
                if (response != null)
                {
                    Entidad = Entidad.ToJson<Entity>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return Entidad;
        }
        protected virtual Entity _Put<Entity>(int id, string url, Entity Entidad) where Entity : IEntidad, new()
        {
            try
            {
                RestSharp.IRestResponse response = BuildPeticionVerb(url, Entidad, RestSharp.Method.PUT);
                if (response != null)
                {
                    Entidad = Entidad.ToJson<Entity>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return Entidad;
        }
        protected virtual string _Delete(int id, string url)
        {
            try
            {
                RestSharp.IRestResponse response = BuildPeticionVerb(url, RestSharp.Method.DELETE);
                if (response != null)
                {
                    return response.Content;
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return String.Empty;
        }
        protected virtual IEnumerable<Entity> _Top<Entity>(string url, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            try
            {
                if (where == null || where.Count() <= 0) return null;
                where.ToList().ForEach(x => x.Prioridad = true);
                string body = CBase._ToJson(where);
                RestSharp.IRestResponse response = BuildPeticionVerb(url, body, RestSharp.Method.POST);
                if (response != null)
                {
                    Entity Entidad = new Entity();
                    return Entidad.ToJson<IEnumerable<Entity>>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return null;
        }
        /// <summary>
        /// Realiza la busqueda y regresa todos los resultados en una lista de Entidades.
        /// </summary>
        /// <typeparam name="Entity">Tipo del cual sera la lista.</typeparam>
        /// <param name="url">URL del webservice.</param>
        /// <param name="where">Consdiciones busqueda.</param>
        /// <returns></returns>
        protected virtual IEnumerable<Entity> _Search<Entity>(string url, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            try
            {
                string body = CBase._ToJson(where);
                RestSharp.IRestResponse response = BuildPeticionVerb(url, body, RestSharp.Method.POST);
                if (response != null)
                {
                    Entity Entidad = new Entity();
                    return Entidad.ToJson<IEnumerable<Entity>>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return null;
        }
        /// <summary>
        /// Realiza la busqueda y regresa los resultados paginados.
        /// </summary>
        /// <typeparam name="Entity">Tipo del cual sera la lista.</typeparam>
        /// <param name="url">URL del webservice.</param>
        /// <param name="where">Consdiciones busqueda.</param>
        /// <returns></returns>
        protected virtual IPagina<Entity> _SearchByPage<Entity>(string url, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            try
            {
                string body = CBase._ToJson(where);
                RestSharp.IRestResponse response = BuildPeticionVerb(url, body, RestSharp.Method.POST);
                if (response != null)
                {
                    Entity Entidad = new Entity();
                    return Entidad.ToJson<DPagina<Entity>>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return null;
        }
        protected virtual IPagina<Entity> _GetPage<Entity>(string url) where Entity : IEntidad, new()
        {
            try
            {
                IEnumerable<IPropsWhere> whereIndex = _FiltroIndex();
                string body = CBase._ToJson(whereIndex);
                RestSharp.IRestResponse response = BuildPeticionVerb(url, body, RestSharp.Method.POST);
                if (response != null)
                {
                    Entity Entidad = new Entity();
                    return Entidad.ToJson<DPagina<Entity>>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return null;
        }
        #endregion
        #region MVC_UI
        protected ActionResult EvaluaErrorInterno(string partialViewCustomError, ActionResult mvcDefault)
        {
            if (this.HasError())
            {
                //Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                //Response.StatusCode = (int)HttpStatusCode.BadRequest;
                //Response.Status = "el estatus";
                //Response.StatusDescription = " las descripcion";
                return PartialView(partialViewCustomError, _ErrorInterno);
            }
            else
            {
                return mvcDefault;
            }
        }
        protected ActionResult EvaluaErrorInterno(ActionResult mvcDefault)
        {
            /*if(this.HasError())
			{
				return PartialView("_RespuestaERR", _ErrorInterno);
			}
			else
			{
				return mvcDefault;
			}*/
            return EvaluaErrorInterno("_RespuestaERR", mvcDefault);
        }
        protected ActionResult EvaluaErrorInternoJSON(object Respuesta)
        {
            if (this.HasError())
            {
                return Json(_ErrorInterno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Respuesta, JsonRequestBehavior.AllowGet);
            }
        }
        protected IUICatalogo _UICatalogo<Entity>(IPagina<Entity> PaginaInfo, string metodo) where Entity : IEntidad, new()
        {
            IPaginaUI paginaUI = new CPaginaUI();
            IUICatalogo catalogo = new CUICatalogo();
            paginaUI.Nombre = _CatalogoNombre;
            paginaUI.PaginaActual = PaginaInfo.Pagina;
            paginaUI.PaginasTotal = PaginaInfo.Paginas;
            paginaUI.Url = this.GetUrlApiService();
            paginaUI.Metodo = metodo;
            paginaUI.NumeroPaginaTexto = this.TraduceTexto("IPaginaUINumeroPaginaTexto");
            if (PaginaInfo.Contenido != null && PaginaInfo.Contenido.Count() > 0)
            {
                catalogo.Contenido = PaginaInfo.Contenido.Select(p => p as IEntidad);
            }
            catalogo.Nombre = _CatalogoNombre;
            catalogo.NombreToolBar = this._NombreToolBarView;
            catalogo.ControladorName = (String.IsNullOrEmpty(_ControladorNombre)) ? _CatalogoNombre : _ControladorNombre;
            catalogo.PaginaInfo = paginaUI;
            catalogo.Traductor = this.TraduceTexto;
            return catalogo;
        }
        protected virtual IEnumerable<IPropsWhere> _FiltroIndex()
        {
            return null;
        }
        protected virtual ActionResult _UIIndex<Entity>(string vistaIndex, string vistaPaginado, int idMenuCaller) where Entity : IEntidad, new()
        {
            IUICatalogo catalogo = null;
            try
            {
                if (_Usuario != null)
                {
                    _Usuario.IdMenuCaller = idMenuCaller;
                }
                IPagina<Entity> page = this._GetPage<Entity>(String.Format("{0}/1", this.GetUrlApiService()));
                if (page != null)
                {
                    //var a = new JavaScriptSerializer().Serialize(page.Contenido);
                    catalogo = _UICatalogo<Entity>(page, "get");
                    catalogo.PaginaInfo.Url = this.GetUrlApiService(vistaPaginado, null);
                    catalogo.FieldsSearch = this._UIBuildFieldsSearch(new Entity());
                    catalogo.Usuario = _Usuario;
                    catalogo.MenuCaller = idMenuCaller;
                    catalogo.NombreToolBar = this._NombreToolBarView;
                    IEnumerable<IPerfilAcciones> permisos = this.GetPermisos(idMenuCaller, _Usuario.Data.Perfil);
                    catalogo.SetPermisos(permisos);
                    catalogo.Empresas = new JavaScriptSerializer().Serialize(page.Contenido);
                }
            }
            catch (Exception eIndex)
            {
                OnError(eIndex);
            }
            return EvaluaErrorInterno(PartialView(vistaIndex, catalogo));
        }
        protected virtual ActionResult _UIIndex<Entity>(int idMenuCaller) where Entity : IEntidad, new()
        {
            return _UIIndex<Entity>("Index", "IndexPaginado", idMenuCaller);
            /*IUICatalogo catalogo = null;
			try
			{
				if(_Usuario != null)
				{
					_Usuario.IdMenuCaller = idMenuCaller;
				}
				IPagina<Entity> page = this._GetPage<Entity>(String.Format("{0}/1", this.GetUrlApiService()));
				if(page != null)
				{					
					catalogo = _UICatalogo<Entity>(page, "get");
					catalogo.PaginaInfo.Url = this.GetUrlApiService("IndexPaginado", null);
					catalogo.FieldsSearch = this._UIBuildFieldsSearch(new Entity());
					catalogo.Usuario = _Usuario;
					catalogo.MenuCaller = idMenuCaller;
					IEnumerable<IPerfilAcciones> permisos = this.GetPermisos(idMenuCaller, _Usuario.Data.Perfil);
					catalogo.SetPermisos(permisos);
				}
			}
			catch(Exception eIndex)
			{
				OnError(eIndex);
			}
			return EvaluaErrorInterno(PartialView(catalogo));*/
        }
        protected virtual ActionResult _UIIndex<Entity>() where Entity : IEntidad, new()
        {
            return _UIIndex<Entity>(0);
        }
        protected virtual ActionResult _UIIndexPaginado<Entity>(string vistaIndex, string vistaBuscar, int pagina) where Entity : IEntidad, new()
        {
            IPagina<Entity> page = this._GetPage<Entity>(String.Format("{0}/{1}", this.GetUrlApiService(vistaIndex, null), pagina));
            IUICatalogo catalogo = null;
            if (page != null)
            {
                catalogo = _UICatalogo<Entity>(page, "get");
            }
            return EvaluaErrorInterno(PartialView(vistaBuscar, catalogo));
        }
        protected virtual ActionResult _UIIndexPaginado<Entity>(int pagina) where Entity : IEntidad, new()
        {
            return _UIIndexPaginado<Entity>("Index", "Buscar", pagina);
            /*IPagina<Entity> page = this._GetPage<Entity>(String.Format("{0}/{1}", this.GetUrlApiService("Index", null), pagina));
			IUICatalogo catalogo = null;
			if(page != null)
			{
				catalogo = _UICatalogo<Entity>(page, "get");
			}
			return EvaluaErrorInterno(PartialView("Buscar", catalogo));*/
        }
        protected virtual ActionResult _UINew(string PartialViewName)
        {
            return this._UINew(PartialViewName, null);
        }
        protected virtual ActionResult _UINew(string PartialViewName, object model)
        {
            ViewBag.LabelCreate = Idiomas.IcapWeb.CatalogoCrearEntidad;
            ViewBag.Action = "New";
            if (model != null)
                return EvaluaErrorInterno(PartialView(PartialViewName, model));
            else
                return EvaluaErrorInterno(PartialView(PartialViewName));
        }
        protected virtual ActionResult _UINew<Entity>(Entity entity) where Entity : IEntidad, new()
        {
            this._New<Entity>(entity, GetUrlApiService());
            return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
        }
        protected virtual ActionResult _UINew<Entity>(List<Entity> entity) where Entity : IEntidad, new()
        {
            string body = CBase._ToJson(entity);
            RestSharp.IRestResponse response = BuildPeticionVerb(GetUrlApiService(), body, RestSharp.Method.POST);
            if (response != null)
            {
                entity = CBase._ToJson<List<Entity>>(response.Content);
            }
            return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
        }
        protected virtual ActionResult _UIEditar<Entity>(string PartialViewName, int id) where Entity : IEntidad, new()
        {
            ViewBag.LabelCreate = Idiomas.IcapWeb.CatalogoCrearEntidad;
            string url = String.Format("{0}/{1}", GetUrlApiService("Get", null), id);
            Entity Entidad = this._GetById<Entity>(id, url);
            ViewBag.Action = "Put";
            return EvaluaErrorInterno(PartialView(PartialViewName, Entidad));
        }
        protected virtual ActionResult _UIEditar<Entity>(int id, Entity Entidad) where Entity : IEntidad, new()
        {
            string url = String.Format("{0}/{1}", GetUrlApiService(), id);
            this._Put<Entity>(id, url, Entidad);
            return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
        }
        protected virtual ActionResult _UIEditar<Entity>(string url, int id, Entity Entidad) where Entity : IEntidad, new()
        {
            this._Put<Entity>(id, url, Entidad);
            return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
        }
        protected virtual ActionResult _UIEliminar<Entity>(int id) where Entity : IEntidad, new()
        {
            ViewBag.LabelDelete = Idiomas.IcapWeb.CatalogoBorrarEntidad;
            string url = String.Format("{0}/{1}", GetUrlApiService("Get", null), id);
            Entity Entidad = this._GetById<Entity>(id, url);
            return EvaluaErrorInterno(PartialView(Entidad));
        }
        protected virtual ActionResult _UIEliminaById(int id)
        {
            string url = String.Format("{0}/{1}", GetUrlApiService(), id);
            this._Delete(id, url);
            return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
        }
        protected virtual ActionResult _UISearchAll<Entity>(string PartialViewName, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            bool validawhere = this.EvaluaWhere(new Entity(), where);
            IEnumerable<Entity> Result = null;
            if (!validawhere)
            {
                SetError(_BusquedaNovalida, true);
            }
            else
            {
                string url = GetUrlApiService();
                Result = this._Search<Entity>(url, where);
            }
            return EvaluaErrorInterno(PartialView(PartialViewName, Result));
        }
        protected virtual ActionResult _UISearchAll<Entity>(IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            return this._UISearchAll<Entity>("Buscar", where);
        }
        protected virtual ActionResult _UISearch<Entity>(int idPagina, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            string url = String.Format("{0}/{1}", GetUrlApiService(), idPagina);
            return this._UISearch<Entity>(url, where);
        }

        protected virtual ActionResult _UISearch<Entity>(string url, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            bool validawhere = this.EvaluaWhere(new Entity(), where);
            IUICatalogo catalogo = null;
            if (!validawhere)
            {
                SetError(_BusquedaNovalida, true);
            }
            else
            {
                IPagina<Entity> Result = this._SearchByPage<Entity>(url, where);
                if (Result != null)
                {
                    catalogo = _UICatalogo<Entity>(Result, "post");
                    catalogo.PaginaInfo.Parametros = CBase._ToJson(where);
                }
            }
            return EvaluaErrorInterno(PartialView("Buscar", catalogo));
        }
        /**/

        protected virtual ActionResult _Top<Entity>(IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            IEnumerable<Entity> Result = null;
            string url = GetUrlApiService();
            Result = this._Top<Entity>(url, where);
            return EvaluaErrorInternoJSON(Result);
        }
        protected virtual ActionResult _Top<Entity>(int top, IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
        {
            IEnumerable<Entity> Result = null;
            string url = String.Format("{0}/{1}", GetUrlApiService(), top);
            Result = this._Top<Entity>(url, where);
            return EvaluaErrorInternoJSON(Result);
        }
        /**/
        protected bool EvaluaWhere(IEntidad Entidad, IEnumerable<IPropsWhere> where)
        {
            foreach (IPropsWhere _where in where)
            {
                if (!_where.ValidaConsultaMVC(Entidad))
                    return false;
            }
            return true;
        }
        /// <summary>
        /// Obtenemos el listado de los campos para buscar en el catalogo en base a las data annotations definidas en la interfaz.
        /// </summary>
        /// <param name="Entity">Referencia a la entidad.</param>
        /// <returns>Listado de campos validos para buscar.</returns>
        protected virtual IEnumerable<IPropsWhere> _UIBuildFieldsSearch(IEntidad Entity)
        {
            List<IPropsWhere> FieldsSearch = new List<IPropsWhere>();
            string[] DefaultFIeldsOmitir = Entity.GetFieldsSearchOmitir();//Si no es nulo omitimos reflection y cosntruimos los campos manualmente.
            string[] DefaultFIelds = Entity.GetFieldsSearch();//Si no es nulo omitimos reflection y cosntruimos los campos manualmente.
            if (DefaultFIelds == null)
            {
                Type objType = Entity.GetType();
                if (objType != null)
                {
                    PropertyInfo[] properties = objType.GetInterface(_InterfaceName).GetProperties();
                    foreach (PropertyInfo propiedad in properties)
                    {
                        DisplayAttribute displayAttribute = propiedad.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault() as DisplayAttribute;
                        FiltroBusqueda Filtro = propiedad.GetCustomAttributes(typeof(FiltroBusqueda), true).FirstOrDefault() as FiltroBusqueda;
                        if (displayAttribute != null)
                        {
                            bool add = true;
                            if (DefaultFIeldsOmitir != null)
                            {
                                IEnumerable<string> existe = from tbl in DefaultFIeldsOmitir
                                                             where tbl == propiedad.Name
                                                             select tbl;
                                if (existe != null && existe.Count() > 0)
                                {
                                    add = false;
                                }
                            }
                            if (add)
                            {
                                CPropsWhere pAux = new CPropsWhere();
                                if (Filtro != null)
                                {
                                    pAux.NombreCampo = Filtro.Campo;
                                    if (!String.IsNullOrEmpty(Filtro.CampoDisplay))
                                    {
                                        pAux.Condicion = CIdioma.TraduceCadena(Filtro.CampoDisplay);
                                    }
                                    else
                                    {
                                        pAux.Condicion = CIdioma.TraduceCadena(displayAttribute.Name);
                                    }
                                    pAux.OmiteConcatenarTabla = Filtro.OmiteConcatenarTabla;
                                    pAux.Plantilla = Filtro.Plantilla;
                                }
                                else
                                {
                                    pAux.NombreCampo = propiedad.Name;
                                    pAux.Condicion = CIdioma.TraduceCadena(displayAttribute.Name);
                                }
                                if (Filtro == null || !Filtro.ExcluirCampo)
                                {
                                    FieldsSearch.Add(pAux);
                                }

                                /*new CPropsWhere()
                            {
                                NombreCampo = propiedad.Name,
                                Condicion = CIdioma.TraduceCadena(displayAttribute.Name)

                            }*/
                            }
                        }
                    }
                }
            }//if(DefaultFIelds == null) ends.
            else
            {
                DefaultFIelds.ToList().ForEach(x => FieldsSearch.Add(new CPropsWhere()
                {
                    NombreCampo = x.Split('|')[0], /*Nombre del campo*/
                    Condicion = CIdioma.TraduceCadena(x.Split('|')[1]) /*Texto a mostrar en ui*/
                }));
            }
            return FieldsSearch;
        }//function _UIBuildFieldsSearch ends.
        #endregion
        #region MULTI_IDIOMA
        protected void LoadMessagesResx()
        {
            try
            {
                _BusquedaNovalida = (Idiomas.IcapWeb.MvcBaseBusquedaNovalida != null) ? Idiomas.IcapWeb.MvcBaseBusquedaNovalida : "Busqueda no valida Default";
                _msjWebApiError = (Idiomas.IcapWeb.MVCBaseErrorWebApiResponse != null) ? Idiomas.IcapWeb.MVCBaseErrorWebApiResponse : "Error al recibir respuesta del WebService Default";
                _MVCBaseErrorWebApiResponseVacia = (Idiomas.IcapWeb.MVCBaseErrorWebApiResponseVacia != null) ? Idiomas.IcapWeb.MVCBaseErrorWebApiResponseVacia : "Error al recibir respuesta del WebService Default";
            }
            catch (Exception Error)
            {
                OnError(Error);
            }
        }
        /// <summary>
        ///  Obtiene el texto en el recurso dependiendo del Idioma seleccionado si no lo encuentra se regresa el texto tal cual fue enviado a buscar.
        /// </summary>
        /// <param name="cadena">Texto a traducir.</param>
        /// <returns>Texto traducido.</returns>
        public string TraduceTexto(string cadena)
        {
            return CIdioma.TraduceCadena(cadena);
        }
        #endregion

        #region PERMISOS
        protected IUICatalogo BuildUICatalogo(string Nombre, string controladorName, IUsuarios usuario)
        {
            IUICatalogo catalogo = new CUICatalogo();
            catalogo.Traductor = this.TraduceTexto;
            catalogo.Nombre = Nombre;
            catalogo.ControladorName = controladorName;
            catalogo.PaginaInfo = null;
            catalogo.Usuario = usuario;
            catalogo.MenuCaller = usuario.IdMenuCaller;
            IEnumerable<IPerfilAcciones> permisos = this.GetPermisos(usuario.IdMenuCaller, usuario.Data.Perfil);
            catalogo.SetPermisos(permisos);
            return catalogo;
        }
        protected IEnumerable<IPerfilAcciones> GetPermisos(int idMenu)
        {
            try
            {
                IPerfiles perfil = (_Usuario != null && _Usuario.Data != null) ? _Usuario.Data.Perfil : null;
                return this.GetPermisos(idMenu, perfil);
            }

            catch (Exception e)
            {
                OnError(e);
            }
            return null;
        }
        protected IEnumerable<IPerfilAcciones> GetPermisos(int idMenu, IPerfiles perfil)
        {
            IEnumerable<IPerfilAcciones> allPermisos = null;
            try
            {
                Negocio.NPerfilAcciones permisos = new Negocio.NPerfilAcciones();
                IEnumerable<IPropsWhere> where = permisos.FiltroIndex(idMenu, perfil);
                if (where == null || where.Count() <= 0) return null;
                string body = CBase._ToJson(where);
                RestSharp.IRestResponse response = BuildPeticionVerb("PerfilAcciones/SearchAll", body, RestSharp.Method.POST);
                if (response != null)
                {
                    allPermisos = CBase._ToJson<IEnumerable<CPerfilAcciones>>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return allPermisos;
        }
        protected virtual bool _HasPermiso(int idMenu, string name)
        {
            int idPerfil = this._Usuario.IdPerfil;
            IEnumerable<IPerfilAcciones> permisos = this.GetPermisos(idMenu);
            IEnumerable<IPerfilAcciones> permiso = permisos.Where(p => p.AccionNombre.ToLower() == name.ToLower() && p.IdPerfil == idPerfil);
            if (permiso != null && permiso.Count() > 0) return true;
            return false;
        }
        #endregion

        #region FILE_UPLOAD
        [HttpGet]
        public virtual ActionResult PlantillaLoad()
        {
            return EvaluaErrorInterno(PartialView("_LoadFile"));
        }
        /// <summary>
        /// Se encarga de almacenar los archivos enviadoas desde el cliente en una ruta de paso
        /// </summary>
        /// <returns></returns>
        protected virtual string[] SaveFiles(fnValidaFileUpload fnValidacion)
        {
            List<string> ListFiles = new List<string>();
            try
            {
                if (ConfigurationManager.AppSettings["PathFileTemporal"] == null)
                {
                    OnError("No se ha definido PathFileTemporal");
                    return null;
                }
                string pathDefault = ConfigurationManager.AppSettings["PathFileTemporal"].ToString();
                string NewPath = System.IO.Path.Combine(Server.MapPath("~/"), pathDefault);
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];

                    //file.

                    int fileSize = file.ContentLength;
                    string fileName = file.FileName;
                    string mimeType = file.ContentType;
                    string icapPath = System.IO.Path.Combine(NewPath, fileName);
                    if (!String.IsNullOrEmpty(fileName) && fileSize > 0)
                    {
                        bool isValid = true;
                        if (fnValidacion != null)
                        {
                            isValid = fnValidacion(file);
                        }
                        if (isValid)
                        {
                            System.IO.Stream fileContent = file.InputStream;
                            if (System.IO.File.Exists(icapPath))
                            {
                                System.IO.File.Delete(icapPath);
                            }
                            file.SaveAs(icapPath);
                            ListFiles.Add(icapPath);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return ListFiles.ToArray();
        }
        protected virtual List<CFileRequest> FilesToByteArray(fnValidaFileUpload fnValidacion)
        {
            List<CFileRequest> ListFiles = new List<CFileRequest>();
            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    int fileSize = file.ContentLength;
                    string fileName = file.FileName;
                    string mimeType = file.ContentType;
                    if (!String.IsNullOrEmpty(fileName) && fileSize > 0)
                    {
                        bool isValid = true;
                        if (fnValidacion != null)
                        {
                            isValid = fnValidacion(file);
                        }
                        if (isValid)
                        {
                            CFileRequest auxFile = new CFileRequest();
                            auxFile.Nombre = fileName;
                            auxFile.Contenido = new byte[fileSize];
                            file.InputStream.Read(auxFile.Contenido, 0, auxFile.Contenido.Length);
                            ListFiles.Add(auxFile);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return ListFiles;
        }
        protected IEnumerable<DFile> ToFiles(string path, IEnumerable<CFileRequest> files)
        {
            if (!files.Any()) return null;
            List<DFile> all = new List<DFile>();
            files.ToList().ForEach(x =>
            {
                all.Add(new DFile()
                {
                    Ruta = path,
                    Name = x.Nombre,
                    Contenido = x.Contenido
                });
            });
            return all;
        }
        #endregion
        #region DATOS_USER
        public int GetIdUnidad()
        {
            if (_Usuario != null)
            {
                if (_Usuario.Data != null)
                {
                    if (_Usuario.Data.Unidad != null)
                    {
                        return _Usuario.Data.Unidad.IdUnidad;
                    }
                }
            }
            return 0;
        }
        public string GetUnidad()
        {
            if (_Usuario != null)
            {
                if (_Usuario.Data != null)
                {
                    if (_Usuario.Data.Unidad != null)
                    {
                        return _Usuario.Data.Unidad.Unidad;
                    }
                }
            }
            return String.Empty;
        }
        public int GetIdEmpresa()
        {
            if (_Usuario != null)
            {
                if (_Usuario.Data != null)
                {
                    if (_Usuario.Data.Empresa != null)
                    {
                        return _Usuario.Data.Empresa.IdEmpresa;
                    }
                }
            }
            return 0;
        }
        public string GetEmpresa()
        {
            if (_Usuario != null)
            {
                if (_Usuario.Data != null)
                {
                    if (_Usuario.Data.Empresa != null)
                    {
                        return _Usuario.Data.Empresa.Empresa;
                    }
                }
            }
            return String.Empty;
        }
        #endregion
        #region ExportExcel
        [HttpGet]
        [ActionName("Excel")]
        public ActionResult Excel()
        {
            byte[] excel = null;
            string fileName = String.Empty;
            try
            {
                string url = String.Format("{0}", GetUrlApiService());
                fileName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (String.IsNullOrEmpty(fileName))
                {
                    fileName = "ExcelAux.xlsx";
                }
                else
                {
                    fileName = String.Format("{0}.xlsx", fileName);
                }
                string body = Request.QueryString["whereIcap"];
                RestSharp.IRestResponse response = BuildPeticionVerb(url, body, RestSharp.Method.POST);
                if (response != null)
                {
                    excel = response.RawBytes;
                }
            }
            catch (Exception eExcel)
            {
                OnError(eExcel);
            }
            return EvaluaErrorInterno(File(excel, "application/vnd.ms-excel", fileName));
        }
        #endregion
        #region ExportPdf
        [HttpGet]
        [ActionName("Pdf")]
        public ActionResult Pdf(string html)
        {
            Byte[] pdf = null;
            try
            {
                pdf = Negocio.NBase.HtmlToPDF(html);
            }
            catch (Exception ePdf)
            {
                OnError(ePdf);
            }
            return EvaluaErrorInterno(File(pdf, "application/pdf", "algo.pdf"));
        }
        #endregion
        #region MANEJO_ARCHIVOS		
        protected virtual Entity HandleFile<Entity>(string action, string controller, Entity archivo) where Entity : new()
        {
            try
            {
                string url = GetUrlApiService(action, controller);
                RestSharp.IRestResponse response = BuildPeticionVerb(url, archivo, RestSharp.Method.POST);
                if (response != null)
                {
                    archivo = CBase._ToJson<Entity>(response.Content);
                }
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return archivo;
        }
        public virtual DFile PostSingleFile(DFile archivo)
        {
            archivo.ContenidoString = Convert.ToBase64String(archivo.Contenido);
            archivo.Contenido = null;
            archivo = HandleFile<DFile>("PostFile", null, archivo);
            return archivo;
        }
        public virtual IEnumerable<DFile> PostFile(List<DFile> archivos)
        {
            //return HandleFile<List<DFile>>("PostFile", null, archivos);
            List<DFile> up = new List<DFile>();
            DFile aux = null;
            archivos.ForEach(x =>
            {
                x.ContenidoString = Convert.ToBase64String(x.Contenido);
                x.Contenido = null;
                aux = HandleFile<DFile>("PostFile", null, x);
                x.Url = aux.Url;
                up.Add(x);
            });
            return up;
        }
        public virtual DFile GetFile(DFile archivo)
        {
            DFile file = HandleFile<DFile>("GetFile", null, archivo);
            if (file != null && !String.IsNullOrEmpty(file.ContenidoString))
            {
                file.Contenido = Convert.FromBase64String(file.ContenidoString);
                file.Name = System.IO.Path.GetFileName(file.Url);
            }
            return file;
        }
        public virtual DFile DelFile(DFile archivo)
        {
            archivo.Contenido = null;
            archivo.ContenidoString = String.Empty;
            DFile file = HandleFile<DFile>("DelFile", null, archivo);
            return file;
        }
        #endregion
        public virtual string ViewToString(PartialViewResult vista, string NombreVista)
        {
            string html;
            using (var sw = new StringWriter())
            {
                PartialViewResult result = vista;
                result.View = ViewEngines.Engines.FindPartialView(ControllerContext, NombreVista).View;
                ViewContext vc = new ViewContext(ControllerContext, result.View, result.ViewData, result.TempData, sw);
                result.View.Render(vc, sw);
                html = sw.GetStringBuilder().ToString();
            }
            return html;
        }
        [HttpGet]
        public virtual ActionResult DescargaReporte(string IdReporte, string NombreArchivo)
        {
            if (TempData[IdReporte] != null)
            {
                byte[] data = TempData[IdReporte] as byte[];
                return File(data, "application/octet-stream", NombreArchivo);
            }
            else
            {
                return new EmptyResult();
            }
        }
    }//class MvcBase ends.
}//Namespace Controladores.MVC ends.
