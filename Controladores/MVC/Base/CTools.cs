﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controladores
{
	public class CTools
	{
		public static string GetBaseUrl()
		{
			return ConfigurationManager.AppSettings["web_api_url_base"].ToString();

		}
	}
}
