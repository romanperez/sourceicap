﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ICAPController : MvcBase
	{
		[AllowAnonymous]
		public ActionResult Error()
		{
			ViewBag.Error = Request.QueryString["msg"];
			return PartialView();
		}
		[HttpGet]
		public ActionResult LogOut()
		{
			base.KillSession();
			return RedirectToAction("Login", "Acceso");
		}
		public ActionResult Index()
		{
			CIcapIndex Inicio = new CIcapIndex();
			if(HasSession)
			{
				Inicio.Usuario = _Usuario;
				if(Inicio.Usuario != null && Inicio.Usuario.Data != null && Inicio.Usuario.Data.Perfil != null)
				{
					Inicio.Menus = GetMenus(Inicio.Usuario.Data.Perfil);
				}				
			}
			if(HasIdioma)
			{
				Inicio.Idioma = _Idioma;
			}			
			
			return View(Inicio);
		}
		public ActionResult IcapModal()
		{
			return PartialView();
		}

		public ActionResult MenuPrincipal()
		{		
			return PartialView();
		}
		protected IEnumerable<IMenus> GetMenus(IPerfiles perfil)
		{						
			CMenus mnu = new CMenus();
			IEnumerable<IMenus> menus = null;
			IRestResponse response = BuildPeticionVerb("Menu/MenuByPerfil", perfil, Method.POST);
			if(response != null)
			{
				menus = mnu.ToJson<List<CMenus>>(response.Content);
				NMenus MNU = new NMenus();
				MNU.GetMenuIdioma(menus);
			}
			return menus;
		}
		[HttpGet]
		public ActionResult MantenerSession()
		{
			return PartialView();
		}
	}
}
