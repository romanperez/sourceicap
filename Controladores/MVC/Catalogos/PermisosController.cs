using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	
	[IcapAuthorize]
	public class PermisosController   : MvcBase
	{
		public PermisosController()
		{
			_CatalogoNombre = "Permisos";
			_InterfaceName = "IPermisos";
			_ViewEdicion = "IPermisosEditarNew";			
		}		
		[HttpGet]
		[ActionName("Index")]		
		public ActionResult Index()
		{
			return _Get();
		}
        [HttpGet]
        [ActionName("GetPerfilPermisos")]
        public ActionResult GetPerfilPermisos(int id)
        {
            return _GetPerfilPermisos(id);
        }
		[HttpPost]
		[ActionName("Put")]
		public ActionResult PermisosPerfil(CPermisosJs permisos)
		{
			return base._UIEditar<CPermisosJs>(permisos.IdPerfil, permisos);
		}
        protected virtual ActionResult _GetPerfilPermisos(int id)
        {
            CPermisosJs permiso = new CPermisosJs();
            try
            {
                RestSharp.IRestResponse response = BuildPeticionVerb(base.GetUrlApiService() + "/" + id , RestSharp.Method.GET);
                if (response != null)
                {
                    permiso = CBase._ToJson<CPermisosJs>(response.Content);                   
                }
            }
            catch (Exception eIndex)
            {
                OnError(eIndex);
            }
            return EvaluaErrorInternoJSON(permiso);            
        }
		protected virtual ActionResult _Get() 
		{
			List<CPermisos> permisos = new List<CPermisos>();
			CPermisos permiso = new CPermisos();
			try
			{
				RestSharp.IRestResponse response = BuildPeticionVerb(base.GetUrlApiService("Get",null), RestSharp.Method.GET);				
				if(response != null)
				{					
					permisos = CBase._ToJson<List<CPermisos>>(response.Content);
					if(permisos != null && permisos.Count() > 0)
					{
						permiso = permisos.First();
					}
				}
			}
			catch(Exception eIndex)
			{
				OnError(eIndex);
			}
			return EvaluaErrorInterno(View(permiso));
		}
	}//class PermisosController  ends.
}//Namespace Controladores.MVC ends.

