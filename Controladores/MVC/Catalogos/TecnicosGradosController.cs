using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class TecnicosGradosController : MvcBase
	{
		public TecnicosGradosController()
		{
			_CatalogoNombre = "TecnicosGrados";
			_InterfaceName = "ITecnicosGrados";
			_ViewEdicion = "ITecnicosGradosEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CTecnicosGrados>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{			
			return base._UIIndexPaginado<CTecnicosGrados>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CTecnicosGrados tecnicoGrado = null;
			try
			{
				tecnicoGrado = new CTecnicosGrados();
				tecnicoGrado.Empresa = base.GetEmpresa();
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return base._UINew(_ViewEdicion, tecnicoGrado);
		}
		[HttpPost]
		public ActionResult New(CTecnicosGrados entity)
		{
			entity.IdEmpresa = base.GetIdEmpresa();
			return base._UINew<CTecnicosGrados>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CTecnicosGrados>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CTecnicosGrados entity)
		{
			entity.IdEmpresa = base.GetIdEmpresa();
			return base._UIEditar<CTecnicosGrados>(entity.IdGrado, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CTecnicosGrados>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CTecnicosGrados>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CTecnicosGrados>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CTecnicosGrados>(where);
		}				
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CTecnicosGrados>(id, where);
		}

        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<ITecnicosGrados> GradosXls = null;
                NExcel<CTecnicosGrados> xls = new NExcel<CTecnicosGrados>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    GradosXls = NTecnicosGrados.GetUnidadesFromTable(dt);
                }
                GradosXls = this.BuildPeticionVerb<List<CTecnicosGrados>>(this.GetUrlApiService("NewXls", "TecnicosGrados"), GradosXls, RestSharp.Method.POST);
                Resp = PartialView("_TecnicosGradosWithError", GradosXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class TecnicosGradosController  ends.
}//Namespace Controladores.MVC ends.

