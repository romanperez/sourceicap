using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ProyectoContratosController   : MvcBase
	{
		public ProyectoContratosController ()
		{			
			_CatalogoNombre = "ProyectoContratos";
			_InterfaceName = "IProyectoContratos";
			_ViewEdicion = "IProyectoContratosEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index()
		{
			return base._UIIndex<CProyectoContratos>();
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CProyectoContratos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CProyectoContratos entity)
		{           
			  return base._UINew<CProyectoContratos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CProyectoContratos>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CProyectoContratos entity)
		{            
				return base._UIEditar<CProyectoContratos>(entity.IdProyectoContrato, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CProyectoContratos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CProyectoContratos>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CProyectoContratos>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CProyectoContratos>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CProyectoContratos>(id, where);
	}
	

   }//class ProyectoContratosController  ends.
}//Namespace Controladores.MVC ends.

