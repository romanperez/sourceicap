using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ActividadesController : MvcBase
	{
		protected const string PATH_ACTIVIDADES = @"ACTIVIDADES";
		public ActividadesController()
		{
			_CatalogoNombre = "Actividades";
			_InterfaceName = "IActividades";
			_ViewEdicion = "IActividadesEditarNew";
		}
		#region PROTECTED
		protected IActividades SetIdEmpresaUser(IActividades entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = GetIdEmpresa();
				entity.Empresa = GetEmpresa();
			}
			return entity;
		}
		protected void SendEnviaDocumentos(IActividades actividad)
		{
			try
			{			
				if(actividad == null) return;
				IEnumerable<DFile> pAux= this.ToFiles(ActividadesController.PATH_ACTIVIDADES, base.FilesToByteArray(null));
				List<DFile> prodecimientosDocumentos =null;
				if(pAux != null && pAux.Count() > 0) prodecimientosDocumentos = pAux.ToList();
				if(prodecimientosDocumentos != null && prodecimientosDocumentos.Count() > 0)
				{
					prodecimientosDocumentos = this.PostFile(prodecimientosDocumentos).ToList();
				}
				string[] map = null;
				if(!String.IsNullOrEmpty(actividad.AuxFilesMaps))
				{
					map = actividad.AuxFilesMaps.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
					foreach(string tipoDoc in map)
					{
						string[] tipodocc = tipoDoc.Split('/');
						foreach(DFile fileSended in prodecimientosDocumentos)
						{
							if(tipodocc[1] == fileSended.Name)
							{
								if(tipodocc[0] == "fileProcedimientoSeguridad")
								{
									actividad.ProcedimientoSeguridad = fileSended.Url;
								}
								if(tipodocc[0] == "fileAnalisisRiesgo")
								{
									actividad.AnalisisRiesgo = fileSended.Url;
								}
							}
						}
					}
				}
				/*
				string[] files = base.SaveFiles(null);
				if(files == null || files.Count() <= 0) return;
				
				foreach(string file in files)
				{
					string name = file;
					string FileName = System.IO.Path.GetFileName(name);
					string pathLogos = System.IO.Path.Combine(Server.MapPath("~/"), path);
					string newLogo = System.IO.Path.Combine(pathLogos, FileName);
					if(System.IO.File.Exists(newLogo))
					{
						System.IO.File.Delete(newLogo);
					}
					System.IO.File.Move(name, newLogo);
					

				}*/
			}
			catch(Exception err)
			{
				OnError(err);
			}
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CActividades>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CActividades>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			IActividades entity = null;
			try
			{
				entity = new CActividades();
				SetIdEmpresaUser(entity);
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CActividades entity)
		{
			SetIdEmpresaUser(entity);
			SendEnviaDocumentos(entity);
			return EvaluaErrorInterno(base._UINew<CActividades>(entity));			
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CActividades>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CActividades entity)
		{
			SetIdEmpresaUser(entity);
			SendEnviaDocumentos(entity);			
			return EvaluaErrorInterno(base._UIEditar<CActividades>(entity.IdActividad, entity));
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CActividades>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CActividades>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CActividades>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CActividades>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CActividades>(id, where);
		}		
		[HttpGet]
		public ActionResult DescargaProcedimientoSeguridad(int id)
		{
			return DescargarFile(id, 0);
		}
		[HttpGet]
		public ActionResult DescargaAnalisisRiesgo(int id)
		{
			return DescargarFile(id, 1);
		}
		protected ActionResult DescargarFile(int id,int op)
		{
			ActionResult resp = null;
			try
			{
				DFile file = new DFile();
				ActividadesController documento = new ActividadesController();
				IActividades doc = documento.GetById(id);
				switch(op)
				{
					case 0:
						file.Url = doc.ProcedimientoSeguridad;
						break;
					case 1:
						file.Url = doc.AnalisisRiesgo;
						break;
				}				
				file = base.GetFile(file);
				resp = File(file.Contenido, "application/octet-stream", file.Name);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}	
		public IActividades GetById(int id)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Get", "Actividades"), id);
			return base._GetById<CActividades>(id, url);
		}
	}//class ActividadesController  ends.
}//Namespace Controladores.MVC ends.

