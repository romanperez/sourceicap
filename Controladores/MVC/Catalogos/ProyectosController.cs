using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ProyectosController : MvcBase
	{
		public ProyectosController()
		{
			_CatalogoNombre = "Proyectos";
			_InterfaceName = "IProyectos";
			_ViewEdicion = "IProyectosEditarNew";
		}
		#region PROTECTED
		protected IProyectos SetIdEmpresaUser(IProyectos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.IdUnidad = base.GetIdUnidad();
				entity.Empresa = base.GetEmpresa();
				entity.Unidad = base.GetUnidad();
			}
			return entity;
		}
		#endregion
		[HttpPost]
		public ActionResult ProyectoContratoTR(CContratos2 contratoTR)
		{
			IProyectoContratos pc = null;
			try
			{
				if(contratoTR != null)
				{
					pc = new CProyectoContratos();
					pc.Contrato = contratoTR.Contrato;
					pc.IdContrato = contratoTR.IdContrato;
					pc.IdCliente = contratoTR.IdCliente;
					pc.Estatus = true;
					pc.RazonSocial = contratoTR.RazonSocial;
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_ProyectoContratoTR", pc));
		}
		/// <summary>
		/// Obtienen la plantilla html para la nueva cotizacion.
		/// </summary>
		/// <param name="id">Id del menu que realiza la peticion.</param>
		/// <returns>Plantilla</returns>
		[HttpGet]
		[ActionName("IndexAll")]
		public ActionResult IndexAll(int id)
		{
			_CatalogoNombre = "ProyectosAll";
			_ControladorNombre = "Proyectos";
			_NombreToolBarView = "_ToolBarProyectosAll";
			return _UIIndex<CProyectos>("Index", "IndexPaginadoAll", id);
		}
		[HttpGet]
		public ActionResult IndexPaginadoAll(int id)
		{
			_CatalogoNombre = "ProyectosAll";
			_ControladorNombre = "Proyectos";
			_NombreToolBarView = "_ToolBarProyectosAll";
			return base._UIIndexPaginado<CProyectos>("IndexAll", "Buscar", id);

		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CProyectos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CProyectos>(id);
		}
		/// <summary>
		/// Obtiene la plantilla para modificar el proyecto
		/// </summary>
		/// <param name="id">id de la cotizacion asociada al proyecto</param>
		/// <returns>Plantilla html</returns>
		[HttpGet]
		public ActionResult New(int id)
		{
			IProyectos proyecto = null;
			if(id > 0)
			{
				proyecto = this.GetByCotizacion(id);
			}
			else
			{
				proyecto = new CProyectos();
				proyecto.FechaInicial = DateTime.Now;
				proyecto.FechaFinal = DateTime.Now;
			}
			SetIdEmpresaUser(proyecto);
			return base._UINew(_ViewEdicion, proyecto);
		}
		[HttpGet]
		public ActionResult NewHeader(int id)
		{
			ActionResult resp = null;
			try
			{
				IProyectos proyecto = null;
				if(id > 0)
				{
					proyecto = this.GetByCotizacion(id);
				}
				else
				{
					proyecto = new CProyectos();
					proyecto.FechaInicial = DateTime.Now;
					proyecto.FechaFinal = DateTime.Now;
				}
				SetIdEmpresaUser(proyecto);
				resp = PartialView(proyecto);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult New(CProyectos entity)
		{
			SetIdEmpresaUser(entity);
			return _UINew<CProyectos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CProyectos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CProyectos entity)
		{
			SetIdEmpresaUser(entity);
			if(entity.IdProyecto <= 0)
			{
				return this.New(entity);
			}
			else
			{
				return base._UIEditar<CProyectos>(entity.IdProyecto, entity);
			}

		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CProyectos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CProyectos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CProyectos>(id, where);
		}
		[HttpPost]
		[ActionName("SearchByEmpresa")]
		public ActionResult SearchByEmpresa(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CProyectos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CProyectos>(where);
		}
		[HttpPost]
		[ActionName("TopAll")]
		public ActionResult TopAll(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CProyectos>(where);
		}
		[HttpGet]
		public ActionResult MovimientosPedidos(int id)
		{
			ActionResult resp = null;
			try
			{
				MenusController mnContrato = new MenusController();
				IMenus mnuContrato = mnContrato.GetMenuByName("Contratos");

				CPedidoMovimientos index = new CPedidoMovimientos();
				string url = String.Format("{0}/{1}", GetUrlApiService("PedidosByProyecto", "Proyectos"), id);
				index.Pedidos = BuildPeticionVerb<List<CPedidos>>(url, Method.POST);
				url = String.Format("{0}/{1}", GetUrlApiService("MovimientosByProyecto", "Proyectos"), id);
				index.Movimientos = BuildPeticionVerb<List<CMovimientos>>(url, Method.POST);
				bool permisoConfirmar = this._HasPermiso(mnuContrato.IdMenu, "ConfirmarInsumosCantidad");
				ViewBag.ConfirmarInsumosCantidad = permisoConfirmar;
				resp = PartialView("PedidosMovimientos/_MovimientosPedidos", index);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CProyectos>(id, where);
		}
		protected IProyectos GetByCotizacion(int idCotizacion)
		{
			IProyectos newProyecto = null;
			try
			{
				CotizacionesController coti = new CotizacionesController();
				ICotizaciones icoti = coti.GetById(idCotizacion);
				newProyecto = new CProyectos();
				newProyecto.IdCotizacion = idCotizacion;
				newProyecto.FechaInicial = DateTime.Now;
				newProyecto.FechaFinal = DateTime.Now;
				newProyecto.IdCliente = icoti.IdCliente;
				newProyecto.Proyecto = (String.IsNullOrEmpty(icoti.Proyecto)) ? icoti.ProyectoInicial : icoti.Proyecto;
				newProyecto.Descripcion = icoti.DescripcionTitulo;
				List<IPropsWhere> whereE = new List<IPropsWhere>();
				whereE.Add(new CPropsWhere()
				{
					Prioridad = true,
					Condicion = "=",
					NombreCampo = NProyectos.eFields.IdCotizacion.ToString(),
					Valor = idCotizacion
				});
				IEnumerable<IProyectos> allProyectos = this._Search<CProyectos>(base.GetUrlApiService("SearchByCotizacion", null), whereE);
				if(allProyectos != null && allProyectos.Count() > 0)
				{
					newProyecto = allProyectos.FirstOrDefault();
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return newProyecto;
		}
		protected override ActionResult _UINew<Entity>(Entity entity)
		{
			this._New<Entity>(entity, GetUrlApiService("New", null));
			return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
		}
		[HttpGet]
		public ActionResult GeneraFromProyecto(int id)
		{
			ActionResult resp = null;
			try
			{
				CProyectos proyecto = this._GetById<CProyectos>(id, String.Format("{0}/{1}", GetUrlApiService("Get", null), id));
				IContratos2 entity = new CContratos2();
				entity.Proyecto = proyecto;
				if(proyecto != null)
				{
					entity.Empresa = proyecto.Empresa;
					entity.Unidad = proyecto.Unidad;
				}
				resp = PartialView("_ContratoProyectoHeader", entity);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult GeneraContrato(CContratos2 Entity)
		{
			ActionResult resp = null;
			try
			{
				RestSharp.IRestResponse response = BuildPeticionVerb(GetUrlApiService("GeneraContrato", "Contratos"), Entity, RestSharp.Method.POST);
				if(response != null)
				{
					Entity = CBase._ToJson<CContratos2>(response.Content);
				}
				resp = PartialView("_RespuestaOK");
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}

		[HttpPost]
		public ActionResult ContratrosByProyecto(int id)
		{
			IProyectos proyecto = null;
			ActionResult resp = null;
			try
			{
				proyecto = _GetById(id);
				resp = PartialView("_ContratrosByProyecto", proyecto);
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult AdministraProyecto(int id)
		{
			IProyectos proyecto = null;
			try
			{

				if(id <= 0)
				{
					SetError(TraduceTexto("IProyectoSinProyecto"));
				}
				proyecto = _GetById(id);
				if(proyecto == null)
				{
					SetError(TraduceTexto("IProyectoSinProyecto"));
				}
				else
				{
					proyecto.Contratos = new List<CProyectoContratos>();
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView(proyecto));
		}
		[HttpGet]
		public ActionResult AdministraProyectoAll(int id)
		{
			IProyectos proyecto = null;
			try
			{

				if(id <= 0)
				{
					SetError(TraduceTexto("IProyectoSinProyecto"));
				}
				proyecto = _GetById(id);
				if(proyecto == null)
				{
					SetError(TraduceTexto("IProyectoSinProyecto"));
				}
				else
				{
					proyecto.Contratos = new List<CProyectoContratos>();
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView(proyecto));
		}
		/// <summary>
		/// Obtiene el listado de contratos [vista VWCONTRATOS2]
		/// </summary>
		/// <param name="id">Id Proyecto</param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult GetContratos(int id)
		{
			ActionResult resp = null;
			IUICatalogo catalogo = null;
			try
			{
				MenusController mnContrato = new MenusController();
				ContratosController contratoController = new ContratosController();
				IMenus mnuContrato = mnContrato.GetMenuByName("Contratos");
				string url = String.Format("{0}/{1}", GetUrlApiService(), id);
				if(_Usuario != null && mnContrato != null)
				{
					_Usuario.IdMenuCaller = mnuContrato.IdMenu;
				}
				catalogo = this.BuildUICatalogo(contratoController._CatalogoNombre, contratoController._CatalogoNombre, _Usuario);
				List<CvwContratos2> contratos = base.BuildPeticionVerb<List<CvwContratos2>>(url, Method.GET);
				catalogo.Contenido = contratos;
				if(catalogo.Contenido == null)
				{
					catalogo.Contenido = new List<CvwContratos2>();
				}
				resp = PartialView(catalogo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult GetOrdenesTrabajo(int id)
		{
			ActionResult resp = null;
			IUICatalogo catalogo = null;
			try
			{
				MenusController mnContrato = new MenusController();
				OrdenesTrabajoController CtrOrdenes = new OrdenesTrabajoController();
				IMenus mnuContrato = mnContrato.GetMenuByName("OrdenesTrabajo");
				string url = String.Format("{0}/{1}", GetUrlApiService(), id);
				if(_Usuario != null && mnContrato != null)
				{
					_Usuario.IdMenuCaller = mnuContrato.IdMenu;
				}
				catalogo = this.BuildUICatalogo(CtrOrdenes._CatalogoNombre, CtrOrdenes._CatalogoNombre, _Usuario);
				List<COrdenesTrabajo> ordenes = base.BuildPeticionVerb<List<COrdenesTrabajo>>(url, Method.GET);
				catalogo.Contenido = ordenes;
				if(catalogo.Contenido == null)
				{
					catalogo.Contenido = new List<CvwContratos2>();
				}
				resp = PartialView(catalogo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult DetallesProyecto(int id)
		{
			ActionResult resp = null;
			try
			{
				IProyectos proyecto = _GetById(id);
				resp = PartialView(proyecto);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult AsignarCotizacion(int id)
		{
			ActionResult resp = null;
			try
			{
				IProyectos proyecto = _GetById(id);
				resp = PartialView(proyecto);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult SetCotizacionProyecto(CProyectos Entidad)
		{
			try
			{
				string url = GetUrlApiService();
				CProyectos p = BuildPeticionVerb<CProyectos>(url, Entidad, Method.POST);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
		}
		[HttpGet]
		public ActionResult GetInsumosByProyecto(int id)
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IvwContratos2> contratos = _GetInsumosByProyecto(id);
				resp = PartialView(contratos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult GetInsumosByProyectoConcepto(int id)
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IvwContratos2> contratos = _GetInsumosByProyectoConcepto(id);
				resp = PartialView("InsumosByProyecto/_RptContratos", contratos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}

		[HttpGet]
		public ActionResult GetInsumosByProyectoInsumo(int id)
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IvwContratos2> contratos = _GetInsumosByProyectoInsumos(id);
				resp = PartialView("InsumosByProyecto/_RptContratos", contratos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}

		[HttpPost]
		public ActionResult CantPedidoByProyecto(CPedidosDetalles entity)
		{
			List<CPedidosDetalles> pedidos = new List<CPedidosDetalles>();
			try
			{
				string url = GetUrlApiService();
				pedidos = this.BuildPeticionVerb<List<CPedidosDetalles>>(url, entity, Method.POST);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(pedidos);
		}
		[HttpGet]
		public ActionResult ConsultaInstaladoSolicitado()
		{
			ActionResult Resp = null;
			try
			{
				Resp = PartialView();
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpPost]
		public ActionResult ConsultaInstaladoSolicitado(CFiltrosInstaladoSolicitado entity)
		{
			ActionResult Resp = null;
			try
			{
				IEnumerable<IProyectos> allProyectos = _GetInsumosByProyectoAll(entity);
				Resp = PartialView("Consultas/_ConsultaInstaladoSolicitado", allProyectos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpGet]
		public ActionResult ParametrosFecha()
		{
			return PartialView("InstaladoFecha/_ParametrosFecha");
		}
		[HttpPost]
		public ActionResult InstaladoFechaConceptos(CProyectos Entity)
		{
			ActionResult Resp = null;
			try
			{
				IEnumerable<IContratosDetalles2> conceptos = _InstaladoFechaConceptos(Entity);
				Resp = PartialView("InstaladoFecha/_ConceptosInstaladosFecha", conceptos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpPost]
		public ActionResult InstaladoFechaInsumos(CProyectos Entity)
		{
			ActionResult Resp = null;
			try
			{
				IEnumerable<IContratosDetallesInsumos2> insumos = _InstaladoFechaInsumos(Entity);
				Resp = PartialView("InstaladoFecha/_InsumosInstaladosFecha", insumos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpGet]
		public ActionResult TipoCambioEstablecer()
		{
			ActionResult Resp = null;
			try
			{
				MonedasController mc = new MonedasController();
				IEnumerable<IMonedas> Monedas = mc.GetAllMonedasByEmpresa(GetIdEmpresa());
				Resp = PartialView("Costos/_EstablecerTiposCambios", Monedas);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		protected CDescargaReporte GeneraReporte(string NameFile, ETipoReporte Tipo, MProyectosCostos Modelo)//,string Url)//,int IdEmpresa,int IdUnidad)
		{
			CDescargaReporte reporte = null;
			Modelo.IdEmpresa = GetIdEmpresa();
			Modelo.IdUnidad = GetIdUnidad();
			List<CProyectos> conversion = this.BuildPeticionVerb<List<CProyectos>>(GetUrlApiService(), Modelo, Method.POST);
			if(conversion != null)
			{
				Reportes.RptProyectosCostos rpt = new Reportes.RptProyectosCostos();
				rpt.MonedaDestino = String.Format("{0} {1}", Modelo.MonedaDestino.MonedaDestino, Modelo.MonedaDestino.CodigoMonedaDestino);
				rpt.FechaTipoCambio = Modelo.MonedaDestino.FechaInicio.ToString("d");
				rpt.UrlBase = this.GetBaseUrlService();
				rpt.Peticion = MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile, rpt.FechaTipoCambio.Replace("//", "_"));
				TempData[reporte.Id] = rpt.GeneraReporte(Tipo, conversion, Modelo.TipoCambios, Modelo);
			}
			return reporte;
		}
		[HttpPost]
		public ActionResult ReporteCostosXls(MProyectosCostos Modelo)
		{
			CDescargaReporte reporte = null;
			//List<CProyectos> conversion = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.GeneraReporte("Costos_{0}.xls", ETipoReporte.Excel, Modelo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}
		[HttpPost]
		public ActionResult ReporteCostosPdf(MProyectosCostos Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.GeneraReporte("Costos_{0}.pdf", ETipoReporte.Pdf, Modelo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}
		[HttpPost]
		public ActionResult ConvierteImportes(MProyectosCostos Modelo)
		{
			List<CProyectos> conversion = null;
			try
			{
				Modelo.IdEmpresa = GetIdEmpresa();
				Modelo.IdUnidad = GetIdUnidad();
				conversion = this.BuildPeticionVerb<List<CProyectos>>(GetUrlApiService(), Modelo, Method.POST);
				if(conversion != null)
				{
					CUICatalogoProyectos pConvertido = new CUICatalogoProyectos();
					pConvertido.CalculaCostos(conversion);
					if(!String.IsNullOrEmpty(pConvertido.Error))
					{
						SetError(pConvertido.Error);
					}
					Modelo.Proyectos = conversion;
					Modelo.TotalGenerals = pConvertido.TGeneral.ToString("c4");
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(Modelo);
		}
		public IProyectos _GetById(int idProyecto)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Get", "Proyectos"), idProyecto);
			return this._GetById<CProyectos>(idProyecto, url);
		}
		public IEnumerable<IvwContratos2> _GetInsumosByProyecto(int IdProyecto)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("GetInsumosByProyecto", "Proyectos"), IdProyecto);
			return this.BuildPeticionVerb<List<CvwContratos2>>(url, Method.GET);
		}
		public IEnumerable<IvwContratos2> _GetInsumosByProyectoConcepto(int IdProyecto)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("GetInsumosByProyectoConcepto", "Proyectos"), IdProyecto);
			return this.BuildPeticionVerb<List<CvwContratos2>>(url, Method.GET);
		}
		public IEnumerable<IvwContratos2> _GetInsumosByProyectoInsumos(int IdProyecto)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("GetInsumosByProyectoInsumos", "Proyectos"), IdProyecto);
			return this.BuildPeticionVerb<List<CvwContratos2>>(url, Method.GET);
		}

		public IEnumerable<IProyectos> _GetInsumosByProyectoAll(CFiltrosInstaladoSolicitado filtros)
		{
			string url = GetUrlApiService("ConsultaInstaladoSolicitado", "Proyectos");
			return this.BuildPeticionVerb<List<CProyectos>>(url, filtros, Method.POST);
		}
		public IEnumerable<IContratosDetalles2> _InstaladoFechaConceptos(IProyectos proyecto)
		{
			string url = GetUrlApiService("InstaladoFechaConceptos", "Proyectos");
			return this.BuildPeticionVerb<List<CContratosDetalles2>>(url, proyecto, Method.POST);
		}
		public IEnumerable<IContratosDetalles2> _SolicitudInsumosInstaladoFechaFiltro(IProyectos proyecto)
		{
			string url = GetUrlApiService("SolicitudInsumosInstaladoFechaFiltro", "Proyectos");
			return this.BuildPeticionVerb<List<CContratosDetalles2>>(url, proyecto, Method.POST);
		}
		public IEnumerable<IContratosDetallesInsumos2> _InstaladoFechaInsumos(IProyectos proyecto)
		{
			string url = GetUrlApiService("InstaladoFechaInsumos", "Proyectos");
			return this.BuildPeticionVerb<List<CContratosDetallesInsumos2>>(url, proyecto, Method.POST);
		}
		#region FiltrosConsultas
		[HttpGet]
		public ActionResult GetFiltroContratos(int id)
		{
			ActionResult Resp = null;
			try
			{
				IProyectos iProyecto = this._GetById(id);
				IEnumerable<IContratosDetalles2> contratos = null;
				if(iProyecto != null)
				{
					contratos = iProyecto.Contratos.GroupBy(i => new
					{
						IdContrato = i.IdContrato,
						Contrato = i.Contrato
					}).Select(i => new CContratosDetalles2()
					{
						IdContrato = i.Key.IdContrato,
						Contrato = i.Key.Contrato
					});
				}
				Resp = PartialView("Consultas/_FiltroContratos", contratos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpGet]
		public ActionResult GetFiltroUbicacion(int id)
		{
			ActionResult Resp = null;
			try
			{
				string url = GetUrlApiService("SearchAll", "ContratosDetalles2");
				IProyectos iProyecto = this._GetById(id);
				List<IContratosDetalles2> contratos = null;
				List<string> Ubicaciones = new List<string>();
				IEnumerable<CContratosDetalles2> result = null;
				if(iProyecto != null && iProyecto.Contratos!=null)
				{
					NContratosDetalles2 cd = new NContratosDetalles2();
					IEnumerable<IPropsWhere> where = cd.GetConceptosByIdContrato(iProyecto.Contratos.Select(x => new CContratosDetalles2()
					{
						IdContrato = x.IdContrato
					}));					
					if(where != null)
					{
						result = BuildPeticionVerb<List<CContratosDetalles2>>(url, where, Method.POST);
						if(result != null)
						{
							Ubicaciones = result.GroupBy(x => x.Ubicacion).Select(x=>x.Key).ToList();
							if(Ubicaciones != null)
							{
								contratos = new List<IContratosDetalles2>();
								Ubicaciones.ForEach(x =>
								{
									contratos.Add(new CContratosDetalles2()
									{
										IdContrato=-1,
										Contrato=x
									});
								});
							}
						}
					}
				}
				Resp = PartialView("Consultas/_FiltroUbicaciones", contratos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		#endregion
		#region CONCEPTOS_POR_CONTRATO
		[HttpPost]
		public ActionResult ConceptosPorContratosView(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;
			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosPorContratos", "Proyectos"), Entity, Method.POST);
				arAccion = PartialView("_TablaConceptosContratos", conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}
		[HttpPost]
		public ActionResult ConceptosPorContratos(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;

			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
				arAccion = PartialView(conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}
		[HttpPost]
		public ActionResult ReporteConceptosContratosXls(CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratos_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 1);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}

		[HttpPost]
		public ActionResult ReporteConceptoContratosPdf(CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratos_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 1);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}
		#endregion
		#region CONCEPTOS_POR_CONTRATO_INSTALADO
		[HttpPost]
		public ActionResult ConceptosPorContratosInstaladoView(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;
			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosPorContratosInstalado", "Proyectos"), Entity, Method.POST);
				arAccion = PartialView("_TablaConceptosContratos", conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}
		[HttpPost]
		public ActionResult ConceptosPorContratosInstalado(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;

			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
				arAccion = PartialView(conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}
		[HttpPost]
		public ActionResult ReporteConceptosContratosInstaladoXls(CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.ReporteConceptosContratosInstalados(String.Format("ConceptoContratos_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}

		[HttpPost]
		public ActionResult ReporteConceptoContratosInstaladoPdf(CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.ReporteConceptosContratosInstalados(String.Format("ConceptoContratos_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}
		#endregion
		#region CONCEPTOS_POR_CONTRATO_SOLICITADO_INSTALADO
		[HttpPost]
		public ActionResult ConceptosPorContratosSolicitadoInstalado(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;

			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
				arAccion = PartialView(conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}

		[HttpPost]
		public ActionResult ConceptosPorContratosSolicitadoInstaladoBoton(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;

			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosPorContratosSolicitadoInstalado", "Proyectos"), Entity, Method.POST);
				arAccion = PartialView(conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}

		public ActionResult ReporteConceptosContratosSolicitadoInstaladoXls(CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.ReporteConceptosContratosSolicitadoInstalado(String.Format("ConceptoContratosSolicitadoInstalado_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 1);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}

		[HttpPost]
		public ActionResult ReporteConceptoContratosSolicitadoInstaladoPdf(CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			try
			{
				ProyectosReportes pr = new ProyectosReportes();
				pr.Controlador = this;
				reporte = pr.ReporteConceptosContratosSolicitadoInstalado(String.Format("ConceptoContratosSolicitadoInstalado_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 1);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}
		#endregion
		#region CONCEPTOS_POR_CONTRATO_SOLICITADO_ESTADO
		[HttpPost]
		public ActionResult ConceptosPorContratosSolicitadoEstado(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;

			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
				arAccion = PartialView(conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}

		[HttpPost]
		public ActionResult ConceptosPorContratosSolicitadoEstadoDetalle(CContratosDetalles2 Entity)
		{
			IEnumerable<IContratosDetalles2> conversion = null;
			ActionResult arAccion = null;

			try
			{
				conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosPorContratosSolicitadoEstado", "Proyectos"), Entity, Method.POST);
				arAccion = PartialView(conversion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(arAccion);
		}

        [HttpPost]
        public ActionResult ReporteConceptosContratosSolicitadoEstadoXls(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratosSolicitadoEstado_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 2);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }

        [HttpPost]
        public ActionResult ReporteConceptoContratosSolicitadoEstadoPdf(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratosSolicitadoEstado_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 2);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
		#endregion
        #region CONCEPTOS_POR_CONTRATO_SOLICITADO_AREA
        [HttpPost]
        public ActionResult ConceptosPorContratosSolicitadoArea(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
                arAccion = PartialView(conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ConceptosPorContratosSolicitadoAreaDetalle(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosPorContratosSolicitadoArea", "Proyectos"), Entity, Method.POST);
                arAccion = PartialView(conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ReporteConceptosContratosSolicitadoAreaXls(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratosSolicitadoArea_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 3);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }

        [HttpPost]
        public ActionResult ReporteConceptoContratosSolicitadoAreaPdf(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratosSolicitadoArea_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 3);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        #endregion
        #region CONCEPTOS_POR_CONTRATO_INSTALADO_AREA
        [HttpPost]
        public ActionResult ConceptosPorContratosInstaladoArea(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
                arAccion = PartialView(conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ConceptosPorContratosInstaladoAreaDetalle(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosPorContratosInstaladoArea", "Proyectos"), Entity, Method.POST);
                arAccion = PartialView("ConceptosPorContratosSolicitadoAreaDetalle",conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ReporteConceptosContratosInstaladoAreaXls(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratosInstaladoArea_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 4);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }

        [HttpPost]
        public ActionResult ReporteConceptoContratosInstaladoAreaPdf(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoContratosInstaladoArea_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 4);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        #endregion
        #region CONCEPTOS_POR_CONTRATO_SOLICITADO_AREA
        [HttpPost]
        public ActionResult ConceptosSolicitadoEstadoArea(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
                arAccion = PartialView(conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ConceptosSolicitadoEstadoAreaDetalle(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosSolicitadoEstadoArea", "Proyectos"), Entity, Method.POST);
					 arAccion = PartialView("ConceptosSolicitadoEstadoDetalleArea", conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ReporteConceptosSolicitadoEstadoAreaXls(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoSolicitadoEstadoArea_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 5);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }

        [HttpPost]
        public ActionResult ReporteConceptosSolicitadoEstadoAreaPdf(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratos(String.Format("ConceptoSolicitadoEstadoArea_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 5);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        #endregion	
        #region CONCEPTOS_SOLICITADO_INSTALADO_AREA
        [HttpPost]
        public ActionResult ConceptosSolicitadoInstaladoArea(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService(), Entity, Method.POST);
                arAccion = PartialView(conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        [HttpPost]
        public ActionResult ConceptosSolicitadoInstaladoBotonArea(CContratosDetalles2 Entity)
        {
            IEnumerable<IContratosDetalles2> conversion = null;
            ActionResult arAccion = null;

            try
            {
                conversion = this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("ConceptosSolicitadoInstaladoArea", "Proyectos"), Entity, Method.POST);
                arAccion = PartialView(conversion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(arAccion);
        }

        public ActionResult ReporteConceptosSolicitadoInstaladoAreaXls(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratosSolicitadoInstalado(String.Format("ConceptoSolicitadoInstaladoUbicacion_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Excel, Modelo, 2);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }

        [HttpPost]
        public ActionResult ReporteConceptosSolicitadoInstaladoAreaPdf(CContratosDetalles2 Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                ProyectosReportes pr = new ProyectosReportes();
                pr.Controlador = this;
                reporte = pr.ReporteConceptosContratosSolicitadoInstalado(String.Format("ConceptoSolicitadoInstaladoUbicacion_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")), ETipoReporte.Pdf, Modelo, 2);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        #endregion
		  #region Solicitud_Insumos_Instalados
		  protected IEnumerable<IContratosDetallesInsumos2> GetInsumos(IEnumerable<IvwContratos2> contratos, string filtro)
		  {
			  if(contratos == null) return null;
			  List<IContratosDetallesInsumos2> insumos = new List<IContratosDetallesInsumos2>();
			  string[] idContratos = null;
			  bool addContrato = false;
			  if(!String.IsNullOrEmpty(filtro))
			  {
				  idContratos = filtro.Split(',');
			  }
			  contratos.ToList().ForEach(c =>
			  {
				  if(idContratos != null && idContratos.Contains(c.IdContrato.ToString()))
				  {
					  addContrato = true;
				  }
				  else if(String.IsNullOrEmpty(filtro))
				  {
					  addContrato = true;
				  }
				  else
				  {
					  addContrato = false;
				  }
				  if(addContrato)
				  {
					  if(c.Conceptos != null)
					  {
						  c.Conceptos.ForEach(co =>
						  {
							  if(co.Insumos != null)
							  {
								  co.Insumos.ForEach(i =>
								  {
									  insumos.Add(i);
								  });
							  }
						  });
					  }
				  }
			  });
			  if(insumos != null)
			  {
				  return insumos.GroupBy(i => new
				  {
					  IdInsumo = i.IdInsumo,
					  Codigo = i.Codigo,
					  Descripcion = i.Descripcion,
					  Unidad = i.Unidad
				  }).Select(i => new CContratosDetallesInsumos2()
				  {
					  IdInsumo = i.Key.IdInsumo,
					  Codigo = i.Key.Codigo,
					  Descripcion = i.Key.Descripcion,
					  Unidad = i.Key.Unidad,
					  CantidadEjecutada = i.Sum(x => x.CantidadEjecutada),
					  CantidadProgramada = i.Sum(x => x.CantidadProgramada)
				  }).OrderBy(i => i.Codigo);
			  }
			  return null;
		  }
		  public ActionResult SolicitudInsumosInstaladoProgramadoFiltro(CContratosDetalles2 Entity)
		  {
			  ActionResult arAccion = null;
			  try
			  {
				  IEnumerable<IvwContratos2> contratos = _GetInsumosByProyecto(Entity.IdContrato);
				  arAccion = PartialView("SolicitudInsumosInstaladosDetalle", GetInsumos(contratos, Entity.Contrato));
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInterno(arAccion);
		  }
		  public ActionResult SolicitudInsumosInstaladoProgramado(CContratosDetalles2 Entity)
		  {
			  ActionResult arAccion = null;
			  try
			  {
				  IEnumerable<IvwContratos2> contratos = _GetInsumosByProyecto(Entity.IdContrato);
				  arAccion = PartialView(GetInsumos(contratos, null));
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInterno(arAccion);
		  }
		  [HttpPost]
		  public ActionResult SolicitudInsumosInstaladoProgramadoXls(CContratosDetalles2 Modelo)
		  {
			  CDescargaReporte reporte = null;
			  try
			  {
				  ProyectosReportes pr = new ProyectosReportes();
				  pr.Controlador = this;
				  IEnumerable<IvwContratos2> contratos = _GetInsumosByProyecto(Modelo.IdContrato);
				  IEnumerable<IContratosDetallesInsumos2> insumos = GetInsumos(contratos, Modelo.Contrato);
				  reporte = pr.ReporteSolicitudInsumosInstaladoProgramado(String.Format("InsumosInstalados_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")),
																							 ETipoReporte.Excel,
																							 Modelo,
																							 insumos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInternoJSON(reporte);
		  }

		  [HttpPost]
		  public ActionResult SolicitudInsumosInstaladoProgramadoPdf(CContratosDetalles2 Modelo)
		  {
			  CDescargaReporte reporte = null;
			  try
			  {
				  ProyectosReportes pr = new ProyectosReportes();
				  pr.Controlador = this;
				  IEnumerable<IvwContratos2> contratos = _GetInsumosByProyecto(Modelo.IdContrato);
				  IEnumerable<IContratosDetallesInsumos2> insumos = GetInsumos(contratos, Modelo.Contrato);
				  reporte = pr.ReporteSolicitudInsumosInstaladoProgramado(String.Format("InsumosInstalados_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")),
																							 ETipoReporte.Pdf,
																							 Modelo,
																							 insumos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInternoJSON(reporte);
		  }
		  #endregion

		 #region Solicitud_Insumos_Instalados_Fecha
		  protected IEnumerable<IContratosDetallesInsumos2> GetInsumosFecha(IEnumerable<IContratosDetalles2> contratos, string filtro)
		  {
			  if(contratos == null) return null;
			  List<IContratosDetallesInsumos2> insumos = new List<IContratosDetallesInsumos2>();
			  string[] idContratos = null;
			  bool addContrato = false;
			  if(!String.IsNullOrEmpty(filtro))
			  {
				  idContratos = filtro.Split(',');
			  }
			  contratos.ToList().ForEach(c =>
			  {
				  if(idContratos != null && idContratos.Contains(c.IdContrato.ToString()))
				  {
					  addContrato = true;
				  }
				  else if(String.IsNullOrEmpty(filtro))
				  {
					  addContrato = true;
				  }
				  else
				  {
					  addContrato = false;
				  }
				  if(addContrato)
				  {									
						if(c.Insumos != null)
						{
							c.Insumos.ForEach(i =>
							{
								insumos.Add(i);
							});
						}											  
				  }
			  });
			  if(insumos != null)
			  {
				  return insumos.GroupBy(i => new
				  {
					  IdInsumo = i.IdInsumo,
					  Codigo = i.Codigo,
					  Descripcion = i.Descripcion,
					  Unidad = i.Unidad
				  }).Select(i => new CContratosDetallesInsumos2()
				  {
					  IdInsumo = i.Key.IdInsumo,
					  Codigo = i.Key.Codigo,
					  Descripcion = i.Key.Descripcion,
					  Unidad = i.Key.Unidad,
					  CantidadEjecutada = i.Sum(x => x.CantidadEjecutada),
					  CantidadProgramada = i.Sum(x => x.CantidadProgramada)
				  }).OrderBy(i => i.Codigo);
			  }
			  return null;
		  }
		  public ActionResult SolicitudInsumosInstaladoFechaFiltro(CContratosDetalles2 Entity)
		  {
			  ActionResult arAccion = null;
			  try
			  {
				  IProyectos pp = new CProyectos();
				  pp.IdProyecto = Entity.IdContrato;
				  pp.FechaInicial = Entity.Fecha;
				  pp.FechaFinal = Entity.FechaFinal;
				  IEnumerable<IContratosDetalles2> contratos = _SolicitudInsumosInstaladoFechaFiltro(pp);
				  arAccion = PartialView("SolicitudInsumosInstaladosDetalleFecha", GetInsumosFecha(contratos, Entity.Contrato));
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInterno(arAccion);
		  }
		  public ActionResult SolicitudInsumosInstaladoFecha(CContratosDetalles2 Entity)
		  {
			  ActionResult arAccion = null;
			  try
			  {
				  IProyectos pp = new CProyectos();
				  pp.IdProyecto = Entity.IdContrato;
				  pp.FechaInicial = DateTime.Now;
				  pp.FechaFinal = DateTime.Now;
				  IEnumerable<IContratosDetalles2> contratos = _InstaladoFechaConceptos(pp);
				  arAccion = PartialView(GetInsumosFecha(contratos, null));
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInterno(arAccion);
		  }
		  [HttpPost]
		  public ActionResult SolicitudInsumosInstaladoFechaXls(CContratosDetalles2 Modelo)
		  {
			  CDescargaReporte reporte = null;
			  try
			  {
				  ProyectosReportes pr = new ProyectosReportes();
				  pr.Controlador = this;				  
				  IProyectos pp = new CProyectos();
				  pp.IdProyecto = Modelo.IdContrato;
				  pp.FechaInicial = Modelo.Fecha;
				  pp.FechaFinal = Modelo.FechaFinal;
				  IEnumerable<IContratosDetalles2> contratos = _SolicitudInsumosInstaladoFechaFiltro(pp);
				  IEnumerable<IContratosDetallesInsumos2> insumos = GetInsumosFecha(contratos, Modelo.Contrato);
				  reporte = pr.ReporteSolicitudInsumosInstaladoFecha(String.Format("InsumosInstalados_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")),
																							 ETipoReporte.Excel,
																							 Modelo,
																							 insumos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInternoJSON(reporte);
		  }

		  [HttpPost]
		  public ActionResult SolicitudInsumosInstaladoFechaPdf(CContratosDetalles2 Modelo)
		  {
			  CDescargaReporte reporte = null;
			  try
			  {
				  ProyectosReportes pr = new ProyectosReportes();
				  pr.Controlador = this;
				  IProyectos pp = new CProyectos();
				  pp.IdProyecto = Modelo.IdContrato;
				  pp.FechaInicial = Modelo.Fecha;
				  pp.FechaFinal = Modelo.FechaFinal;
				  IEnumerable<IContratosDetalles2> contratos = _SolicitudInsumosInstaladoFechaFiltro(pp);
				  IEnumerable<IContratosDetallesInsumos2> insumos = GetInsumosFecha(contratos, Modelo.Contrato);
				  reporte = pr.ReporteSolicitudInsumosInstaladoFecha(String.Format("InsumosInstalados_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")),
																							 ETipoReporte.Pdf,
																							 Modelo,
																							 insumos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInternoJSON(reporte);
		  }
		  #endregion

		#region Estimaciones
		#region EstimacionesPorContrato
		  protected IEnumerable<IContratosDetalles2> _GetEstimacionesPorContrato(CContratosDetalles2 Entity)
		  {			  
			  return this.BuildPeticionVerb<List<CContratosDetalles2>>(GetUrlApiService("EstimacionesPorContratoIndex", "Estimaciones"), Entity, Method.POST);
		  }
		  protected ActionResult _EstimacionesPorContrato(string view, CContratosDetalles2 Entity)
		  {			  
			  ActionResult arAccion = null;
			  try
			  {
				  arAccion = PartialView(view, _GetEstimacionesPorContrato(Entity));
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInterno(arAccion);
		  }
		  [HttpPost]
		  public ActionResult EstimacionesPorContratoIndex(CContratosDetalles2 Entity)
		  {
			  return _EstimacionesPorContrato("EstimacionesPorContratoIndex", Entity);
		  }
		  [HttpPost]
		  public ActionResult EstimacionesPorContrato(CContratosDetalles2 Entity)
		  {
			  return _EstimacionesPorContrato("EstimacionesPorContrato", Entity);
		  }
		  [HttpPost]
		  public ActionResult EstimacionesPorContratoXls(CContratosDetalles2 Modelo)
		  {
			  CDescargaReporte reporte = null;
			  try
			  {
				  ProyectosReportes pr = new ProyectosReportes();
				  pr.Controlador = this;
				  IEnumerable<IContratosDetalles2> conceptos = _GetEstimacionesPorContrato(Modelo);
				  reporte = pr.ReporteEstimacionesPorContrato(String.Format("Estimaciones_{0}_{1}.xls", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")),
																			ETipoReporte.Excel,
																			Modelo,
																			conceptos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInternoJSON(reporte);
		  }

		  [HttpPost]
		  public ActionResult EstimacionesPorContratoPdf(CContratosDetalles2 Modelo)
		  {
			  CDescargaReporte reporte = null;
			  try
			  {
				  ProyectosReportes pr = new ProyectosReportes();
				  pr.Controlador = this;
				  IEnumerable<IContratosDetalles2> conceptos = _GetEstimacionesPorContrato(Modelo);
				  reporte = pr.ReporteEstimacionesPorContrato(String.Format("Estimaciones_{0}_{1}.pdf", Modelo.Descripcion, DateTime.Now.ToString("ddMMyyyy")),
																			ETipoReporte.Pdf,
																			Modelo,
																			conceptos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return EvaluaErrorInternoJSON(reporte);
		  }
		#endregion
		#endregion

	}//class ProyectosController  ends.
}//Namespace Controladores.MVC ends.
