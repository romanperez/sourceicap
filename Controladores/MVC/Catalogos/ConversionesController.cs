using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ConversionesController : MvcBase
	{
		public ConversionesController()
		{
			_CatalogoNombre = "Conversiones";
			_InterfaceName = "IConversiones";
			_ViewEdicion = "IConversionesEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CConversiones>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CConversiones>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CConversiones entity)
		{
			return base._UINew<CConversiones>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CConversiones>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CConversiones entity)
		{
			return base._UIEditar<CConversiones>(entity.IdConversion, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CConversiones>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CConversiones>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CConversiones>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CConversiones>(where);
		}
		[HttpPost]
		public ActionResult ConversionMoneda(CTipoCambios Entity)
		{
			try
			{								
				Entity = _ConversionMoneda(Entity) as CTipoCambios;
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInternoJSON(Entity);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CConversiones>(id, where);
		}

		public ITipoCambios _ConversionMoneda(ITipoCambios Entity)
		{
			try
			{
				Entity = base.BuildPeticionVerb<CTipoCambios>(base.GetUrlApiService("ConversionMoneda", "Conversiones"), Entity, Method.POST);				
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return Entity;
		}
	}//class ConversionesController  ends.
}//Namespace Controladores.MVC ends.

