using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
using System.IO;
using Reportes;
using Microsoft.Reporting.WebForms;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class CotizacionesController : MvcBase
	{
		public CotizacionesController()
		{
			_CatalogoNombre = "Cotizaciones";
			_InterfaceName = "ICotizaciones";
			_ViewEdicion = "ICotizacionesEditarNew";
		}
		#region PROTECTED
		protected ICotizaciones SetIdEmpresaUser(ICotizaciones entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
				entity.IdUnidad = base.GetIdUnidad();
				entity.Unidad = base.GetUnidad();
			}
			return entity;
		}
		public virtual ICotizaciones _Cotizacion(ICotizaciones Entidad) 
		{
			return this._Cotizacion(Entidad , base.GetUrlApiService("TipoIvaTipoCambio", null));
		}
		public virtual ICotizaciones _Cotizacion(ICotizaciones Entidad, string url) 
		{
			try
			{
				RestSharp.IRestResponse response = BuildPeticionVerb(url, Entidad, RestSharp.Method.POST);
				EvaluaRespuestaWebServie(response);
				if(response != null)
				{
					Entidad = Entidad.ToJson<CCotizaciones>(response.Content);
				}
				
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return Entidad;
		}

		public IMonedas GetPesos(int IdEmpresa)
		{
			MonedasController mPredeterminada = new MonedasController();
			return mPredeterminada.GetPesos(IdEmpresa);
		}
		public IIvaValores GetIvaByFecha(IIvaValores iva)
		{
			IvaValoresController civa = new IvaValoresController();
			return civa.GetIvaByFecha(iva);
		}

	/*	protected virtual ICotizaciones _RealizaConvercion(ICotizaciones Cotizacion)
		{
			try
			{
				RestSharp.IRestResponse response = BuildPeticionVerb(base.GetUrlApiService(), Cotizacion, RestSharp.Method.POST);
				if(response != null)
				{
					Cotizacion = CBase._ToJson<CCotizaciones>(response.Content);
				}
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return Cotizacion;
		}*/
		#endregion
		[HttpPost]
		[ActionName("PlantillaTablaAdd")]
		public ActionResult PlantillaTablaAdd(CCotizaciones ParamCotizacion)
		{						
			ICotizacionesDetalles entity=null;
			//int idTipoCambio = 0;
			double utilidad = 0;
			try
			{
				ParamCotizacion = SetIdEmpresaUser(ParamCotizacion as ICotizaciones)as CCotizaciones ;
				if(ParamCotizacion.IdCotizacion>0)
				{
					ICotizaciones ParamCotizacion2 = GetById(ParamCotizacion.IdCotizacion);
					ParamCotizacion.Utilidad = ParamCotizacion2.Utilidad;
				}
				//ConversionesController control = new ConversionesController();
				//ITipoCambios conversion = new CTipoCambios();				
				double.TryParse(ParamCotizacion.Utilidad.ToString(), out utilidad);
				entity = ParamCotizacion.Conceptos.First();
				//int idMonedaOriginal =entity.IdMoneda;
				IMonedas monedaPesos = this.BuildPeticionVerb<CMonedas>(String.Format("{0}/{1}", GetUrlApiService("GetPesos", "Monedas"), ParamCotizacion.IdEmpresa), Method.GET);
				/*if(cotizacion.IdMoneda <= 0 || moneda == null)
				{
					SetError(TraduceTexto("ICotizacionSeleccioneMoneda"),true);
				}*/
				if(utilidad<=0)
				{
					SetError(TraduceTexto("ICotizacionUtilidadZero"), true);
				}
				entity.IdMoneda = monedaPesos.IdMoneda;
				entity.Moneda = monedaPesos.Moneda;
				entity.CodigoMoneda = monedaPesos.Codigo;
				//if(idMonedaOriginal != cotizacion.IdMoneda)
				//{
				//	int.TryParse(cotizacion.IdTipoCambio.ToString(), out idTipoCambio);
				//	conversion.IdTipoCambio = idTipoCambio;
				//	conversion.IdMonedaOrigen = idMonedaOriginal; //De pesos A
				//	conversion.IdMonedaDestino = monedaPesos.IdMoneda; //Moneda destino[Dolar,euro ,etc]
				//	conversion.Cantidad = entity.Precio;
				//	conversion = control._ConversionMoneda(conversion);
				//	if(control.HasError())
				//	{
				//		SetError(control.GetError(), true);
				//	}
				//	if(conversion != null)
				//	{
				//		entity.Precio = conversion.Cantidad;						
				//	}		
				//}				
				entity.Precio = NCotizaciones.GetUtilidad(entity.Precio, utilidad);
				entity.Cantidad = 1;
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_PlantillaConceptoTR", entity));
		}
		/// <summary>
		/// Obtienen la plantilla html para la nueva cotizacion.
		/// </summary>
		/// <param name="id">Id del menu que realiza la peticion.</param>
		/// <returns>Plantilla</returns>
		[HttpGet]
		[ActionName("IndexAll")]
		public ActionResult IndexAll(int id)
		{
			_CatalogoNombre = "CotizacionesAll";
			_ControladorNombre = "Cotizaciones";
			_NombreToolBarView = "_ToolBarCotizacionesAll";
			return _UIIndex<CCotizaciones>("Index", "IndexPaginadoAll", id);			
		}
		/// <summary>
		/// Obtienen la plantilla html para la nueva cotizacion.
		/// </summary>
		/// <param name="id">Id del menu que realiza la peticion.</param>
		/// <returns>Plantilla</returns>
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CCotizaciones>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginadoAll(int id)
		{
			_CatalogoNombre = "CotizacionesAll";
			_ControladorNombre = "Cotizaciones";
			_NombreToolBarView = "_ToolBarCotizacionesAll";
			return base._UIIndexPaginado<CCotizaciones>("IndexAll", "Buscar", id);

		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CCotizaciones>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CCotizaciones entity = null;
			try
			{
				IIvaValores iva = new CIvaValores();
				IMonedas pesos = null;
				entity = new CCotizaciones();
				entity.Fecha = DateTime.Now;
				entity.Utilidad = 0;
				SetIdEmpresaUser(entity);
				iva.IdEmpresa = entity.IdEmpresa;
				iva.FechaInicio = entity.Fecha;
				iva = GetIvaByFecha(iva);
				pesos = GetPesos(entity.IdEmpresa);
				if( iva != null && pesos !=null)
				{
					entity.IdIva = iva.IdIva;
					entity.Iva = iva.Porcentaje;
					entity.IdMoneda = pesos.IdMoneda;
					entity.Moneda = pesos.Moneda;
					entity.CodigoMoneda = pesos.Codigo;
				}
				if(iva == null)
				{
					SetError(TraduceTexto("NCotizacionesNoIvaValido"),true);
				}
				if(pesos == null)
				{
					SetError(TraduceTexto("NCotizacionesNoDefinicionMXN"), true);
				}
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		[HttpPost]
		public ActionResult New(CCotizaciones entity)
		{
			//SetIdEmpresaUser(entity);
			//return base._UINew<CCotizaciones>(entity);
			ActionResult resp = null;
			try
			{
				SetIdEmpresaUser(entity);				
				entity = this._New<CCotizaciones>(entity, GetUrlApiService());
				if(!HasError())
				{
					resp = Editar(entity.IdCotizacion);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{			
			ViewBag.LabelCreate = Idiomas.IcapWeb.CatalogoCrearEntidad;
			string url = String.Format("{0}/{1}", GetUrlApiService("GetValidaciones", null), id);
			CCotizaciones Entidad = this._GetById<CCotizaciones>(id, url);
			ViewBag.Action = "Put";
			return EvaluaErrorInterno("_ErrorInModal", PartialView(_ViewEdicion, Entidad));
		}
		[HttpGet]
		public ActionResult EditarDatosAdicionales(int id)
		{
			_ViewEdicion = "EditarDatosAdicionales";
			return Editar(id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CCotizaciones entity)
		{
			//SetIdEmpresaUser(entity);
			//return base._UIEditar<CCotizaciones>(entity.IdCotizacion, entity);
			ActionResult resp = null;
			try
			{
				string url = String.Format("{0}/{1}", GetUrlApiService(), entity.IdCotizacion);
				SetIdEmpresaUser(entity);
				entity = this._Put<CCotizaciones>(entity.IdCotizacion,url,entity);
				if(!HasError())
				{
					resp = Editar(entity.IdCotizacion);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult PutDatosComplementarios(CCotizaciones entity)
		{
			CCotizaciones original = GetById(entity.IdCotizacion) as CCotizaciones;
			original.Cotizacion = entity.Cotizacion;
			original.Solicitud = entity.Solicitud;
			original.ProyectoInicial = entity.ProyectoInicial;
			original.DescripcionTitulo = entity.DescripcionTitulo;
			original.DescripcionCompleta = entity.DescripcionCompleta;
			original.DepartamentoAtencion = entity.DepartamentoAtencion;
			original.FechaEntrega = entity.FechaEntrega;
			original.PersonalClienteAtencion = entity.PersonalClienteAtencion;
			original.PersonalClienteContacto = entity.PersonalClienteContacto;
			string url = String.Format("{0}/{1}", GetUrlApiService("Put", "Cotizaciones"), entity.IdCotizacion);
			return base._UIEditar<CCotizaciones>(url, entity.IdCotizacion, original);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			ViewBag.LabelDelete = Idiomas.IcapWeb.CatalogoBorrarEntidad;
			string url = String.Format("{0}/{1}", GetUrlApiService("GetValidaciones", null), id);
			CCotizaciones Entidad = this._GetById<CCotizaciones>(id, url);
			return EvaluaErrorInterno("_ErrorInModal", PartialView(Entidad));
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CCotizaciones>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CCotizaciones>(id, where);
		}
		[HttpPost]
		[ActionName("SearchByEmpresa")]
		public ActionResult SearchByEmpresa(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CCotizaciones>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CCotizaciones>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CCotizaciones>(id, where);
		}

		[HttpPost]
		public ActionResult InsumosByConceptos(int id,CPropsWhere where)
		{
			IEnumerable<IConceptosInsumos> Insumos = null;
			try
			{
				int idCotizacion = id;
				//CDataTipoCambio dataTipo = _GetDatosMoneda(Request);
				//TipoCambiosController tipo = new TipoCambiosController();
				//ITipoCambios tipoc = tipo.GetById(dataTipo.IdTipoCambio);
				Insumos = GetInsumosByConcepto(idCotizacion, where);
				//Insumos = new NConceptosInsumos().ConvierteInsumos(dataTipo, tipoc, Insumos);
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInterno(PartialView("../Shared/ExplosionInsumos/_ExplosionInsumos", Insumos));
		}		
		[HttpPost]
		public ActionResult InsumosByCapitulos(int id,IEnumerable<CPropsWhere>  where)
		{
			IEnumerable<IConceptosInsumos> insumosList = null;
			try
			{
				//CDataTipoCambio dataTipo = _GetDatosMoneda(Request);
				//TipoCambiosController tipo = new TipoCambiosController();
				//ITipoCambios tipoc = tipo.GetById(dataTipo.IdTipoCambio);
				insumosList = _InsumosByCapitulos(id, where);
				//insumosList = new NConceptosInsumos().ConvierteInsumos(dataTipo,tipoc,insumosList);
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInterno(PartialView("_InsumosByCapitulo", insumosList));
		}
		[HttpPost]
		public ActionResult ConceptosByCapitulo(int id,CPropsWhere where)
		{			
			IEnumerable<IConceptos> conceptosAgrupados = null;
			try
			{
				//CDataTipoCambio dataTipo = _GetDatosMoneda(Request);
				//TipoCambiosController tipo = new TipoCambiosController();
				//ITipoCambios tipoc = tipo.GetById(dataTipo.IdTipoCambio);
				//IMonedas moneda = GetPredeterminada(GetIdEmpresa());
				conceptosAgrupados = GetConceptos(id, where);
				ICotizaciones cotizacion = null;
				int IdEmpresa = GetIdEmpresa();
				IMonedas monedaPesos = this.BuildPeticionVerb<CMonedas>(String.Format("{0}/{1}", GetUrlApiService("GetPesos", "Monedas"), IdEmpresa), Method.GET);
				if(id > 0)
				{
					cotizacion = GetById(id);					
					conceptosAgrupados.ToList().ForEach(x => x.Moneda = monedaPesos.Moneda);
					//if(cotizacion.IdMoneda == moneda.IdMoneda) //cotizacion original en son pesos
					//{
					//	if(dataTipo.IdMoneda == tipoc.IdMoneda) //se convierte a dolares
					//	{
					//		conceptosAgrupados = new NConceptos().ConceptosToDollar(tipoc, conceptosAgrupados);
					//		conceptosAgrupados.ToList().ForEach(x => x.Moneda = dataTipo.Moneda);
					//	}
					//	else
					//	{
					//		conceptosAgrupados.ToList().ForEach(x => x.Moneda = moneda.Moneda);
					//	}
					//}
					//if(cotizacion.IdMoneda == tipoc.IdMoneda) //cotizacion original en son dolares
					//{
					//	if(dataTipo.IdMoneda == moneda.IdMoneda) //se convierte a pesos
					//	{
					//		conceptosAgrupados = new NConceptos().ConceptosToPesos(tipoc, conceptosAgrupados);
					//		conceptosAgrupados.ToList().ForEach(x => x.Moneda = dataTipo.Moneda);
					//	}
					//	else
					//	{
					//		conceptosAgrupados.ToList().ForEach(x => x.Moneda = tipoc.Moneda);
					//	}
					//}
				}				
				else
				{
					conceptosAgrupados.ToList().ForEach(x => x.Moneda = monedaPesos.Moneda);
					/*if(dataTipo.IdMoneda == tipoc.IdMoneda)
					{
						conceptosAgrupados = new NConceptos().ConceptosToDollar(tipoc, conceptosAgrupados);
					}
					else
					{
						conceptosAgrupados.ToList().ForEach(x => x.Moneda = dataTipo.Moneda);
					}*/
				}			
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInterno(PartialView("_ConceptosGroupByCapitulo", conceptosAgrupados));
		}		
		
		[HttpGet]
		public ActionResult CopiarCotizacion(int id)
		{
			try
			{
				CCotizaciones coti = GetById(id)as CCotizaciones;
				SetIdEmpresaUser(coti);
				coti = NCotizaciones.CopiaCotizacion(coti as ICotizaciones) as CCotizaciones;
				this._New<CCotizaciones>(coti, GetUrlApiService("New",null));
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView());
		}	
		[HttpPost]
		public ActionResult NewXls(CCotizaciones entity)
		{			
			try
			{
				double utilidad = 0;
				IMonedas pesos = null;
				IIvaValores iva = new CIvaValores();				
				NExcel<CCotizaciones> xls = new NExcel<CCotizaciones>();				
				entity.Fecha = DateTime.Now;
				SetIdEmpresaUser(entity);												
				iva.IdEmpresa = entity.IdEmpresa;
				iva.FechaInicio = entity.Fecha;
				iva = GetIvaByFecha(iva);
				pesos = GetPesos(entity.IdEmpresa);
				double.TryParse(entity.Utilidad.ToString(), out utilidad);
				if(iva != null && pesos != null)
				{
					entity.IdIva = iva.IdIva;
					entity.Iva = iva.Porcentaje;
					entity.IdMoneda = pesos.IdMoneda;
					entity.Moneda = pesos.Moneda;
					entity.CodigoMoneda = pesos.Codigo;
				}
				if(iva == null)
				{
					SetError(TraduceTexto("NCotizacionesNoIvaValido"), true);
				}
				if(utilidad <= 0)
				{
					SetError(TraduceTexto("ICotizacionUtilidadZero"), true);
				}
				else
				{
					IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
					if(filesLoaded != null && filesLoaded.Count() > 0)
					{
						System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
						new NCotizaciones().GetConceptosNombre(entity, dt);
					}
					entity = this.BuildPeticionVerb<CCotizaciones>(this.GetUrlApiService("FindByConcepto", "Conceptos"), entity, RestSharp.Method.POST);
					if(entity != null && entity.Conceptos != null)
					{
						entity.Conceptos.ForEach(x =>
						{
							x.Precio = NCotizaciones.GetUtilidad(x.Precio, utilidad);
						});
					}
				}				
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		//public PartialViewResult ImprimeCotizacion23(int id)
		//{
		//	ICotizaciones coti = GetById(id) ;
		//	coti.Conceptos.ForEach(x =>
		//	{
		//		x.Insumos = coti.Insumos.Where(i => i.IdConcepto == x.IdConcepto).ToList();
		//	});
		//	return ImprimeCotizacion(coti as CCotizaciones);
		//}
		/*public PartialViewResult ImprimeCotizacion(CCotizaciones cotizacion)
		{
			return PartialView("ImprimeCotizacion", cotizacion);			
		}*/
		[HttpGet]
		public ActionResult ImprimeCotizacion(int id)
		{
			ICotizaciones coti = GetById(id);
			coti.Conceptos.ForEach(x =>
			{
				x.Insumos = coti.Insumos.Where(i => i.IdConcepto == x.IdConcepto).ToList();
			});
			CDescargaReporte reporte = new CDescargaReporte();
			reporte.Id = Guid.NewGuid().ToString();
			reporte.Nombre = String.Format("{0}.pdf", coti.Cotizacion);
			RptImprimeCotizacion rdlc = new RptImprimeCotizacion();
			rdlc.UrlBase = this.GetBaseUrlService();
			rdlc.Peticion = MVC.MvcBase.BuildPeticionService;
			TempData[reporte.Id] = rdlc.GeneraReporte(id, new ReportViewer());
			return EvaluaErrorInternoJSON(reporte);
		}
		[HttpGet]
		public override ActionResult PlantillaLoad()
		{
			bool addConceptos = false;
			ActionResult resp = null;
			try
			{
				MenusController mnu = new MenusController();
				IMenus menu= mnu.GetMenuByName("Cotizaciones");				
				if(menu != null && this._HasPermiso(menu.IdMenu, "ConceptoExcelCotizacion"))
				{
					addConceptos = true;
				}
				ViewBag.PermisoAddConceptos = addConceptos;
				resp = base.PlantillaLoad();				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		//public ActionResult ImprimeCotizacion2(int id)
		//{
		//	//return PartialView();
		//	string html ;
		//	ICotizaciones coti = GetById(id);
		//	using(var sw = new StringWriter())
		//	{
		//		PartialViewResult result = ImprimeCotizacion23(id);
		//		result.View = ViewEngines.Engines.FindPartialView(ControllerContext, "ImprimeCotizacion").View;
		//		ViewContext vc = new ViewContext(ControllerContext, result.View, result.ViewData, result.TempData, sw);
		//		result.View.Render(vc, sw);
		//		html = sw.GetStringBuilder().ToString();
		//	}
		//	return base.Pdf(html);
		//}
		public ActionResult PreviewCotizacion(int id)
		{
			ActionResult resp = null;
			try
			{
				EmpresasController ctrl = new EmpresasController();				
				ICotizaciones cotizacion = GetDetalleById(id);
				ViewBag.Empresa = ctrl.GetEmpresaById(cotizacion.IdEmpresa);				
				resp = PartialView(cotizacion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		protected IEnumerable<IConceptosInsumos> GetInsumosByConcepto(int idCotizacion,CPropsWhere where)
		{
			IEnumerable<IConceptosInsumos> Insumos = null;
			try
			{
				NCotizaciones n = new NCotizaciones();
				IEnumerable<IPropsWhere> filtro = null;
				if(idCotizacion <= 0)
				{					
					filtro = n.InsumosByConcepto(where.Valor.ToString());
					ConceptosInsumosController insumos = new ConceptosInsumosController();
					Insumos = insumos.GetInsumosByConcepto(filtro).OrderBy(x => x.IdConcepto);
				}
				else
				{
					IEnumerable<ICotizacionesDetallesInsumos> insumosCotizacion = null;
					filtro = n.InsumosByConceptoCotizacion(idCotizacion);
					CotizacionesDetallesInsumosController insumos = new CotizacionesDetallesInsumosController();
					insumosCotizacion = insumos.GetInsumosByConcepto(filtro).OrderBy(x => x.IdConcepto);
					Insumos = from tbl in insumosCotizacion
									  select new CConceptosInsumos()
									  {
										  Cantidad = tbl.Cantidad,
										  Codigo = tbl.Codigo,
										  CostoInsumo = tbl.Costo,
										  Descripcion = tbl.Descripcion,
										  IdCapitulo = tbl.IdCapitulo,
										  IdConcepto = tbl.IdConcepto,
										  IdInsumo = tbl.IdInsumo,
										  Moneda = tbl.Moneda,
										  IdMoneda = tbl.IdMoneda,
										  Unidad = tbl.Unidad
									  };
				}
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return Insumos;
		}
		protected IEnumerable<IConceptos> GetConceptos(int idCotizacion, CPropsWhere where)
		{
			IEnumerable<IConceptos> Conceptos = null;
			try
			{
				NCotizaciones n = new NCotizaciones();
				IEnumerable<IPropsWhere> filtro = null;
				if(idCotizacion <= 0)
				{
					filtro = n.ConceptosByCapitulo(where.Valor.ToString());
					IEnumerable<IConceptos> ConceptosAux = this._Search<CConceptos>(base.GetUrlApiService("SearchAll", "Conceptos"), filtro);
					Conceptos = n.GroupByCapitulo(ConceptosAux);
				}
				else
				{										
					filtro = n.ConceptosByCapitulo(idCotizacion);
					IEnumerable<ICotizacionesDetalles> ConceptosAux = this._Search<CCotizacionesDetalles>(base.GetUrlApiService("SearchAll", "CotizacionesDetalles"), filtro);
					IEnumerable<IConceptos> conceptos1 = from tbl in ConceptosAux
																	 select new CConceptos()
																	 {
																		 Capitulo=tbl.Capitulo,
																		 Concepto=tbl.Concepto,
																		 Descripcion=tbl.Descripcion,
																		 IdCapitulo=tbl.IdCapitulo,
																		 IdConcepto=tbl.IdConcepto,
																		 Precio=tbl.Precio
																	 };
					Conceptos = n.GroupByCapitulo(conceptos1);
				}
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return Conceptos;
		}
		protected IEnumerable<IConceptosInsumos> _InsumosByCapitulos(int IdCotizacion, IEnumerable<CPropsWhere> where)
		{
			IEnumerable<IConceptosInsumos> Insumos = null;
			try
			{
				NCotizaciones n = new NCotizaciones();
				IEnumerable<IPropsWhere> filtro = null;
				if(IdCotizacion <= 0)
				{
					filtro = n.InsumosByCapitulo(where);							
					ConceptosInsumosController insumos = new ConceptosInsumosController();					
					Insumos = insumos.GetInsumosByConcepto(filtro).OrderBy(x => x.IdConcepto);
				}
				else
				{					
					filtro = n.InsumosByCapitulo(IdCotizacion, where);
					CotizacionesDetallesInsumosController insumos = new CotizacionesDetallesInsumosController();
					IEnumerable<ICotizacionesDetallesInsumos> insumosCotizacion = insumos.GetInsumosByConcepto(filtro).OrderBy(x => x.IdConcepto);
					Insumos = from tbl in insumosCotizacion
								 select new CConceptosInsumos()
								 {
									 Cantidad = tbl.Cantidad,
									 Codigo = tbl.Codigo,
									 CostoInsumo = tbl.Costo,
									 Descripcion = tbl.Descripcion,
									 IdCapitulo = tbl.IdCapitulo,
									 IdConcepto = tbl.IdConcepto,
									 IdInsumo = tbl.IdInsumo,
									 Moneda = tbl.Moneda,
									 IdMoneda = tbl.IdMoneda,
									 Unidad =tbl.Unidad
								 };
				}
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return Insumos;
		}
		
		public ICotizaciones GetById(int IdCotizacion)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Get", "Cotizaciones"), IdCotizacion);
			return this._GetById<CCotizaciones>(IdCotizacion, url);
		}
		public ICotizaciones GetDetalleById(int IdCotizacion)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("GetDetalleCotizacion", "Cotizaciones"), IdCotizacion);
			ICotizaciones cotizacion = base.BuildPeticionVerb<CCotizaciones>(url, Method.GET);			
			return cotizacion;
		}


	}//class CotizacionesController  ends.
}//Namespace Controladores.MVC ends.