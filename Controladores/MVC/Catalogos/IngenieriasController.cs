using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class IngenieriasController   : MvcBase
	{
		public IngenieriasController ()
		{			
			_CatalogoNombre = "Ingenierias";
			_InterfaceName = "IIngenierias";
			_ViewEdicion = "IIngenieriasEditarNew";			
		}
		#region PROTECTED
		protected IIngenierias SetIdEmpresaUser(IIngenierias entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.Empresa = GetEmpresa();
				entity.Unidad = GetUnidad();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CIngenierias>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CIngenierias>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			IIngenierias entity = null;
			try
			{
				entity = new CIngenierias();
				entity.Fecha = DateTime.Now;
				SetIdEmpresaUser(entity);
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}		
		[HttpPost]
        public ActionResult New(CIngenierias entity)
		{           
			  return base._UINew<CIngenierias>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CIngenierias>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CIngenierias entity)
		{
			SetIdEmpresaUser(entity);	
			return base._UIEditar<CIngenierias>(entity.IdIngenieria, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CIngenierias>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CIngenierias>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CIngenierias>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CIngenierias>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CIngenierias>(id, where);
	}
   }//class IngenieriasController  ends.
}//Namespace Controladores.MVC ends.

