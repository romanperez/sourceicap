using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class OTConceptosController   : MvcBase
	{
		public OTConceptosController ()
		{			
			_CatalogoNombre = "OTConceptos";
			_InterfaceName = "IOTConceptos";
			_ViewEdicion = "IOTConceptosEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<COTConceptos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<COTConceptos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
      public ActionResult New(COTConceptos entity)
		{           
			  return base._UINew<COTConceptos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<COTConceptos>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(COTConceptos entity)
		{            
				return base._UIEditar<COTConceptos>(entity.IdOTConcepto, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<COTConceptos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<COTConceptos>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<COTConceptos>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<COTConceptos>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<COTConceptos>(id, where);
	}
	[HttpPost]
	public ActionResult RegistraEjecucion(COTConceptosEjecucion entity)
	{
		this._New<COTConceptosEjecucion>(entity, GetUrlApiService("New", "OTConceptosEjecucion"));
		return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
	}
	[HttpGet]
	public ActionResult DeleteEjecucion(int id)
	{
		if(id <= 0)
		{
			return PartialView("_RespuestaOK");
		}
		this._Delete(id,String.Format("{0}/{1}", GetUrlApiService("Delete", "OTConceptosEjecucion"),id));
		return EvaluaErrorInterno(PartialView("_RespuestaOK", _ResponseWS));
	}
   }//class OTConceptosController  ends.
}//Namespace Controladores.MVC ends.

