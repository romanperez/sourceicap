using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ContratosController : MvcBase
	{
		public ContratosController()
		{
			_CatalogoNombre = "Contratos";
			_InterfaceName = "IContratos2";
			_ViewEdicion = "IContratosEditarNew";
		}
		#region PROTECTED
		protected IContratos2 SetIdEmpresaUser(IContratos2 entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = GetIdEmpresa();
				entity.Empresa = GetEmpresa();
				entity.IdUnidad = GetIdUnidad();
				entity.Unidad = GetUnidad();
			}
			return entity;
		}
		#endregion
		[HttpPost]
		[ActionName("GetPrecioConceptos")]
		public ActionResult GetPrecioConceptos(CContratos2 entity)
		{
			ActionResult resp = null;
			try
			{
				if(entity.IdContrato <= 0)
				{					
					string url = String.Empty;
					NContratos contratoNew = new NContratos();
					List<CPropsWhere> where= contratoNew.BuscaPreciosConceptos(entity);
					url = GetUrlApiService("SearchAll", "Conceptos");
					IEnumerable<IConceptos> allConceptos = BuildPeticionVerb<List<CConceptos>>(url, where, Method.POST);
					NContratos.ToContratoDetalles(entity.Conceptos, allConceptos);
					NContratos.ToContratoDetalles(entity.Adicionales, allConceptos);
				}
				else
				{
					entity = GetById(entity.IdContrato) as CContratos2;
				}				
				resp = PartialView(entity);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		[ActionName("PlantillaTablaAdd")]
		public ActionResult PlantillaTablaAdd(int id,CContratos2 entity)
		{
			IContratosDetalles2 concepto = null;
			double utilidad = 0;
			ActionResult resp = null;
			try
			{
				if(entity.IdContrato > 0)
				{
					IContratos2 entity2 = GetById(entity.IdContrato);
					entity.Utilidad = entity2.Utilidad;
				}
				entity = SetIdEmpresaUser(entity as IContratos2) as CContratos2;
				double.TryParse(entity.Utilidad.ToString(), out utilidad);
				if(utilidad <= 0)
				{
					SetError(TraduceTexto("IContratoUtilidadZero"), true);
				}
				concepto = entity.Conceptos.First();
				IMonedas monedaPesos = this.BuildPeticionVerb<CMonedas>(String.Format("{0}/{1}", GetUrlApiService("GetPesos", "Monedas"), entity.IdEmpresa), Method.GET);
				concepto.IdMoneda = monedaPesos.IdMoneda;
				concepto.Moneda = monedaPesos.Moneda;
				concepto.CodigoMoneda = monedaPesos.Codigo;
				concepto.Precio = NCotizaciones.GetUtilidad(concepto.Precio, utilidad);
				concepto.Cantidad = 1;
				string tipo = (id == 0) ? "C" : "A";
				ViewData["PrefijoTR"] = tipo;
				resp = PartialView("_PlantillaConceptoTR", concepto);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CContratos2>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CContratos2>(id);
		}
		[HttpPost]
		public ActionResult NewProyecto(CProyectos EntityProyecto)
		{
			IContratos2 entity = null;
			try
			{				
				CotizacionesController coti = new CotizacionesController();
				IIvaValores iva = new CIvaValores();
				IMonedas pesos = null;				
				entity = new CContratos2();
			   //entity.Aprobado = false;
				entity.Fecha = DateTime.Now;
				entity.IdCliente = EntityProyecto.IdCliente;
				entity.RazonSocial = EntityProyecto.RazonSocial;
				SetIdEmpresaUser(entity);
				iva.IdEmpresa = entity.IdEmpresa;
				iva.FechaInicio = entity.Fecha;
				iva = coti.GetIvaByFecha(iva);
				pesos = coti.GetPesos(entity.IdEmpresa);

				if(iva != null && pesos != null)
				{
					entity.IdIva = iva.IdIva;
					entity.Iva = iva.Porcentaje;
					entity.IdMoneda = pesos.IdMoneda;
					entity.Moneda = pesos.Moneda;
					entity.CodigoMoneda = pesos.Codigo;
				}
				if(iva == null)
				{
					SetError(TraduceTexto("NCotizacionesNoIvaValido"), true);
				}
				if(pesos == null)
				{
					SetError(TraduceTexto("NCotizacionesNoDefinicionMXN"), true);
				}
				ViewData["ModificaCliente"] = false;
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		[HttpGet]
		public ActionResult New()
		{
			IContratos2 entity = null;
			try
			{
				CotizacionesController coti = new CotizacionesController();
				IIvaValores iva = new CIvaValores();
				IMonedas pesos = null;
				//CotizacionesController cotizacion = new CotizacionesController();
				//ICotizaciones cotiAux = new CCotizaciones();
				entity = new CContratos2();
				//entity.Aprobado = false;
				entity.Fecha = DateTime.Now;
				SetIdEmpresaUser(entity);
				iva.IdEmpresa = entity.IdEmpresa;
				iva.FechaInicio = entity.Fecha;
				iva = coti.GetIvaByFecha(iva);
				pesos = coti.GetPesos(entity.IdEmpresa);
				if(iva != null && pesos != null)
				{
					entity.IdIva = iva.IdIva;
					entity.Iva = iva.Porcentaje;
					entity.IdMoneda = pesos.IdMoneda;
					entity.Moneda = pesos.Moneda;
					entity.CodigoMoneda = pesos.Codigo;
				}
				if(iva == null)
				{
					SetError(TraduceTexto("NCotizacionesNoIvaValido"), true);
				}
				if(pesos == null)
				{
					SetError(TraduceTexto("NCotizacionesNoDefinicionMXN"), true);
				}
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		[HttpPost]
		public ActionResult New(CContratos2 entity)
		{
			//SetIdEmpresaUser(entity);
			//return base._UINew<CContratos2>(entity);
			ActionResult resp = null;
			try
			{
				SetIdEmpresaUser(entity);
				entity = this._New<CContratos2>(entity, GetUrlApiService());
				if(!HasError())
				{
					resp = Editar(entity.IdContrato);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CContratos2>(_ViewEdicion, id);
		}		
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CContratos2 entity)
		{
			//SetIdEmpresaUser(entity);
			//return base._UIEditar<CContratos2>(entity.IdContrato, entity);
			ActionResult resp = null;
			try
			{
				string url = String.Format("{0}/{1}", GetUrlApiService(), entity.IdContrato);
				SetIdEmpresaUser(entity);
				entity = this._Put<CContratos2>(entity.IdContrato, url, entity);
				if(!HasError())
				{
					resp = Editar(entity.IdContrato);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CContratos2>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CContratos2>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CContratos2>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CContratos2>(where);
		}
		[HttpPost]
		[ActionName("TopSinProyecto")]
		public ActionResult TopSinProyecto(IEnumerable<CPropsWhere> where)
		{			
			return base._Top<CContratos2>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CContratos2>(id, where);
		}
		[HttpGet]
		public ActionResult ConfirmaSolicitudInsumosContrato()
		{			
			ActionResult resp = null;
			try
			{				
				resp = PartialView("_AprobarContrato");
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);			
		}
		[HttpGet]
		public ActionResult SolicitarInsumosByProyecto(int id)
		{
			ActionResult resp = null;
			try
			{			
				string url = String.Format("{0}/{1}", GetUrlApiService(), id);
				CProyectos p = BuildPeticionVerb<CProyectos>(url, Method.POST);				
				EnviaEmail(id);
				resp = PartialView("_RespuestaOK", _ResponseWS);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		
		public ActionResult PreviewContrato(int id)
		{
			ActionResult resp = null;
			try
			{
				EmpresasController ctrl = new EmpresasController();
				IContratos2 contrato = GetDetalleById(id);
				ViewBag.Empresa = ctrl.GetEmpresaById(contrato.IdEmpresa);
				resp = PartialView(contrato);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult ObtienePrecios(CContratos2 entity)
		{			
			try
			{				
				if(entity.IdContrato > 0)
				{
					IContratos2 entity2 = GetById(entity.IdContrato);
					entity.Utilidad = entity2.Utilidad;
				}
				SetIdEmpresaUser(entity);
				string url = GetUrlApiService("ObtienePrecios", "Contratos");
				entity = base.BuildPeticionVerb<CContratos2>(url, entity, Method.POST);				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(entity);
		}
		[HttpPost]
		public ActionResult GeneraCotizacionFromContrato(CContratos2 entity)
		{
			try
			{				
				string url = GetUrlApiService();
				entity = base.BuildPeticionVerb<CContratos2>(url, entity, Method.POST);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_RespuestaOK"));
		}
		[HttpPost]
		public ActionResult CotizacionesGeneradas(CContratos2 entity)
		{
			ActionResult resp = null;
			try
			{
				string url = GetUrlApiService();
				List<CContratosCotizaciones> all = base.BuildPeticionVerb<List<CContratosCotizaciones>>(url, entity, Method.POST);
				resp = PartialView(all);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
#region ESTIMACIONES
        [HttpGet]
        public ActionResult ReporteEstimacionResumenEconomicoParametros(int id)
        {
            return PartialView("Estimaciones/ReporteEstimacionResumenEconomicoParametros", new CEstimacionesResumenEconomico() { IdContrato = id });
        }
		[HttpPost]
		public ActionResult EstimacionesPorContrato(int id, List<CEstimaciones> estimaciones)
		{
			ActionResult resp = null;
			try
			{
				IEstimaciones estimacion = base.BuildPeticionVerb<CEstimaciones>(String.Format("{0}/{1}", GetUrlApiService("EstimacionesPorContrato", "Estimaciones"), id), estimaciones, Method.POST);
				if(estimacion != null && estimacion.IdEstimacion > 0)
				{
					ContratosController contratos = new ContratosController();
					resp = contratos.EstimacionesPorProyecto(id);
				}
				else
				{
					resp = PartialView("_RespuestaERR");
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult EstimacionesPorProyecto(int id)
		{
			ActionResult resp = null;
			try
			{
                string numeroEstimacion =(Request != null && Request.QueryString != null && Request.QueryString["Estimacion"]!= null)? Request.QueryString["Estimacion"].ToString():"0";
                int NumeroEstimacion = Convert.ToInt32(numeroEstimacion);
                IContratos2 contrato = GetEstimacionesPorContrato(id);
                CModeloEstimaciones modeloEstimacion = new CModeloEstimaciones(NumeroEstimacion, contrato);
                resp = PartialView("Estimaciones/_EstimacionesConceptos", modeloEstimacion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
        [HttpPost]
        public ActionResult SendNewEstimacion(CEstimaciones entity)
        {
            ActionResult resp = null;
            try
            {
                IContratos2 contrato = _SendNewEstimacion(entity);
                CModeloEstimaciones modeloEstimacion = new CModeloEstimaciones(contrato);                
                resp = PartialView("Estimaciones/_EstimacionesConceptos", modeloEstimacion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInterno(resp);
        }
        [HttpGet]
        public ActionResult AddEstimacion(int id)
        {
            IEstimaciones estimacion = new CEstimaciones();
            estimacion.FechaInicio = DateTime.Now;
            estimacion.FechaFin = DateTime.Now;
            estimacion.IdContratosDetalles = id; /*En este caso se asigna el Id del contrato al cual se le desea crear una nueva estimacion.*/
            return PartialView("Estimaciones/_AddEstimacion",estimacion);
        }
        [HttpPost]
        public ActionResult ValidaEstimacionFecha(CEstimaciones Entity)
        {            
            try
            {
                Entity = ValidaPeriodoEstimacion(Entity) as CEstimaciones;                
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(Entity);
        }
		[HttpGet]
		public ActionResult ContratoEstimacion(int id)
		{
			ActionResult resp = null;
			try
			{				
				//IContratos2 contrato = GetEstimacionesPorContrato(id);
				//resp = PartialView("Estimaciones/ContratoEstimacion",contrato);                
                IContratos2 contrato = GetEstimacionesPorContrato(id);
                CModeloEstimaciones modeloEstimacion = new CModeloEstimaciones(1,contrato);
                resp = PartialView("Estimaciones/ContratoEstimacionIndex", modeloEstimacion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
#endregion
		public IContratos2 GetEstimacionesPorContrato(int IdContrato)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("GetEstimacionesPorContrato", "ContratosDetalles2"), IdContrato);
			return base.BuildPeticionVerb<CContratos2>(url, Method.GET);
		}
        public IContratos2 _SendNewEstimacion(IEstimaciones entity)
        {
            string url = String.Format("{0}", GetUrlApiService("NewEstimacion", "Estimaciones"));
            return base.BuildPeticionVerb<CContratos2>(url, entity, Method.POST);
        }
        public IEstimaciones ValidaPeriodoEstimacion(IEstimaciones estimacion)
        {
            string url = String.Format("{0}", GetUrlApiService("ValidaPeriodoEstimacion", "Estimaciones"));
            return base.BuildPeticionVerb<CEstimaciones>(url,estimacion, Method.POST);
        }
		public IContratos2 GetDetalleById(int IdContrato)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("GetDetalleContrato", "ContratosDetalles2"), IdContrato);
			return base.BuildPeticionVerb<CContratos2>(url, Method.GET);			
		}
		public IContratos2 GetById(int IdContrato)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Get", "Contratos"), IdContrato);
			return base.BuildPeticionVerb<CContratos2>(url, Method.GET);
		}
		public virtual void EnviaEmail(int IdProyecto)
		{
			if(IdProyecto <= 0) return;
			string url = String.Format("{0}/{1}", GetUrlApiService("InsumosSinPedido", "Proyectos"), IdProyecto);
			IProyectos proyecto = base.BuildPeticionVerb<CProyectos>(url, Method.POST);
			if(proyecto.IdProyecto > 0)
			{
				ProyectosController pc = new ProyectosController();
				IProyectos proyectoSolicitar = pc._GetById(proyecto.IdProyecto);				
				url = String.Format("{0}/{1}", GetUrlApiService("DetalleSolicitudInsumos", "Proyectos"), proyecto.IdProyecto);
				List<CPedidosDetalles> detalles = base.BuildPeticionVerb<List<CPedidosDetalles>>(url, Method.POST);
				string body = base.ViewToString(PartialView("_InsumosEmail", detalles), "_InsumosEmail");
				url =  GetUrlApiService("EnviaEmail", "Proyectos");
				CEmail mail = new CEmail();
				mail.Body = body;
				mail.isHtml = true;
				mail.Asunto = proyectoSolicitar.Proyecto;
				mail = base.BuildPeticionVerb<CEmail>(url, mail,Method.POST);				
			}
		}
	}//class ContratosController  ends.
}//Namespace Controladores.MVC ends.

