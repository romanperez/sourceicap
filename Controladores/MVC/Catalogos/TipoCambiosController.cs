using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class TipoCambiosController : MvcBase
	{
		public TipoCambiosController()
		{
			_CatalogoNombre = "TipoCambios";
			_InterfaceName = "ITipoCambios";
			_ViewEdicion = "ITipoCambiosEditarNew";
		}
		#region PROTECTED
		protected ITipoCambios SetIdEmpresaUser(ITipoCambios entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CTipoCambios>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CTipoCambios>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CTipoCambios entity = null;
			try
			{
				entity = new CTipoCambios();
				entity.FechaInicio = DateTime.Now;
				entity.FechaFin = DateTime.Now;
				entity.Empresa = base.GetEmpresa();
				entity.Estatus = true;
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CTipoCambios entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CTipoCambios>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CTipoCambios>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CTipoCambios entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CTipoCambios>(entity.IdTipoCambio, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CTipoCambios>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CTipoCambios>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CTipoCambios>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CTipoCambios>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CTipoCambios>(id, where);
		}
		[HttpPost]
		public ActionResult TipoCambioByFecha(CTipoCambios Entity)
		{
			try
			{
				Entity.IdEmpresa = GetIdEmpresa();
				Entity = GetTipoCambioByFecha(Entity) as CTipoCambios;
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInternoJSON(Entity);
		}
		/// <summary>
		/// Obtiene el porcentaje de iva correspondiente a una fecha determinada.
		/// </summary>
		/// <param name="IdEmpresa">Identificador de la empresa.</param>
		/// <returns>Objeto IvaValor</returns>
		public ITipoCambios GetTipoCambioByFecha(ITipoCambios Entity)
		{
			try
			{
				Entity = base.BuildPeticionVerb<CTipoCambios>(base.GetUrlApiService("TipoCambioByFecha", "TipoCambios"), Entity, Method.POST);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return Entity;
		}


		//public ITipoCambios GetById(int id)
		//{
		//	string url = String.Format("{0}/{1}", GetUrlApiService("Get", "TipoCambios"), id); //AKIIII ME KEDE FALTA TIPOS DE CAMBIOS
		//	return base._GetById<CTipoCambios>(id, url) as ITipoCambios;
		//}
	}//class TipoCambiosController  ends.
}//Namespace Controladores.MVC ends.

