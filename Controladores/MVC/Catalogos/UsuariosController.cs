using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class UsuariosController   : MvcBase
	{
		public UsuariosController ()
		{			
			_CatalogoNombre = "Usuarios";
			_InterfaceName = "IUsuarios";
			_ViewEdicion = "IUsuariosEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CUsuarios>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CUsuarios>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CUsuarios entity)
		{           
			  return base._UINew<CUsuarios>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CUsuarios>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CUsuarios entity)
		{            
				return base._UIEditar<CUsuarios>(entity.IdUsuario, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CUsuarios>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CUsuarios>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CUsuarios>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CUsuarios>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CUsuarios>(id, where);
	}
   }//class UsuariosController  ends.
}//Namespace Controladores.MVC ends.

