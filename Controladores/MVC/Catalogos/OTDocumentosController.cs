using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class OTDocumentosController : MvcBase
	{
		public OTDocumentosController()
		{
			_CatalogoNombre = "OTDocumentos";
			_InterfaceName = "IOTDocumentos";
			_ViewEdicion = "IOTDocumentosEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<COTDocumentos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<COTDocumentos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(COTDocumentos entity)
		{
			return base._UINew<COTDocumentos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<COTDocumentos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(COTDocumentos entity)
		{
			return base._UIEditar<COTDocumentos>(entity.IdOTDocumentos, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<COTDocumentos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			ActionResult resp = null;
			try
			{
				IOTDocumentos doc=GetById(id);
				DFile ff= new DFile();
 				ff.Url=doc.RutaDocumento;
				resp = base._UIEliminaById(id);
				DelFile(ff);
			}
			catch(Exception ee)
			{
				OnError(ee);
			}
			return resp;
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<COTDocumentos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<COTDocumentos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<COTDocumentos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<COTDocumentos>(id, where);
		}
		public IOTDocumentos GetById(int id)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Get", "OTDocumentos"), id);
			return base._GetById<COTDocumentos>(id, url);
		}
	}//class OTDocumentosController  ends.
}//Namespace Controladores.MVC ends.

