using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ContratosDetallesController : MvcBase
	{
		public ContratosDetallesController()
		{
			_CatalogoNombre = "ContratosDetalles";
			_InterfaceName = "IContratosDetalles";
			_ViewEdicion = "IContratosDetallesEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CContratosDetalles>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CContratosDetalles>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CContratosDetalles entity)
		{
			return base._UINew<CContratosDetalles>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CContratosDetalles>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CContratosDetalles entity)
		{
			return base._UIEditar<CContratosDetalles>(entity.IdContratoDetalle, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CContratosDetalles>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CContratosDetalles>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CContratosDetalles>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CContratosDetalles>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CContratosDetalles>(id, where);
		}
		[HttpPost]
		public ActionResult ContratoDetalles(CContratos contrato)
		{			
			List<CContratosDetalles> res = new List<CContratosDetalles>();
			try
			{				
				RestSharp.IRestResponse response = BuildPeticionVerb(GetUrlApiService(),contrato, RestSharp.Method.POST);
				if(response != null)
				{
					res = new List<CContratosDetalles>();
					res = CBase._ToJson<List<CContratosDetalles>>(response.Content);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_ConeptosToContrato", res));
		}
		[HttpPost]
		public ActionResult ContratoDetallesInsumos(CContratos contrato)
		{
			CotizacionesController cotizacion = new CotizacionesController();
			IEnumerable<IConceptosInsumos> Insumos = null;
			try
			{
				RestSharp.IRestResponse response = BuildPeticionVerb(GetUrlApiService(), contrato, RestSharp.Method.POST);
				List<CContratosDetallesInsumos> res = new List<CContratosDetallesInsumos>();
				if(response != null)
				{
					res = new List<CContratosDetallesInsumos>();
					res = CBase._ToJson<List<CContratosDetallesInsumos>>(response.Content);
					if(res != null && res.Count() > 0)
					{
						Insumos = from t in res
									 select new CConceptosInsumos()
									 {
										 Cantidad = t.Cantidad,
										 Codigo = t.Codigo,
										 CostoInsumo = t.Costo,
										 Descripcion = t.Descripcion,
										 IdCapitulo = t.IdCapitulo,
										 IdConcepto = t.IdConcepto,
										 IdInsumo = t.IdInsumo,
										 Moneda = t.Moneda,
										 IdMoneda = t.IdMoneda
									 };
					}
				}				
			}
			catch(Exception e)
			{
				OnError(e);
			}			
			return EvaluaErrorInterno(PartialView("../Shared/ExplosionInsumos/_ExplosionInsumos", Insumos));
		}
	}//class ContratosDetallesController  ends.
}//Namespace Controladores.MVC ends.

