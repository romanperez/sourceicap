using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ContratosDetallesInsumosController   : MvcBase
	{
		public ContratosDetallesInsumosController ()
		{			
			_CatalogoNombre = "ContratosDetallesInsumos";
			_InterfaceName = "IContratosDetallesInsumos";
			_ViewEdicion = "IContratosDetallesInsumosEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index()
		{
			return base._UIIndex<CContratosDetallesInsumos>();
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CContratosDetallesInsumos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CContratosDetallesInsumos entity)
		{           
			  return base._UINew<CContratosDetallesInsumos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CContratosDetallesInsumos>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CContratosDetallesInsumos entity)
		{            
				return base._UIEditar<CContratosDetallesInsumos>(entity.IdInsumoContrato, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CContratosDetallesInsumos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CContratosDetallesInsumos>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CContratosDetallesInsumos>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CContratosDetallesInsumos>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CContratosDetallesInsumos>(id, where);
	}
   }//class ContratosDetallesInsumosController  ends.
}//Namespace Controladores.MVC ends.

