using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class FamiliasController   : MvcBase
	{
		public FamiliasController ()
		{			
			_CatalogoNombre = "Familias";
			_InterfaceName = "IFamilias";
			_ViewEdicion = "IFamiliasEditarNew";			
		}
		#region PROTECTED
		protected IFamilias SetIdEmpresaUser(IFamilias entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();				
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CFamilias>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CFamilias>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CFamilias entity = null;
			try
			{
				entity = new CFamilias();
				entity.Empresa = base.GetEmpresa();				
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}		
		[HttpPost]
        public ActionResult New(CFamilias entity)
		{
			SetIdEmpresaUser(entity);  
			return base._UINew<CFamilias>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CFamilias>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CFamilias entity)
		{
			SetIdEmpresaUser(entity);	
			return base._UIEditar<CFamilias>(entity.IdFamilia, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CFamilias>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CFamilias>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CFamilias>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CFamilias>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CFamilias>(id, where);
	}
   }//class FamiliasController  ends.
}//Namespace Controladores.MVC ends.

