using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
    [IcapAuthorize]
    public class MovimientosDetallesController : MvcBase
    {
        public MovimientosDetallesController()
        {
            _CatalogoNombre = "MovimientosDetalles";
            _InterfaceName = "IMovimientosDetalles";
            _ViewEdicion = "IMovimientosDetallesEditarNew";
        }
        [HttpGet]
        [ActionName("Index")]
        public ActionResult Index(int id)
        {
            return base._UIIndex<CMovimientosDetalles>(id);
        }
        [HttpGet]
        public ActionResult IndexPaginado(int id)
        {
            return base._UIIndexPaginado<CMovimientosDetalles>(id);
        }
        [HttpGet]
        public ActionResult New()
        {
            return base._UINew(_ViewEdicion);
        }
        [HttpPost]
        public ActionResult New(CMovimientosDetalles entity)
        {
            return base._UINew<CMovimientosDetalles>(entity);
        }
        [HttpGet]
        public ActionResult Editar(int id)
        {
            return base._UIEditar<CMovimientosDetalles>(_ViewEdicion, id);
        }
        [HttpPost]
        [ActionName("Put")]
        public ActionResult Editar(CMovimientosDetalles entity)
        {
            return base._UIEditar<CMovimientosDetalles>(entity.IdMovimientoDetalle, entity);
        }
        [HttpGet]
        public ActionResult Eliminar(int id)
        {
            return base._UIEliminar<CMovimientosDetalles>(id);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult EliminaById(int id)
        {
            return base._UIEliminaById(id);
        }
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
            return base._UISearchAll<CMovimientosDetalles>(where);
        }
        [HttpGet]
        [ActionName("GetAllMovsById")]
        public ActionResult GetAllMovsById(int id)
        {
            ActionResult resp = null;
            try
            {
               /* List<CPropsWhere> existencias = new List<CPropsWhere>();
                existencias.Add(new CPropsWhere()
                {
                    Prioridad = true,
                    Condicion = "=",
                    NombreCampo = NMovimientosDetalles.eFields.IdMovimiento.ToString(),
                    Valor = id
                });
					 MovimientosController movController = new MovimientosController();
					 IMovimientos mov = movController.GetMovimientoById(id);					
                IEnumerable<IMovimientosDetalles> allMovs = this._Search<CMovimientosDetalles>(base.GetUrlApiService("SearchAll", "MovimientosDetalles"), existencias);
					 mov.DetalleMovimientos = allMovs.Select(x=>x as CMovimientosDetalles).ToList();
                resp = PartialView("_MovimientosById", mov);
					*/

					string url = String.Format("{0}/{1}", GetUrlApiService("GetMovimiento", "Movimientos"), id);
				    IMovimientos mov = this._GetById<CMovimientos>(id, url) as IMovimientos;
                resp = PartialView("_MovimientosById", mov);
            }
            catch (Exception eLoad)
            {
                OnError(eLoad);
            }
            return EvaluaErrorInterno(resp);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
        {
            return base._UISearch<CMovimientosDetalles>(id, where);
        }
        [HttpPost]
        [ActionName("Top")]
        public ActionResult Top(IEnumerable<CPropsWhere> where)
        {
            return base._Top<CMovimientosDetalles>(where);
        }
        [HttpPost]
        [ActionName("TopCustom")]
        public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
        {
            return base._Top<CMovimientosDetalles>(id, where);
        }
    }//class MovimientosDetallesController  ends.
}//Namespace Controladores.MVC ends.

