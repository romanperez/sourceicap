using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class CotizacionesDetallesInsumosController : MvcBase
	{
		public CotizacionesDetallesInsumosController()
		{
			_CatalogoNombre = "CotizacionesDetallesInsumos";
			_InterfaceName = "ICotizacionesDetallesInsumos";
			_ViewEdicion = "ICotizacionesDetallesInsumosEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index()
		{
			return base._UIIndex<CCotizacionesDetallesInsumos>();
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CCotizacionesDetallesInsumos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CCotizacionesDetallesInsumos entity)
		{
			return base._UINew<CCotizacionesDetallesInsumos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CCotizacionesDetallesInsumos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CCotizacionesDetallesInsumos entity)
		{
			return base._UIEditar<CCotizacionesDetallesInsumos>(entity.IdInsumoCotizacion, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CCotizacionesDetallesInsumos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CCotizacionesDetallesInsumos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CCotizacionesDetallesInsumos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CCotizacionesDetallesInsumos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CCotizacionesDetallesInsumos>(id, where);
		}
		public IEnumerable<ICotizacionesDetallesInsumos> GetInsumosByConcepto(IEnumerable<IPropsWhere> where)
		{
			IEnumerable<CCotizacionesDetallesInsumos> allInsumos = this._Search<CCotizacionesDetallesInsumos>(base.GetUrlApiService("SearchAll", "CotizacionesDetallesInsumos"), where);
			return (allInsumos == null) ? new List<CCotizacionesDetallesInsumos>() : allInsumos;
		}
	}//class CotizacionesDetallesInsumosController  ends.
}//Namespace Controladores.MVC ends.

