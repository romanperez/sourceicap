using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ClasificacionConceptosController : MvcBase
	{
		public ClasificacionConceptosController()
		{
			_CatalogoNombre = "ClasificacionConceptos";
			_InterfaceName = "IClasificacionConceptos";
			_ViewEdicion = "IClasificacionConceptosEditarNew";
		}
		#region PROTECTED
		protected IClasificacionConceptos SetIdEmpresaUser(IClasificacionConceptos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CClasificacionConceptos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CClasificacionConceptos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CClasificacionConceptos entity = null;
			try
			{
				entity = new CClasificacionConceptos();
				entity.Empresa = base.GetEmpresa();
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CClasificacionConceptos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CClasificacionConceptos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CClasificacionConceptos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CClasificacionConceptos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CClasificacionConceptos>(entity.IdConceptoClasificacion, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CClasificacionConceptos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CClasificacionConceptos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CClasificacionConceptos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CClasificacionConceptos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CClasificacionConceptos>(id, where);
		}

        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IClasificacionConceptos> ClasificacionConceptoXls = null;
                NExcel<CUnidades> xls = new NExcel<CUnidades>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    ClasificacionConceptoXls = NClasificacionConceptos.GetClasificacionConceptoFromTable(dt);
                }
                ClasificacionConceptoXls = this.BuildPeticionVerb<List<CClasificacionConceptos>>(this.GetUrlApiService("NewXls", "ClasificacionConceptos"), ClasificacionConceptoXls, RestSharp.Method.POST);
                Resp = PartialView("_ClasificacionConceptosWithError", ClasificacionConceptoXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class ClasificacionConceptosController  ends.
}//Namespace Controladores.MVC ends.