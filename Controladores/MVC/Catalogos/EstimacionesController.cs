using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class EstimacionesController : MvcBase
	{
		public EstimacionesController()
		{
			_CatalogoNombre = "Estimaciones";
			_InterfaceName = "IEstimaciones";
			_ViewEdicion = "IEstimacionesEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index()
		{
			return base._UIIndex<CEstimaciones>();
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CEstimaciones>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CEstimaciones entity)
		{
			return base._UINew<CEstimaciones>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CEstimaciones>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CEstimaciones entity)
		{
			return base._UIEditar<CEstimaciones>(entity.IdEstimacion, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CEstimaciones>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CEstimaciones>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CEstimaciones>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEstimaciones>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEstimaciones>(id, where);
		}
        #region REPORTES       
        protected CDescargaReporte _ReporteEstimacionCantidades(CEstimaciones Modelo,string NameFile, ETipoReporte Tipo)
        {
            CDescargaReporte reporte = null;
            ContratosController controllerContrato = new ContratosController();
            IContratos2 contrato = controllerContrato.GetById(Modelo.IdContrato);
            EstimacionesReportes pr = new EstimacionesReportes();
            pr.Controlador = this;
            reporte = pr.ReporteEstimacionesCantidades(String.Format(NameFile, Modelo.IdContrato, DateTime.Now.ToString("ddMMyyyy")), 
                                                       Tipo, 
                                                       contrato, 
                                                       Modelo);
            return reporte;
        }
        protected CDescargaReporte _ReporteResumenEconomico(CEstimacionesResumenEconomico Modelo, string NameFile, ETipoReporte Tipo)
        {
            CDescargaReporte reporte = null;
            ContratosController controllerContrato = new ContratosController();
            IContratos2 contrato = controllerContrato.GetById(Modelo.IdContrato);
            EstimacionesReportes pr = new EstimacionesReportes();
            pr.Controlador = this;
            reporte = pr.ReporteEstimacionesResumenEconomico(String.Format(NameFile, Modelo.IdContrato, DateTime.Now.ToString("ddMMyyyy")),
                                                       Tipo,
                                                       contrato,
                                                       Modelo);
            return reporte;
        }
        [HttpPost]
        public ActionResult ReporteEstimacionResumenEconomico(CEstimacionesResumenEconomico Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {
                if (Modelo.PorcentajeAmortizacionAnticipo <= 0)
                {
                    Modelo.PorcentajeAmortizacionAnticipo = null;
                }
                if (Modelo.ImportePagar == 1)//pdf
                {
                    reporte = _ReporteResumenEconomico(Modelo, "ReporteEstimacionesResumenEconomico{0}_{1}.pdf", ETipoReporte.Pdf);
                }
                else
                {
                    reporte = _ReporteResumenEconomico(Modelo, "ReporteEstimacionesResumenEconomico{0}_{1}.xls", ETipoReporte.Excel);
                }
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        [HttpPost]
        public ActionResult ReporteEstimacionCantidadesPdf(CEstimaciones Modelo)
        {
            CDescargaReporte reporte = null;
            try
            {                                
               reporte=  _ReporteEstimacionCantidades(Modelo, "ReporteEstimacionesCantidades{0}_{1}.pdf", ETipoReporte.Pdf);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        [HttpPost]
        public ActionResult ReporteEstimacionCantidadesXls(CEstimaciones Modelo)
        {            
            CDescargaReporte reporte = null;
            try
            {
                reporte = _ReporteEstimacionCantidades(Modelo, "ReporteEstimacionesCantidades{0}_{1}.xls", ETipoReporte.Excel);             
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return EvaluaErrorInternoJSON(reporte);
        }
        #endregion
    }//class EstimacionesController  ends.
}//Namespace Controladores.MVC ends.