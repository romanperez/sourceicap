using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class PerfilesController   : MvcBase
	{
		public PerfilesController ()
		{			
			_CatalogoNombre = "Perfiles";
			_InterfaceName = "IPerfiles";
			_ViewEdicion = "IPerfilesEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CPerfiles>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CPerfiles>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CPerfiles entity)
		{           
			  return base._UINew<CPerfiles>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CPerfiles>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CPerfiles entity)
		{            
				return base._UIEditar<CPerfiles>(entity.IdPerfil, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CPerfiles>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CPerfiles>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CPerfiles>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CPerfiles>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CPerfiles>(id, where);
	}
   }//class PerfilesController  ends.
}//Namespace Controladores.MVC ends.

