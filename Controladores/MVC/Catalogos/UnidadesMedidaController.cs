using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class UnidadesMedidaController   : MvcBase
	{
		public UnidadesMedidaController ()
		{			
			_CatalogoNombre = "UnidadesMedida";
			_InterfaceName = "IUnidadesMedida";
			_ViewEdicion = "IUnidadesMedidaEditarNew";			
		}
		protected IUnidadesMedida SetIdEmpresaUser(IUnidadesMedida entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CUnidadesMedida>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CUnidadesMedida>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CUnidadesMedida entity = null;
			try
			{
				entity = new CUnidadesMedida();
				entity.Empresa = base.GetEmpresa();
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}		
		[HttpPost]
      public ActionResult New(CUnidadesMedida entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CUnidadesMedida>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CUnidadesMedida>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CUnidadesMedida entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CUnidadesMedida>(entity.IdUnidad, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CUnidadesMedida>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CUnidadesMedida>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CUnidadesMedida>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CUnidadesMedida>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CUnidadesMedida>(id, where);
	}
   }//class UnidadesMedidaController  ends.
}//Namespace Controladores.MVC ends.

