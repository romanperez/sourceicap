using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class InsumosController : MvcBase
	{
		public InsumosController()
		{
			_CatalogoNombre = "Insumos";
			_InterfaceName = "IInsumos";
			_ViewEdicion = "IInsumosEditarNew";
		}
		#region PROTECTED
		protected IInsumos SetIdEmpresaUser(IInsumos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CInsumos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CInsumos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CInsumos entity = null;
			try
			{
				entity = new CInsumos();
				entity.Empresa = base.GetEmpresa();
				entity.EsServicio = false;
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CInsumos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CInsumos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CInsumos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CInsumos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CInsumos>(entity.IdInsumo, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CInsumos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CInsumos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CInsumos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{			
			return base._Top<CInsumos>(where);
		}
		[HttpPost]
		[ActionName("TopInsumosServicios")]
		public ActionResult TopInsumosServicios(IEnumerable<CPropsWhere> where)
		{
			string url = GetUrlApiService("TopInsumosServicios", null);
			IEnumerable<CInsumos>Result = this._Top<CInsumos>(url, where);
			return EvaluaErrorInternoJSON(Result);

		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CInsumos>(id, where);
		}
		#region CargaMasiva
		[HttpGet]
		public override ActionResult PlantillaLoad()
		{
			return base.PlantillaLoad();
		}
		[HttpPost]
		public ActionResult NewXls()
		{
			ActionResult Resp = null;
			try
			{
				IEnumerable<IInsumos> InsumosXls=null;
				NExcel<CInsumos> xls = new NExcel<CInsumos>();				
			   IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
				if(filesLoaded != null && filesLoaded.Count() > 0)
				{
					System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
					InsumosXls = NInsumos.GetInsumosFromTable(dt);					
				}
				InsumosXls = this.BuildPeticionVerb<List<CInsumos>>(this.GetUrlApiService("NewXls", "Insumos"), InsumosXls, RestSharp.Method.POST);
				Resp = PartialView("_InsumosWithError",InsumosXls);				
			}			
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(Resp);
		}
		#endregion
	}//class InsumosController  ends.
}//Namespace Controladores.MVC ends.

