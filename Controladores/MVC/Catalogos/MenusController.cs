using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class MenusController : MvcBase
	{
		public MenusController()
		{
			_CatalogoNombre = "Menus";
			_InterfaceName = "IMenus";
			_ViewEdicion = "IMenusEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CMenus>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CMenus>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CMenus entity)
		{
			return base._UINew<CMenus>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CMenus>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CMenus entity)
		{
			return base._UIEditar<CMenus>(entity.IdMenu, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CMenus>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CMenus>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CMenus>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CMenus>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CMenus>(id, where);
		}
		public IMenus  GetMenuByName(string Menu)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NMenus.eFields.Menu.ToString(),
				Valor = Menu
			});
			IEnumerable<IMenus> allMenu = this._Search<CMenus>(base.GetUrlApiService("SearchAll", "Menus"), whereE);
			if(allMenu != null && allMenu.Count() > 0)
			{
				return allMenu.FirstOrDefault();
			}
			else
			{
				return null;
			}
		}
	}//class MenusController  ends.
}//Namespace Controladores.MVC ends.

