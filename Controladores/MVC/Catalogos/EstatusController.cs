using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class EstatusController : MvcBase
	{
		public EstatusController()
		{
			_CatalogoNombre = "Estatus";
			_InterfaceName = "IEstatus";
			_ViewEdicion = "IEstatusEditarNew";
		}
		#region PROTECTED
		protected IEstatus SetIdEmpresaUser(IEstatus entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = GetIdEmpresa();
				entity.Empresa = GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CEstatus>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CEstatus>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			IEstatus entity = null;
			try
			{
				entity = new CEstatus();
				SetIdEmpresaUser(entity);
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		[HttpPost]
		public ActionResult New(CEstatus entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CEstatus>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CEstatus>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CEstatus entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CEstatus>(entity.IdEstatus, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CEstatus>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CEstatus>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CEstatus>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEstatus>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEstatus>(id, where);
		}
		[HttpGet]
		[ActionName("OTTipo")]
		public ActionResult OTTipo()
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IEstatus> estatus = GetOTTipo();
				resp = PartialView("_OpEstatus", estatus);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		[ActionName("OTEstado")]
		public ActionResult OTEstado()
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IEstatus> estatus = GetOTEstado();
				resp = PartialView("_OpEstatus",estatus);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		[ActionName("ProyectoEstado")]
		public ActionResult ProyectoEstado()
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IEstatus> estatus = GetProyectoEstado();
				resp = PartialView("_OpEstatus", estatus);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		[ActionName("OTPrioridad")]
		public ActionResult OTPrioridad()
		{
			ActionResult resp = null;
			try
			{
				IEnumerable<IEstatus> estatus = GetOTPrioridad();
				resp = PartialView("_OpEstatus", estatus);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		public IEnumerable<IEstatus> GetProyectoEstado()
		{
			return base.BuildPeticionVerb<List<CEstatus>>(GetUrlApiService("ProyectoEstado", "Estatus"), Method.POST);
		}
		public IEnumerable<IEstatus> GetOTTipo()
		{
			return base.BuildPeticionVerb<List<CEstatus>>(GetUrlApiService("OTTipo", "Estatus"), Method.POST);
		}
		public IEnumerable<IEstatus> GetOTEstado()
		{
			return base.BuildPeticionVerb<List<CEstatus>>(GetUrlApiService("OTEstado", "Estatus"), Method.POST);
		}
		public IEnumerable<IEstatus> GetOTPrioridad()
		{
			return base.BuildPeticionVerb<List<CEstatus>>(GetUrlApiService("OTPrioridad", "Estatus"), Method.POST);
		}
	}//class EstatusController  ends.
}//Namespace Controladores.MVC ends.

