using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class IvaValoresController : MvcBase
	{
		public IvaValoresController()
		{
			_CatalogoNombre = "IvaValores";
			_InterfaceName = "IIvaValores";
			_ViewEdicion = "IIvaValoresEditarNew";
		}
		#region PROTECTED
		protected IIvaValores SetIdEmpresaUser(IIvaValores entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CIvaValores>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CIvaValores>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CIvaValores entity = null;
			try
			{
				entity = new CIvaValores();
				entity.FechaInicio = DateTime.Now;
				entity.FechaFin = DateTime.Now;
				entity.Empresa = base.GetEmpresa();
				entity.Estatus = true;
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CIvaValores entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CIvaValores>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CIvaValores>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CIvaValores entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CIvaValores>(entity.IdIva, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CIvaValores>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CIvaValores>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CIvaValores>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CIvaValores>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CIvaValores>(id, where);
		}
		[HttpPost]
		public ActionResult IvaByFecha(CIvaValores Entity)
		{			
			try
			{
				Entity.IdEmpresa = GetIdEmpresa();
				Entity = GetIvaByFecha(Entity)as CIvaValores;
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInternoJSON(Entity);
		}
		/// <summary>
		/// Obtiene el porcentaje de iva correspondiente a una fecha determinada.
		/// </summary>
		/// <param name="IdEmpresa">Identificador de la empresa.</param>
		/// <returns>Objeto IvaValor</returns>
		public IIvaValores GetIvaByFecha(IIvaValores Entity)
		{			
			try
			{
				Entity = base.BuildPeticionVerb<CIvaValores>(base.GetUrlApiService("IvaByFecha", "IvaValores"), Entity, Method.POST);				
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return Entity;
		}
	}//class IvaValoresController  ends.
}//Namespace Controladores.MVC ends.

