using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class CapitulosController : MvcBase
	{
		public CapitulosController()
		{
			_CatalogoNombre = "Capitulos";
			_InterfaceName = "ICapitulos";
			_ViewEdicion = "ICapitulosEditarNew";
		}
		#region PROTECTED
		protected ICapitulos SetIdEmpresaUser(ICapitulos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CCapitulos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CCapitulos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CCapitulos entity = null;
			try
			{
				entity = new CCapitulos();
				entity.Empresa = base.GetEmpresa();
				entity.Estatus = true;
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CCapitulos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CCapitulos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CCapitulos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CCapitulos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CCapitulos>(entity.IdCapitulo, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CCapitulos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CCapitulos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CCapitulos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CCapitulos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CCapitulos>(id, where);
		}
	}//class CapitulosController  ends.
}//Namespace Controladores.MVC ends.

