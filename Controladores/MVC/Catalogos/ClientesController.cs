using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ClientesController : MvcBase
	{
		public ClientesController()
		{
			_CatalogoNombre = "Clientes";
			_InterfaceName = "IClientes";
			_ViewEdicion = "IClientesEditarNew";
		}
#region PROTECTED		
		/*protected override IEnumerable<IPropsWhere> _FiltroIndex()
		{			
			NClientes cliente = new NClientes();
			return cliente.FiltroIndex(_Usuario, base.GetIdUnidad().ToString());
		}
		protected virtual IEnumerable<IPropsWhere> _CombineWithFiltro(IEnumerable<IPropsWhere> where)
		{
			NClientes cliente = new NClientes();
			return cliente.CombineWithFiltro(_FiltroIndex(),where);
		}*/
		protected IClientes SetIdUnidadUser(IClientes cliente)
		{
			if(cliente != null)
			{
				cliente.IdUnidad = GetIdUnidad();
				cliente.Unidad = GetUnidad();
				cliente.Empresa = GetEmpresa();
			}
			
			return cliente;
		}
#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CClientes>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CClientes>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CClientes cliente = null;
			try
			{
				cliente = new CClientes();
				cliente.Empresa = base.GetEmpresa();		
				cliente.Unidad = base.GetUnidad();				
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return _UINew(_ViewEdicion, cliente);
		}
		[HttpPost]
		public ActionResult New(CClientes entity)
		{			
			entity = SetIdUnidadUser(entity) as CClientes;			
			return base._UINew<CClientes>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CClientes>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CClientes entity)
		{
			entity = SetIdUnidadUser(entity) as CClientes;
			return base._UIEditar<CClientes>(entity.IdCliente, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CClientes>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CClientes>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CClientes>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]		
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CClientes>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]		
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CClientes>(id, where);
		}


        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IClientes> ClientesXls = null;
                NExcel<CClientes> xls = new NExcel<CClientes>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    ClientesXls = NClientes.GetClientesFromTable(dt);
                }
                ClientesXls = this.BuildPeticionVerb<List<CClientes>>(this.GetUrlApiService("NewXls", "Clientes"), ClientesXls, RestSharp.Method.POST);
                Resp = PartialView("_ClientesWithError", ClientesXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class ClientesController  ends.
}//Namespace Controladores.MVC ends.

