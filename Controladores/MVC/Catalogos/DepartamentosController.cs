using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class DepartamentosController : MvcBase
	{
		public DepartamentosController()
		{
			_CatalogoNombre = "Departamentos";
			_InterfaceName = "IDepartamentos";
			_ViewEdicion = "IDepartamentosEditarNew";
		}
		#region PROTECTED		
		protected IDepartamentos SetIdEmpresaUser(IDepartamentos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
				entity.IdUnidad = base.GetIdUnidad();
				entity.Unidad = base.GetUnidad();
			}
			return entity;
		}		
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CDepartamentos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CDepartamentos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CDepartamentos entity =null;
			try
			{
				entity = new CDepartamentos();
				entity.Empresa = base.GetEmpresa();
				entity.Unidad = base.GetUnidad();
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CDepartamentos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CDepartamentos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CDepartamentos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CDepartamentos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CDepartamentos>(entity.IdDepartamento, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CDepartamentos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CDepartamentos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CDepartamentos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CDepartamentos>(where);
		}
		[HttpPost]
		[ActionName("TopByEmpresa")]
		public ActionResult TopByEmpresa(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CDepartamentos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CDepartamentos>(id, where);
		}


        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IDepartamentos> DepartamentosXls = null;
                NExcel<CUnidades> xls = new NExcel<CUnidades>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    DepartamentosXls = NDepartamentos.GetDepartamentosFromTable(dt);
                }
                DepartamentosXls = this.BuildPeticionVerb<List<CDepartamentos>>(this.GetUrlApiService("NewXls", "Departamentos"), DepartamentosXls, RestSharp.Method.POST);
                Resp = PartialView("_UnidadesWithError", DepartamentosXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class DepartamentosController  ends.
}//Namespace Controladores.MVC ends.