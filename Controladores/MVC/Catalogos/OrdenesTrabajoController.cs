using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
using System.IO;
using Reportes;
using Microsoft.Reporting.WebForms;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class OrdenesTrabajoController : MvcBase
	{
		protected const string PATH_OT = "OT";
		public OrdenesTrabajoController()
		{
			_CatalogoNombre = "OrdenesTrabajo";
			_InterfaceName = "IOrdenesTrabajo";
			_ViewEdicion = "IOrdenesTrabajoEditarNew";
		}
		#region PROTECTED
		protected IOrdenesTrabajo SetIdEmpresaUser(IOrdenesTrabajo entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
				entity.Unidad = base.GetUnidad();
				entity.Estatus = true;
			}
			return entity;
		}
		#endregion
		[HttpPost]
		public ActionResult OtPlantillaTecnico(CTecnicos tecnico)
		{
			IOTPersonal otPersnal = new COTPersonal();
			otPersnal.IdOTPersonal = 0;
			otPersnal.IdOrdenTrabajo = 0;
			otPersnal.IdTecnico = tecnico.IdTecnico;
			otPersnal.NombreEmpleado = tecnico.NombreEmpleado;
			otPersnal.NombreGrado = tecnico.NombreGrado;
			otPersnal.NombreEspecialidad = tecnico.NombreEspecialidad;
			return PartialView("_OTTecnicoTR", otPersnal);
		}
		[HttpPost]
		public ActionResult OtPlantillaActividad(CActividades activity)
		{
			return PartialView("_ActividadOT", activity);
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<COrdenesTrabajo>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<COrdenesTrabajo>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			COrdenesTrabajo entity = null;
			try
			{
				entity = new COrdenesTrabajo();
				SetIdEmpresaUser(entity);
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		[HttpPost]
		public ActionResult New(COrdenesTrabajo entity)
		{
			ActionResult resp = null;
			try
			{
				SetIdEmpresaUser(entity);
				IEnumerable<DFile> aux = this.ToFiles(OrdenesTrabajoController.PATH_OT,base.FilesToByteArray(null));
				if(aux != null && aux.Count() > 0)
				{
					entity.UpLoadFiles = aux.ToList();
					entity.UpLoadFiles = this.PostFile(entity.UpLoadFiles).ToList();
				}				
				entity = this._New<COrdenesTrabajo>(entity, GetUrlApiService());				
				if(!HasError())
				{
					resp = Editar(entity.IdOrdenTrabajo);
				}				
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<COrdenesTrabajo>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(COrdenesTrabajo entity)
		{
			ActionResult resp = null;
			try
			{
				string url = String.Empty;
				SetIdEmpresaUser(entity);
				IEnumerable<DFile> aux = this.ToFiles(OrdenesTrabajoController.PATH_OT, base.FilesToByteArray(null));
				if(aux != null && aux.Count() > 0)
				{
					entity.UpLoadFiles = aux.ToList();
					entity.UpLoadFiles = this.PostFile(entity.UpLoadFiles).ToList();
				}
				//resp = base._UIEditar<COrdenesTrabajo>(entity.IdOrdenTrabajo, entity);
				url = String.Format("{0}/{1}", GetUrlApiService(), entity.IdOrdenTrabajo);
				entity = this._Put<COrdenesTrabajo>(entity.IdOrdenTrabajo, url, entity);
				if(!HasError())
				{
					resp = Editar(entity.IdOrdenTrabajo);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<COrdenesTrabajo>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<COrdenesTrabajo>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<COrdenesTrabajo>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<COrdenesTrabajo>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<COrdenesTrabajo>(id, where);
		}
		[HttpGet]
		public ActionResult OtPlantillaDiagramaFile(int id)
		{
			ViewData["Consecutivo"] = id;
			return PartialView("_BodyLoadFiles", new COrdenesTrabajo());
		}
		[HttpGet]
		public ActionResult VistaPrevia(int id)
		{
			ActionResult resp = null;
			try
			{				
				DFile file = new DFile();
				OTDocumentosController documento = new OTDocumentosController();
				IOTDocumentos doc = documento.GetById(id);
				file.Url = doc.RutaDocumento;
				file = base.GetFile(file);
				resp = PartialView(file);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult DescargaDocumento(int id)
		{
			ActionResult resp = null;
			try
			{
				DFile file = new DFile();
				OTDocumentosController documento = new OTDocumentosController();
				IOTDocumentos doc = documento.GetById(id);
				file.Url = doc.RutaDocumento;
				file = base.GetFile(file);
				resp = File(file.Contenido, "application/octet-stream", file.Name);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult GetConceptosByContrato(IEnumerable<CContratosDetalles2> AllContratos)
		{
			ActionResult resp = null;
			try
			{

				string url = GetUrlApiService("SearchAll", "ContratosDetalles2");
				NContratosDetalles2 cd = new NContratosDetalles2();
				IEnumerable<IPropsWhere> where = cd.GetConceptosByIdContrato(AllContratos);
				IEnumerable<CContratosDetalles2> result = null;
				if(where != null)
				{
					result = BuildPeticionVerb<List<CContratosDetalles2>>(url, where, Method.POST);
				}
				resp = PartialView(result);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult _EjecutarConceptos(int id /*IdOTConcepto*/)
		{
			ActionResult resp = null;
			try
			{				
				/*string url = GetUrlApiService("Get", "ContratosDetalles2", id.ToString());
				CContratosDetalles2 result = BuildPeticionVerb<CContratosDetalles2>(url, Method.GET);
				IOTConceptos concepto = NContratosDetalles2.ToOTConcepto(result);*/
				string url = GetUrlApiService("GetEjecucionesByConcepto", "OTConceptos",id.ToString());
				COTConceptos result = BuildPeticionVerb<COTConceptos>(url, Method.GET);
				if(result == null)
				{
					result = new COTConceptos();
				}
				if(result.Ejecuciones == null)
				{
					result.Ejecuciones = new List<COTConceptosEjecucion>();
				}
				resp = PartialView(result);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return EvaluaErrorInterno(resp);
		}

		[HttpGet]
		public ActionResult ConceptoPlantilla(int id)
		{
			ActionResult resp = null;
			try
			{
				string url = GetUrlApiService("Get", "ContratosDetalles2",id.ToString());
				CContratosDetalles2 result = BuildPeticionVerb<CContratosDetalles2>(url, Method.GET);
				IOTConceptos concepto = NContratosDetalles2.ToOTConcepto(result);				
				ViewData["colDelete"] = true;
				resp = PartialView("_OTConceptoTR", concepto);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult GetConceptos(int id/*IdOrdenTrabajo*/)
		{
			ActionResult resp = null;
			IOrdenesTrabajo orden = null;
			try
			{
				if(id > 0)
				{
					orden = GetByIdOrden(id);
					if(orden != null)
					{
						ViewData["colDelete"] = true;
						resp = PartialView("_TblConceptos", orden.OTConceptos);
					}
				}
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult ConfiguraActividad(int id)
		{
			CParamRptOTActividad p = null;
			try
			{
				IOrdenesTrabajo ot = GetByIdOrden(id);				
				p = new CParamRptOTActividad();
				p.IdProyecto = ot.IdProyecto;
				p.FechaHoy = DateTime.Now;
				p.FechaInicial = DateTime.Now;
				p.FechaFinal = DateTime.Now;				
				p.Contratos = base.BuildPeticionVerb<List<CvwContratos2>>("Proyectos/GetContratos/"+p.IdProyecto , Method.GET);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView(p));
		}
		[HttpPost]
		public ActionResult ImprimeRptActividad(int id,CParamRptOTActividad actividadParametros)
		{
			//Serializar las fechas en el formato dd/mm/yyyy lo hace correctamente solo si el metodo esta marcado como [HttpPost]
			//IOrdenesTrabajo ot = GetByIdOrden(id);			
			//return PartialView(ot);			
			CDescargaReporte reporte = null;
			try
			{

				//IOrdenesTrabajo ot = GetByIdOrden(id);
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format("{0}.pdf", actividadParametros.FechaInicial.ToString().Replace("/","_"));
				RptOTByActividad rdlc = new RptOTByActividad();
				rdlc.UrlBase = this.GetBaseUrlService();
				rdlc.Peticion = MVC.MvcBase.BuildPeticionService;
				IEnumerable<IOrdenesTrabajo> ordenes = this.ActividadesByContrato(actividadParametros);
				if(!this.HasError())
				{
					TempData[reporte.Id] = rdlc.GeneraReporte(ordenes, new ReportViewer(),actividadParametros.FechaHoy);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}


		/*[HttpGet]
		public PartialViewResult ImprimeRptGlobal(int id)
		{
			IOrdenesTrabajo ot = GetByIdOrden(id);
			return PartialView(ot);
		}*/
		[HttpGet]
		public ActionResult ConfiguraReporte(int id)
		{
			return PartialView();
		}
		[HttpPost]
		public ActionResult ImprimeRptGlobal(int id,CParamRptOTGlobal parametros)
		{
			CDescargaReporte reporte = null;
			try
			{
				reporte = new CDescargaReporte();
				IOrdenesTrabajo ot = GetByIdOrden(id);				
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format("{0}.pdf", ot.Folio);
				RptGlobalOT rdlc = new RptGlobalOT();
				rdlc.UrlBase = this.GetBaseUrlService();
				rdlc.Peticion = MVC.MvcBase.BuildPeticionService;
				rdlc.IdOrdenTrabajo = ot.IdOrdenTrabajo;
				rdlc.Parametros = parametros;
				TempData[reporte.Id] = rdlc.GeneraReporte(new ReportViewer());
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(reporte);
		}


		[HttpGet]
		public ActionResult SelectContrato(int id)
		{
			ActionResult resp = null;
			try
			{
				ProyectosController ctr = new ProyectosController();
				IProyectos proyecto = ctr._GetById(id);
				resp = PartialView(proyecto);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult NewByContrato(COrdenesTrabajo ot)
		{			
			try
			{
				ProyectosController proyecto = new ProyectosController();
				ContratosController contrato = new ContratosController();
				if(ot!=null)
				{
					IProyectos p = proyecto._GetById(ot.IdProyecto);
					IContratos2 c = (ot.Contratos != null && ot.Contratos.Count>0)?contrato.GetById(((IOTContratos)ot.Contratos.First()).IdContrato):null;
					ot.IdOrdenTrabajo = 0;
					SetIdEmpresaUser(ot);
					ot.Proyecto = p.Proyecto;
					ot.IdCliente = p.IdCliente;
					ot.Ubicacion = p.Ubicacion;
					ot.RazonSocial = p.RazonSocial;
					ot.Contratos = new List<COTContratos>();
					ot.FechaInicio = p.FechaInicial;
					ot.FechaFin = p.FechaFinal;
					ot.Ubicacion = p.Ubicacion;
					ot.Contratos.Add(new COTContratos()
					{
						Contrato=c.Contrato,
						IdContrato=c.IdContrato,
						IdOrdenTrabajo=ot.IdOrdenTrabajo,
						IdContratoOrden=0
					});
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, ot));
		}
		public IOrdenesTrabajo GetByIdOrden(int IdOrdenTrabajo)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Get", "OrdenesTrabajo"), IdOrdenTrabajo);
			return this._GetById<COrdenesTrabajo>(IdOrdenTrabajo, url);
		}
		public IEnumerable<IOrdenesTrabajo> ActividadesByContrato(CParamRptOTActividad Entity)
		{
			string url =  GetUrlApiService("ActividadesByContrato", "OrdenesTrabajo");
			return BuildPeticionVerb<List<COrdenesTrabajo>>(url, Entity, Method.POST);
		}
	}//class OrdenesTrabajoController  ends.
}//Namespace Controladores.MVC ends.

