using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ConceptosController : MvcBase
	{
		public ConceptosController()
		{
			_CatalogoNombre = "Conceptos";
			_InterfaceName = "IConceptos";
			_ViewEdicion = "IConceptosEditarNew";
		}
		[HttpPost]
		public ActionResult PlantillaTablaAdd(CInsumos insumo)
		{
			IConceptosInsumos ci = new NConceptosInsumos().ToConceptoInsumo(insumo);			
			return PartialView("_PlantillaTablaAdd",ci);
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CConceptos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CConceptos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CConceptos entity = null;
			try
			{
				entity = new CConceptos();
				entity.Empresa = base.GetEmpresa();			
				entity.Estatus = true;
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CConceptos entity)
		{
			entity.IdEmpresa = base.GetIdEmpresa();
			entity.Estatus = true;
			entity.Fecha = DateTime.Now;
			return base._UINew<CConceptos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CConceptos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CConceptos entity)
		{
			entity.IdEmpresa = base.GetIdEmpresa();
			entity.Estatus = true;
			entity.Fecha = DateTime.Now;
			return base._UIEditar<CConceptos>(entity.IdConcepto, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CConceptos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CConceptos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CConceptos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CConceptos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CConceptos>(id, where);
		}
		#region CargaMasiva
		[HttpGet]
		public override ActionResult PlantillaLoad()
		{
			return base.PlantillaLoad();
		}
		[HttpPost]
		public ActionResult NewXls()
		{
			ActionResult Resp = null;
			try
			{
				IEnumerable<IConceptos> ConceptosXls = null;
				NExcel<CConceptos> xls = new NExcel<CConceptos>();
				IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
				if(filesLoaded != null && filesLoaded.Count() > 0)
				{
					System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
					ConceptosXls = NConceptos.GetConceptosFromTable(dt);
				}
				ConceptosXls = this.BuildPeticionVerb<List<CConceptos>>(this.GetUrlApiService("NewXls", "Conceptos"), ConceptosXls, RestSharp.Method.POST);
				Resp = PartialView("_ConceptosWithError", ConceptosXls);				
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(Resp);
		}
		#endregion
	}//class ConceptosController  ends.
}//Namespace Controladores.MVC ends.

