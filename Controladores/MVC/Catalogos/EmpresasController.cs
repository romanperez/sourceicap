using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class EmpresasController : MvcBase
	{
		protected string path = @"Content\Empresas";
		public EmpresasController()
		{
			_CatalogoNombre = "Empresas";
			_InterfaceName = "IEmpresas";
			_ViewEdicion = "IEmpresasEditarNew";
		}
		protected void SetLogoEmpresa(CEmpresas empresa)
		{
			try
			{
				if(empresa == null) return;
				string[] files = base.SaveFiles(null);
				if(files == null || files.Count() <= 0) return;
				string name = files.FirstOrDefault();
				string FileName = System.IO.Path.GetFileName(name);
				string pathLogos = System.IO.Path.Combine(Server.MapPath("~/"), path);
				string newLogo = System.IO.Path.Combine(pathLogos, FileName);
				if(System.IO.File.Exists(newLogo))
				{
					System.IO.File.Delete(newLogo);
				}
				System.IO.File.Move(name, newLogo);
				empresa.Logo = path + "\\" + FileName;
				empresa.Logo = empresa.Logo.Replace("\\", "/");
			}
			catch(Exception err)
			{
				OnError(err);
			}
		}
        [HttpGet]
        [ActionName("Index")]
        public ActionResult Index(int id)
        {
            return base._UIIndex<CEmpresas>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CEmpresas>(id);
		}
		#region CAMBIAEMPRESA
		public IEmpresas GetEmpresaById(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NEmpresas.eFields.IdEmpresa.ToString(),
				Valor = id
			});
			IEnumerable<IEmpresas> allEmpresas = this._Search<CEmpresas>(base.GetUrlApiService("SearchAll", "Empresas"), whereE);
			return allEmpresas.FirstOrDefault();
		}
		protected IUnidades GetUnidadById(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NUnidades.eFields.IdUnidad.ToString(),
				Valor = id
			});
			IEnumerable<IUnidades> allUnidades = this._Search<CUnidades>(base.GetUrlApiService("SearchAll", "Unidades"), whereE);
			return allUnidades.FirstOrDefault();
		}
		protected IDepartamentos GetDepartamentoById(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NDepartamentos.eFields.IdDepartamento.ToString(),
				Valor = id
			});
			IEnumerable<IDepartamentos> allDepa = this._Search<CDepartamentos>(base.GetUrlApiService("SearchAll", "Departamentos"), whereE);
			return allDepa.FirstOrDefault();
		}
		[HttpPost]
		public ActionResult CambiarEmpresaUser(FormCollection data)
		{
			string empresa = data.Get("IdEmpresa");
			string unidad = data.Get("IdUnidad");
			string departamento = data.Get("IdDepartamento");
			int iEmpresa = 0;
			int iUnidad = 0;
			int iDepartamento = 0;
			int.TryParse(empresa, out iEmpresa);
			int.TryParse(unidad, out iUnidad);
			int.TryParse(departamento, out iDepartamento);

			if(_Usuario != null)
			{
				if(iEmpresa > 0)
				{
					_Usuario.Data.Empresa = GetEmpresaById(iEmpresa) as CEmpresas;
				}
				else
				{
					_Usuario.Data.Empresa = null;
				}
				if(iUnidad > 0)
				{
					_Usuario.Data.Unidad = GetUnidadById(iUnidad) as CUnidades;
				}
				else
				{
					_Usuario.Data.Unidad = null;
				}
				if(iDepartamento > 0)
				{
					_Usuario.Data.Departamento = GetDepartamentoById(iDepartamento) as CDepartamentos;
				}
				else
				{
					_Usuario.Data.Departamento = null;
				}
			}
			return RedirectToAction("Index", "ICAP");
		}
		[HttpGet]
		public ActionResult CambiaEmpresa(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NEmpresas.eFields.Estatus.ToString(),
				Valor = true
			});
			IEnumerable<IEmpresas> allEmpresas = this._Search<CEmpresas>(base.GetUrlApiService("SearchAll", null), whereE);
			return View(allEmpresas);
		}
		[HttpGet]
		public ActionResult GetUnidades(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NUnidades.eFields.Estatus.ToString(),
				Valor = true,
				Operador = DBase.AND
			});
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NUnidades.eFields.IdEmpresa.ToString(),
				Valor = id
			});
			IEnumerable<IUnidades> allUnidades = this._Search<CUnidades>(base.GetUrlApiService("SearchAll", "Unidades"), whereE);
			return PartialView("_Unidades", allUnidades);
		}
		[HttpGet]
		public ActionResult GetDepartamentos(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NDepartamentos.eFields.Estatus.ToString(),
				Valor = true,
				Operador = DBase.AND
			});
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NDepartamentos.eFields.IdUnidad.ToString(),
				Valor = id
			});
			IEnumerable<IDepartamentos> allUnidades = this._Search<CDepartamentos>(base.GetUrlApiService("SearchAll", "Departamentos"), whereE);
			return PartialView("_Departamentos", allUnidades);
		}

		#endregion
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CEmpresas entity)
		{
			this.SetLogoEmpresa(entity);
			entity.Estatus = true;
			return base._UINew<CEmpresas>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CEmpresas>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CEmpresas entity)
		{
			this.SetLogoEmpresa(entity);
			entity.Estatus = true;
			return base._UIEditar<CEmpresas>(entity.IdEmpresa, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CEmpresas>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CEmpresas>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CEmpresas>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEmpresas>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEmpresas>(id, where);
		}
		[HttpGet]
		public ActionResult HelpBuscar()
		{
			return View();
		}
	}//class EmpresasController  ends.
}//Namespace Controladores.MVC ends.

