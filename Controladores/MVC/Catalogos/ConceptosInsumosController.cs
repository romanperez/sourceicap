using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ConceptosInsumosController : MvcBase
	{
		public ConceptosInsumosController()
		{
			_CatalogoNombre = "ConceptosInsumos";
			_InterfaceName = "IConceptosInsumos";
			_ViewEdicion = "IConceptosInsumosEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CConceptosInsumos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CConceptosInsumos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CConceptosInsumos entity)
		{
			return base._UINew<CConceptosInsumos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CConceptosInsumos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CConceptosInsumos entity)
		{
			return base._UIEditar<CConceptosInsumos>(entity.IdConceptoInsumo, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CConceptosInsumos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CConceptosInsumos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CConceptosInsumos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CConceptosInsumos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CConceptosInsumos>(id, where);
		}
		public IEnumerable<IConceptosInsumos> GetInsumosByConcepto(IEnumerable<IPropsWhere> where)
		{
			IEnumerable<CConceptosInsumos> allInsumos = this._Search<CConceptosInsumos>(base.GetUrlApiService("SearchAll", "ConceptosInsumos"), where);
			return (allInsumos == null) ? new List<CConceptosInsumos>() : allInsumos;
		}
	}//class ConceptosInsumosController  ends.
}//Namespace Controladores.MVC ends.

