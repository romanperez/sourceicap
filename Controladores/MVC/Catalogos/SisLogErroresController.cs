using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class SisLogErroresController   : MvcBase
	{		
		public SisLogErroresController ()
		{			
			_CatalogoNombre = "SIS_LOG_ERRORES";
			_InterfaceName = "ISisLogErrores";
			_ViewEdicion = "ISIS_LOG_ERRORESEditarNew";	
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CSisLogErrores>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CSisLogErrores>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CSisLogErrores entity)
		{           
			  return base._UINew<CSisLogErrores>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CSisLogErrores>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CSisLogErrores entity)
		{            
				return base._UIEditar<CSisLogErrores>(entity.Log_Error_Id, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CSisLogErrores>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
			  return base._UISearchAll<CSisLogErrores>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
			  return base._UISearch<CSisLogErrores>(id, where);
        }
	}//class SisLogErroresController  ends.
}//Namespace Controladores.MVC ends.

