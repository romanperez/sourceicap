using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class PedidosDetallesController : MvcBase
	{
		public PedidosDetallesController()
		{
			_CatalogoNombre = "PedidosDetalles";
			_InterfaceName = "IPedidosDetalles";
			_ViewEdicion = "IPedidosDetallesEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CPedidosDetalles>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CPedidosDetalles>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}
		[HttpPost]
		public ActionResult New(CPedidosDetalles entity)
		{
			return base._UINew<CPedidosDetalles>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CPedidosDetalles>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CPedidosDetalles entity)
		{
			return base._UIEditar<CPedidosDetalles>(entity.IdPedidoDetalle, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CPedidosDetalles>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CPedidosDetalles>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CPedidosDetalles>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CPedidosDetalles>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CPedidosDetalles>(id, where);
		}

		[HttpGet]
		public ActionResult DetallesByPedido(int id)
		{
			IEnumerable<CPedidosDetalles> Result = null;
			try
			{
				string url = GetUrlApiService("SearchAll", null);
				List<CPropsWhere> where = new List<CPropsWhere>();
				where.Add(new CPropsWhere()
				{
					Condicion="=",
					Prioridad=true,
					NombreCampo=NPedidosDetalles.eFields.IdPedido.ToString(),
					Valor=id
				});
		      Result = this._Search<CPedidosDetalles>(url, where);
				if(Result != null && Result.Count()>0)
				{
					Result = Result.GroupBy(i => new
					{
						IdPedido = i.IdPedido,
						Codigo = i.Codigo,
						Serie = i.Serie,
						Descripcion = i.Descripcion,
						UnidadMedidaDescripcion = i.UnidadMedidaDescripcion
					}).Select(ii=> new CPedidosDetalles()
					{
						IdPedidoDetalle=0,
						IdPedido=ii.Key.IdPedido,
						Codigo=ii.Key.Codigo,
						Serie=ii.Key.Serie,
						Descripcion=ii.Key.Descripcion,
						UnidadMedidaDescripcion=ii.Key.UnidadMedidaDescripcion,
						Cantidad= ii.Sum(x =>x.Cantidad),
						CantidadEnviada = ii.Sum(x => x.CantidadEnviada),
						CantidadRecibida = ii.Sum(x => x.CantidadRecibida)
					});
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_DetalleByPedido", Result));
		}
		[HttpPost]		
		public ActionResult ConfirmaRecepcion(List<CPedidosDetalles> entity)
		{
			try
			{
				string url = GetUrlApiService("ConfirmaRecepcion", "PedidosDetalles");
				IPedidosDetalles result = this.BuildPeticionVerb<CPedidosDetalles>(url, entity, Method.POST);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_RespuestaOK"));
		}
	}//class PedidosDetallesController  ends.
}//Namespace Controladores.MVC ends.