using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{	
	[IcapAuthorize]
	public class MovimientosController : MvcBase
	{
		protected string _UrlIndexPaginado;
		protected string _NameAccion;
		IEnumerable<IPropsWhere> _where;
		public MovimientosController()
		{
			_CatalogoNombre = "Movimientos";
			_InterfaceName = "IMovimientos";
			_ViewEdicion = "IMovimientosEditarNew";
			_UrlIndexPaginado = "";
			_NameAccion = "";			
		}
		[HttpGet]
		[ActionName("IndexEntradas")]
		public ActionResult IndexEntradas(int id /*IdMenu que hace el llamado Revisar permisos*/)
		{
			_NameAccion = "IndexEntradas";
			_UrlIndexPaginado = "IndexPaginadoEntradas";
			_where = this._FiltrosByUserLoggin(0);
			/*"Entradas" debe coincidir con la propiedad "Catalogo" del campo [Parametros] de la tabla Menus*/
			return this._UIIndex2<CMovimientos>(id, "Entradas", _CatalogoNombre); 
		}
		[HttpGet]
		[ActionName("IndexSalidas")]
		public ActionResult IndexSalidas(int id /*IdMenu que hace el llamado Revisar permisos*/)
		{
			_NameAccion = "IndexSalidas";
			_UrlIndexPaginado = "IndexPaginadoSalidas";
			_where = this._FiltrosByUserLoggin(0);
			/*"Salidas" debe coincidir con la propiedad "Catalogo" del campo [Parametros] de la tabla Menus*/
			return this._UIIndex2<CMovimientos>(id, "Salidas", _CatalogoNombre);
		}
		[HttpGet]
		[ActionName("IndexTraspasos")]
		public ActionResult IndexTraspasos(int id /*IdMenu que hace el llamado Revisar permisos*/)
		{
			_NameAccion = "IndexTraspasos";
			_UrlIndexPaginado = "IndexPaginadoTraspasos";
			_where = this._FiltrosByUserLoggin(0);
			/*"Traspasos" debe coincidir con la propiedad "Catalogo" del campo [Parametros] de la tabla Menus*/
			return this._UIIndex2<CMovimientos>(id, "Traspasos", _CatalogoNombre);
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id /*IdMenu que hace el llamado Revisar permisos*/)
		{
			_NameAccion = "Index";
			return this._UIIndex<CMovimientos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginadoEntradas(int id /*Pagina a visualizar*/)
		{
			_NameAccion = "PaginadoEntradas";
			_where = this._FiltrosByUserLoggin(0);
			/*"Entradas" debe coincidir con la propiedad "Catalogo" del campo [Parametros] de la tabla Menus*/
			return _UIIndexPaginado2(id,"Entradas");
		}
		[HttpGet]
		public ActionResult IndexPaginadoSalidas(int id /*Pagina a visualizar*/) 
		{
			_NameAccion = "PaginadoSalidas";
			_where = this._FiltrosByUserLoggin(0);
			/*"Salidas" debe coincidir con la propiedad "Catalogo" del campo [Parametros] de la tabla Menus*/
			return _UIIndexPaginado2(id,"Salidas");
		}
		[HttpGet]
		public ActionResult IndexPaginadoTraspasos(int id /*Pagina a visualizar*/)
		{
			_NameAccion = "PaginadoTraspasos";
			_where = this._FiltrosByUserLoggin(0);
			/*"Traspasos" debe coincidir con la propiedad "Catalogo" del campo [Parametros] de la tabla Menus*/
			return _UIIndexPaginado2(id, "Traspasos");
		}
		[HttpGet]
		public ActionResult BuscaMovimiento()
		{
			ActionResult Resp = null;
			try
			{
				Resp = PartialView();
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpPost]
		public ActionResult BuscaMovimiento(CFiltrosMovimiento Entity)
		{
			ActionResult Resp = null;
			try
			{
				string url = GetUrlApiService();
				List<CMovimientos> movimientos = BuildPeticionVerb<List<CMovimientos>>(url, Entity, Method.POST);
				Resp = PartialView("_BuscaMovimientos", movimientos);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(Resp);
		}
		[HttpGet]
		public ActionResult NewMovimientoEntrada()
		{
			ActionResult resp = null;
			try
			{
				IMovimientos m = new CMovimientos();
				int iUnidad = _Usuario.Data.Unidad.IdUnidad;
				m.Fecha = DateTime.Now;
				m.IdMovimiento = 0;
				m.IdUnidad = iUnidad;
				resp = base._UINew("IMovimientosEntradaNew", m);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult NewMovimientoSalida()
		{
			ActionResult resp = null;
			try
			{
				int iUnidad = _Usuario.Data.Unidad.IdUnidad;
				Entidades.IMovimientos salidaM = new CMovimientos();
				salidaM.Fecha = DateTime.Now;
				salidaM.IdMovimiento = 0;
				salidaM.IdUnidad = iUnidad;
				resp = base._UINew("IMovimientosSalidaNew", salidaM);		
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult NewTraspasoFromPedido()
		{
			return PartialView();
		}
		[HttpGet]
		public ActionResult NewEntradaFromPedido()
		{
			return PartialView();
		}
		[HttpPost]
		public ActionResult SaveTraspasoFromPedido(CModelTraspasos Entity)
		{			
			try
			{
				string url = GetUrlApiService("RecibeInsumosByProyecto", "Proyectos");
				Entity = BuildPeticionVerb<CModelTraspasos>(url,Entity, Method.POST);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInternoJSON(Entity);
		}		
		[HttpPost]
		public ActionResult SaveEntradaFromPedido(CMovimientos Entity)
		{
			ActionResult resp = null;
			try
			{
				string url = GetUrlApiService("New","Movimientos");
				SetUserLogged(Entity);
				Entity.Tipo = (int)NMovimientos.eTipoMovimientos.Entrada;
				Entity = this.BuildPeticionVerb<CMovimientos>(url, Entity, Method.POST);
				resp = PartialView("_RespuestaOK");
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpPost]
		public ActionResult NewTraspasoFromPedido(CProyectos Entity)
		{
			ActionResult resp = null;
			try
			{
				UnidadesController unidades = new UnidadesController();
				CModelTraspasos traspasoModel = new CModelTraspasos();
				int iEmpresa = _Usuario.Data.Empresa.IdEmpresa;
				string url = String.Format("{0}/{1}", GetUrlApiService("PedidosByProyecto", "Proyectos"), Entity.IdProyecto);
				
				traspasoModel.Movimiento = new CMovimientos();				
				traspasoModel.Movimiento.IdUnidad =  _Usuario.Data.Unidad.IdUnidad;
				traspasoModel.Movimiento.Fecha = DateTime.Now;
				traspasoModel.Pedidos = BuildPeticionVerb<List<CPedidos>>(url, Method.POST);				
				traspasoModel.Almacenes = new AlmacenesController().GetAlmacenByUnidad(traspasoModel.Movimiento.IdUnidad).Select(x=>x as CAlmacenes).ToList();
				if(traspasoModel.Pedidos != null && traspasoModel.Pedidos.Count() > 0)
				{					
					traspasoModel.AlmacenesDestino = new AlmacenesController().GetAlmacenByUnidad(traspasoModel.Pedidos.First().IdUnidad).Select(x => x as CAlmacenes).ToList();
				}
				traspasoModel.Empresa = GetEmpresa();
				traspasoModel.Unidad = GetUnidad();
				resp = base._UINew("IMovimientosTraspasoNewFromProyecto", traspasoModel);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		//
		//
		[HttpPost]
		public ActionResult NewEntradaFromPedido(CProyectos Entity)
		{
			ActionResult resp = null;
			try
			{
				int iEmpresa = _Usuario.Data.Empresa.IdEmpresa;
				CModelEntrada entradaModel = new CModelEntrada();				
				string url = String.Format("{0}/{1}", GetUrlApiService("PedidosByProyecto", "Proyectos"), Entity.IdProyecto);
				entradaModel.Movimiento = new CMovimientos();
				entradaModel.Movimiento.IdUnidad = _Usuario.Data.Unidad.IdUnidad;
				entradaModel.Movimiento.Fecha = DateTime.Now;
				entradaModel.Pedidos = BuildPeticionVerb<List<CPedidos>>(url, Method.POST);
				entradaModel.Almacenes = new AlmacenesController().GetAlmacenByUnidad(entradaModel.Movimiento.IdUnidad).Select(x => x as CAlmacenes).ToList();
				entradaModel.Empresa = GetEmpresa();
				entradaModel.Unidad = GetUnidad();
				entradaModel.IdUnidad = GetIdUnidad();
				entradaModel.Fecha = DateTime.Now;
				resp = base._UINew("IMovimientosEntradaNewFromProyecto", entradaModel);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		//
		//
		[HttpGet]
		public ActionResult NewMovimientoTraspaso(int id /*MenuCaller*/)
		{
			ActionResult resp = null;
			try
			{
				CModelTraspasos traspasoModel = new CModelTraspasos();
				UnidadesController unidades = new UnidadesController();
				int iUnidad = _Usuario.Data.Unidad.IdUnidad;
				int iEmpresa = _Usuario.Data.Empresa.IdEmpresa;
				CMovimientos traspasoM = new CMovimientos();
				traspasoM.Fecha = DateTime.Now;
				traspasoM.IdMovimiento = 0;
				traspasoM.IdUnidad = iUnidad;
				traspasoModel.Movimiento = traspasoM;
				traspasoModel.Movimiento.Fecha = DateTime.Now;
				traspasoModel.PermisoCambioUnidades = _HasPermiso(id,"MovimientoTraspasoCambioUnidad");
				if(!traspasoModel.PermisoCambioUnidades)
				{
					traspasoModel.Almacenes = new AlmacenesController().GetAlmacenByUnidad(traspasoM.IdUnidad).Select(x=>x as CAlmacenes).ToList();
				}
				else
				{
					traspasoModel.Unidades = unidades._GetUnidadesByEmpresa(iEmpresa).Select(x=>x as CUnidades).ToList();
				}
				resp = base._UINew("IMovimientosTraspasoNew", traspasoModel);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(resp);
		}
		[HttpGet]
		public ActionResult GetAlmacenesOption(int id/*idUnidad*/)
		{
			IEnumerable<IAlmacenes> almacenes = null;
			try
			{			
				AlmacenesController alm = new AlmacenesController();
				almacenes = alm.GetAlmacenByUnidad(id);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return EvaluaErrorInterno(PartialView("_Almacenes", almacenes));
		}
		[HttpPost]
		public ActionResult New(CMovimientos entity)
		{
			SetUserLogged(entity);
			return base._UINew<CMovimientos>(entity);
		}
		protected override ActionResult _UIEditar<Entity>(string PartialViewName, int id)
		{
			IMovimientos Entidad = null;
			try
			{
				ViewBag.LabelCreate = Idiomas.IcapWeb.CatalogoCrearEntidad;
				string url = String.Format("{0}/{1}", GetUrlApiService(PartialViewName, null), id);
				Entidad = this._GetById<Entity>(id, url) as IMovimientos;
				if(Entidad != null)
				{
					ViewBag.Action = "Put";
					NMovimientos.eTipoMovimientos movimientoTipo = (NMovimientos.eTipoMovimientos)Entidad.Tipo;
					switch(movimientoTipo)
					{
						case NMovimientos.eTipoMovimientos.Entrada:
							_ViewEdicion = "IMovimientosEntradaNew";
							break;
						case NMovimientos.eTipoMovimientos.Salida:
							_ViewEdicion = "IMovimientosSalidaNew";
							break;
						default:
							_ViewEdicion = "_MovimientoNoReconocido";
							break;
					}
				}
			}
			catch(Exception eEdit)
			{
				OnError(eEdit);
			}
			return EvaluaErrorInterno(PartialView(_ViewEdicion, Entidad));
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return _UIEditar<CMovimientos>("Get", id);
		}
		[HttpGet]
		public ActionResult EditarSalida(int id)
		{
			return _UIEditar<CMovimientos>("GetSalida", id);
		}		
		[HttpPost]
		public ActionResult EditarTraspaso(FormCollection data)
		{			
			CMovimientos Entidad = null;
			ActionResult resp = null;
			try
			{
				int idMenu =0;
				int idMovimiento =0;
				int.TryParse(data.Get("idMenu"),out idMenu);
				int.TryParse(data.Get("idMovimiento"), out idMovimiento);
				if(idMenu > 0 && idMovimiento > 0)
				{
					ViewBag.LabelCreate = Idiomas.IcapWeb.CatalogoCrearEntidad;
					string url = String.Format("{0}/{1}", GetUrlApiService("GetTraspaso", null), idMovimiento);
					Entidad = this._GetById<CMovimientos>(idMovimiento, url);
					if(Entidad != null)
					{						
						CModelTraspasos traspasoModel = new CModelTraspasos();
						UnidadesController unidades = new UnidadesController();
						int iUnidad = _Usuario.Data.Unidad.IdUnidad;
						int iEmpresa = _Usuario.Data.Empresa.IdEmpresa;
						traspasoModel.Movimiento = Entidad;
						traspasoModel.Unidades = unidades._GetUnidadesByEmpresa(iEmpresa).Select(x=> x as CUnidades).ToList();
						traspasoModel.PermisoCambioUnidades = _HasPermiso(idMenu, "MovimientoTraspasoCambioUnidad");
						ViewBag.Action = "Put";
						resp = PartialView("IMovimientosTraspasoNew", traspasoModel);						
					}
				}
				else
				{
					resp = PartialView("_MovimientoNoReconocido");
				}
			}
			catch(Exception eEdit)
			{
				OnError(eEdit);
			}
			return EvaluaErrorInterno(resp);



		}
		[HttpGet]
		public ActionResult PlantillaSelectUnity(int id)
		{
			UnidadesController unidad = new UnidadesController();
			return unidad.GetUnidadesByEmpresa(id);
		}
		[HttpPost]
		public ActionResult PlantillaTablaAdd(CMovimientosDetalles insumo)
		{		
			insumo.IdMovimiento = 0;
			insumo.Precio = 0;
			return PartialView("_AddDetalleMovimiento",insumo);
		}
		[HttpPost]
		public ActionResult PlantillaTablaAddSalida(CInsumosExistencias insumo)
		{
			CMovimientosDetalles mov = new CMovimientosDetalles();
			mov.IdMovimiento = 0;
			mov.IdMovimientoDetalle = 0;
			mov.CantidadExistenciaInsumo = insumo.Cantidad;
			mov.Cantidad = 0;
			mov.Codigo = insumo.Codigo;
			mov.Descripcion = insumo.Descripcion;
			mov.IdInsumo = insumo.IdInsumo;
			return PartialView("_AddDetalleMovimientoSalida", mov);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CMovimientos entity)
		{
			SetUserLogged(entity);
			return base._UIEditar<CMovimientos>(entity.IdMovimiento, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CMovimientos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CMovimientos>(where);
		}
		[HttpPost]
		[ActionName("SearchTraspaso")]
		public ActionResult SearchTraspaso(int id, IEnumerable<CPropsWhere> where)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Search", null), id);
			List<CPropsWhere> where2 = new List<CPropsWhere>();
			where2.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NMovimientos.eFields.Tipo.ToString(),
				Valor = (int)NMovimientos.eTipoMovimientos.Traspaso,
				Operador = DBase.AND,
				Condicion = "="
			});
			where.ToList().ForEach(x =>
			{
				where2.Add(x);
			});
			return base._UISearch<CMovimientos>(url, where2);

		}
		[HttpPost]
		[ActionName("SearchSalidas")]
		public ActionResult SearchSalidas(int id, IEnumerable<CPropsWhere> where)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Search", null), id);
			List<CPropsWhere> where2 = new List<CPropsWhere>();
			where2.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NMovimientos.eFields.Tipo.ToString(),
				Valor = (int)NMovimientos.eTipoMovimientos.Salida,
				Operador = DBase.AND,
				Condicion = "="
			});
			where.ToList().ForEach(x =>
			{
				where2.Add(x);
			});
			return base._UISearch<CMovimientos>(url, where2);
			
		}
		[HttpPost]
		[ActionName("SearchEntradas")]
		public ActionResult SearchEntradas(int id, IEnumerable<CPropsWhere> where)
		{
			string url = String.Format("{0}/{1}", GetUrlApiService("Search", null), id);
			List<CPropsWhere> where2 = new List<CPropsWhere>();
			where2.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NMovimientos.eFields.Tipo.ToString(),
				Valor = (int)NMovimientos.eTipoMovimientos.Entrada,
				Operador = DBase.AND,
				Condicion = "="
			});
			where.ToList().ForEach(x =>
			{				
				where2.Add(x);
			});
			return base._UISearch<CMovimientos>(url, where2);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CMovimientos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CMovimientos>(id, where);
		}
		#region PROTECTED
		protected void SetUserLogged(IMovimientos entity)
		{
			try
			{
				entity.IdUsuario = _Usuario.IdUsuario;
				entity.Estatus = true;
			}
			catch(Exception e)
			{
				OnError(e);
			}
		}
		protected IEnumerable<IPropsWhere> _FiltrosByUserLoggin(int idUnidadParam)
		{
			int iEmpresa = 0;
			int iUnidad = 0;
			try
			{

				if(_Usuario != null)
				{
					if(_Usuario.Data != null)
					{
						if(_Usuario.Data.Empresa != null)
						{
							iEmpresa = _Usuario.Data.Empresa.IdEmpresa;
						}
						if(_Usuario.Data.Unidad != null)
						{
							iUnidad = _Usuario.Data.Unidad.IdUnidad;
						}
						if(idUnidadParam > 0) iUnidad = idUnidadParam;
						return _FiltroByEmpresaUnidad(iEmpresa, iUnidad);
					}
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return null;
		}
		protected IEnumerable<IPropsWhere> _FiltroByEmpresaUnidad(int iEmpresa,int iUnidad)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			try
			{
				where.Add(new CPropsWhere()
				{
					NombreCampo="Empresas.IdEmpresa",
					Valor=iEmpresa
				});
				where.Add(new CPropsWhere()
				{
					NombreCampo = "Unidades.IdUnidad",
					Valor = iUnidad
				});
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return where;
		}
		protected override IEnumerable<IPropsWhere> _FiltroIndex()
		{
			return _where;
		}
		protected  ActionResult _UIIndex2<Entity>(int idMenuCaller,string catalogoName,string controladorName)
		{
			IUICatalogo catalogo = null;
			try
			{
				IPagina<CMovimientos> page = base._GetPage<CMovimientos>(String.Format("{0}/1", this.GetUrlApiService(_NameAccion, null)));
				if(page != null)
				{
					catalogo = _UICatalogo<CMovimientos>(page, "get");
					catalogo.Nombre = catalogoName;
					catalogo.PaginaInfo.Nombre = catalogoName;
					catalogo.ControladorName = controladorName;
					catalogo.PaginaInfo.Url = base.GetUrlApiService(_UrlIndexPaginado, null);
					catalogo.FieldsSearch = base._UIBuildFieldsSearch(new CMovimientos());
					catalogo.Usuario = _Usuario;
					catalogo.MenuCaller = idMenuCaller;
					IEnumerable<IPerfilAcciones> permisos = base.GetPermisos(idMenuCaller, _Usuario.Data.Perfil);
					catalogo.SetPermisos(permisos);					
				}
			}
			catch(Exception eIndex)
			{
				OnError(eIndex);
			}
			return EvaluaErrorInterno(PartialView(catalogo));
		}
		protected virtual ActionResult _UIIndexPaginado2(int pagina,string Nombre)
		{
			IPagina<CMovimientos> page = this._GetPage<CMovimientos>(String.Format("{0}/{1}", this.GetUrlApiService(_NameAccion, null), pagina));
			IUICatalogo catalogo = null;
			if(page != null)
			{
				catalogo = _UICatalogo<CMovimientos>(page, "get");
				catalogo.Nombre = Nombre;
				catalogo.PaginaInfo.Nombre = Nombre;
			}
			return EvaluaErrorInterno(PartialView("Buscar", catalogo));
		}
		/*public IMovimientos GetMovimientoById(int IdMovimiento)
		{
			IMovimientos mov = null;
			try
			{
				string url = String.Format("{0}/{1}", GetUrlApiService("GetMovimiento", "Movimientos"), IdMovimiento);
				mov = BuildPeticionVerb<CMovimientos>(url, Method.GET);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return mov;
		}*/
		#endregion
	}//class MovimientosController  ends.
}//Namespace Controladores.MVC ends.


