using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class AlmacenesController : MvcBase
	{
		public AlmacenesController()
		{
			_CatalogoNombre = "Almacenes";
			_InterfaceName = "IAlmacenes";
			_ViewEdicion = "IAlmacenesEditarNew";
		}
		#region PROTECTED
		protected IAlmacenes SetIdEmpresaUser(IAlmacenes entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
				entity.IdUnidad = base.GetIdUnidad();
				entity.Unidad = base.GetUnidad();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CAlmacenes>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CAlmacenes>(id);
		}
		[HttpGet]
		public ActionResult New()
		{			
			CAlmacenes entity = null;
			try
			{
				entity = new CAlmacenes();
				entity.Empresa = base.GetEmpresa();
				entity.Unidad = base.GetUnidad();
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CAlmacenes entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CAlmacenes>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{			
			return base._UIEditar<CAlmacenes>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CAlmacenes entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CAlmacenes>(entity.IdAlmacen, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CAlmacenes>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CAlmacenes>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CAlmacenes>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CAlmacenes>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CAlmacenes>(id, where);
		}
		public IEnumerable<IAlmacenes> GetAlmacenByUnidad(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NAlmacenes.eFields.IdUnidad.ToString(),
				Valor = id
			});
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NAlmacenes.eFields.Estatus.ToString(),
				Valor = true
			});
			IEnumerable<CAlmacenes> allAlmacenes = this._Search<CAlmacenes>(base.GetUrlApiService("SearchAll", "Almacenes"), whereE);
			return (allAlmacenes == null) ? new List<CAlmacenes>() : allAlmacenes;
		}
	}//class AlmacenesController  ends.
}//Namespace Controladores.MVC ends.

