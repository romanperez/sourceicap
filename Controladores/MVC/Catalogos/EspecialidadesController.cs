using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class EspecialidadesController : MvcBase
	{
		public EspecialidadesController()
		{
			_CatalogoNombre = "Especialidades";
			_InterfaceName = "IEspecialidades";
			_ViewEdicion = "IEspecialidadesEditarNew";
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CEspecialidades>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CEspecialidades>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CEspecialidades especialidad = null;
			try
			{
				especialidad = new CEspecialidades();
				especialidad.Empresa = base.GetEmpresa();
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return base._UINew(_ViewEdicion, especialidad);
		}
		[HttpPost]
		public ActionResult New(List<CEspecialidades> entities)
		{
			if(entities != null && entities.Count() > 0)
			{
				entities.ForEach(x =>
				{
					x.IdEmpresa = base.GetIdEmpresa();
					x.Estatus = true;
				});
			}
			return EvaluaErrorInterno(base._UINew<CEspecialidades>(entities));
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{		
			return base._UIEditar<CEspecialidades>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CEspecialidades entity)
		{
			entity.IdEmpresa = base.GetIdEmpresa();
			return base._UIEditar<CEspecialidades>(entity.IdEspecialidad, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CEspecialidades>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CEspecialidades>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CEspecialidades>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEspecialidades>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEspecialidades>(id, where);
		}
		public IEnumerable<IEspecialidades> GetByGradoId(int IdGrado)
		{
			
			IEnumerable<CEspecialidades> allEspecialidades = this._Search<CEspecialidades>(base.GetUrlApiService("SearchAll", "Especialidades"), 
				                                          new NEspecialidades().FiltroByGradoId(IdGrado));
			return allEspecialidades;
		}

        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IEspecialidades> EspecialidadesXls = null;
                NExcel<CEspecialidades> xls = new NExcel<CEspecialidades>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    EspecialidadesXls = NEspecialidades.GetEspecialidadesFromTable(dt);
                }
                EspecialidadesXls = this.BuildPeticionVerb<List<CEspecialidades>>(this.GetUrlApiService("NewXls", "Especialidades"), EspecialidadesXls, RestSharp.Method.POST);
                Resp = PartialView("_EspecialidadesWithError", EspecialidadesXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class EspecialidadesController  ends.
}//Namespace Controladores.MVC ends.

