using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class MonedasController : MvcBase
	{
		public MonedasController()
		{
			_CatalogoNombre = "Monedas";
			_InterfaceName = "IMonedas";
			_ViewEdicion = "IMonedasEditarNew";
		}
		#region PROTECTED
		protected IMonedas SetIdEmpresaUser(IMonedas entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();				
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CMonedas>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CMonedas>(id);
		}
		[HttpGet]
		public ActionResult New()
		{			
			CMonedas entity = null;
			try
			{
				entity = new CMonedas();
				entity.Empresa = base.GetEmpresa();				
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CMonedas entity)
		{
			SetIdEmpresaUser(entity);
			entity.Estatus = true;
			return base._UINew<CMonedas>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CMonedas>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CMonedas entity)
		{
			SetIdEmpresaUser(entity);
			entity.Estatus = true;
			return base._UIEditar<CMonedas>(entity.IdMoneda, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CMonedas>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CMonedas>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CMonedas>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CMonedas>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CMonedas>(id, where);
		}
		/// <summary>
		/// Obtiene la moneda predeterminada [Pesos Mexicanos] para realizar calculos por empresa.
		/// </summary>
		/// <returns>IMoneda objeto con los datos.</returns>
		public IMonedas GetPesos(int IdEmpresa)
		{
			IMonedas Moneda = null;
			try
			{
				string url = String.Format("{0}/{1}", base.GetUrlApiService("GetPesos", "Monedas"), IdEmpresa);
				RestSharp.IRestResponse response = BuildPeticionVerb(url, RestSharp.Method.GET);
				if(response != null)
				{
					Moneda = Entidades.CBase._ToJson<CMonedas>(response.Content);
				}
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return Moneda;
		}
		public  IEnumerable<IMonedas> GetAllMonedasByEmpresa(int IdEmpresa)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NMonedas.eFields.IdEmpresa.ToString(),
				Valor = IdEmpresa
			});
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NMonedas.eFields.Estatus.ToString(),
				Valor = true
			});
			IEnumerable<CMonedas> allMonedas = this._Search<CMonedas>(base.GetUrlApiService("SearchAll", "Monedas"), whereE);
			return (allMonedas == null) ? new List<CMonedas>() : allMonedas;
		}
	}//class MonedasController  ends.
}//Namespace Controladores.MVC ends.

