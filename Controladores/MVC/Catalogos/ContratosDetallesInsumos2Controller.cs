using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ContratosDetallesInsumos2Controller   : MvcBase
	{
		public ContratosDetallesInsumos2Controller ()
		{			
			_CatalogoNombre = "ContratosDetallesInsumos2";
			_InterfaceName = "IContratosDetallesInsumos2";
			_ViewEdicion = "IContratosDetallesInsumos2EditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index()
		{
			return base._UIIndex<CContratosDetallesInsumos2>();
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CContratosDetallesInsumos2>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CContratosDetallesInsumos2 entity)
		{           
			  return base._UINew<CContratosDetallesInsumos2>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CContratosDetallesInsumos2>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CContratosDetallesInsumos2 entity)
		{            
				return base._UIEditar<CContratosDetallesInsumos2>(entity.IdInsumoContrato, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CContratosDetallesInsumos2>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CContratosDetallesInsumos2>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CContratosDetallesInsumos2>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CContratosDetallesInsumos2>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CContratosDetallesInsumos2>(id, where);
	}
   }//class ContratosDetallesInsumos2Controller  ends.
}//Namespace Controladores.MVC ends.

