using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ClientesPersonalController : MvcBase
	{
		public ClientesPersonalController()
		{
			_CatalogoNombre = "ClientesPersonal";
			_InterfaceName = "IClientesPersonal";
			_ViewEdicion = "IClientesPersonalEditarNew";
		}
		protected IClientesPersonal SetIdUnidadUser(IClientesPersonal cliente)
		{
			if(cliente != null)
			{
				cliente.Unidad = GetUnidad();
				cliente.Empresa = GetEmpresa();
			}
			return cliente;
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CClientesPersonal>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CClientesPersonal>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CClientesPersonal clientePersonal = null;
			try
			{
				clientePersonal = new CClientesPersonal();
				clientePersonal.Empresa = base.GetEmpresa();
				clientePersonal.Unidad = base.GetUnidad();
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return _UINew(_ViewEdicion, clientePersonal);
		}
		[HttpPost]
		public ActionResult New(CClientesPersonal entity)
		{
			return base._UINew<CClientesPersonal>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CClientesPersonal>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CClientesPersonal entity)
		{
			return base._UIEditar<CClientesPersonal>(entity.IdPersonalCliente, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CClientesPersonal>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CClientesPersonal>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CClientesPersonal>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CClientesPersonal>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CClientesPersonal>(id, where);
		}
		[HttpPost]		
		public ActionResult PersonalByCliente(int id)
		{			
			ActionResult resp = null;
			try
			{
				IEnumerable<IClientesPersonal> res = _PersonalByCliente(id);
				resp = PartialView("_ListClientesPersonal", res);
			}
			catch(Exception eBy)
			{
				OnError(eBy);
			}
			return EvaluaErrorInterno(resp);
		}
		public IEnumerable<IClientesPersonal> _PersonalByCliente(int IdCliete)
		{			
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NClientesPersonal.eFields.IdCliente.ToString(),
				Condicion = "=",
				Valor = IdCliete
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NClientesPersonal.eFields.Estatus.ToString(),
				Condicion = "=",
				Valor = true
			});			
			return base.BuildPeticionVerb<List<CClientesPersonal>>(base.GetUrlApiService("SearchAll", "ClientesPersonal"), where, Method.POST);
		}


        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IClientesPersonal> ClientesPersonalXls = null;
                NExcel<CClientesPersonal> xls = new NExcel<CClientesPersonal>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    ClientesPersonalXls = NClientesPersonal.GetClientesPersonalFromTable(dt);
                }
                ClientesPersonalXls = this.BuildPeticionVerb<List<CClientesPersonal>>(this.GetUrlApiService("NewXls", "ClientesPersonal"), ClientesPersonalXls, RestSharp.Method.POST);
                Resp = PartialView("_ClientesPersonalWithError", ClientesPersonalXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class ClientesPersonalController  ends.
}//Namespace Controladores.MVC ends.

