using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class ContratosDetalles2Controller   : MvcBase
	{
		public ContratosDetalles2Controller ()
		{			
			_CatalogoNombre = "ContratosDetalles2";
			_InterfaceName = "IContratosDetalles2";
			_ViewEdicion = "IContratosDetalles2EditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index()
		{
			return base._UIIndex<CContratosDetalles2>();
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CContratosDetalles2>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CContratosDetalles2 entity)
		{           
			  return base._UINew<CContratosDetalles2>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CContratosDetalles2>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CContratosDetalles2 entity)
		{            
				return base._UIEditar<CContratosDetalles2>(entity.IdContratosDetalles, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CContratosDetalles2>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CContratosDetalles2>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CContratosDetalles2>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CContratosDetalles2>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CContratosDetalles2>(id, where);
	}
   }//class ContratosDetalles2Controller  ends.
}//Namespace Controladores.MVC ends.

