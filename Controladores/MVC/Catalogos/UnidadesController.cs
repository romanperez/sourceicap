using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class UnidadesController : MvcBase
	{
		public UnidadesController()
		{
			_CatalogoNombre = "Unidades";
			_InterfaceName = "IUnidades";
			_ViewEdicion = "IUnidadesEditarNew";
		}
		#region PROTECTED		
		protected override IEnumerable<IPropsWhere> _FiltroIndex()
		{
			NUnidades unidad = new NUnidades();
			return unidad.FiltroIndex(_Usuario, base.GetIdEmpresa().ToString());
		}
		protected virtual IEnumerable<IPropsWhere> _CombineWithFiltro(IEnumerable<IPropsWhere> where)
		{
			NUnidades unidad = new NUnidades();
			return unidad.CombineWithFiltro(_FiltroIndex(), where);
		}
		protected IUnidades SetIdEmpresaUser(IUnidades entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
			}
			return entity;
		}
		protected virtual IEnumerable<IPropsWhere> _FiltroTop(IEnumerable<IPropsWhere> where)
		{
			NUnidades unidades = new NUnidades();
			IEnumerable<IPropsWhere> NewWhere = unidades.CombineWithFiltro(_FiltroIndex(), where);
			return NewWhere;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CUnidades>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CUnidades>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CUnidades unidad = null;
			try
			{
				unidad = new CUnidades();
				base._UINew(_ViewEdicion);
				if(_Usuario != null)
				{
					unidad.Empresa = base.GetEmpresa();
				}
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return PartialView(_ViewEdicion, unidad);

		}
		[HttpPost]
		public ActionResult New(CUnidades entity)
		{	
			SetIdEmpresaUser(entity);
			return base._UINew<CUnidades>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CUnidades>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CUnidades entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CUnidades>(entity.IdUnidad, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CUnidades>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CUnidades>(_CombineWithFiltro(where));
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CUnidades>(id, _CombineWithFiltro(where));
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CUnidades>(_FiltroTop(where));
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CUnidades>(id, _FiltroTop(where));
		}
		[HttpGet]
		public ActionResult GetUnidadesByEmpresa(int id)
		{
			IEnumerable<IUnidades> unidades = null;
			try
			{
				unidades = _GetUnidadesByEmpresa(id);
			}
			catch(Exception eAlmUni)
			{
				OnError(eAlmUni);
			}
			return EvaluaErrorInterno(PartialView("../Shared/Unidades/_EmpresaUnidades", unidades));
		}
		public IEnumerable<IUnidades> _GetUnidadesByEmpresa(int id)
		{
			List<IPropsWhere> whereE = new List<IPropsWhere>();
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NUnidades.eFields.IdEmpresa.ToString(),
				Valor = id
			});
			whereE.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NUnidades.eFields.Estatus.ToString(),
				Valor = true
			});
			IEnumerable<CUnidades> allUnidades = this._Search<CUnidades>(base.GetUrlApiService("SearchAll","Unidades"), whereE);
			return allUnidades;
		}

        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IUnidades> UnidadesXls = null;
                NExcel<CUnidades> xls = new NExcel<CUnidades>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    UnidadesXls = NUnidades.GetUnidadesFromTable(dt);
                }
                UnidadesXls = this.BuildPeticionVerb<List<CUnidades>>(this.GetUrlApiService("NewXls", "Unidades"), UnidadesXls, RestSharp.Method.POST);
                Resp = PartialView("_UnidadesWithError", UnidadesXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class UnidadesController  ends.
}//Namespace Controladores.MVC ends.

