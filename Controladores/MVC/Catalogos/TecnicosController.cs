using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class TecnicosController : MvcBase
	{
		public TecnicosController()
		{
			_CatalogoNombre = "Tecnicos";
			_InterfaceName = "ITecnicos";
			_ViewEdicion = "ITecnicosEditarNew";
		}
		#region PROTECTED
		protected ITecnicos SetIdEmpresaUser(ITecnicos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.IdEmpresa = base.GetIdEmpresa();
				entity.Empresa = base.GetEmpresa();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			_InterfaceName = "IEmpleados";
			return base._UIIndex<CEmpleados>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			_InterfaceName = "IEmpleados";
			return base._UIIndexPaginado<CEmpleados>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CTecnicos entity = null;
			try
			{
				entity = new CTecnicos();
				entity.Empresa = base.GetEmpresa();
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return base._UINew(_ViewEdicion, entity);
		}
		[HttpPost]
		public ActionResult New(CTecnicos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UINew<CTecnicos>(entity);
		}
		[HttpPost]
		public ActionResult GradoTR(CTecnicosGrados grado)
		{
			EspecialidadesController cEspecialidades = new EspecialidadesController();
			IEnumerable<IEspecialidades> Especialidades = cEspecialidades.GetByGradoId(grado.IdGrado);
			return PartialView("_EspecialidadTR", Especialidades);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CTecnicos>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CTecnicos entity)
		{
			SetIdEmpresaUser(entity);
			return base._UIEditar<CTecnicos>(entity.IdEmpleado, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CTecnicos>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CEmpleados>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CEmpleados>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CTecnicos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CTecnicos>(id, where);
		}
		[HttpGet]
		public ActionResult TecnicosByEmpresa()
		{
			ActionResult resp = null;
			try
			{
				NTecnicos t = new NTecnicos();
				List<IPropsWhere> where = new List<IPropsWhere>();
				where.Add(t.FiltrosEmpresa(base.GetIdEmpresa()));
				string url = GetUrlApiService("SearchAll", "Tecnicos");
				IEnumerable<CTecnicos> Result = this._Search<CTecnicos>(url, where);
				resp=PartialView(Result);

			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(resp);
		}


        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<ITecnicos> TecnicosXls = null;
                NExcel<CTecnicos> xls = new NExcel<CTecnicos>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    TecnicosXls = NTecnicos.GetTecnicosFromTable(dt);
                }
                TecnicosXls = this.BuildPeticionVerb<List<CTecnicos>>(this.GetUrlApiService("NewXls", "Tecnicos"), TecnicosXls, RestSharp.Method.POST);
                Resp = PartialView("_TecnicosWithError", TecnicosXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class TecnicosController  ends.
}//Namespace Controladores.MVC ends.

