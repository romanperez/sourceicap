using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class PedidosController : MvcBase
	{
		public PedidosController()
		{
			_CatalogoNombre = "Pedidos";
			_InterfaceName = "IPedidos";
			_ViewEdicion = "IPedidosEditarNew";
		}
		#region PROTECTED
		protected IPedidos SetIdEmpresaUser(IPedidos entity)
		{
			if(_Usuario != null && entity != null)
			{
				entity.Empresa = base.GetEmpresa();
				entity.IdUnidad = base.GetIdUnidad();
				entity.Unidad = base.GetUnidad();
			}
			return entity;
		}
		#endregion
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CPedidos>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CPedidos>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			CPedidos entity = null;
			try
			{
				entity = new CPedidos();
				entity.FechaSolicitado = DateTime.Now;
				entity.FechaLlegada = null;
				entity.Empresa = base.GetEmpresa();
				entity.Unidad = base.GetUnidad();
			}
			catch(Exception eNew)
			{
				OnError(eNew);
			}
			return EvaluaErrorInterno(base._UINew(_ViewEdicion, entity));
		}
		[HttpPost]
		public ActionResult New(CPedidos entity)
		{
			SetIdEmpresaUser(entity);
			entity.Estatus = true;
			return base._UINew<CPedidos>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return EvaluaErrorInterno(base._UIEditar<CPedidos>(_ViewEdicion, id));
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CPedidos entity)
		{
			SetIdEmpresaUser(entity);
			entity.Estatus = true;
			return base._UIEditar<CPedidos>(entity.IdPedido, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CPedidos>(id);
		}

		[HttpPost]
		[ActionName("AddPedido")]
		public ActionResult AddPedido(CInsumos insumo)
		{
			return PartialView("_EditPedido", new NPedidosDetalles().ToDetallePedido(insumo));
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CPedidos>(where);
		}
		[HttpPost]
		[ActionName("Search")]
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CPedidos>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CPedidos>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CPedidos>(id, where);
		}
		
	}//class PedidosController  ends.
}//Namespace Controladores.MVC ends.

