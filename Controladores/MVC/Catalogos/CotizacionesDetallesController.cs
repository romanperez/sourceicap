using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class CotizacionesDetallesController   : MvcBase
	{
		public CotizacionesDetallesController ()
		{			
			_CatalogoNombre = "CotizacionesDetalles";
			_InterfaceName = "ICotizacionesDetalles";
			_ViewEdicion = "ICotizacionesDetallesEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CCotizacionesDetalles>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CCotizacionesDetalles>(id);
		}
		[HttpGet]
		public ActionResult New()
		{
			return base._UINew(_ViewEdicion);
		}		
		[HttpPost]
        public ActionResult New(CCotizacionesDetalles entity)
		{           
			  return base._UINew<CCotizacionesDetalles>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CCotizacionesDetalles>(_ViewEdicion, id);
		}
		[HttpPost]
      [ActionName("Put")]
		public ActionResult Editar(CCotizacionesDetalles entity)
		{            
				return base._UIEditar<CCotizacionesDetalles>(entity.IdCotizacionDetalle, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CCotizacionesDetalles>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
            return base._UIEliminaById(id);	
		}
        [HttpPost]
        [ActionName("SearchAll")]
        public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
        {
	     return base._UISearchAll<CCotizacionesDetalles>(where);
        }
        [HttpPost]
        [ActionName("Search")]
        public ActionResult Search(int id ,IEnumerable<CPropsWhere> where)
        {
	    return base._UISearch<CCotizacionesDetalles>(id, where);
        }
 	[HttpPost]
	[ActionName("Top")]	
        public ActionResult Top(IEnumerable<CPropsWhere> where)
	{
	   return base._Top<CCotizacionesDetalles>(where);
	}
	[HttpPost]
        [ActionName("TopCustom")]	
	public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
	{
	  return base._Top<CCotizacionesDetalles>(id, where);
	}
   }//class CotizacionesDetallesController  ends.
}//Namespace Controladores.MVC ends.

