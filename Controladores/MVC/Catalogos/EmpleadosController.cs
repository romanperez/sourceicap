﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using RestSharp;
using Entidades;
using Negocio;
using Datos;
namespace Controladores.MVC
{
	[IcapAuthorize]
	public class EmpleadosController : MvcBase
	{
		public EmpleadosController()
		{
			_CatalogoNombre = "Empleados";
			_InterfaceName = "IEmpleados";
			_ViewEdicion = "IEmpleadosEditarNew";			
		}
		[HttpGet]
		[ActionName("Index")]
		public ActionResult Index(int id)
		{
			return base._UIIndex<CEmpleados>(id);
		}
		[HttpGet]
		public ActionResult IndexPaginado(int id)
		{
			return base._UIIndexPaginado<CEmpleados>(id);
		}
		[HttpGet]
		public ActionResult New()
		{			
			CEmpleados empleado = null;
			try
			{
				empleado = new CEmpleados();
				empleado.Empresa = base.GetEmpresa();				
			}
			catch(Exception enew)
			{
				OnError(enew);
			}
			return _UINew(_ViewEdicion, empleado);
		}
		[HttpPost]
		public ActionResult New(CEmpleados entity)
		{
			entity.Estatus = true;
			return base._UINew<CEmpleados>(entity);
		}
		[HttpGet]
		public ActionResult Editar(int id)
		{
			return base._UIEditar<CEmpleados>(_ViewEdicion, id);
		}
		[HttpPost]
		[ActionName("Put")]
		public ActionResult Editar(CEmpleados entity)
		{
			entity.Estatus = true;
			return base._UIEditar<CEmpleados>(entity.IdEmpleado, entity);
		}
		[HttpGet]
		public ActionResult Eliminar(int id)
		{
			return base._UIEliminar<CEmpleados>(id);
		}
		[HttpPost]
		[ActionName("Delete")]
		public ActionResult EliminaById(int id)
		{
			return base._UIEliminaById(id);
		}
		[HttpPost]
		[ActionName("SearchAll")]		
		public ActionResult SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._UISearchAll<CEmpleados>(where);
		}
		[HttpPost]
		[ActionName("Search")]		
		public ActionResult Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._UISearch<CEmpleados>(id, where);
		}
		[HttpPost]
		[ActionName("Top")]		
		public ActionResult Top(IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEmpleados>(where);
		}
		[HttpPost]
		[ActionName("TopCustom")]		
		public ActionResult TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Top<CEmpleados>(id, where);
		}


        #region CargaMasiva
        [HttpGet]
        public override ActionResult PlantillaLoad()
        {
            return base.PlantillaLoad();
        }
        [HttpPost]
        public ActionResult NewXls()
        {
            ActionResult Resp = null;
            try
            {
                IEnumerable<IEmpleados> EmpleadosXls = null;
                NExcel<CEmpleados> xls = new NExcel<CEmpleados>();
                IEnumerable<CFileRequest> filesLoaded = base.FilesToByteArray(null);
                if (filesLoaded != null && filesLoaded.Count() > 0)
                {
                    System.Data.DataTable dt = xls.MainByteToExcel(filesLoaded.FirstOrDefault().Contenido);
                    EmpleadosXls = NEmpleados.GetEmpleadosFromTable(dt);
                }
                EmpleadosXls = this.BuildPeticionVerb<List<CEmpleados>>(this.GetUrlApiService("NewXls", "Empleados"), EmpleadosXls, RestSharp.Method.POST);
                Resp = PartialView("_EmpleadosWithError", EmpleadosXls);
            }
            catch (Exception eNew)
            {
                OnError(eNew);
            }
            return EvaluaErrorInterno(Resp);
        }
        #endregion
	}//class EmpleadosController ends.
}//Namespace Controladores.MVC ends.
