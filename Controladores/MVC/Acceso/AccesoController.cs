﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using Entidades;
using Idiomas;
using System.Web.Security;
using RestSharp;
using Negocio;
namespace Controladores.MVC
{
	[AllowAnonymous]
	public class AccesoController : MvcBase
	{
		 public ActionResult Login()
		 {
			 LoadIdiomas();
			 return View();
		 }//Login ends.
		 public ActionResult InicioSesion()
		 {			 
			 return PartialView();
		 }
		 public ActionResult GetIdiomas()
		 {
			 return PartialView();
		 }
		 [HttpPost]
		 public ActionResult SetIdioma(int id)
		 {
			 base.ChangeIdioma(id);
			 return RedirectToAction("Login");
		 }//Login ends.
		 [HttpPost]
		 public ActionResult Login(CUsuarios entidad)
		 {
			 //NConceptos con = new NConceptos();
			 //IConceptos c = new CConceptos();
			 //c.IdConcepto = 33;
			 //c.Concepto = "Cable";
			 //c.IdEmpresa = 1;
			 //IEnumerable<IEntidad> res=con.FindByPrimaryKey(con.MetaDatos(c, "IConceptos","Conceptos"));
			 //IEnumerable<IEntidad> res2 = con.FindByUniqueKey(con.MetaDatos(c, "IConceptos", "Conceptos"));
			 
			 // CExcel ee = new CExcel();
			// ee.DataTableToExcel();
			 if(ModelState.IsValid)
			 {
				 if(entidad == null) entidad = new CUsuarios();
				 Negocio.NUsuarios nuser = new NUsuarios();
				 entidad.Clave = nuser.ClaveMD5(entidad.Clave);
				 entidad = ValidaLoggin(entidad) as CUsuarios;
				 if(String.IsNullOrEmpty(entidad.InicioSession))
				 {
					 Session[IcapAuthorize.ICAP_SESSION_CLAVE] = entidad as IUsuarios;
					 FormsAuthentication.SetAuthCookie(entidad.Usuario, true);
					 return RedirectToAction("Index", "ICAP");
				 }				 
			 }
			 LoadIdiomas();
			 return View(entidad);			 
		 }//Login ends.
		 protected void LoadIdiomas()
		 {
			 ViewBag.LabelIdioma = Idiomas.IcapWeb.AccesoIdioma;
			 ViewBag.LabelInicioSession = Idiomas.IcapWeb.AccesoInicioSession;
			 ViewBag.Idiomas = CIdioma.GetAllIdiomas();
			 ViewBag.IdiomaSeleccionado = _Idioma.IdIdioma;
		 }
		 protected IUsuarios ValidaLoggin(CUsuarios entidad)
		 {
			 const string NO_DISPONIBLE ="Lo sentimos por el momento el servicio esta NO disponible";
			 try
			 {
				 IRestResponse response = BuildPeticionVerb("Loggin/InicioSession", entidad,Method.POST);
				 if(response != null)
				 {
					 entidad = entidad.ToJson<CUsuarios>(response.Content);
				 }
				 if(HasError())
				 {
					 entidad.InicioSession =String.Format("{0} {1}",_ErrorInterno.IdError, NO_DISPONIBLE);
				 }
			 }
			 catch(Exception eValida)
			 {
				 OnError(eValida);
				 if(HasError())
				 {
					 entidad.InicioSession = String.Format("{0} {1}", _ErrorInterno.IdError, NO_DISPONIBLE);
				 }
			 }
			 return entidad;
		 }
	}//AccesoAccesoController ends.
 }//Controladores.MVC ends.

