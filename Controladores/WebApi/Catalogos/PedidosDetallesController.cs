using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class PedidosDetallesController  : ApiBaseController
	{
		public PedidosDetallesController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NPedidosDetalles.eFields.IdPedidoDetalle.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CPedidosDetalles>(NPedidosDetalles.eFields.IdPedidoDetalle.ToString(), id);
      }        
		[HttpPost]
		public HttpResponseMessage New(CPedidosDetalles Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CPedidosDetalles>(NPedidosDetalles.eFields.IdPedidoDetalle.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CPedidosDetalles Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NPedidosDetalles.eFields.IdPedidoDetalle.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage ConfirmaRecepcion(List<CPedidosDetalles> entity)
		{
			try
			{				
				NPedidosDetalles pd = new NPedidosDetalles();
				pd.ConfirmaRecepcion(entity);
				if(pd.HasError())
				{
					SetError(pd.GetErrorMessage(), true);
				}
				base.BuildResponseOK<CPedidosDetalles>(new CPedidosDetalles());
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
        protected override void BuildContext()
        {
            _Context = new NContext<NPedidosDetalles>();
        }		 
	} //class PedidosDetallesController  ends.
}//namespace Controladores.API ends

