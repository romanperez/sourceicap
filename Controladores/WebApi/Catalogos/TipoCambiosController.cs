using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class TipoCambiosController : ApiBaseController
	{
		public TipoCambiosController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NTipoCambios.eFields.IdTipoCambio.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CTipoCambios>(NTipoCambios.eFields.IdTipoCambio.ToString(), id, FiltrosTipoCambios());
		}
		[HttpPost]
		public HttpResponseMessage New(CTipoCambios Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CTipoCambios>(NTipoCambios.eFields.IdTipoCambio.ToString(), id, FiltrosTipoCambios(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NTipoCambios d = new NTipoCambios();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NTipoCambios.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosTipoCambios(whereActivos));			
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CTipoCambios Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NTipoCambios.eFields.IdTipoCambio.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NTipoCambios>();
		}
		protected virtual IEnumerable<IPropsWhere> FiltrosTipoCambios()
		{
			NTipoCambios d = new NTipoCambios();
			List<CPropsWhere> whe = new List<CPropsWhere>();
			whe.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			return whe;
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosTipoCambios(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosTipoCambios();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
		[HttpPost]
		public HttpResponseMessage TipoCambioByFecha(CTipoCambios Entity)
		{
			try
			{
				NTipoCambios ntipocambio = new NTipoCambios();
				Entity = ntipocambio.GetIdValorCambio(Entity) as CTipoCambios;
				if(ntipocambio.HasError())
				{
					SetError(ntipocambio.GetErrorMessage());
				}
				if(Entity == null)
				{
					SetError(TraduceTexto("NCotizacionesNoTipoCambioValido"), true);
				}
				BuildResponse<ITipoCambios>(Entity);
			}
			catch(Exception eInterno)
			{
				OnError(eInterno);
			}
			return _Response;
		}		 
	} //class TipoCambiosController  ends.
}//namespace Controladores.API ends

