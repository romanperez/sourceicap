using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class ClientesPersonalController  : ApiBaseController
	{
		public ClientesPersonalController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NClientesPersonal.eFields.IdPersonalCliente.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CClientesPersonal>(NClientesPersonal.eFields.IdPersonalCliente.ToString(), id, FiltrosCP());
      }        
		[HttpPost]
		public HttpResponseMessage New(CClientesPersonal Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CClientesPersonal>(NClientesPersonal.eFields.IdPersonalCliente.ToString(), id,FiltrosCP(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FiltrosCP(where));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CClientesPersonal Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NClientesPersonal.eFields.IdPersonalCliente.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NClientesPersonal>();
        }
		  protected IEnumerable<IPropsWhere> FiltrosCP()
		  {
			  NClientesPersonal np = new NClientesPersonal();
			  return np.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId());
		  }
		  protected IEnumerable<CPropsWhere> FiltrosCP(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosCP();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }

          [HttpPost]
          public HttpResponseMessage NewXls(List<CClientesPersonal> Entity)
          {
              try
              {
                  int idEmpresa = base._EmpresaId();
                  NClientesPersonal save = new NClientesPersonal();
                  IEnumerable<IClientesPersonal> Result = save.SaveMasivo2(Entity, idEmpresa);
                  if (save.HasError())
                  {
                      SetError(save.GetErrorMessage());
                  }
                  BuildResponse<IEnumerable<IClientesPersonal>>(Result);
              }
              catch (Exception err)
              {
                  OnError(err);
              }
              return _Response;
          }
	} //class ClientesPersonalController  ends.
}//namespace Controladores.API ends

