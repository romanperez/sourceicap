using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class EstimacionesController : ApiBaseController
	{
		public EstimacionesController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NEstimaciones.eFields.IdEstimacion.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CEstimaciones>(NEstimaciones.eFields.IdEstimacion.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage New(CEstimaciones Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CEstimaciones>(NEstimaciones.eFields.IdEstimacion.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CEstimaciones Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NEstimaciones.eFields.IdEstimacion.ToString(), id);
		}

		[HttpPost]
		public HttpResponseMessage EstimacionesPorContrato(int id,List<CEstimaciones> estimaciones)
		{
			try
			{
				NEstimaciones nestimaciones = _Context.GetNegocio() as NEstimaciones;              
				IEstimaciones result = null;
				if(estimaciones != null && estimaciones.Count > 0)
				{
					nestimaciones.ObtieneCantidadProgramada(id,estimaciones.First().FechaInicio,estimaciones.First().FechaFin);                    
					nestimaciones.SaveMasivo(estimaciones);
					if(nestimaciones.HasError())
					{
						SetError(nestimaciones.GetErrorMessage());
					}
					else
					{
						result = new CEstimaciones();
                        result.IdEstimacion = id;
					}
				}
				BuildResponse<IEstimaciones>(result);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return _Response;
		}

		[HttpPost]
		public HttpResponseMessage EstimacionesPorContratoIndex(CContratosDetalles2 Entity)
		{
			try
			{
				NEstimaciones nestimaciones = _Context.GetNegocio() as NEstimaciones;
				IEnumerable<IContratosDetalles2> result = nestimaciones.EstimacionesPorContratoIndex(Entity.IdContrato, Entity.Contrato,Entity.Fecha,Entity.FechaFinal);
				if(nestimaciones.HasError())
				{
					SetError(nestimaciones.GetErrorMessage());
				}
				BuildResponse<IEnumerable<IContratosDetalles2>>(result);				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
        [HttpPost]
        public HttpResponseMessage NewEstimacion(CEstimaciones estimacion)
        {
            try
            {
                NContratos contrato = new NContratos();
                IContratos2 _contrato = contrato.NewEstimacion(estimacion.IdContratosDetalles,estimacion);
                BuildResponse<IContratos2>(_contrato);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage ValidaPeriodoEstimacion(CEstimaciones estimacion)
        {
            try
            {
                NEstimaciones nestimaciones = _Context.GetNegocio() as NEstimaciones;
                nestimaciones.ValidaPeriodoEstimacion(estimacion);
                if (nestimaciones.HasError() )
                {
                    SetError(nestimaciones.GetErrorMessage());
                }
                BuildResponse<IEstimaciones>(estimacion);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        #region REPORTES
        [HttpPost]
        public HttpResponseMessage ReporteEstimacionesCantidades(CEstimaciones estimacion)
        {
            try
            {
                NEstimaciones nestimaciones = _Context.GetNegocio() as NEstimaciones;
                IEnumerable<IEstimaciones> estimaciones= nestimaciones.ReporteEstimacionesCantidades(estimacion);
                if (nestimaciones.HasError())
                {
                    SetError(nestimaciones.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IEstimaciones>>(estimaciones);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage ReporteEstimacionesResumenEconomico(CEstimacionesResumenEconomico estimacion)
        {
            try
            {
                NEstimaciones nestimaciones = _Context.GetNegocio() as NEstimaciones;
                IEnumerable<IEstimacionesResumenEconomico> estimaciones = nestimaciones.ReporteEstimacionesResumenEconomico(estimacion);
                if (nestimaciones.HasError())
                {
                    SetError(nestimaciones.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IEstimacionesResumenEconomico>>(estimaciones);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        #endregion
        protected override void BuildContext()
		{
			_Context = new NContext<NEstimaciones>();
		}
	} //class EstimacionesController  ends.
}//namespace Controladores.API ends