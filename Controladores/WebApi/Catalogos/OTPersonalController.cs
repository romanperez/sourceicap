using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class OTPersonalController  : ApiBaseController
	{
		public OTPersonalController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NOTPersonal.eFields.IdOTPersonal.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<COTPersonal>(NOTPersonal.eFields.IdOTPersonal.ToString(), id);
      }        
		[HttpPost]
		public HttpResponseMessage New(COTPersonal Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<COTPersonal>(NOTPersonal.eFields.IdOTPersonal.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, COTPersonal Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NOTPersonal.eFields.IdOTPersonal.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NOTPersonal>();
        }
	} //class OTPersonalController  ends.
}//namespace Controladores.API ends

