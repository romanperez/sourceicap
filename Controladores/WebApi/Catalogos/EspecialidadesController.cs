using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class EspecialidadesController : ApiBaseController
	{
		protected string campoEmpresa;
		public EspecialidadesController()
		{
			BuildContext();
			campoEmpresa = NEspecialidades.eFields.IdEmpresa.ToString();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NEspecialidades.eFields.IdEspecialidad.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CEspecialidades>(NEspecialidades.eFields.IdEspecialidad.ToString(), 
				                                   id,
															  Filtros(this.campoEmpresa),
															  OrdenaEspecialidades);
		}
		[HttpPost]
		public HttpResponseMessage New(List<CEspecialidades> Entity)
		{
			if(Entity != null && Entity.Count > 0)
			{
				return base._New(Entity);
			}
			else
			{
				SetError(TraduceTexto("IEspecialidadesListaVacia"));
				base.BuildResponseERROR();
				return _Response;
			}
			
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CEspecialidades>(NEspecialidades.eFields.IdEspecialidad.ToString(), id, Filtros(this.campoEmpresa, where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{			
			NEspecialidades negocio = new NEspecialidades();
			IEnumerable<CPropsWhere> where2 = negocio.FiltroSoloEspecialidadesActivos(where).Select(x => x as CPropsWhere);
			return base._TopList(Filtros(this.campoEmpresa, where2));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, Filtros(this.campoEmpresa, where));
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CEspecialidades Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NEspecialidades.eFields.IdEspecialidad.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NEspecialidades>();
		}
		protected IEnumerable<Entity> OrdenaEspecialidades<Entity>(IEnumerable<Entity> especialidades) where Entity : IEntidad, new()
		{
			return especialidades.OrderBy(x => ((IEspecialidades)(x)).IdGrado);
		}

        [HttpPost]
        public HttpResponseMessage NewXls(List<CEspecialidades> Entity)
        {
            try
            {
                int idEmpresa = base._EmpresaId();
                NEspecialidades save = new NEspecialidades();
                IEnumerable<IEspecialidades> Result = save.SaveMasivo2(Entity, idEmpresa);
                if (save.HasError())
                {
                    SetError(save.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IEspecialidades>>(Result);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
	} //class EspecialidadesController  ends.
}//namespace Controladores.API ends

