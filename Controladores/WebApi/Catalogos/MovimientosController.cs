using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class MovimientosController  : ApiBaseController
	{
		public MovimientosController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return this._MyGetById(id,false,false);
		}
		[HttpGet]
		public HttpResponseMessage GetMovimiento(int id)
		{
			return this._MyGetById(id, false,true);
		}
		[HttpGet]
		public HttpResponseMessage GetSalida(int id)
		{
			return this._MyGetById(id, true,false);
		}
		 [HttpGet]
		public HttpResponseMessage GetTraspaso(int id)
		{
			return this._MyGetById(id, true,false);
		}
		 
		[HttpPost]
		public HttpResponseMessage IndexEntradas(int id,IEnumerable<CPropsWhere> where)
		{
			return _GetPageMovimientosByTipo(id,(int) NMovimientos.eTipoMovimientos.Entrada,where);
		}
		[HttpPost]
		public HttpResponseMessage IndexSalidas(int id, IEnumerable<CPropsWhere> where)
		{
			return _GetPageMovimientosByTipo(id, (int)NMovimientos.eTipoMovimientos.Salida, where);
		}
		[HttpPost]
		public HttpResponseMessage IndexTraspasos(int id, IEnumerable<CPropsWhere> where)
		{
			return _GetPageMovimientosByTipo(id, (int)NMovimientos.eTipoMovimientos.Traspaso, where);
		}  
 		[HttpPost]
		public HttpResponseMessage PaginadoEntradas(int id, IEnumerable<CPropsWhere> where)
      {
			return _GetPageMovimientosByTipo(id,(int) NMovimientos.eTipoMovimientos.Entrada,where);
      }
		[HttpPost]
		public HttpResponseMessage PaginadoSalidas(int id, IEnumerable<CPropsWhere> where)
		{
			return _GetPageMovimientosByTipo(id, (int)NMovimientos.eTipoMovimientos.Salida, where);
		}
		[HttpPost]
		public HttpResponseMessage PaginadoTraspasos(int id, IEnumerable<CPropsWhere> where)
		{
			return _GetPageMovimientosByTipo(id, (int)NMovimientos.eTipoMovimientos.Traspaso, where);
		}   
		[HttpPost]
		public HttpResponseMessage New(CMovimientos Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CMovimientos>("Movimientos."+ NMovimientos.eFields.IdMovimiento.ToString(), id,FiltrosMovimientos( where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPost]
		public HttpResponseMessage BuscaMovimiento(CFiltrosMovimiento Entity)
		{
			try
			{
				NMovimientos mov = new NMovimientos();
				IEnumerable<IMovimientos> res= mov.BuscaMovimiento(Entity);
				if(mov.HasError())
				{
					SetError(mov.GetErrorMessage(),true);
				}
				BuildResponseOK<IEnumerable<IMovimientos>>(res);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CMovimientos Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NMovimientos.eFields.IdMovimiento.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NMovimientos>();
        }

		  #region PROTECTED
		  protected virtual HttpResponseMessage _GetPageMovimientosByTipo(int pagina, int idMovimiento, IEnumerable<CPropsWhere> where)
		  {
			  try
			  {				 
				  IPagina<CMovimientos> _pagina = null;
				  NMovimientos movimiento = new NMovimientos();
				  _pagina = movimiento.GetPageMovimientosByTipo(idMovimiento, pagina,where);				 
				  if(_pagina == null)
				  {
					  SetError(Item_NoFound);
				  }
				  base.BuildResponse<IPagina<CMovimientos>>(_pagina);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return _Response;
		  }		 
		 protected virtual HttpResponseMessage _MyGetById(int id,bool cargaExistencias,bool IsQuery)
		 {
			 try
			 {
				 IEntidad Entidad = null;
				 IMovimientos Movimiento;
				 if(_Context != null)
				 {
					 Entidad = _Context.GetById(NMovimientos.eFields.IdMovimiento.ToString(), id);
					 Movimiento = Entidad as IMovimientos;
					 if(Entidad != null)
					 {
						 NMovimientos movs = new NMovimientos();
						 Entidad = movs.ObtieneDetalles(Movimiento, cargaExistencias);
						 movs.SetDatosUnidadesAlmacenes(Movimiento);
						 movs.SetSolicituInsumoAsociada(Movimiento);
						 bool esValidoEditar = (!IsQuery) ? movs.EsMovimientoValidoEditar(Movimiento) : IsQuery;
						 NMovimientos.eTipoMovimientos Tipo = (NMovimientos.eTipoMovimientos)Movimiento.Tipo;
						 if(Tipo!=NMovimientos.eTipoMovimientos.Traspaso && ((IMovimientos)Entidad).TieneSolicituInsumoAsociada && !IsQuery)
						 {
							SetError(base.TraduceTexto("NMovimientosSalidaNoEditable"));
						 }
						 if(!esValidoEditar)
						 {
							 SetError(base.TraduceTexto("NMovimientosNoEsValidoParaEditar"));
						 }
						 ///La validacion para editarlo va aqui.
					 }
				 }
				 else
				 {
					 SetError(No_Context);
				 }
				 if(Entidad == null)
				 {
					 SetError(Item_NoFound);
				 }
				 BuildResponse<IEntidad>(Entidad);
			 }
			 catch(Exception e)
			 {
				 OnError(e);
			 }
			 return _Response;
		 }		  
		 #endregion
		 protected virtual IEnumerable<CPropsWhere> FiltrosMovimientos()
		 {
			 NMovimientos d = new NMovimientos();
			 return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
		 }
		 protected virtual IEnumerable<CPropsWhere> FiltrosMovimientos(IEnumerable<CPropsWhere> where1)
		 {
			 IEnumerable<IPropsWhere> where2 = this.FiltrosMovimientos();
			 return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		 }
	} //class MovimientosController  ends.
}//namespace Controladores.API ends

