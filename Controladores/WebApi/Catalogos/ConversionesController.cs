using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class ConversionesController : ApiBaseController
	{
		public ConversionesController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NConversiones.eFields.IdConversion.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CConversiones>(NConversiones.eFields.IdConversion.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage New(CConversiones Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CConversiones>(NConversiones.eFields.IdConversion.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CConversiones Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NConversiones.eFields.IdConversion.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage ConversionMoneda(CTipoCambios Entity)
		{
			try
			{				
				NConversiones conversion = new NConversiones();
				Entity=  conversion.ConvierteCantidad(Entity) as CTipoCambios;
				if(conversion.HasError() )
				{
					SetError(conversion.GetErrorMessage());
				}				
				BuildResponse<ITipoCambios>(Entity);
			}
			catch(Exception eInterno)
			{
				OnError(eInterno);
			}
			return _Response;
		}		 
		protected override void BuildContext()
		{
			_Context = new NContext<NConversiones>();
		}
	} //class ConversionesController  ends.
}//namespace Controladores.API ends

