using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class DepartamentosController  : ApiBaseController
	{
		public DepartamentosController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NDepartamentos.eFields.IdDepartamento.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CDepartamentos>(NDepartamentos.eFields.IdDepartamento.ToString(), id, FiltrosDepartamentos());
      }        
		[HttpPost]
		public HttpResponseMessage New(CDepartamentos Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CDepartamentos>(NDepartamentos.eFields.IdDepartamento.ToString(), id,FiltrosDepartamentos(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NDepartamentos d= new NDepartamentos();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NDepartamentos.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosDepartamentos(whereActivos));			
		}
		[HttpPost]
		public HttpResponseMessage TopByEmpresa(IEnumerable<CPropsWhere> where)
		{
			NDepartamentos d = new NDepartamentos();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NDepartamentos.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			List<IPropsWhere> where1 = new List<IPropsWhere>();
			where1.Add(d.FiltrosEmpresa(base._EmpresaId()));
			return base._TopList(_Context.Filtros(whereActivos, where1).Select(x => x as CPropsWhere),this.PersonalizaDepartamentos);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CDepartamentos Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NDepartamentos.eFields.IdDepartamento.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NDepartamentos>();
        }
		  protected virtual IEnumerable<CPropsWhere> FiltrosDepartamentos()
		  {
			  NDepartamentos d = new NDepartamentos();
			  return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x=>x as CPropsWhere);
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosDepartamentos(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosDepartamentos();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
		  protected IEnumerable<IEntidad> PersonalizaDepartamentos(IEnumerable<IEntidad> result)
		  {
			  return from tbl in result
						select new CDepartamentos()
						{
							Nombre = String.Format("[{0}] {1}", ((IDepartamentos)tbl).Unidad, ((IDepartamentos)tbl).Nombre),
							Codigo= ((IDepartamentos)tbl).Codigo,
							Empresa = ((IDepartamentos)tbl).Empresa,
							Estatus = ((IDepartamentos)tbl).Estatus,
							IdDepartamento = ((IDepartamentos)tbl).IdDepartamento,
							IdEmpresa = ((IDepartamentos)tbl).IdEmpresa,
							IdUnidad = ((IDepartamentos)tbl).IdUnidad,
							/*Nombre = ((IDepartamentos)tbl).Nombre,*/
							Unidad = ((IDepartamentos)tbl).Unidad
						};
		  }
		  public override IExcel BuildExcelExport()
		  {
			  NExcel<CDepartamentos> excel = new NExcel<CDepartamentos>();
			  excel.InterfaceName = "IDepartamentos";
			  excel.NameSheet = "Departamentos";
			  excel.FileName = "Departamentos.xlsx";
			  return excel;
		  }
		  protected override IEnumerable<CPropsWhere> FiltraBusqueda(IEnumerable<CPropsWhere> where)
		  {
			  if(where != null && where.Count() > 0)
			  {
				  return FiltrosDepartamentos(where);
			  }
			  else
			  {
				  return FiltrosDepartamentos();
			  }
		  }

          [HttpPost]
          public HttpResponseMessage NewXls(List<CDepartamentos> Entity)
          {
              try
              {
                  int idEmpresa = base._EmpresaId();
                  NDepartamentos save = new NDepartamentos();
                  IEnumerable<IDepartamentos> Result = save.SaveMasivo2(Entity, idEmpresa);
                  if (save.HasError())
                  {
                      SetError(save.GetErrorMessage());
                  }
                  BuildResponse<IEnumerable<IDepartamentos>>(Result);
              }
              catch (Exception err)
              {
                  OnError(err);
              }
              return _Response;
          }
	} //class DepartamentosController  ends.
}//namespace Controladores.API ends

