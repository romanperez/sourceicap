using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class FamiliasController : ApiBaseController
	{
		public FamiliasController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NFamilias.eFields.IdFamilia.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CFamilias>(NFamilias.eFields.IdFamilia.ToString(), id, FiltrosFamilias());
		}
		[HttpPost]
		public HttpResponseMessage New(CFamilias Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CFamilias>(NFamilias.eFields.IdFamilia.ToString(), id,FiltrosFamilias(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NFamilias d = new NFamilias();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NFamilias.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosFamilias(whereActivos));				
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CFamilias Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NFamilias.eFields.IdFamilia.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NFamilias>();
		}
		protected virtual IEnumerable<IPropsWhere> FiltrosFamilias()
		{
			NFamilias d = new NFamilias();
			List<CPropsWhere> whe = new List<CPropsWhere>();
			whe.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			return whe ;
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosFamilias(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosFamilias();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
	} //class FamiliasController  ends.
}//namespace Controladores.API ends

