using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class ContratosController : ApiBaseController
	{
		public ContratosController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NContratos.eFields.IdContrato.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CContratos2>(NContratos.eFields.IdContrato.ToString(), id, FiltrosContratosOnlyEmpresa(null));
		}
		[HttpPost]
		public HttpResponseMessage New(CContratos2 Entity)
		{
			Entity.IdUsuario = _IdUsuario();
			if(Entity.CostosManuales != null)
			{
				Entity.IdUsuarioCostos = _IdUsuario();				
			}
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CContratos2>(NContratos.eFields.IdContrato.ToString(), id, FiltrosContratosOnlyEmpresa(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopSinProyecto(IEnumerable<CPropsWhere> where)
		{
			NContratos c = new NContratos();
			return this._TopList(FiltrosContratos(where), c.ContratosSinProyecto);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CContratos2 Entity)
		{
			Entity.IdUsuario = _IdUsuario();
			if(Entity.CostosManuales != null)
			{
				Entity.IdUsuarioCostos = _IdUsuario();			
			}
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{			
			return base._Delete(NContratos.eFields.IdContrato.ToString(), id);
		}
		public HttpResponseMessage GeneraContrato(CContratos2 Entity)
		{
			try
			{
				NContratos c = new NContratos();
				Entity.IdUsuario = _IdUsuario();
				Entity = c.GeneraFromProyecto(Entity as CContratos2) as CContratos2;
				if(c.HasError())
				{
					SetError(c.GetErrorMessage());
				}
				BuildResponse<IEntidad>(Entity);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			
			return _Response;
		}
		public HttpResponseMessage ObtienePrecios(CContratos2 Entity)
		{
			try
			{
				NContratos c = new NContratos();				
				c.ObtienePrecios(Entity);
				if(c.HasError())
				{
					SetError(c.GetErrorMessage());
				}
				BuildResponse<IEntidad>(Entity);
			}
			catch(Exception e)
			{
				OnError(e);
			}

			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage GeneraCotizacionFromContrato(CContratos2 entity)
		{
			try
			{				
				NContratos c = new NContratos();
				c.GeneraCotizacionFromContrato(entity);
				if(c.HasError())
				{
					SetError(c.GetErrorMessage());
				}
				BuildResponse<IEntidad>(entity);			
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage SolicitarInsumosByProyecto(int id)
		{
			try
			{
				NProyectos p = new NProyectos();
				CProyectos entity = new CProyectos();
				entity.IdProyecto = id;								
				p.SolicitarInsumosByProyecto(entity,base._IdUsuario());
				if(p.HasError())
				{
					SetError(p.GetErrorMessage());
				}
				BuildResponse<IEntidad>(entity);				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}

		

		[HttpPost]
		public HttpResponseMessage CotizacionesGeneradas(CContratos2 entity)
		{
			try
			{
				IEnumerable<IContratosCotizaciones> cotizaciones = null;
				NContratos c = new NContratos();				
				if(c.HasError())
				{
					SetError(c.GetErrorMessage());
				}
				cotizaciones = c.CotizacionesGeneradas(entity);
				BuildResponse<IEnumerable<IContratosCotizaciones>>(cotizaciones);				
			}
			catch(Exception e)
			{
				OnError(e);
			}

			return _Response;
		}

		protected override void BuildContext()
		{
			_Context = new NContext<NContratos>();			
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosContratosOnlyEmpresa(IEnumerable<CPropsWhere> where1)
		{
			NContratos d = new NContratos();
			CPropsWhere empresa = d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere;
			List<CPropsWhere> where2 = new List<CPropsWhere>();
			where2.Add(empresa);
			if(where1 == null)
			{
				where1 = new List<CPropsWhere>();
			}
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosContratos()
		{
			NDepartamentos d = new NDepartamentos();
			return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosContratos(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosContratos();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
		
	} //class ContratosController  ends.
}//namespace Controladores.API ends