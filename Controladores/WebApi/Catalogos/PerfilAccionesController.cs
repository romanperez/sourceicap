using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class PerfilAccionesController  : ApiBaseController
	{
		public PerfilAccionesController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NPerfilAcciones.eFields.IdPerfilAcciones.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CPerfilAcciones>(NPerfilAcciones.eFields.IdPerfilAcciones.ToString(), id);
      }        
		[HttpPost]
		public HttpResponseMessage New(CPerfilAcciones Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CPerfilAcciones>(NPerfilAcciones.eFields.IdPerfilAcciones.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CPerfilAcciones Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NPerfilAcciones.eFields.IdPerfilAcciones.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NPerfilAcciones>();
        }
	} //class PerfilAccionesController  ends.
}//namespace Controladores.API ends

