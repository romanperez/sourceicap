﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class EmpleadosController : ApiBaseController
	{
		 public EmpleadosController()
        {
            BuildContext();            
        }       
      [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NEmpleados.eFields.IdEmpleado.ToString(), id);
		}      
 		[HttpPost]
		public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CEmpleados>(NEmpleados.eFields.IdEmpleado.ToString(), id, FiltrosEmpleados());
      }        
		[HttpPost]
		public HttpResponseMessage New(CEmpleados Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CEmpleados>(NEmpleados.eFields.IdEmpleado.ToString(), id, FiltrosEmpleados(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NEmpleados e = new NEmpleados();
			IEnumerable<CPropsWhere> whereActivos = e.FiltroActivos(NEmpleados.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosEmpleados(whereActivos));				
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CEmpleados Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
            return base._Delete(NEmpleados.eFields.IdEmpleado.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NEmpleados>();
        }
		  protected virtual IEnumerable<CPropsWhere> FiltrosEmpleados()
		  {
			  NEmpleados d = new NEmpleados();
			  List<CPropsWhere> where = new List<CPropsWhere>();			  
			  where.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			  return where;
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosEmpleados(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosEmpleados();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
		  public override IExcel BuildExcelExport()
		  {
			  NExcel<CEmpleados> excel = new NExcel<CEmpleados>();
			  excel.InterfaceName = "IEmpleados";
			  excel.NameSheet = "Empleados";
			  excel.FileName = "Empleados.xlsx";
			  return excel;
		  }
		  protected override IEnumerable<CPropsWhere> FiltraBusqueda(IEnumerable<CPropsWhere> where)
		  {
			  if(where != null && where.Count() > 0)
			  {
				  return FiltrosEmpleados(where);
			  }
			  else
			  {
				  return FiltrosEmpleados();
			  }
		  }


          [HttpPost]
          public HttpResponseMessage NewXls(List<CEmpleados> Entity)
          {
              try
              {
                  int idEmpresa = base._EmpresaId();
                  NEmpleados save = new NEmpleados();
                  IEnumerable<IEmpleados> Result = save.SaveMasivo2(Entity, idEmpresa);
                  if (save.HasError())
                  {
                      SetError(save.GetErrorMessage());
                  }
                  BuildResponse<IEnumerable<IEmpleados>>(Result);
              }
              catch (Exception err)
              {
                  OnError(err);
              }
              return _Response;
          }
		 
    } //class EmpleadosController ends.
}//namespace Controladores.API ends