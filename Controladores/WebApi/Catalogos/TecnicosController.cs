using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class TecnicosController  : ApiBaseController
	{
		 protected string campoEmpresa;
		 public TecnicosController ()
        {
            BuildContext();
			   DEmpresas emp = new DEmpresas();
				campoEmpresa = String.Format("{0}.{1}",emp.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
         //return base._Get(NTecnicos.eFields.IdTecnico.ToString(), id);
			return _GetTecnico(id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return _GetPage<CEmpleados>(String.Empty, id, Filtros(this.campoEmpresa, true));
      }        
		[HttpPost]
		public HttpResponseMessage New(CTecnicos Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CTecnicos>(NTecnicos.eFields.IdTecnico.ToString(), id, Filtros(this.campoEmpresa,true, where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(Filtros(this.campoEmpresa, true, where));			
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CTecnicos Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return this._Delete(id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NTecnicos>();
        }
		 // protected override HttpResponseMessage _GetPage<Entity>(string key, int pagina)
		 protected override HttpResponseMessage _GetPage<Entity>(string key, int pagina, IEnumerable<IPropsWhere> where)
		 {
			  try
			  {
				  IPagina<Entity> _pagina = null;
				  NTecnicos tec = new NTecnicos();
				  if(where != null)
				  {
					  tec.AddWhere(where);
				  }
				  _pagina = tec.BuscarIndex<Entity>(pagina);				  
				  if(_pagina == null)
				  {
					  SetError(Item_NoFound);
				  }
				  BuildResponse<IPagina<Entity>>(_pagina);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return _Response;
		  }
		 protected override HttpResponseMessage _Search<Entity>(string key, int pagina, IEnumerable<CPropsWhere> where)
		 {
			 return _GetPage<Entity>(key, pagina, where);
		 }
		 protected ITecnicos GetTecnico(int IdEmpleado)
		 {
			 NTecnicos tec = new NTecnicos();
			 return tec.GetTecnicoByIdEmpleado(IdEmpleado);
		 }
		 protected virtual HttpResponseMessage _GetTecnico(int IdEmpleado)
		 {
			 try
			 {
				 BuildResponse<ITecnicos>(GetTecnico(IdEmpleado));
			 }
			 catch(Exception e)
			 {
				 OnError(e);
			 }
			 return _Response;
		 }
		 protected virtual HttpResponseMessage _Delete(int IdEmpleado)
		 {
			 try
			 {
				 ITecnicos tec= GetTecnico(IdEmpleado);
				 NTecnicos ntec = new NTecnicos(tec);
				 ntec.Delete();
				 BuildResponse<string>(Item_Deleted);
			 }
			 catch(Exception e)
			 {
				 OnError(e);
			 }
			 return _Response;
		 }

         [HttpPost]
         public HttpResponseMessage NewXls(List<CTecnicos> Entity)
         {
             try
             {
                 int idEmpresa = base._EmpresaId();
                 NTecnicos save = new NTecnicos();
                 IEnumerable<ITecnicos> Result = save.SaveMasivo2(Entity, idEmpresa);
                 if (save.HasError())
                 {
                     SetError(save.GetErrorMessage());
                 }
                 BuildResponse<IEnumerable<ITecnicos>>(Result);
             }
             catch (Exception err)
             {
                 OnError(err);
             }
             return _Response;
         }
	} //class TecnicosController  ends.
}//namespace Controladores.API ends

