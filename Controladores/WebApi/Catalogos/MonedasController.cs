using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class MonedasController : ApiBaseController
	{
		protected string campoEmpresa;
		public MonedasController()
		{
			BuildContext();
			campoEmpresa = NMonedas.eFields.IdEmpresa.ToString();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NMonedas.eFields.IdMoneda.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CMonedas>(NMonedas.eFields.IdMoneda.ToString(), id, Filtros(this.campoEmpresa));
		}
		[HttpPost]
		public HttpResponseMessage New(CMonedas Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CMonedas>(NMonedas.eFields.IdMoneda.ToString(), id, Filtros(this.campoEmpresa,where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NMonedas alm = new NMonedas();
			IEnumerable<CPropsWhere> whereActivos = alm.FiltroActivos(NAlmacenes.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(Filtros(this.campoEmpresa, whereActivos));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, Filtros(this.campoEmpresa,where));
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CMonedas Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NMonedas.eFields.IdMoneda.ToString(), id);
		}

		[HttpGet]
		public HttpResponseMessage GetPesos(int id)/*IdEmpresa para obtner el registro de la moneda PESOS MEXICANOS*/
		{
			IMonedas moneda = NConversiones.GetMonedaPesos(id);
			BuildResponse<IEntidad>(moneda);
			return _Response;
		}

		protected override void BuildContext()
		{
			_Context = new NContext<NMonedas>();
		}
		
	} //class MonedasController  ends.
}//namespace Controladores.API ends

