using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class ContratosDetallesController  : ApiBaseController
	{
		public ContratosDetallesController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NContratosDetalles.eFields.IdContratoDetalle.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage ContratoDetalles(CContratos contrato)
		{
			return this._ContratoDetalles(contrato);
		}
		[HttpPost]
		public HttpResponseMessage ContratoDetallesInsumos(CContratos contrato)
		{
			return this._ContratoDetallesInsumos(contrato);
		}   
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CContratosDetalles>(NContratosDetalles.eFields.IdContratoDetalle.ToString(), id);
      }        
		[HttpPost]
		public HttpResponseMessage New(CContratosDetalles Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CContratosDetalles>(NContratosDetalles.eFields.IdContratoDetalle.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CContratosDetalles Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NContratosDetalles.eFields.IdContratoDetalle.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NContratosDetalles>();
        }
		  protected virtual HttpResponseMessage _ContratoDetalles(CContratos contrato)
		  {
			  try
			  {
				  /*NContratosDetalles detalle = new NContratosDetalles();
				  if(contrato.IdContrato <= 0)
				  {
					  NContratos NContrato = new NContratos();
					  IEntidad Entidad = NContrato.CotizacionByProyecto(contrato.IdProyecto);
					  if(Entidad == null)
					  {
						  SetError(Item_NoFound);
					  }
					  IEnumerable<ICotizacionesDetalles> detalles = ((ICotizaciones)(Entidad)).Conceptos;
					  BuildResponse<IEnumerable<IContratosDetalles>>(detalle.ToContratoDetalle(detalles));
				  }
				  else
				  {
					  BuildResponse<IEnumerable<IContratosDetalles>>(detalle.GetDetalleContratoById(contrato.IdContrato));
				  }*/
				  NContratos detalle = new NContratos();
				  IEnumerable<IContratosDetalles> conceptos=detalle.GetConceptos(contrato);
				  BuildResponse<IEnumerable<IContratosDetalles>>(conceptos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return _Response;
		  }
		  protected virtual HttpResponseMessage _ContratoDetallesInsumos(CContratos contrato)
		  {
			  try
			  {
				  NContratos detalle = new NContratos();
				  IEnumerable<IContratosDetallesInsumos> insumos = detalle.GetInsumos(contrato);
				  BuildResponse<IEnumerable<IContratosDetallesInsumos>>(insumos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return _Response;
		  }
	} //class ContratosDetallesController  ends.
}//namespace Controladores.API ends

