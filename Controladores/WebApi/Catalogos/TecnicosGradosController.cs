using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class TecnicosGradosController  : ApiBaseController
	{
		 protected string campoEmpresa;
		 public TecnicosGradosController ()
        {
            BuildContext();
				campoEmpresa = NTecnicosGrados.eFields.IdEmpresa.ToString();
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NTecnicosGrados.eFields.IdGrado.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CTecnicosGrados>(NTecnicosGrados.eFields.IdGrado.ToString(), id, Filtros(this.campoEmpresa));
      }        
		[HttpPost]
		public HttpResponseMessage New(CTecnicosGrados Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CTecnicosGrados>(NTecnicosGrados.eFields.IdGrado.ToString(), id, Filtros(this.campoEmpresa, where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NTecnicosGrados negocio = new NTecnicosGrados();
			IEnumerable<CPropsWhere> where2 = negocio.FiltroActivos(NTecnicosGrados.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(Filtros(this.campoEmpresa, where2));
		}		
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CTecnicosGrados Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NTecnicosGrados.eFields.IdGrado.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NTecnicosGrados>();
        }


        [HttpPost]
        public HttpResponseMessage NewXls(List<CTecnicosGrados> Entity)
        {
            try
            {
                int idEmpresa = base._EmpresaId();
                NTecnicosGrados save = new NTecnicosGrados();
                IEnumerable<ITecnicosGrados> Result = save.SaveMasivo2(Entity, idEmpresa);
                if (save.HasError())
                {
                    SetError(save.GetErrorMessage());
                }
                BuildResponse<IEnumerable<ITecnicosGrados>>(Result);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
	} //class TecnicosGradosController  ends.
}//namespace Controladores.API ends

