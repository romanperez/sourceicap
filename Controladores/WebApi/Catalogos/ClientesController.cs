using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class ClientesController : ApiBaseController
	{
		public ClientesController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NClientes.eFields.IdCliente.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CClientes>(NClientes.eFields.IdCliente.ToString(), id, FiltrosCliente());
		}
		[HttpPost]
		public HttpResponseMessage New(CClientes Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CClientes>(NClientes.eFields.IdCliente.ToString(), id, FiltrosCliente(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NClientes cli = new NClientes();
			IEnumerable<CPropsWhere> whereActivos = cli.FiltroActivos(NClientes.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);			 			
			return base._TopList(FiltrosCliente(whereActivos));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CClientes Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NClientes.eFields.IdCliente.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NClientes>();
		}
		protected IEnumerable<IPropsWhere> FiltrosCliente()
		{
			NClientes np = new NClientes();
			return np.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId());
		}
		protected IEnumerable<CPropsWhere> FiltrosCliente(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosCliente();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}


        [HttpPost]
        public HttpResponseMessage NewXls(List<CClientes> Entity)
        {
            try
            {
                int idEmpresa = base._EmpresaId();
                NClientes save = new NClientes();
                IEnumerable<IClientes> Result = save.SaveMasivo2(Entity, idEmpresa);
                if (save.HasError())
                {
                    SetError(save.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IClientes>>(Result);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
	} //class ClientesController  ends.
}//namespace Controladores.API ends

