using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class UnidadesMedidaController  : ApiBaseController
	{		
		 protected string campoEmpresa;
		 public UnidadesMedidaController ()
        {
            BuildContext();
				this.campoEmpresa = NUnidadesMedida.eFields.IdEmpresa.ToString();
        }
		
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NUnidadesMedida.eFields.IdUnidad.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CUnidadesMedida>(NUnidadesMedida.eFields.IdUnidad.ToString(), id, Filtros(this.campoEmpresa));
      }        
		[HttpPost]
		public HttpResponseMessage New(CUnidadesMedida Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CUnidadesMedida>(NUnidadesMedida.eFields.IdUnidad.ToString(), id, this.Filtros(this.campoEmpresa,where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NUnidadesMedida alm = new NUnidadesMedida();
			IEnumerable<CPropsWhere> whereActivos = alm.FiltroActivos(NAlmacenes.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(Filtros(this.campoEmpresa, whereActivos));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, Filtros(this.campoEmpresa, where));
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CUnidadesMedida Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NUnidadesMedida.eFields.IdUnidad.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NUnidadesMedida>();
        }
		  
	} //class UnidadesMedidaController  ends.
}//namespace Controladores.API ends

