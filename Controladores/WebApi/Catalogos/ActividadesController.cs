using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class ActividadesController  : ApiBaseController
	{
		public ActividadesController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NActividades.eFields.IdActividad.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CActividades>(NActividades.eFields.IdActividad.ToString(), id, FitrosActividades());
      }        
		[HttpPost]
		public HttpResponseMessage New(CActividades Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CActividades>(NActividades.eFields.IdActividad.ToString(), id, FitrosActividades(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FitrosActividades(where));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CActividades Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NActividades.eFields.IdActividad.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NActividades>();
        }
		  protected virtual IEnumerable<CPropsWhere> FitrosActividades()
		  {
			  NDepartamentos d = new NDepartamentos();
			  List<CPropsWhere> where1 = new List<CPropsWhere>();
				where1.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
				return where1;
		  }
		  protected virtual IEnumerable<CPropsWhere> FitrosActividades(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FitrosActividades();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
	} //class ActividadesController  ends.
}//namespace Controladores.API ends

