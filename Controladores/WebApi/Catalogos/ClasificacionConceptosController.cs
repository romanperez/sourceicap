using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class ClasificacionConceptosController : ApiBaseController
	{
		public ClasificacionConceptosController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CClasificacionConceptos>(NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString(), id,FiltrosClasificacion());
		}
		[HttpPost]
		public HttpResponseMessage New(CClasificacionConceptos Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CClasificacionConceptos>(NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString(), id,FiltrosClasificacion(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FiltrosClasificacion(where));	
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CClasificacionConceptos Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NClasificacionConceptos>();
		}
		protected virtual IEnumerable<IPropsWhere> FiltrosClasificacion()
		{
			NClasificacionConceptos d = new NClasificacionConceptos();
			List<CPropsWhere> whe = new List<CPropsWhere>();
			whe.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			return whe;
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosClasificacion(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosClasificacion();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}

        [HttpPost]
        public HttpResponseMessage NewXls(List<CClasificacionConceptos> Entity)
        {
            try
            {
                int idEmpresa = base._EmpresaId();
                NClasificacionConceptos save = new NClasificacionConceptos();
                IEnumerable<IClasificacionConceptos> Result = save.SaveMasivo2(Entity, idEmpresa);
                if (save.HasError())
                {
                    SetError(save.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IClasificacionConceptos>>(Result);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
	} //class ClasificacionConceptosController  ends.
}//namespace Controladores.API ends