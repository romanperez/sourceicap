using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class CapitulosController  : ApiBaseController
	{
		public CapitulosController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NCapitulos.eFields.IdCapitulo.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CCapitulos>(NCapitulos.eFields.IdCapitulo.ToString(), id, FiltrosCapitulos());
      }        
		[HttpPost]
		public HttpResponseMessage New(CCapitulos Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CCapitulos>(NCapitulos.eFields.IdCapitulo.ToString(), id,FiltrosCapitulos( where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NCapitulos d = new NCapitulos();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NCapitulos.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosCapitulos(whereActivos));						
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CCapitulos Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NCapitulos.eFields.IdCapitulo.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NCapitulos>();
        }
		  protected virtual IEnumerable<IPropsWhere> FiltrosCapitulos()
		  {
			  NCapitulos d = new NCapitulos();
			  List<CPropsWhere> whe = new List<CPropsWhere>();
			  whe.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			  return whe;
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosCapitulos(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosCapitulos();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
	} //class CapitulosController  ends.
}//namespace Controladores.API ends

