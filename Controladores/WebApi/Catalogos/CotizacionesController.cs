using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class CotizacionesController : ApiBaseController
	{
		public CotizacionesController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return _GetCotizacion(id);
		}
		[HttpGet]
		public HttpResponseMessage GetValidaciones(int id)
		{
			try
			{
				NCotizaciones cotizacion = new NCotizaciones();
				if(!cotizacion.IsValidModificaciones(id))
				{
					SetError(cotizacion.GetErrorMessage());
					BuildResponseERROR();
				}
				else
				{
					return _GetCotizacion(id);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpGet]
		public HttpResponseMessage GetDetalleCotizacion(int id)
		{
			try
			{
				ICotizaciones Entidad = _GetDetalleCotizacion(id);				
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CCotizaciones>(NCotizaciones.GetCampoId(), id, FiltrosCotizaciones());
		}
		[HttpPost]
		public HttpResponseMessage IndexAll(int id)
		{
			return base._GetPage<CCotizaciones>(NCotizaciones.GetCampoId(), id, FiltrosCotizacionesSoloEmpresa());
		}
		[HttpPost]
		public HttpResponseMessage New(CCotizaciones Entity)
		{
			Entity.IdUsuario = _IdUsuario();
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CCotizaciones>(NCotizaciones.GetCampoId(), id, FiltrosCotizaciones(where));
		}
		[HttpPost]
		public HttpResponseMessage SearchByEmpresa(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CCotizaciones>(NCotizaciones.GetCampoId(), id, FiltrosCotizacionesSoloEmpresa(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CCotizaciones Entity)
		{
			Entity.IdUsuario = _IdUsuario();
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NCotizaciones.eFields.IdCotizacion.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NCotizaciones>();
		}


		protected virtual HttpResponseMessage _GetCotizacion(int id)
		{
			try
			{
				IEntidad Entidad = null;
				if(_Context != null)
				{
					Entidad = _Context.GetById(NCotizaciones.eFields.IdCotizacion.ToString(), id);
				}
				else
				{
					SetError(No_Context);
				}
				if(Entidad == null)
				{
					SetError(Item_NoFound);
				}
				NCotizaciones coti = new NCotizaciones();
				coti.LoadConceptos(Entidad as ICotizaciones);
				coti.LoadInsumos(Entidad as ICotizaciones);
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected virtual ICotizaciones _GetDetalleCotizacion(int id)
		{
			ICotizaciones Entidad = null;
			try
			{								
				NCotizaciones coti = new NCotizaciones();
				Entidad = coti.GetDetalleCotizacion(id);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return Entidad;
		}
		[HttpPost]
		public HttpResponseMessage Conversion(CCotizaciones cotizacion)
		{
			cotizacion = new NCotizaciones().RealizaConversionMonedas(cotizacion) as CCotizaciones;
			BuildResponse<ICotizaciones>(cotizacion);
			return _Response;
		}
		protected virtual IEnumerable<IPropsWhere> FiltrosCotizacionesSoloEmpresa()
		{
			NCotizaciones d = new NCotizaciones();
			IPropsWhere p = d.FiltrosEmpresa(base._EmpresaId());
			List<IPropsWhere> w = new List<IPropsWhere>();
			w.Add(p);
			return w;
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosCotizacionesSoloEmpresa(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosCotizacionesSoloEmpresa();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosCotizaciones()
		{
			NCotizaciones d = new NCotizaciones();
			return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
		}

		protected virtual IEnumerable<CPropsWhere> FiltrosCotizaciones(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosCotizaciones();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
	} //class CotizacionesController  ends.
}//namespace Controladores.API ends