using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class SisLogErroresController  : ApiBaseController
	{
		public SisLogErroresController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NSisLogErrores.eFields.Log_Error_Id.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CSisLogErrores>(NSisLogErrores.eFields.Log_Error_Id.ToString(), id);
      }        
		[HttpPost]
		public HttpResponseMessage New(CSisLogErrores Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage Search(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return this._Search<CSisLogErrores>(NSisLogErrores.eFields.Log_Error_Id.ToString(), id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CSisLogErrores Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NSisLogErrores.eFields.Log_Error_Id.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NSisLogErrores>();
        }
	} //class SisLogErroresController  ends.--
}//namespace Controladores.API ends

