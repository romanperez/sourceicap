using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class PermisosController : ApiBaseController
	{
		public PermisosController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
        [HttpGet]
        [ActionName("GetPerfilPermisos")]
        public HttpResponseMessage GetPerfilPermisos(int id)
        {
            return _GetPerfilPermisos(id);
        }
		[HttpPut]		
		public HttpResponseMessage Put(int id,CPermisosJs permisos)
		{			
			return base._Put(permisos.IdPerfil, permisos);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NPermisos>();
		}
        protected virtual HttpResponseMessage _GetPerfilPermisos(int id)
        {
            try
            {
                CPermisosJs permisos = new CPermisosJs();
                NPermisos npermisos = new NPermisos();                
                permisos = npermisos.GetPermisos(id);             
                if (permisos == null)
                {
                    SetError(Item_NoFound);
                }
                BuildResponse<CPermisosJs>(permisos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
	} //class UsuariosController  ends.
}//namespace Controladores.API ends

