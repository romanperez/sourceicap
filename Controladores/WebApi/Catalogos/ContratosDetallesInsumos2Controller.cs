using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class ContratosDetallesInsumos2Controller  : ApiBaseController
	{
		public ContratosDetallesInsumos2Controller ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NContratosDetallesInsumos2.eFields.IdInsumoContrato.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CContratosDetallesInsumos2>(NContratosDetallesInsumos2.eFields.IdInsumoContrato.ToString(), id);
      }        
		[HttpPost]
		public HttpResponseMessage New(CContratosDetallesInsumos2 Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CContratosDetallesInsumos2>(NContratosDetallesInsumos2.eFields.IdInsumoContrato.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CContratosDetallesInsumos2 Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NContratosDetallesInsumos2.eFields.IdInsumoContrato.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NContratosDetallesInsumos2>();
        }
	} //class ContratosDetallesInsumos2Controller  ends.
}//namespace Controladores.API ends

