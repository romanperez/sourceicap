using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class UsuariosController  : ApiBaseController
	{
		public UsuariosController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NUsuarios.eFields.IdUsuario.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CUsuarios>(NUsuarios.eFields.IdUsuario.ToString(), id,FiltrosUsuarios());			
      }        
		[HttpPost]
		public HttpResponseMessage New(CUsuarios Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CUsuarios>(NUsuarios.eFields.IdUsuario.ToString(), id, FiltrosUsuarios(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FiltrosUsuarios(where));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CUsuarios Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NUsuarios.eFields.IdUsuario.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NUsuarios>();
        }
		  protected virtual IEnumerable<CPropsWhere> FiltrosUsuarios()
		  {
			  NUsuarios d = new NUsuarios();
			  List<CPropsWhere> where = new List<CPropsWhere>();
			  IPropsWhere where1= d.FiltrosEmpresa(base._EmpresaId());
			  where.Add(where1 as CPropsWhere);
			  return where;
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosUsuarios(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosUsuarios();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
	} //class UsuariosController  ends.
}//namespace Controladores.API ends

