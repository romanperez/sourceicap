using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class ContratosDetalles2Controller : ApiBaseController
	{
		public ContratosDetalles2Controller()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NContratosDetalles2.eFields.IdContratosDetalles.ToString(), id);
		}
		[HttpGet]
		public HttpResponseMessage GetDetalleContrato(int id)
		{
			try
			{
				IContratos2 Entidad = _GetDetalleContrato(id);
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpGet]		
		public HttpResponseMessage GetEstimacionesPorContrato(int id)
		{
			try
			{
				IContratos2 Entidad = _EstimacionesPorContrato(id);
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CContratosDetalles2>(NContratosDetalles2.eFields.IdContratosDetalles.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage New(CContratosDetalles2 Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CContratosDetalles2>(NContratosDetalles2.eFields.IdContratosDetalles.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CContratosDetalles2 Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NContratosDetalles2.eFields.IdContratosDetalles.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NContratosDetalles2>();
		}
		protected virtual IContratos2 _GetDetalleContrato(int id)
		{
			IContratos2 Entidad = null;
			try
			{
				NContratos contrato = new NContratos();
				Entidad = contrato.GetDetalleContrato(id);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return Entidad;
		}
		protected virtual IContratos2 _EstimacionesPorContrato(int id)
		{
			IContratos2 Entidad = null;
			try
			{
				NContratos contrato = new NContratos();
				Entidad = contrato.EstimacionesPorContrato(id, null,null);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return Entidad;
		}
	} //class ContratosDetalles2Controller  ends.
}//namespace Controladores.API ends

