using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class OrdenesTrabajoController : ApiBaseController
	{
		public OrdenesTrabajoController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return this._GetPage<COrdenesTrabajo>(NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString(), id, FiltrosOT());
		}
		[HttpPost]
		public HttpResponseMessage New(COrdenesTrabajo Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<COrdenesTrabajo>(NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString(), id,FiltrosOT( where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FiltrosOT( where));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPost]
		public HttpResponseMessage ActividadesByContrato(CParamRptOTActividad Entity)
		{
			try
			{
				NOrdenesTrabajo ordenes = new NOrdenesTrabajo();
				IEnumerable<IOrdenesTrabajo> resultado = ordenes.GetActividadesByContrato(Entity.IdContrato, Entity.FechaInicial, Entity.FechaFinal);
				if(ordenes.HasError())
				{
					SetError(ordenes.GetErrorMessage());
				}
				BuildResponse<IEnumerable<IOrdenesTrabajo>>(resultado);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPut]		
		public HttpResponseMessage Put(int id, COrdenesTrabajo Entity)		
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NOrdenesTrabajo>();
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosOT()
		{
			List<CPropsWhere> w = new List<CPropsWhere>();
			NOrdenesTrabajo d = new NOrdenesTrabajo();
			CPropsWhere pw= d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere;
			w.Add(pw);
			return w;
			//return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosOT(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosOT();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
		/*protected override HttpResponseMessage _GetPage<Entity>(string key, int pagina, IEnumerable<IPropsWhere> where)
		{
			try
			{
				IPagina<Entity> _pagina = null;
				NOrdenesTrabajo ots = new NOrdenesTrabajo();
				if(where != null)
				{
					ots.AddWhere(where);
				}
				_pagina = ots.BuscarIndex<Entity>(pagina);
				if(_pagina == null)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IPagina<Entity>>(_pagina);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}*/
		/*public override HttpResponseMessage PostFile(DFile archivo)
		{
			return base.PostFile(archivo);
		}*/
	} //class OrdenesTrabajoController  ends.
}//namespace Controladores.API ends

