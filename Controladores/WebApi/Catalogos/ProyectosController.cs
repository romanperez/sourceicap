using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
    public class ProyectosController : ApiBaseController
    {
        public ProyectosController()
        {
            BuildContext();
        }
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return base._Get();
        }
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            return base._Get(NProyectos.eFields.IdProyecto.ToString(), id);
        }
        [HttpGet]
        public HttpResponseMessage GetContratos(int id)
        {
            try
            {
                NContratos contrato = new NContratos();
                IEnumerable<IvwContratos2> resultado = contrato.GetContratos(id);
                BuildResponse<IEnumerable<IvwContratos2>>(resultado);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
        [HttpGet]
        public HttpResponseMessage GetOrdenesTrabajo(int id)
        {
            try
            {
                NOrdenesTrabajo ordenes = new NOrdenesTrabajo();
                IEnumerable<IOrdenesTrabajo> resultado = ordenes.GetOrdenesByProyecto(id);
                BuildResponse<IEnumerable<IOrdenesTrabajo>>(resultado);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage Index(int id)
        {
            return _GetPage<CProyectos>(NProyectos.eFields.IdProyecto.ToString(), id, FiltrosProyectos());
        }
        [HttpPost]
        public HttpResponseMessage IndexAll(int id)
        {
            return _GetPage<CProyectos>(NProyectos.eFields.IdProyecto.ToString(), id, FiltrosSoloEmpresa());
        }
        [HttpPost]
        public HttpResponseMessage New(CProyectos Entity)
        {
            return base._New(Entity);
        }
        [HttpPost]
        public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
        {
            return base._Search(where);
        }
        [HttpPost]
        public HttpResponseMessage SearchByCotizacion(IEnumerable<CPropsWhere> where)
        {
            return this.MySearch(where);
        }
        [HttpPost]
        public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
        {
            return this._Search<CProyectos>(NProyectos.eFields.IdProyecto.ToString(), id, FiltrosProyectos(where));
        }
        [HttpPost]
        public HttpResponseMessage SearchByEmpresa(int id, IEnumerable<CPropsWhere> where)
        {
            return this._Search<CProyectos>(NProyectos.eFields.IdProyecto.ToString(), id, FiltrosSoloEmpresa(where));
        }
        [HttpPost]
        public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
        {
            return base._TopList(FiltrosProyectos(where));
        }
        [HttpPost]
        public HttpResponseMessage TopAll(IEnumerable<CPropsWhere> where)
        {
            return base._TopList(FiltrosSoloEmpresa(where));
        }
        [HttpPost]
        public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
        {
            return base._TopList(id, where);
        }
        [HttpPost]
        public HttpResponseMessage SetCotizacionProyecto(CProyectos Entidad)
        {
            try
            {
                NProyectos proyectos = new NProyectos();
                proyectos.AsignaCotizacion(Entidad);
                if (proyectos.HasError())
                {
                    SetError(proyectos.GetErrorMessage());
                }
                BuildResponse<IEntidad>(Entidad);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage PedidosByProyecto(int id)
        {
            try
            {
                NPedidos p = new NPedidos();
                IEnumerable<IPedidos> res = p.PedidosByProyecto(id);
                if (p.HasError())
                {
                    SetError(p.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IPedidos>>(res);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage SolicitudTraspasoByProyecto(int id)
        {
            try
            {
                NPedidos p = new NPedidos();
                IEnumerable<IPedidos> res = p.PedidosByProyecto(id);
                if (p.HasError())
                {
                    SetError(p.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IPedidos>>(res);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage MovimientosByProyecto(int id)
        {
            try
            {
                NMovimientos m = new NMovimientos();
                IEnumerable<IMovimientos> res = m.MovimientosByProyecto(id);
                if (m.HasError())
                {
                    SetError(m.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IMovimientos>>(res);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage RecibeInsumosByProyecto(CModelTraspasos Entity)
        {
            try
            {
                NPedidos pedido = new NPedidos();
                pedido.RecibeInsumosByProyecto(Entity, base._IdUsuario());
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<CModelTraspasos>(Entity);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage InsumosSinPedido(int id)
        {
            try
            {
                CProyectos proyecto = new CProyectos();
                NProyectosContratos pc = new NProyectosContratos();
                bool envia = pc.ObtieneInsumosSinPedido(id);

                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                if (envia)
                {
                    proyecto.IdProyecto = id;
                }
                BuildResponse<CProyectos>(proyecto);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage DetalleSolicitudInsumos(int id)
        {
            try
            {
                NPedidos pc = new NPedidos();
                IEnumerable<IPedidosDetalles> detalle = pc.PedidosByProyectoMail(new CProyectos()
                {
                    IdProyecto = id
                });
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IPedidosDetalles>>(detalle);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage CantPedidoByProyecto(CPedidosDetalles entity)
        {
            try
            {
                NPedidos pc = new NPedidos();
                IEnumerable<IPedidosDetalles> detalle = pc.ExistenciasVSPedidoByProyecto(entity.IdPedido /*Actua Como IdProyecto*/,
                                                                                          entity.IdPedidoDetalle /*Actua como IdAlmacen*/);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IPedidosDetalles>>(detalle);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpGet]
        public HttpResponseMessage GetInsumosByProyecto(int id)
        {
            try
            {
                NProyectos pc = new NProyectos();
                IEnumerable<IvwContratos2> contratos = pc.InsumosByProyecto(id);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IvwContratos2>>(contratos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpGet]
        public HttpResponseMessage GetInsumosByProyectoConcepto(int id)
        {
            try
            {
                NProyectos pc = new NProyectos();
                IEnumerable<IvwContratos2> contratos = pc.InsumosByProyectoConceptos(id);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IvwContratos2>>(contratos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpGet]
        public HttpResponseMessage GetInsumosByProyectoInsumos(int id)
        {
            try
            {
                NProyectos pc = new NProyectos();
                IEnumerable<IvwContratos2> contratos = pc.InsumosByProyectoInsumos(id);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IvwContratos2>>(contratos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
		  [HttpPost]
		  public HttpResponseMessage SolicitudInsumosInstaladoFechaFiltro(CProyectos Entity)
		  {
			  try
			  {
				  NProyectos pc = new NProyectos();
				  IEnumerable<IContratosDetalles2> contratos = pc.SolicitudInsumosInstaladoFechaFiltro(Entity);
				  if(pc.HasError())
				  {
					  SetError(pc.GetErrorMessage());
				  }
				  BuildResponse<IEnumerable<IContratosDetalles2>>(contratos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return _Response;
		  }
        [HttpPost]
        public HttpResponseMessage InstaladoFechaConceptos(CProyectos Entity)
        {
            try
            {
                NProyectos pc = new NProyectos();
                IEnumerable<IContratosDetalles2> contratos = pc.InstaladoFechaConceptos(Entity);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(contratos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage InstaladoFechaInsumos(CProyectos Entity)
        {
            try
            {
                NProyectos pc = new NProyectos();
                IEnumerable<IContratosDetallesInsumos2> contratos = pc.InstaladoFechaInsumos(Entity);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetallesInsumos2>>(contratos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage ConsultaInstaladoSolicitado(CFiltrosInstaladoSolicitado entity)
        {
            try
            {
                NProyectos pc = new NProyectos();
                IEnumerable<IProyectos> proyectos = pc.ConsultaInstaladoSolicitado(entity);
                if (pc.HasError())
                {
                    SetError(pc.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IProyectos>>(proyectos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPut]
        public HttpResponseMessage Put(int id, CProyectos Entity)
        {
            return base._Put(id, Entity);
        }
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            return base._Delete(NProyectos.eFields.IdProyecto.ToString(), id);
        }
        protected override void BuildContext()
        {
            _Context = new NContext<NProyectos>();
        }
        protected HttpResponseMessage MySearch(IEnumerable<IPropsWhere> where)
        {
            try
            {
                NProyectos proyectos = new NProyectos();
                proyectos.AddWhere(where);
                IEnumerable<IEntidad> res = proyectos.Buscar();
                BuildResponse<IEnumerable<IEntidad>>(res);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        public override HttpResponseMessage EnviaEmail(CEmail Entidad)
        {
            IParametros destinatarios = NParametros.GetByNombre("SOL_INS_DESTINATARIO");
            Entidad.To = destinatarios.Valor.ToString();
            return base.EnviaEmail(Entidad);
        }
        [HttpPost]
        public HttpResponseMessage ConvierteImportes(MProyectosCostos Modelo)
        {
            try
            {
                IEnumerable<IProyectos> proyectosCostos = null;
                bool ProyectoCostosGlobal = _HasPermiso("ProyectoCostosGlobal", _IdPerfil(), _IdMenu());
                if (ProyectoCostosGlobal)
                {
                    NProyectos proyectos = new NProyectos();
                    proyectosCostos = proyectos.ConvierteImportes(Modelo, ProyectoCostosGlobal);
                }
                BuildResponse<IEnumerable<IProyectos>>(proyectosCostos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage ReporteCostos(MProyectosCostos Modelo)
        {
            try
            {
                BuildResponse<IEnumerable<IProyectos>>(GeneraReporteCostos(Modelo));
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        protected IEnumerable<IProyectos> GeneraReporteCostos(MProyectosCostos Modelo)
        {
            IEnumerable<IProyectos> proyectosCostos = null;
            bool ProyectoCostosGlobal = _HasPermiso("ProyectoCostosGlobal", _IdPerfil(), _IdMenu());
            if (ProyectoCostosGlobal)
            {
                NProyectos proyectos = new NProyectos();
                proyectosCostos = proyectos.GetReporteCostos(Modelo, ProyectoCostosGlobal);
            }
            return proyectosCostos;
        }

        protected virtual IEnumerable<IPropsWhere> FiltrosSoloEmpresa()
        {
            NProyectos d = new NProyectos();
            IPropsWhere p = d.FiltrosEmpresa(base._EmpresaId());
            List<IPropsWhere> w = new List<IPropsWhere>();
            w.Add(p);
            return w;
        }
        protected virtual IEnumerable<CPropsWhere> FiltrosSoloEmpresa(IEnumerable<CPropsWhere> where1)
        {
            IEnumerable<IPropsWhere> where2 = this.FiltrosSoloEmpresa();
            return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
        }
        protected virtual IEnumerable<CPropsWhere> FiltrosProyectos()
        {
            NDepartamentos d = new NDepartamentos();
            return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
        }
        protected virtual IEnumerable<CPropsWhere> FiltrosProyectos(IEnumerable<CPropsWhere> where1)
        {
            IEnumerable<IPropsWhere> where2 = this.FiltrosProyectos();
            return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
        }
        protected override HttpResponseMessage _GetPage<Entity>(string key, int pagina, IEnumerable<IPropsWhere> where)
        {
            try
            {
                bool ProyectoCostosGlobal = false;
                ProyectoCostosGlobal = _HasPermiso("ProyectoCostosGlobal", _IdPerfil(), _IdMenu());
                IPagina<Entity> _pagina = null;
                NProyectos allProyectos = new NProyectos();
                if (where != null)
                {
                    allProyectos.AddWhere(where);
                }
                _pagina = allProyectos.BuscarIndex<Entity>(pagina, ProyectoCostosGlobal);
                if (_pagina == null)
                {
                    SetError(Item_NoFound);
                }
                BuildResponse<IPagina<Entity>>(_pagina);

            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        protected override HttpResponseMessage _Search<Entity>(string key, int pagina, IEnumerable<CPropsWhere> where)
        {
            return _GetPage<Entity>(key, pagina, where);
        }

        [HttpPost]
        public HttpResponseMessage ConceptosPorContratos(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosPorContratos(Entity.IdContrato, Entity.Contrato);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }
        [HttpPost]
        public HttpResponseMessage ConceptosPorContratosInstalado(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosPorContratosInstalado(Entity.IdContrato, Entity.Contrato);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage ConceptosPorContratosSolicitadoInstalado(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosPorContratosSolicitadoInstalado(Entity.IdContrato, Entity.Contrato, Entity.Fecha, Entity.FechaFinal);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage ConceptosPorContratosSolicitadoEstado(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosPorContratosSolicitadoEstado(Entity.IdContrato, Entity.Contrato);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage ConceptosPorContratosSolicitadoArea(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosPorContratosSolicitadoArea(Entity.IdContrato, Entity.Contrato);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage ConceptosPorContratosInstaladoArea(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosPorContratosInstaladoArea(Entity.IdContrato, Entity.Contrato);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage ConceptosSolicitadoEstadoArea(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosSolicitadoEstadoArea(Entity.IdContrato, Entity.Contrato);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

        [HttpPost]
        public HttpResponseMessage ConceptosSolicitadoInstaladoArea(CContratosDetalles2 Entity)
        {
            try
            {
                NProyectos pedido = new NProyectos();
                IEnumerable<IContratosDetalles2> lista = pedido.ConceptosSolicitadoInstaladoArea(Entity.IdContrato, Entity.Contrato, Entity.Fecha, Entity.FechaFinal);
                if (pedido.HasError())
                {
                    SetError(pedido.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IContratosDetalles2>>(lista);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return _Response;
        }

    } //class ProyectosController  ends.
}//namespace Controladores.API ends

