using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class OTConceptosController : ApiBaseController
	{
		public OTConceptosController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NOTConceptos.eFields.IdOTConcepto.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<COTConceptos>(NOTConceptos.eFields.IdOTConcepto.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage New(COTConceptos Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<COTConceptos>(NOTConceptos.eFields.IdOTConcepto.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, COTConceptos Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NOTConceptos.eFields.IdOTConcepto.ToString(), id);
		}		
		[HttpGet]
		public HttpResponseMessage GetEjecucionesByConcepto(int id)
		{
			try
			{
				IEntidad Entidad = null;
				if(_Context != null)
				{
					Entidad = _Context.GetById(NOTConceptos.eFields.IdOTConcepto.ToString(), id);
					if(Entidad != null)
					{
						((IOTConceptos)Entidad).Ejecuciones = NOTConceptosEjecucion.GetEjecucionesByIdOTConcepto(Entidad as IOTConceptos);
					}
				}
				else
				{
					SetError(No_Context);
				}
				if(Entidad == null)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NOTConceptos>();
		}
	} //class OTConceptosController  ends.
}//namespace Controladores.API ends

