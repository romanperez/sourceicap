﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
namespace Controladores.API
{
	public class MenuController  : ApiBaseController
	{
		[HttpPost]
		public IEnumerable<IMenus> MenuByPerfil(CPerfiles perfil)
		{
			IEnumerable<IMenus> menus = new List<IMenus>();
			try
			{
				NMenus negocio = BuildNegocio() as NMenus;
				menus = negocio.GetAllMenus(perfil);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return menus;
		}
		protected override INegocio BuildNegocio()
		{
			return new NMenus();
		}
		protected override INegocio BuildNegocio(IEntidad Entity)
		{
			return new NMenus(Entity);
		}
	}
}
