using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class IvaValoresController  : ApiBaseController
	{
		public IvaValoresController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NIvaValores.eFields.IdIva.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CIvaValores>(NIvaValores.eFields.IdIva.ToString(), id, FiltrosIva());
      }        
		[HttpPost]
		public HttpResponseMessage New(CIvaValores Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CIvaValores>(NIvaValores.eFields.IdIva.ToString(), id,FiltrosIva( where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NIvaValores d = new NIvaValores();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NIvaValores.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosIva(whereActivos));				
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CIvaValores Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NIvaValores.eFields.IdIva.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NIvaValores>();
        }
		  protected virtual IEnumerable<IPropsWhere> FiltrosIva()
		  {
			  NIvaValores d = new NIvaValores();
			  List<CPropsWhere> whe = new List<CPropsWhere>();
			  whe.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			  return whe;
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosIva(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosIva();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
		  [HttpPost]
		  public HttpResponseMessage IvaByFecha(CIvaValores Entity)
		  {			 
			  try
			  {
				  NIvaValores niva = new NIvaValores();
				  Entity = niva.GetIvaByFecha(Entity) as CIvaValores;
				  if(niva.HasError())
				  {
					  SetError(niva.GetErrorMessage());
				  }
				  if(Entity == null)
				  {
					  SetError(TraduceTexto("NCotizacionesNoIvaValido"), true);
				  }
				  BuildResponse<IIvaValores>(Entity);
			  }
			  catch(Exception eInterno)
			  {
				  OnError(eInterno);
			  }
			  return _Response;
		  }		 
	} //class IvaValoresController  ends.
}//namespace Controladores.API ends

