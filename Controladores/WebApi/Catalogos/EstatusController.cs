using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class EstatusController  : ApiBaseController
	{
		public EstatusController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NEstatus.eFields.IdEstatus.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CEstatus>(NEstatus.eFields.IdEstatus.ToString(), id,FiltrosEstatus());
      }        
		[HttpPost]
		public HttpResponseMessage New(CEstatus Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CEstatus>(NEstatus.eFields.IdEstatus.ToString(), id,FiltrosEstatus( where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NEstatus d = new NEstatus();
			IEnumerable<CPropsWhere> whereActivos = d.FiltroActivos(NEstatus.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosEstatus(whereActivos));									
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CEstatus Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NEstatus.eFields.IdEstatus.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage OTTipo()
		{
			try
			{
				IEnumerable<IEstatus> res = null;
				NEstatus estatus = new NEstatus();
				res = estatus.OTTipo(base._EmpresaId());
				BuildResponse<IEnumerable<IEstatus>>(res);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage OTEstado()
		{
			try
			{
				IEnumerable<IEstatus> res = null;
				NEstatus estatus = new NEstatus();				
				res = estatus.OTEstado(base._EmpresaId());
				BuildResponse<IEnumerable<IEstatus>>(res);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage ProyectoEstado()
		{
			try
			{
				IEnumerable<IEstatus> res = null;
				NEstatus estatus = new NEstatus();
				res = estatus.ProyectoEstado(base._EmpresaId());
				BuildResponse<IEnumerable<IEstatus>>(res);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage OTPrioridad()
		{
			try
			{
				IEnumerable<IEstatus> res = null;
				NEstatus estatus = new NEstatus();				
				res = estatus.OTPrioridad(base._EmpresaId());
				BuildResponse<IEnumerable<IEstatus>>(res);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}



        protected override void BuildContext()
        {
            _Context = new NContext<NEstatus>();
        }
		  protected virtual IEnumerable<CPropsWhere> FiltrosEstatus()
		  {
			  NEstatus d = new NEstatus();
			  List<CPropsWhere> where = new List<CPropsWhere>();
			  where.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			  return where;
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosEstatus(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosEstatus();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
	} //class EstatusController  ends.
}//namespace Controladores.API ends

