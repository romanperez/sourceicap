using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class Contratos2Controller : ApiBaseController
	{
		public Contratos2Controller()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NContratos.eFields.IdContrato.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CContratos2>(NContratos.eFields.IdContrato.ToString(), id, FiltrosContratos());			
		}
		[HttpPost]
		public HttpResponseMessage New(CContratos2 Entity)
		{
			Entity.IdUsuario = _IdUsuario();
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CContratos2>(NContratos.eFields.IdContrato.ToString(), id, FiltrosContratos(where));			
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FiltrosContratos(where));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CContratos2 Entity)
		{
			Entity.IdUsuario = _IdUsuario();
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NContratos.eFields.IdContrato.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NContratos>();
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosContratos()
		{
			NDepartamentos d = new NDepartamentos();
			return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosContratos(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosContratos();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
	} //class Contratos2Controller  ends.
}//namespace Controladores.API ends

