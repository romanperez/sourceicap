using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class IngenieriasController  : ApiBaseController
	{
		public IngenieriasController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return base._Get(NIngenierias.eFields.IdIngenieria.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CIngenierias>(NIngenierias.eFields.IdIngenieria.ToString(), id, FiltrosIngenierias());
      }        
		[HttpPost]
		public HttpResponseMessage New(CIngenierias Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CIngenierias>(NIngenierias.eFields.IdIngenieria.ToString(), id, FiltrosIngenierias(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(FiltrosIngenierias(where));
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}		
		[HttpPut]
		public HttpResponseMessage Put(int id, CIngenierias Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NIngenierias.eFields.IdIngenieria.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NIngenierias>();
        }
		  protected virtual IEnumerable<CPropsWhere> FiltrosIngenierias()
		  {
			  NDepartamentos d = new NDepartamentos();
			  return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId()).Select(x => x as CPropsWhere);
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosIngenierias(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosIngenierias();
			  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		  }
		 
	} //class IngenieriasController  ends.
}//namespace Controladores.API ends

