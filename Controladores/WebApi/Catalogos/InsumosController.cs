using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class InsumosController : ApiBaseController
	{
		
		public InsumosController()
		{
			BuildContext();		
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NInsumos.eFields.IdInsumo.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CInsumos>(NInsumos.eFields.IdInsumo.ToString(), id, FiltrosInsumos());
		}
		[HttpPost]
		public HttpResponseMessage New(CInsumos Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CInsumos>(NInsumos.eFields.IdInsumo.ToString(), id, FiltrosInsumos(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{			
			NInsumos insumo = new NInsumos();
			IEnumerable<CPropsWhere> whereActivos = insumo.FiltroSoloInsumosActivos(where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosInsumos(whereActivos));		
		}
		[HttpPost]
		public HttpResponseMessage TopInsumosServicios(IEnumerable<CPropsWhere> where)
		{
			NInsumos insumo = new NInsumos();
			IEnumerable<CPropsWhere> whereActivos = insumo.FiltroSoloInsumosServiciosActivos(where).Select(x => x as CPropsWhere);
			return base._TopList(FiltrosInsumos(whereActivos), PersonalizaInsumosServicios);		
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id,FiltrosInsumos( where));
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CInsumos Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NInsumos.eFields.IdInsumo.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage NewXls(List<CInsumos> Entity)
		{
			try
			{
				int idEmpresa = base._EmpresaId();
				NInsumos save = new NInsumos();
				IEnumerable<IInsumos> Result = save.SaveMasivo2(Entity, idEmpresa);
				if(save.HasError())
				{
					SetError(save.GetErrorMessage());
				}
				BuildResponse<IEnumerable<IInsumos>>(Result);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return _Response;
		}
       
		protected override void BuildContext()
		{
			_Context = new NContext<NInsumos>();
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosInsumos()
		{
			NInsumos d = new NInsumos();
			List<CPropsWhere> where = new List<CPropsWhere>();
			where.Add(d.FiltrosEmpresa(base._EmpresaId()) as CPropsWhere);
			return where;
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosInsumos(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosInsumos();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
        public override IExcel BuildExcelExport()
        {
            NExcel<CInsumos> excel = new NExcel<CInsumos>();           
            excel.InterfaceName = "IInsumos";
            excel.NameSheet = "Insumos";
				excel.FileName = "Insumos.xlsx";
            return excel;
        }
		  protected override IEnumerable<CPropsWhere> FiltraBusqueda(IEnumerable<CPropsWhere> where)
		  {
			  if(where != null && where.Count() > 0)
			  {
				  return FiltrosInsumos(where);
			  }
			  else
			  {
				  return FiltrosInsumos();
			  }
		  }
		protected IEnumerable<IEntidad> PersonalizaInsumosServicios(IEnumerable<IEntidad> result)
		{
			NInsumos negocio = new NInsumos();
			negocio.GetCostoInsumos(result.Select(x=>x as IInsumos));			
			return from tbl in result
					/* where ((IInsumos)tbl).Costo>0*/
					 select new CInsumos()
					 {
						Codigo = (((IInsumos)tbl).EsServicio) ? String.Format("[{0}] {0}",base.TraduceTexto("IInsumoAutoEsServicio") ,((IInsumos)tbl).Codigo) :
																			  String.Format("[{0}] {1}", base.TraduceTexto("IInsumoAutoEsInsumo"), ((IInsumos)tbl).Codigo),
						Descripcion = ((IInsumos)tbl).Descripcion,
						Empresa = ((IInsumos)tbl).Empresa,
						EsServicio =((IInsumos)tbl).EsServicio,
						Estatus=((IInsumos)tbl).Estatus,
						Familia =((IInsumos)tbl).Familia,
						IdEmpresa=((IInsumos)tbl).IdEmpresa,
						IdFamilia=((IInsumos)tbl).IdFamilia,
						IdInsumo =((IInsumos)tbl).IdInsumo,
						IdMoneda =((IInsumos)tbl).IdMoneda,
						IdUnidad =((IInsumos)tbl).IdUnidad,
						Moneda =((IInsumos)tbl).Moneda,
						CodigoMoneda=((IInsumos)tbl).CodigoMoneda,
						Serie=((IInsumos)tbl).Serie,
						Unidad =((IInsumos)tbl).Unidad,
						Costo = ((IInsumos)tbl).Costo,
						UnidadMedidaDescripcion=((IInsumos)tbl).UnidadMedidaDescripcion
					 };
		}
	} //class InsumosController  ends.
}//namespace Controladores.API ends

