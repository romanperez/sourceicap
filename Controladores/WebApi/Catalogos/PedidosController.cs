using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp =  System.Web.Http  ;
using Controladores.API;
namespace Service_ICAP.Controllers
{    
    public class PedidosController  : ApiBaseController
	{
		 protected const string VER_TODOS_PEDIDOS = "VerTodosPedidos";
		 public PedidosController ()
        {
            BuildContext();            
        }       
        [HttpGet]		
		public HttpResponseMessage Get()
		{
            return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
            return this._Get(NPedidos.eFields.IdPedido.ToString(), id);
		}       
 		[HttpPost]
      public HttpResponseMessage Index(int id)
      {
			return base._GetPage<CPedidos>(NPedidos.GetCampoId(), id, FiltrosPedidos());
      }        
		[HttpPost]
		public HttpResponseMessage New(CPedidos Entity)
		{
            return base._New(Entity);
		}
		[HttpPost]		
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
            return base._Search(where);			
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CPedidos>(NPedidos.GetCampoId(), id,FiltrosPedidos( where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CPedidos Entity)
		{
            return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NPedidos.eFields.IdPedido.ToString(), id);
		}		
        protected override void BuildContext()
        {
            _Context = new NContext<NPedidos>();
        }
		  protected virtual IEnumerable<CPropsWhere> FiltrosPedidos()
		  {
			  if(base._HasPermiso(PedidosController.VER_TODOS_PEDIDOS,_IdPerfil(), _IdMenu()))
			  {
				  return null;
			  }
			  else
			  {
				  NPedidos d = new NPedidos();
				  List<CPropsWhere> where = new List<CPropsWhere>();
				  where.Add(d.FiltrosUnidad(base._UnidadId()) as CPropsWhere);
				  return where;
			  }
		  }
		  protected virtual IEnumerable<CPropsWhere> FiltrosPedidos(IEnumerable<CPropsWhere> where1)
		  {
			  IEnumerable<IPropsWhere> where2 = this.FiltrosPedidos();
			  if(where2 == null)
			  {
				  return where1;
			  }
			  else
			  {
				  return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
			  }
			  
		  }
		  protected override HttpResponseMessage _Get(string key, int id)
		  {
			  try
			  {
				  IEntidad Entidad = null;
				  NPedidosDetalles detalle = new NPedidosDetalles();
				  if(_Context != null)
				  {
					  Entidad = _Context.GetById(key, id);
				  }
				  else
				  {
					  SetError(No_Context);
				  }
				  if(Entidad == null)
				  {
					  SetError(Item_NoFound);
				  }
				  else
				  {
					  ((IPedidos)Entidad).Detalle = detalle.GetDetalleByPedido(Entidad as IPedidos);
					  if(((IPedidos)Entidad).Detalle == null)
					  {
						  ((IPedidos)Entidad).Detalle = new List<CPedidosDetalles>();
					  }
				  }				  
				  BuildResponse<IEntidad>(Entidad);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return _Response;
		  }
	} //class PedidosController  ends.
}//namespace Controladores.API ends

