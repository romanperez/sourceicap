using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class UnidadesController : ApiBaseController
	{
		public UnidadesController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NUnidades.eFields.IdUnidad.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id, IEnumerable<CPropsWhere> where)
		{
			return base._GetPage<CUnidades>(NUnidades.eFields.IdUnidad.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage New(CUnidades Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CUnidades>(NUnidades.eFields.IdUnidad.ToString(), id, where);
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			return base._TopList(where);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CUnidades Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NUnidades.eFields.IdUnidad.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NUnidades>();
		}

        [HttpPost]
        public HttpResponseMessage NewXls(List<CUnidades> Entity)
        {
            try
            {
                int idEmpresa = base._EmpresaId();
                NUnidades save = new NUnidades();
                IEnumerable<IUnidades> Result = save.SaveMasivo2(Entity, idEmpresa);
                if (save.HasError())
                {
                    SetError(save.GetErrorMessage());
                }
                BuildResponse<IEnumerable<IUnidades>>(Result);
            }
            catch (Exception err)
            {
                OnError(err);
            }
            return _Response;
        }
	} //class UnidadesController  ends.
}//namespace Controladores.API ends

