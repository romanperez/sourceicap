using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class ConceptosController : ApiBaseController
	{
		protected string campoEmpresa;
		public ConceptosController()
		{
			BuildContext();
			campoEmpresa = NConceptos.eFields.IdEmpresa.ToString();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return this._GetConcepto(id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CConceptos>(NConceptos.eFields.IdConcepto.ToString(), id, Filtros(this.campoEmpresa));
		}
		[HttpPost]
		public HttpResponseMessage New(CConceptos Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CConceptos>(NConceptos.eFields.IdConcepto.ToString(), id, Filtros(this.campoEmpresa, where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NEspecialidades negocio = new NEspecialidades();
			IEnumerable<CPropsWhere> where2 = negocio.FiltroActivos(NConceptos.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(Filtros(this.campoEmpresa, where2));
		}
		
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CConceptos Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NConceptos.eFields.IdConcepto.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage NewXls(List<CConceptos> Entity)
		{
			try
			{
				int idEmpresa = base._EmpresaId();
				NConceptos save = new NConceptos();
				IEnumerable<IConceptos> Result = save.SaveMasivo2(Entity, idEmpresa);
				if(save.HasError())
				{
					SetError(save.GetErrorMessage());
				}
				BuildResponse<IEnumerable<IConceptos>>(Result);
			}
			catch(Exception err)
			{
				OnError(err);
			}
			return _Response;
		}
		[HttpPost]
		public HttpResponseMessage FindByConcepto(CCotizaciones coti)
		{
			try
			{				
				NConceptos buscar = new NConceptos();
				IEnumerable<ICotizacionesDetalles> conceptos2= buscar.FindConceptos(coti.IdEmpresa, coti.IsCopy, coti.Conceptos);
				if(conceptos2 != null)
				{
					coti.Conceptos = conceptos2.Select(c=> c as CCotizacionesDetalles).ToList();
				}
				else
				{
					coti.Conceptos = null;
				}
				if(buscar.HasError())
				{
					SetError(buscar.GetErrorMessage(), true);
				}
				BuildResponse<ICotizaciones>(coti);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;				 
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NConceptos>();
		}
		/// <summary>
		/// Obtiene una entidad en base a la llave primaria.
		/// </summary>		
		/// <param name="id">Valor de la llave primaria.</param>
		/// <returns>Respuesta en formato Http.</returns>
		protected virtual HttpResponseMessage _GetConcepto(int id)
		{
			try
			{
				IEntidad Entidad = null;
				if(_Context != null)
				{
					Entidad = _Context.GetById(NConceptos.eFields.IdConcepto.ToString(), id);
					if(Entidad != null)
					{
						NConceptosInsumos movs = new NConceptosInsumos();
						var lst =movs.ObtieneDetallesById(((IConceptos)Entidad).IdConcepto);
						if(lst != null && lst.Count()>0)
						{
							((IConceptos)Entidad).Insumos = lst.Select(x => x as CConceptosInsumos).ToList();
						}
					
					}
				}
				else
				{
					SetError(No_Context);
				}
				if(Entidad == null)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}		
		protected IEnumerable<IEntidad> TopWithInsumos(IEnumerable<IEntidad> result)
		{			
			return result;
		}
	} //class ConceptosController  ends.
}//namespace Controladores.API ends