using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using Datos;
using System.Net.Http;
using System.Net;
using webhttp = System.Web.Http;
using Controladores.API;
namespace Service_ICAP.Controllers
{
	public class AlmacenesController : ApiBaseController
	{
		public AlmacenesController()
		{
			BuildContext();
		}
		[HttpGet]
		public HttpResponseMessage Get()
		{
			return base._Get();
		}
		[HttpGet]
		public HttpResponseMessage Get(int id)
		{
			return base._Get(NAlmacenes.eFields.IdAlmacen.ToString(), id);
		}
		[HttpPost]
		public HttpResponseMessage Index(int id)
		{
			return base._GetPage<CAlmacenes>(NAlmacenes.eFields.IdAlmacen.ToString(), id, FiltrosAlmacenes());
		}
		[HttpPost]
		public HttpResponseMessage New(CAlmacenes Entity)
		{
			return base._New(Entity);
		}
		[HttpPost]
		public HttpResponseMessage SearchAll(IEnumerable<CPropsWhere> where)
		{
			return base._Search(where);
		}
		[HttpPost]
		public HttpResponseMessage Search(int id, IEnumerable<CPropsWhere> where)
		{
			return base._Search<CAlmacenes>(NAlmacenes.eFields.IdAlmacen.ToString(), id, FiltrosAlmacenes(where));
		}
		[HttpPost]
		public HttpResponseMessage Top(IEnumerable<CPropsWhere> where)
		{
			NAlmacenes alm = new NAlmacenes();
			IEnumerable<CPropsWhere> whereActivos = alm.FiltroActivos(NAlmacenes.eFields.Estatus.ToString(), where).Select(x => x as CPropsWhere);
			return base._TopList(whereActivos);
		}
		[HttpPost]
		public HttpResponseMessage TopCustom(int id, IEnumerable<CPropsWhere> where)
		{
			return base._TopList(id, where);
		}
		[HttpPut]
		public HttpResponseMessage Put(int id, CAlmacenes Entity)
		{
			return base._Put(id, Entity);
		}
		[HttpDelete]
		public HttpResponseMessage Delete(int id)
		{
			return base._Delete(NAlmacenes.eFields.IdAlmacen.ToString(), id);
		}
		protected override void BuildContext()
		{
			_Context = new NContext<NAlmacenes>();
		}
		protected virtual IEnumerable<IPropsWhere> FiltrosAlmacenes()
		{
			NAlmacenes d = new NAlmacenes();
			return d.FiltrosEmpresaUnidad(base._EmpresaId(), base._UnidadId());
		}
		protected virtual IEnumerable<CPropsWhere> FiltrosAlmacenes(IEnumerable<CPropsWhere> where1)
		{
			IEnumerable<IPropsWhere> where2 = this.FiltrosAlmacenes();
			return _Context.Filtros(where1, where2).Select(x => x as CPropsWhere);
		}
	} //class AlmacenesController  ends.
}//namespace Controladores.API ends

