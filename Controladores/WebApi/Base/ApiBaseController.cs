﻿/*
 string uri = Url.Link("DefaultApi", new
				{
					id = 20
				});
				_Response.Headers.Location = new Uri(uri);	
 //[ActionName("Get")]
 * 
 * this.ControllerContext.RouteData.Values["action"].ToString();
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Controladores.MVC;
using Entidades;
using Datos;
using Negocio;
using System.IO;
using System.Net.Http.Headers;

//using System.Web.Mvc.HttpGetAttribute;
//using System.Web.Http.HttpGetAttribute;
namespace Controladores.API
{
	public class ApiBaseController : ApiController
	{
		#region MensajesMultiIdioma
		protected string No_Context;
		protected string Item_Deleted;
		protected string Item_NoFound;
		#endregion
		protected HttpResponseMessage _Response;
		protected IError _ErrorInterno;
		protected IContext _Context;
		protected delegate IEnumerable<IEntidad> FnTopCustom(IEnumerable<IPropsWhere> where);
		protected delegate IEnumerable<IEntidad> PersonalizaLista(IEnumerable<IEntidad> result);
		protected delegate IEnumerable<Entity> BeforeSend<Entity>(IEnumerable<Entity> result) where Entity : IEntidad, new();
		#region ManejoError
		protected virtual void OnError(Exception error)
		{
			_ErrorInterno = new DError();
			try
			{
				_ErrorInterno.SaveLog(error);
			}//try ends.
			catch(Exception eInesperado)
			{
				_ErrorInterno.Error = eInesperado.Message;
			}//catch ends.			
			BuildResponseERROR();
		}
		protected virtual void OnError(String error)
		{
			this.OnError(new Exception(error));
		}
		protected virtual void ErrorInNegocio(INegocio Negocio)
		{
			if(Negocio == null) return;
			if(Negocio.HasError())
			{
				_ErrorInterno = Negocio.GetErrorDetail();
				OnError(_ErrorInterno.Error);
			}
		}
		public virtual void InicializaError()
		{
			if(_ErrorInterno == null) _ErrorInterno = DBase.StaticInicializaError();
		}
		public virtual void SetError(string error)
		{
			InicializaError();
			_ErrorInterno.Error = error;
		}
		public virtual void SetError(string error, bool append)
		{
			InicializaError();
			_ErrorInterno.Error = String.Format("{0} {1}", _ErrorInterno.Error, error);
		}
		public string GetError()
		{
			const string NOERROR = "No Error.";
			if(_ErrorInterno != null)
			{
				if(_ErrorInterno.IdError > 0)
				{
					return _ErrorInterno.IdError.ToString();
				}
				else
				{
					return (!String.IsNullOrEmpty(_ErrorInterno.Error)) ? _ErrorInterno.Error : NOERROR;
				}
			}
			return NOERROR;
		}
		protected bool HasError()
		{
			if(_ErrorInterno != null) return true;
			else return false;
		}
		protected void EvaluaErrorInterno<EntityReponse>(INegocio negocio, EntityReponse response)
		{
			this.ErrorInNegocio(negocio);
			if(!HasError())
			{
				_Response = Request.CreateResponse<EntityReponse>(HttpStatusCode.OK, response);
			}
		}
		protected virtual void BuildResponseERROR()
		{
			_Response = new HttpResponseMessage()
			{
				StatusCode = HttpStatusCode.InternalServerError,// (HttpStatusCode)422, // Unprocessable Entity
				ReasonPhrase = this.GetError()
			};
		}
		protected virtual void BuildResponseOKFile(byte[] file,IExcel ExcelInfo)
		{
			_Response = new HttpResponseMessage(HttpStatusCode.OK);
			//_Response.Content = new StreamContent(new FileStream(file, FileMode.Open, FileAccess.Read));
			_Response.Content = new StreamContent(new MemoryStream(file));
			_Response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
			_Response.Content.Headers.ContentDisposition.FileName = ExcelInfo.FileName;
			_Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/msexcel");
		}
		protected virtual void BuildResponseOK<EntityReponse>(EntityReponse response)
		{
			_Response = Request.CreateResponse<EntityReponse>(HttpStatusCode.OK, response);
			//string uri = Url.Link("DefaultApi", new
			//{
			//	id = 20
			//});
			//_Response.Headers.Location = new Uri(uri);
			_Response.Headers.Add("RespuestaWSICAP", this.TraduceTexto("WebApiResponseOK"));
		}
		//protected void ValidaResponseNoEmpty()
		//{
		//    if(_Response == null)
		//    {
		//        _Response = new HttpResponseMessage()
		//        {
		//            StatusCode = HttpStatusCode.OK,
		//            ReasonPhrase =(_ErrorInterno!= null)? _ErrorInterno.Error : "No se recibieron detalles."
		//        };
		//    }
		//}		
		#endregion

		#region Factory
		protected virtual INegocio BuildNegocio()
		{
			return null;
		}
		protected virtual INegocio BuildNegocio(IEntidad Entity)
		{
			return null;
		}
		#endregion

		#region MultiIdioma
		public override System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Web.Http.Controllers.HttpControllerContext controllerContext, System.Threading.CancellationToken cancellationToken)
		{
			LoadMessagesResx();
			return base.ExecuteAsync(controllerContext, cancellationToken);
		}
		/////////////// 
		/// <summary>
		/// REVISAR EL CAMBIO DE IDIOMA DESDE EL WEB API
		/// </summary>
		protected void LoadMessagesResx()
		{
			try
			{
				No_Context = (Idiomas.IcapWeb.WebApiBaseNoContext != null) ? Idiomas.IcapWeb.WebApiBaseNoContext : "No Context default";
				Item_Deleted = (Idiomas.IcapWeb.WebApiBaseItemDelete != null) ? Idiomas.IcapWeb.WebApiBaseItemDelete : "Eliminado default";
				Item_NoFound = (Idiomas.IcapWeb.WebApiBaseItemNoFound != null) ? Idiomas.IcapWeb.WebApiBaseItemNoFound : "No found default";
			}
			catch(Exception Error)
			{
				OnError(Error);
			}
		}
		/// <summary>
		///  Obtiene el texto en el recurso dependiendo del Idioma seleccionado si no lo encuentra se regresa el texto tal cual fue enviado a buscar.
		/// </summary>
		/// <param name="cadena">Texto a traducir.</param>
		/// <returns>Texto traducido.</returns>
		public string TraduceTexto(string cadena)
		{
			return CIdioma.TraduceCadena(cadena);
		}
		#endregion

		protected virtual void BuildContext()
		{
		}
		protected virtual void BuildResponse<Entity>(Entity objeto)
		{
			if(_Context.HasError())
			{
				_ErrorInterno = _Context.GetError();
			}
			if(HasError())
			{
				BuildResponseERROR();
			}
			else
			{
				BuildResponseOK<Entity>(objeto);
			}
		}
		protected virtual void BuildResponseFile(byte[] excel,IExcel info)
		{
			if(_Context.HasError())
			{
				_ErrorInterno = _Context.GetError();
			}
			if(HasError())
			{
				BuildResponseERROR();
			}
			else
			{
				BuildResponseOKFile(excel, info);
			}
		}
		protected virtual HttpResponseMessage _New(IEnumerable<IEntidad> Entities)
		{
			IEnumerable<IEntidad> resp = null;
			try
			{
				if(_Context != null)
				{
					resp = _Context.New(Entities);
				}
				else
				{
					SetError(No_Context);
				}
				BuildResponse<IEnumerable<IEntidad>>(resp);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected virtual HttpResponseMessage _New(IEntidad Entity)
		{
			try
			{
				if(_Context != null)
				{
					_Context.New(Entity);
				}
				else
				{
					SetError(No_Context);
				}
				BuildResponse<IEntidad>(Entity);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected virtual HttpResponseMessage _Put(int id, IEntidad Entity)
		{
			try
			{
				if(_Context != null)
				{
					_Context.Put(id, Entity);
				}
				else
				{
					SetError(No_Context);
				}
				BuildResponse<IEntidad>(Entity);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected virtual HttpResponseMessage _Delete(string key, int id)
		{
			try
			{
				if(_Context != null)
				{
					_Context.Delete(key, id);
				}
				else
				{
					SetError(No_Context);
				}
				BuildResponse<string>(Item_Deleted);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}

		/// <summary>
		/// Obtiene una entidad en base a la llave primaria.
		/// </summary>
		/// <param name="key">Nombre del campo el cual es la llave primaria.</param>
		/// <param name="id">Valor de la llave primaria.</param>
		/// <returns>Respuesta en formato Http.</returns>
		protected virtual HttpResponseMessage _Get(string key, int id)
		{
			try
			{
				IEntidad Entidad = null;
				if(_Context != null)					
				{
					Entidad = _Context.GetById(key, id);
				}
				else
				{
					SetError(No_Context);
				}
				if(Entidad == null)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IEntidad>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		/// <summary>
		/// Obtiene todo el catalogo.
		/// </summary>
		/// <param name="key">Nombre del campo el cual es la llave primaria.</param>
		/// <param name="id">Valor de la llave primaria.</param>
		/// <returns>Respuesta en formato Http.</returns>
		protected virtual HttpResponseMessage _Get()
		{
			try
			{
				IEnumerable<IEntidad> Entidades = null;
				if(_Context != null)
				{
					Entidades = _Context.Buscar();
				}
				else
				{
					SetError(No_Context);
				}
				if(Entidades == null)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IEnumerable<IEntidad>>(Entidades);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		/// <summary>
		/// Obtienen todo el catalogo  paginado.
		/// </summary>
		/// <typeparam name="Entity">Clase la cual es usada como elemento de la paginacion.</typeparam>
		/// <param name="key">Nombre del campo el cual es la llave primaria.</param>
		/// <param name="id">Valor de la llave primaria.</param>
		/// <returns>Respuesta en formato Http.</returns>
		protected virtual HttpResponseMessage _GetPage<Entity>(string key, int pagina) where Entity : IEntidad, new()
		{
			return _GetPage<Entity>(key, pagina, null);
		}
		protected virtual HttpResponseMessage _GetPage<Entity>(string key,
																				 int pagina,
																				 IEnumerable<IPropsWhere> where) where Entity : IEntidad, new()
		{
			return this._GetPage<Entity>(key, pagina, where, null);
		}
		protected virtual HttpResponseMessage _GetPage<Entity>(string key, 
			                                                    int pagina,  
			                                                    IEnumerable<IPropsWhere> where,
																				 BeforeSend<Entity> onBeforeSend) where Entity : IEntidad, new()
		{
			try
			{
				IPagina<Entity> _pagina = null;
				if(_Context != null)
				{
					if(where != null)
					{
						_Context.Where(where);
					}
					_pagina = _Context.Buscar<Entity>(pagina, key);
				}
				else
				{
					SetError(No_Context);
				}
				if(_pagina == null)
				{
					//SetError(Item_NoFound);
					_pagina = new DPagina<Entity>();
				}
				if(onBeforeSend != null)
				{				
					_pagina.Contenido = onBeforeSend(_pagina.Contenido);										
				}
				BuildResponse<IPagina<Entity>>(_pagina);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
#region ExcelExport
		public virtual IExcel BuildExcelExport()
		{
			return null;
		}
		[HttpPost]
		public HttpResponseMessage Excel(IEnumerable<CPropsWhere> where)
		{
			IExcel excel = this.BuildExcelExport();
			return this._Excel(excel, where);
		}
		protected virtual IEnumerable<IEntidad> FiltraResultados(IEnumerable<IEntidad> Result)
		{
			return Result;
		}
		protected virtual IEnumerable<CPropsWhere> FiltraBusqueda(IEnumerable<CPropsWhere> where)
		{
			return where;
		}
		protected virtual HttpResponseMessage _Excel(IExcel excelInfo, IEnumerable<CPropsWhere> where)
		{
			try
			{
				IEnumerable<IEntidad> Result = null;
				byte[] Excel=null;
				if(_Context != null && excelInfo != null)
				{
					_Context.Where(FiltraBusqueda(where));					
					Result = _Context.Buscar();
					if(Result != null && Result.Count() > 0)
					{
						//Excel = _Context.ExportExcel(excel, Result);
						Excel = excelInfo.MainExcel(FiltraResultados(Result));
					}
				}
				else
				{
					if(excelInfo == null)
					{
						SetError(TraduceTexto("ApiBaseNoDefitionForExcelExport"));
					}
					if(_Context == null)
					{
						SetError(No_Context, true);
					}
				}
				if(Result == null || Result.Count() <= 0)
				{
					SetError(Item_NoFound, true);
				}
				BuildResponseFile(Excel, excelInfo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
#endregion
		protected virtual HttpResponseMessage _Search(IEnumerable<CPropsWhere> where)
		{
			try
			{
				IEnumerable<IEntidad> Result = null;
				if(_Context != null)
				{
					_Context.Where(where);
					Result = _Context.Buscar();
				}
				else
				{
					SetError(No_Context);
				}
				if(Result == null || Result.Count() <= 0)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IEnumerable<IEntidad>>(Result);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;

		}
		protected virtual HttpResponseMessage _Search<Entity>(string key, int pagina, IEnumerable<CPropsWhere> where) where Entity : IEntidad, new()
		{
			try
			{
				IPagina<Entity> _pagina = null;
				if(_Context != null)
				{
					_Context.Where(where);
					_pagina = _Context.Buscar<Entity>(pagina, key);
				}
				else
				{
					SetError(No_Context);
				}
				if(_pagina == null)
				{
					SetError(Item_NoFound);
				}
				BuildResponse<IPagina<Entity>>(_pagina);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected virtual int _GetDefaultTop()
		{
			int iTopDefault = 0;
			Negocio.NContext<Negocio.NParametros> param = new NContext<NParametros>();
			List<IPropsWhere> whereAll = new List<IPropsWhere>();
			CPropsWhere where = new CPropsWhere();
			where.Prioridad = true;
			where.Valor = "TOP_SEARCH_AJAX";
			where.NombreCampo = NParametros.eFields.Nombre.ToString();
			where.Condicion = "=";
			whereAll.Add(where);
			param.Where(whereAll);
			IParametros result = param.BuscarScalar() as IParametros;
			if(result != null)
			{
				int.TryParse(result.Valor.ToString(), out iTopDefault);
				if(iTopDefault <= 0) iTopDefault = 10;
			}
			return iTopDefault;
		}
		/// <summary>
		/// Realiza la busqueda en la tabla pero agrega un top de resultados.(Top por default)
		/// </summary>
		/// <param name="top"></param>
		/// <param name="where">Condiciones de busqueda.</param>
		/// <param name="fnPersonalizaLista">Personaliza la lista.</param>
		/// <returns>Lista de resultados.</returns>
		protected virtual HttpResponseMessage _TopList(IEnumerable<CPropsWhere> where, PersonalizaLista fnPersonalizaLista)
		{
			return _TopList(0, where, fnPersonalizaLista);
		}
		/// <summary>
		/// Realiza la busqueda en la tabla pero agrega un top de resultados.(Top por default)
		/// </summary>
		/// <param name="where">Condiciones de busqueda.</param>
		/// <returns>Lista de resultados.</returns>
		protected virtual HttpResponseMessage _TopList(IEnumerable<CPropsWhere> where)
		{
			return _TopList(0, where, null);
		}
		/// <summary>
		///  Realiza la busqueda en la tabla pero agrega un top de resultados.
		/// </summary>
		/// <param name="top">Top limite de registros a devolver.</param>
		/// <param name="where">Condiciones de busqueda.</param>
		/// <returns>Lista de resultados.</returns>
		protected virtual HttpResponseMessage _TopList(int top, IEnumerable<CPropsWhere> where)
		{
			return _TopList(top, where, null);
		}
		/// <summary>
		/// Realiza la busqueda en la tabla pero agrega un top de resultados.
		/// </summary>
		/// <param name="top">Top limite de registros a devolver.</param>
		/// <param name="where">Condiciones de busqueda.</param>
		///  <param name="fnPersonalizaLista">Personaliza la lista.</param>
		/// <returns>Lista de resultados.</returns>
		protected virtual HttpResponseMessage _TopList(int top, IEnumerable<CPropsWhere> where, PersonalizaLista fnPersonalizaLista)
		{
			try
			{
				IEnumerable<IEntidad> Result = null;
				if(top <= 0)
				{
					top = this._GetDefaultTop();
				}
				if(_Context != null)
				{
					_Context.Where(where);
					Result = _Context.Buscar(top);
					if(fnPersonalizaLista != null)
					{
						Result = fnPersonalizaLista(Result);
					}
				}
				else
				{
					SetError(No_Context);
				}
				BuildResponse<IEnumerable<IEntidad>>(Result);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		protected virtual HttpResponseMessage _TopList(IEnumerable<CPropsWhere> where, FnTopCustom ExeTopCustom)
		{
			try
			{
				IEnumerable<IEntidad> Result = ExeTopCustom(where);													
				BuildResponse<IEnumerable<IEntidad>>(Result);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		///////////////
		protected object _GetValorHeader(string HeaderName)
		{
			try
			{
				if(this.Request != null)
				{
					if(base.Request.Headers != null)
					{
						IEnumerable<string> headerValues = this.Request.Headers.GetValues(HeaderName);
						if(headerValues != null && headerValues.Count() > 0)
						{
							return headerValues.FirstOrDefault();
						}
					}
				}
			}
			catch(Exception)
			{

			}
			return null;
		}
		protected int _GetValorHeaderInt(string HeaderName)
		{
			try
			{
				object var = this._GetValorHeader(HeaderName);
				string valor = (var != null) ? var.ToString() : String.Empty;
				int IdValor = 0;
				int.TryParse(valor, out IdValor);
				return IdValor;
			}
			catch(Exception eIdEmpresa)
			{
				OnError(eIdEmpresa);
			}
			return -1;
		}
		#region CUSTOM_HEADERS
		protected int _UnidadId()
		{
			return _GetValorHeaderInt("userIcap-IdUnidad");
		}
		protected int _EmpresaId()
		{
			return _GetValorHeaderInt("userIcap-IdEmpresa");
		}
		protected int _IdUsuario()
		{
			return _GetValorHeaderInt("userIcap-IdUsuario");
		}
		protected int _IdPerfil()
		{
			return _GetValorHeaderInt("userIcap-IdPerfil");
		}
		protected int _IdMenu()
		{
			return _GetValorHeaderInt("userIcap-IdMenu");
		}
		#endregion
		protected virtual IEnumerable<IPropsWhere> Filtros(string campo)
		{
			return _Context.Filtros(_Context.FiltroEmpresa(this._EmpresaId(), campo));
		}
		protected virtual IEnumerable<CPropsWhere> Filtros(string campo, IEnumerable<CPropsWhere> where)
		{
			return _Context.Filtros(where, Filtros(campo)).Select(x => x as CPropsWhere);
		}


		protected virtual IEnumerable<IPropsWhere> Filtros(string campo, bool omiteConcatenar)
		{
			return _Context.Filtros(_Context.FiltroEmpresa(this._EmpresaId(), "=", campo, omiteConcatenar));
		}
		protected virtual IEnumerable<CPropsWhere> Filtros(string campo, bool omiteConcatenar, IEnumerable<CPropsWhere> where)
		{
			return _Context.Filtros(where, Filtros(campo, omiteConcatenar)).Select(x => x as CPropsWhere);
		}
		/// <summary>
		/// Determina si un perfil tiene determinado permiso
		/// </summary>
		/// <param name="namePermiso">Nombre del permiso.</param>
		/// <param name="perfil">Id del perfil.</param>
		/// <param name="id">Id Menu</param>
		/// <returns></returns>
		protected bool _HasPermiso(string namePermiso,int perfil,int id)
		{
			return _Context.HasPermiso(namePermiso,perfil,id);
		}
		#region MANEJO_ARCHIVOS
		public virtual string GetPathBase()
		{
			string newPath = String.Empty;
			IEnumerable<IParametros> files = NParametros.GetByPrefijo("API_FILES");
			IParametros map = files.Where(x => x.Nombre == "API_FILES_USE_MAP_SERVER").FirstOrDefault();
			IParametros path = files.Where(x => x.Nombre == "API_FILES_ROOT").FirstOrDefault();
			string[] useMap = map.Valor.ToString().Split(',');
			if(useMap[0] == "1")
			{
				newPath = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(useMap[1]), path.Valor.ToString());
			}
			else
			{
				newPath = path.Valor.ToString();
			}
			return newPath;
		}		
		[HttpPost]
		public virtual HttpResponseMessage PostFile(DFile archivo)
		{
			try
			{
				string sBase=GetPathBase();
				/*archivos.ToList().ForEach(x =>
				{
					x.BuildRuta(sBase);
					x.GuardaArchivo(sBase);
					x.Url = System.IO.Path.Combine(x.Ruta, x.Name);
					x.Contenido = null;
				});
				BuildResponse<IEnumerable<DFile>>(archivos);*/
				if(!String.IsNullOrEmpty(archivo.ContenidoString))
				{
					archivo.Contenido = Convert.FromBase64String(archivo.ContenidoString);					
				}				
				archivo.BuildRuta(sBase);
				archivo.GuardaArchivo(sBase);
				archivo.Url = System.IO.Path.Combine(archivo.Ruta, archivo.Name);
				archivo.Contenido = null;
				archivo.ContenidoString = String.Empty;
				BuildResponse<DFile>(archivo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public virtual HttpResponseMessage GetFile(DFile archivo)
		{
			try
			{
				string sBase = GetPathBase();				
				archivo.LeeArchivo(sBase);
				BuildResponse<DFile>(archivo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public virtual HttpResponseMessage DelFile(DFile archivo)
		{
			try
			{
				string sBase = GetPathBase();				
				archivo.EliminaArchivo(sBase);
				BuildResponse<DFile>(archivo);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		[HttpPost]
		public virtual HttpResponseMessage EnviaEmail(CEmail Entidad)
		{
			try
			{
				NProyectos p = new NProyectos();
				Entidad= p.EnviarEmail(Entidad);				
				BuildResponse<CEmail>(Entidad);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return _Response;
		}
		#endregion
	}
}
