﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Entidades;
using System.Net.Http.Headers;
namespace Controladores.API
{
    public class LanguageHandler : DelegatingHandler
    {
        //private ISet<string> supportedCultures = new HashSet<string>() { "en-us", "en", "fr-fr", "fr" };
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpHeaderValueCollection<StringWithQualityHeaderValue> idiomas = request.Headers.AcceptLanguage;
            if (idiomas != null && idiomas.Count > 0)
            {
                IEnumerable<IIdioma> headerValue = CIdioma.GetAllIdiomas().Where(i => i.Codigo.ToUpper() == idiomas.First().Value.ToUpper());
                IIdioma idiomaSelect = (headerValue != null && headerValue.Count()>0)?headerValue.FirstOrDefault() :null;
                if (idiomaSelect != null)
                {
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(idiomaSelect.Codigo);
                }                
            }
            return await base.SendAsync(request, cancellationToken);
        }
    }//class CultureHandler ends.
}//Namesapce Controladores.API ends.