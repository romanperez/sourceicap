﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
namespace Controladores.API
{
	public class LogginController : ApiBaseController
	{
		[HttpPost]
		public IUsuarios InicioSession(CUsuarios loggin)
		{			
			try
			{
				NUsuarios user = BuildNegocio(loggin) as NUsuarios;
				if(loggin == null) loggin = new CUsuarios();
				loggin.InicioSession = String.Empty;
				loggin.Estatus = false;
				IUsuarios auxResp =user.IniciarSesion();
				if(auxResp != null)
				{
					loggin = auxResp as CUsuarios;				
				}				
				if(user.HasError())
				{
					loggin.InicioSession = user.GetErrorMessage();
				}
				else
				{
					loggin.InicioSession = String.Empty;
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return loggin;
		}
		protected override INegocio BuildNegocio()
		{
			return new NUsuarios();
		}
		protected override INegocio BuildNegocio(IEntidad Entity)
		{
			return new NUsuarios(Entity);
		}
	}
}
