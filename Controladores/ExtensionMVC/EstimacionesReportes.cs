﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using RestSharp;
namespace Negocio
{
    public class EstimacionesReportes
	{
		public Controladores.MVC.MvcBase Controlador
		{
			set;
			get;
		}
        public CDescargaReporte ReporteEstimacionesCantidades(string NameFile,
                                                                   ETipoReporte Tipo,
                                                                   IContratos2 parametros,
                                                                   IEstimaciones Modelo)
        {
            CDescargaReporte reporte = null;
            IEnumerable<IEstimaciones> data = null;
            data = Controlador.BuildPeticionVerb<List<CEstimaciones>>(Controlador.GetUrlApiService("ReporteEstimacionesCantidades", "Estimaciones"),
                                                                      Modelo, Method.POST);
            if (data != null)
            {
                Reportes.RptEstimacionesPorContrato rpt = new Reportes.RptEstimacionesPorContrato();
                rpt.UrlBase = Controlador.GetBaseUrlService();
                rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
                reporte = new CDescargaReporte();
                reporte.Id = Guid.NewGuid().ToString();
                reporte.Nombre = String.Format(NameFile);
                Controlador.TempData[reporte.Id] = rpt.GeneraReporteEstimacionesCantidades(Tipo, parametros, data);
            }
            return reporte;
        }	
        public CDescargaReporte ReporteEstimacionesResumenEconomico(string NameFile,                                                               
                                                                    ETipoReporte Tipo,
                                                                    IContratos2 parametros,
                                                                    IEstimacionesResumenEconomico Modelo)
		{
			CDescargaReporte reporte = null;
            IEnumerable<CEstimacionesResumenEconomico> data = null;
            data = Controlador.BuildPeticionVerb<List<CEstimacionesResumenEconomico>>(Controlador.GetUrlApiService("ReporteEstimacionesResumenEconomico", "Estimaciones"), 
                                                                      Modelo, Method.POST);
            if (data != null)
			{
                Reportes.RptEstimacionesPorContrato rpt = new Reportes.RptEstimacionesPorContrato();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
                Controlador.TempData[reporte.Id] = rpt.GeneraReporteEstimacionesResumenEconomico(Tipo, parametros, data);
			}
			return reporte;
		}		
	}
}
