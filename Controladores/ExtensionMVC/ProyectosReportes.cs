﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using RestSharp;
namespace Negocio
{
	public class ProyectosReportes
	{
		public Controladores.MVC.MvcBase Controlador
		{
			set;
			get;
		}
		public CDescargaReporte ReporteConceptosContratos(string NameFile, ETipoReporte Tipo, CContratosDetalles2 Modelo, int tipo)
		{
			CDescargaReporte reporte = null;
			List<CContratosDetalles2> conversion = null;

            string servicio = string.Empty;
            switch (tipo)
            {
                case 1:
                    servicio = "ConceptosPorContratos";
                    break;
                case 2:
                    servicio = "ConceptosPorContratosSolicitadoEstado";
                    break;
                case 3:
                    servicio = "ConceptosPorContratosSolicitadoArea";
                    break;
                case 4:
                    servicio = "ConceptosPorContratosInstaladoArea";
                    break;
                case 5:
                    servicio = "ConceptosSolicitadoEstadoArea";
                    break;
            }

			conversion = Controlador.BuildPeticionVerb<List<CContratosDetalles2>>(Controlador.GetUrlApiService(servicio,"Proyectos"), Modelo, Method.POST);
            


			if(conversion != null)
			{
				Reportes.RptConceptosContratos rpt = new Reportes.RptConceptosContratos();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, conversion,Modelo.Descripcion, tipo);
			}
			return reporte;
		}
		public CDescargaReporte ReporteConceptosContratosInstalados(string NameFile, ETipoReporte Tipo, CContratosDetalles2 Modelo)
		{
			CDescargaReporte reporte = null;
			List<CContratosDetalles2> conversion = null;
			conversion = Controlador.BuildPeticionVerb<List<CContratosDetalles2>>(Controlador.GetUrlApiService("ConceptosPorContratosInstalado","Proyectos"), Modelo, Method.POST);
			if(conversion != null)
			{
				Reportes.RptConceptosContratosInstalados rpt = new Reportes.RptConceptosContratosInstalados();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, conversion, Modelo.Descripcion);
			}
			return reporte;
		}
		public CDescargaReporte GeneraReporte(string NameFile, ETipoReporte Tipo, MProyectosCostos Modelo)
		{
			CDescargaReporte reporte = null;
			Modelo.IdEmpresa = Controlador.GetIdEmpresa();
			Modelo.IdUnidad = Controlador.GetIdUnidad();
			List<CProyectos> conversion = Controlador.BuildPeticionVerb<List<CProyectos>>(Controlador.GetUrlApiService("ReporteCostos", "Proyectos"), Modelo, Method.POST);
			if(conversion != null)
			{
				Reportes.RptProyectosCostos rpt = new Reportes.RptProyectosCostos();
				rpt.MonedaDestino = String.Format("{0} {1}", Modelo.MonedaDestino.MonedaDestino, Modelo.MonedaDestino.CodigoMonedaDestino);
				rpt.FechaTipoCambio = Modelo.MonedaDestino.FechaInicio.ToString("d");
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile, rpt.FechaTipoCambio.Replace("//", "_"));
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, conversion, Modelo.TipoCambios, Modelo);
			}
			return reporte;
		}
		public CDescargaReporte ReporteConceptosContratosSolicitadoInstalado(string NameFile, ETipoReporte Tipo, CContratosDetalles2 Modelo, int tipo)
		{
			CDescargaReporte reporte = null;
			List<CContratosDetalles2> conversion = null;

            if (tipo == 1)
			    conversion = Controlador.BuildPeticionVerb<List<CContratosDetalles2>>(Controlador.GetUrlApiService("ConceptosPorContratosSolicitadoInstalado", "Proyectos"), Modelo, Method.POST);
            else
                conversion = Controlador.BuildPeticionVerb<List<CContratosDetalles2>>(Controlador.GetUrlApiService("ConceptosSolicitadoInstaladoArea", "Proyectos"), Modelo, Method.POST);

			if(conversion != null)
			{
				Reportes.RptConceptosContratosSolicitadoInstalado rpt = new Reportes.RptConceptosContratosSolicitadoInstalado();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, conversion, Modelo.Descripcion, tipo);
			}
			return reporte;
		}
		public CDescargaReporte ReporteSolicitudInsumosInstaladoProgramado(string NameFile, 
																								 ETipoReporte Tipo, 
																								 CContratosDetalles2 Modelo,
																								 IEnumerable<IContratosDetallesInsumos2> ReporteData)
		{
			CDescargaReporte reporte = null;
			if(ReporteData != null)
			{
				Reportes.RptInsumosInstaladosProgramados rpt = new Reportes.RptInsumosInstaladosProgramados();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, ReporteData, Modelo.Descripcion, 1);
			}
			return reporte;
		}
		public CDescargaReporte ReporteSolicitudInsumosInstaladoFecha(string NameFile,
																								 ETipoReporte Tipo,
																								 CContratosDetalles2 Modelo,
																								 IEnumerable<IContratosDetallesInsumos2> ReporteData)
		{
			CDescargaReporte reporte = null;
			if(ReporteData != null)
			{
				Reportes.RptInsumosInstaladosProgramados rpt = new Reportes.RptInsumosInstaladosProgramados();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, ReporteData, Modelo.Descripcion, 2);
			}
			return reporte;
		}
		public CDescargaReporte ReporteEstimacionesPorContrato(string NameFile,
																				 ETipoReporte Tipo,
																				 CContratosDetalles2 Modelo,
																				 IEnumerable<IContratosDetalles2> ReporteData)
		{
			CDescargaReporte reporte = null;
			if(ReporteData != null)
			{
				Reportes.RptEstimacionesPorContrato rpt = new Reportes.RptEstimacionesPorContrato();
				rpt.UrlBase = Controlador.GetBaseUrlService();
				rpt.Peticion = Controladores.MVC.MvcBase.BuildPeticionService;
				reporte = new CDescargaReporte();
				reporte.Id = Guid.NewGuid().ToString();
				reporte.Nombre = String.Format(NameFile);
				Controlador.TempData[reporte.Id] = rpt.GeneraReporte(Tipo, ReporteData, Modelo.Descripcion);
			}
			return reporte;
		}
	}
}
