﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public  delegate string TraduceTexto(string texto);
	public interface IUICatalogo
	{
		IEnumerable<IEntidad> Contenido
		{
			set;
			get;
		}
		IEnumerable<IPropsWhere> FieldsSearch
		{
			set;
			get;
		}
		IUsuarios Usuario
		{
			set;
			get;
		}
		IEnumerable<IPerfilAcciones> Permisos
		{
			set;
			get;
		}
		IPaginaUI PaginaInfo
		{
			set;
			get;
		}
		string Nombre
		{
			get;
			set;
		}
		string ControladorName
		{
			get;
			set;
		}
		string controlesPersonalizados
		{
			set;
			get;
		}
		string NombreToolBar
		{
			set;
			get;
		}
		int MenuCaller
		{
			set;
			get;
		}
		/// <summary>
		/// Indica el ID del menu que inicio la peticion
		/// </summary>
		/*int IdMenu
		{
			get;
			set;
		}	*/	
		TraduceTexto Traductor
		{
			get;
			set;
		}
        string Empresas
        {
            get;
            set;
        }

        void SetPermisos(IEnumerable<IPerfilAcciones> permisos);
		bool HasPermiso(string name);		
	}//IPaginaUI ends.
}//NameSpace ends.
