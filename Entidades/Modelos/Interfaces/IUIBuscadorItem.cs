﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public interface IUIBuscadorItem
	{
		string Name{set;get;}
		string DisplayName
		{
			set;
			get;
		}
		string Tipo
		{
			set;
			get;
		}
	}//IPaginaUI ends.
}//NameSpace ends.
