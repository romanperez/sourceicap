﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public interface IPaginaUI
	{
		string Nombre{set;get;}
		string Url{set;get;}
		string Parametros{set;get;}
		string Metodo{set;get;}
		string NumeroPaginaTexto
		{
			set;
			get;
		}
		int PaginaActual{set;get;}
		int PaginasTotal{set;get;}
		int PrimeraPagina{set;get;}
		int PaginaAnterior{set;get;}
		int PaginaSiguiente{set;get;}
		int UlimaPagina{set;get;}
	}//IPaginaUI ends.
}//NameSpace ends.
