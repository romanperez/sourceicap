﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{	
	public interface IUICatalogoProyectos
	{
		string Error
		{
			set;
			get;
		}
		IUICatalogo Catalogo
		{
			set;
			get;
		}
		List<CProyectosCostos> Monedas
		{
			set;
			get;
		}
		List<CProyectosCostos> TotalPagina
		{
			set;
			get;
		}
		List<CProyectosCostos> TotalGlobal
		{
			set;
			get;
		}
		double ByMoneda(IProyectosCostos Moneda, IProyectos proyecto);
		double TotalPaginaByMoneda(IProyectosCostos Moneda);
		double TotalGlobalByMoneda(IProyectosCostos Moneda);
		
	}//IPaginaUI ends.
}//NameSpace ends.
