﻿using Idiomas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CParamRptOTActividad
	{
		protected DateTime _FechaHoy;
		protected DateTime _FechaInicial;
		protected DateTime _FechaFinal;
		[Display(Name = "CParamRptOTActividadProyecto", ResourceType = typeof(IcapWeb))]
		public int IdProyecto
		{
			set;
			get;
		}
		[Display(Name = "CParamRptOTActividadContrato", ResourceType = typeof(IcapWeb))]
		public int IdContrato
		{
			set;
			get;
		}

		/*
		[Display(Name = "CParamRptOTActividadFechaInicial", ResourceType = typeof(IcapWeb))]
		//[DataType(DataType.Date)]
		//[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public virtual DateTime FechaInicial
		{
			set;
			get;
		}
		[Display(Name = "CParamRptOTActividadFechaFinal", ResourceType = typeof(IcapWeb))]
		//[DataType(DataType.Date)]
		//[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public virtual DateTime FechaFinal
		{
			set;
			get;
		}
		*/
		[Display(Name = "CParamRptOTActividadFechaHoy", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaHoy
		{
			get
			{
				return _FechaHoy;
			}
			set
			{
				_FechaHoy = value;
			}
		}
	   [Display(Name = "CParamRptOTActividadFechaInicial", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaInicial
		{
			get
			{
				return _FechaInicial;
			}
			set
			{
				_FechaInicial = value;
			}
		}
	   [Display(Name = "CParamRptOTActividadFechaFinal", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaFinal
		{
			get
			{
				return _FechaFinal;
			}
			set
			{
				_FechaFinal = value;
			}
		}
		public virtual IEnumerable<IvwContratos2> Contratos
		{
			set;
			get;
		}
		public CParamRptOTActividad()
		{
			Contratos = new List<IvwContratos2>();
		}
	}
}
