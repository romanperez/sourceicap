using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{			
    public class CPaginaUI:IPaginaUI
    {
		 public virtual string Nombre
		 {
			 set;
			 get;
		 }
		 public virtual string Url
		 {
			 set;
			 get;
		 }
		 public virtual string Parametros
		 {
			 set;
			 get;
		 }
		 public virtual int PaginaActual
		 {
			 set;
			 get;
		 }
		 public virtual int PaginasTotal
		 {
			 set;
			 get;
		 }
		 public virtual int PrimeraPagina
		 {
			 set;
			 get;
		 }
		 public virtual int PaginaAnterior
		 {
			 set;
			 get;
		 }
		 public virtual int PaginaSiguiente
		 {
			 set;
			 get;
		 }
		 public virtual int UlimaPagina
		 {
			 set;
			 get;
		 }
		 public virtual string Metodo
		 {
			 set;
			 get;
		 }
		 public virtual string NumeroPaginaTexto
		 {
			 set;
			 get;
		 }
	 }// CIPaginaUI ends.
}// Entidades ends.
