﻿using Idiomas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CFiltrosInstaladoSolicitado
	{	
		[Display(Name = "CFiltrosMovimientoFechaIni", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaIni
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoFechaFin", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaFin
		{
			set;
			get;
		}		
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		public virtual string Proyecto
		{
			set;
			get;
		}
		[Display(Name = "IClientesRazonSocial", ResourceType = typeof(IcapWeb))]  
		public virtual int IdCliente
		{
			set;
			get;
		}		
	}
}
