﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CModelTraspasos
	{
		public string Empresa
		{
			set;
			get;
		}
		public string Unidad
		{
			set;
			get;
		}
		public bool PermisoCambioUnidades
		{
			set;
			get;
		}
		public CMovimientos Movimiento
		{
			set;
			get;
		}
		public List<CUnidades> Unidades
		{
			set;
			get;
		}
		public List<CAlmacenes> Almacenes
		{
			set;
			get;
		}
		public List<CPedidos> Pedidos
		{
			set;
			get;
		}
		public List<CAlmacenes> AlmacenesDestino
		{
			set;
			get;
		}
	}
}
