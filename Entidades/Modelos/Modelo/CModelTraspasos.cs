﻿using Idiomas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CModelEntrada
	{
		[Display(Name = "IMovimientosFecha", ResourceType = typeof(IcapWeb))]
		public DateTime Fecha
		{
			set;get;
		}
		public string Empresa
		{
			set;
			get;
		}
		public string Unidad
		{
			set;
			get;
		}
		public int IdUnidad
		{
			set;
			get;
		}
		public CMovimientos Movimiento
		{
			set;
			get;
		}		
		public List<CAlmacenes> Almacenes
		{
			set;
			get;
		}
		public List<CPedidos> Pedidos
		{
			set;
			get;
		}		
	}
}
