﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CFileRequest
	{
		public virtual string Nombre
		{
			set;
			get;
		}
		public virtual byte[] Contenido
		{
			set;
			get;
		}
	}
}
