﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CEmail
	{
		public virtual string To
		{
			set;
			get;
		}
		//public virtual string CC
		//{
		//	set;
		//	get;
		//}
		//public virtual string CCO
		//{
		//	set;
		//	get;
		//}
		public virtual string From
		{
			set;
			get;
		}
		public virtual string Asunto
		{
			set;
			get;
		}
		public virtual string Body
		{
			set;
			get;
		}
		public virtual bool isHtml
		{
			set;
			get;
		}
		public virtual bool Enviado
		{
			set;
			get;
		}
	}
}
