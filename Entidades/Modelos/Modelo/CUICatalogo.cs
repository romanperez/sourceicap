using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public class CUICatalogo:IUICatalogo
    {		
		protected IEnumerable<IEntidad> _Contenido;
		protected IPaginaUI _PaginaInfo;
		IEnumerable<IPropsWhere> _FieldsSearch;
		public virtual IEnumerable<IPropsWhere> FieldsSearch
		{
			set
			{
				_FieldsSearch = value;
			}
			get
			{
				return _FieldsSearch;
			}
		}
		public virtual IEnumerable<IEntidad> Contenido
		{
			set
			{
				_Contenido=value;
			}
			get
			{
				return _Contenido;
			}
		}
		public virtual IPaginaUI PaginaInfo
		{
			set
			{
				_PaginaInfo = value;
			}
			get
			{
				return _PaginaInfo;
			}
		}
		public virtual  string Nombre
		 {
			 get;
			 set;
		 }
		public virtual string Metodo
		{
			set;
			get;
		}
		public virtual int IdMenu
		{
			get;
			set;
		}
		public virtual string Permisos
		{
			set;
			get;
		}
		public CUICatalogo(){}
		public virtual TraduceTexto Traductor
		{
			set;
			get;
		}
		public virtual void SetPermisos(IEnumerable<IPerfilAcciones> permisos)	
		{
			List<IPerfiles> newPermisos = new List<IPerfiles>();
			foreach(IPerfilAcciones permiso in permisos)
			{
				string []valores = permiso.ParametrosJs.Split('|');
				if(valores != null && valores.Count() > 0)
				{
					foreach(string valor in valores)
					{
						string[] propiedades = valor.Split(',');
						string Name = propiedades[0];
						string value = propiedades[1];
						if(value.ToLower() == "traduce")
						{
							value = this.Traductor(propiedades[2]);
						}
						else
						{
							value=
						}
					}
				}
			}
		}
	 }// CUICatalogo ends.
}// Entidades ends.
