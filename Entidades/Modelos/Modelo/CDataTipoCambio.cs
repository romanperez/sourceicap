﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CDataTipoCambio
	{
		public int IdMoneda
		{
			set;
			get;
		}
		public string Moneda
		{
			set;
			get;
		}
		public int IdTipoCambio
		{
			set;
			get;
		}
		public bool RequiereConversion(IMonedas Predeterminada)
		{
			return IdMoneda != Predeterminada.IdMoneda;
		}
	}
}
