﻿using Idiomas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CFiltrosMovimiento
	{
		[Display(Name = "IMovimientosTipo", ResourceType = typeof(IcapWeb))]
		public virtual int Tipo
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoFechaIni", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaIni
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoFechaFin", ResourceType = typeof(IcapWeb))]
		public virtual DateTime FechaFin
		{
			set;
			get;
		}		
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		public virtual string Proyecto
		{
			set;
			get;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		public virtual string Contrato
		{
			set;
			get;
		}
		[Display(Name = "IUsuariosUsuario", ResourceType = typeof(IcapWeb))]
		public virtual int IdUsuario
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoIdUnidadOrigen", ResourceType = typeof(IcapWeb))]
		public virtual int IdUnidadOrigen
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoIdAlmacenOrigen", ResourceType = typeof(IcapWeb))]
		public virtual int IdAlmacenOrigen
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoIdUnidadDestino", ResourceType = typeof(IcapWeb))]
		public virtual int IdUnidadDestino
		{
			set;
			get;
		}
		[Display(Name = "CFiltrosMovimientoIdAlmacenDestino", ResourceType = typeof(IcapWeb))]
		public virtual int IdAlmacenDestino
		{
			set;
			get;
		}
	}
}
