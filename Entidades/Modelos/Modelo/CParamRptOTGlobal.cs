﻿using Idiomas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CParamRptOTGlobal
	{
		[Display(Name = "CParamRptOTGlobalConcepto", ResourceType = typeof(IcapWeb))]
		public virtual bool Concepto
		{
			set;
			get;
		}
		[Display(Name = "CParamRptOTGlobalPartida", ResourceType = typeof(IcapWeb))]
		public virtual bool Partida
		{
			set;
			get;
		}
		[Display(Name = "CParamRptOTGlobalInsumos", ResourceType = typeof(IcapWeb))]
		public virtual bool Insumos
		{
			set;
			get;
		}
		[Display(Name = "CParamRptOTGlobalAnalisis", ResourceType = typeof(IcapWeb))]
		public virtual bool Analisis
		{
			set;
			get;
		}		
	}
}
