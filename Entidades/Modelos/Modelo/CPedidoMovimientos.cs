﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CPedidoMovimientos
	{
		public virtual IEnumerable<IPedidos> Pedidos
		{
			set;
			get;
		}
		public virtual IEnumerable<IMovimientos> Movimientos
		{
			set;
			get;
		}
	}
}
