﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class EntityTable:Attribute
	{
		public bool IsPrimary
		{
			set;
			get;
		}
		public bool IsUnique
		{
			set;
			get;
		}
		public string Table
		{
			set;
			get;
		}
		public string Campo
		{
			set;
			get;
		}
		public string Propiedad
		{
			set;
			get;
		}
		public object Valor
		{
			set;
			get;
		}
		public bool ShowResult
		{
			set;
			get;
		}
	}
}
