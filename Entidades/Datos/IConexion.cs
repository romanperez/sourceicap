using System.Collections.Generic;
using System.Data;
namespace Entidades
{
    public partial interface IConexion
    {        
        string ConnectionString { set; get; }    
        string Query { set; get; }
        IDataReader GetReader();
        object GetParam(string Name);
        void NonQuery();
        void CreateCommand(System.Data.CommandType tipo);
		  void ChangeQueryCommand();        
        void AddParam(string Name, object Value);
        void AddParam(IDataParameter Parametro);
        void AddParam(object Type, System.Data.ParameterDirection Direction, string Name, object Value);
        void AddParam(object Type, System.Data.ParameterDirection Direction, string Name);
		  void AddParam(object Type,int longitud, System.Data.ParameterDirection Direction, string Name);
		  void AddParam(object Type, int longitud, System.Data.ParameterDirection Direction, string Name,object Value); 
        void CloseConection();
        List<data> GetListEntities<data>();	
		  void BeginTran();
        void CommitTran();
        void RollBackTran();
		  string DBName();
		  ConnectionState Status();
		  bool HasRows(IDataReader reader);
		  object ExcuteScalar();
    }
}

