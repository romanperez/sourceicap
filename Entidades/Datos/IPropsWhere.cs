﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{

	public interface IPropsWhere
	{
		bool OmiteConcatenarTabla
		{
			set;
			get;
		}
		bool OmiteParametro
		{
			set;
			get;
		}
		string Plantilla
		{
			set;
			get;
		}
		bool Prioridad{set;get;}
		string Operador{set;get;}
		string NombreCampo{set;get;}
		string Condicion{set;get;}
		object Valor{set;get;}
		bool ValidaConsultaMVC(object Entidad);
		string RemoveDot();
	}
}
