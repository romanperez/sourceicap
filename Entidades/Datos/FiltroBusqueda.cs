﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Entidades
{
	public class FiltroBusqueda:Attribute
	{
		public string Campo
		{
			set;
			get;
		}
		public string CampoDisplay
		{
			set;
			get;
		}
		public bool ExcluirCampo
		{
			set;
			get;
		}
		public bool OmiteConcatenarTabla
		{
			set;
			get;
		}
		/// <summary>
		/// Se usa para enviar el where como cadena en ves de parametro.
		/// </summary>
		public string Plantilla
		{
			set;
			get;
		}
		public FiltroBusqueda()
		{
			ExcluirCampo = false;			
		}
	}
}
