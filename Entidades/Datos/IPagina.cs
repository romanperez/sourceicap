﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public interface IPagina<Entity> where Entity : IEntidad, new()
	{
		int Pagina
		{
			set;
			get;
		}
		int Paginas
		{
			set;
			get;
		}
		int RegistrosPP
		{
			set;
			get;
		}
		IEnumerable<Entity> Contenido
		{
			set;
			get;
		}
		string BuildQuery(string table, string campoKey, string select, string where);
	}
}
