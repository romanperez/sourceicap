﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public interface IMyException
	{
		int IdError
		{
			set;
			get;
		}
		int Columna
		{
			set;
			get;
		}
		int Linea
		{
			set;
			get;
		}
		string Pila
		{
			set;
			get;
		}
		string Funcion
		{
			set;
			get;
		}
		string Archivo
		{
			set;
			get;
		}
		string Error
		{
			set;
			get;
		}
		string Info
		{
			set;
			get;
		}
	}
}
