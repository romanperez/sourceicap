﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public interface IError
	{
		int IdError
		{
			set;
			get;
		}
		string Error
		{
			set;
			get;
		}
		void SaveLog(Exception error);
		void SaveFile(Exception eBase, IMyException log);
	}
}
