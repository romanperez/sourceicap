﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Entidades
{
	public class DFile
	{
		public string Error
		{
			set;
			get;
		}
		public string Name
		{
			set;
			get;
		}
		/// <summary>
		/// Ruta fisica del archivo [Ruta\Nombrearchivo.Extension]
		/// </summary>
		public string Ruta
		{
			set;
			get;
		}
		public string Url
		{
			set;
			get;
		}
		public byte[] Contenido
		{
			set;
			get;
		}
		public string ContenidoString
		{
			set;
			get;
		}
		public void OnError(Exception e)
		{
			Error = e.Message;
		}
		public void BuildRuta(string pathBase)
		{
			try
			{
				string[] ruta = this.Ruta.Split('/');
				foreach(string r in ruta)
				{
					if(r.IndexOf('.') <= 0)
					{
						pathBase =Path.Combine(pathBase,r);
						if(!Directory.Exists(pathBase))
						{
							Directory.CreateDirectory(pathBase);
						}
					}
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
		}
		public void GuardaArchivo(string path)
		{
			string fullPath = Path.Combine(path, this.Ruta,this.Name);
			if(File.Exists(fullPath))
			{
				File.Delete(fullPath);
			}
			File.WriteAllBytes(fullPath, this.Contenido);			
		}
		public void LeeArchivo(string path)
		{
			string fullPath = Path.Combine(path, this.Url);
			this.Contenido= File.ReadAllBytes(fullPath);			
			if(this.Contenido != null)
			{
				this.ContenidoString = Convert.ToBase64String(this.Contenido);
				this.Contenido = null;
			}						
		}
		public void EliminaArchivo(string path)
		{
			string fullPath = Path.Combine(path, this.Url);
			if(File.Exists(fullPath))
			{
				File.Delete(fullPath);
			}
		}
	}
}
