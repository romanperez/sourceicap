﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Entidades
{
	public interface IBase
	{
		IConexion GetConection
		{
			get;
		}
		string Tabla
		{
			set;
			get;
		}
		int Delete
		{
			set;
			get;
		}
		int Update
		{
			set;
			get;
		}
		int Nuevo
		{
			set;
			get;
		}
		string GroupBy
		{
			set;
			get;
		}
		IEntidad BuildEntity();
		IEnumerable<IEntidad> Select(int top, IEntidad entity);
		IEnumerable<IEntidad> Select(IEntidad entity);
		IPagina<Entity> Select<Entity>(IEntidad entity, string camposKey, int Pagina)where Entity:IEntidad,new() ;
		IPagina<Entity> Select<Entity>(IEntidad entity, string camposKey, int Pagina, int RegistrosPP) where Entity : IEntidad, new();
		void AddWhere(List<IPropsWhere> propiedades);
		void AddWhere(IPropsWhere propiedad);
		void AddWhere(string Nombre, string Condicion, object Valor);
		void AddWhere(string Nombre, string Condicion);
		void ClearWhere();
		List<IPropsWhere> Where();
		void OnError(Exception Error);
		void ClearError();
		void InicializaError();
		IError GetError();
		void SetError(string error);
		void SetError(string error, bool append);
		void Save(IEntidad IEntity);
		void SetConfigJoins(string []campos,string[] join);
		string[] CamposJoin();
		string[] CondicionesJoin();
		double fnConvierteCantidad(int IdMonedaOrigen, int IdMonedaDestino, double cantidad, double tipocambio);
		int fnGetIdValorCambio(int IdEmpresa, DateTime Fecha, int IdMonedaOrigen, int IdMonedaDestino);
		double fnGetValorCambio(int IdEmpresa, DateTime Fecha, int IdMonedaOrigen, int IdMonedaDestino);
		double fnGetValorIva(int IdEmpresa, DateTime Fecha);
		int fnGetIdValorIva(int IdEmpresa, DateTime Fecha);
        int fnValidaPeriodoEstimacion(int IdContrato, DateTime Fecha);
	}
}
