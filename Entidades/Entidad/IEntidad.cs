﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
	public interface IEntidad
	{
		string[] GetAllCampos();
		string[] GetCamposKey();
		string[] GetOmitirCamposSelect();
		//string[] GetCamposSelectJoin();
		string[] GetFieldsSearch();
		string[] GetFieldsSearchOmitir();
		clase ToJson<clase>(string entidad);
		string ToJson(IEntidad entidad);
		string[] GetFieldsExcelExport();
		string[] GetFieldsExcelExportOmitir();		
	}
}
