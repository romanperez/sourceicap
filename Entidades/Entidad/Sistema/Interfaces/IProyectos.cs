using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface IProyectos : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdProyecto
		{
			get;
			set;
		}
		int IdCliente
		{
			set;get;
		}
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			set;
			get;
		}
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdUnidad
		{
			set;
			get;
		}
		int? IdCotizacion
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectosProyecto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Proyecto",Plantilla="'{0}'")]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Proyecto
		{
			get;
			set;
		}		
		[Display(Name = "IProyectosDescripcion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Descripcion", Plantilla = "'{0}'")]
		string Descripcion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectosUbicacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectosUbicacion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Ubicacion", Plantilla = "'{0}'")]
		string Ubicacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectosFechaInicial", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectosFechaInicial", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "FechaInicial", Plantilla = "'{0}'")]
		DateTime FechaInicial
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectosFechaFinal", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectosFechaFinal", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "FechaFinal", Plantilla = "'{0}'")]
		DateTime FechaFinal
		{
			get;
			set;
		}		
		[Display(Name = "IClientesRazonSocial", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Clientes.RazonSocial",OmiteConcatenarTabla=true,Plantilla="'{0}'")]
		string RazonSocial
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesTotal", ResourceType = typeof(IcapWeb))]
		double Total
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Empresa
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Unidades.Unidad", OmiteConcatenarTabla = true,Plantilla="'{0}'")]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IProyectoContratosIdContrato", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		List<CProyectoContratos> Contratos
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesCotizacion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Cotizaciones.Cotizacion", OmiteConcatenarTabla = true,Plantilla="'{0}'")]
		string Cotizacion
		{
			set;
			get;
		}
		[Display(Name = "IProyectoContratosIdContrato", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string AllContratos
		{
			set;
			get;
		}		
		int IdEstatus
		{
			get;
			set;
		}
		[Display(Name = "IProyectoEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Estatus.Nombre", OmiteConcatenarTabla = true,Plantilla="'{0}'")]
		string EstatusProyecto
		{
			get;
			set;
		}
		List<CvwContratos2> vwContratos
		{
			set;
			get;
		}
		List<CProyectosCostos> CostosGlobales
		{
			set;
			get;
		}
		string MonedaCosto
		{
			set;
			get;
		}
		double CostoProyecto
		{
			set;
			get;
		}
		string MonedaConversion
		{
			set;
			get;
		}
		double MonedaConversionCosto
		{
			set;
			get;
		}
	}// IProyectos ends.
}// LibEntidades.Entidades ends.
