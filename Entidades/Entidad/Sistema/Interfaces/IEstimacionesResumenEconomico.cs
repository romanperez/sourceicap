using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 03/10/2017 11:24:11 a. m.*/
namespace Entidades
{
    public partial interface IEstimacionesResumenEconomico:IEstimaciones
	{        

        string EstimacionTitle{set;get;}
		DateTime? FechaInicio2	{set;get;}
		DateTime?FechaFin2{set;get;}
		double ImporteAcumuladoAnterior{set;get;}
		double ImporteAnticipo{set;get;}
		double ImporteAmortizacionAnticipo{set;get;}
        double ImportePagar { set; get; }
	}// IEstimaciones ends.
}// LibEntidades.Entidades ends.
