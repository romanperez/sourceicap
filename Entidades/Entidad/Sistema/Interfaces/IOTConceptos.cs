using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:36 p. m.*/
namespace Entidades
{
	public partial interface IOTConceptos : IEntidad
	{		
		int IdOTConcepto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTConceptosIdOrdenTrabajo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTConceptosIdOrdenTrabajo", ResourceType = typeof(IcapWeb))]
		int IdOrdenTrabajo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTConceptosIdContratoDetalle", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTConceptosIdContratoDetalle", ResourceType = typeof(IcapWeb))]
		int IdContratosDetalles
		{
			get;
			set;
		}
		int IdConcepto
		{
			set;
			get;
		}		
		[Display(Name = "IOTConceptosCantidadSolicitada", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}		
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		string UnidadMedida
		{
			set;
			get;
		}
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		string Concepto
		{
			set;
			get;
		}
		[Display(Name = "IConceptosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			set;
			get;
		}		
		[Display(Name = "IOTConceptosCantidadProgramada", ResourceType = typeof(IcapWeb))]
		double CantidadProgramada
		{
			get;
			set;
		}		
		[Display(Name = "IOTConceptosCantidadEjecutada", ResourceType = typeof(IcapWeb))]
		double CantidadEjecutada
		{
			get;
			set;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		string Contrato
		{
			get;
			set;
		}
		List<COTConceptosEjecucion> Ejecuciones
		{
			set;
			get;
		}
		CConceptos InfoConcepto
		{
			set;
			get;
		}
		[Display(Name = "IContratos2Ubicacion", ResourceType = typeof(IcapWeb))]
		string Ubicacion
		{
			get;
			set;
		}
		[Display(Name = "IClasificacionConceptosClasificacion", ResourceType = typeof(IcapWeb))]
		string Clasificacion
		{
			set;
			get;
		}
		int IdConceptoClasificacion
		{
			get;
			set;
		}
		[Display(Name = "IContratosDetalles2Partida", ResourceType = typeof(IcapWeb))]
		int Partida
		{
			get;
			set;
		}
		List<CContratosDetallesInsumos2> Insumos
		{
			set;
			get;
		}
		[Display(Name = "IContratos2EsAdicional", ResourceType = typeof(IcapWeb))]
		bool EsAdicional
		{
			get;
			set;
		}
		[Display(Name = "IOTConceptosCantidadProgramada", ResourceType = typeof(IcapWeb))]
		double CantidadProgramadaOT
		{
			get;
			set;
		}
		[Display(Name = "IOTConceptosCantidadEjecutada", ResourceType = typeof(IcapWeb))]
		double CantidadEjecutadaOT
		{
			get;
			set;
		}
	}// IOTConceptos ends.
}// LibEntidades.Entidades ends.
