using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:33 p. m.*/
namespace Entidades
{
	public partial interface IConceptosInsumos : IEntidad
	{
		int IdConceptoInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosInsumosIdConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConceptosInsumosIdConcepto", ResourceType = typeof(IcapWeb))]
		int IdConcepto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosInsumosIdInsumo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConceptosInsumosIdInsumo", ResourceType = typeof(IcapWeb))]
		int IdInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosInsumosCantidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConceptosInsumosCantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		string Codigo
		{
			get;
			set;
		}
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}

		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		string CodigoMoneda
		{
			get;
			set;
		}
		double CostoInsumo
		{
			set;
			get;
		}
		int IdCapitulo
		{
			set;
			get;
		}
		int IdMoneda
		{
			set;
			get;
		}
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			set;
			get;
		}
		[Display(Name = "IUnidadesMedidaDescripcion", ResourceType = typeof(IcapWeb))]
		string UnidadMedidaDescripcion
		{
			set;
			get;
		}
		string ConceptoPadre
		{
			set;
			get;
		}
		string ToXml();		
	}// IConceptosInsumos ends.
}// LibEntidades.Entidades ends.
