using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:34 p. m.*/
namespace Entidades
{
	public partial interface IEspecialidades : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdEspecialidad
		{
			get;
			set;
		}
		[Display(Name = "IEspecialidadesIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEspecialidadesGrado", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEspecialidadesGrado", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdGrado
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEspecialidadesCodigo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEspecialidadesCodigo", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Codigo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEspecialidadesNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEspecialidadesNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEspecialidadesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEspecialidadesEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		int IdTecnico
		{
			set;
			get;
		}
		[Display(Name = "IEspecialidadesGrado", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "tecnicosgrados.Nombre",OmiteConcatenarTabla=true)]
		string Grado
		{
			get;
			set;
		}
	}// IEspecialidades ends.
}// LibEntidades.Entidades ends.
