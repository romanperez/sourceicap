using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:34 p. m.*/
namespace Entidades
{
	public partial interface ICotizacionesDetalles : IEntidad
	{
		int IdCotizacionDetalle
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesDetallesIdCotizacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesDetallesIdCotizacion", ResourceType = typeof(IcapWeb))]
		int IdCotizacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesDetallesIdIConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesDetallesIdIConcepto", ResourceType = typeof(IcapWeb))]
		int IdConcepto
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesDetallesPartida", ResourceType = typeof(IcapWeb))]
		int Partida
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesDetallesCantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			set;
			get;
		}
		/// <summary>
		/// El precio que se guarda o se envia a base de datos, es el precio unitario del concepto MAS el porcentaje de utilidad.
		/// La moneda que se guarda es en pesos MXN.
		/// </summary>
		[Required(ErrorMessageResourceName = "RequiredICotizacionesDetallesPrecio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesDetallesPrecio", ResourceType = typeof(IcapWeb))]
		double Precio
		{
			get;
			set;
		}
		[Display(Name = "IConceptosUbicacion", ResourceType = typeof(IcapWeb))]
		string Ubicacion
		{
			get;
			set;
		}		
		/*[Required(ErrorMessageResourceName = "RequiredICotizacionesDetallesPorcentajeDescuento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesDetallesPorcentajeDescuento", ResourceType = typeof(IcapWeb))]
		double PorcentajeDescuento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesDetallesDescuentoMonto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesDetallesDescuentoMonto", ResourceType = typeof(IcapWeb))]
		double DescuentoMonto
		{
			get;
			set;
		}*/
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		string Concepto
		{
			get;
			set;
		}		
		[Display(Name = "IConceptosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Display(Name = "ICapitulosCapitulo", ResourceType = typeof(IcapWeb))]
		string Capitulo
		{
			get;
			set;
		}
		int IdCapitulo
		{
			get;
			set;
		}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		string CodigoMoneda
		{
			get;
			set;
		}
		int IdMoneda
		{
			get;
			set;
		}
		int IdUnidadMedida
		{
			get;
			set;
		}		
		[Display(Name = "IConceptosIdUnidadMedida", ResourceType = typeof(IcapWeb))]
		string UnidadMedida
		{
			get;
			set;
		}
		List<CCotizacionesDetallesInsumos> Insumos
		{
			set;
			get;
		}
		double PrecioOriginal
		{
			set;
			get;
		}
		double ValorTipoCambio
		{
			set;
			get;
		}
		string Monedas
		{
			set;
			get;
		}
		DateTime Fecha
		{
			set;
			get;
		}
	}// ICotizacionesDetalles ends.
}// LibEntidades.Entidades ends.
