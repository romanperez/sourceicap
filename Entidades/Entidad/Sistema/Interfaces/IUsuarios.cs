using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public partial interface IUsuarios : IEntidad
	{
		int IdUsuario
		{
			get;
			set;
		}
		int IdPerfil
		{
			get;
			set;
		}
		int? IdEmpleado
		{
			get;
			set;
		}
		[Required(ErrorMessage = "*")]
		[Display(Name = "IUsuariosUsuario", ResourceType = typeof(IcapWeb))]
		string Usuario
		{
			get;
			set;
		}		
		[Display(Name = "IUsuariosClave", ResourceType = typeof(IcapWeb))]
		[StringLength(16, ErrorMessageResourceName = "IUsuariosClaveLength", ErrorMessageResourceType = typeof(IcapWeb), MinimumLength = 0)]
		string Clave
		{
			get;
			set;
		}
		
		[Display(Name = "IUsuariosClaveConfirm", ResourceType = typeof(IcapWeb))]
		[Compare("Clave", ErrorMessageResourceName = "IUsuariosClavesNoCoinciden", ErrorMessageResourceType = typeof(IcapWeb))]
		[StringLength(16, ErrorMessageResourceName = "IUsuariosClaveLength", ErrorMessageResourceType = typeof(IcapWeb), MinimumLength = 0)]
		string ClaveConfirm
		{
			get;
			set;
		}
		DateTime FechaRegistro
		{
			get;
			set;
		}
		CUsuariosData Data
		{
			get;
			set;
		}
		[Display(Name = "IUsuariosEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			set;
			get;
		}
		string InicioSession
		{
			set;
			get;
		}
		[Display(Name = "IUsuariosPerfil", ResourceType = typeof(IcapWeb))]
		string Perfil
		{
			set;
			get;
		}
		[Display(Name = "IUsuariosNombreEmpleado", ResourceType = typeof(IcapWeb))]
		string NombreEmpleado
		{
			set;
			get;
		}
		int IdMenuCaller
		{
			set;
			get;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			set;
			get;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			set;
			get;
		}
		 [Display(Name = "IDepartamentosNombre", ResourceType = typeof(IcapWeb))]
		string Departamento
		{
			set;
			get;
		}
	}// IUsuarios ends.
}// LibEntidades.Entidades ends.
