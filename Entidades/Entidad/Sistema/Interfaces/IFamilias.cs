using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 26/01/2017 04:27:07 p. m.*/
namespace Entidades
{
	public partial interface IFamilias : IEntidad
	{
		 [EntityTable(IsPrimary = true)]
		int IdFamilia
		{
			get;
			set;
		}
		[Display(Name = "IFamiliasFamilia", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Familia
		{
			get;
			set;
		}
		[Display(Name = "IFamiliasDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Display(Name = "IFamiliasEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIFamiliasIdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IFamiliasIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}

	}// IFamilias ends.
}// LibEntidades.Entidades ends.
