using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface ISisLogErrores : IEntidad
	{
		[Display(Name = "ISIS_LOG_ERRORESLogError", ResourceType = typeof(IcapWeb))]
		int Log_Error_Id
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogFecha", ResourceType = typeof(IcapWeb))]		
		DateTime? Log_Fecha
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogArchivo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]		
		string Log_Archivo
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogFuncion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Log_Funcion
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogLinea", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int? Log_Linea
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogColumna", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int? Log_Columna
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogPila", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Log_Pila
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogError", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Log_Error
		{
			get;
			set;
		}
		[Display(Name = "ISIS_LOG_ERRORESLogInfo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Log_Info
		{
			get;
			set;
		}

	}// ISisLogErrores ends.
}// LibEntidades.Entidades ends.
