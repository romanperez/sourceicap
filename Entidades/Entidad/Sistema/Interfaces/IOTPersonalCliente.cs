using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:37 p. m.*/
namespace Entidades
{
	public partial interface IOTPersonalCliente : IEntidad
	{
		int IdOTPersonalCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalClienteIdOrdenTrabajo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalClienteIdOrdenTrabajo", ResourceType = typeof(IcapWeb))]
		int IdOrdenTrabajo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalClienteIdEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalClienteIdEstatus", ResourceType = typeof(IcapWeb))]
		int IdEstatus
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalClienteIdPersonalCliente", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalClienteIdPersonalCliente", ResourceType = typeof(IcapWeb))]
		int IdPersonalCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalClienteEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalClienteEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}

		[Display(Name = "IClientesPersonalNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}		
		[Display(Name = "IClientesPersonalApellidoPaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoPaterno
		{
			get;
			set;
		}
		[Display(Name = "IClientesPersonalApellidoMaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoMaterno
		{
			get;
			set;
		}		
		[Display(Name = "IClientesPersonalPuesto", ResourceType = typeof(IcapWeb))]
		string Puesto
		{
			get;
			set;
		}
		[Display(Name = "IEstatusNombre", ResourceType = typeof(IcapWeb))]
		string NombreEstatus
		{
			get;
			set;
		}
		string NombreCompleto
		{			
			get;
		}
	}// IOTPersonalCliente ends.
}// LibEntidades.Entidades ends.
