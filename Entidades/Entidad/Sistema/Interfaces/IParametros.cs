using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:37 p. m.*/
namespace Entidades
{
    public partial interface IParametros:IEntidad
    {                
        int IdParametro { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIParametrosNombre", ErrorMessageResourceType = typeof(IcapWeb))]
[Display(Name = "IParametrosNombre", ResourceType = typeof(IcapWeb))] 
string Nombre { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIParametrosValor", ErrorMessageResourceType = typeof(IcapWeb))]
[Display(Name = "IParametrosValor", ResourceType = typeof(IcapWeb))] 
string Valor { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIParametrosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
[Display(Name = "IParametrosEstatus", ResourceType = typeof(IcapWeb))] 
bool Estatus { get;set; }
		[Display(Name = "IParametrosDescripcion", ResourceType = typeof(IcapWeb))] 
string Descripcion { get;set; }

    }// IParametros ends.
}// LibEntidades.Entidades ends.
