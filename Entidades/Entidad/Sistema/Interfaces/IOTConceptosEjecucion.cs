using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 05/05/2017 01:03:59 p. m.*/
namespace Entidades
{
	public partial interface IOTConceptosEjecucion : IEntidad
	{
		int IdOTConceptoEjecucion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTConceptosEjecucionIdOTConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTConceptosEjecucionIdOTConcepto", ResourceType = typeof(IcapWeb))]
		int IdOTConcepto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTConceptosEjecucionCantidadProgramada", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTConceptosEjecucionCantidadProgramada", ResourceType = typeof(IcapWeb))]
		double CantidadProgramada
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTConceptosEjecucionCantidadEjecutada", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTConceptosEjecucionCantidadEjecutada", ResourceType = typeof(IcapWeb))]
		double CantidadEjecutada
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTConceptosEjecucionFecha", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTConceptosEjecucionFecha", ResourceType = typeof(IcapWeb))]
		DateTime Fecha
		{
			get;
			set;
		}

	}// IOTConceptosEjecucion ends.
}// LibEntidades.Entidades ends.
