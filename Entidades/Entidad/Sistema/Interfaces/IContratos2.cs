using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{
	public partial interface IContratos2 : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2SubTotal", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2SubTotal", ResourceType = typeof(IcapWeb))]
		double SubTotal
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2Total", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2Total", ResourceType = typeof(IcapWeb))]
		double Total
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2PorcentajeDescuento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2PorcentajeDescuento", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		double PorcentajeDescuento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2DescuentoMonto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2DescuentoMonto", ResourceType = typeof(IcapWeb))]
		double DescuentoMonto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2IdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2IdEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2IdCliente", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2IdCliente", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2IdUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2IdUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdUnidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2IdIva", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2IdIva", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdIva
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2IdTipoCambio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2IdTipoCambio", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int? IdTipoCambio
		{
			get;
			set;
		}
		[Display(Name = "IContratos2IdMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdMoneda
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2Contrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Contrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratos2Fecha", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratos2Fecha", ResourceType = typeof(IcapWeb))]
		DateTime Fecha
		{
			get;
			set;
		}		
		//[Display(Name = "IPedidosPedido", ResourceType = typeof(IcapWeb))]
		//[FiltroBusqueda(ExcluirCampo = true)]
		//int? IdPedido
		//{
		//	get;
		//	set;
		//}		
		//[Display(Name = "IContratosAprobado", ResourceType = typeof(IcapWeb))]
		//[FiltroBusqueda(ExcluirCampo = true)]
		//bool Aprobado
		//{
		//	get;
		//	set;
		//}
		int? IdUsuario
		{
			set;
			get;
		}						
	
		[Display(Name = "IClientesRazonSocial", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Clientes.RazonSocial", OmiteConcatenarTabla = true)]
		string RazonSocial
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Empresa
		{
			get;
			set;
		}
		[Display(Name = "ITipoCambiosValor", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo= true)]
		double Valor
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Unidades.Unidad", OmiteConcatenarTabla = true)]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Monedas.Moneda", OmiteConcatenarTabla = true)]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string CodigoMoneda
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesIva", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "IvaValores.porcentaje", OmiteConcatenarTabla = true)]
		double Iva
		{
			get;
			set;
		}
		//bool? Movimiento
		//{
		//	get;
		//	set;
		//}
		[Display(Name = "IContratosUtilidad", ResourceType = typeof(IcapWeb))]
		double Utilidad
		{
			get;
			set;
		}
		List<CContratosDetalles2> Conceptos
		{
			set;
			get;
		}
		List<CContratosDetalles2> Adicionales
		{
			set;
			get;
		}
		List<CContratosDetalles2> CostosManuales
		{
			set;
			get;
		}
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda (Campo="Proyectos.Proyecto",OmiteConcatenarTabla=true)]
		CProyectos Proyecto
		{
			set;
			get;
		}
		[Display(Name = "IProyectosUsuario", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]		
		CUsuarios Usuario
		{
			set;
			get;
		}
		[Display(Name = "IProyectosUsuarioCostos", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		CUsuarios UsuarioCostos
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesCotizacion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Cotizaciones.Cotizacion",OmiteConcatenarTabla=true)]
		string Cotizacion
		{
			set;
			get;
		}
		[Display(Name = "IContratosUsuarioCostos", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		int? IdUsuarioCostos{set;get;}
		[Display(Name = "IContratosMotivoModificacion", ResourceType = typeof(IcapWeb))]
		string MotivoModificacion
		{
			set;
			get;
		}
        [Display(Name = "IContratosAnticipo", ResourceType = typeof(IcapWeb))]        
        double? Anticipo
        {
            get;
            set;
        }
        int? IdPersonalCliente
        {
            get;
            set;
        }
        [Display(Name = "IContratoContactoCliente", ResourceType = typeof(IcapWeb))]
        string ContactoCliente
        {
            get;
            set;
        }
	}// IContratos2 ends.
}// LibEntidades.Entidades ends.
