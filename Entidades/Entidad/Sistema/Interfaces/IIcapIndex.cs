using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public partial interface IIcapIndex
    {
		IIdioma Idioma
		{
			set;
			get;
		}
		IUsuarios Usuario
		{
			set;
			get;
		}
		IEnumerable<IMenus> Menus
		{
			set;
			get;
		}       
    }// IUsuarios ends.
}// LibEntidades.Entidades ends.
