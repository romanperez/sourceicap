using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:37 p. m.*/
namespace Entidades
{
	public partial interface IOTDocumentos : IEntidad
	{
		int IdOTDocumentos
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTDocumentosIdOrdenTrabajo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTDocumentosIdOrdenTrabajo", ResourceType = typeof(IcapWeb))]
		int IdOrdenTrabajo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTDocumentosIdTipoDocumento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTDocumentosIdTipoDocumento", ResourceType = typeof(IcapWeb))]
		int IdTipoDocumento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTDocumentosRutaDocumento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTDocumentosRutaDocumento", ResourceType = typeof(IcapWeb))]
		string RutaDocumento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTDocumentosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTDocumentosEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}		
	}// IOTDocumentos ends.
}// LibEntidades.Entidades ends.
