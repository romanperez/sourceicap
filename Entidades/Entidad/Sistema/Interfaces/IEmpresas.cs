using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{
	
	public partial interface IEmpresas : IEntidad
	{		
		
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasDireccion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasDireccion", ResourceType = typeof(IcapWeb))]
		string Direccion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasTelefono", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasTelefono", ResourceType = typeof(IcapWeb))]
		string Telefono
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasCorreoElectronico", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasCorreoElectronico", ResourceType = typeof(IcapWeb))]
		string CorreoElectronico
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasSitioWeb", ResourceType = typeof(IcapWeb))]
		string SitioWeb
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "IEmpresasRfcLength", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasRFC", ResourceType = typeof(IcapWeb))]
		[StringLength(CEmpresas.LEN_FISICA, 
						  MinimumLength =CEmpresas.LEN_MORAL,
			           ErrorMessageResourceName = "IEmpresasRfcLength", ErrorMessageResourceType = typeof(IcapWeb))]		
		string RFC
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasCodigoPostal", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasCodigoPostal", ResourceType = typeof(IcapWeb))]
		[StringLength(CEmpresas.LENGTH_CP,
						  MinimumLength = CEmpresas.LENGTH_CP,
						  ErrorMessageResourceName = "IEmpresasCodigoPostalLength", ErrorMessageResourceType = typeof(IcapWeb))]	
		int CodigoPostal
		{
			get;
			set;
		}		
		[Display(Name = "IEmpresasLogo", ResourceType = typeof(IcapWeb))]
		string Logo
		{
			get;
			set;
		}
		bool Estatus
		{
			get;
			set;
		}

	}// IEmpresas ends.
}// LibEntidades.Entidades ends.