using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 23/06/2017 01:59:19 p. m.*/
namespace Entidades
{
	public partial interface IMovimientosContratos : IEntidad
	{
		int IdMovimientoContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosContratosIdProyecto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosContratosIdProyecto", ResourceType = typeof(IcapWeb))]
		int IdProyecto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosContratosIdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosContratosIdContrato", ResourceType = typeof(IcapWeb))]
		int IdContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosContratosIdMovimiento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosContratosIdMovimiento", ResourceType = typeof(IcapWeb))]
		int IdMovimiento
		{
			get;
			set;
		}
		int IdInsumo
		{
			set;
			get;
		}
		int IdConcepto
		{
			set;
			get;
		}
		int? IdPedidoDetalle
		{
			set;
			get;
		}
		int? IdMovimientoDetalle
		{
			set;
			get;
		}
		int? IdMovimientoTraspaso
		{
			set;
			get;
		}
	}// IMovimientosContratos ends.
}// LibEntidades.Entidades ends.
