using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:36 p. m.*/
namespace Entidades
{
	public partial interface IOrdenesTrabajo : IEntidad
	{		
		int IdOrdenTrabajo
		{
			get;
			set;
		}
		int IdEmpresa
		{
			get;
			set;
		}
		int IdProyecto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoComentarios", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoComentarios", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Comentarios
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoComentariosCliente", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoComentariosCliente", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string ComentariosCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		//[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoIdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		//[Display(Name = "IOrdenesTrabajoIdContrato", ResourceType = typeof(IcapWeb))]
		//int IdContrato
		//{
		//	get;
		//	set;
		//}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoIdActividad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoIdActividad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdActividad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoFolio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoFolio", ResourceType = typeof(IcapWeb))]
		string Folio
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoUbicacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoUbicacion", ResourceType = typeof(IcapWeb))]
		string Ubicacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoFechaInicio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoFechaInicio", ResourceType = typeof(IcapWeb))]		
		DateTime FechaInicio
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoFechaFin", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoFechaFin", ResourceType = typeof(IcapWeb))]
		DateTime FechaFin
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoIdEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoIdEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		int IdEstatus
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOrdenesTrabajoIdPrioridad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOrdenesTrabajoIdPrioridad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdPrioridad
		{
			get;
			set;
		}
		[Display(Name = "IOrdenesTrabajoIdTipo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		int IdTipo
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo ="Empresas.Empresa",OmiteConcatenarTabla=true)]
		string Empresa
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Unidades.Unidad", OmiteConcatenarTabla = true)]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IActividadesActividad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Actividades.Actividad", OmiteConcatenarTabla = true)]
		string Actividad
		{
			get;
			set;
		}	
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Proyectos.Proyecto", OmiteConcatenarTabla = true)]
		string Proyecto
		{
			get;
			set;
		}
		[Display(Name = "IOrdenesTrabajoCliente", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Clientes.RazonSocial", OmiteConcatenarTabla = true)]
		string RazonSocial
		{
			get;
			set;
		}
		CActividades OTActividad
		{
			set;
			get;
		}
		
		List<COTPersonalCliente> PersonalClienteAtencion
		{
			get;
			set;
		}
		List<COTPersonalCliente> PersonalClienteContacto
		{
			get;
			set;
		}
		[Display(Name = "IOTPersonalIdTecnico", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		List<COTPersonal> OTPersonal
		{
			get;
			set;
		}
		[Display(Name = "IOrdenesTrabajoIdContrato", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		List<COTContratos> Contratos
		{
			set;
			get;
		}
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		List<COTConceptos> OTConceptos
		{
			set;
			get;
		}
		[Display(Name = "IOrdenesTrabajoIdEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "e1.Nombre", OmiteConcatenarTabla = true)]
		string EstatusOT
		{
			get;
			set;
		}		
		[Display(Name = "IOrdenesTrabajoIdPrioridad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "e2.Nombre", OmiteConcatenarTabla = true)]
	   string PrioridadOT
		{
			get;
			set;
		}
		[Display(Name = "IOrdenesTrabajoIdTipo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "e3.Nombre", OmiteConcatenarTabla = true)]
		string TipoOT
		{
			get;
			set;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Contrato
		{
			get;
			set;
		}
		int IdCliente
		{
			set;
			get;
		}
		List<DFile> UpLoadFiles
		{
			set;
			get;
		}
		List<COTDocumentos> OTDiagramas
		{
			set;
			get;
		}
		List<COTDocumentos> OTDetalleInstalacion
		{
			set;
			get;
		}
		string ListaDiagramas
		{
			set;
			get;
		}
		string ListaDetallesInstalacion
		{
			set;
			get;
		}
		string ListClienteAtencion
		{
			set;
			get;
		}
		string ListClienteContacto
		{
			set;
			get;
		}
		string ListTecnicos
		{
			set;
			get;
		}
	}// IOrdenesTrabajo ends.
}// LibEntidades.Entidades ends.
