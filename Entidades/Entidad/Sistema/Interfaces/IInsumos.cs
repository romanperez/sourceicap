using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:35 p. m.*/
namespace Entidades
{
	public partial interface IInsumos : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdInsumo
		{
			get;
			set;
		}
		int? IdFamilia
		{
			set;
			get;
		}
		[Display(Name = "IFamiliasFamilia", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Familias.Familia",OmiteConcatenarTabla=true)]
		string Familia
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Display(Name = "IInsumosIdUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		int? IdUnidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosCodigo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		[FiltroBusqueda(Campo = "Codigo")]
		string Codigo
		{
			get;
			set;
		}
		[Display(Name = "IInsumosSerie", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Serie")]
		string Serie
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosDescripcion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Descripcion")]
		string Descripcion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosIdMoneda", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosIdMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		int IdMoneda
		{
			get;
			set;
		}		
		[Required(ErrorMessageResourceName = "RequiredIInsumosEsServicio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosEsServicio", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "EsServicio")]
		bool EsServicio
		{
			get;
			set;
		}
		double? Costo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Estatus")]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "UnidadesMedida.Unidad", OmiteConcatenarTabla = true)]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesMedidaDescripcion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string UnidadMedidaDescripcion
		{
			set;
			get;
		}		
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Monedas.Moneda", OmiteConcatenarTabla = true)]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string CodigoMoneda
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Empresa
		{
			get;
			set;
		}
	}// IInsumos ends.
}// LibEntidades.Entidades ends.
