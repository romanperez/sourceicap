using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/03/2017 03:46:54 p. m.*/
namespace Entidades
{
	public partial interface IContratosDetallesInsumos2 : IEntidad
	{		
		int IdInsumoContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesInsumos2IdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesInsumos2IdContrato", ResourceType = typeof(IcapWeb))]
		int IdContrato
		{
			get;
			set;
		}
		int IdContratosDetalles
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesInsumos2IdConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesInsumos2IdConcepto", ResourceType = typeof(IcapWeb))]
		int IdConcepto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesInsumos2IdInsumo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesInsumos2IdInsumo", ResourceType = typeof(IcapWeb))]
		int IdInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesInsumos2Cantidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesInsumos2Cantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}
		[Display(Name = "IOTConceptosCantidadProgramada", ResourceType = typeof(IcapWeb))]
		double CantidadProgramada
		{
			set;
			get;
		}
		[Display(Name = "IContratosDetallesInsumos2CantidadPorConcepto", ResourceType = typeof(IcapWeb))]
		double CantidadPorConcepto
		{
			get;
			set;
		}
		[Display(Name = "IOTConceptosEjecucionCantidadEjecutada", ResourceType = typeof(IcapWeb))]
		double CantidadEjecutada
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesInsumos2Costo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesInsumos2Costo", ResourceType = typeof(IcapWeb))]
		double Costo
		{
			get;
			set;
		}
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		string Codigo
		{
			get;
			set;
		}
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		/// <summary>
		/// Especifica la unidad de medida
		/// </summary>
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			get;
			set;
		}

		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		string CodigoMoneda
		{
			get;
			set;
		}
		int IdMoneda
		{
			set;
			get;
		}
		bool EsAdicional
		{
			get;
			set;
		}
		string MotivoModificacion
		{
			set;
			get;
		}
		double CostoOriginal
		{
			set;
			get;
		}
		double ValorTipoCambio
		{
			set;
			get;
		}
		string Monedas
		{
			set;
			get;
		}
		DateTime Fecha
		{
			set;
			get;
		}
		[Display(Name = "IPedidosPedido", ResourceType = typeof(IcapWeb))]
		int? IdPedido
		{
			set;
			get;
		}
		[Display(Name = "IMovimientosMovimiento", ResourceType = typeof(IcapWeb))]
		bool? Movimiento
		{
			set;
			get;
		}
		bool? Pedido
		{
			set;
			get;
		}
		[Display(Name = "IContratosDetallesInsumos2CantidadSolicitada", ResourceType = typeof(IcapWeb))]
		double CantidadSolicitada	{set;get;}
		[Display(Name = "IContratosDetallesInsumos2CantidadEnviada", ResourceType = typeof(IcapWeb))]
		double CantidadEnviada	{set;get;}
		[Display(Name = "IContratosDetallesInsumos2CantidadRecibida", ResourceType = typeof(IcapWeb))]
		double CantidadRecibida	{set;get;}
		[Display(Name = "IContratosDetallesInsumos2SalidaAlmacen", ResourceType = typeof(IcapWeb))]
		double SalidaAlmacen
		{
			set;
			get;
		}
		string ToXml();
	}// IContratosDetallesInsumos2 ends.
}// LibEntidades.Entidades ends.
