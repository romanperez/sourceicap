using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{
	public partial interface IvwContratos2 : IContratos2,IEntidad
	{		
		
		[Display(Name = "IContratos2SubTotal", ResourceType = typeof(IcapWeb))]
		double SubTotalOriginal
		{
			get;
			set;
		}		
		[Display(Name = "IContratos2Total", ResourceType = typeof(IcapWeb))]
		double TotalOriginal
		{
			get;
			set;
		}

		double ValorTipoCambio
		{
			get;
			set;
		}

		string Monedas
		{
			get;
			set;
		}		
	}// IContratos2 ends.
}// LibEntidades.Entidades ends.
