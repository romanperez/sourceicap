using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/03/2017 03:46:54 p. m.*/
namespace Entidades
{
	public partial interface IProyectoContratos : IEntidad
	{
		int IdProyectoContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectoContratosIdProyecto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectoContratosIdProyecto", ResourceType = typeof(IcapWeb))]
		int IdProyecto
		{
			get;
			set;
		}
		int IdCliente
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectoContratosIdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectoContratosIdContrato", ResourceType = typeof(IcapWeb))]
		int IdContrato
		{
			get;
			set;
		}
		[Display(Name = "IProyectoContratosIdContrato", ResourceType = typeof(IcapWeb))]
		string Contrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIProyectoContratosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IProyectoContratosEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		string RazonSocial
		{
			set;
			get;
		}
		string Empresa
		{
			set;
			get;
		}
		string Unidad
		{
			set;
			get;
		}

	}// IProyectoContratos ends.
}// LibEntidades.Entidades ends.
