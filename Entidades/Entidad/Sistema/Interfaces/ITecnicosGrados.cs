using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:39 p. m.*/
namespace Entidades
{
	public partial interface ITecnicosGrados : IEntidad
	{
		 [EntityTable(IsPrimary = true)]
		int IdGrado
		{
			get;
			set;
		}
		[Display(Name = "IEspecialidadesIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITecnicosGradosCodigo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITecnicosGradosCodigo", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Codigo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITecnicosGradosNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITecnicosGradosNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITecnicosGradosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITecnicosGradosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
	}// ITecnicosGrados ends.
}// LibEntidades.Entidades ends.
