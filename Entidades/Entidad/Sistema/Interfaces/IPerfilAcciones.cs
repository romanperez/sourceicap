using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 04/01/2017 10:37:31 a. m.*/
namespace Entidades
{
	public partial interface IPerfilAcciones : IEntidad
	{
		int IdPerfilAcciones
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilAccionesIdAccion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilAccionesIdAccion", ResourceType = typeof(IcapWeb))]
		int IdAccion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilAccionesIdPerfil", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilAccionesIdPerfil", ResourceType = typeof(IcapWeb))]
		int IdPerfil
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilAccionesIdMenu", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilAccionesIdMenu", ResourceType = typeof(IcapWeb))]
		int IdMenu
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilAccionesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilAccionesEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		string AccionNombre
		{
			get;
			set;
		}

		string ParametrosJs
		{
			get;
			set;
		}

		string ParametrosMvc
		{
			get;
			set;
		}

		string Html
		{
			get;
			set;
		}

	}// IPerfilAcciones ends.
}// LibEntidades.Entidades ends.