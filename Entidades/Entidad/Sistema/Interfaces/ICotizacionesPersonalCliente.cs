using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 02/06/2017 11:10:18 a. m.*/
namespace Entidades
{
	public partial interface ICotizacionesPersonalCliente : IEntidad
	{
		int IdCotizacionPersonalCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesPersonalClienteIdCotizacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesPersonalClienteIdCotizacion", ResourceType = typeof(IcapWeb))]
		int IdCotizacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesPersonalClienteIdEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesPersonalClienteIdEstatus", ResourceType = typeof(IcapWeb))]
		int IdEstatus
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesPersonalClienteIdPersonalCliente", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesPersonalClienteIdPersonalCliente", ResourceType = typeof(IcapWeb))]
		int IdPersonalCliente
		{
			get;
			set;
		}
		[Display(Name = "IClientesPersonalNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Display(Name = "IClientesPersonalApellidoPaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoPaterno
		{
			get;
			set;
		}
		[Display(Name = "IClientesPersonalApellidoMaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoMaterno
		{
			get;
			set;
		}
		[Display(Name = "IClientesPersonalPuesto", ResourceType = typeof(IcapWeb))]
		string Puesto
		{
			get;
			set;
		}
		[Display(Name = "IEstatusNombre", ResourceType = typeof(IcapWeb))]
		string NombreEstatus
		{
			get;
			set;
		}
		string NombreCompleto
		{
			get;
		}
	}// ICotizacionesPersonalCliente ends.
}// LibEntidades.Entidades ends.