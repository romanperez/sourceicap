using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:36 p. m.*/
namespace Entidades
{
	public partial interface IMovimientosDetalles : IEntidad
	{
		int IdMovimientoDetalle
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosDetallesIdMovimiento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosDetallesIdMovimiento", ResourceType = typeof(IcapWeb))]
		int IdMovimiento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosDetallesIdInsumo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosDetallesIdInsumo", ResourceType = typeof(IcapWeb))]
		int IdInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosDetallesCantidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosDetallesCantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}
		[Display(Name = "IMovimientosDetallesCantidad", ResourceType = typeof(IcapWeb))]
		double CantidadExistenciaInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosDetallesPrecio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosDetallesPrecio", ResourceType = typeof(IcapWeb))]
		double Precio
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosDetallesObservacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosDetallesObservacion", ResourceType = typeof(IcapWeb))]
		string Observacion
		{
			get;
			set;
		}
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		string Codigo
		{
			set;
			get;
		}
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			set;
			get;
		}
		[Display(Name = "IInsumosSerie", ResourceType = typeof(IcapWeb))]		
		string Serie
		{
			get;
			set;
		}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		string CodigoMoneda
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesMedidaDescripcion", ResourceType = typeof(IcapWeb))]
		string UnidadMedidaDescripcion
		{
			set;
			get;
		}
		int IdPedidoDetalle
		{
			set;
			get;
		}
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]		
		string Concepto
		{
			get;
			set;
		}
	}// IMovimientosDetalles ends.
}// LibEntidades.Entidades ends.
