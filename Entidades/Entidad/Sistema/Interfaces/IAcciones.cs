using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 04/01/2017 10:37:31 a. m.*/
namespace Entidades
{
	public partial interface IAcciones : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdAccion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIAccionesAccionNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IAccionesAccionNombre", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string AccionNombre
		{
			get;
			set;
		}
		[Display(Name = "IAccionesParametrosJs", ResourceType = typeof(IcapWeb))]
		string ParametrosJs
		{
			get;
			set;
		}
		[Display(Name = "IAccionesParametrosMvc", ResourceType = typeof(IcapWeb))]
		string ParametrosMvc
		{
			get;
			set;
		}
		[Display(Name = "IAccionesHtml", ResourceType = typeof(IcapWeb))]
		string Html
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIAccionesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IAccionesEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		/// <summary>
		/// Indica la lista de menus a los cuales solo se mostrara esta accion.
		/// </summary>
		[Display(Name = "IAccionesMenuIncluir", ResourceType = typeof(IcapWeb))]
		string MenuIncluir
		{
			get;
			set;
		}
		/// <summary>
		/// Indica la lista de menus los cuales no se mostrara la accion.
		/// </summary>
		[Display(Name = "IAccionesMenuOmitir", ResourceType = typeof(IcapWeb))]
		string MenuOmitir
		{
			get;
			set;
		}
	}// IAcciones ends.
}// LibEntidades.Entidades ends.
