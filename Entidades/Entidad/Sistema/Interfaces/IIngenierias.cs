using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:35 p. m.*/
namespace Entidades
{
	public partial interface IIngenierias : IEntidad
	{
		int IdIngenieria
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIngenieriasIdProyecto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIngenieriasIdProyecto", ResourceType = typeof(IcapWeb))]
		int IdProyecto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIngenieriasIngenieria", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIngenieriasIngenieria", ResourceType = typeof(IcapWeb))]
		string Ingenieria
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIngenieriasFecha", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIngenieriasFecha", ResourceType = typeof(IcapWeb))]
		DateTime Fecha
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIngenieriasEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIngenieriasEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		string Proyecto
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			get;
			set;
		}	  
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
	}// IIngenierias ends.
}// LibEntidades.Entidades ends.
