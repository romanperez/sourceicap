using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface IPermisos : IEntidad
	{
		List<CMenus> Menus
		{
			set;
			get;
		}
		List<CPerfiles> Perfiles
		{
			set;
			get;
		}
		List<CAcciones> Acciones
		{
			set;
			get;
		}
	}// IPerfiles ends.
}// LibEntidades.Entidades ends.
