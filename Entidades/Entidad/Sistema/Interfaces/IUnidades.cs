using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{
	public partial interface IUnidades : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdUnidad
		{
			get;
			set;
		}
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIUnidadesUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Unidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIUnidadesDireccion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IUnidadesDireccion", ResourceType = typeof(IcapWeb))]
		string Direccion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIUnidadesTelefono", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IUnidadesTelefono", ResourceType = typeof(IcapWeb))]
		string Telefono
		{
			get;
			set;
		}		
		[Display(Name = "IUnidadesEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		/*CEmpresas Empresa
		{
			get;
			set;
		}*/

	}// IUnidades ends.
}// LibEntidades.Entidades ends.
