using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:37 p. m.*/
namespace Entidades
{
	public partial interface IOTPersonal : IEntidad
	{
		int IdOTPersonal
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalIdOrdenTrabajo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalIdOrdenTrabajo", ResourceType = typeof(IcapWeb))]
		int IdOrdenTrabajo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalIdTecnico", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalIdTecnico", ResourceType = typeof(IcapWeb))]
		int IdTecnico
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIOTPersonalEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IOTPersonalEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IUsuariosNombreEmpleado", ResourceType = typeof(IcapWeb))]
		string NombreEmpleado
		{
			set;
			get;
		}
		[Display(Name = "IEspecialidadesNombre", ResourceType = typeof(IcapWeb))]
		string NombreEspecialidad
		{
			get;
			set;
		}
		[Display(Name = "ITecnicosGradosNombre", ResourceType = typeof(IcapWeb))]
		string NombreGrado
		{
			get;
			set;
		}

	}// IOTPersonal ends.
}// LibEntidades.Entidades ends.
