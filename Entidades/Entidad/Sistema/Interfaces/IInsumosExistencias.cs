using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:35 p. m.*/
namespace Entidades
{
	public partial interface IInsumosExistencias : IEntidad
	{
		int IdExistencia
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosExistenciasIdAlmacen", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosExistenciasIdAlmacen", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdAlmacen
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosExistenciasIdInsumo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosExistenciasIdInsumo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosExistenciasCantidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosExistenciasCantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIInsumosExistenciasEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IInsumosExistenciasEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}		
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="Insumos.Codigo",OmiteConcatenarTabla=true)]
		string Codigo
		{
			get;
			set;
		}
		[Display(Name = "IInsumosSerie", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Insumos.Serie", OmiteConcatenarTabla = true)]
		string Serie
		{
			get;
			set;
		}
		
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Insumos.Descripcion", OmiteConcatenarTabla = true)]
		string Descripcion
		{
			get;
			set;
		}
		[Display(Name = "IAlmacenesAlmacen", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="Almacenes.Almacen" ,OmiteConcatenarTabla=true)]
		string Almacen
		{
			get;
			set;
		}
        [Display(Name = "IInsumosUltimoMovimiento", ResourceType = typeof(IcapWeb))]
		DateTime UltimoMovimiento
		{
			set;
			get;
		}
		double Costo
		{
			set;
			get;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Empresa
		{
			get;
			set;
		}		
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Unidades.Unidad", OmiteConcatenarTabla = true)]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Monedas.Moneda", OmiteConcatenarTabla = true)]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string CodigoMoneda
		{
			get;
			set;
		}
	}// IInsumosExistencias ends.
}// LibEntidades.Entidades ends.
