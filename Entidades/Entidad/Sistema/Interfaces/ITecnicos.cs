using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:39 p. m.*/
namespace Entidades
{
	public partial interface ITecnicos : IEntidad
	{
		int IdTecnico
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITecnicosIdEmpleado", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITecnicosIdEmpleado", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpleado
		{
			get;
			set;
		}
		//[Required(ErrorMessageResourceName = "RequiredITecnicosIdEspecialidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITecnicosIdEspecialidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEspecialidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITecnicosIdGrado", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITecnicosIdGrado", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdGrado
		{
			get;
			set;
		}

		[Display(Name = "IUsuariosNombreEmpleado", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string NombreEmpleado
		{
			set;
			get;
		}		
		[Display(Name = "IEspecialidadesNombre", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string NombreEspecialidad
		{
			get;
			set;
		}
		[Display(Name = "ITecnicosGradosNombre", ResourceType = typeof(IcapWeb))]
		string NombreGrado
		{
			get;
			set;
		}
		int IdEmpresa
		{
			get;
			set;
		}		
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		List<CEspecialidades> Especialidades
		{
			get;
			set;
		}
	}// ITecnicos ends.
}// LibEntidades.Entidades ends.
