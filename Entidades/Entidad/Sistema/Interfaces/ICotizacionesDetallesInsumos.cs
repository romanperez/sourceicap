using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 21/02/2017 09:48:11 a. m.*/
namespace Entidades
{
    public partial interface ICotizacionesDetallesInsumos:IEntidad
    {                
      int IdInsumoCotizacion { get;set; }
		int IdCotizacion { get;set; }
		int IdConcepto { get;set; }
		int IdInsumo { get;set; }
		double Cantidad { get;set; }
		[Display(Name = "ICotizacionesDetallesInsumosCosto", ResourceType = typeof(IcapWeb))]
		 double Costo { get;set; }
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		string Codigo
		{
			get;
			set;
		}
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}

		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			get;
			set;
		}		
		int IdCapitulo
		{
			set;
			get;
		}
		int IdMoneda
		{
			set;
			get;
		}
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			set;
			get;
		}
		double CostoOriginal
		{
			set;
			get;
		}
		double ValorTipoCambio
		{
			set;
			get;
		}
		string Monedas
		{
			set;
			get;
		}
		DateTime Fecha
		{
			set;
			get;
		}
    }// ICotizacionesDetallesInsumos ends.
}// LibEntidades.Entidades ends.
