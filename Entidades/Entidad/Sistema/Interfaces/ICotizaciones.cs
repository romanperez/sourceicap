using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:33 p. m.*/
namespace Entidades
{
	public partial interface ICotizaciones : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdCotizacion
		{
			get;
			set;
		}
		[Display(Name = "IUsuariosUsuario", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdUsuario
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesMoneda", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdMoneda
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesCotizacion", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Cotizacion
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesTotal", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesTotal", ResourceType = typeof(IcapWeb))]
		double Total
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesPorcentajeDescuento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesPorcentajeDescuento", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		double PorcentajeDescuento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesDescuentoMonto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesDescuentoMonto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		double DescuentoMonto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesIdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesIdEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesIdCliente", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesIdCliente", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesIdUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesIdUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdUnidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesIdIva", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesIdIva", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdIva
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICotizacionesFecha", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesFecha", ResourceType = typeof(IcapWeb))]
		DateTime Fecha
		{
			get;
			set;
		}

		[Required(ErrorMessageResourceName = "RequiredICotizacionesExito", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesExito", ResourceType = typeof(IcapWeb))]
		[Range(1, 100, ErrorMessageResourceName = "ICotizacionesRangoValidoExito", ErrorMessageResourceType = typeof(IcapWeb))]
		double Exito
		{
			get;
			set;
		}

		[Required(ErrorMessageResourceName = "RequiredICotizacionesSubTotal", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICotizacionesSubTotal", ResourceType = typeof(IcapWeb))]
		double SubTotal
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesIva", ResourceType = typeof(IcapWeb))]
		double Iva
		{
			get;
			set;
		}
		[Display(Name = "IClientesRazonSocial", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Clientes.RazonSocial", OmiteConcatenarTabla = true)]
		string RazonSocial
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string Empresa
		{
			get;
			set;
		}
		int? IdTipoCambio
		{
			set;
			get;
		}
		[Display(Name = "ITipoCambiosValor", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		double Valor
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Unidades.Unidad", OmiteConcatenarTabla = true)]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Monedas.Moneda", OmiteConcatenarTabla = true)]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string CodigoMoneda
		{
			get;
			set;
		}
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Proyectos.Proyecto", OmiteConcatenarTabla = true)]
		string Proyecto
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesUtilidad", ResourceType = typeof(IcapWeb))]
		double? Utilidad
		{
			set;
			get;
		}
		List<CCotizacionesDetalles> Conceptos
		{
			set;
			get;
		}
		List<CCotizacionesDetallesInsumos> Insumos
		{
			set;
			get;
		}
		bool IsCopy
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesUsuarioCotizo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		CUsuarios Usuario
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesSolicitud", ResourceType = typeof(IcapWeb))]
		string Solicitud
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesProyectoInicial", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string ProyectoInicial
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesDescripcionTitulo", ResourceType = typeof(IcapWeb))]		
		string DescripcionTitulo
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesDescripcionCompleta", ResourceType = typeof(IcapWeb))]
		string DescripcionCompleta
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesDepartamentoAtencion", ResourceType = typeof(IcapWeb))]		
		string DepartamentoAtencion
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesFechaEntrega", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string FechaEntrega
		{
			set;
			get;
		}
		List<CCotizacionesPersonalCliente> PersonalClienteAtencion
		{
			set;
			get;
		}
		List<CCotizacionesPersonalCliente> PersonalClienteContacto
		{
			set;
			get;
		}
		string ListClienteAtencion
		{
			set;
			get;
		}
		string ListClienteContacto
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesSubTotalOriginal", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		double SubTotalOriginal	
		{
			set;get;
		}
		[Display(Name = "ICotizacionesTotalOriginal", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
	   double TotalOriginal
		{
			set;get;
		}
		double ValorTipoCambio
		{
			get;
			set;
		}

		string Monedas
		{
			get;
			set;
		}		
	}// ICotizaciones ends.
}// LibEntidades.Entidades ends.
