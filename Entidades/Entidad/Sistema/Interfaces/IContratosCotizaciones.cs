using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 19/06/2017 04:59:59 p. m.*/
namespace Entidades
{
	public partial interface IContratosCotizaciones : IEntidad
	{
		int IdContratoCotizacion
		{
			get;
			set;
		}
		int IdContrato
		{
			get;
			set;
		}
		int IdCotizacion
		{
			get;
			set;
		}
		[Display(Name = "IContratosCotizacionesFecha", ResourceType = typeof(IcapWeb))]
		DateTime Fecha
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesCotizacion", ResourceType = typeof(IcapWeb))]
		string Cotizacion
		{
			set;
			get;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]		
		string Contrato
		{
			set;
			get;
		}
	}// IContratosCotizaciones ends.
}// LibEntidades.Entidades ends.
