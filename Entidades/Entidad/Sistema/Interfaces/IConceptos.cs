using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:32 p. m.*/
namespace Entidades
{
	public partial interface IConceptos : IEntidad
	{
	   [EntityTable(IsPrimary=true)]
		int IdConcepto
		{
			get;
			set;
		}
		[Display(Name = "IConceptosIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true,ShowResult=false)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosIdUnidadMedida", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		int? IdUnidadMedida 
		{
			get;
			set;
		}
		[Display(Name = "IClasificacionConceptosClasificacion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int? IdConceptoClasificacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true,ShowResult=true)]
		string Concepto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosDescripcion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConceptosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConceptosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConceptosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		int? IdCapitulo
		{
			set;get;
		}
		double Precio
		{
			set;
			get;
		}
		[Display(Name = "IConceptosInsumos", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		List<CConceptosInsumos> Insumos
		{
			set;
			get;
		}
		[Display(Name = "ICapitulosCapitulo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo ="Capitulos.Capitulo",OmiteConcatenarTabla=true)]
		string Capitulo
		{
			get;
			set;
		}
		int IdMoneda
		{
			set;
			get;
		}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string CodigoMoneda
		{
			get;
			set;
		}
		[Display(Name = "IConceptosIdUnidadMedida", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="UnidadesMedida.Unidad",OmiteConcatenarTabla = true)]
		string UnidadMedida
		{
			get;
			set;
		}
		[Display(Name = "IClasificacionConceptosClasificacion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Clasificacion
		{
			get;
			set;
		}
		[Display(Name = "IConceptosFecha", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		DateTime Fecha
		{
			get;
			set;
		}
		string ToXml();
		bool EsValido
		{
			set;
			get;
		}
		string MotivoNoValido
		{
			set;
			get;
		}
	}// IConceptos ends.
}// LibEntidades.Entidades ends.
