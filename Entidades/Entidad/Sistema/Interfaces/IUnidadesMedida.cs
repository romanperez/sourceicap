using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:39 p. m.*/
namespace Entidades
{
	public partial interface IUnidadesMedida : IEntidad
	{
		 [EntityTable(IsPrimary = true)]
		int IdUnidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIUnidadesMedidaUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IUnidadesMedidaUnidad", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Unidad
		{
			get;
			set;
		}		
		[Display(Name = "IUnidadesMedidaDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIUnidadesMedidaEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IUnidadesMedidaEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}

	}// IUnidadesMedida ends.
}// LibEntidades.Entidades ends.
