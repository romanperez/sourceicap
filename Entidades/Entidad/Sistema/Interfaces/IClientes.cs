using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 15/11/2016 04:27:39 p. m.*/
namespace Entidades
{
    public partial interface IClientes:IEntidad
    {                
      int IdCliente { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIEmpresasEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
		int IdUnidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesUnidad", ResourceType = typeof(IcapWeb))]		
		string Unidad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesRazonSocial", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesRazonSocial", ResourceType = typeof(IcapWeb))]  
		string RazonSocial { get;set; }

		[Required(ErrorMessageResourceName = "RequiredIClientesNombreComercial", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesNombreComercial", ResourceType = typeof(IcapWeb))]  
		string NombreComercial { get;set; }

		[Required(ErrorMessageResourceName = "IEmpresasRfcLength", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesRfc", ResourceType = typeof(IcapWeb))]
		[StringLength(CEmpresas.LEN_FISICA,
						 MinimumLength = CEmpresas.LEN_MORAL,
						 ErrorMessageResourceName = "IEmpresasRfcLength", ErrorMessageResourceType = typeof(IcapWeb))]	
		string Rfc { get;set; }

		[Display(Name = "IClientesTelefono", ResourceType = typeof(IcapWeb))]  
		string Telefono { get;set; }

		[Display(Name = "IClientesDireccion", ResourceType = typeof(IcapWeb))]  
		string Direccion { get;set; }

		[Display(Name = "IClientesCodigoPostal", ResourceType = typeof(IcapWeb))]
		[StringLength(CEmpresas.LENGTH_CP,
						 MinimumLength = CEmpresas.LENGTH_CP,
						 ErrorMessageResourceName = "IEmpresasCodigoPostalLength", ErrorMessageResourceType = typeof(IcapWeb))]	
		int CodigoPostal { get;set; }

		bool Estatus { get;set; }
                    
    }// IClientes ends.
}// LibEntidades.Entidades ends.
