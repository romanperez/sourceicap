using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 26/04/2017 01:51:06 p. m.*/
namespace Entidades
{
	public partial interface IOTContratos : IEntidad
	{
		int IdContratoOrden
		{
			get;
			set;
		}
		int IdOrdenTrabajo
		{
			get;
			set;
		}
		int IdContrato
		{
			get;
			set;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		string Contrato
		{
			get;
			set;
		}
	}// IOTContratos ends.
}// LibEntidades.Entidades ends.
