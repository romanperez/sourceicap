﻿using System;
namespace Entidades
{
	public interface IIdioma
	{
		string Codigo
		{
			get;
			set;
		}
		bool Estatus
		{
			get;
			set;
		}
		int IdIdioma
		{
			get;
			set;
		}
		string Idioma
		{
			get;
			set;
		}
	}
}
