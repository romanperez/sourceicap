using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:35 p. m.*/
namespace Entidades
{
	public partial interface IIvaValores : IEntidad
	{
		int IdIva
		{
			get;
			set;
		}
		int IdEmpresa
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredIIvaValoresPorcentaje", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIvaValoresPorcentaje", ResourceType = typeof(IcapWeb))]
		double Porcentaje
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIvaValoresFechaInicio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIvaValoresFechaInicio", ResourceType = typeof(IcapWeb))]
		DateTime FechaInicio
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIvaValoresFechaFin", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIvaValoresFechaFin", ResourceType = typeof(IcapWeb))]
		DateTime FechaFin
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIIvaValoresEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IIvaValoresEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
	}// IIvaValores ends.
}// LibEntidades.Entidades ends.
