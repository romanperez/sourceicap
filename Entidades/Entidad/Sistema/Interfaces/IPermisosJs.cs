using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 04/01/2017 10:37:31 a. m.*/
namespace Entidades
{
	public interface IPermisosJs : IEntidad
	{
		int IdPerfil
		{
			set;
			get;
		}
		string[] MenuVer
		{
			set;
			get;
		}
		string[] MenuPermisos
		{
			set;
			get;
		}
	}
}// LibEntidades.Entidades ends.
