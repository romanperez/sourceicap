using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/01/2017 02:07:42 p. m.*/
namespace Entidades
{
	public partial interface ITipoCambios : IEntidad
	{
		int IdTipoCambio
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosIdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosIdEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosIdMonedaOrigen", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosIdMonedaOrigen", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdMonedaOrigen
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosIdMonedaDestino", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosIdMonedaDestino", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdMonedaDestino
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosFechaInicio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosFechaInicio", ResourceType = typeof(IcapWeb))]
		DateTime FechaInicio
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosFechaFin", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosFechaFin", ResourceType = typeof(IcapWeb))]
		DateTime FechaFin
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosValor", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosValor", ResourceType = typeof(IcapWeb))]
		double Valor
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredITipoCambiosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ITipoCambiosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		[Display(Name = "ITipoCambiosMonedaOrigen", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="M1.Moneda",OmiteConcatenarTabla=true)]
		string MonedaOrigen
		{
			get;
			set;
		}
		[Display(Name = "ITipoCambiosMonedaDestino", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "M2.Moneda", OmiteConcatenarTabla = true)]
		string MonedaDestino
		{
			set;
			get;
		}
		string CodigoMonedaOrigen
		{
			get;
			set;
		}		
		string CodigoMonedaDestino
		{
			set;
			get;
		}
		double Cantidad
		{
			set;
			get;
		}
	}// ITipoCambios ends.
}// LibEntidades.Entidades ends.
