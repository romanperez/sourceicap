using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/01/2017 02:07:42 p. m.*/
namespace Entidades
{
	public partial interface ICapitulos : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdCapitulo
		{
			get;
			set;
			
		}
		[Required(ErrorMessageResourceName = "RequiredICapitulosIdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICapitulosIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpresa
		{
			get;
			set;
		}		
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICapitulosCapitulo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICapitulosCapitulo", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Capitulo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredICapitulosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "ICapitulosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}

	}// ICapitulos ends.
}// LibEntidades.Entidades ends.
