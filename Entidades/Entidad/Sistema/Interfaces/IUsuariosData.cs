using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public partial interface IUsuariosData
	{
		CEmpleados Empleado
		{
			set;
			get;
		}
		CPerfiles Perfil
		{
			set;
			get;
		}
		CDepartamentos Departamento
		{
			set;
			get;
		}
		CUnidades Unidad
		{
			set;
			get;
		}
		CEmpresas Empresa
		{
			set;
			get;
		}
	}// IUsuarios ends.
}// LibEntidades.Entidades ends.
