using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:33 p. m.*/
namespace Entidades
{
	public partial interface IContratosDetalles : IEntidad
	{
		int IdContratoDetalle
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesIdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesIdContrato", ResourceType = typeof(IcapWeb))]
		int IdContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesIdConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesIdConcepto", ResourceType = typeof(IcapWeb))]
		int IdConcepto
		{
			get;
			set;
		}
		
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesPartida", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesPartida", ResourceType = typeof(IcapWeb))]
		int Partida
		{
			get;
			set;
		}
		
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesPrecio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetallesPrecio", ResourceType = typeof(IcapWeb))]
		double Precio
		{
			get;
			set;
		}
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		string Concepto
		{
			get;
			set;
		}
		[Display(Name = "IConceptosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Display(Name = "ICapitulosCapitulo", ResourceType = typeof(IcapWeb))]
		string Capitulo
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesMoneda", ResourceType = typeof(IcapWeb))]
		int IdMoneda
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			set;
			get;
		}
	}// IContratosDetalles ends.
}// LibEntidades.Entidades ends.
