using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:36 p. m.*/
namespace Entidades
{
	public partial interface IMovimientos : IEntidad
	{
		[Display(Name = "IMovimientosMovimiento", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdMovimiento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosTipo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosTipo", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int Tipo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosFecha", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosFecha", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		DateTime Fecha
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosIdAlmacen", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosIdAlmacen", ResourceType = typeof(IcapWeb))]
		[Range(1,int.MaxValue)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdAlmacen
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosIdAlmacenDestino", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosIdAlmacenDestino", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int? IdAlmacenDestino
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosIdUsuario", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosIdUsuario", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdUsuario
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMovimientosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMovimientosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IAlmacenesIdUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdUnidad
		{
			get;
			set;
		}
		List<CMovimientosDetalles> DetalleMovimientos
		{
			set;
			get;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="Unidades.Unidad",OmiteConcatenarTabla = true)]
		string Unidad
		{
			set;
			get;
		}
		//
		[Display(Name = "IAlmacenesIdUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		int IdUnidadDestino
		{
			get;
			set;
		}		
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string UnidadDestino
		{
			set;
			get;
		}
		//
		[Display(Name = "IUsuariosUsuario", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Usuarios.Usuario", OmiteConcatenarTabla = true)]
		string Usuario
		{
			set;
			get;
		}	
		[Display(Name = "IAlmacenesAlmacen", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Almacenes.Almacen", OmiteConcatenarTabla = true)]
		string Almacen
		{
			get;
			set;
		}
		[Display(Name = "IAlmacenesAlmacen", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo=true)]
		string AlmacenDestino
		{
			get;
			set;
		}
		int IdContrato
		{
			set;
			get;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Contratos2.Contrato", OmiteConcatenarTabla = true)]
		string Contrato
		{
			get;
			set;
		}
		int IdProyecto
		{
			set;
			get;
		}
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Proyecto
		{
			set;
			get;
		}
		bool TieneSolicituInsumoAsociada
		{
			set;
			get;
		}
	}// IMovimientos ends.
}// LibEntidades.Entidades ends.
