using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{
    public partial interface IDepartamentos:IEntidad
    {
		 [EntityTable(IsPrimary = true)]
		 int IdDepartamento
		 {
			 get;
			 set;
		 }		 
		 int IdEmpresa
		 {
			 get;
			 set;
		 }		 
		 [Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		 string Empresa
		 {
			 get;
			 set;
		 }
		 [EntityTable(IsUnique = true, ShowResult = false)]
		 int IdUnidad
		 {
			 get;
			 set;
		 }
		  [Display(Name = "IDepartamentosIdUnidad", ResourceType = typeof(IcapWeb))]
		 string Unidad
		 {
			 get;
			 set;
		 }
		 [Required(ErrorMessageResourceName = "RequiredIDepartamentosCodigo", ErrorMessageResourceType = typeof(IcapWeb))]
		 [Display(Name = "IDepartamentosCodigo", ResourceType = typeof(IcapWeb))]
		 [EntityTable(IsUnique = true, ShowResult = true)]
		 string Codigo
		 {
			 get;
			 set;
		 }
		 [Required(ErrorMessageResourceName = "RequiredIDepartamentosNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		 [Display(Name = "IDepartamentosNombre", ResourceType = typeof(IcapWeb))]
		 string Nombre
		 {
			 get;
			 set;
		 }
		 [Display(Name = "IDepartamentosEstatus", ResourceType = typeof(IcapWeb))]
		 bool Estatus
		 {
			 get;
			 set;
		 }		            
    }// IDepartamentos ends.
}// LibEntidades.Entidades ends.
