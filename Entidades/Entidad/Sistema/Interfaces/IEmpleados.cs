using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{
	public partial interface IEmpleados : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdEmpleado
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpleadosIdDepartamento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpleadosIdDepartamento", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdDepartamento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpleadosNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "NameIEmpleadosNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpleadosApellidoPaterno", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "NameIEmpleadosApellidoPaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoPaterno
		{
			get;
			set;
		}
		[Display(Name = "NameIEmpleadosApellidoMaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoMaterno
		{
			get;
			set;
		}
		[Display(Name = "NameIEmpleadosApellidoMatricula", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Matricula
		{
			get;
			set;
		}
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpleadosIdDepartamento", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="Departamentos.Nombre",OmiteConcatenarTabla=true)]
		//[Inner(campo = "Departamentos.Nombre")]
		string DepartamentoNombre
		{
			get;
			set;
		}
		int IdEmpresa
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		int IdUnidad
		{
			get;
			set;
		}
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo ="Unidades.Unidad",OmiteConcatenarTabla=true)]
		string Unidad
		{
			get;
			set;
		}
		int IdTecnico
		{
			set;
			get;
		}
		[Display(Name = "IEspecialidadesGrado", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdGrado
		{
			set;
			get;
		}
		[Display(Name = "IEspecialidadesGrado", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Grado
		{
			set;
			get;
		}
		string Especialidades
		{
			set;
			get;
		}
		string NombreCompleto();
		string NombreCompletoPerfil(IPerfiles perfil);
		[Display(Name = "IEmpleadosEmail", ResourceType = typeof(IcapWeb))]
		string Email
		{
			set;
			get;
		}
		[Display(Name = "IEmpleadosTelefono", ResourceType = typeof(IcapWeb))]
		string Telefono
		{
			set;
			get;
		}
		[Display(Name = "IEmpleadosCelular", ResourceType = typeof(IcapWeb))]
		string Celular
		{
			set;
			get;
		}

	}// IEmpleados ends.
}// LibEntidades.Entidades ends.
