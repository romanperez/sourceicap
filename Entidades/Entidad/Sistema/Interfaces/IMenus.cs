using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
namespace Entidades
{
	public partial interface IMenus : IEntidad
	{
		[Display(Name = "IMenusIdMenu", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsPrimary = true)]
		int IdMenu
		{
			get;
			set;
		}
		[Display(Name = "IMenusIdMenuPadre", ResourceType = typeof(IcapWeb))]
		int IdMenuPadre
		{
			get;
			set;
		}
		[Display(Name = "IMenusIdMenu", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Menu
		{
			get;
			set;
		}
		[Display(Name = "IMenusTitulo", ResourceType = typeof(IcapWeb))]
		string Titulo
		{
			get;
			set;
		}
		[Display(Name = "IMenusImagen", ResourceType = typeof(IcapWeb))]
		string Imagen
		{
			get;
			set;
		}
		string ImagenTab
		{
			get;
			set;
		}
		[Display(Name = "IMenusUrl", ResourceType = typeof(IcapWeb))]
		string Url
		{
			get;
			set;
		}
		[Display(Name = "IMenusOrden", ResourceType = typeof(IcapWeb))]
		int Orden
		{
			get;
			set;
		}
		[Display(Name = "IMenusParametros", ResourceType = typeof(IcapWeb))]
		string Parametros
		{
			get;
			set;
		}
		[Display(Name = "IMenusEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		List<CMenus> SubMenus
		{
			get;
			set;
		}
		void SetSubMenus(IEnumerable<IMenus> menus);
		bool HasItems
		{
			get;
		}
		IEnumerable<IMenus> GetSubMenus();
	}// IMonedas ends.
}// LibEntidades.Entidades ends.
