using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface IPedidos : IEntidad
	{
		[Display(Name = "IPedidosPedido", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdPedido
		{
			get;
			set;
		}
		int? IdProyecto
		{
			set;get;
		}
		int? IdContrato
		{
			set;
			get;
		}
		int IdUnidad
		{
			get;
			set;
		}
		//[Required(ErrorMessageResourceName = "RequiredIPedidosIdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		//[Display(Name = "IPedidosIdContrato", ResourceType = typeof(IcapWeb))]
		//int IdContrato
		//{
		//	get;
		//	set;
		//}
		/*[Required(ErrorMessageResourceName = "RequiredIPedidosPedido", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosPedido", ResourceType = typeof(IcapWeb))]
		string Pedido
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPedidosDescripcion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}*/
		[Required(ErrorMessageResourceName = "RequiredIPedidosFechaSolicitado", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosFechaSolicitado", ResourceType = typeof(IcapWeb))]
		DateTime FechaSolicitado
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPedidosFechaLlegada", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosFechaLlegada", ResourceType = typeof(IcapWeb))]
		DateTime? FechaLlegada
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPedidosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		[Display(Name = "IDepartamentosIdUnidad", ResourceType = typeof(IcapWeb))]		
		[FiltroBusqueda(Campo = "Unidades.Unidad", OmiteConcatenarTabla = true)]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Proyectos.Proyecto", OmiteConcatenarTabla = true)]
		string Proyecto
		{
			set;
			get;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "Contratos2.Contrato", OmiteConcatenarTabla = true)]
		string Contrato
		{
			get;
			set;
		}
		List<CPedidosDetalles> Detalle
		{
			set;
			get;
		}
	}// IPedidos ends.
}// LibEntidades.Entidades ends.
