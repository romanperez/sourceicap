using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:33 p. m.*/
namespace Entidades
{
	public partial interface IContratos : IEntidad
	{
		int IdContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosIdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosIdEmpresa", ResourceType = typeof(IcapWeb))]
		int IdEmpresa
		{
			get;
			set;
		}
		[Display(Name = "IPedidosPedido", ResourceType = typeof(IcapWeb))]
		int? IdPedido
		{
			get;
			set;
		}
		int IdProyecto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosContrato", ResourceType = typeof(IcapWeb))]
		string Contrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosAprobado", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosAprobado", ResourceType = typeof(IcapWeb))]
		bool Aprobado
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		
		[Display(Name = "IUnidadesUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
		int IdUsuario
		{
			set;
			get;
		}
		[Display(Name = "IProyectosProyecto", ResourceType = typeof(IcapWeb))]
		string Proyecto
		{
			get;
			set;
		}
		[Display(Name = "ICotizacionesMoneda", ResourceType = typeof(IcapWeb))]
		int IdMoneda
		{
			set;
			get;
		}
		[Display(Name = "ICotizacionesMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			set;
			get;
		}
	}// IContratos ends.
}// LibEntidades.Entidades ends.
