using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:32 p. m.*/
namespace Entidades
{
	public partial interface IActividades : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdActividad
		{
			get;
			set;
		}
		[Display(Name = "IActividadesIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		[FiltroBusqueda(ExcluirCampo=true)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIActividadesIdEspecialidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEspecialidad
		{
			get;
			set;
		}
		[Display(Name = "IActividadesEspecialidad", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo="Especialidades.Especialidad",OmiteConcatenarTabla=true)]
		string Especialidad
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredIActividadesActividad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IActividadesActividad", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Actividad
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIActividadesDescripcion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IActividadesDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIActividadesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IActividadesEstatus", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IActividadesProcedimientoSeguridad", ResourceType = typeof(IcapWeb))]
		string ProcedimientoSeguridad
		{
			set;
			get;
		}
		[Display(Name = "IActividadesAnalisisRiesgo", ResourceType = typeof(IcapWeb))]
		string AnalisisRiesgo
		{
			set;
			get;
		}		
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
		string AuxFilesMaps
		{
			get;
			set;
		}
		[Display(Name = "IClasificacionConceptosClasificacion", ResourceType = typeof(IcapWeb))]		
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdConceptoClasificacion
		{
			get;
			set;
		}
		[Display(Name = "IClasificacionConceptosClasificacion", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(Campo = "clasificacionconceptos.Clasificacion", OmiteConcatenarTabla = true)]		
		string Clasificacion
		{
			get;
			set;
		}
	}// IActividades ends.
}// LibEntidades.Entidades ends.
