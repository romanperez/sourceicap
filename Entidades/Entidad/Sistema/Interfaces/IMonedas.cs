using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:36 p. m.*/
namespace Entidades
{
	public partial interface IMonedas : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdMoneda
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMonedasMoneda", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Moneda
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMonedasCodigo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]	
		string Codigo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIMonedasEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IMonedasEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}		
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
	}// IMonedas ends.
}// LibEntidades.Entidades ends.
