using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 06/04/2017 03:52:26 p. m.*/
namespace Entidades
{
	public partial interface IConversiones : IEntidad
	{
		int IdConversion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConversionesIdMonedaOrigen", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConversionesIdMonedaOrigen", ResourceType = typeof(IcapWeb))]
		int IdMonedaOrigen
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConversionesIdMonedaDestino", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConversionesIdMonedaDestino", ResourceType = typeof(IcapWeb))]
		int IdMonedaDestino
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIConversionesOperador", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IConversionesOperador", ResourceType = typeof(IcapWeb))]
		string Operador
		{
			get;
			set;
		}
		[Display(Name = "ITipoCambiosMonedaOrigen", ResourceType = typeof(IcapWeb))]
		string MonedaOrigen
		{
			get;
			set;
		}
		[Display(Name = "ITipoCambiosMonedaDestino", ResourceType = typeof(IcapWeb))]
		string MonedaDestino
		{
			set;
			get;
		}
		string CodigoMonedaOrigen
		{
			get;
			set;
		}
		string CodigoMonedaDestino
		{
			set;
			get;
		}

	}// IConversiones ends.
}// LibEntidades.Entidades ends.
