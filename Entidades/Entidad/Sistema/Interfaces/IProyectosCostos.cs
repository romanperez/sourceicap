using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface IProyectosCostos : IEntidad
	{
		int IdProyecto
		{
			set;
			get;
		}
		int IdMoneda
		{
			set;
			get;
		}
		string Moneda
		{
			set;
			get;
		}
		string Codigo
		{
			set;
			get;
		}
		double Total
		{
			set;
			get;
		}
		string Totals
		{
			set;
			get;
		}
	}// IProyectos ends.
}// LibEntidades.Entidades ends.
