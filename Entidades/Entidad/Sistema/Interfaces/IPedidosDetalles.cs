using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface IPedidosDetalles : IEntidad
	{
		int IdPedidoDetalle
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPedidosDetallesIdPedido", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosDetallesIdPedido", ResourceType = typeof(IcapWeb))]
		int IdPedido
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPedidosDetallesIdInsumo", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosDetallesIdInsumo", ResourceType = typeof(IcapWeb))]
		int IdInsumo
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPedidosDetallesCantidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPedidosDetallesCantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}
		[Display(Name = "IPedidosDetallesCantidadEnviada", ResourceType = typeof(IcapWeb))]
		double? CantidadEnviada
		{
			get;
			set;
		}		
		[Display(Name = "IPedidosDetallesCantidadRecibida", ResourceType = typeof(IcapWeb))]
		double? CantidadRecibida
		{
			get;
			set;
		}
		double CantidadEnviada2
		{
			get;			
		}
		double PorcentajeEnvio
		{
			get;
		}
		[Display(Name = "ITraspasoCantidadTraspasar", ResourceType = typeof(IcapWeb))]
		double CantidadTraspasar
		{
			set;
			get;
		}
		[Display(Name = "IInsumosCodigo", ResourceType = typeof(IcapWeb))]
		string Codigo
		{
			get;
			set;
		}		
		[Display(Name = "IInsumosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Display(Name = "IInsumosSerie", ResourceType = typeof(IcapWeb))]		
		string Serie
		{
			get;
			set;
		}
		[FiltroBusqueda(ExcluirCampo = true)]
		[Display(Name = "IPedidosDetallesUnidadMedidaDescripcion", ResourceType = typeof(IcapWeb))]
		string UnidadMedidaDescripcion
		{
			set;
			get;
		}
		int? IdConcepto
		{
			set;
			get;
		}
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		string Concepto
		{
			get;
			set;
		}
		int IdContrato
		{
			set;
			get;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]		
		string Contrato
		{
			get;
			set;
		}
	}// IPedidosDetalles ends.
}// LibEntidades.Entidades ends.
