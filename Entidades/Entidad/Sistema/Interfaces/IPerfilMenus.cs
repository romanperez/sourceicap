using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 04/01/2017 10:37:32 a. m.*/
namespace Entidades
{
    public partial interface IPerfilMenus:IEntidad
    {                
        int IdPerfilModulo { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIPerfilMenusIdPerfil", ErrorMessageResourceType = typeof(IcapWeb))]
[Display(Name = "IPerfilMenusIdPerfil", ResourceType = typeof(IcapWeb))] 
int IdPerfil { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIPerfilMenusIdMenu", ErrorMessageResourceType = typeof(IcapWeb))]
[Display(Name = "IPerfilMenusIdMenu", ResourceType = typeof(IcapWeb))] 
int IdMenu { get;set; }
		[Required(ErrorMessageResourceName = "RequiredIPerfilMenusEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
[Display(Name = "IPerfilMenusEstatus", ResourceType = typeof(IcapWeb))] 
bool Estatus { get;set; }

    }// IPerfilMenus ends.
}// LibEntidades.Entidades ends.
