using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 10/04/2017 12:10:56 p. m.*/
namespace Entidades
{
	public partial interface IClasificacionConceptos : IEntidad
	{
		[EntityTable(IsPrimary = true)]
		int IdConceptoClasificacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClasificacionConceptosIdEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClasificacionConceptosIdEmpresa", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = false)]
		[FiltroBusqueda(ExcluirCampo = true)]
		int IdEmpresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClasificacionConceptosClasificacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClasificacionConceptosClasificacion", ResourceType = typeof(IcapWeb))]
		[EntityTable(IsUnique = true, ShowResult = true)]
		string Clasificacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClasificacionConceptosDescripicion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClasificacionConceptosDescripicion", ResourceType = typeof(IcapWeb))]
		string Descripicion
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		[FiltroBusqueda(ExcluirCampo = true)]
		string Empresa
		{
			get;
			set;
		}
	}// IClasificacionConceptos ends.
}// LibEntidades.Entidades ends.
