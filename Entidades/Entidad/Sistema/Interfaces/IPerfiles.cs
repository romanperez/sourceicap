using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{
	public partial interface IPerfiles : IEntidad
	{
		int IdPerfil
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilesNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilesNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilesDescripcion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilesDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIPerfilesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IPerfilesEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}

	}// IPerfiles ends.
}// LibEntidades.Entidades ends.
