using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:32 p. m.*/
namespace Entidades
{
	public partial interface IClientesPersonal : IEntidad
	{
		int IdPersonalCliente
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesPersonalIdCliente", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesPersonalIdCliente", ResourceType = typeof(IcapWeb))]
		int IdCliente
		{
			get;
			set;
		}
		[Display(Name = "IClientesRazonSocial", ResourceType = typeof(IcapWeb))]
		string RazonSocial
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesPersonalNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesPersonalNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesPersonalApellidoPaterno", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesPersonalApellidoPaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoPaterno
		{
			get;
			set;
		}
		[Display(Name = "IClientesPersonalApellidoMaterno", ResourceType = typeof(IcapWeb))]
		string ApellidoMaterno
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesPersonalPuesto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesPersonalPuesto", ResourceType = typeof(IcapWeb))]
		string Puesto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesPersonalTelefono", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesPersonalTelefono", ResourceType = typeof(IcapWeb))]
		string Telefono
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesPersonalEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesPersonalEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEmpresasEmpresa", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIClientesUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IClientesUnidad", ResourceType = typeof(IcapWeb))]
		string Unidad
		{
			get;
			set;
		}
	}// IClientesPersonal ends.
}// LibEntidades.Entidades ends.
