using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 15/11/2016 04:27:39 p. m.*/
namespace Entidades
{
    public partial interface IAlmacenes:IEntidad
    {
		[EntityTable(IsPrimary = true)]
		 int IdAlmacen
		 {
			 get;
			 set;
		 }
		
		 int IdEmpresa
		 {
			 get;
			 set;
		 }
		 [Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		 [FiltroBusqueda(ExcluirCampo=true)]
		 string Empresa
		 {
			 get;
			 set;
		 }

		 [Required(ErrorMessageResourceName = "RequiredIAlmacenesIdUnidad", ErrorMessageResourceType = typeof(IcapWeb))]
		 [Display(Name = "IAlmacenesIdUnidad", ResourceType = typeof(IcapWeb))]
		 [EntityTable(IsUnique = true, ShowResult = false)]
		 [FiltroBusqueda(ExcluirCampo = true)]
		 int IdUnidad
		 {
			 get;
			 set;
		 }
		 
		 string Unidad
		 {
			 get;
			 set;
		 }
		 [Required(ErrorMessageResourceName = "RequiredIAlmacenesCodigo", ErrorMessageResourceType = typeof(IcapWeb))]
		 [Display(Name = "IAlmacenesCodigo", ResourceType = typeof(IcapWeb))]
		 [EntityTable(IsUnique = true, ShowResult = true)]
		 string Codigo
		 {
			 get;
			 set;
		 }
		 [Required(ErrorMessageResourceName = "RequiredIAlmacenesAlmacen", ErrorMessageResourceType = typeof(IcapWeb))]
		 [Display(Name = "IAlmacenesAlmacen", ResourceType = typeof(IcapWeb))]
		 string Almacen
		 {
			 get;
			 set;
		 }
		 [Required(ErrorMessageResourceName = "RequiredIAlmacenesEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		 [Display(Name = "IAlmacenesEstatus", ResourceType = typeof(IcapWeb))]
		 [FiltroBusqueda(ExcluirCampo = true)]
		 bool Estatus
		 {
			 get;
			 set;
		 }
                    
    }// IAlmacenes ends.
}// LibEntidades.Entidades ends.
