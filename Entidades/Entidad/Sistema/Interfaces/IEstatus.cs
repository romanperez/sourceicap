using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 08/12/2016 05:26:34 p. m.*/
namespace Entidades
{
	public partial interface IEstatus : IEntidad
	{
		int IdEstatus
		{
			get;
			set;
		}
		int IdEmpresa
		{
			set;
			get;
		}
		[Required(ErrorMessageResourceName = "RequiredIEstatusClasificacion", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEstatusClasificacion", ResourceType = typeof(IcapWeb))]
		string Clasificacion
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEstatusNombre", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEstatusNombre", ResourceType = typeof(IcapWeb))]
		string Nombre
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEstatusEstatus", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEstatusEstatus", ResourceType = typeof(IcapWeb))]
		bool Estatus
		{
			get;
			set;
		}
		[Display(Name = "IEmpresasEmpresa", ResourceType = typeof(IcapWeb))]
		string Empresa
		{
			get;
			set;
		}
	}// IEstatus ends.
}// LibEntidades.Entidades ends.
