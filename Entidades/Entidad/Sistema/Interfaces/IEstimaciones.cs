using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 03/10/2017 11:24:11 a. m.*/
namespace Entidades
{
	public partial interface IEstimaciones : IEntidad
	{
		int IdEstimacion
		{
			get;
			set;
		}
        int IdContrato
        {
            set;
            get;
        }
		[Required(ErrorMessageResourceName = "RequiredIEstimacionesIdContratosDetalles", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEstimacionesIdContratosDetalles", ResourceType = typeof(IcapWeb))]
		int IdContratosDetalles
		{
			get;
			set;
		}
        [Display(Name = "IEstimacionesNumeroEstimacion", ResourceType = typeof(IcapWeb))]
        int NumeroEstimacion
        {
            get;
            set;
        }
		[Required(ErrorMessageResourceName = "RequiredIEstimacionesFecha", ErrorMessageResourceType = typeof(IcapWeb))]
        [Display(Name = "IEstimacionesFechaInicio", ResourceType = typeof(IcapWeb))]
		DateTime FechaInicio
		{
			get;
			set;
		}
        [Required(ErrorMessageResourceName = "RequiredIEstimacionesFechaFinal", ErrorMessageResourceType = typeof(IcapWeb))]
        [Display(Name = "IEstimacionesFechaFin", ResourceType = typeof(IcapWeb))]
        DateTime FechaFin
        {
            get;
            set;
        }
		[Required(ErrorMessageResourceName = "RequiredIEstimacionesCantidadEstimada", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEstimacionesCantidadEstimada", ResourceType = typeof(IcapWeb))]
		double CantidadEstimada
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIEstimacionesImporteEstimado", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IEstimacionesImporteEstimado", ResourceType = typeof(IcapWeb))]
		double ImporteEstimado
		{
			get;
			set;
		}
        [Display(Name = "IEstimacionesCantidadAcumuladaAnterior", ResourceType = typeof(IcapWeb))]
        double CantidadAcumuladaAnterior { set; get; }
        [Display(Name = "IEstimacionesImporteEstimado", ResourceType = typeof(IcapWeb))]
        double CantidadEjecutadaOT { set; get; }
        [Display(Name = "IEstimacionesCantidadAcumuladaActual", ResourceType = typeof(IcapWeb))]
        double CantidadAcumuladaActual { set; get; }
        [Display(Name = "IEstimacionesImporteAcumuladoActual", ResourceType = typeof(IcapWeb))]
        double ImporteAcumuladoActual { set; get; }
        [Display(Name = "IEstimacionesCantidadPorEstimar", ResourceType = typeof(IcapWeb))]
        double CantidadPorEstimar { set; get; }
        [Display(Name = "IEstimacionesImportePorEstimar", ResourceType = typeof(IcapWeb))]
        double ImportePorEstimar { set; get; }
        [Display(Name = "IEstimacionesImporteDeductivo", ResourceType = typeof(IcapWeb))]
        double? ImporteDeductivo { set; get; }
       
        [Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]        
        string Concepto
        {
            get;
            set;
        }        
        [Display(Name = "IConceptosDescripcion", ResourceType = typeof(IcapWeb))]
        string Descripcion
        {
            get;
            set;
        }
        [Display(Name = "IConceptosIdUnidadMedida", ResourceType = typeof(IcapWeb))]        
        string UnidadMedida
        {
            get;
            set;
        }
        double  CantidadContratada { set; get; }
        double PrecioUnitario { set; get; }
        double ImporteContratado { set; get; }
         [Display(Name = "IEstimacionesPorcentajeAmortizacionAnticipo", ResourceType = typeof(IcapWeb))] 
        double? PorcentajeAmortizacionAnticipo { set; get; }
	}// IEstimaciones ends.
}// LibEntidades.Entidades ends.
