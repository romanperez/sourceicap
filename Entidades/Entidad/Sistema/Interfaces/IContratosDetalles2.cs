using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Idiomas;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{
	public partial interface IContratosDetalles2 : IEntidad
	{				
		int IdContratosDetalles
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetalles2IdContrato", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2IdContrato", ResourceType = typeof(IcapWeb))]
		int IdContrato
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetalles2IdConcepto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2IdConcepto", ResourceType = typeof(IcapWeb))]
		int IdConcepto
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetallesPartida", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2Partida", ResourceType = typeof(IcapWeb))]
		int Partida
		{
			get;
			set;
		}		
		/// <summary>
		/// El precio que se guarda o se envia a base de datos, es el precio unitario del concepto MAS el porcentaje de utilidad.
		/// La moneda que se guarda es en pesos MXN.
		/// </summary>
		[Required(ErrorMessageResourceName = "RequiredIContratosDetalles2Precio", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2Precio", ResourceType = typeof(IcapWeb))]
		double Precio
		{
			get;
			set;
		}
		/*[Required(ErrorMessageResourceName = "RequiredIContratosDetalles2PorcentajeDescuento", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2PorcentajeDescuento", ResourceType = typeof(IcapWeb))]
		double PorcentajeDescuento
		{
			get;
			set;
		}
		[Required(ErrorMessageResourceName = "RequiredIContratosDetalles2DescuentoMonto", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2DescuentoMonto", ResourceType = typeof(IcapWeb))]
		double DescuentoMonto
		{
			get;
			set;
		}*/
		[Required(ErrorMessageResourceName = "RequiredIContratosDetalles2Cantidad", ErrorMessageResourceType = typeof(IcapWeb))]
		[Display(Name = "IContratosDetalles2Cantidad", ResourceType = typeof(IcapWeb))]
		double Cantidad
		{
			get;
			set;
		}
		[Display(Name = "IOTConceptosEjecucionCantidadProgramada", ResourceType = typeof(IcapWeb))]
		double CantidadProgramada
		{
			get;
			set;
		}		
		[Display(Name = "IOTConceptosEjecucionCantidadEjecutada", ResourceType = typeof(IcapWeb))]
		double CantidadEjecutada
		{
			get;
			set;
		}
		[Display(Name = "IOTContratoDetalles2OrdenesTrabajo", ResourceType = typeof(IcapWeb))]
		int Ots
		{
			set;
			get;
		}
		[Display(Name = "IConceptosConcepto", ResourceType = typeof(IcapWeb))]
		string Concepto
		{
			get;
			set;
		}
		[Display(Name = "IConceptosDescripcion", ResourceType = typeof(IcapWeb))]
		string Descripcion
		{
			get;
			set;
		}
		[Display(Name = "ICapitulosCapitulo", ResourceType = typeof(IcapWeb))]
		string Capitulo
		{
			get;
			set;
		}
		int IdMoneda
		{
			get;
			set;
		}
		List<CContratosDetallesInsumos2> Insumos{set;get;}
		[Display(Name = "IMonedasMoneda", ResourceType = typeof(IcapWeb))]
		string Moneda
		{
			get;
			set;
		}
		[Display(Name = "IMonedasCodigo", ResourceType = typeof(IcapWeb))]
		string CodigoMoneda
		{
			get;
			set;
		}
		int IdUnidadMedida
		{
			get;
			set;
		}
		[Display(Name = "IConceptosIdUnidadMedida", ResourceType = typeof(IcapWeb))]
		string UnidadMedida
		{
			get;
			set;
		}
		[Display(Name = "IContratos2Contrato", ResourceType = typeof(IcapWeb))]
		string Contrato
		{
			get;
			set;
		}
		[Display(Name = "IContratos2Ubicacion", ResourceType = typeof(IcapWeb))]
		string Ubicacion
		{
			get;
			set;
		}
		[Display(Name = "IContratos2EsAdicional", ResourceType = typeof(IcapWeb))]
		bool EsAdicional
		{
			get;
			set;
		}		
		string ToXml();
		CConceptos InfoConcepto
		{
			set;
			get;
		}
		double PrecioOriginal
		{
			set;
			get;
		}
		double ValorTipoCambio
		{
			set;
			get;
		}
		string Monedas
		{
			set;
			get;
		}
		DateTime Fecha
		{
			set;
			get;
		}
        DateTime FechaFinal
        {
            set;
            get;
        }

        double BaseInstalado { get; set; }
        double AdicionalInstalado { get; set; }
		  List<CEstimaciones> Estimaciones
		  {
			  set;
			  get;
		  }
			double CantidadPorEstimar
			{
				set;
				get;
			}
			double ImportePorEstimar
			{
				set;
				get;
			}
	}// IContratosDetalles2 ends.
}// LibEntidades.Entidades ends.