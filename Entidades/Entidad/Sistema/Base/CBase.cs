﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
namespace Entidades
{
	public abstract class CBase:IEntidad
	{
		public virtual string[] GetAllCampos()
		{
			return null;
		}
		public virtual string[] GetCamposKey()
		{
			return null;
		}
		public virtual string[] GetOmitirCamposSelect()
		{
			return null;
		}
		/*public virtual string[] GetCamposSelectJoin()
		{
			return null;
		}*/
		public virtual string[] GetFieldsSearch()
		{
			return null;
		}
		public virtual string[] GetFieldsSearchOmitir()
		{
			return null;
		}
		public virtual string[] GetFieldsExcelExport()
		{
			return null;
		}
		public virtual string[] GetFieldsExcelExportOmitir()
		{
			return null;
		}
		#region Serializacion
		public virtual clase ToJson<clase>(string entidad)
		{
			/*JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
			return (clase)jsonSerializer.Deserialize(entidad, typeof(clase));*/
			return CBase._ToJson<clase>(entidad);
		}//JsonToObject ends. 
		public virtual string ToJson(IEntidad entidad)
		{
			/*JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
			return jsonSerializer.Serialize(entidad);*/
			return CBase._ToJson(entidad);
				 
		}//ToJSON
		public static string _ToJson(object data)
		{
			/*if(data == null) return String.Empty;
			JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
			return jsonSerializer.Serialize(data);*/
			return JsonConvert.SerializeObject(data);
		}
		public static clase _ToJson<clase>(string data)
		{
			return JsonConvert.DeserializeObject<clase>(data);
			/*JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
			return (clase)jsonSerializer.Deserialize(data, typeof(clase));*/
		}
		#endregion
	}
}
