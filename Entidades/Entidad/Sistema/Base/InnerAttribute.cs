﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	//[AttributeUsage(Inherited = true)]
	public class InnerAttribute:Attribute
	{
		 protected string _Campo;

		 public InnerAttribute(string campo)
		{
			 this._Campo = campo;
		}
		 public override string ToString()
		 {
			 return this._Campo;
		 }
	}
}
