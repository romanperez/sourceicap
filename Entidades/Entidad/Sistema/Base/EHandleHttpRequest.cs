﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Entidades
{
	public class EHandleHttpRequest
	{
		public EHandleHttpRequest()
		{
		}		
		/// <summary>
		/// Regresa el valor del Query string 
		/// </summary>
		/// <param name="sNombre">Nombre de la varible query string</param>
		/// <returns>Si el valor no existe regresa [""](Cadena vacia) </returns>
		public string GetQryString(System.Web.HttpRequest request, string sNombre)
		{
			string sValor;
			sValor = (request.Params[sNombre.ToLower()] != null) ? request.Params[sNombre.ToLower()].ToString() : "";
			return sValor;
		}
		/// <summary>
		/// Obtiene los parametros enviados por el objeto XmlHttpRequest.Send()
		/// </summary>
		/// <returns>string</returns>
		public string GetInfoFromSend(System.Web.HttpRequest request)
		{
			string sParam = "";
			System.IO.StreamReader reader = new System.IO.StreamReader(request.InputStream);
			sParam = reader.ReadToEnd();
			return sParam;
		}
	}  
}