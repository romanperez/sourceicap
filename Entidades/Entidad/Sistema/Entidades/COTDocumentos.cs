using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{

	public partial class COTDocumentos : CBase, IOTDocumentos
	{
		/// <summary>
		/// Lista de propiedades clase [ COTDocumentos].
		/// </summary>		
		protected int _IdOTDocumentos;
		protected int _IdOrdenTrabajo;
		protected int _IdTipoDocumento;
		protected string _RutaDocumento;
		protected bool _Estatus;

		public virtual int IdOTDocumentos
		{
			get
			{
				return _IdOTDocumentos;
			}
			set
			{
				_IdOTDocumentos = value;
			}
		}
		public virtual int IdOrdenTrabajo
		{
			get
			{
				return _IdOrdenTrabajo;
			}
			set
			{
				_IdOrdenTrabajo = value;
			}
		}
		public virtual int IdTipoDocumento
		{
			get
			{
				return _IdTipoDocumento;
			}
			set
			{
				_IdTipoDocumento = value;
			}
		}
		public virtual string RutaDocumento
		{
			get
			{
				return _RutaDocumento;
			}
			set
			{
				_RutaDocumento = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}	
		public COTDocumentos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[]{"Contenido"};
		}
	}// COTDocumentos ends.
}// Entidades ends.
