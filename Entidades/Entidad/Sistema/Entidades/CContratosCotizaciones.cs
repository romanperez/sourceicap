using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 19/06/2017 04:59:59 p. m.*/
namespace Entidades
{

	public partial class CContratosCotizaciones : CBase, IContratosCotizaciones
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratosCotizaciones].
		/// </summary>		
		protected int _IdContratoCotizacion;
		protected int _IdContrato;
		protected int _IdCotizacion;
		protected DateTime _Fecha;

		public virtual int IdContratoCotizacion
		{
			get
			{
				return _IdContratoCotizacion;
			}
			set
			{
				_IdContratoCotizacion = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int IdCotizacion
		{
			get
			{
				return _IdCotizacion;
			}
			set
			{
				_IdCotizacion = value;
			}
		}
		public virtual DateTime Fecha
		{
			get
			{
				return _Fecha;
			}
			set
			{
				_Fecha = value;
			}
		}
		public virtual string Cotizacion
		{
			set;
			get;
		}
		public virtual string Contrato
		{
			set;
			get;
		}

		public CContratosCotizaciones()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Cotizacion", "Contrato" };
		}
	}// CContratosCotizaciones ends.
}// Entidades ends.
