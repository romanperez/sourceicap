using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:39 p. m.*/
namespace Entidades
{

	public partial class CClientesPersonal : CBase, IClientesPersonal
	{
		/// <summary>
		/// Lista de propiedades clase [ CClientesPersonal].
		/// </summary>		
		protected int _IdPersonalCliente;
		protected int _IdCliente;
		protected string _RazonSocial;
		protected string _Nombre;
		protected string _ApellidoPaterno;
		protected string _ApellidoMaterno;
		protected string _Puesto;
		protected string _Telefono;
		protected bool _Estatus;
		protected string _Empresa;
		protected string _Unidad;
		public virtual int IdPersonalCliente
		{
			get
			{
				return _IdPersonalCliente;
			}
			set
			{
				_IdPersonalCliente = value;
			}
		}
		public virtual int IdCliente
		{
			get
			{
				return _IdCliente;
			}
			set
			{
				_IdCliente = value;
			}
		}
		public virtual string RazonSocial
		{
			get
			{
				return _RazonSocial;
			}
			set
			{
				_RazonSocial = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual string ApellidoPaterno
		{
			get
			{
				return _ApellidoPaterno;
			}
			set
			{
				_ApellidoPaterno = value;
			}
		}
		public virtual string ApellidoMaterno
		{
			get
			{
				return _ApellidoMaterno;
			}
			set
			{
				_ApellidoMaterno = value;
			}
		}
		public virtual string Puesto
		{
			get
			{
				return _Puesto;
			}
			set
			{
				_Puesto = value;
			}
		}
		public virtual string Telefono
		{
			get
			{
				return _Telefono;
			}
			set
			{
				_Telefono = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}
		public virtual string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}
		public CClientesPersonal()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "RazonSocial", "Empresa", "Unidad" };
		}
		/*public override string[] GetCamposSelectJoin()
		{
			return new string[] { "RazonSocial" };
		}*/
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "RazonSocial", "IdCliente", "Empresa", "Unidad" };
		}
	}// CClientesPersonal ends.
}// Entidades ends.
