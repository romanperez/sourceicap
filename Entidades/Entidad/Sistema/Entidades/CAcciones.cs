using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 04/01/2017 10:37:31 a. m.*/
namespace Entidades
{

	public partial class CAcciones : CBase, IAcciones
	{
		/// <summary>
		/// Lista de propiedades clase [ CAcciones].
		/// </summary>		
		protected int _IdAccion;
		protected string _AccionNombre;
		protected string _ParametrosJs;
		protected string _ParametrosMvc;
		protected string _Html;
		protected bool _Estatus;

		public virtual int IdAccion
		{
			get
			{
				return _IdAccion;
			}
			set
			{
				_IdAccion = value;
			}
		}
		public virtual string AccionNombre
		{
			get
			{
				return _AccionNombre;
			}
			set
			{
				_AccionNombre = value;
			}
		}
		public virtual string ParametrosJs
		{
			get
			{
				return _ParametrosJs;
			}
			set
			{
				_ParametrosJs = value;
			}
		}
		public virtual string ParametrosMvc
		{
			get
			{
				return _ParametrosMvc;
			}
			set
			{
				_ParametrosMvc = value;
			}
		}
		public virtual string Html
		{
			get
			{
				return _Html;
			}
			set
			{
				_Html = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string MenuIncluir
		{
			get;
			set;
		}
		public virtual string MenuOmitir
		{
			get;
			set;
		}

		public CAcciones()
		{
		}
	}// CAcciones ends.
}// Entidades ends.
