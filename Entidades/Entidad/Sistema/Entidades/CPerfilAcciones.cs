using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 04/01/2017 10:37:31 a. m.*/
namespace Entidades
{
	public partial class CPerfilAcciones : CBase, IPerfilAcciones
	{
		/// <summary>
		/// Lista de propiedades clase [ CPerfilAcciones].
		/// </summary>		
		protected int _IdPerfilAcciones;
		protected int _IdAccion;
		protected int _IdPerfil;
		protected int _IdMenu;
		protected bool _Estatus;
		protected string _AccionNombre;
		protected string _ParametrosJs;		
		protected string _ParametrosMvc;
		protected string _Html;

		public virtual int IdPerfilAcciones
		{
			get
			{
				return _IdPerfilAcciones;
			}
			set
			{
				_IdPerfilAcciones = value;
			}
		}
		public virtual int IdAccion
		{
			get
			{
				return _IdAccion;
			}
			set
			{
				_IdAccion = value;
			}
		}
		public virtual int IdPerfil
		{
			get
			{
				return _IdPerfil;
			}
			set
			{
				_IdPerfil = value;
			}
		}

		public virtual int IdMenu
		{
			get
			{
				return _IdMenu;
			}
			set
			{
				_IdMenu = value;
			}
		}

		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string AccionNombre
		{
			get
			{
				return _AccionNombre;
			}
			set
			{
				_AccionNombre = value;
			}
		}

		public virtual string ParametrosJs
		{
			get
			{
				return _ParametrosJs;
			}
			set
			{
				_ParametrosJs = value;
			}
		}

		public virtual string ParametrosMvc
		{
			get
			{
				return _ParametrosMvc;
			}
			set
			{
				_ParametrosMvc = value;
			}
		}

		public virtual string Html
		{
			get
			{
				return _Html;
			}
			set
			{
				_Html = value;
			}
		}
		public CPerfilAcciones()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "AccionNombre", "ParametrosJs", "ParametrosMvc", "Html" };
		}
	}// CPerfilAcciones ends.
}// Entidades ends.