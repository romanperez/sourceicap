using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{

	public partial class CContratosDetalles : CBase, IContratosDetalles
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratosDetalles].
		/// </summary>		
		protected int _IdContratoDetalle;
		protected bool _Estatus;
		protected int _IdContrato;
		protected int _IdConcepto;
		protected int _Partida;

		protected double _Precio;

		public virtual int IdContratoDetalle
		{
			get
			{
				return _IdContratoDetalle;
			}
			set
			{
				_IdContratoDetalle = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
	
		public virtual int Partida
		{
			get
			{
				return _Partida;
			}
			set
			{
				_Partida = value;
			}
		}
		
		public virtual double Precio
		{
			get
			{
				return _Precio;
			}
			set
			{
				_Precio = value;
			}
		}		
		public virtual string Concepto
		{
			get;
			set;
		}
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Capitulo
		{
			get;
			set;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}		
		public virtual string Moneda
		{
			set;
			get;
		}
		public CContratosDetalles()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Concepto", "Descripcion", "Capitulo", "IdMoneda", "Moneda" };

		}
	}// CContratosDetalles ends.
}// Entidades ends.
