using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 10/04/2017 12:10:55 p. m.*/
namespace Entidades
{

	public partial class CClasificacionConceptos : CBase, IClasificacionConceptos
	{
		/// <summary>
		/// Lista de propiedades clase [ CClasificacionConceptos].
		/// </summary>		
		protected int _IdConceptoClasificacion;
		protected int _IdEmpresa;
		protected string _Clasificacion;
		protected string _Descripicion;

		public virtual int IdConceptoClasificacion
		{
			get
			{
				return _IdConceptoClasificacion;
			}
			set
			{
				_IdConceptoClasificacion = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Clasificacion
		{
			get
			{
				return _Clasificacion;
			}
			set
			{
				_Clasificacion = value;
			}
		}
		public virtual string Descripicion
		{
			get
			{
				return _Descripicion;
			}
			set
			{
				_Descripicion = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}

		public CClasificacionConceptos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
	}// CClasificacionConceptos ends.
}// Entidades ends.
