using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:41 p. m.*/
namespace Entidades
{

	public partial class CIngenierias : CBase, IIngenierias
	{
		/// <summary>
		/// Lista de propiedades clase [ CIngenierias].
		/// </summary>		
		protected int _IdIngenieria;
		protected int _IdProyecto;
		protected string _Ingenieria;
		protected DateTime _Fecha;
		protected bool _Estatus;

		public virtual int IdIngenieria
		{
			get
			{
				return _IdIngenieria;
			}
			set
			{
				_IdIngenieria = value;
			}
		}
		public virtual int IdProyecto
		{
			get
			{
				return _IdProyecto;
			}
			set
			{
				_IdProyecto = value;
			}
		}
		public virtual string Ingenieria
		{
			get
			{
				return _Ingenieria;
			}
			set
			{
				_Ingenieria = value;
			}
		}
		public virtual DateTime Fecha
		{
			get
			{
				return _Fecha;
			}
			set
			{
				_Fecha = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Proyecto
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}	  

		public virtual string Empresa
		{
			get;
			set;
		}
		public CIngenierias()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Proyecto", "Unidad", "Empresa" };
			

		}
	}// CIngenierias ends.
}// Entidades ends.
