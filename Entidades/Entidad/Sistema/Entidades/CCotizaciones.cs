using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{

	public partial class CCotizaciones : CBase, ICotizaciones
	{
		/// <summary>
		/// Lista de propiedades clase [ CCotizaciones].
		/// </summary>		
		protected int _IdCotizacion;
		protected int _IdUsuario;
		protected int _IdMoneda;
		protected string _Cotizacion;
		protected double _Total;
		protected double _PorcentajeDescuento;
		protected double _DescuentoMonto;
		protected int _IdEmpresa;
		protected int _IdCliente;
		protected int _IdUnidad;
		protected int _IdIva;
		protected DateTime _Fecha;
		protected double _Exito;
		protected double _SubTotal;
		protected int? _IdTipoCambio;
		protected double? _Utilidad;

		public virtual int IdCotizacion
		{
			get
			{
				return _IdCotizacion;
			}
			set
			{
				_IdCotizacion = value;
			}
		}
		public virtual int IdUsuario
		{
			get
			{
				return _IdUsuario;
			}
			set
			{
				_IdUsuario = value;
			}
		}
		public virtual int IdMoneda
		{
			get
			{
				return _IdMoneda;
			}
			set
			{
				_IdMoneda = value;
			}
		}
		public virtual string Cotizacion
		{
			get
			{
				return _Cotizacion;
			}
			set
			{
				_Cotizacion = value;
			}
		}
		public virtual double Total
		{
			get
			{
				return _Total;
			}
			set
			{
				_Total = value;
			}
		}
		public virtual double PorcentajeDescuento
		{
			get
			{
				return _PorcentajeDescuento;
			}
			set
			{
				_PorcentajeDescuento = value;
			}
		}
		public virtual double DescuentoMonto
		{
			get
			{
				return _DescuentoMonto;
			}
			set
			{
				_DescuentoMonto = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdCliente
		{
			get
			{
				return _IdCliente;
			}
			set
			{
				_IdCliente = value;
			}
		}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual int IdIva
		{
			get
			{
				return _IdIva;
			}
			set
			{
				_IdIva = value;
			}
		}
		public virtual DateTime Fecha
		{
			get
			{
				return _Fecha;
			}
			set
			{
				_Fecha = value;
			}
		}
		public virtual double Exito
		{
			get
			{
				return _Exito;
			}
			set
			{
				_Exito = value;
			}
		}
		public virtual double SubTotal
		{
			get
			{
				return _SubTotal;
			}
			set
			{
				_SubTotal = value;
			}
		}
		public virtual double Iva
		{
			set;
			get;
		}
		public virtual string RazonSocial
		{
			get;
			set;
		}
		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual List<CCotizacionesDetalles> Conceptos
		{
			set;
			get;
		}
		public virtual List<CCotizacionesDetallesInsumos> Insumos
		{
			set;
			get;
		}
		public virtual int? IdTipoCambio
		{
			set
			{
				_IdTipoCambio = value;
			}
			get
			{
				return _IdTipoCambio;
			}
		}
		public virtual string FechaEntrega
		{
			set;
			get;
		}
		public virtual double Valor
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual string Proyecto
		{
			set;
			get;
		}
		
		public virtual double? Utilidad
		{
			get
			{
				return _Utilidad;
			}
			set
			{
				_Utilidad = value;
			}
		}
		public virtual bool IsCopy
		{
			set;
			get;
		}
		public virtual CUsuarios Usuario
		{
			set;
			get;
		}
		public virtual string Solicitud
		{
			set;
			get;
		}		
		public virtual string ProyectoInicial
		{
			set;
			get;
		}
		public virtual string DescripcionTitulo
		{
			set;
			get;
		}
		public virtual string DescripcionCompleta
		{
			set;
			get;
		}
		public virtual string DepartamentoAtencion
		{
			set;
			get;
		}
		public virtual List<CCotizacionesPersonalCliente> PersonalClienteAtencion
		{
			set;
			get;
		}
		public virtual List<CCotizacionesPersonalCliente> PersonalClienteContacto
		{
			set;
			get;
		}
		public virtual string ListClienteAtencion
		{
			set;
			get;
		}
		public virtual string ListClienteContacto
		{
			set;
			get;
		}
		public virtual double SubTotalOriginal
		{
			set;
			get;
		}
		public virtual double TotalOriginal
		{
			set;
			get;
		}

		public virtual double ValorTipoCambio
		{
			get;
			set;
		}

		public virtual string Monedas
		{
			get;
			set;
		}		
		public CCotizaciones()
		{
			PersonalClienteAtencion = new List<CCotizacionesPersonalCliente>();
			PersonalClienteContacto = new List<CCotizacionesPersonalCliente>();
			
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "RazonSocial", "Empresa", "Conceptos", "Iva", 
				                   "Unidad", "Moneda", "CodigoMoneda", "Insumos", 
										 "Proyecto", "IsCopy", "Valor", "Usuario",
										 "PersonalClienteAtencion","PersonalClienteContacto",
			                      "ListClienteAtencion","ListClienteContacto",
										 "SubTotalOriginal","TotalOriginal",
										 "ValorTipoCambio","Monedas"};
		}
	}// CCotizaciones ends.
}// Entidades ends.
