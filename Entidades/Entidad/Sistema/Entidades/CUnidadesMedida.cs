using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:44 p. m.*/
namespace Entidades
{

	public partial class CUnidadesMedida : CBase, IUnidadesMedida
	{
		/// <summary>
		/// Lista de propiedades clase [ CUnidadesMedida].
		/// </summary>		
		protected int _IdUnidad;
		protected int _IdEmpresa;
		protected string _Unidad;
		protected string _Descripcion;
		protected bool _Estatus;

		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public CUnidadesMedida()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "IdUnidad", "IdEmpresa", "Empresa" };
		}
	}// CUnidadesMedida ends.
}// Entidades ends.
