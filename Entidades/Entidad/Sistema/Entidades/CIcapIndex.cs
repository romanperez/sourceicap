using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{		
    public partial class CIcapIndex : IIcapIndex
    {
		protected IIdioma _idioma;
		protected IUsuarios _usuario;
		protected IEnumerable<IMenus> _menus;
		public virtual IIdioma Idioma
		{
			get
			{
				return _idioma;
			}
			set
			{
				_idioma = value;
			}
		}
		 public virtual  IUsuarios Usuario
		{
		 get{return _usuario;}
		 set{_usuario = value;}
		}
		public virtual  IEnumerable<IMenus> Menus
		{
		 get{return _menus;}
		 set{_menus = value;}
		}
		public CIcapIndex()
		{	  
			_menus=new List<IMenus>();  
		}		
    }// CUsuarios ends.
}// Entidades ends.
