using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:41 p. m.*/
namespace Entidades
{

	public partial class CEspecialidades : CBase, IEspecialidades
	{
		/// <summary>
		/// Lista de propiedades clase [ CEspecialidades].
		/// </summary>		
		protected int _IdEspecialidad;
		protected int _IdEmpresa;
		protected int _IdGrado;
		protected string _Codigo;
		protected string _Nombre;
		protected bool _Estatus;

		public virtual int IdEspecialidad
		{
			get
			{
				return _IdEspecialidad;
			}
			set
			{
				_IdEspecialidad = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int  IdGrado
		{
			get
			{
				return _IdGrado;
			}
			set
			{
				_IdGrado = value;
			}
		}
		public virtual string Codigo
		{
			get
			{
				return _Codigo;
			}
			set
			{
				_Codigo = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public virtual int IdTecnico
		{
			set;
			get;
		}
		public virtual string Grado
		{
			get;
			set;
		}
		public CEspecialidades()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "IdTecnico", "Grado" };
		}
	}// CEspecialidades ends.
}// Entidades ends.
