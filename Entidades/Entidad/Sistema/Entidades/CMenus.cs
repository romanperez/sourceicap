using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{ 
    
    public partial class CMenus : CBase,IMenus
    {
      /// <summary>
      /// Lista de propiedades clase [ CMenus].
      /// </summary>		
		protected int _IdMenu;
		protected int _IdMenuPadre;		 
		protected string _Menu;		
		protected string _Titulo;		 
		protected string _Imagen;
		protected string _ImagenTab;
		protected string _Url;		
		protected int _Orden;
		protected bool _Estatus;
		protected string _Parametros;
		
		protected List<CMenus> _SubMenus;
		        			
		public virtual int IdMenu	
		{
			get
			{
				return _IdMenu;
			}
			set
			{
				_IdMenu = value;
			}
		}
		public virtual int IdMenuPadre
		{
			get
			{
				return _IdMenuPadre;
			}
			set
			{
				_IdMenuPadre = value;
			}
		}
		public virtual string Menu
		{
			get
			{
				return _Menu;
			}
			set
			{
				_Menu = value;
			}
		}
		public virtual string Titulo
		{
			get
			{
				return _Titulo;
			}
			set
			{
				_Titulo = value;
			}
		}
		public virtual string Imagen
		{
			get
			{
				return _Imagen;
			}
			set
			{
				_Imagen = value;
			}
		}
		public virtual string ImagenTab
		{
			get
			{
				return _ImagenTab;
			}
			set
			{
				_ImagenTab = value;
			}
		}
		public virtual string Url
		{
			get
			{
				return _Url;
			}
			set
			{
				_Url = value;
			}
		}
		public virtual int Orden
		{
			get
			{
				return _Orden;
			}
			set
			{
				_Orden = value;
			}
		}
		public virtual string Parametros
		{
			get
			{
				return _Parametros;
			}
			set
			{
				_Parametros = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual List<CMenus> SubMenus
		{
			get
			{
				return _SubMenus;
			}
			set
			{
				_SubMenus = value;
			}
		}
		public bool HasItems
		{
			get
			{
				if(_SubMenus != null && _SubMenus.Count() > 0) return true;
				else return false;
			}
		}
		public CMenus()
		{	  
			_SubMenus = new List<CMenus>();
		}
		public void SetSubMenus(IEnumerable<IMenus> menus)
		{
			if(menus == null || menus.Count() <= 0) return;
			_SubMenus = menus.Select( m=> (CMenus)m).ToList();			
		}
		public IEnumerable<IMenus> GetSubMenus()
		{
			return _SubMenus;
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "SubMenus", "HasItems" };
		}
		public override string[] GetFieldsSearchOmitir()
		{		
			return new string[] {"Imagen","Url","Orden" ,"Estatus"};
		}
	 }// CMenus ends.
}// Entidades ends.
