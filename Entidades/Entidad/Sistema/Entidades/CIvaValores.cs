using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:41 p. m.*/
namespace Entidades
{

	public partial class CIvaValores : CBase, IIvaValores
	{
		/// <summary>
		/// Lista de propiedades clase [ CIvaValores].
		/// </summary>		
		protected int _IdIva;
		protected int _IdEmpresa;
		protected double _Porcentaje;
		protected DateTime _FechaInicio;
		protected DateTime _FechaFin;
		protected bool _Estatus;

		public virtual int IdIva
		{
			get
			{
				return _IdIva;
			}
			set
			{
				_IdIva = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual double Porcentaje
		{
			get
			{
				return _Porcentaje;
			}
			set
			{
				_Porcentaje = value;
			}
		}
		public virtual DateTime FechaInicio
		{
			get
			{
				return _FechaInicio;
			}
			set
			{
				_FechaInicio = value;
			}
		}
		public virtual DateTime FechaFin
		{
			get
			{
				return _FechaFin;
			}
			set
			{
				_FechaFin = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public CIvaValores()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
	}// CIvaValores ends.
}// Entidades ends.