using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:39 p. m.*/
namespace Entidades
{
	///<summary>
	///Catalogo actividades
	///</summary>
	public partial class CActividades : CBase, IActividades
	{
		/// <summary>
		/// Lista de propiedades clase [ CActividades].
		/// </summary>		
		protected int _IdActividad;
		protected int _IdEmpresa;
		protected int _IdEspecialidad;
		protected string _Actividad;
		protected string _Descripcion;
		protected string _ProcedimientoSeguridad;
		protected string _AnalisisRiesgo;
		protected bool _Estatus;

		public virtual int IdActividad
		{
			get
			{
				return _IdActividad;
			}
			set
			{
				_IdActividad = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdEspecialidad
		{
			get
			{
				return _IdEspecialidad;
			}
			set
			{
				_IdEspecialidad = value;
			}
		}
		public virtual string Actividad
		{
			get
			{
				return _Actividad;
			}
			set
			{
				_Actividad = value;
			}
		}
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string ProcedimientoSeguridad
		{
			set
			{
				_ProcedimientoSeguridad = value;
			}
			get
			{
				return _ProcedimientoSeguridad;
			}
		}
		public virtual string AnalisisRiesgo
		{
			set
			{
				_AnalisisRiesgo = value;
			}
			get
			{
				return _AnalisisRiesgo;
			}
		}
		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual string Especialidad
		{
			get;
			set;
		}
		public virtual string AuxFilesMaps
		{
			get;
			set;
		}
		public virtual int IdConceptoClasificacion
		{
			get;
			set;
		}		
		public virtual string Clasificacion
		{
			get;
			set;
		}
		public CActividades()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "AuxFilesMaps", "Especialidad", "Clasificacion" };
		}
	}// CActividades ends.
}// Entidades ends.
