using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{

	public partial class CMonedas : CBase, IMonedas
	{
		/// <summary>
		/// Lista de propiedades clase [ CMonedas].
		/// </summary>		
		protected int _IdMoneda;
		protected int _IdEmpresa;
		protected string _Moneda;
		protected string _Codigo;
		protected bool _Estatus;

		public virtual int IdMoneda
		{
			get
			{
				return _IdMoneda;
			}
			set
			{
				_IdMoneda = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Moneda
		{
			get
			{
				return _Moneda;
			}
			set
			{
				_Moneda = value;
			}
		}
		public virtual string Codigo
		{
			get
			{
				return _Codigo;
			}
			set
			{
				_Codigo = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}	
		public virtual string Empresa
		{
			get;
			set;
		}
		public CMonedas()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "IdMoneda", "IdEmpresa","Empresa" };
		}
	}// CMonedas ends.
}// Entidades ends.
