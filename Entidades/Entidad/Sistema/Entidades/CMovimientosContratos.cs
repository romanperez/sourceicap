using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 23/06/2017 01:59:19 p. m.*/
namespace Entidades
{
	public partial class CMovimientosContratos : CBase, IMovimientosContratos
	{
		/// <summary>
		/// Lista de propiedades clase [ CMovimientosContratos].
		/// </summary>		
		protected int _IdMovimientoContrato;
		protected int _IdProyecto;
		protected int _IdContrato;
		protected int _IdMovimiento;

		public virtual int IdMovimientoContrato
		{
			get
			{
				return _IdMovimientoContrato;
			}
			set
			{
				_IdMovimientoContrato = value;
			}
		}
		public virtual int IdProyecto
		{
			get
			{
				return _IdProyecto;
			}
			set
			{
				_IdProyecto = value;
			}
		}
		public virtual int IdConcepto
		{
			set;
			get;
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int IdMovimiento
		{
			get
			{
				return _IdMovimiento;
			}
			set
			{
				_IdMovimiento = value;
			}
		}
		public virtual int IdInsumo
		{
			set;
			get;
		}
		public virtual int? IdPedidoDetalle
		{
			set;
			get;
		}
		public virtual int? IdMovimientoDetalle
		{
			set;
			get;
		}
		public virtual int? IdMovimientoTraspaso
		{
			set;
			get;
		}
		public CMovimientosContratos()
		{
		}
	}// CMovimientosContratos ends.
}// Entidades ends.
