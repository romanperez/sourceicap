using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{

	public partial class COrdenesTrabajo : CBase, IOrdenesTrabajo
	{
		/// <summary>
		/// Lista de propiedades clase [COrdenesTrabajo].
		/// </summary>		
		protected int _IdOrdenTrabajo;
		protected int _IdEmpresa;
		protected int _IdProyecto;
		protected string _Comentarios;
		protected string _ComentariosCliente;
		protected bool _Estatus;
		protected int _IdContrato;
		protected int _IdActividad;
		protected string _Folio;
		protected string _Ubicacion;
		protected DateTime _FechaInicio;
		protected DateTime _FechaFin;
		protected int _IdEstatus;
		protected int _IdPrioridad;
		protected int _IdTipo;

		public virtual int IdOrdenTrabajo
		{
			get
			{
				return _IdOrdenTrabajo;
			}
			set
			{
				_IdOrdenTrabajo = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdProyecto
		{
			get
			{
				return _IdProyecto;
			}
			set
			{
				_IdProyecto = value;
			}
		}
		public virtual string Comentarios
		{
			get
			{
				return _Comentarios;
			}
			set
			{
				_Comentarios = value;
			}
		}
		public virtual string ComentariosCliente
		{
			get
			{
				return _ComentariosCliente;
			}
			set
			{
				_ComentariosCliente = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		/*public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}*/
		public virtual int IdActividad
		{
			get
			{
				return _IdActividad;
			}
			set
			{
				_IdActividad = value;
			}
		}
		public virtual string Folio
		{
			get
			{
				return _Folio;
			}
			set
			{
				_Folio = value;
			}
		}
		public virtual string Ubicacion
		{
			get
			{
				return _Ubicacion;
			}
			set
			{
				_Ubicacion = value;
			}
		}
		[DisplayFormat(ApplyFormatInEditMode = true,
					DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
		public virtual DateTime FechaInicio
		{
			get
			{
				return _FechaInicio;
			}
			set
			{
				_FechaInicio = value;
			}
		}
		[DisplayFormat(ApplyFormatInEditMode = true,
					DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
		public virtual DateTime FechaFin
		{
			get
			{
				return _FechaFin;
			}
			set
			{
				_FechaFin = value;
			}
		}
		public virtual int IdEstatus
		{
			get
			{
				return _IdEstatus;
			}
			set
			{
				_IdEstatus = value;
			}
		}
		public virtual int IdPrioridad
		{
			get
			{
				return _IdPrioridad;
			}
			set
			{
				_IdPrioridad = value;
			}
		}
		public virtual int IdTipo
		{
			get
			{
				return _IdTipo;
			}
			set
			{
				_IdTipo = value;
			}
		}
		public virtual string Proyecto
		{
			set;
			get;
		}

		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string Actividad
		{
			get;
			set;
		}
		public virtual string RazonSocial
		{
			get;
			set;
		}
		public virtual List<COTPersonalCliente> PersonalClienteAtencion
		{
			get;
			set;
		}
		public virtual List<COTPersonalCliente> PersonalClienteContacto
		{
			get;
			set;
		}		
		public virtual string EstatusOT
		{
			get;
			set;
		}		
		public virtual string PrioridadOT
		{
			get;
			set;
		}
		public virtual string Contrato
		{
			get;
			set;
		}
		public virtual int IdCliente
		{
			set;
			get;
		}
		public virtual string TipoOT
		{
			get;
			set;
		}
		public virtual List<COTContratos> Contratos
		{
			set;
			get;
		}
		public virtual List<COTConceptos> OTConceptos
		{
			set;
			get;
		}
		public virtual CActividades OTActividad
		{
			set;
			get;
		}
		public virtual List<COTPersonal> OTPersonal
		{
			get;
			set;
		}
		public virtual List<DFile> UpLoadFiles
		{
			set;
			get;
		}
		public virtual List<COTDocumentos> OTDiagramas
		{
			set;
			get;
		}
		public virtual List<COTDocumentos> OTDetalleInstalacion
		{
			set;
			get;
		}
		public virtual string ListaDiagramas
		{
			set;
			get;
		}
		public virtual string ListaDetallesInstalacion
		{
			set;
			get;
		}
		public virtual string ListClienteAtencion
		{
			set;
			get;
		}
		public virtual string ListClienteContacto
		{
			set;
			get;
		}
		public virtual string ListTecnicos
		{
			set;
			get;
		}
		
		public COrdenesTrabajo()
		{
			PersonalClienteAtencion = new List<COTPersonalCliente>();
			PersonalClienteContacto = new List<COTPersonalCliente>();
			Contratos = new List<COTContratos>();
			OTPersonal = new List<COTPersonal>();
			UpLoadFiles = new List<DFile>();
			OTDiagramas = new List<COTDocumentos>();
			OTDetalleInstalacion = new List<COTDocumentos>();
			OTConceptos = new List<COTConceptos>();
			FechaInicio = DateTime.Now;
			FechaFin = DateTime.Now;
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "Unidad", "Actividad", 
				                   "Proyecto", "RazonSocial", "PersonalClienteAtencion", 
										 "PersonalClienteContacto", "EstatusOT", "PrioridadOT",
										 "Contrato","IdCliente","Contratos","OTActividad",
										 "OTPersonal","OTDiagramas","UpLoadFiles","OTDetalleInstalacion",
										 "ListaDiagramas","ListaDetallesInstalacion","OTConceptos","TipoOT",
										 "ListClienteAtencion","ListClienteContacto","ListTecnicos"};
		}
	}// COrdenesTrabajo ends.
}// Entidades ends.