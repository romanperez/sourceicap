using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:41 p. m.*/
namespace Entidades
{

	public partial class CInsumos : CBase, IInsumos
	{
		/// <summary>
		/// Lista de propiedades clase [ CInsumos].
		/// </summary>		
		protected int _IdInsumo;
		protected int _IdEmpresa;
		protected int? _IdFamilia;
		protected int? _IdUnidad; /*Si es servicio no tiene unidad de medida*/
		protected string _Codigo;
		protected string _Serie;
		protected string _Descripcion;
		protected int _IdMoneda;
		protected bool _EsServicio;
		protected bool _Estatus;
		protected string _Unidad;
		protected string _UnidadMedidaDescripcion;
		protected string _Moneda;
		protected double? _Costo;

		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int? IdFamilia
		{
			get
			{
				return _IdFamilia;
			}
			set
			{
				_IdFamilia = value;
			}
		}
		public virtual int? IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual string Codigo
		{
			get
			{
				return _Codigo;
			}
			set
			{
				_Codigo = value;
			}
		}
		public virtual string Serie
		{
			get
			{
				return _Serie;
			}
			set
			{
				_Serie = value;
			}
		}
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		public virtual int IdMoneda
		{
			get
			{
				return _IdMoneda;
			}
			set
			{
				_IdMoneda = value;
			}
		}		
		public virtual bool EsServicio
		{
			get
			{
				return _EsServicio;
			}
			set
			{
				_EsServicio = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual  string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}
		public virtual string UnidadMedidaDescripcion
		{
			get
			{
				return _UnidadMedidaDescripcion;
			}
			set
			{
				_UnidadMedidaDescripcion = value;
			}
		}
		
		public virtual string Moneda
		{
			get
			{
				return _Moneda;
			}
			set
			{
				_Moneda = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public virtual string Familia
		{
			get;
			set;
		}
		public virtual double? Costo
		{
			get
			{
				return _Costo;
			}
			set
			{
				_Costo = value;
			}
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public CInsumos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Unidad", "UnidadMedidaDescripcion", "Moneda", "Empresa", "Familia", "CodigoMoneda" };
		}
		/*public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Unidad", "UnidadMedidaDescripcion", "Moneda", "Empresa","Familia" };
		}*/
	}// CInsumos ends.
}// Entidades ends.
