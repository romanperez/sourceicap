using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{
	///<summary>
	///Catalogo Contratos
	///</summary>
	public partial class CContratos : CBase, IContratos
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratos].
		/// </summary>		
		protected int _IdContrato;
		protected int _IdEmpresa;
		protected int? _IdPedido;		
		protected int _IdProyecto;
		protected int _IdMoneda;
		protected string _Contrato;
		protected bool _Aprobado;
		protected bool _Estatus;

		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int? IdPedido
		{
			get
			{
				return _IdPedido;
			}
			set
			{
				_IdPedido = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdProyecto
		{
			get
			{
				return _IdProyecto;
			}
			set
			{
				_IdProyecto = value;
			}
		}
		public virtual int IdMoneda
		{
			get
			{
				return _IdMoneda;
			}
			set
			{
				_IdMoneda = value;
			}
		}
		public virtual string Contrato
		{
			get
			{
				return _Contrato;
			}
			set
			{
				_Contrato = value;
			}
		}
		public virtual bool Aprobado
		{
			get
			{
				return _Aprobado;
			}
			set
			{
				_Aprobado = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string Proyecto
		{
			get;
			set;
		}
		/// <summary>
		/// Especifica quien realiza la salida y solicitud de materiales.
		/// </summary>
		public virtual int IdUsuario
		{
			set;
			get;
		}
		public virtual string Moneda
		{
			set;
			get;
		}
		public CContratos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "Unidad", "Proyecto", "IdUsuario", "Moneda" };
		}
	}// CContratos ends.
}// Entidades ends.