using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 05/05/2017 01:03:59 p. m.*/
namespace Entidades
{

	public partial class COTConceptosEjecucion : CBase, IOTConceptosEjecucion
	{
		/// <summary>
		/// Lista de propiedades clase [ COTConceptosEjecucion].
		/// </summary>		
		protected int _IdOTConceptoEjecucion;
		protected int _IdOTConcepto;
		protected double _CantidadProgramada;
		protected double _CantidadEjecutada;
		protected DateTime _Fecha;

		public virtual int IdOTConceptoEjecucion
		{
			get
			{
				return _IdOTConceptoEjecucion;
			}
			set
			{
				_IdOTConceptoEjecucion = value;
			}
		}
		public virtual int IdOTConcepto
		{
			get
			{
				return _IdOTConcepto;
			}
			set
			{
				_IdOTConcepto = value;
			}
		}
		public virtual double CantidadProgramada
		{
			get
			{
				return _CantidadProgramada;
			}
			set
			{
				_CantidadProgramada = value;
			}
		}
		public virtual double CantidadEjecutada
		{
			get
			{
				return _CantidadEjecutada;
			}
			set
			{
				_CantidadEjecutada = value;
			}
		}
		public virtual DateTime Fecha
		{
			get
			{
				return _Fecha;
			}
			set
			{
				_Fecha = value;
			}
		}

		public COTConceptosEjecucion()
		{
		}
	}// COTConceptosEjecucion ends.
}// Entidades ends.
