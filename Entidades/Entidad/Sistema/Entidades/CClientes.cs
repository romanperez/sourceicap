using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:39 p. m.*/
namespace Entidades
{
	///<summary>
	///Catalogo clientes
	///</summary>
	public partial class CClientes : CBase, IClientes
	{
		/// <summary>
		/// Lista de propiedades clase [ CClientes].
		/// </summary>		
		protected int _IdCliente;
		protected int _IdUnidad;
		protected string _Unidad;
		protected string _Empresa;
		protected string _RazonSocial;
		protected string _NombreComercial;
		protected string _Rfc;
		protected string _Telefono;
		protected string _Direccion;
		protected int _CodigoPostal;
		protected bool _Estatus;

		public virtual int IdCliente
		{
			get
			{
				return _IdCliente;
			}
			set
			{
				_IdCliente = value;
			}
		}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}
		public virtual string RazonSocial
		{
			get
			{
				return _RazonSocial;
			}
			set
			{
				_RazonSocial = value;
			}
		}
		public virtual string NombreComercial
		{
			get
			{
				return _NombreComercial;
			}
			set
			{
				_NombreComercial = value;
			}
		}
		public virtual string Rfc
		{
			get
			{
				return _Rfc;
			}
			set
			{
				_Rfc = value;
			}
		}
		public virtual string Telefono
		{
			get
			{
				return _Telefono;
			}
			set
			{
				_Telefono = value;
			}
		}
		public virtual string Direccion
		{
			get
			{
				return _Direccion;
			}
			set
			{
				_Direccion = value;
			}
		}
		public virtual int CodigoPostal
		{
			get
			{
				return _CodigoPostal;
			}
			set
			{
				_CodigoPostal = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}

		public CClientes()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Unidad","Empresa" };
		}
		/*public override string[] GetCamposSelectJoin()
		{
			return new string[] { "Unidad" };
		}*/
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Unidad", "Empresa" };
		}
	}// CClientes ends.
}// Entidades ends.
