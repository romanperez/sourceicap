using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{ 
    
    public partial class CSisLogErrores : CBase,ISisLogErrores
    {
        /// <summary>
        /// Lista de propiedades clase [ CSisLogErrores].
        /// </summary>		
		 protected int _LogErrorId;
		protected DateTime? _LogFecha;
		protected string _LogArchivo;
		protected string _LogFuncion;
		protected int? _LogLinea;
		protected int? _LogColumna;
		protected string _LogPila;
		protected string _LogError;
		protected string _LogInfo;
        
		public virtual  int Log_Error_Id
		{
		 get{return _LogErrorId;}
		 set{_LogErrorId = value;}
		}
		public virtual  DateTime? Log_Fecha
		{
		 get{return _LogFecha;}
		 set{_LogFecha = value;}
		}
		public virtual  string Log_Archivo
		{
		 get{return _LogArchivo;}
		 set{_LogArchivo = value;}
		}
		public virtual  string Log_Funcion
		{
		 get{return _LogFuncion;}
		 set{_LogFuncion = value;}
		}
		public virtual  int? Log_Linea
		{
		 get{return _LogLinea;}
		 set{_LogLinea = value;}
		}
		public virtual  int? Log_Columna
		{
		 get{return _LogColumna;}
		 set{_LogColumna = value;}
		}
		public virtual  string Log_Pila
		{
		 get{return _LogPila;}
		 set{_LogPila = value;}
		}
		public virtual  string Log_Error
		{
		 get{return _LogError;}
		 set{_LogError = value;}
		}
		public virtual  string Log_Info
		{
		 get{return _LogInfo;}
		 set{_LogInfo = value;}
		}
			
	public CSisLogErrores()
	{	  
	}
    }// CSisLogErrores ends.
}// Entidades ends.
