using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{			
    public partial class CUsuariosData 
    {
		 public virtual CEmpleados Empleado
		 {
			 set;
			 get;
		 }
		 public virtual CPerfiles Perfil
		 {
			 set;
			 get;
		 }
		 public virtual CDepartamentos Departamento
		 {
			 set;
			 get;
		 }
		 public virtual CUnidades Unidad
		 {
			 set;
			 get;
		 }
		 public virtual CEmpresas Empresa
		 {
			 set;
			 get;
		 }
		 public virtual string GetInfoUser()
		 {
			 if(Empleado == null) return CIdioma.TraduceCadena("CUsuariosDataNoEmpleado");
			 return Empleado.NombreCompletoPerfil(this.Perfil);
		 }
		 
		 public virtual string GetInfoEmpresa()
		 {
			 string noEmpresa =String.Empty;
			 string noUnidad = String.Empty;
			 string noDepartamento = String.Empty;			
			 if(Empresa == null)
			 {
				 noEmpresa = CIdioma.TraduceCadena("CUsuariosDataNoEmpresa");
			 }
			 else
			 {
				 noEmpresa = Empresa.Empresa;
			 }
			 if(Unidad == null)
			 {
				 noUnidad = CIdioma.TraduceCadena("CUsuariosDataNoUnidad");
			 }
			 else
			 {
				 noUnidad = Unidad.Unidad;
			 }
			 if(Departamento == null)
			 {
				 noDepartamento = CIdioma.TraduceCadena("CUsuariosDataNoDepartamento");
			 }
			 else
			 {
				  noDepartamento =Departamento.Nombre; 
			 }


			 return String.Format("{0} ({1} {2})",noEmpresa,noUnidad,noDepartamento);
		 }
		 public virtual string GetEmpresaLogo()
		 {
			 if(Empresa != null && !String.IsNullOrEmpty(Empresa.Logo))
			 {
				 return String.Format("~/{0}",Empresa.Logo);
			 }
			 return "../Content/Imagenes/user.png";
		 }
    }// CUsuarios ends.
}// Entidades ends.
