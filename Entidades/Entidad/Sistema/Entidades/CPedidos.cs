using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{

	public partial class CPedidos : CBase, IPedidos
	{
		/// <summary>
		/// Lista de propiedades clase [ CPedidos].
		/// </summary>		
		protected int _IdPedido;
		protected int _IdUnidad;
		//protected int _IdContrato;
		//protected string _Pedido;
		//protected string _Descripcion;
		protected DateTime _FechaSolicitado;
		protected DateTime? _FechaLlegada;
		protected bool _Estatus;

		public virtual int IdPedido
		{
			get
			{
				return _IdPedido;
			}
			set
			{
				_IdPedido = value;
			}
		}
		public virtual int? IdProyecto
		{
			set;
			get;
		}
		public virtual int? IdContrato
		{
			set;
			get;
		}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		//public virtual  int IdContrato
		//{
		// get{return _IdContrato;}
		// set{_IdContrato = value;}
		//}
		//public virtual  string Pedido
		//{
		// get{return _Pedido;}
		// set{_Pedido = value;}
		//}
		//public virtual  string Descripcion
		//{
		// get{return _Descripcion;}
		// set{_Descripcion = value;}
		//}
		public virtual DateTime FechaSolicitado
		{
			get
			{
				return _FechaSolicitado;
			}
			set
			{
				_FechaSolicitado = value;
			}
		}
		public virtual DateTime? FechaLlegada
		{
			get
			{
				return _FechaLlegada;
			}
			set
			{
				_FechaLlegada = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			get;
			set;
		}

		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string Proyecto
		{
			get;
			set;
		}
		public virtual string Contrato
		{
			get;
			set;
		}
		public virtual List<CPedidosDetalles> Detalle
		{
			set;
			get;
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "Unidad", "Detalle", "Proyecto", "Contrato" };
		}
		public CPedidos()
		{
		}
	}// CPedidos ends.
}// Entidades ends.
