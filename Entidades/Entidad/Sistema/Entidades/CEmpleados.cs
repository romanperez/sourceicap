using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{

	public partial class CEmpleados : CBase, IEmpleados
	{
		/// <summary>
		/// Lista de propiedades clase [ CEmpleados].
		/// </summary>		
		protected int _IdEmpleado;
		protected int _IdDepartamento;
		protected string _Nombre;
		protected string _ApellidoPaterno;
		protected string _ApellidoMaterno;
		protected string _Matricula;
		protected bool _Estatus;
		protected string _DepartamentoNombre;
		protected int _IdEmpresa;
		protected string _Empresa;

		public virtual int IdEmpleado
		{
			get
			{
				return _IdEmpleado;
			}
			set
			{
				_IdEmpleado = value;
			}
		}
		public virtual int IdDepartamento
		{
			get
			{
				return _IdDepartamento;
			}
			set
			{
				_IdDepartamento = value;
			}
		}

		public virtual string DepartamentoNombre
		{
			get
			{
				return _DepartamentoNombre;
			}
			set
			{
				_DepartamentoNombre = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual string ApellidoPaterno
		{
			get
			{
				return _ApellidoPaterno;
			}
			set
			{
				_ApellidoPaterno = value;
			}
		}
		public virtual string ApellidoMaterno
		{
			get
			{
				return _ApellidoMaterno;
			}
			set
			{
				_ApellidoMaterno = value;
			}
		}
		public virtual string Matricula
		{
			get
			{
				return _Matricula;
			}
			set
			{
				_Matricula = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}


		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}
		public virtual int IdUnidad
		{
			get;
			set;
		}
		public virtual int IdTecnico
		{
			set;
			get;
		}
		public virtual int IdGrado
		{
			set;
			get;
		}
		public virtual string Grado
		{
			set;
			get;
		}
		public virtual string Especialidades
		{
			set;
			get;
		}
		public virtual  string Unidad
		{
			get;
			set;
		}
		public virtual string Email
		{
			set;
			get;
		}
		
		public virtual string Telefono
		{
			set;
			get;
		}		
		public virtual string Celular
		{
			set;
			get;
		} 
		public CEmpleados()
		{
		}
		public virtual string NombreCompleto()
		{
			return String.Format("{0} {1} {2}", _Nombre, _ApellidoPaterno, _ApellidoMaterno);
		}
		public virtual string NombreCompletoPerfil(IPerfiles perfil)
		{
			if(perfil == null)return  NombreCompleto();
			return String.Format("{0} ({1})", NombreCompleto(), perfil.Nombre);
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "DepartamentoNombre", "IdEmpresa", "Empresa", "IdUnidad", "Unidad", "IdTecnico", "Especialidades", "IdGrado", "Grado" };
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { /*"DepartamentoNombre",*/ "IdDepartamento", "IdEmpresa", "Empresa", "IdUnidad", "Unidad" };
		}
	}// CEmpleados ends.
}// Entidades ends.
