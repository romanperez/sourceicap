using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:41 p. m.*/
namespace Entidades
{

	public partial class CEstatus : CBase, IEstatus
	{
		/// <summary>
		/// Lista de propiedades clase [ CEstatus].
		/// </summary>		
		protected int _IdEstatus;
		protected int _IdEmpresa;
		protected string _Clasificacion;
		protected string _Nombre;
		protected bool _Estatus;

		public virtual int IdEstatus
		{
			get
			{
				return _IdEstatus;
			}
			set
			{
				_IdEstatus = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Clasificacion
		{
			get
			{
				return _Clasificacion;
			}
			set
			{
				_Clasificacion = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public CEstatus()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
	}// CEstatus ends.
}// Entidades ends.
