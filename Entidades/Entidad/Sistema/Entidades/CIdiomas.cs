﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class CIdioma:IIdioma
	{
		public const string DEFAULT = "es-MX";
		public const string DEFAULT_NAME = "español";
		public const string COOKIE_NAME_LANGUAGE = "icap_languaje";
		public const int COOKIE_EXPIRES = 1; /*tIEMPO DE VIDA DE LA COOKIE DE ICAP IDIOMA EN DIAS*/
		public int IdIdioma
		{
			set;
			get;
		}
		public string Idioma
		{
			set;
			get;
		}
		public string Codigo
		{
			set;
			get;
		}
		public bool Estatus
		{
			set;
			get;
		}
		/// <summary>
		/// Regresa la lista de los idiomas soportados por ICAP Web
		/// </summary>
		/// <returns>Lista de isiomas</returns>
		public static IEnumerable<IIdioma> GetAllIdiomas()
		{
			List<IIdioma> _idiomas = new List<IIdioma>();
			_idiomas.Add(new CIdioma()
			{
				IdIdioma = 1,
				Codigo = "es-MX",
				Idioma = "Español",
				Estatus = true
			});			
			_idiomas.Add(new CIdioma()
			{
				IdIdioma = 2,
				Codigo = "en-US",
				Idioma = "English",
				Estatus = true
			});
			return _idiomas.Where(i=> i.Estatus == true);
		}
		/// <summary>
		/// Regresa el idioma predeterminado por ICAP web
		/// </summary>
		/// <returns>Objeto CIdioma predeterminado</returns>
		public static IIdioma DefaultIdioma()
		{
			return new CIdioma(){ IdIdioma=1, Codigo=CIdioma.DEFAULT, Idioma = CIdioma.DEFAULT_NAME, Estatus=true};
		}
		/// <summary>
		///  Obtiene el texto en el recurso dependiendo del Idioma seleccionado si no lo encuentra se regresa el texto tal cual fue enviado a buscar.
		/// </summary>
		/// <param name="cadena">Texto a traducir.</param>
		/// <returns>Texto traducido.</returns>
		public static string TraduceCadena(string cadena)
		{
			ResourceManager rm = new ResourceManager(typeof(Idiomas.IcapWeb));			
			string texto = rm.GetString(cadena);
			if(String.IsNullOrEmpty(texto))
			{
				return cadena;
			}
			else
			{
				return texto;
			}
		}
	}
}