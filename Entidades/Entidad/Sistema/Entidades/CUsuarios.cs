using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{		
	///<summary>
	///Catalogo de usuarios para el inicio de sesion.
	///</summary>
    public partial class CUsuarios : CBase,IUsuarios
    {
        /// <summary>
        /// Lista de propiedades clase [ CUsuarios].
        /// </summary>		
		protected int _IdUsuario;
		protected int _IdPerfil;
		protected int? _IdEmpleado;
		protected string _Usuario;
		protected string _Clave;
		protected string _ClaveConfrim;
		protected DateTime _FechaRegistro;		
		protected bool _Estatus;
		protected CUsuariosData _Data;
		protected string _InicioSession;
		protected string _Perfil;
		protected string _NombreEmpleado;
			
		public virtual  int IdUsuario
		{
		 get{return _IdUsuario;}
		 set{_IdUsuario = value;}
		}
		public virtual int? IdEmpleado
		{
			get
			{
				return _IdEmpleado;
			}
			set
			{
				_IdEmpleado = value;
			}
		}
		public virtual  int IdPerfil
		{
		 get{return _IdPerfil;}
		 set{_IdPerfil = value;}
		}
		public virtual  string Usuario
		{
		 get{return _Usuario;}
		 set{_Usuario = value;}
		}
		public virtual  string Clave
		{
		 get{return _Clave;}
		 set{_Clave = value;}
		}
		public virtual string ClaveConfirm
		{
			get
			{
				return _ClaveConfrim;
			}
			set
			{
				_ClaveConfrim = value;
			}
		}
		public virtual  DateTime FechaRegistro
		{
		 get{return _FechaRegistro;}
		 set{_FechaRegistro = value;}
		}
		public virtual CUsuariosData Data
		{
		 get{return _Data;}
			set
			{
				_Data = value;
			}
		}
		public virtual bool Estatus
		{			
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}		
		public virtual string InicioSession
		{
			get
			{
				return _InicioSession;
			}
			set
			{
				_InicioSession = value;
			}
		}
		public virtual string Perfil
		{
			get
			{
				return _Perfil;
			}
			set
			{
				_Perfil = value;
			}
		}
		public virtual string NombreEmpleado
		{
			get
			{
				return _NombreEmpleado;
			}
			set
			{
				_NombreEmpleado = value;
			}
		}
		public virtual int IdMenuCaller
		{
			set;
			get;
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public virtual string Unidad
		{
			set;
			get;
		}
		public virtual string Departamento
		{
			set;
			get;
		}
		public CUsuarios()
		{	  
			FechaRegistro = DateTime.Now;
			_Data = new CUsuariosData();
			IdPerfil=0;
			IdUsuario=0;   
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Data", "InicioSession", "Clave", "Perfil", "NombreEmpleado", "ClaveConfirm", "IdMenuCaller" ,
			"Empresa","Unidad","Departamento"};
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "NombreEmpleado", "Perfil", "InicioSession", "Estatus", "FechaRegistro", "ClaveConfirm","Clave" };
		}
    }// CUsuarios ends.
}// Entidades ends.
