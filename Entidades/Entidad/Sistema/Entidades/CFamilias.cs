using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 26/01/2017 04:27:07 p. m.*/
namespace Entidades
{

	public partial class CFamilias : CBase, IFamilias
	{
		/// <summary>
		/// Lista de propiedades clase [ CFamilias].
		/// </summary>		
		protected int _IdFamilia;
		protected string _Familia;
		protected string _Descripcion;
		protected bool _Estatus;
		protected int _IdEmpresa;

		public virtual int IdFamilia
		{
			get
			{
				return _IdFamilia;
			}
			set
			{
				_IdFamilia = value;
			}
		}
		public virtual string Familia
		{
			get
			{
				return _Familia;
			}
			set
			{
				_Familia = value;
			}
		}
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Empresa
		{
			get;
			set;
		}
		public CFamilias()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] {"Empresa" };
		} 
	}// CFamilias ends.
}// Entidades ends.
