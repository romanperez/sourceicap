using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:44 p. m.*/
namespace Entidades
{

	public partial class CTecnicosGrados : CBase, ITecnicosGrados
	{
		/// <summary>
		/// Lista de propiedades clase [ CTecnicosGrados].
		/// </summary>		
		protected int _IdGrado;
		protected int _IdEmpresa;
		protected string _Codigo;
		protected string _Nombre;
		protected bool _Estatus;

		public virtual int IdGrado
		{
			get
			{
				return _IdGrado;
			}
			set
			{
				_IdGrado = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Codigo
		{
			get
			{
				return _Codigo;
			}
			set
			{
				_Codigo = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public CTecnicosGrados()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
	}// CTecnicosGrados ends.
}// Entidades ends.
