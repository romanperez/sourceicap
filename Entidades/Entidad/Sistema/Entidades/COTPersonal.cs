using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{

	public partial class COTPersonal : CBase, IOTPersonal
	{
		/// <summary>
		/// Lista de propiedades clase [ COTPersonal].
		/// </summary>		
		protected int _IdOTPersonal;
		protected int _IdOrdenTrabajo;
		protected int _IdTecnico;
		protected bool _Estatus;

		public virtual int IdOTPersonal
		{
			get
			{
				return _IdOTPersonal;
			}
			set
			{
				_IdOTPersonal = value;
			}
		}
		public virtual int IdOrdenTrabajo
		{
			get
			{
				return _IdOrdenTrabajo;
			}
			set
			{
				_IdOrdenTrabajo = value;
			}
		}
		public virtual int IdTecnico
		{
			get
			{
				return _IdTecnico;
			}
			set
			{
				_IdTecnico = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string NombreEmpleado
		{
			set;
			get;
		}
		public virtual string NombreEspecialidad
		{
			get;
			set;
		}
		public virtual string NombreGrado
		{
			get;
			set;
		}
		public COTPersonal()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "NombreEmpleado", "NombreEspecialidad", "NombreGrado" };

		}
	}// COTPersonal ends.
}// Entidades ends.
