using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/01/2017 02:07:42 p. m.*/
namespace Entidades
{

	public partial class CTipoCambios : CBase, ITipoCambios
	{
		/// <summary>
		/// Lista de propiedades clase [ CTipoCambios].
		/// </summary>		
		protected int _IdTipoCambio;
		protected int _IdEmpresa;
		protected int _IdMonedaOrigen;
		protected int _IdMonedaDestino;
		protected DateTime _FechaInicio;
		protected DateTime _FechaFin;
		protected double _Valor;
		protected bool _Estatus;

		public virtual int IdTipoCambio
		{
			get
			{
				return _IdTipoCambio;
			}
			set
			{
				_IdTipoCambio = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdMonedaOrigen
		{
			get
			{
				return _IdMonedaOrigen;
			}
			set
			{
				_IdMonedaOrigen = value;
			}
		}
		public virtual int IdMonedaDestino
		{
			get
			{
				return _IdMonedaDestino;
			}
			set
			{
				_IdMonedaDestino = value;
			}
		}
		public virtual DateTime FechaInicio
		{
			get
			{
				return _FechaInicio;
			}
			set
			{
				_FechaInicio = value;
			}
		}
		public virtual DateTime FechaFin
		{
			get
			{
				return _FechaFin;
			}
			set
			{
				_FechaFin = value;
			}
		}
		public virtual double Valor
		{
			get
			{
				return _Valor;
			}
			set
			{
				_Valor = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public virtual string MonedaOrigen
		{
			set;
			get;
		}
		public virtual string MonedaDestino
		{
			set;
			get;
		}
		public virtual string CodigoMonedaOrigen
		{
			get;
			set;
		}
		public virtual string CodigoMonedaDestino
		{
			set;
			get;
		}
		public virtual double Cantidad
		{
			set;
			get;
		}
		public CTipoCambios()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "MonedaOrigen", "MonedaDestino", "CodigoMonedaOrigen", "CodigoMonedaDestino", "Cantidad" };
		}
	}// CTipoCambios ends.
}// Entidades ends.
