using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{

	public partial class CUnidades : CBase, IUnidades
	{
		/// <summary>
		/// Lista de propiedades clase [ CUnidades].
		/// </summary>		
		protected int _IdUnidad;
		protected int _IdEmpresa;
		protected string _Empresa;
		protected string _Unidad;
		protected string _Direccion;
		protected string _Telefono;
		protected bool _Estatus;
		//protected CEmpresas _Empresa;
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}
		public virtual string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}
		public virtual string Direccion
		{
			get
			{
				return _Direccion;
			}
			set
			{
				_Direccion = value;
			}
		}
		public virtual string Telefono
		{
			get
			{
				return _Telefono;
			}
			set
			{
				_Telefono = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		/*public virtual CEmpresas Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}*/
		public CUnidades()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa"};
		}
		/*public override string[] GetCamposSelectJoin()
		{
			return new string[] { "EmpresaNombre" };
		}*/
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Empresa","Estatus" };
		}
	}// CUnidades ends.
}// Entidades ends.
