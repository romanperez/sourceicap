using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 21/02/2017 09:48:10 a. m.*/
namespace Entidades
{

	public partial class CContratosDetallesInsumos : CBase, IContratosDetallesInsumos
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratosDetallesInsumos].
		/// </summary>		
		protected int _IdInsumoContrato;
		protected int _IdContrato;
		protected int _IdConcepto;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected double _Costo;

		public virtual int IdInsumoContrato
		{
			get
			{
				return _IdInsumoContrato;
			}
			set
			{
				_IdInsumoContrato = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual double Costo
		{
			get
			{
				return _Costo;
			}
			set
			{
				_Costo = value;
			}
		}
		public virtual string Codigo
		{
			get;
			set;
		}
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual int IdCapitulo
		{
			set;
			get;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}   
		public CContratosDetallesInsumos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Descripcion", "Moneda", "IdCapitulo", "IdMoneda" };
		}
	}// CContratosDetallesInsumos ends.
}// Entidades ends.
