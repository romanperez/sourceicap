using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{ 
    
    public partial class CParametros : CBase,IParametros
    {
        /// <summary>
        /// Lista de propiedades clase [ CParametros].
        /// </summary>		
	protected int _IdParametro;
		protected string _Nombre;
		protected string _Valor;
		protected bool _Estatus;
		protected string _Descripcion;
        
			public virtual  int IdParametro
		{
		 get{return _IdParametro;}
		 set{_IdParametro = value;}
		}
		public virtual  string Nombre
		{
		 get{return _Nombre;}
		 set{_Nombre = value;}
		}
		public virtual  string Valor
		{
		 get{return _Valor;}
		 set{_Valor = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
		public virtual  string Descripcion
		{
		 get{return _Descripcion;}
		 set{_Descripcion = value;}
		}
			
	public CParametros()
	{	  
	}
    }// CParametros ends.
}// Entidades ends.
