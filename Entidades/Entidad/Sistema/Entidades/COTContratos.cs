using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Entidades
{

	public partial class COTContratos : CBase, IOTContratos
	{
		/// <summary>
		/// Lista de propiedades clase [ COTContratos].
		/// </summary>		
		protected int _IdContratoOrden;
		protected int _IdOrdenTrabajo;
		protected int _IdContrato;

		public virtual int IdContratoOrden
		{
			get
			{
				return _IdContratoOrden;
			}
			set
			{
				_IdContratoOrden = value;
			}
		}
		public virtual int IdOrdenTrabajo
		{
			get
			{
				return _IdOrdenTrabajo;
			}
			set
			{
				_IdOrdenTrabajo = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}

		public virtual string Contrato
		{
			get;
			set;
		}
		public COTContratos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Contrato" };
		}
	}// COTContratos ends.
}// Entidades ends.
