using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public class CPermisosJs:CBase,IPermisosJs
	{
		public int IdPerfil
		{
			set;
			get;
		}
		public string[] MenuVer
		{
			set;
			get;
		}
		public string[] MenuPermisos
		{
			set;
			get;
		}
	}
}// Entidades ends.
