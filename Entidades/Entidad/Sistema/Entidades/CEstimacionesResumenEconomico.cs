using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 03/10/2017 11:24:11 a. m.*/
namespace Entidades
{
    public partial class CEstimacionesResumenEconomico : CEstimaciones,IEstimacionesResumenEconomico
    {
        public virtual string EstimacionTitle { set; get; }
        public virtual DateTime? FechaInicio2 { set; get; }
        public virtual DateTime? FechaFin2 { set; get; }
        public virtual double ImporteAcumuladoAnterior { set; get; }
        public virtual double ImporteAnticipo { set; get; }
        public virtual double ImporteAmortizacionAnticipo { set; get; }
        public virtual double ImportePagar { set; get; }
    }// CEstimaciones ends.
}// Entidades ends.
