using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 02/06/2017 11:10:18 a. m.*/
namespace Entidades
{

	public partial class CCotizacionesPersonalCliente : CBase, ICotizacionesPersonalCliente
	{
		/// <summary>
		/// Lista de propiedades clase [ CCotizacionesPersonalCliente].
		/// </summary>		
		protected int _IdCotizacionPersonalCliente;
		protected int _IdCotizacion;
		protected int _IdEstatus;
		protected int _IdPersonalCliente;

		public virtual int IdCotizacionPersonalCliente
		{
			get
			{
				return _IdCotizacionPersonalCliente;
			}
			set
			{
				_IdCotizacionPersonalCliente = value;
			}
		}
		public virtual int IdCotizacion
		{
			get
			{
				return _IdCotizacion;
			}
			set
			{
				_IdCotizacion = value;
			}
		}
		public virtual int IdEstatus
		{
			get
			{
				return _IdEstatus;
			}
			set
			{
				_IdEstatus = value;
			}
		}
		public virtual int IdPersonalCliente
		{
			get
			{
				return _IdPersonalCliente;
			}
			set
			{
				_IdPersonalCliente = value;
			}
		}
		public virtual string Nombre
		{
			get;
			set;
		}
		public virtual string ApellidoPaterno
		{
			get;
			set;
		}
		public virtual string ApellidoMaterno
		{
			get;
			set;
		}
		public virtual string Puesto
		{
			get;
			set;
		}
		public virtual string NombreEstatus
		{
			get;
			set;
		}
		public virtual string NombreCompleto
		{
			get
			{
				return String.Format("{0} {1} {2}", this.Nombre, this.ApellidoPaterno, this.ApellidoPaterno);
			}
		}

		public CCotizacionesPersonalCliente()
		{
			
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Nombre", "ApellidoPaterno", "ApellidoMaterno", "Puesto", "NombreEstatus", "NombreCompleto" };
		}

	}// CCotizacionesPersonalCliente ends.
}// Entidades ends.
