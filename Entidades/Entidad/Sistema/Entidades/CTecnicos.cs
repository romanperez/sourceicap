using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:44 p. m.*/
namespace Entidades
{

	public partial class CTecnicos : CBase, ITecnicos
	{
		/// <summary>
		/// Lista de propiedades clase [ CTecnicos].
		/// </summary>		
		protected int _IdTecnico;
		protected int _IdEmpleado;
		protected int _IdEspecialidad;
		protected int _IdGrado;
		protected List<CEspecialidades> _Especialidades;
		public virtual int IdTecnico
		{
			get
			{
				return _IdTecnico;
			}
			set
			{
				_IdTecnico = value;
			}
		}
		public virtual int IdEmpleado
		{
			get
			{
				return _IdEmpleado;
			}
			set
			{
				_IdEmpleado = value;
			}
		}
		public virtual int IdEspecialidad
		{
			get
			{
				return _IdEspecialidad;
			}
			set
			{
				_IdEspecialidad = value;
			}
		}
		public virtual int IdGrado
		{
			get
			{
				return _IdGrado;
			}
			set
			{
				_IdGrado = value;
			}
		}
		public virtual string NombreEmpleado
		{
			set;
			get;
		}
		public virtual string NombreEspecialidad
		{
			set;
			get;
		}
		public virtual string NombreGrado
		{
			set;
			get;
		}
		public virtual int IdEmpresa
		{
			set;
			get;
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public virtual List<CEspecialidades> Especialidades
		{
			get
			{
				return _Especialidades;
			}
			set
			{
				_Especialidades = value;
			}
		}
		public CTecnicos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "NombreEmpleado", "NombreEspecialidad", "NombreGrado", "IdEmpresa", "Empresa", "Especialidades" };
		}
	}// CTecnicos ends.
}// Entidades ends.