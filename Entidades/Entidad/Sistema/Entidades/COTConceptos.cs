using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{

	public partial class COTConceptos : CBase, IOTConceptos
	{
		/// <summary>
		/// Lista de propiedades clase [ COTConceptos].
		/// </summary>		
		protected int _IdOTConcepto;
		protected int _IdOrdenTrabajo;
		protected int _IdContratoDetalles;
		protected int _IdConcepto;		
		public virtual int IdOTConcepto
		{
			get
			{
				return _IdOTConcepto;
			}
			set
			{
				_IdOTConcepto = value;
			}
		}
		public virtual int IdOrdenTrabajo
		{
			get
			{
				return _IdOrdenTrabajo;
			}
			set
			{
				_IdOrdenTrabajo = value;
			}
		}
		public virtual int IdContratosDetalles
		{
			get
			{
				return _IdContratoDetalles;
			}
			set
			{
				_IdContratoDetalles = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual double Cantidad
		{
			set;
			get;
		}
		public virtual double CantidadProgramada
		{
			get;
			set;
		}
		public virtual double CantidadEjecutada
		{
			get;
			set;
		}
		public virtual double CantidadProgramadaOT
		{
			get;
			set;
		}
		public virtual double CantidadEjecutadaOT
		{
			get;
			set;
		}
		public virtual string UnidadMedida
		{
			set;
			get;
		}
		public virtual string Concepto
		{
			set;
			get;
		}
		public virtual string Descripcion
		{
			set;
			get;
		}
		public virtual string Contrato
		{
			set;
			get;
		}
		public virtual List<COTConceptosEjecucion> Ejecuciones
		{
			set;
			get;
		}
		public virtual  CConceptos InfoConcepto
		{
			set;
			get;
		}
		public virtual string Ubicacion
		{
			set;
			get;
		}
		public virtual string Clasificacion
		{
			set;
			get;
		}
		public virtual int IdConceptoClasificacion
		{
			get;
			set;
		}
		public virtual List<CContratosDetallesInsumos2> Insumos
		{
			set;
			get;
		}
		public virtual int Partida
		{
			set;
			get;
		}
		public virtual bool EsAdicional
		{
			get;
			set;
		}	
		public COTConceptos()
		{
			InfoConcepto = new CConceptos();
			Insumos = new List<CContratosDetallesInsumos2>();
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Cantidad", "UnidadMedida", "Concepto", "Descripcion", 
				"CantidadProgramada", "CantidadEjecutada", 
				"Contrato", "Ejecuciones","InfoConcepto","Ubicacion","Clasificacion","IdConceptoClasificacion" ,
			   "Partida","Insumos","EsAdicional",
			   "CantidadProgramadaOT","CantidadEjecutadaOT"};
		}
	}// COTConceptos ends.
}// Entidades ends.
