using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:39 p. m.*/
namespace Entidades
{
	///<summary>
	///Catalogo Conceptos
	///</summary>
	public partial class CConceptos : CBase, IConceptos
	{
		/// <summary>
		/// Lista de propiedades clase [ CConceptos].
		/// </summary>		
		protected int _IdConcepto;
		protected int _IdEmpresa;		
		protected int? _IdCapitulo;
		protected int? _IdUnidadMedida;
		protected int? _IdConceptoClasificacion;
		protected double _Precio;
		protected string _Concepto;
		protected string _Descripcion;
		protected bool _Estatus;
		protected DateTime _Fecha;
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int? IdUnidadMedida
		{
			get
			{
				return _IdUnidadMedida;
			}
			set
			{
				_IdUnidadMedida = value;
			}
		}
		public virtual int? IdConceptoClasificacion
		{
			get
			{
				return _IdConceptoClasificacion;
			}
			set
			{
				_IdConceptoClasificacion = value;
			}
		}
		public virtual string Concepto
		{
			get
			{
				return _Concepto;
			}
			set
			{
				_Concepto = value;
			}
		}
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		
		public virtual int? IdCapitulo
		{
			get
			{
				return _IdCapitulo;
			}
			set
			{
				_IdCapitulo = value;
			}
		}
		public virtual double Precio
		{
			get
			{
				return _Precio;
			}
			set
			{
				_Precio = value;
			}
		}
		public virtual DateTime Fecha
		{
			set
			{
				_Fecha = value;
			}
			get
			{
				return _Fecha;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual List<CConceptosInsumos> Insumos
		{
			set;
			get;
		}
		public virtual string Capitulo
		{
			get;
			set;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}
		public virtual string Moneda
		{
			get;
			set;
		}		
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual string UnidadMedida
		{
			get;
			set;
		}
		public virtual string Clasificacion
		{
			get;
			set;
		}
		public virtual bool EsValido
		{
			set;
			get;
		}
		public virtual string MotivoNoValido
		{
			set;
			get;
		}
		public CConceptos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa", "Insumos", "Capitulo", 
				                    "Moneda", "UnidadMedida", "Clasificacion", 
										  "CodigoMoneda", "IdMoneda","EsValido",
										  "MotivoNoValido" };
		}
		public virtual string ToXml()
		{
			StringBuilder sb = new StringBuilder("");
			sb.Append(String.Format("<Concepto><IdConcepto>{0}</IdConcepto><Precio>{1}</Precio><EsAdicional>{2}</EsAdicional>",
											this.IdConcepto,
											this.Precio,
											"0"));
			if(this.Insumos != null && this.Insumos.Count() > 0)
			{
				sb.Append("<Insumos>");
				this.Insumos.ForEach(i => sb.Append(i.ToXml()));
				sb.Append("</Insumos>");
			}
			sb.Append("</Concepto>");
			return sb.ToString();
		}
	}// CConceptos ends.
}// Entidades ends.
