using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{

	public partial class CMovimientosDetalles : CBase, IMovimientosDetalles
	{
		/// <summary>
		/// Lista de propiedades clase [ CMovimientosDetalles].
		/// </summary>		
		protected int _IdMovimientoDetalle;
		protected int _IdMovimiento;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected double _Precio;
		protected double _CantidadExistenciaInsumo;
		protected string _Observacion;

		public virtual int IdMovimientoDetalle
		{
			get
			{
				return _IdMovimientoDetalle;
			}
			set
			{
				_IdMovimientoDetalle = value;
			}
		}
		public virtual int IdMovimiento
		{
			get
			{
				return _IdMovimiento;
			}
			set
			{
				_IdMovimiento = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual double CantidadExistenciaInsumo
		{
			get
			{
				return _CantidadExistenciaInsumo;
			}
			set
			{
				_CantidadExistenciaInsumo = value;
			}
		}
		public virtual double Precio
		{
			get
			{
				return _Precio;
			}
			set
			{
				_Precio = value;
			}
		}
		public virtual string Observacion
		{
			get
			{
				return _Observacion;
			}
			set
			{
				_Observacion = value;
			}
		}
		public virtual string Codigo
		{
			set;
			get;
		}
		public virtual string Descripcion
		{
			set;
			get;
		}
		public virtual string Serie
		{
			set;
			get;
		}
		
		public virtual string Moneda
		{
			get;
			set;
		}		
		public virtual string CodigoMoneda
		{
			get;
			set;
		}		
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string UnidadMedidaDescripcion
		{
			set;
			get;
		}
		public virtual int IdPedidoDetalle
		{
			set;
			get;
		}
		public virtual string Concepto
		{
			get;
			set;
		}
		public CMovimientosDetalles()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Concepto", "Codigo", "Descripcion", "CantidadExistenciaInsumo", "Moneda", "CodigoMoneda", "Unidad", "UnidadMedidaDescripcion", "Serie", "IdPedidoDetalle" };
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Codigo", "Descripcion", "CantidadExistenciaInsumo", "Moneda", "CodigoMoneda", "Unidad", "UnidadMedidaDescripcion" };
		}
	}// CMovimientosDetalles ends.
}// Entidades ends.
