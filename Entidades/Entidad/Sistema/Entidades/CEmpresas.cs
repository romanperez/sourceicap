using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{

	public partial class CEmpresas : CBase, IEmpresas
	{
		/// <summary>
		/// Longitud del rfc de persona moral.
		/// </summary>
		public const int LEN_MORAL = 12;
		/// <summary>
		/// Longitud del rfc de persona fisica.
		/// </summary>
		public const int LEN_FISICA = 13;
		/// <summary>
		/// Longitud del codigo postal.
		/// </summary>
		public const int LENGTH_CP = 5;
		/// <summary>
		/// Lista de propiedades clase [ CEmpresas].
		/// </summary>		
		protected int _IdEmpresa;
		protected string _Empresa;
		protected string _Direccion;
		protected string _Telefono;
		protected string _CorreoElectronico;
		protected string _SitioWeb;
		protected string _RFC;
		protected int _CodigoPostal;
		protected string _Logo;
		protected bool _Estatus;

		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}
		public virtual string Direccion
		{
			get
			{
				return _Direccion;
			}
			set
			{
				_Direccion = value;
			}
		}
		public virtual string Telefono
		{
			get
			{
				return _Telefono;
			}
			set
			{
				_Telefono = value;
			}
		}
		public virtual string CorreoElectronico
		{
			get
			{
				return _CorreoElectronico;
			}
			set
			{
				_CorreoElectronico = value;
			}
		}
		public virtual string SitioWeb
		{
			get
			{
				return _SitioWeb;
			}
			set
			{
				_SitioWeb = value;
			}
		}
		public virtual string RFC
		{
			get
			{
				return _RFC;
			}
			set
			{
				_RFC = value;
			}
		}
		public virtual int CodigoPostal
		{
			get
			{
				return _CodigoPostal;
			}
			set
			{
				_CodigoPostal = value;
			}
		}
		public virtual string Logo
		{
			get
			{
				return _Logo;
			}
			set
			{
				_Logo = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}

		public CEmpresas()
		{
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Estatus", "Logo", "IdEmpresa" };
		}
	}// CEmpresas ends.
}// Entidades ends.
