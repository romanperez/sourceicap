using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{

	public partial class CPermisos : CBase, IPermisos
	{
		public virtual List<CMenus> Menus
		{
			set;
			get;
		}
		public virtual List<CPerfiles> Perfiles
		{
			set;
			get;
		}
		public virtual List<CAcciones> Acciones
		{
			set;
			get;
		}
	}// CEmpresas ends.
}// Entidades ends.
