using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{

	public partial class CPedidosDetalles : CBase, IPedidosDetalles
	{
		/// <summary>
		/// Lista de propiedades clase [ CPedidosDetalles].
		/// </summary>		
		protected int _IdPedidoDetalle;
		protected int _IdPedido;
		protected int _IdInsumo;
		protected double _Cantidad;

		public virtual int IdPedidoDetalle
		{
			get
			{
				return _IdPedidoDetalle;
			}
			set
			{
				_IdPedidoDetalle = value;
			}
		}
		public virtual int IdPedido
		{
			get
			{
				return _IdPedido;
			}
			set
			{
				_IdPedido = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual double?  CantidadEnviada
		{
			get;
			set;
		}
		public virtual double? CantidadRecibida
		{
			get;
			set;
		}
		public virtual double CantidadEnviada2
		{
			get
			{
				return (CantidadEnviada == null) ? 0 : Convert.ToDouble(CantidadEnviada);
			}
		}
		public virtual double PorcentajeEnvio
		{
			get
			{
				return (Cantidad<=0 )? 0 :((CantidadEnviada2 * 100) /Cantidad);
			}
		}
		public virtual double CantidadTraspasar
		{
			set;
			get;
		}
		public virtual string Codigo
		{
			get;
			set;
		}
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Serie
		{
			get;
			set;
		}		
		public virtual string UnidadMedidaDescripcion
		{
			set;
			get;
		}
		public virtual int? IdConcepto
		{
			set;
			get;
		}
		public virtual string Concepto
		{
			get;
			set;
		}
		public virtual int IdContrato
		{
			set;
			get;
		}
		public virtual string Contrato
		{
			get;
			set;
		}
		public CPedidosDetalles()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Descripcion", "Serie", "UnidadMedidaDescripcion", 
				                   "CantidadTraspasar", "CantidadEnviada2", "Concepto", 
										 "IdContrato", "PorcentajeEnvio","Contrato" };
		}
	}// CPedidosDetalles ends.
}// Entidades ends.
