using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{

	public partial class CConceptosInsumos : CBase, IConceptosInsumos
	{
		/// <summary>
		/// Lista de propiedades clase [CConceptosInsumos].
		/// </summary>		
		protected int _IdConceptoInsumo;
		protected int _IdConcepto;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected double _CostoInsumo;

		public virtual int IdConceptoInsumo
		{
			get
			{
				return _IdConceptoInsumo;
			}
			set
			{
				_IdConceptoInsumo = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual string Codigo
		{
			get;
			set;
		}

		public virtual string Descripcion
		{
			set;
			get;
		}
		public virtual double CostoInsumo
		{
			get
			{
				return _CostoInsumo;
			}
			set
			{
				_CostoInsumo = value;
			}
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual int IdCapitulo
		{
			set;
			get;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}
		public virtual string Unidad
		{
			set;
			get;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual string UnidadMedidaDescripcion
		{
			set;
			get;
		}
		public virtual string ConceptoPadre
		{
			set;
			get;
		}
		public CConceptosInsumos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Descripcion", "Moneda", "IdCapitulo", "Unidad", "CodigoMoneda", "UnidadMedidaDescripcion", "ConceptoPadre" };
		}
		public virtual string ToXml()
		{
			return new StringBuilder(String.Format("<Insumo><IdInsumo>{0}</IdInsumo><IdMoneda>{1}</IdMoneda><Cantidad>{2}</Cantidad><Costo>{3}</Costo></Insumo>",
																this.IdInsumo,
																this.IdMoneda,
																this.Cantidad,
																this.CostoInsumo)).ToString();	
		}
	}// CConceptosInsumos ends.
}// Entidades ends.
