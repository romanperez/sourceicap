using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{ 
    
    public partial class COTPersonalCliente : CBase,IOTPersonalCliente
    {
        /// <summary>
        /// Lista de propiedades clase [ COTPersonalCliente].
        /// </summary>		
		protected int _IdOTPersonalCliente;
		protected int _IdOrdenTrabajo;
		protected int _IdEstatus;
		protected int _IdPersonalCliente;
		protected bool _Estatus;
        
			public virtual  int IdOTPersonalCliente
		{
		 get{return _IdOTPersonalCliente;}
		 set{_IdOTPersonalCliente = value;}
		}
		public virtual  int IdOrdenTrabajo
		{
		 get{return _IdOrdenTrabajo;}
		 set{_IdOrdenTrabajo = value;}
		}
		public virtual  int IdEstatus
		{
		 get{return _IdEstatus;}
		 set{_IdEstatus = value;}
		}
		public virtual  int IdPersonalCliente
		{
		 get{return _IdPersonalCliente;}
		 set{_IdPersonalCliente = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
		public virtual string Nombre
		{
			get;
			set;
		}
		public virtual string ApellidoPaterno
		{
			get;
			set;
		}
		public virtual string ApellidoMaterno
		{
			get;
			set;
		}
		public virtual string Puesto
		{
			get;
			set;
		}
		public virtual string NombreEstatus
		{
			get;
			set;
		}
		public virtual string NombreCompleto
		{
			get
			{
				return String.Format("{0} {1} {2}", this.Nombre, this.ApellidoPaterno, this.ApellidoMaterno);
			}
		}
	public COTPersonalCliente()
	{	  
	}
	public override string[] GetOmitirCamposSelect()
	{
		return new string[] { "Nombre", "ApellidoPaterno", "ApellidoMaterno", "Puesto", "NombreEstatus", "NombreCompleto" };
	}
    }// COTPersonalCliente ends.
}// Entidades ends.
