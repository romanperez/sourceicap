using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 08/12/2016 05:26:38 p. m.*/
namespace Entidades
{

	public partial class CPerfiles : CBase, IPerfiles
	{
		/// <summary>
		/// Lista de propiedades clase [ CPerfiles].
		/// </summary>		
		protected int _IdPerfil;
		protected string _Nombre;
		protected string _Descripcion;
		protected bool _Estatus;

		public virtual int IdPerfil
		{
			get
			{
				return _IdPerfil;
			}
			set
			{
				_IdPerfil = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}

		public CPerfiles()
		{
		}
	}// CPerfiles ends.
}// Entidades ends.
