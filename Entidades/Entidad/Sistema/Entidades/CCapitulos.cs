using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/01/2017 02:07:42 p. m.*/
namespace Entidades
{

	public partial class CCapitulos : CBase, ICapitulos
	{
		/// <summary>
		/// Lista de propiedades clase [ CCapitulos].
		/// </summary>		
		protected int _IdCapitulo;
		protected int _IdEmpresa;
		protected string _Capitulo;
		protected bool _Estatus;

		public virtual int IdCapitulo
		{
			get
			{
				return _IdCapitulo;
			}
			set
			{
				_IdCapitulo = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Capitulo
		{
			get
			{
				return _Capitulo;
			}
			set
			{
				_Capitulo = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Empresa
		{
			set;
			get;
		}

		public CCapitulos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Empresa" };
		}
	}// CCapitulos ends.
}// Entidades ends.
