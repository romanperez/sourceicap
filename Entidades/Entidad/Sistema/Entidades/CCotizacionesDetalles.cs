using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:40 p. m.*/
namespace Entidades
{

	public partial class CCotizacionesDetalles : CBase, ICotizacionesDetalles
	{
		/// <summary>
		/// Lista de propiedades clase [ CCotizacionesDetalles].
		/// </summary>		
		protected int _IdCotizacionDetalle;
		protected int _IdCotizacion;
		protected int _IdConcepto;
		protected int _Partida;
		protected double _Cantidad;
		protected double _Precio;
		protected string _Ubicacion;
		/*protected double _PorcentajeDescuento;
		protected double _DescuentoMonto;*/

		public virtual int IdCotizacionDetalle
		{
			get
			{
				return _IdCotizacionDetalle;
			}
			set
			{
				_IdCotizacionDetalle = value;
			}
		}
		public virtual int IdCotizacion
		{
			get
			{
				return _IdCotizacion;
			}
			set
			{
				_IdCotizacion = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int Partida
		{
			get
			{
				return _Partida;
			}
			set
			{
				_Partida = value;
			}
		}
		public virtual double Precio
		{
			get
			{
				return _Precio;
			}
			set
			{
				_Precio = value;
			}
		}
		public virtual string Ubicacion
		{
			get
			{
				return _Ubicacion;
			}
			set
			{
				_Ubicacion = value;
			}
		}
		/*	public virtual  double PorcentajeDescuento
			{
			 get{return _PorcentajeDescuento;}
			 set{_PorcentajeDescuento = value;}
			}
			public virtual  double DescuentoMonto
			{
			 get{return _DescuentoMonto;}
			 set{_DescuentoMonto = value;}
			}*/
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}

		public virtual string Concepto
		{
			get;
			set;
		}

		public virtual string Descripcion
		{
			get;
			set;
		}

		public virtual string Capitulo
		{
			get;
			set;
		}
		public virtual int IdCapitulo
		{
			get;
			set;
		}
		public virtual int IdMoneda
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual int IdUnidadMedida
		{
			get;
			set;
		}
		public virtual string UnidadMedida
		{
			get;
			set;
		}
		public virtual double PrecioOriginal
		{
			set;
			get;
		}
		public virtual double ValorTipoCambio
		{
			set;
			get;
		}
		public virtual string Monedas
		{
			set;
			get;
		}
		public virtual DateTime Fecha
		{
			set;
			get;
		}
		public virtual List<CCotizacionesDetallesInsumos> Insumos
		{
			set;
			get;
		}
		public override string[] GetOmitirCamposSelect()
		{			
			return new string[] { "Capitulo", "IdCapitulo", 
				                   "Moneda", "CodigoMoneda", "IdMoneda", "Insumos", 
										 "IdUnidadMedida", "UnidadMedida",
										 "PrecioOriginal","ValorTipoCambio","Monedas","Fecha"
			                     };
		}
		public CCotizacionesDetalles()
		{

		}
	}// CCotizacionesDetalles ends.
}// Entidades ends.
