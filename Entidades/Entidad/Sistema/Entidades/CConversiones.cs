using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 06/04/2017 03:52:26 p. m.*/
namespace Entidades
{

	public partial class CConversiones : CBase, IConversiones
	{
		/// <summary>
		/// Lista de propiedades clase [ CConversiones].
		/// </summary>		
		protected int _IdConversion;
		protected int _IdMonedaOrigen;
		protected int _IdMonedaDestino;
		protected string _Operador;

		public virtual int IdConversion
		{
			get
			{
				return _IdConversion;
			}
			set
			{
				_IdConversion = value;
			}
		}
		public virtual int IdMonedaOrigen
		{
			get
			{
				return _IdMonedaOrigen;
			}
			set
			{
				_IdMonedaOrigen = value;
			}
		}
		public virtual int IdMonedaDestino
		{
			get
			{
				return _IdMonedaDestino;
			}
			set
			{
				_IdMonedaDestino = value;
			}
		}
		public virtual string Operador
		{
			get
			{
				return _Operador;
			}
			set
			{
				_Operador = value;
			}
		}

		public virtual string MonedaOrigen
		{
			set;
			get;
		}
		public virtual string MonedaDestino
		{
			set;
			get;
		}
		public virtual string CodigoMonedaOrigen
		{
			get;
			set;
		}
		public virtual string CodigoMonedaDestino
		{
			set;
			get;
		}
		public CConversiones()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "MonedaOrigen", "MonedaDestino", "CodigoMonedaOrigen", "CodigoMonedaDestino" };
		}
	}// CConversiones ends.
}// Entidades ends.
