using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{

	public partial class CProyectos : CBase, IProyectos
	{
		public const string PROYECTO_RSMN_COSTOS = "PROYECTO_RSMN_COSTOS";
		public const string PROYECTO_RSMN_CONVERSION = "PROYECTO_RSMN_CONVERSION";
		/// <summary>
		/// Lista de propiedades clase [ CProyectos].
		/// </summary>		
		protected int _IdProyecto;
		protected int _IdCliente;
		protected int _IdEmpresa;
		protected int _IdUnidad;
		protected int? _IdCotizacion;
		protected string _Proyecto;		
		protected string _Descripcion;
		protected string _Ubicacion;
		protected DateTime _FechaInicial;
		protected DateTime _FechaFinal;		

		public virtual int IdProyecto
		{
			get
			{
				return _IdProyecto;
			}
			set
			{
				_IdProyecto = value;
			}
		}
		public virtual int IdCliente
		{
			get
			{
				return _IdCliente;
			}
			set
			{
				_IdCliente = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual int? IdCotizacion
		{
			get
			{
				return _IdCotizacion;
			}
			set
			{
				_IdCotizacion = value;
			}
		}

		public virtual string Proyecto
		{
			get
			{
				return _Proyecto;
			}
			set
			{
				_Proyecto = value;
			}
		}		
		public virtual string Descripcion
		{
			get
			{
				return _Descripcion;
			}
			set
			{
				_Descripcion = value;
			}
		}
		public virtual string Ubicacion
		{
			get
			{
				return _Ubicacion;
			}
			set
			{
				_Ubicacion = value;
			}
		}
		public virtual DateTime FechaInicial
		{
			get
			{
				return _FechaInicial;
			}
			set
			{
				_FechaInicial = value;
			}
		}
		public virtual DateTime FechaFinal
		{
			get
			{
				return _FechaFinal;
			}
			set
			{
				_FechaFinal = value;
			}
		}
		
		public virtual string RazonSocial
		{
			get;
			set;
		}

		public virtual double Total
		{
			get;
			set;
		}
		
		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual  List<CProyectoContratos> Contratos
		{
			get;
			set;
		}
		public virtual string Cotizacion
		{
			set;
			get;
		}
		public virtual string AllContratos
		{
			set;
			get;
		}
		public virtual int IdEstatus
		{
			get;
			set;
		}		
		public virtual string EstatusProyecto
		{
			get;
			set;
		}
		public virtual List<CvwContratos2> vwContratos
		{
			set;
			get;
		}
		public virtual List<CProyectosCostos> CostosGlobales
		{
			set;
			get;
		}
		public virtual string MonedaCosto{
			set;
			get;
		}
		public virtual double CostoProyecto
		{
			set;
			get;
		}
		public virtual string MonedaConversion
		{
			set;
			get;
		}
		public virtual double MonedaConversionCosto
		{
			set;
			get;
		}
		public CProyectos()
		{
			Contratos = new List<CProyectoContratos>();
			CostosGlobales = new List<CProyectosCostos>();
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "RazonSocial", "Total", "Empresa", 
				                   "Unidad", "Contratos", "Cotizacion", 
										 "AllContratos", "EstatusProyecto", "vwContratos" ,
										 "CostosGlobales","MonedaCosto","CostoProyecto","MonedaConversion","MonedaConversionCosto"};
		}
	}// CProyectos ends.
}// Entidades ends.
