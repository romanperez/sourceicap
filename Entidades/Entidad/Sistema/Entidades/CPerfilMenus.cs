using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 04/01/2017 10:37:31 a. m.*/
namespace Entidades
{ 
    
    public partial class CPerfilMenus : CBase,IPerfilMenus
    {
        /// <summary>
        /// Lista de propiedades clase [ CPerfilMenus].
        /// </summary>		
	protected int _IdPerfilModulo;
		protected int _IdPerfil;
		protected int _IdMenu;
		protected bool _Estatus;
        
			public virtual  int IdPerfilModulo
		{
		 get{return _IdPerfilModulo;}
		 set{_IdPerfilModulo = value;}
		}
		public virtual  int IdPerfil
		{
		 get{return _IdPerfil;}
		 set{_IdPerfil = value;}
		}
		public virtual  int IdMenu
		{
		 get{return _IdMenu;}
		 set{_IdMenu = value;}
		}
		public virtual  bool Estatus
		{
		 get{return _Estatus;}
		 set{_Estatus = value;}
		}
			
	public CPerfilMenus()
	{	  
	}
    }// CPerfilMenus ends.
}// Entidades ends.
