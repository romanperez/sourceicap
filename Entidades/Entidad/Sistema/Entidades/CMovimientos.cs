using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:42 p. m.*/
namespace Entidades
{

	public partial class CMovimientos : CBase, IMovimientos
	{
		/// <summary>
		/// Lista de propiedades clase [ CMovimientos].
		/// </summary>		
		protected int _IdMovimiento;
		protected int _Tipo;
		protected DateTime _Fecha;
		protected int _IdAlmacen;
		protected int? _IdAlmacenDestino;
		protected int _IdUsuario;
		protected bool _Estatus;
		
		public virtual int IdMovimiento
		{
			get
			{
				return _IdMovimiento;
			}
			set
			{
				_IdMovimiento = value;
			}
		}
		public virtual int Tipo
		{
			get
			{
				return _Tipo;
			}
			set
			{
				_Tipo = value;
			}
		}
		public virtual DateTime Fecha
		{
			get
			{
				return _Fecha;
			}
			set
			{
				_Fecha = value;
			}
		}
		public virtual int IdAlmacen
		{
			get
			{
				return _IdAlmacen;
			}
			set
			{
				_IdAlmacen = value;
			}
		}
		public virtual int? IdAlmacenDestino
		{
			get
			{
				return _IdAlmacenDestino;
			}
			set
			{
				_IdAlmacenDestino = value;
			}
		}
		public virtual int IdUsuario
		{
			get
			{
				return _IdUsuario;
			}
			set
			{
				_IdUsuario = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual int IdUnidad
		{
			get;
			set;
		}
		public virtual List<CMovimientosDetalles> DetalleMovimientos
		{
			set;
			get;
		}
		public virtual string Unidad
		{
			set;
			get;
		}	
		public virtual string Usuario
		{
			set;
			get;
		}
		public virtual string Almacen
		{
			get;
			set;
		}
		public virtual string AlmacenDestino
		{
			get;
			set;
		}		
		public virtual int IdUnidadDestino
		{
			get;
			set;
		}

		public virtual string UnidadDestino
		{
			set;
			get;
		}


		public virtual int IdProyecto
		{
			set;
			get;
		}
		public virtual string Proyecto
		{
			set;
			get;
		}


		public virtual int IdContrato
		{
			set;
			get;
		}		
		public virtual string Contrato
		{
			get;
			set;
		}
		public virtual bool TieneSolicituInsumoAsociada
		{
			set;
			get;
		}
		public CMovimientos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "DetalleMovimientos", "IdUnidad", "Unidad", "Usuario", "Almacen", 
				"AlmacenDestino", "IdUnidadDestino","UnidadDestino","IdContrato","Contrato",
			"IdProyecto","Proyecto","TieneSolicituInsumoAsociada"};
		}
		/*public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Tipo", "IdAlmacen", "IdUsuario", "Estatus", "IdUnidad", "Unidad", "Usuario", "Almacen", "AlmacenDestino" , "IdUnidadDestino","UnidadDestino"};
		}*/
	}// CMovimientos ends.
}// Entidades ends.
