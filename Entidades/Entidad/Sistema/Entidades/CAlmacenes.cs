using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Entidades
{
	///<summary>
	///Catalogo almacenes
	///</summary>
	public partial class CAlmacenes : CBase, IAlmacenes
	{
		/// <summary>
		/// Lista de propiedades clase [ CAlmacenes].
		/// </summary>		
		protected int _IdAlmacen;
		protected int _IdUnidad;
		protected string _Codigo;
		protected string _Almacen;
		protected bool _Estatus;
		protected int _IdEmpresa;
		protected string _Empresa;
		protected string _Unidad;

		public virtual int IdAlmacen
		{
			get
			{
				return _IdAlmacen;
			}
			set
			{
				_IdAlmacen = value;
			}
		}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual string Codigo
		{
			get
			{
				return _Codigo;
			}
			set
			{
				_Codigo = value;
			}
		}
		public virtual string Almacen
		{
			get
			{
				return _Almacen;
			}
			set
			{
				_Almacen = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}
		public virtual string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}

		public CAlmacenes()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "IdEmpresa", "Empresa", "Unidad" };
		}
	}// CAlmacenes ends.
}// Entidades ends.
