using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{

	public partial class CContratosDetalles2 : CBase, IContratosDetalles2
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratosDetalles2].
		/// </summary>		
		protected int _IdContratosDetalles;
		protected int _IdContrato;
		protected int _IdConcepto;
		protected int _Partida;
		protected double _Precio;
		/*protected double _PorcentajeDescuento;
		protected double _DescuentoMonto;*/
		protected double _Cantidad;
		protected string _Ubicacion;
		protected bool _EsAdicional;
		public virtual int IdContratosDetalles
		{
			get
			{
				return _IdContratosDetalles;
			}
			set
			{
				_IdContratosDetalles = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int Partida
		{
			get
			{
				return _Partida;
			}
			set
			{
				_Partida = value;
			}
		}
		public virtual double Precio
		{
			get
			{
				return _Precio;
			}
			set
			{
				_Precio = value;
			}
		}
		/*public virtual double PorcentajeDescuento
		{
			get
			{
				return _PorcentajeDescuento;
			}
			set
			{
				_PorcentajeDescuento = value;
			}
		}
		public virtual double DescuentoMonto
		{
			get
			{
				return _DescuentoMonto;
			}
			set
			{
				_DescuentoMonto = value;
			}
		}*/
		public virtual string Ubicacion
		{
			get
			{
				return _Ubicacion;
			}
			set
			{
				_Ubicacion = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual double CantidadProgramada
		{
			get;
			set;
		}
		public virtual double CantidadEjecutada
		{
			get;
			set;
		}
		public virtual int Ots
		{
			set;
			get;
		}
		public virtual string Concepto
		{
			get;
			set;
		}
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Capitulo
		{
			get;
			set;
		}
		public virtual List<CContratosDetallesInsumos2> Insumos
		{
			set;
			get;
		}
		public virtual int IdMoneda
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual int IdUnidadMedida
		{
			get;
			set;
		}
		public virtual string UnidadMedida
		{
			get;
			set;
		}
		public virtual string Contrato
		{
			get;
			set;
		}
		public virtual bool EsAdicional
		{
			get
			{
				return _EsAdicional;
			}
			set
			{
				_EsAdicional = value;
			}
		}
		public virtual CConceptos InfoConcepto
		{
			set;
			get;
		}
		public virtual double PrecioOriginal
		{
			set;
			get;
		}
		public virtual double ValorTipoCambio
		{
			set;
			get;
		}
		public virtual string Monedas
		{
			set;
			get;
		}
		public virtual DateTime Fecha
		{
			set;
			get;
		}
		public virtual double BaseInstalado
		{
			get;
			set;
		}
		public virtual double AdicionalInstalado
		{
			get;
			set;
		}
		public virtual DateTime FechaFinal
		{
			set;
			get;
		}
		public virtual List<CEstimaciones> Estimaciones
		{
			set;
			get;
		}
		public virtual double CantidadPorEstimar
		{
			set;
			get;
		}
		public virtual double ImportePorEstimar  
		{
			set;
			get;
		}
		public CContratosDetalles2()
		{
			Insumos = new List<CContratosDetallesInsumos2>();
			InfoConcepto = new CConceptos();
			Estimaciones = new List<CEstimaciones>();
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] {  "Capitulo", 
				                    "Insumos", "IdMoneda", "Moneda", 
										  "CodigoMoneda", "IdUnidadMedida", "UnidadMedida",
										  "Contrato","InfoConcepto",
										  "PrecioOriginal","ValorTipoCambio","Monedas","Fecha",
									     "CantidadProgramada","CantidadEjecutada","Ots", "AdicionalInstalado", "BaseInstalado",
                                "FechaFinal","Estimaciones","CantidadPorEstimar","ImportePorEstimar"
                                     };
		}

		public virtual string ToXml()
		{
			StringBuilder sb = new StringBuilder("");
			sb.Append(String.Format("<Concepto><IdConcepto>{0}</IdConcepto><Precio>{1}</Precio><EsAdicional>{2}</EsAdicional>",
											this.IdConcepto,
											this.Precio,
											(this.EsAdicional) ? "1" : "0"));
			if(this.Insumos != null && this.Insumos.Count() > 0)
			{
				sb.Append("<Insumos>");
				this.Insumos.ForEach(i => sb.Append(i.ToXml()));
				sb.Append("</Insumos>");
			}
			sb.Append("</Concepto>");
			return sb.ToString();
		}
	}// CContratosDetalles2 ends.
}// Entidades ends.
