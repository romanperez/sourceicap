using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{

	public partial class CvwContratos2 : CContratos2, IvwContratos2
	{		
		public virtual double SubTotalOriginal
		{
			get;
			set;
		}
		public virtual double TotalOriginal
		{
			get;
			set;
		}

		public virtual double ValorTipoCambio
		{
			get;
			set;
		}

		public virtual string Monedas
		{
			get;
			set;
		}		
		public CvwContratos2()
		{
			Conceptos = new List<CContratosDetalles2>();
			Adicionales = new List<CContratosDetalles2>();
			CostosManuales = new List<CContratosDetalles2>();			
		}		
	}// CContratos2 ends.
}// Entidades ends.
