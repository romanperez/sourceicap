using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{

	public partial class CContratosDetallesInsumos2 : CBase, IContratosDetallesInsumos2
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratosDetallesInsumos2].
		/// </summary>		
		protected int _IdInsumoContrato;
		protected int _IdContrato;
		protected int _IdConcepto;
		protected int _IdContratosDetalles;		
		protected int _IdInsumo;
		protected double _Cantidad;
		protected double _Costo;

		public virtual int IdInsumoContrato
		{
			get
			{
				return _IdInsumoContrato;
			}
			set
			{
				_IdInsumoContrato = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual int IdContratosDetalles
		{
			get
			{
				return _IdContratosDetalles;
			}
			set
			{
				_IdContratosDetalles = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual double CantidadProgramada
		{
			set;
			get;
		}		
		public virtual double CantidadPorConcepto
		{
			get;
			set;
		}
		
		public virtual double CantidadEjecutada
		{
			get;
			set;
		}
		public virtual double Costo
		{
			get
			{
				return _Costo;
			}
			set
			{
				_Costo = value;
			}
		}
		public virtual string Codigo
		{
			get;
			set;
		}		
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}
		public virtual bool EsAdicional
		{
			set;
			get;
		}
		public virtual string MotivoModificacion
		{
			set;
			get;
		}
		public virtual double CostoOriginal
		{
			set;
			get;
		}
		public virtual double ValorTipoCambio
		{
			set;
			get;
		}
		public virtual string Monedas
		{
			set;
			get;
		}
		public virtual DateTime Fecha
		{
			set;
			get;
		}
		public virtual int? IdPedido
		{
			set;
			get;
		}
		public virtual  bool? Movimiento
		{
			set;
			get;
		}
		public virtual bool? Pedido
		{
			set;
			get;
		}
		public virtual double CantidadSolicitada
		{
			set;
			get;
		}
		public virtual double CantidadEnviada
		{
			set;
			get;
		}
		public virtual double CantidadRecibida
		{
			set;
			get;
		}
		public virtual double SalidaAlmacen
		{
			set;
			get;
		}
		public CContratosDetallesInsumos2()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Descripcion", "Unidad", "Moneda", "CodigoMoneda", 
				                    "MotivoModificacion", "EsAdicional" ,"CostoOriginal","ValorTipoCambio","Monedas","Fecha",
										 "CantidadProgramada","CantidadEjecutada",
										 "CantidadSolicitada","CantidadEnviada","CantidadRecibida","SalidaAlmacen"};
		}		
		public string ToXml()
		{
			return new StringBuilder(String.Format("<Insumo><IdInsumo>{0}</IdInsumo><IdMoneda>{1}</IdMoneda><Cantidad>{2}</Cantidad><Costo>{3}</Costo></Insumo>",
				                                    this.IdInsumo,
																this.IdMoneda,
																this.Cantidad,
																this.Costo)).ToString();	
		}
	}// CContratosDetallesInsumos2 ends.
}// Entidades ends.
