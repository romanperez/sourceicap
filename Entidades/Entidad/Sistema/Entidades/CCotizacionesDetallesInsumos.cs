using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 21/02/2017 09:48:11 a. m.*/
namespace Entidades
{

	public partial class CCotizacionesDetallesInsumos : CBase, ICotizacionesDetallesInsumos
	{
		/// <summary>
		/// Lista de propiedades clase [ CCotizacionesDetallesInsumos].
		/// </summary>		
		protected int _IdInsumoCotizacion;
		protected int _IdCotizacion;
		protected int _IdConcepto;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected double _Costo;

		public virtual int IdInsumoCotizacion
		{
			get
			{
				return _IdInsumoCotizacion;
			}
			set
			{
				_IdInsumoCotizacion = value;
			}
		}
		public virtual int IdCotizacion
		{
			get
			{
				return _IdCotizacion;
			}
			set
			{
				_IdCotizacion = value;
			}
		}
		public virtual int IdConcepto
		{
			get
			{
				return _IdConcepto;
			}
			set
			{
				_IdConcepto = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual double Costo
		{
			get
			{
				return _Costo;
			}
			set
			{
				_Costo = value;
			}
		}

		public virtual string Codigo
		{
			get;
			set;
		}
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual int IdCapitulo
		{
			set;
			get;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}
		public virtual string Unidad
		{
			set;
			get;
		}
		public virtual double CostoOriginal
		{
			set;
			get;
		}
		public virtual double ValorTipoCambio
		{
			set;
			get;
		}
		public virtual string Monedas
		{
			set;
			get;
		}
		public virtual DateTime Fecha
		{
			set;
			get;
		}
		public CCotizacionesDetallesInsumos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Descripcion", "Moneda", "IdCapitulo", "IdMoneda","Unidad"
			,"CostoOriginal","ValorTipoCambio","Monedas","Fecha"};
		}
	}// CCotizacionesDetallesInsumos ends.
}// Entidades ends.
