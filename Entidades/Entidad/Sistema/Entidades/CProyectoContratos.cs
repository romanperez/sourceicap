using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:54 p. m.*/
namespace Entidades
{
	public partial class CProyectoContratos : CBase, IProyectoContratos
	{
		/// <summary>
		/// Lista de propiedades clase [ CProyectoContratos].
		/// </summary>		
		protected int _IdProyectoContrato;
		protected int _IdProyecto;		
		protected int _IdContrato;
		protected bool _Estatus;

		public virtual int IdProyectoContrato
		{
			get
			{
				return _IdProyectoContrato;
			}
			set
			{
				_IdProyectoContrato = value;
			}
		}
		public virtual int IdProyecto
		{
			get
			{
				return _IdProyecto;
			}
			set
			{
				_IdProyecto = value;
			}
		}
		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string RazonSocial
		{
			set;
			get;
		}
		public virtual string Empresa
		{
			set;
			get;
		}
		public virtual string Unidad
		{
			set;
			get;
		}
		public virtual string Contrato
		{
			set;
			get;
		}
		public virtual int IdCliente
		{
			set;
			get;
		}
		public CProyectoContratos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Contrato", "RazonSocial", "Empresa", "Unidad", "IdCliente" };
		}
	}// CProyectoContratos ends.
}// Entidades ends.
