using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{

	public partial class CContratos2 : CBase, IContratos2
	{
		/// <summary>
		/// Lista de propiedades clase [ CContratos2].
		/// </summary>		
		protected int _IdContrato;
		protected double _SubTotal;
		protected double _Total;
		protected double _PorcentajeDescuento;
		protected double _DescuentoMonto;
		protected int _IdEmpresa;
		protected int _IdCliente;
		protected int _IdUnidad;
		protected int _IdIva;
		protected int? _IdTipoCambio;
		protected int _IdMoneda;
		//protected int? _IdPedido;
		//protected bool _Aprobado;
		protected string _Contrato;
		protected DateTime _Fecha;
		//protected bool? _Movimiento;
		protected double _Utilidad;

		public virtual int IdContrato
		{
			get
			{
				return _IdContrato;
			}
			set
			{
				_IdContrato = value;
			}
		}
		public virtual double SubTotal
		{
			get
			{
				return _SubTotal;
			}
			set
			{
				_SubTotal = value;
			}
		}
		public virtual double Total
		{
			get
			{
				return _Total;
			}
			set
			{
				_Total = value;
			}
		}
		public virtual double PorcentajeDescuento
		{
			get
			{
				return _PorcentajeDescuento;
			}
			set
			{
				_PorcentajeDescuento = value;
			}
		}
		public virtual double DescuentoMonto
		{
			get
			{
				return _DescuentoMonto;
			}
			set
			{
				_DescuentoMonto = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual int IdCliente
		{
			get
			{
				return _IdCliente;
			}
			set
			{
				_IdCliente = value;
			}
		}
		//public virtual bool? Movimiento
		//{
		//	get
		//	{
		//		return _Movimiento;
		//	}
		//	set
		//	{
		//		_Movimiento = value;
		//	}
		//}
		//public virtual int? IdPedido
		//{
		//	get
		//	{
		//		return _IdPedido;
		//	}
		//	set
		//	{
		//		_IdPedido = value;
		//	}
		//} 
		//public virtual bool Aprobado
		//{
		//	get
		//	{
		//		return _Aprobado;
		//	}
		//	set
		//	{
		//		_Aprobado = value;
		//	}
		//}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual int IdIva
		{
			get
			{
				return _IdIva;
			}
			set
			{
				_IdIva = value;
			}
		}
		public virtual int? IdTipoCambio
		{
			get
			{
				return _IdTipoCambio;
			}
			set
			{
				_IdTipoCambio = value;
			}
		}
		public virtual int IdMoneda
		{
			get
			{
				return _IdMoneda;
			}
			set
			{
				_IdMoneda = value;
			}
		}
		public virtual string Contrato
		{
			get
			{
				return _Contrato;
			}
			set
			{
				_Contrato = value;
			}
		}

		public virtual DateTime Fecha
		{
			get
			{
				return _Fecha;
			}
			set
			{
				_Fecha = value;
			}
		}
		public virtual double Utilidad
		{
			get
			{
				return _Utilidad;
			}
			set
			{
				_Utilidad = value;
			}
		}
		/// <summary>
		/// Especifica quien realiza la salida y solicitud de materiales.
		/// </summary>
		public virtual int? IdUsuario
		{
			set;
			get;
		}
		public virtual string RazonSocial
		{
			set;
			get;
		}
		public virtual string Moneda
		{
			set;
			get;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public virtual double Iva
		{
			set;
			get;
		}
		public virtual double Valor
		{
			set;
			get;
		}
		public virtual string Empresa
		{
			get;
			set;
		}				
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual List<CContratosDetalles2> Conceptos
		{
			set;
			get;
		}
		public virtual List<CContratosDetalles2> Adicionales
		{
			set;
			get;
		}
		public virtual List<CContratosDetalles2> CostosManuales
		{
			set;
			get;
		}
		public virtual CProyectos Proyecto
		{
			set;
			get;
		}
		public virtual CUsuarios Usuario
		{
			set;
			get;
		}
		public virtual CUsuarios UsuarioCostos
		{
			set;
			get;
		}
		public virtual string Cotizacion
		{
			set;
			get;
		}
		public virtual int? IdUsuarioCostos
		{
			set;
			get;
		}		
		public virtual string MotivoModificacion
		{
			set;
			get;
		}
        public virtual double? Anticipo
        {
            get;
            set;
        }
        public virtual int? IdPersonalCliente
        {
            get;
            set;
        }        
        public virtual string ContactoCliente
        {
            get;
            set;
        }
		public CContratos2()
		{
			Conceptos = new List<CContratosDetalles2>();
			Adicionales = new List<CContratosDetalles2>();
			CostosManuales = new List<CContratosDetalles2>();
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "RazonSocial", "Moneda", "Iva", "Valor", "Empresa", "Unidad", 
				                   "Conceptos", "Proyecto", "CodigoMoneda", "Usuario", "Cotizacion","Adicionales","CostosManuales","UsuarioCostos" };
		}
	}// CContratos2 ends.
}// Entidades ends.
