using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:41 p. m.*/
namespace Entidades
{

	public partial class CInsumosExistencias : CBase, IInsumosExistencias
	{
		/// <summary>
		/// Lista de propiedades clase [ CInsumosExistencias].
		/// </summary>		
		protected int _IdExistencia;
		protected int _IdAlmacen;
		protected int _IdInsumo;
		protected double _Cantidad;
		protected bool _Estatus;
		protected double _Costo;
		protected DateTime _UltimoMovimiento;
		
		public virtual int IdExistencia
		{
			get
			{
				return _IdExistencia;
			}
			set
			{
				_IdExistencia = value;
			}
		}
		public virtual int IdAlmacen
		{
			get
			{
				return _IdAlmacen;
			}
			set
			{
				_IdAlmacen = value;
			}
		}
		public virtual int IdInsumo
		{
			get
			{
				return _IdInsumo;
			}
			set
			{
				_IdInsumo = value;
			}
		}
		public virtual double Cantidad
		{
			get
			{
				return _Cantidad;
			}
			set
			{
				_Cantidad = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		public virtual string Codigo
		{
			get;
			set;
		}

		public virtual string Serie
		{
			get;
			set;
		}
		public virtual string Descripcion
		{
			get;
			set;
		}
		public virtual string Almacen
		{
			get;
			set;
		}
		public virtual  double Costo
		{
			get
			{
				return _Costo;
			}
			set
			{
				_Costo = value;
			}
		}
		public virtual DateTime UltimoMovimiento
		{
			get
			{
				return _UltimoMovimiento;
			}
			set
			{
				_UltimoMovimiento = value;
			}
		}
		public virtual string Empresa
		{
			get;
			set;
		}
		public virtual string Unidad
		{
			get;
			set;
		}
		public virtual string Moneda
		{
			get;
			set;
		}
		public virtual string CodigoMoneda
		{
			get;
			set;
		}
		public CInsumosExistencias()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Serie", "Descripcion", "Almacen", "Empresa", "Unidad", "Moneda", "CodigoMoneda" };
		}
	}// CInsumosExistencias ends.
}// Entidades ends.
