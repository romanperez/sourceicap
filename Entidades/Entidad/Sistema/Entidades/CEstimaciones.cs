using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 03/10/2017 11:24:11 a. m.*/
namespace Entidades
{	
	public partial class CEstimaciones : CBase, IEstimaciones
	{
		/// <summary>
		/// Lista de propiedades clase [ CEstimaciones].
		/// </summary>		
		protected int _IdEstimacion;
		protected int _IdContratosDetalles;
        protected int _NumeroEstimacion;
        protected DateTime _FechaInicio;
        protected DateTime _FechaFin;
		protected double _CantidadEstimada;
		protected double _ImporteEstimado;

		public virtual int IdEstimacion
		{
			get
			{
				return _IdEstimacion;
			}
			set
			{
				_IdEstimacion = value;
			}
		}
		public virtual int IdContratosDetalles
		{
			get
			{
				return _IdContratosDetalles;
			}
			set
			{
				_IdContratosDetalles = value;
			}
		}
        public virtual int NumeroEstimacion
        {
            get { return _NumeroEstimacion; }
            set {_NumeroEstimacion=value; }
        }
		public virtual DateTime FechaInicio
		{
			get
			{
                return _FechaInicio;
			}
			set
			{
                _FechaInicio = value;
			}
		}
        public virtual DateTime FechaFin
        {
            get
            {
                return _FechaFin;
            }
            set
            {
                _FechaFin = value;
            }
        }
		public virtual double CantidadEstimada
		{
			get
			{
				return _CantidadEstimada;
			}
			set
			{
				_CantidadEstimada = value;
			}
		}
		public virtual double ImporteEstimado
		{
			get
			{
				return _ImporteEstimado;
			}
			set
			{
				_ImporteEstimado = value;
			}
		}
        public virtual double? ImporteDeductivo
        { set; get; }
        public virtual double CantidadAcumuladaAnterior { set; get; }
        public virtual double CantidadEjecutadaOT { set; get; }
        public virtual double CantidadAcumuladaActual { set; get; }
        public virtual double ImporteAcumuladoActual { set; get; }
        public virtual double CantidadPorEstimar { set; get; }
        public virtual double ImportePorEstimar { set; get; }
        public virtual int IdContrato
        {
            set;
            get;
        }
         public virtual string Concepto
        {
            get;
            set;
        }               
         public virtual string Descripcion
        {
            get;
            set;
        }        
         public virtual string UnidadMedida
         {
             get;
             set;
         }
         public virtual double CantidadContratada { set; get; }
         public virtual double PrecioUnitario { set; get; }
         public virtual double ImporteContratado { set; get; }
         public virtual double? PorcentajeAmortizacionAnticipo { set; get; }
		public CEstimaciones()
		{
		}
        public override string[] GetOmitirCamposSelect()
        {
            return new string[]{"IdContrato","Concepto","Descripcion","UnidadMedida","CantidadAcumuladaAnterior","CantidadEjecutadaOT","CantidadAcumuladaActual","ImporteAcumuladoActual",
                                "CantidadPorEstimar","ImportePorEstimar","CantidadContratada","PrecioUnitario","ImporteContratado"};
        }
	}// CEstimaciones ends.
}// Entidades ends.
