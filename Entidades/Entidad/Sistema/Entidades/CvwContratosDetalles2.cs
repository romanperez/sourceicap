using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{

	public partial class CvwContratosDetalles2 : CContratosDetalles2, IvwContratosDetalles2
	{
		public CvwContratosDetalles2()
		{
			base.Insumos = new List<CContratosDetallesInsumos2>();
			base.InfoConcepto = new CConceptos();
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Concepto", "Descripcion", "Capitulo", 
				                    "Insumos", "IdMoneda", "Moneda", 
										  "CodigoMoneda", "IdUnidadMedida", "UnidadMedida",
										  "Contrato","InfoConcepto"};
		}
	}// CContratosDetalles2 ends.
}// Entidades ends.
