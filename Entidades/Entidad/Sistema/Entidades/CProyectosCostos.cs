using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 15/11/2016 04:27:43 p. m.*/
namespace Entidades
{
	public partial class CProyectosCostos : CBase, IProyectosCostos
	{
		public virtual int IdProyecto
		{
			set;
			get;
		}
		public virtual int IdMoneda
		{
			set;
			get;
		}
		public virtual string Moneda
		{
			set;
			get;
		}
		public virtual string Codigo
		{
			set;
			get;
		}
		public virtual double Total
		{
			set;
			get;
		}
		public virtual string Totals
		{
			set;
			get;
		}
		public CProyectosCostos()
		{
			
		}
	}// CProyectosCostos ends.
}// Entidades ends.
