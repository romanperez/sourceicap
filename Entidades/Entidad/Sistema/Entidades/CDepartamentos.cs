using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 21/12/2016 12:08:27 p. m.*/
namespace Entidades
{

	public partial class CDepartamentos : CBase, IDepartamentos
	{
		/// <summary>
		/// Lista de propiedades clase [ CDepartamentos].
		/// </summary>		
		protected int _IdDepartamento;
		protected int _IdEmpresa;
		protected string _Empresa;
		protected int _IdUnidad;
		protected string _Unidad;
		protected string _Codigo;
		protected string _Nombre;
		protected bool _Estatus;
		public virtual int IdDepartamento
		{
			get
			{
				return _IdDepartamento;
			}
			set
			{
				_IdDepartamento = value;
			}
		}
		public virtual int IdEmpresa
		{
			get
			{
				return _IdEmpresa;
			}
			set
			{
				_IdEmpresa = value;
			}
		}
		public virtual string Empresa
		{
			get
			{
				return _Empresa;
			}
			set
			{
				_Empresa = value;
			}
		}
		public virtual int IdUnidad
		{
			get
			{
				return _IdUnidad;
			}
			set
			{
				_IdUnidad = value;
			}
		}
		public virtual string Unidad
		{
			get
			{
				return _Unidad;
			}
			set
			{
				_Unidad = value;
			}
		}
		public virtual string Codigo
		{
			get
			{
				return _Codigo;
			}
			set
			{
				_Codigo = value;
			}
		}
		public virtual string Nombre
		{
			get
			{
				return _Nombre;
			}
			set
			{
				_Nombre = value;
			}
		}
		public virtual bool Estatus
		{
			get
			{
				return _Estatus;
			}
			set
			{
				_Estatus = value;
			}
		}
		
		public CDepartamentos()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Unidad","IdEmpresa" ,"Empresa"};		
		}
		public override string[] GetFieldsSearchOmitir()
		{
			return new string[] { "Unidad", "Empresa","Estatus" };
		}
	}// CDepartamentos ends.
}// Entidades ends.
