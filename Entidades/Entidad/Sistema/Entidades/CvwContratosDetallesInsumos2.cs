using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 30/03/2017 03:46:53 p. m.*/
namespace Entidades
{

	public partial class CvwContratosDetallesInsumos2 : CContratosDetallesInsumos2, IContratosDetallesInsumos2
	{
		public CvwContratosDetallesInsumos2()
		{
		}
		public override string[] GetOmitirCamposSelect()
		{
			return new string[] { "Codigo", "Descripcion", "Unidad", "Moneda", "CodigoMoneda", 
				                    "MotivoModificacion", "EsAdicional"};
		}		
	}// CContratosDetallesInsumos2 ends.
}// Entidades ends.
