﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Entidades
{
	public interface INegocio
	{
		void InicializaError();
		void SetError(string error);
		void SetError(string error, bool append);
		string GetErrorMessage();
		IError GetErrorDetail();
		void ClearError();
		bool HasError();		
		void AddWhere(string Nombre, string Condicion);
		void AddWhere(string Nombre, string condicion, string Valor);
		void AddWhere(IPropsWhere propiedad);
		void AddWhere(IEnumerable<IPropsWhere> propiedades);
		void ClearWhere();
		IEnumerable<IEntidad> Buscar(int top);	
		IEnumerable<IEntidad> Buscar();
		IEntidad BuscarScalar();
		IPagina<Entity> Buscar<Entity>(string campoKey, int pagina)where Entity:IEntidad,new();
		IPagina<Entity> Buscar<Entity>(string campoKey, int pagina,int registrosPP) where Entity : IEntidad, new();
		void Save();
		void Delete();		
		string TraduceTexto(string cadena);
		IEntidad BuildEntity();
		void SetEntity(IEntidad Entity);
		void ClearEntity();
      void OnError(Exception eInterno);        
        /////
      void Update();
      bool ValidaSave();
      bool ValidaUpdate();        
      bool ValidaDelete();
        //////
		IEnumerable<IPropsWhere> CombineWithFiltro(IEnumerable<IPropsWhere> filtro, IEnumerable<IPropsWhere> where);
		IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities);
	}
}
