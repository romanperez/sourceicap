﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public interface IContext// <Negocio> where Negocio : INegocio, new()
    {
		 INegocio GetNegocio();
		 IEnumerable<IEntidad> New(IEnumerable<IEntidad> Entities);
		 void New(IEntidad Entity);
       void Put(int id, IEntidad Entity);
       void Delete(string key, int id);
		 IEntidad BuscarScalar();
		 IEnumerable<IEntidad> Buscar(int top);
		 IEnumerable<IEntidad> Buscar();
       IPagina<Entity> Buscar<Entity>(int pagina, string key) where Entity : IEntidad, new();       
       IPagina<Entity> Buscar<Entity>(int pagina, int registroPP, string key) where Entity : IEntidad, new();        
       void Where(IEnumerable<IPropsWhere> where);
       IEntidad GetById(string keyId, int id);
       bool HasError();
       IError GetError();
		 IPropsWhere FiltroEmpresa(int IdEmpresa, string Campo);
		 IPropsWhere FiltroEmpresa(int IdEmpresa, string condicion, string Campo);
		 IPropsWhere FiltroEmpresa(int IdEmpresa, string condicion, string Campo, bool OmiteConcatenarTabla);
		 IEnumerable<IPropsWhere> Filtros(params IPropsWhere[] where);
		 IEnumerable<IPropsWhere> Filtros(IEnumerable<IPropsWhere> where1, IEnumerable<IPropsWhere> where2);		
		 bool HasPermiso(string permiso,int perfil,int Menu);		 
		// byte[] ExportExcel(IExcel excelConfig, IEnumerable<IEntidad> Data);
    }//Interdace IContext ends.
}//namespace Entidades ends.