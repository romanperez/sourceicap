﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public interface IExcel
	{
        string FileOutPut { set; get; }
		  string FileName
		  {
			  set;
			  get;
		  }
		  string InterfaceName
		  {
			  set;
			  get;
		  }
		  string NameSheet
		  {
			  set;
			  get;
		  }
        byte[] MainExcel(IEnumerable<IEntidad> datos);
	}
}
