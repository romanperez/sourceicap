﻿using Controladores;
using System.Web;
using System.Web.Mvc;

namespace WEB_ICAP
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
			filters.Add(new IcapAuthorize());
		}
	}
}