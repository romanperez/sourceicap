﻿using System.Web;
using System.Web.Optimization;

namespace WEB_ICAP
{
	public class BundleConfig
	{
		// Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
							"~/Scripts/jquery-1.9.1.js",
							"~/Scripts/jquery-ui-1.9.2.custom.js",	
						   "~/Scripts/moment.js",							
							"~/Scripts/bootstrap/bootstrap.js",
							"~/Scripts/es.js",/*https://github.com/moment/moment se agrega para el idioma español del datapicker de bootstrap*/
							"~/Scripts/bootstrap-datetimepicker.js"));


			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.unobtrusive*",
						"~/Scripts/jquery.validate*"));

			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Site_ICAP.css",
																				  "~/Content/ICAP/Loggin.css",
													    						  "~/Content/Menu/menu-vertical.css",
																				  "~/Content/bootstrap/bootstrap.css",
																				  "~/Content/bootstrap/bootstrap-datetimepicker.css",
																				  "~/Content/jquery-ui-1.9.2.custom.css",
																				  "~/Content/bootstrap/bootstrap-theme.css"));


			bundles.Add(new ScriptBundle("~/Content/Icap").IncludeDirectory("~/Content/ICAP", "*.css", true));

			bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
							"~/Content/themes/base/jquery.ui.core.css",
							"~/Content/themes/base/jquery.ui.resizable.css",
							"~/Content/themes/base/jquery.ui.selectable.css",
							"~/Content/themes/base/jquery.ui.accordion.css",
							"~/Content/themes/base/jquery.ui.autocomplete.css",
							"~/Content/themes/base/jquery.ui.button.css",
							"~/Content/themes/base/jquery.ui.dialog.css",
							"~/Content/themes/base/jquery.ui.slider.css",
							"~/Content/themes/base/jquery.ui.tabs.css",
							"~/Content/themes/base/jquery.ui.datepicker.css",
							"~/Content/themes/base/jquery.ui.progressbar.css",
							"~/Content/themes/base/jquery.ui.theme.css"));

			/*bundles.Add(new ScriptBundle("~/Icap/Sistema").Include("~/Scripts/Sistema/Acceso.js",
																					 "~/Scripts/Sistema/FuncionesGenerales.js",
																					 "~/Scripts/Sistema/Empleados.js",
                                                                "~/Scripts/Sistema/Catalogos.js",
																					 "~/Scripts/Sistema/ICAP_MAIN.js"));*/
			/*bundles.Add(new ScriptBundle("~/Icap/Sistema").Include("~/Scripts/Sistema/Acceso.js",
																					 "~/Scripts/Sistema/FuncionesGenerales.js",
																					 "~/Scripts/Sistema/Empleados.js",
																					 "~/Scripts/Sistema/Catalogos.js",
																					 "~/Scripts/Sistema/ICAP_MAIN.js"));*/
			bundles.Add(new ScriptBundle("~/Icap/Sistema/Base").IncludeDirectory("~/Scripts/Sistema/Base", "*.js", true));
			bundles.Add(new ScriptBundle("~/Icap/Sistema").IncludeDirectory("~/Scripts/Sistema", "*.js",false));

		}
	}
}