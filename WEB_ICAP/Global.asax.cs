﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Entidades;
using Controladores;
using Controladores.MVC;
namespace WEB_ICAP
{
	// Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
	// visite http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		} //Application_Start ends.
		protected void Application_Error(object sender, EventArgs e)
		{
			Exception exception = Server.GetLastError();
			IError error = MvcBase.OnErrorGeneral(exception);			
			Response.Clear();
			HttpException httpException = exception as HttpException;			
			Server.ClearError();			
			Response.Redirect("~/ICAP/Error?msg=" + error.IdError);
		}
		protected void Session_Start(object sender, EventArgs e)
		{			
			MvcBase.OnErrorGeneral(new Exception(String.Format("Session iniciada {0} {1}",GetIP(),System.DateTime.Now)));			
		}
		protected void Session_End(object sender, EventArgs e)
		{
			MvcBase.OnErrorGeneral(new Exception(String.Format("Session terminada {0} {1}", GetIP(), System.DateTime.Now)));
		}
		protected string GetIP()
		{
			string ip=String.Empty;
			try
			{
				ip = Request.ServerVariables["REMOTE_ADDR"];
			}
			catch(Exception err)
			{
				ip = err.Message;
			}
			return ip;
		}
		
	}
	
}