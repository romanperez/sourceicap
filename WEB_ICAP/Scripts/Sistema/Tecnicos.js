﻿ClassIndexIcapMain.prototype.TecnicosInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.tecnicos = new Tecnicos();
    generico.AfterEditar = generico.tecnicos.changeModal;
    generico.AfterNuevo = generico.tecnicos.changeModal;
    generico.gridDelete = generico.tecnicos.AddBtnsDelete;
    generico.AddGrado = generico.tecnicos.AddGrado;
    generico.Save = generico.tecnicos.SaveTecnico;
    generico.Inicializa2 = generico.tecnicos.Inicializa;


    generico.Inicializa3 = generico.tecnicos.Inicializa2;
    generico.LoadTecnicos = generico.tecnicos.LoadTecnicos;
    generico.Inicializa(generico.Inicializa3);


    //generico.Inicializa();    
}
function Tecnicos()
{   
}
Tecnicos.prototype.AddBtnsDelete = function (tbl)
{
    var catalogo = this;
    if (tbl.MovsDelete == null)
    {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');
            var obj = catalogo.ICAP.GetObject(tr);
            if (parseInt(obj.IdTecnico, 10) > 0) {
                tbl.MovsDelete.push(parseInt(obj.IdTecnico, 10));
            }
            $(tr).remove();           
        });
    });
}
Tecnicos.prototype.AddGrado = function (btn)
{
    var catalogo = this;
    try
    {
        var grado = document.getElementById('IEspecialidadesGradoAutoComplete');
        var id = document.getElementById('IdGrado');
        var msjFaltaGrado = $(btn).attr('data-falta-grado');
        var url = $(btn).attr('data-url-plantilla');
        if (id == null || id == undefined || id.ResultSelectionAutoComplete == null || id.ResultSelectionAutoComplete == undefined)
        {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionTecnicos, msjFaltaGrado);
            return;
        }
        $.ajax(
        {
            url: url,
            type: 'POST',
            data: JSON.stringify(id.ResultSelectionAutoComplete),
            contentType: "application/json",
            success: function (html)
            {
                $(catalogo.tblEspecialidades).append(html);
                $(id).val('');
                $(grado).val('');
                catalogo.gridDelete(catalogo.tblEspecialidades);
            }
        });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Tecnicos.prototype.Inicializa = function ()
{
    var catalogo = this;
    catalogo.tblEspecialidades = document.getElementById('tbodyTecnicosEspecialidades');
    catalogo.btnAddGrado = document.getElementById('btnAddGrado');
    catalogo.dvNotificacionesValidacionTecnicos = document.getElementById('dvNotificacionesValidacionTecnicos');
    catalogo.IdEmpleado = document.getElementById('IdEmpleado');
    catalogo.gridDelete(catalogo.tblEspecialidades);
    $(catalogo.btnAddGrado).click(function ()
    {
        catalogo.AddGrado($(this));
    });
    $(catalogo.btnAddGrado).tooltip();
}
Tecnicos.prototype.changeModal = function () {
    var catalogo = this;
    try
    {    
        catalogo.Inicializa2();
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
}
Tecnicos.prototype.SaveTecnico = function (title, wait)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {        
        var msjEspecialidadRepetida = $(this).attr('data-especialidad-duplicado');
        var msjZero = $(this).attr('data-especialidad-zero');
        var msjNoEmpleado = $(this).attr('data-no-empleado');

        var tecnico = new Object();
        tecnico.IdEmpleado = $(catalogo.IdEmpleado).val();
        tecnico.Especialidades = new Array();
        var iNumEspecialidades = 0;
        var EspecialidadesZero = false;
        var hayRepetidos = false;
        if (catalogo.tblEspecialidades == null || catalogo.tblEspecialidades == undefined) {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionTecnicos, msjVacio);
            return;
        }
        $(catalogo.tblEspecialidades).find('tr').each(function () {
            var obj = catalogo.ICAP.GetObject($(this));            
            if (tecnico.Especialidades.length > 0)
            {
                $(tecnico.Especialidades).each(function () {
                    if (this.IdEspecialidad == obj.IdEspecialidad)
                    {
                        hayRepetidos = true;
                    }
                });
            }
            if (!hayRepetidos) {
                tecnico.Especialidades.push(obj);
                iNumEspecialidades++;
            }
        });
        $(catalogo.tblEspecialidades.MovsDelete).each(function () {
            var obj = new Object();
            obj.IdTecnico = this * -1;
            tecnico.Especialidades.push(obj);
        });
        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined) {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }
        var form = document.getElementById(NombreFormulario);
       
        var valido = true;
        if (parseInt(tecnico.IdEmpleado, 10) <= 0)
        {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionTecnicos, msjNoEmpleado);
            valido = false;
        }
        if (tecnico.Especialidades.length <= 0) {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionTecnicos, msjZero);
            valido = false;
        }
        if (hayRepetidos) {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionTecnicos, msjEspecialidadRepetida);
            valido = false;
        }           
        if (valido) {
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            if (title != null && wait != null) {
                catalogo.BuildWaitModal(title, wait);
            }
            $.ajax(
                {
                    url: url,
                    type: method,
                    data: JSON.stringify(tecnico),
                    contentType: "application/json",
                    success: function (html)
                    {
                        catalogo.ICAP.CloseModal();
                        $(catalogo.Notificaciones).html(html);                         
                        catalogo.ReloadCurrentPage();
                    }, error: function (response) {
                        catalogo.ICAP.AddToBodyModal(response.responseText, true);
                    }
                });
        }
        
    });
}


Tecnicos.prototype.LoadTecnicos = function (btn) {
    var catalogo = this;
    try {
        var url = $(btn).attr('data-url-plantilla');
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        var fnLoadExcel = function (frm, btn2) {
            var url = $(btn).attr('data-url-uploadfile');
            var formData = new FormData(frm);
            if (title != null && wait != null) {
                catalogo.ReplaceBodyModal('<div class="dvCatalogosLoading">' + wait + '</div>');
            }
            $.ajax(
              {
                  url: url,
                  type: 'post',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (html) {
                      var hayerror = catalogo.HasError(html);
                      catalogo.ReplaceBodyModal(html);
                      if (!hayerror) {
                         /* catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.AfterNuevo();
                          catalogo.Save(title, wait);*/
                      }
                  }
              });
        };
        var fnAfterLoad = function (objMain, frm) {
            var input = document.getElementById('fileTecnicosXls');
            $(input).change(function () {
                var lbl = document.getElementById('lblTecnicosXlsSelect');
                $(lbl).html(($(this).val().split('\\').pop()));
            });
        };
        catalogo.PlantillaLoadFile(btn,
                                   url,
                                   'get',
                                   function (f, b) {
                                       fnLoadExcel(f, b);
                                   },
                                   function () {
                                       catalogo.ICAP.CloseModal();
                                   },
                                   function (c, frm) {
                                       fnAfterLoad(c, frm);
                                   }
                                   );
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}

Tecnicos.prototype.Inicializa2 = function (main) {
    var catalogo = main;
    catalogo.rptCargaMasivaTecnico = document.getElementById('rptCargaMasivaTecnicos');
    $(catalogo.rptCargaMasivaTecnico).click(function () {
        catalogo.LoadTecnicos($(this));
    });
}