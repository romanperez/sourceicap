﻿function Catalogos(icap, nombreCatalogo, mnuTitle, dvPadre, url, idMenu)
{
    var catalogo = this;
    catalogo.ICAP = icap;    
    catalogo.ControladorName = nombreCatalogo;
    catalogo.NombreCatalogo = nombreCatalogo;
    catalogo.TituloCatalogo = mnuTitle;
    catalogo.dvPadre = dvPadre;
    catalogo.MyUrl = url;
    catalogo.IdMenu = idMenu;
    //catalogo.ConfigDataPicker();
}
Catalogos.prototype.ConfigDataPicker = function ()
{
    var config = SetConfiguracionDatapicker();
    $.datepicker.setDefaults(config);
}
Catalogos.prototype.ToDataPicker = function (html)
{    
    $(html).datepicker();
}
Catalogos.prototype.OnError = function (error)
{
    try
    {
        this.ICAP.OnError(error);
    }
    catch (e)
    {
        alert(error);
    }
}
Catalogos.prototype.BuildWaitModal=function(title,msj)
{
    var catalogo = this;
    catalogo.ICAP.ShowModal(title,'<div class="dvCatalogosLoading">' + msj + '</div>', null, false, null, null);
}
Catalogos.prototype.ReplaceBodyModal = function (body)
{
    var catalogo = this;
    catalogo.ICAP.ChangeBodyModal(body);     
}
Catalogos.prototype.SetUIDatePicker = function ()
{
    $('input.icapDatePicker').each(function ()
    {
       // $(this).datepicker();
        $(this).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY HH:mm:ss' });
    });
}
Catalogos.prototype.SetUIBoostrap = function ()
{
    $('input.text-box').addClass('form-control');
    this.SetUIDatePicker();
    this.GetDivNotificacion();
}
Catalogos.prototype.OnOffCheck = function ()
{
    var catalogo = this;
    var NombreFormulario = catalogo.NombreFormulario;
    if (NombreFormulario == null || NombreFormulario == undefined)
    {
        NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
    }
    var form = document.getElementById(NombreFormulario);
    $(form).find('input[type="checkbox"]').each(function ()
    {
        if (!$(this).hasClass('NoCheckInicial'))
        {
            $(this).attr('checked', 'checked');
        }
    });
}
Catalogos.prototype.HasError = function (html)
{
    var dvAux = document.createElement('div');
    var hasError = null;
    $(dvAux).html(html);
    hasError = $(dvAux).find('div.errorRespMvc').length;
    if (hasError >= 1)
    {
        return true;
    }
    return false;
}
Catalogos.prototype.EventNuevo = function (btn)
{
    var catalogo = this;
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).html();
    var url = catalogo.ControladorName + '/New';
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);        
    $.ajax({
              url: url,
              type: 'GET',
              success: function (html) {
                  var hayerror = catalogo.HasError(html);
                  catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                  $(catalogo.Notificaciones).html('');
                  if (!hayerror) {
                      catalogo.OnOffCheck();
                      catalogo.SetUIBoostrap();
                      catalogo.ICAP.BuildAutoCompletes();
                      catalogo.Save(title, wait);
                      if (catalogo.AfterNuevo != null && catalogo.AfterNuevo != undefined) {
                          catalogo.AfterNuevo();
                      }
                  }
              }
          });    
}
Catalogos.prototype.EventEditar = function (btn)
{
    var catalogo = this;
    var obj = catalogo.GetObject();
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");
    var title = $(btn).html();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    $.ajax(
     {
         url: catalogo.ControladorName + '/Editar/' + obj.Id,
         type: 'GET',
         contentType: 'application/json',
         success: function (html)
         {
             var hayerror = catalogo.HasError(html);
             catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
             $(catalogo.Notificaciones).html('');
             if (!hayerror)
             {             
                 catalogo.SetUIBoostrap();                 
                 catalogo.ICAP.BuildAutoCompletes();
                 catalogo.Save(title, wait);
                 if (catalogo.AfterEditar != null && catalogo.AfterEditar != undefined)
                 {
                     catalogo.AfterEditar();
                 }
             }
         }        
     });
}
Catalogos.prototype.EventExcel = function (btn)
{
    try
    {
        var catalogo = this;
        var parametros = $(catalogo.Paginador).attr("data-parametros");        
        window.open('/' + catalogo.ControladorName + '/Excel?whereIcap=' + parametros,'_blank');        
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Catalogos.prototype.EventEliminar = function (btn)
{
    var catalogo = this;
    var obj = catalogo.GetObject();
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");
    var title = $(btn).attr('data-msj-question');
    if (obj == null) {
        catalogo.ICAP.ShowModal(title, none, null, false, null, null);
        return
    }
    catalogo.BuildWaitModal(title, wait);
    $.ajax(
     {
         url: catalogo.ControladorName + '/Eliminar/' + obj.Id,
         type: 'GET',
         contentType: 'application/json',
         success: function (html)
         {
             var hayerror = catalogo.HasError(html);
             catalogo.ICAP.ShowModal(title, html, null, false, null, null);
             $(catalogo.Notificaciones).html('');
             if (!hayerror)
             {
                 catalogo.EliminaSave(title, wait);
             }
         }        
     });
}
Catalogos.prototype.OcultaColumnas=function(btn)
{
    var catalogo = this;
    var ulMetaInfo = document.getElementById('ulColumnDinamic' + catalogo.NombreCatalogo);
    var colsHide = new Array();
    var colsHideName = new Array();
    $(ulMetaInfo).find('input.colGridDinamic:checkbox:checked').each(function ()
    {
        var columna = $(this).val()
        if (catalogo.Grid != null && catalogo.Grid != undefined)
        {
            $(catalogo.Grid).find('*[data-property="' + columna + '"]').hide();
            colsHide.push($(this));
            colsHideName.push(columna);
        }        
    });
    $(ulMetaInfo).find('input.colGridDinamic:checkbox:not(:checked)').each(function () {
        var columna = $(this).val()
        if (catalogo.Grid != null && catalogo.Grid != undefined) {
            $(catalogo.Grid).find('*[data-property="' + columna + '"]').show();
        }
    });
    catalogo.ColsHide = colsHideName;
    catalogo.ColsHideMeta = colsHide;
    if (catalogo.AfterOculta != null)
    {
        catalogo.AfterOculta(colsHide);
    }
}
Catalogos.prototype.ColumnasDinamicas = function ()
{
    var catalogo = this;    
    var btnAccep = document.getElementById('btnColumnDinamic' + catalogo.NombreCatalogo);
    $(btnAccep).click(function ()
    {
        catalogo.OcultaColumnas($(this));
    });
}
Catalogos.prototype.Inicializa = function (fnInicializaCustom)
{
    var catalogo = this;
    try
    {                        
        var nombreCatalogo = catalogo.NombreCatalogo;
        if (catalogo.postFijo != null && catalogo.postFijo != undefined)
        {
            nombreCatalogo = nombreCatalogo + catalogo.postFijo;
        }
        var titleCatalogo = catalogo.TituloCatalogo;
        var padre = catalogo.dvPadre;
        catalogo.getGrid = function ()
        {   
            var grid = null;
            //if(ControladorName == null ||  ControladorName == undefined)
            //{
            //    grid = document.getElementById('tabla' + nombreCatalogo);
            //}
            //else
            //{
            grid = document.getElementById('tabla' + nombreCatalogo);
            if (grid == null || grid == undefined)
            {
                grid=$(catalogo.dvContenedorTabla).find('table.tblSelectCatalogo')[0];
            }
            //}
            return grid;
        }
        catalogo.getPaginador = function ()
        {
            var paginador = document.getElementById('paginador' + nombreCatalogo);
            if (paginador == null || paginador == undefined)
            {
                paginador = $(catalogo.dvContenedorTabla).find('div.PaginadorIcap')[0];
            }
            return paginador;
        }
        catalogo.dvContenedorTabla = document.getElementById('dvTabla' + nombreCatalogo);
        catalogo.Grid = catalogo.getGrid();
        catalogo.Nuevo = document.getElementById('idToolNuevo' + nombreCatalogo);
        catalogo.Editar = document.getElementById('idToolEditar' + nombreCatalogo);
        catalogo.Eliminar = document.getElementById('idToolEliminar' + nombreCatalogo);
        catalogo.BtnHelpBuscar = document.getElementById('btnToolHelpSearch' + nombreCatalogo);
        catalogo.Buscar = document.getElementById('btnToolBuscar' + nombreCatalogo);
        catalogo.Excel = document.getElementById('idToolExcel' + nombreCatalogo);
        catalogo.Notificaciones = document.getElementById('dvNotificaciones' + nombreCatalogo);
//        catalogo.dvContenedorTabla = document.getElementById('dvTabla' + nombreCatalogo);
        catalogo.txtValueSearch = document.getElementById('txtValueSearch' + nombreCatalogo);
        catalogo.ToolBar = document.getElementById('dvToolBarOpciones' + nombreCatalogo);
        catalogo.SearchFields = new Object();
        catalogo.Paginador = catalogo.getPaginador();
        catalogo.GridClick(catalogo);
        catalogo.BuildPaginador();
        catalogo.BuildFieldsBuscar();
        $(catalogo.Nuevo).click(function (){catalogo.EventNuevo($(this));});
        $(catalogo.Editar).click(function (){catalogo.EventEditar($(this));});
        $(catalogo.Eliminar).click(function () { catalogo.EventEliminar($(this)); });
        $(catalogo.Excel).click(function () { catalogo.EventExcel($(this)); });
        $(catalogo.BtnHelpBuscar).click(function ()
        {
            catalogo.HelpBuscar($(this));
        });
        $(catalogo.Buscar).click(function ()
        {            
            catalogo.IniciaBusqueda(catalogo);
        });
        $(catalogo.txtValueSearch).keypress(function (e)
        {
            if (e.which == 13)
            {
                catalogo.IniciaBusqueda(catalogo);
            }
        });
        if (fnInicializaCustom != null)
        {
            fnInicializaCustom(catalogo);
        }
        catalogo.ColumnasDinamicas();
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Catalogos.prototype.IniciaBusqueda = function (iniCatalogo)
{
    var catalogo = iniCatalogo;
    var wait = $(catalogo.Buscar).attr("data-msj-wait");
    var none = $(catalogo.Buscar).attr("data-msj-none");
    var title = $(catalogo.Buscar).html();
    catalogo.Busqueda(title, wait, none);
}
Catalogos.prototype.GridClick = function (catalogoOriginal)
{
    var catalogo = catalogoOriginal;    
    $(catalogo.Grid).find('tr.ui-rw-cat').each(function ()
    {               
        $(this).click(function ()
        {
            $(catalogo.Grid).find('tr.ui-rw-cat').each(function () { $(this).removeClass('trSeleccionado'); });
            $(this).addClass('trSeleccionado');           
        });
    });
}
Catalogos.prototype.BuildFieldsBuscar = function ()
{
    var catalogo = this;
    $('*.icap-Buscar').each(function () {
        $(this).click(function ()
        {
            var li = $(this).parent();
            catalogo.SearchFields.field = $(li).attr('data-field-name');
            catalogo.SearchFields.omiteConcatenar = $(li).attr('data-field-omiteConcatenar');
            catalogo.SearchFields.plantilla = $(li).attr('data-plantilla');
            catalogo.SearchFields.display = $(li).attr('data-field-display');
            $(catalogo.txtValueSearch).attr('placeholder', catalogo.SearchFields.display);
            catalogo.txtValueSearch.value = null;
        });
    });
}
Catalogos.prototype.HelpBuscar = function (btn)
{
    var catalogo = this;
    try
    {
        var msjWait=$(btn).attr('data-msj-wait');
        var url = $(btn).attr('data-url-busqueda');
        var title = $(btn).attr('data-title');
        catalogo.BuildWaitModal(title, msjWait);
        $.ajax(
           {
               url: url,
               type: 'GET',             
               success: function (html)
               {
                   catalogo.ReplaceBodyModal(html);
               }
           });
    }
    catch (e) {catalogo.OnError(e);}
}

Catalogos.prototype.BuildBusquedaData = function ()
{
    var catalogo = this;
    var searchArray = new Array();
    var search = new Object();
    search["NombreCampo"] = catalogo.SearchFields.field;
    search["Plantilla"] = catalogo.SearchFields.plantilla;
    search["OmiteConcatenarTabla"] = (catalogo.SearchFields.omiteConcatenar == '1') ? true : false;
    search["Valor"] = catalogo.txtValueSearch.value;
    searchArray.push(search);
    return searchArray;
}

Catalogos.prototype.Busqueda = function (title, wait, none)
{
    var catalogo = this;
    try
    {
        var valueSearch = catalogo.txtValueSearch.value;
        var option = catalogo.SearchFields.field;//catalogo.SearchFields.options[catalogo.SearchFields.selectedIndex].value;
        var omite = catalogo.SearchFields.omiteConcatenar;
        if (catalogo.SearchAction == null || catalogo.SearchAction == undefined)
        {
            catalogo.SearchAction = 'Search';
        }
        if (valueSearch == null || option == null)
        {            
            catalogo.ICAP.ShowModal(title, none, null, false, null, null);
            return;
        }

       /* var fnBuildBusqueda = function ()
        {
            var searchArray = new Array();
            var search = new Object();            
            search["NombreCampo"] = option;
            search["Plantilla"] = catalogo.SearchFields.plantilla;
            search["OmiteConcatenarTabla"] = (omite == '1') ? true : false;
            search["Valor"] = valueSearch;
            searchArray.push(search);
            return JSON.stringify(searchArray);
        }   */     
        catalogo.BuildWaitModal(title, wait);
        if (catalogo.BeforeSearch != null) {
            catalogo.BeforeSearch();
        }
        $.ajax(
           {
               url: catalogo.ControladorName + '/' + catalogo.SearchAction + '/1',
               type: 'POST',
               contentType: 'application/json',
               success: function (html)
               {
                   var hayerror = catalogo.HasError(html);
                   $(catalogo.Notificaciones).html('');
                   $(catalogo.dvContenedorTabla).html(html);
                   catalogo.ICAP.CloseModal();
                   if (!hayerror)
                   {                                          
                       catalogo.Grid = catalogo.getGrid();
                       if (catalogo.Grid != null)
                       {
                           catalogo.GridClick(catalogo);
                       }
                       catalogo.Paginador = catalogo.getPaginador();
                       catalogo.BuildPaginador();
                       if (catalogo.AfterSearch != null)
                       {
                           catalogo.AfterSearch();
                       }
                   }
               },
               data:JSON.stringify(catalogo.BuildBusquedaData())// fnBuildBusqueda()
           });
    }
    catch (e) {catalogo.OnError(e);}
}
Catalogos.prototype.ReloadCurrentPage = function (fnAfterSave)
{
    var catalogo = this;
    try
    {   
        if (catalogo.Paginador != null)
        {
            var pages = $(catalogo.Paginador).attr("data-total-pages");
            var url = $(catalogo.Paginador).attr("data-url");
            var parametros = $(catalogo.Paginador).attr("data-parametros");
            var metodo = $(catalogo.Paginador).attr("data-method");
            $(catalogo.dvContenedorTabla).html('');
            catalogo.ICAP.SetLoading(catalogo.dvContenedorTabla);            
            $.ajax(
            {
                url: url + '/' + catalogo.Paginador.currentePage,
                type: metodo,
                contentType: 'application/json',
                data: parametros,
                success: function (html)
                {                   
                    var hayerror = catalogo.HasError(html);
                    $(catalogo.dvContenedorTabla).html(html);
                    catalogo.ICAP.NotLoading(catalogo.dvContenedorTabla);
                    if (!hayerror)
                    {                    
                        catalogo.Grid = catalogo.getGrid();
                        catalogo.GridClick(catalogo);
                        catalogo.Paginador = catalogo.getPaginador();
                        catalogo.BuildPaginador();                     
                        if (fnAfterSave != null && fnAfterSave != undefined) {
                            if (fnAfterSave.afterload == 1) {
                                fnAfterSave.fn();
                            }
                        }
                    }
                    
                }
            });
        }
        if (fnAfterSave != null && fnAfterSave != undefined)
        {
            if (fnAfterSave.afterload == null )
            {
                fnAfterSave();
            }
            
        }
    }
    catch (e) {
        catalogo.OnError(e);
    }
}
Catalogos.prototype.BuildPaginador = function ()
{
    var catalogo = this;
    try
    {        
        if (catalogo.Paginador == null) return;
        var pages =$(catalogo.Paginador).attr("data-total-pages");
        var url =$(catalogo.Paginador).attr("data-url");
        var parametros = $(catalogo.Paginador).attr("data-parametros");
        var metodo = $(catalogo.Paginador).attr("data-method");
        catalogo.Paginador.currentePage = $(catalogo.Paginador).attr("data-current-page");
        
        first = $(catalogo.Paginador).find("span.btnfirst").first();
        previous = $(catalogo.Paginador).find("span.btnprevious").first();
        next = $(catalogo.Paginador).find("span.btnnext").first();
        last = $(catalogo.Paginador).find("span.btnlast").first();       
        var reloadPage = function ()
        {
           if (parseInt(catalogo.Paginador.currentePage,10) <= 0) catalogo.Paginador.currentePage = 1;
           if (parseInt(catalogo.Paginador.currentePage,10) >parseInt(pages,10)) catalogo.Paginador.currentePage = pages;
           $(catalogo.dvContenedorTabla).html('');
           catalogo.ICAP.SetLoading(catalogo.dvContenedorTabla);
           $.ajax(
           {
               url: url + '/' + catalogo.Paginador.currentePage,
               type: metodo,
               contentType: 'application/json',
               data: parametros,
               success: function (html)
               {                   
                   $(catalogo.dvContenedorTabla).html(html);                   
                   catalogo.Grid = catalogo.getGrid();
                   catalogo.GridClick(catalogo);
                   catalogo.Paginador = catalogo.getPaginador();
                   catalogo.BuildPaginador();
                   catalogo.ICAP.NotLoading(catalogo.dvContenedorTabla);
                   if (catalogo.AfterPaginador != null)
                   {
                       catalogo.AfterPaginador();
                   }
               }
           });
        }

        $(first).click(function ()
        {
            catalogo.Paginador.currentePage = 1;
            reloadPage();
        });

        $(previous).click(function () {
            catalogo.Paginador.currentePage--;
            reloadPage();
        });
        $(next).click(function ()
        {            
            catalogo.Paginador.currentePage++;            
            reloadPage();
        });

        $(last).click(function () {
            catalogo.Paginador.currentePage = pages;
            reloadPage();
        });
        $(catalogo.dvPadre).find('input.txtNumberPage').keypress(function (e)
        {
            if (e.which == 13)
            {
                var pageNumber = $(this).val();
                if (pageNumber != isNaN)
                { 
                    if (parseInt(pageNumber, 10) > 0 && parseInt(pageNumber, 10) <= parseInt(pages, 10))
                    {
                        catalogo.Paginador.currentePage = parseInt(pageNumber, 10);
                        reloadPage();
                    }
                }                
            }
        });
    }
    catch (e)
    {
        catalogo.OnError(e);
    }
}

Catalogos.prototype.GetDivNotificacion = function ()
{
    var catalogo = this;
    var NombreFormulario = catalogo.NombreFormulario;
    if (NombreFormulario == null || NombreFormulario == undefined) {
        NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
    }
    var form = document.getElementById(NombreFormulario);
    var dv = $(form).find('div.dvNotificacionCatalogo')[0];
    if (dv != null && dv != undefined)
    {
        catalogo.NotificacionesModal = dv;
    }
}
Catalogos.prototype.SetInfo=function(html)
{
    var catalogo = this;
    if (catalogo.NotificacionesModal != null && catalogo.NotificacionesModal != undefined)
    {
        $(catalogo.NotificacionesModal).html(html);
    }
}
Catalogos.prototype.Save = function (title, wait)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {        

        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined)
        {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }
        var form = document.getElementById(NombreFormulario);
        if ($(form).valid())
        {
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            var data = $(form).serialize();
            //var dvInfo = document.createElement('div');            
            if (title != null && wait != null) {
                //catalogo.BuildWaitModal(title, wait);
                 //$(dvInfo).html(catalogo.GetInfoHtml(wait));
                //catalogo.ICAP.AddToBodyModal($(dvInfo), true);
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));                
            }
            $.ajax(
              {
                  url: url,
                  type: method,
                  data: data,
                  contentType: "application/x-www-form-urlencoded",
                  success: function (html)
                  {
                      var hayerror = catalogo.HasError(html);
                      if (hayerror)
                      {
                          catalogo.SetInfo(html);
                      }
                      else
                      {
                          catalogo.ICAP.CloseModal();
                          $(catalogo.Notificaciones).html(html);
                          catalogo.ReloadCurrentPage(function () {
                              catalogo.Aftersave(title, html);
                          });
                      }
                     /* var dvModal = catalogo.ICAP.GetModal();
                      if (dvModal != null && dvModal!=undefined)
                      {
                          $(dvModal).unbind("hidden.bs.modal");
                          $(dvModal).on('hidden.bs.modal', function ()
                          {
                              alert('cerrando modal !!!!');
                              catalogo.ReloadCurrentPage(function ()
                              {
                                  catalogo.Aftersave(title, html);
                              });
                          });
                      }  */                   
                  }
              });
        }
    });   
}
Catalogos.prototype.EliminaSave = function (title, wait)
{
    var catalogo = this;
    $('#btnElimina' + catalogo.NombreCatalogo).click(function ()
    {
        var url = $(this).attr("data-url-action");
        var id = $(this).attr("data-property-id");        
        if (title != null && wait != null)
        {
            catalogo.BuildWaitModal(title, wait);
        }
        $.ajax(
          {
              url: url + '/' + id,
              type: "post",              
              success: function (html)
              {                
                  catalogo.ICAP.CloseModal();
                  $(catalogo.Notificaciones).html(html);
                  catalogo.ReloadCurrentPage(function ()
                  {
                      catalogo.Aftersave(title,html);
                  });
              }
          });
    });
}
Catalogos.prototype.Aftersave = function (title,msj)
{
    var catalogo = this;
    $(catalogo.Notificaciones).html(msj);
   /* alert('1');
    catalogo.ICAP.CloseModal();
    catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')',  msj , null, false, null, null);
    catalogo.ICAP.ShowModalMode(title, msj);*/
}
Catalogos.prototype.GetRowSelect = function () {
    try {
        var catalogo = this;
        var selected = $(catalogo.Grid).find('tr.trSeleccionado');
        if (selected != null) { return selected[0]; }
        else { return null; }
    }
    catch (e) { this.OnError(e); }
}
Catalogos.prototype.GetAtributos = function (obj)
{
    var ob = new Object();
    ob.prop = $(obj).attr('data-property');
    ob.val = $(obj).attr('data-property-value');
    return ob;
}
Catalogos.prototype.GetObject = function () {
    try {
        var catalogo = this;
        var tr = catalogo.GetRowSelect();
        if (tr != null) {
            var entidadJSON = new Object();
            var objeto = catalogo.GetAtributos(tr);
            if (objeto != null) {
                entidadJSON.NombrePropiedadId = objeto.prop;
                entidadJSON.Id =  objeto.val;
            }
            $(tr).find('td').each(function () {
                var ob1 = catalogo.GetAtributos($(this));
                if (ob1 != null) {
                    entidadJSON[ob1.prop] = ob1.val;
                }
            });
            return entidadJSON;
        }
        return null;
    }
    catch (e) { this.OnError(e); }
}
Catalogos.prototype.ShowErrorInModal = function (elemento, error)
{
    var catalogo = this;
    $(elemento).html(catalogo.ReturnErrorHtml(error));
    $(elemento).show();
}
Catalogos.prototype.InfoInModal = function (elemento, error)
{
    var catalogo = this;
    if (error == "" || error == null || error == undefined)
    {
        error = 'Espere..';
    }
    $(elemento).html('<div class="alert alert-info" role="alert">' + error + '</div>');
    $(elemento).show();
}
Catalogos.prototype.ReturnErrorHtml = function (error)
{
    return '<div  class="alert alert-danger col-md-12" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" >&times;</span></button><spa> ' + error + '</span></div>';
}
Catalogos.prototype.GetInfoHtml=function(msj)
{
    return ' <div class="alert alert-info alert-dismissible" role="alert"> ' +
         '   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> ' +
         msj + ' </div> ';
}
Catalogos.prototype.GetLoadingInfoHtml = function (msj) {
    return ' <div class="alert alert-warning alert-dismissible" role="alert"> ' +
         '   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> ' +
        ' <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>' +
        '<strong style="padding-left:15px;">' + msj + '</strong> </div> ';
}
Catalogos.prototype.PlantillaLoadFile = function (btn,url,metod,fnYes,fnNo,fnAfterload)
{
    catalogo = this;
    try
    {
        if (url != null && url != undefined && url != '')
        {
           var wait = $(btn).attr("data-msj-wait");
           var title = $(btn).html();
           catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
         $.ajax(
         {
             url: url ,
             type: metod,
             success: function (html)
             {
                 catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                 $(catalogo.Notificaciones).html('');
                 catalogo.SetUIBoostrap();                 
                 catalogo.ICAP.BuildAutoCompletes();
                 var frm = document.getElementById('frmGenericoLoadFile');
                 $(frm).find('span.btn-rsp-yes').each(function ()
                 {
                     $(this).click(function () { fnYes(frm,$(this)); });
                 });
                 $(frm).find('span.btn-rsp-no').each(function () {
                     $(this).click(function () { fnNo(frm,$(this)); });
                 });
                 if (fnAfterload != null)
                 {
                     fnAfterload(catalogo,frm);
                 }
             }
         });
        }

    }
    catch (e) { this.OnError(e); }
}
Catalogos.prototype.OnSelectRow=function(tbl,multiple,fnExe)
{
    var catalogo = this;
    $(tbl).find('tr').each(function ()
    {
        $(this).click(function ()
        {
            if (multiple == false)
            {
                $(tbl).find('tr').each(function ()
                {
                    $(this).removeClass('trActivo');
                });
            }
            if ($(this).hasClass('trActivo'))
            {
                $(this).removeClass('trActivo');
            }
            else
            {
                $(this).addClass('trActivo');
                if (fnExe != null)
                {
                    fnExe($(this));
                }
            }
        });
    });
}
Catalogos.prototype.GetMenuByNombre = function (menuNombre)
{
    var catalogo = this;
    var div = document.getElementById('mnuIcapMain');
    var mnu = $(div).find('a[data-name-mnu="' + menuNombre + '"]')[0];
    return mnu;
}
Catalogos.prototype.GetMenuActivo = function ()
{
    var catalogo = this;
    var menu = catalogo.ICAP.GetNameMenuActivo();
    return catalogo.GetMenuByNombre(menu);
}