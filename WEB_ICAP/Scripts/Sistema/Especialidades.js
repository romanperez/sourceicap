﻿ClassIndexIcapMain.prototype.EspecialidadesInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.especialidades = new Especialidades();
    generico.AfterEditar = generico.especialidades.changeModal;
    generico.AfterNuevo = generico.especialidades.changeModal;
    generico.AddEspecialidad = generico.especialidades.AddEspecialidad;
    generico.EspecialidadTR = generico.especialidades.EspecialidadTR;

    generico.ValidaMasivo = generico.especialidades.ValidaMasivo;
    generico.ValidaSimple = generico.especialidades.ValidaSimple;

    generico.Inicializa2 = generico.especialidades.Inicializa;
    generico.Save = generico.especialidades.SaveEspecialidad;

    generico.Inicializa3 = generico.especialidades.Inicializa2;
    generico.LoadEspecialidades = generico.especialidades.LoadEspecialidades;
    generico.Inicializa(generico.Inicializa3);


    //generico.Inicializa();

    
}
function Especialidades()
{    
    
}
Especialidades.prototype.changeModal = function () {
    var catalogo = this;
    try {
        catalogo.Inicializa2();
    }
    catch (ee) {
        catalogo.OnError(ee);
    }
}
Especialidades.prototype.Inicializa = function ()
{
    var catalogo = this;

    
    catalogo.btnAddEspecialidad = document.getElementById('btnAddEspecialidad');
    catalogo.dvNotiValEspecialidades = document.getElementById('dvNotificacionesValidacionEspecialidades');
    catalogo.IEspecialidadesGradoAutoComplete = document.getElementById('IEspecialidadesGradoAutoComplete');
    catalogo.IdEspecialidad = document.getElementById('IdEspecialidad');
    catalogo.Codigo = document.getElementById('Codigo');
    catalogo.Nombre = document.getElementById('Nombre');
    catalogo.IdGrado=document.getElementById('IdGrado');
    catalogo.tBodyAllEspecialidades = document.getElementById('tBodyAllEspecialidades');
    $(catalogo.Nombre).keypress(function (event) 
    {
        if (event.which == 13)
        {
            catalogo.AddEspecialidad(catalogo.btnAddEspecialidad);
            event.preventDefault();
        }});
    $(catalogo.btnAddEspecialidad).click(function ()
    {
        catalogo.AddEspecialidad($(this));
    });
    $(catalogo.IEspecialidadesGradoAutoComplete).focus();
    //$(catalogo.btnAddEspecialidad).tooltip();
}
Especialidades.prototype.EspecialidadTR = function (data)
{        
    var tr = '<tr> ' +
    '   <td data-property="Codigo" data-property-value="' + data.Codigo + '">' + data.Codigo + '</td> ' +
    '   <td data-property="Nombre" data-property-value="' + data.Nombre + '">' + data.Nombre + '</td> ' +
    '   <td><div style="color:#286090"><i class="fa fa-trash fa-2x dvPreview" aria-hidden="true" style="cursor:pointer"></i></div></td> ' +
    '</tr>';
    return tr;
}
Especialidades.prototype.AddEspecialidad = function (btn)
{
    var catalogo = this;
    var msjFaltanDatos = $(btn).attr('data-falta-datos');
    var data = new Object();
    if ($.trim($(catalogo.Codigo).val()) == '' || $.trim($(catalogo.Nombre).val()) == '')
    {
        catalogo.ShowErrorInModal(catalogo.dvNotiValEspecialidades, msjFaltanDatos);
        return;
    }
    data.Codigo = $.trim($(catalogo.Codigo).val());
    data.Nombre = $.trim($(catalogo.Nombre).val());
    var fnEliminaTR = function (tr)
    {
        $(tr).remove();
    }
    var tr = catalogo.EspecialidadTR(data);    
    $(catalogo.tBodyAllEspecialidades).append(tr);

    $(catalogo.tBodyAllEspecialidades).find('i.dvPreview').each(function () {
        $(this).click(function () {
            var tr = $(this).closest('tr');
            fnEliminaTR(tr);
        });
    });

    $(catalogo.Codigo).val('');
    $(catalogo.Nombre).val('');
    $(catalogo.Codigo).focus();
}
Especialidades.prototype.ValidaMasivo = function (especialidades)
{
    var catalogo = this;
    especialidades.IsValid = true;
    if (especialidades.lenght <= 0)
    {
        especialidades.IsValid = false;
    }
    return especialidades;
}
Especialidades.prototype.ValidaSimple = function (id)
{
    var catalogo = this;
    var obj = new Object();
    obj.IdGrado = $(catalogo.IdGrado).val();
    obj.IdEspecialidad = id;
    obj.Codigo = $(catalogo.Codigo).val();
    obj.Nombre = $(catalogo.Nombre).val();
    obj.Estatus = $('#Estatus').prop('checked');// $('#Estatus').val();
    obj.IsValid = true;
    if (parseInt( obj.IdGrado,10) <= 0)
    {
        obj.IsValid = false;
    }
    if (obj.IdEspecialidad <= 0) {
        obj.IsValid = false;
    }
    if ($.trim(obj.Codigo) == '' || $.trim(obj.Nombre) == '')
    {
        obj.IsValid = false;
    }
    return obj;
}
Especialidades.prototype.SaveEspecialidad = function (title, wait)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {      
        var especialidades = new Array();        
        $(catalogo.tBodyAllEspecialidades).find('tr').each(function ()
        {
            var obj = catalogo.ICAP.GetObject($(this));
            obj.IdEspecialidad = 0;
            obj.IdGrado = $(catalogo.IdGrado).val();
            especialidades.push(obj);
        });      
        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined) {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }
        var form = document.getElementById(NombreFormulario);
        var especialidad = new Object();
        if (parseInt($(catalogo.IdEspecialidad).val(), 10) > 0)
        {
            especialidad = catalogo.ValidaSimple(parseInt($(catalogo.IdEspecialidad).val(), 10));
        }
        else
        {
            especialidad = catalogo.ValidaMasivo(especialidades);
        }
        if (especialidad.IsValid)
        {
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            //var dvInfo = document.createElement('div');
            if (title != null && wait != null)
            {
                // catalogo.BuildWaitModal(title, wait);
                //$(dvInfo).html(catalogo.GetInfoHtml(wait));
                //catalogo.ICAP.AddToBodyModal($(dvInfo), true);
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
            }
            $.ajax(
                {
                    url: url,
                    type: method,
                    data: JSON.stringify(especialidad),
                    contentType: "application/json",
                    success: function (html)
                    {
                        var hayerror = catalogo.HasError(html);
                        if (hayerror) {
                            catalogo.SetInfo(html);
                        }
                        else {
                            catalogo.ICAP.CloseModal();
                            $(catalogo.Notificaciones).html(html);
                            catalogo.ReloadCurrentPage();
                        }
                    }
                });
        }
    });
}

Especialidades.prototype.LoadEspecialidades = function (btn) {
    var catalogo = this;
    try {
        var url = $(btn).attr('data-url-plantilla');
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        var fnLoadExcel = function (frm, btn2) {
            var url = $(btn).attr('data-url-uploadfile');
            var formData = new FormData(frm);
            if (title != null && wait != null) {
                catalogo.ReplaceBodyModal('<div class="dvCatalogosLoading">' + wait + '</div>');
            }
            $.ajax(
              {
                  url: url,
                  type: 'post',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (html) {
                      var hayerror = catalogo.HasError(html);
                      catalogo.ReplaceBodyModal(html);
                      if (!hayerror) {
                          catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.AfterNuevo();
                          catalogo.Save(title, wait);
                      }
                  }
              });
        };
        var fnAfterLoad = function (objMain, frm) {
            var input = document.getElementById('fileEspecialidadesXls');
            $(input).change(function () {
                var lbl = document.getElementById('lblEspecialidadesXlsSelect');
                $(lbl).html(($(this).val().split('\\').pop()));
            });
        };
        catalogo.PlantillaLoadFile(btn,
                                   url,
                                   'get',
                                   function (f, b) {
                                       fnLoadExcel(f, b);
                                   },
                                   function () {
                                       catalogo.ICAP.CloseModal();
                                   },
                                   function (c, frm) {
                                       fnAfterLoad(c, frm);
                                   }
                                   );
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}

Especialidades.prototype.Inicializa2 = function (main) {
    var catalogo = main;
    catalogo.rptCargaMasivaEspecialidad = document.getElementById('rptCargaMasivaEspecialidades');
    $(catalogo.rptCargaMasivaEspecialidad).click(function () {
        catalogo.LoadEspecialidades($(this));
    });
}
