﻿function ReporteProyectos(catalogo) {
    if (catalogo != null) {
        this.Inicializa(catalogo);
    }
}

ReporteProyectos.prototype.Inicializa = function (catalogo) {
    var adm = this;
    this.catalogo = catalogo;
    adm.IdProyecto = document.getElementById('IdProyecto');
    adm.btnConceptosPorContratos = document.getElementById('btnConceptosContratosProyectos');
    adm.btnConceptosContratosProyectosInstalado = document.getElementById('btnConceptosContratosProyectosInstalado');
    adm.btnConceptosContratosProyectosSolicitadoInstalado = document.getElementById('btnConceptosContratosProyectosSolicitadoInstalado');
    adm.btnConceptosPorContratosSolicitadoEstado = document.getElementById('btnConceptosContratosProyectosSolicitadoEstado');
    adm.btnConceptosPorContratosSolicitadoArea = document.getElementById('btnConceptosContratosProyectosSolicitadoArea');
    adm.btnConceptosPorContratosInstaladoArea = document.getElementById('btnConceptosContratosProyectosInstaladoArea');
    adm.btnConceptosSolicitadoEstadoArea = document.getElementById('btnConceptoProyectosSolicitadoEstadoArea');
    adm.btnConceptosProyectosSolicitadoInstaladoArea = document.getElementById('btnConceptosProyectosSolicitadoInstaladoArea');
    adm.btnSolicitudInsumosInstalados = document.getElementById('btnSolicitudInsumosInstalados');
    adm.btnSolicitudInsumosInstaladosFecha = document.getElementById('btnSolicitudInsumosInstaladosFecha');
    adm.btnAllEstimaciones = document.getElementById('btnAllEstimaciones');
    adm.dvAdmProyectoBody = document.getElementById('dvAdmProyectoBody');


    if (adm.btnConceptosPorContratos != null) {
        $(adm.btnConceptosPorContratos).click(function () {
            adm.ConceptosPorContratos($(this));
        });
    }

    if (adm.btnConceptosContratosProyectosSolicitadoInstalado != null) {
        $(adm.btnConceptosContratosProyectosSolicitadoInstalado).click(function () {
            var fechaInicial = new Date();
            var fechaFinal = new Date();
            adm.ConceptosPorContratosSolicitadoInstalado($(this), fechaInicial, fechaFinal);
        });
    }


    if (adm.btnConceptosContratosProyectosInstalado != null) {
        $(adm.btnConceptosContratosProyectosInstalado).click(function () {
            adm.ConceptosPorContratosInstalado($(this));
        });
    }

    if (adm.btnConceptosPorContratosSolicitadoEstado != null) {
        $(adm.btnConceptosPorContratosSolicitadoEstado).click(function () {
            adm.ConceptosPorContratosSolicitadoEstado($(this));
        });
    }

    if (adm.btnConceptosPorContratosSolicitadoArea != null) {
        $(adm.btnConceptosPorContratosSolicitadoArea).click(function () {
            adm.ConceptosPorContratosSolicitadoArea($(this));
        });
    }

    if (adm.btnConceptosPorContratosInstaladoArea != null) {
        $(adm.btnConceptosPorContratosInstaladoArea).click(function () {
            adm.ConceptosPorContratosInstaladoArea($(this));
        });
    }


    if (adm.btnConceptosSolicitadoEstadoArea != null) {
        $(adm.btnConceptosSolicitadoEstadoArea).click(function () {
            adm.ConceptosSolicitadoEstadoArea($(this));
        });
}

    if (adm.btnConceptosProyectosSolicitadoInstaladoArea != null) {
        $(adm.btnConceptosProyectosSolicitadoInstaladoArea).click(function () {
            var fechaInicial = new Date();
            var fechaFinal = new Date();
            adm.ConceptosSolicitadoInstaladoArea($(this), fechaInicial, fechaFinal);
        });
    if( adm.btnSolicitudInsumosInstalados!=null)
    {
        $(adm.btnSolicitudInsumosInstalados).click(function () {
            adm.SolicitudInsumosInstalados($(this));
        });
    }
    if (adm.btnSolicitudInsumosInstaladosFecha != null)
    {
        $(adm.btnSolicitudInsumosInstaladosFecha).click(function () {
            adm.SolicitudInsumosInstaladosFecha($(this));
        });
    }
    $(adm.btnAllEstimaciones).click(function () {
        adm.EstimacionesPorContrato($(this));
    });
}
}
ReporteProyectos.prototype.EstimacionesPorContrato = function (btn)
{
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror)
                 {
                     $("#ctCCSIFechaInicial").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });

                     $("#ctCCSIFechaFinal").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });
                     $("#btnBuscarEstimacionesPorFecha").click(function () {
                         adm.ReporteByFechaBoton($(this));
                     });
                     adm.FuncionalidadToolBar(btn,8);
                 }
             }
         });
}


ReporteProyectos.prototype.FiltraContratos = function (btn,btnCaller,fnCustom)
{
    var adm = this;
    catalogo = adm.catalogo;
    var ulContratosDinamicos = document.getElementById('ulContratosDinamicos');
    var url = $(btnCaller).attr('data-url-tbl');    
    var dv = document.getElementById($(btnCaller).attr('data-tbl-target'));
    var msjWait = $(btnCaller).attr('data-msj-wait');
    var id = parseInt($(btnCaller).attr('data-proyecto'), 10);
    var columns = new Array();
    var ids = "";
    var c = 0;
    $(ulContratosDinamicos).find('input.colContratoDinamic:checkbox:checked').each(function ()
    {        
        var id = parseInt($(this).val(), 10);
        var value = id;
        if (id <= 0)
        {
            value = $(this).attr('data-ubicacion');
        }
        columns.push(value);
    });    
    $(columns).each(function ()
    {
        if (c < columns.length-1) {
            ids += this + ",";
        }
        else {
            ids += this;
        }
       c++;
    });
    adm.ListaContratosMostrar = ids;
    $(dv).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = ids;
    if (fnCustom != null)
    {
        eObjeto=fnCustom(eObjeto);
    }
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(dv).html(html);
             }
         });
         
}
ReporteProyectos.prototype.ColumnasDinamicas = function (btnCaller,fnCustom)
{
    var adm = this;
    catalogo = adm.catalogo;
    var msjWait = $(btnCaller).attr('data-msj-wait');
    var dvColumDin = document.getElementById('dvColumnasDinamicasProyecto');
    var url = $(dvColumDin).attr('data-url');
    var id = parseInt($(btnCaller).attr('data-proyecto'), 10);
    $(dvColumDin).html(msjWait);    
    $.ajax(
       {
           url: url + '/' + id,
           type: 'GET',         
           success: function (html)
           {
               var hayerror = catalogo.HasError(html);
               $(dvColumDin).html(html);
               if (!hayerror)
               {                   
                   var btncontratosdinamicos = document.getElementById('btncontratosdinamicos');
                   $(btncontratosdinamicos).click(function () {
                       adm.FiltraContratos($(this), btnCaller,fnCustom);
                   });
               }
           }
       });  
}
ReporteProyectos.prototype.FuncionalidadToolBar = function (btn, tipo)
{
    var adm = this;   
    catalogo = adm.catalogo;

    if (tipo != 4 && tipo != 5 && tipo != 6) {
        adm.btnExcelConceptosContratos = document.getElementById('idToolReportesExportarExcel');
        adm.btnPdfConceptosContratos = document.getElementById('idToolReportesExportarPdf');
    }
    else {
        adm.btnExcelConceptosContratos = document.getElementById('idToolReportesUbicacionesExportarExcel');
        adm.btnPdfConceptosContratos = document.getElementById('idToolReportesUbicacionesExportarPdf');
    }

    if (adm.btnExcelConceptosContratos != null) {
        $(adm.btnExcelConceptosContratos).click(function () {
            if (tipo == 1)
                adm.RptConceptosContratos($(this));
            if (tipo == 2)
                adm.RptConceptosContratosSolicitadoInstalado($(this));
            if (tipo == 3)
                adm.RptConceptosContratosSolicitadoEstado($(this));
            if (tipo == 4)
                adm.RptConceptosContratosSolicitadoArea($(this));
            if (tipo == 5)
                adm.RptConceptosSolicitadoEstadoArea($(this));
            if (tipo == 6)
                adm.RptConceptosSolicitadoInstaladoArea($(this));
            if (tipo == 7)
                adm.RptSolicitudInsumosInstaladoFechaFiltro($(this));
            if (tipo == 8)
                adm.RptSolicitudInsumosInstaladoFechaFiltro($(this));
        });
    }

    if (adm.btnPdfConceptosContratos != null) {
        $(adm.btnPdfConceptosContratos).click(function () {
            if (tipo == 1 )
                adm.RptConceptosContratos($(this));
            if (tipo == 2)
                adm.RptConceptosContratosSolicitadoInstalado($(this));
            if (tipo == 3)
                adm.RptConceptosContratosSolicitadoEstado($(this));
            if (tipo == 4 )
                adm.RptConceptosContratosSolicitadoArea($(this));
            if (tipo == 5)
                adm.RptConceptosSolicitadoEstadoArea($(this));
            if (tipo == 6)
                adm.RptConceptosSolicitadoInstaladoArea($(this));
            if (tipo == 7)
                adm.RptSolicitudInsumosInstaladoFechaFiltro($(this));
            if (tipo == 8)
                adm.RptSolicitudInsumosInstaladoFechaFiltro($(this));
        });
    }
    var fnCustom = null;
    if (tipo == 2 || tipo==6 || tipo ==7 || tipo==8)
    {
        fnCustom = function (obj)
        {
            obj.Fecha = $("#ctCCSIFechaInicial").val();
            obj.FechaFinal = $("#ctCCSIFechaFinal").val();
            return obj;
        }
    }
    adm.CloseTab();
    adm.SetTitleReporte($(btn).attr('data-title'));
    adm.ColumnasDinamicas(btn,fnCustom);
}

ReporteProyectos.prototype.ConceptosPorContratos = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror)
                 {
                     adm.FuncionalidadToolBar(btn, 1);
                 }
             }
         });
}

ReporteProyectos.prototype.ConceptosPorContratosSolicitadoInstalado = function (btn, fechaInicial, fechaFinal) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    eObjeto.Fecha = fechaInicial;
    eObjeto.FechaFinal = fechaFinal;


    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {

                     $("#ctCCSIFechaInicial").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });

                     $("#ctCCSIFechaFinal").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });


                     $("#ctCCSIFechaInicial").val(eObjeto.Fecha.getDate() + "/" + (eObjeto.Fecha.getMonth() + 1) + "/" + eObjeto.Fecha.getFullYear());
                     $("#ctCCSIFechaFinal").val(eObjeto.FechaFinal.getDate() + "/" + (eObjeto.FechaFinal.getMonth() + 1) + "/" + eObjeto.FechaFinal.getFullYear());

                     $("#btnBuscarConceptosSolicitadoInstalado").click(function () {
                         adm.ConceptosPorContratosSolicitadoInstaladoBoton($(this));
                     });

                     adm.FuncionalidadToolBar(btn, 2);
                 }
             }
         });
}

ReporteProyectos.prototype.RptConceptosContratos = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = Proyecto.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
             }
         });
}

ReporteProyectos.prototype.RptConceptosContratosSolicitadoInstalado = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = catalogo.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
        }
    });
}

ReporteProyectos.prototype.ConceptosPorContratosInstalado = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror)
                 {
                     adm.FuncionalidadToolBar(btn, 1);
                 }
             }
         });
             }

ReporteProyectos.prototype.SetTitleReporte = function (title)
{
    $('#hTitleReporte').html(title);   
}

ReporteProyectos.prototype.GetProyecto = function ()
{
    var tr = $('#tblSingleProyecto').find('tr:first');
    var Proyecto = this.catalogo.ICAP.GetObject($(tr));
    return Proyecto;
}

ReporteProyectos.prototype.CloseTab = function () {
    var adm = this;
    $('#dvCloseAllDetale').click(function () {
        $(adm.dvAdmProyectoBody).html('');
         });

}

ReporteProyectos.prototype.ConceptosPorContratosSolicitadoInstaladoBoton = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    if (adm.IdProyecto <= 0) return;
    $("#dvConceptosContratosSolicitadoInstaladoDetalle").html(catalogo.GetLoadingInfoHtml(msjWait));

    var eObjeto = new Object();
    eObjeto.IdContrato = adm.IdProyecto;
    eObjeto.Contrato = adm.ListaContratosMostrar;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();

    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $("#dvConceptosContratosSolicitadoInstaladoDetalle").html(html);
                 if (!hayerror) {

}
             }
         });
}

ReporteProyectos.prototype.ConceptosPorContratosSolicitadoEstado = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {
                     adm.FuncionalidadToolBar(btn, 3);
                 }
             }
         });
}


ReporteProyectos.prototype.RptConceptosContratosSolicitadoEstado = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = catalogo.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
        }
    });
}

ReporteProyectos.prototype.ConceptosPorContratosSolicitadoArea = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {
                     adm.FuncionalidadToolBar(btn, 4);
                 }
             }
         });
}


ReporteProyectos.prototype.RptConceptosContratosSolicitadoArea = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = catalogo.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
        }
    });
}

ReporteProyectos.prototype.ConceptosPorContratosInstaladoArea = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {
                     adm.FuncionalidadToolBar(btn, 4);
                 }
             }
         });
}

ReporteProyectos.prototype.ConceptosSolicitadoEstadoArea = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {
                     adm.FuncionalidadToolBar(btn, 5);
                 }
             }
         });
}


ReporteProyectos.prototype.RptConceptosSolicitadoEstadoArea = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = catalogo.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
             }
         });
}

ReporteProyectos.prototype.ConceptosSolicitadoInstaladoArea = function (btn, fechaInicial, fechaFinal) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    eObjeto.Fecha = fechaInicial;
    eObjeto.FechaFinal = fechaFinal;


    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {

                     $("#ctCCSIFechaInicial").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });

                     $("#ctCCSIFechaFinal").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });


                     $("#ctCCSIFechaInicial").val(eObjeto.Fecha.getDate() + "/" + (eObjeto.Fecha.getMonth() + 1) + "/" + eObjeto.Fecha.getFullYear());
                     $("#ctCCSIFechaFinal").val(eObjeto.FechaFinal.getDate() + "/" + (eObjeto.FechaFinal.getMonth() + 1) + "/" + eObjeto.FechaFinal.getFullYear());

                     $("#btnBuscarConceptosSolicitadoInstaladoArea").click(function () {
                         adm.ConceptosSolicitadoInstaladoBotonArea($(this));
                     });

                     adm.FuncionalidadToolBar(btn, 6);
                 }
             }
         });
}

ReporteProyectos.prototype.ConceptosSolicitadoInstaladoBotonArea = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    if (adm.IdProyecto <= 0) return;
    $("#dvConceptosSolicitadoInstaladoDetalleArea").html(catalogo.GetLoadingInfoHtml(msjWait));

    var eObjeto = new Object();
    eObjeto.IdContrato = adm.IdProyecto;
    //eObjeto.Contrato = adm.ListaContratosMostrar;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();

    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $("#dvConceptosSolicitadoInstaladoDetalleArea").html(html);
                 if (!hayerror) {

                 }
             }
         });
}

ReporteProyectos.prototype.RptConceptosSolicitadoInstaladoArea = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = catalogo.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();;
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();;
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
        }
    });
}

ReporteProyectos.prototype.SolicitudInsumosInstalados = function (btn)
{
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror) {
                     adm.FuncionalidadToolBar(btn, 1);
                 }
             }
         });
}
ReporteProyectos.prototype.SolicitudInsumosInstaladosFecha = function (btn)
{
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;

    adm.IdProyecto = id;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    var eObjeto = new Object();
    eObjeto.IdContrato = id;
    eObjeto.Contrato = null;
    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror)
                 {

                     $("#ctCCSIFechaInicial").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });

                     $("#ctCCSIFechaFinal").datetimepicker({
                         locale: 'es', format: 'DD/MM/YYYY'
                     });                     
                     $("#btnSolicitudInsumosInstaladosFechaFiltro").click(function ()
                     {
                         adm.SolicitudInsumosInstaladoFechaFiltro($(this));
                     });
                     adm.FuncionalidadToolBar(btn, 7);
                 }
             }
         });
}
ReporteProyectos.prototype.SolicitudInsumosInstaladoFechaFiltro = function (btn) {    
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    if (adm.IdProyecto <= 0) return;
    $("#dvInsumosDetalle").html(catalogo.GetLoadingInfoHtml(msjWait));

    var eObjeto = new Object();
    eObjeto.IdContrato = adm.IdProyecto;
    eObjeto.Contrato = adm.ListaContratosMostrar;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();

    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $("#dvInsumosDetalle").html(html);
             }
         });
}

ReporteProyectos.prototype.RptSolicitudInsumosInstaladoFechaFiltro = function (btn) {
    var catalogo = this;
    var Proyecto = catalogo.GetProyecto();
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).attr('data-title');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var eObjeto = new Object();
    eObjeto.IdContrato = Proyecto.IdProyecto;
    eObjeto.Contrato = catalogo.ListaContratosMostrar;
    eObjeto.Descripcion = Proyecto.Proyecto;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();
    catalogo.catalogo.BuildWaitModal(title, wait);
    $.ajax({
        url: url,
        data: JSON.stringify(eObjeto),
        contentType: "application/json",
        type: 'post',
        success: function (response) {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            catalogo.catalogo.ICAP.CloseModal();
        }
    });
}
ReporteProyectos.prototype.ReporteByFechaBoton = function (btn) {
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var dvTarget = $(btn).attr('data-target');
    if (adm.IdProyecto <= 0) return;
    $("#" + dvTarget).html(catalogo.GetLoadingInfoHtml(msjWait));

    var eObjeto = new Object();
    eObjeto.IdContrato = adm.IdProyecto;
    eObjeto.Contrato = adm.ListaContratosMostrar;
    eObjeto.Fecha = $("#ctCCSIFechaInicial").val();
    eObjeto.FechaFinal = $("#ctCCSIFechaFinal").val();

    $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(eObjeto),
             contentType: "application/json",
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $("#" + dvTarget).html(html);
                 if (!hayerror) {

                 }
             }
         });
}