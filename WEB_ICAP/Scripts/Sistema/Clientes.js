﻿ClassIndexIcapMain.prototype.MenusClientes = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros) {
    var tab = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    tab.MenuClientes = new MenuClientes();
    // tab.AfterEditar = tab.MenuInsumos.EsServicio;
    tab.Inicializa2 = tab.MenuClientes.Inicializa2;
    tab.LoadClientes = tab.MenuClientes.LoadClientes;
    tab.Inicializa(tab.Inicializa2);
}
function MenuClientes() {
}
MenuClientes.prototype.Inicializa2 = function (main) {
    var catalogo = main;
    catalogo.rptCargaMasivaCliente = document.getElementById('rptCargaMasivaClientes');
    $(catalogo.rptCargaMasivaCliente).click(function () {
        catalogo.LoadClientes($(this));
    });
}
MenuClientes.prototype.LoadClientes = function (btn) {
    var catalogo = this;
    try {
        var url = $(btn).attr('data-url-plantilla');
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        var fnLoadExcel = function (frm, btn2) {
            var url = $(btn).attr('data-url-uploadfile');
            var formData = new FormData(frm);
            if (title != null && wait != null) {
                catalogo.ReplaceBodyModal('<div class="dvCatalogosLoading">' + wait + '</div>');
            }
            $.ajax(
              {
                  url: url,
                  type: 'post',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (html) {
                      var hayerror = catalogo.HasError(html);
                      catalogo.ReplaceBodyModal(html);
                      if (!hayerror) {
                          catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.AfterNuevo();
                          catalogo.Save(title, wait);
                      }
                  }
              });
        };
        var fnAfterLoad = function (objMain, frm) {
            var input = document.getElementById('fileClientesXls');
            $(input).change(function () {
                var lbl = document.getElementById('lblClientesXlsSelect');
                $(lbl).html(($(this).val().split('\\').pop()));
            });
        };
        catalogo.PlantillaLoadFile(btn,
                                   url,
                                   'get',
                                   function (f, b) {
                                       fnLoadExcel(f, b);
                                   },
                                   function () {
                                       catalogo.ICAP.CloseModal();
                                   },
                                   function (c, frm) {
                                       fnAfterLoad(c, frm);
                                   }
                                   );
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
