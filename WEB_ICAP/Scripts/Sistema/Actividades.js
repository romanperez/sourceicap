﻿ClassIndexIcapMain.prototype.ActividadesInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.actividades = new Actividades();
    generico.Save = generico.actividades.Save;
    generico.GetInfoFile = generico.actividades.GetInfoFile;
    generico.AfterEditar = generico.actividades.Inicializa2;
    generico.AfterNuevo = generico.actividades.Inicializa2;
    generico.Inicializa();
   
}
function Actividades()
{
    var empresa = this;           
}
Actividades.prototype.Inicializa2 = function ()
{
    var catalogo = this;
    try
    {
        $('input.actividadFile').each(function ()
        {
            $(this).change(function () {
                var label = $(this).parent().find('label');
                $(label).html(($(this).val().split('\\').pop()));
            });
        });
    }
    catch (error)
    {
       catalogo.OnError(error);        
    }
}
Actividades.prototype.GetInfoFile = function (form)
{
    var catalogo = this;
    try
    {        
        var aux = document.getElementById('AuxFilesMaps');
        var fileInput1 = document.getElementById('fileProcedimientoSeguridad');
        var fileInput2 = document.getElementById('fileAnalisisRiesgo');
        var saux = '';
        if (fileInput1 != null && fileInput1 != undefined)
        {
            if (fileInput1.files != null && fileInput1.files != undefined && fileInput1.files.length>0) {
                saux = 'fileProcedimientoSeguridad/' + fileInput1.files[0].name;
            }
        }
        if (fileInput2 != null && fileInput2 != undefined && fileInput2.files.length > 0)
        {
            if (fileInput2.files != null && fileInput2.files != undefined) {
                saux = saux+'|fileAnalisisRiesgo/' + fileInput2.files[0].name;
            }
        }        
        $(aux).val(saux);
        var formData = new FormData(form);
        return formData;
    }
    catch (error)
    {
        catalogo.OnError(error);
        return null;
    }
}
Actividades.prototype.fnCambiaImagen = function (btn, dv)
{
    var divParent = $(btn).closest('div');
    $(divParent).hide();
    $(dv).css("visibility", "visible");
    $(dv).show();
}
Actividades.prototype.Save = function (title, wait)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {       
        var form = document.getElementById('formularioEditarNew' + catalogo.NombreCatalogo);
        if ($(form).valid())
        {
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            var data = catalogo.GetInfoFile(form);
            var dvInfo = document.createElement('div');
            if (title != null && wait != null && data != null)
            {
                //catalogo.BuildWaitModal(title, wait);
               /* $(dvInfo).html(catalogo.GetInfoHtml(wait));
                catalogo.ICAP.AddToBodyModal($(dvInfo), true);*/
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
            }
            if (data != null)
            {
                $.ajax(
                  {
                      url: url,
                      type: method,
                      data: data,
                      contentType: false,
                      processData: false,
                      success: function (html)
                      {
                          var hayerror = catalogo.HasError(html);
                          if (hayerror) {
                              catalogo.SetInfo(html);
                          }
                          else {
                              catalogo.ICAP.CloseModal();
                              $(catalogo.Notificaciones).html(html);
                              catalogo.ReloadCurrentPage(function () {
                                  catalogo.Aftersave(title, html);
                              });
                          }
                      }
                  });
            }
        }
    });
}