﻿function Estimaciones(catalogo)
{
    if (catalogo != null)
    {
        this.Inicializa(catalogo);
    }
}
Estimaciones.prototype.Inicializa = function (catalogo)
{
    var adm = this;
    this.catalogo = catalogo;
    adm.dvAdmProyectoBody = document.getElementById('dvAdmProyectoBody');
    this.dvEstimaciones = '#dvContenidoEstimaciones';
    
}
Estimaciones.prototype.SaveAllEstimaciones = function (btn, dvNoti)
{
    var mainEstimaciones = this;
    var catalogo = this.catalogo;
    var idContrato = parseInt($(btn).attr('data-idcontrato'), 10);
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var msjOk = $(btn).attr('data-op-ok');        
    var tbl = document.getElementById('tbdodyEstimacionesConceptos');
    var estimaciones = new Array();
    var importeDeductivo = parseFloat($('#ImporteDeductivo').val(), 10);
    //var PorcentajeAmortizacionAnticipo = parseFloat($('#PorcentajeAmortizacionAnticipo').val(), 10);
    $(tbl).find('tr').each(function ()
    {
        var estimacion = catalogo.ICAP.GetObject($(this));
        estimacion.ImporteDeductivo = importeDeductivo;
        //estimacion.PorcentajeAmortizacionAnticipo = PorcentajeAmortizacionAnticipo;
        estimaciones.push(estimacion);
    });
    /*$(tbl).find('input.Estimacion').each(function ()
    {
        var estimacion = new Object();
        estimacion.IdContratosDetalles = parseInt($(this).attr('data-idcontratodetalle'),10);
        estimacion.Fecha = $(this).attr('data-fecha');
        estimacion.CantidadEstimada =parseFloat($(this).val(),10);
        estimaciones.push(estimacion);
    });*/
    /*if (mainEstimaciones.estimacionesDelete != null && mainEstimaciones.estimacionesDelete.length > 0)
    {
        $(mainEstimaciones.estimacionesDelete).each(function () {
            estimaciones.push(this);
        });
    }*/
    $(dvNoti).html(catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax(
         {
             url: url + '/' + idContrato,
             type: 'POST',
             data: JSON.stringify(estimaciones),
             contentType: "application/json",
             success: function (html)
             {                                
                 var fnHayError = function (ctx,html)
                 {
                     $('#dvNotificacionEstimacion').html('');
                     var hayerror = ctx.catalogo.HasError(html);
                     if (hayerror)
                     {
                         $('#dvNotificacionEstimacion').html(html);
                         //$(ctx.dvEstimaciones).html(html);
                         //mainEstimaciones.EliminaEstimaciones();
                     }                     
                 }
                 var btnReload = document.getElementById('btnEstimacionMainHidden');
                 mainEstimaciones.ContratoEstimacion(idContrato,btnReload,
                     function ()
                     {
                         fnHayError(mainEstimaciones, html);
                     }
                     );
             }
         });
}
Estimaciones.prototype.EliminaEstimaciones = function ()
{
    var estimacion = this;
    var estimacionesDelete = new Array();
    var tbl = document.getElementById('tbdodyEstimacionesConceptos');
    $(tbl).find('td.eliminaEstimacion').each(function ()
    {
        $(this).click(function ()
        {
            var id = parseInt($(this).attr('data-property-value'), 10);            
            $(this).html('');
            $(this).next('td').html('');
            $(this).unbind('click');
            var estimacion = new Object();
            estimacion.IdEstimacion = id * -1;
            estimacionesDelete.push(estimacion);
        });
    });    
    estimacion.estimacionesDelete = estimacionesDelete;
}

Estimaciones.prototype.InicializaToolbar = function ()
{
    var estimacion = this;
    var catalogo = this.catalogo;
    var dvNotificacionEstimacion = document.getElementById('dvNotificacionEstimacion');
    var btnSave = document.getElementById('btnSaveEstimaciones');
    var btnBackMain = document.getElementById('btnBackMain');
    var btnAddEstimacion = document.getElementById('btnAddEstimacion');
    var btnMostrarEstimacion = document.getElementById('btnMostrarEstimacion');
    var btnEstimacionRptCantidadesXls = document.getElementById('btnEstimacionRptCantidadesXls');
    var btnEstimacionRptCantidadesPdf = document.getElementById('btnEstimacionRptCantidadesPdf');
    var btnEstimacionRptResumenEconomico = document.getElementById('btnEstimacionRptResumenEconomico');

    $(btnSave).click(function ()
    {
        estimacion.SaveAllEstimaciones($(this), dvNotificacionEstimacion);
    });
    $(btnBackMain).click(function ()
    {
        estimacion.Back($(this));
    });
    $(btnAddEstimacion).click(function ()
    {
        estimacion.AddEstimacion($(this));
    });
    $(btnMostrarEstimacion).click(function () {
        estimacion.MostrarEstimacion($(this));
    });
    $(btnEstimacionRptCantidadesXls).click(function ()
    {
        estimacion.ReporteEstimacionCantidades($(this));
    });
    $(btnEstimacionRptCantidadesPdf).click(function () {
        estimacion.ReporteEstimacionCantidades($(this));
    });
    $(btnEstimacionRptResumenEconomico).click(function () {
        estimacion.ReporteEstimacionesResumenEconomico($(this));
    });
    estimacion.CloseTab();
}
Estimaciones.prototype.ReporteEstimacionesResumenEconomico = function (btn)
{
    var estimacion = this;
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url');
    var urlRpt = $(btn).attr('data-url-rpt');
    var urlRptDwn = $(btn).attr('data-url-rpt-dw');
    var title = $(btn).attr('data-title');
    var catalogo = estimacion.catalogo;
    catalogo.BuildWaitModal(title, msjWait);
    $.ajax(
      {
          url: url + '/' + estimacion.IdContrato,
          type: 'get',
          success: function (html)
          {
              var hayerror = catalogo.HasError(html);
              catalogo.ReplaceBodyModal(html);
              if (!hayerror)
              {
                  var btnExportaReporte = document.getElementById('btnExportaReporte');
                  $(btnExportaReporte).click(function ()
                  {
                      var form = document.getElementById('frmReporte');
                      var data = $(form).serialize();
                      $('#dvNotificacionCatalogoResmnCostos').html(estimacion.catalogo.GetLoadingInfoHtml(msjWait));
                      $.ajax({
                          url: urlRpt,
                          type: 'POST',
                          data: data,
                          contentType: "application/x-www-form-urlencoded",
                          success: function (response)
                          {
                              catalogo.ICAP.CloseModal();
                              window.location = urlRptDwn + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
                          }
                      });
                  });
                  
              }
          }
      });
    
}
Estimaciones.prototype.ReporteEstimacionCantidades = function (btn)
{
    var estimacion = this;    
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var entity = new Object();
    entity.IdContrato = estimacion.IdContrato;
    entity.NumeroEstimacion = 1;
    if (estimacion.NumeroEstimacion != null && parseInt(estimacion.NumeroEstimacion,10)>0)
    {
        entity.NumeroEstimacion = estimacion.NumeroEstimacion;
    }        
    $('#dvNotificacionEstimacion').html(estimacion.catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax({
        url: url,        
        type: 'POST',
        data: JSON.stringify(entity),
        contentType: "application/json",        
        success: function (response)
        {
            window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
            $('#dvNotificacionEstimacion').html('');            
        }
    });
}

Estimaciones.prototype.MostrarEstimacion = function (btn)
{    
    var estimacion = this;
    var NumeroEstimacion = $('input[name=estimacionSeleccionada]:checked').val();    
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url');
    $(estimacion.dvEstimaciones).html(estimacion.catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax({
        url: url + '/' + estimacion.IdContrato + '?Estimacion=' + NumeroEstimacion,
        type: 'get',                
        success: function (html)
        {
            $(estimacion.dvEstimaciones).html(html);
            estimacion.NumeroEstimacion = NumeroEstimacion;
        }
    });
}
Estimaciones.prototype.SendNewEstimacion = function (btn,estimacionNew)
{
    var estimacion = this;    
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url');
    $(estimacion.dvEstimaciones).html(estimacion.catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax({
        url: url ,
        type: 'post',
        data: estimacionNew,
        contentType: "application/x-www-form-urlencoded",
        success: function (html)
        {
            $(estimacion.dvEstimaciones).html(html);
            $('#btnAddEstimacion').hide();
            $('#liFiltroEstimacion').hide();

        }
    });
}

Estimaciones.prototype.ValidaEstimacion = function (catalogo, msjWait, btn)
{
    var estimacion = this;
    var form = $(btn).closest("form");
    var dv = document.getElementById('dvNotificacionEstimacionNew');
    var url = $(form).attr('action');
    var method = $(form).attr('method');
    var data = $(form).serialize();
    $(dv).html(catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax({url: url,
            type: method,
            data: data,
            contentType: "application/x-www-form-urlencoded",
            success: function (response)
            {
                if (response.IdError != null)
                {
                    $(dv).html(catalogo.ReturnErrorHtml(response.Error));
                }
                else
                {
                    var btnMostrarEstimacion = document.getElementById('btnSendNewEstimacion');
                    catalogo.ICAP.CloseModal();
                    estimacion.SendNewEstimacion(btnMostrarEstimacion, data);
                }                
            }
    });
}
Estimaciones.prototype.AddEstimacion = function (btn)
{
    var estimacion = this;
    var catalogo = this.catalogo;
    var title = $(btn).attr('data-title');
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');

    catalogo.BuildWaitModal(title, msjWait);
    $.ajax(
      {
          url: url + '/' + estimacion.IdContrato,
          type: 'get',
          success: function (html)
          {
              var hayerror = catalogo.HasError(html);
              catalogo.ReplaceBodyModal(html);
              if (!hayerror)
              {
                  $('#FechaInicio').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
                  $('#FechaFin').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
                  $('#btnValidaPeriodoEstimacion').click(function ()
                  {
                      estimacion.ValidaEstimacion(catalogo, msjWait, $(this));
                  });
              }
          }
      });

}
Estimaciones.prototype.Back = function (btn)
{
    var estimacion = this;
    var catalogo = this.catalogo;
    var administraProyectos = new AdministraProyectos(catalogo);
    administraProyectos.LoadAllContratos(btn);
}
Estimaciones.prototype.ContratoEstimacion = function (IdContrato, btnCaller,fnAfter)
{
    var estimacion = this;
    var catalogo = this.catalogo;
    var url = $(btnCaller).attr('data-url');
    var msjWait = $(btnCaller).attr('data-msj-wait');
    var title = $(btnCaller).attr('data-title');    
    $(estimacion.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax(
      {
          url: url + '/' + IdContrato,
          type: 'GET',
          success: function (html)
          {
              var hayerror = catalogo.HasError(html);              
              $(estimacion.dvAdmProyectoBody).html(html);              
              if (!hayerror)
              {
                  estimacion.IdContrato = IdContrato;
                  estimacion.InicializaToolbar();
                  estimacion.EliminaEstimaciones();
                  if (fnAfter != null)
                  {
                      fnAfter();
                  }
              }
          }
      });
}
Estimaciones.prototype.CloseTab = function () {
    var adm = this;
    $('#dvCloseAllDetale').click(function () {
        $(adm.dvAdmProyectoBody).html('');
    });

}