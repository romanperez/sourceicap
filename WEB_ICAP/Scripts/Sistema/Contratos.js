﻿ClassIndexIcapMain.prototype.MenuContratos = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.contrato = new Contratos();
    generico.AfterEditar = generico.contrato.changeModal;
    generico.AfterNuevo = generico.contrato.changeModal;
    generico.GetConceptosByProyecto = generico.contrato.GetConceptosByProyecto;
    generico.GetInsumos = generico.contrato.GetInsumos;    
    generico.Inicializa2 = generico.contrato.Inicializa;
    generico.ApruebaContrato = generico.contrato.ApruebaContrato;
    generico.EnviaAprobacion = generico.contrato.EnviaAprobacion;
    generico.ObiteneIvaPorcentaje = generico.contrato.ObiteneIvaPorcentaje;
    generico.SetUIControles = generico.contrato.SetUIControles;
    generico.AddConcepto = generico.contrato.AddConcepto;
    generico.AddBtnsDelete = generico.contrato.AddBtnsDelete;
    generico.GetContrato = generico.contrato.GetContrato;
    generico.Save = generico.contrato.SaveContrato;
    generico.PreviewContrato = generico.contrato.PreviewContrato;
    generico.ModificaCostos = generico.contrato.ModificaCostos;
    generico.ModificaCostosAfterGet = generico.contrato.ModificaCostosAfterGet;
    generico.GetPreciosUser = generico.contrato.GetPreciosUser;
    generico.fnShowHideInsumos = generico.contrato.fnShowHideInsumos;
    generico.SetNuevosPrecios = generico.contrato.SetNuevosPrecios;
    generico.CotizacionesGeneradas = generico.contrato.CotizacionesGeneradas;
    generico.GeneraCotizacionFromContrato = generico.contrato.GeneraCotizacionFromContrato;
    generico.LogMovimientosPedidos = generico.contrato.LogMovimientosPedidos;
    generico.ConfirmaSolicitudInsumosContrato = generico.contrato.ConfirmaSolicitudInsumosContrato;
    generico.Estimaciones = generico.contrato.Estimaciones;
    if (parametros != null)
    {
        if (parametros.EventNuevo != null) { generico.EventNuevo = parametros.EventNuevo; }
        if (parametros.Proyecto != null) { generico.Proyecto = parametros.Proyecto; }
        if (parametros.postFijo != null) { generico.postFijo = parametros.postFijo; }
        if (parametros.AfterDelete != null) { generico.Aftersave = parametros.AfterDelete; }
        if (parametros.AfterExit != null) { generico.ReloadCurrentPage = parametros.AfterExit; }
        //if (parametros.Grid != null) { generico.Grid = parametros.Grid; }
    }
   // if (parametros != null) {
   //     generico.Inicializa(parametros.InicializaCustom);
   // }
   // else {
        generico.Inicializa();
    //}
    generico.btnToolAprobarContrato = document.getElementById('btnToolAprobarContrato');
    generico.btnToolPreviewContrato = document.getElementById('btnToolPreviewContrato');
    generico.btnToolGeneraCotizacion = document.getElementById('btnToolGeneraCotizacion');
    generico.btnToolShowContratoCotizaciones = document.getElementById('btnToolShowContratoCotizaciones');
    generico.btnSolicitarTodoByProyecto = document.getElementById('btnSolicitarTodoByProyecto');
    generico.btnLogMovimientosPedidos = document.getElementById('btnLogMovimientosPedidos');
    generico.btnAddEstimacion = document.getElementById('btnAddEstimacion');
    $(generico.btnToolAprobarContrato).click(function ()
    {
        generico.ApruebaContrato($(this));
    });
    $(generico.btnToolPreviewContrato).click(function () {
        generico.PreviewContrato($(this));
    });    
    $(generico.btnToolGeneraCotizacion).click(function ()
    {
        generico.GeneraCotizacionFromContrato($(this));
    });
    $(generico.btnToolShowContratoCotizaciones).click(function () {
        generico.CotizacionesGeneradas($(this));
    });
    $(generico.btnSolicitarTodoByProyecto).click(function()
    {
        generico.ConfirmaSolicitudInsumosContrato($(this));
    });
    $(generico.btnLogMovimientosPedidos).click(function () {
        generico.LogMovimientosPedidos($(this));
    });
    $(generico.btnAddEstimacion).click(function ()
    {
        generico.Estimaciones($(this));
    });
    if (parametros != null)
    {
        if (parametros.Grid != null)
        {
            generico.Grid = parametros.Grid;
            generico.GridClick(generico);
        }
    }
}
function Contratos()
{
    
}
Contratos.prototype.LogMovimientosPedidos = function (btn)
{
    var catalogo = this;
    var Element = document.getElementById($(btn).attr('data-element-target'));
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var title = $(btn).html();
    var id = parseInt($(Element).val(), 10);
    if (url == '' || url == null) return;
    if(id<=0 || id ==undefined || isNaN(id))return;
    
    catalogo.BuildWaitModal(title, msjWait);
    $.ajax(
      {
          url: url + '/' + id,
          type: 'get',
          success: function (html)
          {
              var hayerror = catalogo.HasError(html);
              catalogo.ReplaceBodyModal(html);
              if (!hayerror)
              {
                  var adm = new AdministraProyectos(null);
                  adm.catalogo = catalogo;
                  adm.MovimientosPedidos();
              }
          }
      });
}
Contratos.prototype.ConfirmaSolicitudInsumosContrato = function (btn)
{
    var catalogo = this;
    var Element = document.getElementById($(btn).attr('data-element-target'));
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var title = $(btn).html();
    if (url == '' || url == null) return;    
    catalogo.BuildWaitModal(title, msjWait);
    $.ajax(
        {
            url: url ,
            type: 'get',
            success: function (html)
            {
                var hayerror = catalogo.HasError(html);
                catalogo.ReplaceBodyModal(html);
                if (!hayerror)
                {
                    var id = parseInt($(Element).val(), 10);
                    var btnApruebaContrato = document.getElementById('btnApruebaContrato');
                    var btnNoAprobar = document.getElementById('btnNoAprobar');
                    $(btnApruebaContrato).click(function ()
                    {
                        catalogo.EnviaAprobacion(btn, $(this),id);
                    });
                    $(btnNoAprobar).click(function () {
                        catalogo.ICAP.CloseModal();
                    });
                }
            }
        });
}
Contratos.prototype.CotizacionesGeneradas = function (btn)
{
    var catalogo = this;
    var fnBuildCotizacion = function (btn, msjNone, msjWait, url)
    {
        var id = parseInt($(btn).attr('data-id-cotizacion'));
        var coti = new Cotizaciones();
        coti.GetLoadingInfoHtml = catalogo.GetLoadingInfoHtml;
        coti.HasError = catalogo.HasError;
        coti.GetObject = function ()
        {
            var obj = new Object();
            obj.Id = id;
            return obj;
        }
        $(btn).attr('data-msj-none', msjNone);
        $(btn).attr('data-msj-wait', msjWait);
        $(btn).attr('data-url-plantilla', url);
        coti.Btn = btn;
        return coti;
    };
    var fnSetEventos = function ()
    {        
        var tbl = document.getElementById('tblContratosCotizacion');
        var urlPreview = $(tbl).attr('data-url-preview');
        var urlPrint = $(tbl).attr('data-url-print');
        var urlEditar = $(tbl).attr('data-url-editar');
        var msjWait = $(tbl).attr('data-msj-wait');
        var msjNone = $(tbl).attr('data-msj-none');
        $(tbl).find('i.dvContratoCotizacionPreview').each(function ()
        {
            $(this).click(function ()
            {                               
                var coti = fnBuildCotizacion($(this),msjNone,msjWait,urlPreview);
                coti.PreviewCotizacion(coti.Btn, $('#dvResulPreviewCoti'));
            });
        });
        $(tbl).find('i.dvContratoCotizacionPrint').each(function ()
        {
            $(this).click(function ()
            {
                var coti = fnBuildCotizacion($(this), msjNone, msjWait, urlPrint);
                $(coti.Btn).attr('data-msj-url3', urlPrint);
                coti.ImprimirCotizacion(coti.Btn);
            });
        });
        $(tbl).find('i.dvContratoCotizacionEdit').each(function ()
        {
            $(this).click(function ()
            {
                fnEditaCotizacion(msjWait, urlEditar, parseInt($(this).attr('data-id-cotizacion')))
            });
        });
    };
    var fnEditaCotizacion = function (msjWait,url, id)
    {
        catalogo.BuildWaitModal(catalogo.TituloCatalogo, msjWait);
        $.ajax(
     {
         url: url + '/' + id,
         type: 'GET',
         contentType: 'application/json',
         success: function (html)
         {
             var hayerror = catalogo.HasError(html);
             catalogo.ReplaceBodyModal(html);             
             if (!hayerror)
             {
                 var auxNombreCatalogo = catalogo.NombreCatalogo;
                 var coti = new Cotizaciones();
                 coti.ValidaSoloComplemento = true;
                 coti.ICAP = catalogo.ICAP;
                 coti.GetInfoHtml = catalogo.GetInfoHtml;
                 coti.SetInfo = catalogo.SetInfo;
                 coti.HasError = catalogo.HasError;
                 coti.ShowErrorInModal = catalogo.ShowErrorInModal;
                 coti.ReloadCurrentPage = function ()
                 {
                     //catalogo.CotizacionesGeneradas(btn);
                 }
                 catalogo.ICAP.LargeModal(true);
                 catalogo.SetUIBoostrap();
                 catalogo.ICAP.BuildAutoCompletes();
                 coti.NombreCatalogo = 'Cotizaciones';
                 coti.Inicializa();
                 coti.NotificacionesModal = document.getElementById('dvNotificacionesValidacionConceptosCotizaciones');
                 coti.spnClienteContacto = document.getElementById('tabDatosAdicionales');
                 coti.SaveCotizacion(catalogo.TituloCatalogo, msjWait);
                 coti.GetPersonalCliente(coti.spnClienteContacto);
             }
         }
     });
    }
    catalogo.GeneraCotizacionFromContrato(btn,fnSetEventos);
}
Contratos.prototype.GeneraCotizacionFromContrato = function (btn,afterLoad)
{
    var catalogo = this
    var url = $(btn).attr('data-url');
    var msjNone = $(btn).attr('data-msj-none');
    var msjWait = $(btn).attr('data-msj-wait');    
    var title = $(btn).html();
    var obj = catalogo.GetObject();

    if (url == null || url == '' || url == undefined) {        
        return
    }
    if (obj == null || obj.Id == null || obj.Id == undefined)
    {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', msjNone, null, false, null, null);
        return
    }
    cont = new Object();
    cont.IdContrato = obj.Id;
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', msjWait);
    $.ajax(
          {
              url: url,
              type: 'POST',
              data: JSON.stringify(cont),
              contentType: "application/json",
              success: function (html)
              {
                  catalogo.ICAP.LargeModal(true);
                  catalogo.ICAP.ReplaceBodyModal(html);
                  if (afterLoad != null)
                  {
                      afterLoad();
                  }
              }
          });

}
Contratos.prototype.GetInsumos = function (btn)
{   
}
Contratos.prototype.GetConceptosByProyecto = function (btn)
{
    var catalogo = this;
    try
    {
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
}
Contratos.prototype.Inicializa = function ()
{
    var catalogo = this;
    catalogo.tabConceptosByIngenieria = document.getElementById('tabConceptosByIngenieria');
    catalogo.tabExplosionInsumosContratos = document.getElementById('tabExplosionInsumosContratos');
    catalogo.dvNotificaciones = document.getElementById('dvNotificacionesValidacionConceptosContratos');
    catalogo.dvTabConceptosContratos = document.getElementById('dvTabConceptosContratos');
    catalogo.dvTabExpInsumosContratos = document.getElementById('dvTabExpInsumosContratos');
    catalogo.tabModificaCostos = document.getElementById('tabModificaCostos');
    catalogo.IdContrato = document.getElementById('IdContrato');
    catalogo.Fecha = document.getElementById('Fecha');
    catalogo.Iva = document.getElementById('Iva');
    catalogo.Valor = document.getElementById('Valor');
    catalogo.DescuentoMonto = document.getElementById('DescuentoMonto');
    catalogo.Anticipo = document.getElementById('Anticipo');
    catalogo.Utilidad = document.getElementById('Utilidad');
    catalogo.IdPersonalClienteAux = document.getElementById('IdPersonalClienteAux');
    //catalogo.Total = document.getElementById('Total');
    //catalogo.SubTotal = document.getElementById('SubTotal');
    catalogo.btnAddDetalleConceptosContrato = document.getElementById('btnAddDetalleConceptosContrato');
    catalogo.btnAddDetalleConceptosContratoAdicionales = document.getElementById('btnAddDetalleConceptosContratoAdicionales');
    catalogo.TablaDetallesConceptos = document.getElementById('tbdodyConceptosContratos');
    catalogo.TablaDetallesConceptosAdicionales = document.getElementById('tbdodyConceptosContratosAdicionales');

    catalogo.dvNotificacionesValidacionConceptosContratos = document.getElementById('dvNotificacionesValidacionConceptosContratos');
   
    $(catalogo.btnAddDetalleConceptosContrato).click(function ()
    {
        catalogo.AddConcepto($(this));
    });
   
    $(catalogo.btnAddDetalleConceptosContratoAdicionales).click(function () {
        catalogo.AddConcepto($(this));
    });


    catalogo.SetUIControles();
    $(catalogo.tabConceptosByIngenieria).click(function ()
    {
        catalogo.GetConceptosByProyecto($(this));
    });
    $(catalogo.tabModificaCostos).click(function ()
    {
        catalogo.ModificaCostos($(this));
    });
    $(catalogo.tabExplosionInsumosContratos).click(function () {
        catalogo.GetInsumos($(this));
    });
    $(catalogo.Fecha).on("dp.change",function ()
    {        
        catalogo.ObiteneIvaPorcentaje($(this));
    });
    catalogo.AddBtnsDelete(catalogo.TablaDetallesConceptos);
    catalogo.AddBtnsDelete(catalogo.TablaDetallesConceptosAdicionales);    
    catalogo.SetUIControles();
    catalogo.SetUIBoostrap();
    catalogo.ICAP.BuildAutoCompletes();
}
Contratos.prototype.SetNuevosPrecios = function (data)
{
    var catalogo = this;
    try
    {
        var TblConceptos = document.getElementById('tblNewPreciosConceptos');
        var TblAdicionales = document.getElementById('tblNewPreciosConceptosAdicionales');
        var tblTabConceptos = document.getElementById('tbdodyConceptosContratos');
        var tblTabAdicionales = document.getElementById('tbdodyConceptosContratosAdicionales');
        var fnCambiaPrecios = function (tbl,adicional)
        {          
            $(data.Conceptos).each(function ()
            {
                var concepto =this;
                $(tbl).find('tr').each(function ()
                {
                    var conceptoTR = catalogo.ICAP.GetObject($(this));
                    if (conceptoTR.EsAdicional == null)
                    {
                        conceptoTR.EsAdicional = adicional;
                    }
                    var AdicionalObj = new String(conceptoTR.EsAdicional).toLowerCase();
                    var AdicionalTR = new String(concepto.EsAdicional).toLowerCase();

                   

                    if (parseInt(concepto.IdConcepto) == parseInt(conceptoTR.IdConcepto) &&
                        AdicionalObj == AdicionalTR)
                    {
                        var precioFloat = parseFloat(concepto.Precio);
                        var td = $(this).find('td[data-property="Precio"]');
                        $(td[0]).addClass('CellResaltada');
                        if (precioFloat > 0) {
                            $(td[0]).html(concepto.Precio);
                        }
                        else {
                            $(td[0]).html(concepto.MotivoModificacion);
                        }
                        $(td[0]).attr('data-property-value', concepto.Precio);
                    }
                });
            });
        };        
        fnCambiaPrecios(TblConceptos);
        fnCambiaPrecios(TblAdicionales);
        fnCambiaPrecios(tblTabConceptos,false);
        fnCambiaPrecios(tblTabAdicionales,true);
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Contratos.prototype.GetPreciosUser = function ()
{
    var catalogo = this;
    var Conceptos = new Array();
    var tblAllConceptos = document.getElementById('dvTblAllPrecios');
    if (tblAllConceptos == null) return;
    var id = parseInt($(tblAllConceptos).attr('data-has-cambio'), 10);
    if (id == 0) return;
    $(tblAllConceptos).find('tr.trConcepto').each(function ()
    {
        var concepto = catalogo.ICAP.GetObject($(this));
        var dv = $(this).find('div.btnGridSmall');
        if (dv != null) {
            concepto.Insumos = new Array();
            var Insumos = document.getElementById($(dv).attr('data-tr-destino'));
            $(Insumos).find('tr.trInsumo').each(function () {
                var insumo = catalogo.ICAP.GetObject($(this));
                insumo.IdConcepto = concepto.IdConcepto;
                concepto.Insumos.push(insumo);
            });
        }
        Conceptos.push(concepto);
    });    
    return Conceptos;
}
Contratos.prototype.ModificaCostosAfterGet = function (tab)
{
    var catalogo = this;
    try
    {
        var btnAplicaNuevosPrecios = document.getElementById('btnAplicaNuevosPrecios');
        var btnDeshacerNuevosPrecios = document.getElementById('btnDeshacerNuevosPrecios');
        var msjWait = $(tab).attr('data-msj-wait');
        var fnObtienePrecios = function (btn)
        {
            var tblAllConceptos = document.getElementById('dvTblAllPrecios');
            $(tblAllConceptos).attr('data-has-cambio','1');
            var Conceptos = catalogo.GetPreciosUser();            
            var noti = document.getElementById('dvNotificacionesPrecios');            
            var contrato = catalogo.GetContrato();
            contrato.Conceptos = Conceptos;
            contrato.Adicionales = new Array();
            var url = $(btnAplicaNuevosPrecios).attr('data-url');

            
            var fnResumenPrecios = function (dv, data)
            {
                var msjNoTipo = $(dv).attr('data-msj-tipoCambio');
                var msjValorTipo = $(dv).attr('data-valor-tipocambio');
                var msjOkGrl = $(dv).attr('data-op-ok');
                var msjErrorGrl = $(dv).attr('data-op-error');
                var date = $('#Fecha').val();
                var hayErrores = false;
                $(data.Conceptos).each(function ()
                {
                    var concepto = this;
                    $(concepto.Insumos).each(function ()
                    {                        
                        var insumo = this;
                        var nameDiv = new String("#IdR" + insumo.IdConcepto + insumo.IdInsumo + insumo.EsAdicional).toLowerCase();                        
                        var CostoFloat = parseFloat(insumo.Costo);
                        if (CostoFloat <= 0)
                        {                           
                            $(nameDiv).html(msjNoTipo + ' ' + insumo.MotivoModificacion + ' ' + date);
                            $(nameDiv).css('color','#f00');
                            hayErrores = true;
                        }
                        else
                        {                                                   
                            if (insumo.MotivoModificacion != null && $.trim(insumo.MotivoModificacion) != '')
                            {
                                $(nameDiv).html(msjValorTipo + ' ' + insumo.MotivoModificacion + ' ' + date);
                            }
                        }
                        $(nameDiv).css('font-weight', 'bold');
                    });
                });
                if (hayErrores)
                {
                    $('#btnSave' + catalogo.NombreCatalogo).hide();
                    $(noti).html('<div class="alert alert-danger col-md-12" role="alert">' + msjErrorGrl + '</div>');
                }
                else
                {
                    $('#btnSave' + catalogo.NombreCatalogo).show();
                    $(noti).html('<div class="alert alert-success col-md-12" role="alert">' + msjOkGrl + '</div>');
                }                
            }
            
            $(noti).html('<div class="alert alert-info" role="alert">' + msjWait + '</div>');
            $.ajax(
           {
               url: url,
               type: 'POST',
               data: JSON.stringify(contrato),
               contentType: "application/json",
               success: function (data)
               {
                   var contrato = data;
                   if (contrato.IdError != null)
                   {
                       $(noti).html('<div class="alert alert-danger col-md-12" role="alert"> Error:[' + contrato.IdError + ']</div>');
                       $('#btnSave' + catalogo.NombreCatalogo).hide();
                   }
                   else
                   {
                       catalogo.SetNuevosPrecios(data);
                       fnResumenPrecios(noti, data);                       
                   }                   
               }                             
           });

        }
        var fnDeshacer = function ()
        {
            $(tab).attr('data-solo-uno', '0');
            catalogo.ModificaCostos(tab);
        }
        $(btnAplicaNuevosPrecios).click(function () { fnObtienePrecios($(this), tab); });
        $(btnDeshacerNuevosPrecios).click(function () { fnDeshacer(); });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Contratos.prototype.ModificaCostos = function (tab)
{
    var catalogo = this;
    try
    {
        var contrato = catalogo.GetContrato();
        var url = $(tab).attr('data-url');
        var MainTab = document.getElementById($(tab).attr('data-tab'));       
       
        var fnShowHideInsumos = function ()
        {            
            var tblAllConceptos = document.getElementById('dvTblAllPrecios');
            $(tblAllConceptos).find('div.btnGridSmall').each(function () {
                var dv = document.getElementById($(this).attr('data-tr-destino'));
                $(dv).hide();
                $(this).click(function () {
                    var dv = document.getElementById($(this).attr('data-tr-destino'));
                    if ($(dv).is(':visible')) {
                        $(this).removeClass('btnGridDetalleHide');
                        $(this).addClass('btnGridDetalle');
                        $(dv).hide();
                    }
                    else {
                        $(this).removeClass('btnGridDetalle');
                        $(this).addClass('btnGridDetalleHide');
                        $(dv).show();
                    }
                });
            });
        }
        if ($(tab).attr('data-solo-uno') == '0') {
            catalogo.ICAP.SetLoading(MainTab, 250, true);
            $.ajax(
                {
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(contrato),
                    contentType: "application/json",
                    success: function (html)
                    {
                        var hayerror = catalogo.HasError(html);
                        $(MainTab).html(html);
                        catalogo.ICAP.NotLoading(MainTab, true);
                        if (!hayerror) {
                            fnShowHideInsumos();
                            catalogo.ModificaCostosAfterGet(tab);
                            $(tab).attr('data-solo-uno', 1);
                        }
                    }                   
                });
        }
        
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Contratos.prototype.fnShowHideInsumos=function()
{
    var catalogo=this;
    var tblAllConceptos = document.getElementById('tblAllConceptos');
    $(tblAllConceptos).find('div.btnGridSmall').each(function () {
        var dv = document.getElementById($(this).attr('data-tr-destino'));
        $(dv).hide();
        $(this).click(function () {
            var dv = document.getElementById($(this).attr('data-tr-destino'));
            if ($(dv).is(':visible')) {
                $(this).removeClass('btnGridDetalleHide');
                $(this).addClass('btnGridDetalle');
                $(dv).hide();
            }
            else {
                $(this).removeClass('btnGridDetalle');
                $(this).addClass('btnGridDetalleHide');
                $(dv).show();
            }
        });
    });
}
Contratos.prototype.PreviewContrato = function (btn)
{
    var catalogo = this;
    try
    {
        var msjNone = $(btn).attr('data-msj-none');
        var msjWait = $(btn).attr('data-msj-wait');
        var url = $(btn).attr('data-url-plantilla');
        var title = $(btn).html();
        var obj = catalogo.GetObject();
        if (obj == null || obj.Id == null || obj.Id == undefined) {
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', msjNone, null, false, null, null);
            return
        }
        catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', msjWait);       
        $.ajax(
          {
              url: url + '/' + obj.Id,
              type: 'get',
              success: function (html)
              {
                  var hayerror = catalogo.HasError(html);
                  catalogo.ReplaceBodyModal(html);
                  if (!hayerror) {
                      catalogo.OnOffCheck();
                      catalogo.SetUIBoostrap();
                      catalogo.ICAP.BuildAutoCompletes();
                      catalogo.ICAP.LargeModal(true);
                      catalogo.fnShowHideInsumos();
                  }
              }
          });

    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Contratos.prototype.changeModal = function ()
{    
    var catalogo = this;
    try
    {
        catalogo.ICAP.LargeModal(true);
        catalogo.Inicializa2();
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
}
Contratos.prototype.ApruebaContrato = function (btn)
{
    var catalogo = this;
    try
    {
        var obj = catalogo.GetObject();
        var wait = $(btn).attr("data-msj-wait");
        var none = $(btn).attr("data-msj-none");
        var url = $(btn).attr("data-url-aprobar");
        var title = $(btn).html();
        if (obj == null)
        {
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
            return
        }
        catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);       
        $.ajax(
         {
             url: url + '/' + obj.Id,
             type: 'GET',
             contentType: 'application/json',
             success: function (html)
             {
                 var hayerror = catalogo.HasError(html);
                 catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                 $(catalogo.Notificaciones).html('');
                 if (!hayerror)
                 {
                     catalogo.SetUIBoostrap();                  
                     catalogo.ICAP.BuildAutoCompletes();
                     var btnApruebaContrato = document.getElementById('btnApruebaContrato');
                     var btnNoAprobar = document.getElementById('btnNoAprobar');
                     $(btnApruebaContrato).click(function () {
                         catalogo.EnviaAprobacion(btn, $(this));
                     });
                     $(btnNoAprobar).click(function () {
                         catalogo.ICAP.CloseModal();
                     });
                 }
             }            
         });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}

Contratos.prototype.EnviaAprobacion = function (btnPadre,btn,id) {
    var catalogo = this;
    try {        
        var wait = $(btnPadre).attr("data-msj-wait");
        var none = $(btnPadre).attr("data-msj-none");
        var url = $(btn).attr("data-url");       
        var title = $(btnPadre).html();

        if (id <= 0 || isNaN(id))
        {
            return;
        }
        if (url == '' || url == undefined || url == null)
        {
            return;
        }
        catalogo.BuildWaitModal(title, wait);
        $.ajax(
         {
             url: url + '/' + id,
             type: 'GET',
             contentType: 'application/json',
             success: function (html)
             {
                 var hayerror = catalogo.HasError(html);
                 catalogo.ReplaceBodyModal(html);
             }
         });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Contratos.prototype.SetUIControles = function ()
{
    var catalogo = this;
    try
    {        
        $(catalogo.Iva).attr('type', 'number');
        $(catalogo.Iva).removeClass('form-control');
        $(catalogo.Iva).addClass('my-form-control  SimboloPorcentaje');

        $(catalogo.Valor).attr('type', 'number');
        $(catalogo.Valor).removeClass('form-control');
        $(catalogo.Valor).addClass('my-form-control  SimboloMoneda');

        $(catalogo.DescuentoMonto).attr('type', 'number');
        $(catalogo.DescuentoMonto).removeClass('form-control');
        $(catalogo.DescuentoMonto).addClass('my-form-control  SimboloMoneda');

        $(catalogo.Anticipo).attr('type', 'number');
        $(catalogo.Anticipo).removeClass('form-control');
        $(catalogo.Anticipo).addClass('my-form-control  SimboloMoneda');

        $(catalogo.Utilidad).attr('type', 'number');
        $(catalogo.Utilidad).removeClass('form-control');
        $(catalogo.Utilidad).addClass('my-form-control  SimboloPorcentaje');

       /* $(catalogo.Total).attr('type', 'number');
        $(catalogo.Total).removeClass('form-control');
        $(catalogo.Total).addClass('my-form-control  SimboloMoneda');

        $(catalogo.SubTotal).attr('type', 'number');
        $(catalogo.SubTotal).removeClass('form-control');
        $(catalogo.SubTotal).addClass('my-form-control  SimboloMoneda');*/
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Contratos.prototype.ObiteneIvaPorcentaje = function (date)
{
    var co = new Cotizaciones();
    co.CotizacionesElement = new Object();
    co.CotizacionesElement.controles = new Object();
    co.CotizacionesElement.controles.IdIva = document.getElementById('IdIva');
    co.CotizacionesElement.controles.Iva = document.getElementById('Iva');
    co.ObiteneIvaPorcentaje(date);
}
Contratos.prototype.AddConcepto = function (btn)
{
    var catalogo = this;
    try {
        var existe = false;
        var url = $(btn).attr('data-url-plantilla');
        var tipo = $(btn).attr('data-tipo');
        var msjFaltaConcepto = $(btn).attr('data-falta-concepto');
        var msjYaExiste = $(btn).attr('data-existe-concepto');
        var tblTarget = document.getElementById($(btn).attr('data-tblTarget'));

        var id = document.getElementById('IdConceptoAddCotratoAux');
        var auxConcepto = document.getElementById('IdConceptoContratoAutoComplete');
       

        if (id == null || id == undefined || id.ResultSelectionAutoComplete == null || id.ResultSelectionAutoComplete == undefined) {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjFaltaConcepto);
            return;
        }
        $(tblTarget).find('tr.ContratoConcepto').each(function ()
        {
            var obj = catalogo.ICAP.GetObject($(this));
            if (obj.IdConcepto == id.ResultSelectionAutoComplete.IdConcepto) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjYaExiste);
                existe = true;
            }
        });
        if (existe) { return; }
        var contrato = catalogo.GetContrato();
        contrato.Conceptos = new Array();
        contrato.Conceptos.push(id.ResultSelectionAutoComplete)
        $.ajax(
         {
             url: url + '/' + tipo,
             type: 'POST',
             data: JSON.stringify(contrato),
             contentType: "application/json",
             success: function (html)
             {
                 var hayerror = catalogo.HasError(html);
                 $(tblTarget).append(html);
                 $(id).val('');
                 $(auxConcepto).val('');
                 if (!hayerror) {
                     catalogo.AddBtnsDelete(tblTarget);
                 }
             }
         });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }

}
Contratos.prototype.AddBtnsDelete = function (tbl)
{
    var catalogo = this;
    if (tbl.MovsDelete == null)
    {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function () {
        $(this).click(function () {
            var tr = $(this).closest('tr');
            var obj = catalogo.ICAP.GetObject(tr);
            if (parseInt(obj.IdContratosDetalles, 10) > 0) {
                tbl.MovsDelete.push(parseInt(obj.IdContratosDetalles, 10));
            }
            $(tr).remove();            
        });
    });
}
Contratos.prototype.GetContrato = function ()
{
    var catalogo = this;
    var contrato = new Object();   
    contrato.IdContrato = $('#IdContrato').val();
    contrato.IdCliente = $('#IdCliente').val();
    contrato.IdIva = $('#IdIva').val();    
    contrato.Fecha = $('#Fecha').val();
    contrato.Iva = $('#Iva').val();
    contrato.Valor = $('#Valor').val();
    contrato.Contrato = $('#Contrato').val();
    contrato.DescuentoMonto = $('#DescuentoMonto').val();
    contrato.Anticipo = $('#Anticipo').val();
    contrato.IdMoneda = $('#IdMoneda').val();
    contrato.IdPersonalCliente = parseInt($('#IdPersonalClienteAux').val(),10);
    contrato.MotivoModificacion = $('#MotivoModificacion').val();
    contrato.Utilidad = $(catalogo.Utilidad).val();
    contrato.Conceptos = new Array();
    contrato.Adicionales = new Array();
    contrato.TotalConceptos = 0;
    $(catalogo.TablaDetallesConceptos).find('tr.ContratoConcepto').each(function ()
    {
        var obj = catalogo.ICAP.GetObject($(this));
        contrato.Conceptos.push(obj);
        contrato.TotalConceptos++;
    });
    $(catalogo.TablaDetallesConceptosAdicionales).find('tr.ContratoConcepto').each(function ()
    {
        var obj = catalogo.ICAP.GetObject($(this));
        contrato.Adicionales.push(obj);        
    });
    if (catalogo.Proyecto != null)
    {
        contrato.Proyecto = catalogo.Proyecto;
    }
    return contrato;
}
Contratos.prototype.SaveContrato = function (title, wait)
{
    var catalogo = this;
    $('#btnSalirContrato').click(function ()
    {
        catalogo.ICAP.CloseModal();
        catalogo.ReloadCurrentPage();    
    });
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {
        var msjVacia = $(this).attr('data-contrato-vacia');
        var msjNoCliente = $(this).attr('data-contrato-nocliente');
        var msjTipoCambio = $(this).attr('data-contrato-tipocambio');
        var msjiva = $(this).attr('data-contrato-iva');
        var msjVaciaUbicacion = $(this).attr('data-ubicacion-empty');
        var msjOK = $(this).attr('data-op-ok');
        var contrato = catalogo.GetContrato();
        
        $(catalogo.TablaDetallesConceptos.MovsDelete).each(function () {
            var obj = new Object();
            obj.IdContratosDetalles = this * -1;
            contrato.Conceptos.push(obj);
        });

        $(catalogo.TablaDetallesConceptosAdicionales.MovsDelete).each(function () {
            var obj = new Object();
            obj.IdContratosDetalles = this * -1;
            contrato.Adicionales.push(obj);
        });

        
        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined) {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }
        var form = document.getElementById(NombreFormulario);
      

       /* if ($(form).valid())
        {*/
            var valido = true;
            if (contrato.IdCliente <= 0) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjNoCliente);
                valido = false;
            }
            if (contrato.IdIva <= 0) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjiva);
                valido = false;
            }
           /* if (contrato.IdTipoCambio <= 0) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjTipoCambio);
                valido = false;
            }*/
            if (contrato.TotalConceptos <= 0) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjVacia);
                valido = false;
            }
            $(contrato.Conceptos).each(function ()
            {
                var obj = this;
                if (obj.Ubicacion == '')
                {
                    catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjVaciaUbicacion);
                    valido = false;
                }
            });
            $(contrato.Adicionales).each(function ()
            {
                var obj = this;
                if (obj.Ubicacion == '')
                {
                    catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjVaciaUbicacion);
                    valido = false;
                }                                
            });
            contrato.CostosManuales = catalogo.GetPreciosUser();
            if (contrato.CostosManuales != null)
            {
                if (contrato.MotivoModificacion == null || $.trim(contrato.MotivoModificacion) == '')
                {
                    var msjNoMotivo = $('#MotivoModificacion').attr('data-msj-empty');
                    catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionConceptosContratos, msjNoMotivo);
                    valido = false;
                }
            }
            if (valido)
            {
                
                var url = $(form).attr("action");
                var method = $(form).attr("method");
                if (title != null && wait != null) {
                    //catalogo.BuildWaitModal(title, wait);
                    catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                }

                $.ajax(
                  {
                      url: url,
                      type: method,
                      data: JSON.stringify(contrato),
                      contentType: "application/json",
                      success: function (html)
                      {
                          var hayerror = catalogo.HasError(html);
                          if (hayerror)
                          {
                              catalogo.SetInfo(html);
                          }
                          else
                          {
                              /*catalogo.ICAP.CloseModal();
                              $(catalogo.Notificaciones).html(html);
                              catalogo.ReloadCurrentPage();*/
                              catalogo.ICAP.ReplaceBodyModal(html);
                              catalogo.AfterEditar();
                              catalogo.Save(title, wait);
                              catalogo.SetInfo('<div class="alert alert-success" role="alert"> ' +
                                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                    '<span aria-hidden="true">&times;</span></button> ' + msjOK + '</div>');
                          }
                      }
                  });
            }
       /* }*/
    });
}

Contratos.prototype.Estimaciones = function (btn)
{
    var catalogo = this;
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");    
    var estimacion = new Estimaciones(catalogo);
    var obj = catalogo.GetObject();
    if (obj == null || obj.Id == null || obj.Id == undefined) {
        catalogo.ICAP.ShowModal( catalogo.TituloCatalogo , none, null, false, null, null);
        return
    }
    estimacion.ContratoEstimacion(obj.Id, btn);
}
/*AutoComplete.prototype.ObtieneTipoCambioContrato = function (value, seleccion) {
    var Valor = document.getElementById('Valor');
    var IdTipoCambio = document.getElementById('IdTipoCambio');
    var fecha = document.getElementById('Fecha');
    var url = $(Valor).attr('url');
    var tipo = new Object();
    tipo.FechaInicio = $(fecha).val();
    tipo.IdMonedaOrigen = seleccion.IdMoneda;
    tipo.IdMonedaDestino = value;
    $(Valor).addClass('inputLoading');
    $(IdTipoCambio).val('');
    $(Valor).val('');
    $.ajax(
    {
        url: url,
        type: 'POST',
        data: JSON.stringify(tipo),
        contentType: "application/json",
        success: function (tipo) {
            $(Valor).removeClass('inputLoading');
            if (tipo.Error == null) {
                $(Valor).val(tipo.Valor);
                $(IdTipoCambio).val(tipo.IdTipoCambio);
            }
        }
    });
}*/
