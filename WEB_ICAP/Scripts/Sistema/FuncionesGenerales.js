﻿function General()
{
}
General.prototype.GetDivICAP = function ()
{
    return document.getElementById('dvHeadIcap');    
}
General.prototype.GetICAP = function ()
{
    var gn = new General();
    return gn.GetDivICAP().ICAP;
}
General.prototype.SetICAP = function (obj) {
    var gn = new General();
    gn.GetDivICAP().ICAP = obj;
}
function SetConfiguracionDatapicker ()
{
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    return $.datepicker.regional['es'];    
}
General.prototype.Test = function ()
{
    alert('test');
}
function GeneralPrueba()
{
    alert('ddd');
}
function InicializaRootMain(time)
{
    try
    {       
        var main = new ClassIndexIcapMain();
        main.Inicializa();
        main.MantenerSession(time);
        var grl = new General();
        grl.SetICAP(main);
    }
    catch (eIndex) {
        alert(' Inicializacion ' + eIndex);
    }
}