﻿function InsumosByProyecto()
{
}

InsumosByProyecto.prototype.CollapseGrid= function (grid)
{
    $(grid).find('[data-toggle="tooltip"]').each(function () {
        $(this).tooltip();
    });
    $(grid).find('tr.tdDetailContenido').each(function () { $(this).hide(); });
    $(grid).find('td.dvdetail').each(function () {
        $(this).click(function () {
            var div = document.getElementById($(this).attr('data-target-div'));
            if (div != null)
            {
                var trPadre = $(this).closest('tr');
                if ($(div).is(":visible"))
                {
                    $(div).hide();
                    $(this).find('>div>i').removeClass("fa-minus");
                    $(this).find('>div>i').addClass("fa-plus");
                    $(trPadre).removeClass('trActivo');
                }
                else {
                    $(div).show();
                    $(this).find('>div>i').removeClass("fa-plus");
                    $(this).find('>div>i').addClass("fa-minus");
                    $(trPadre).addClass('trActivo');
                }
            }
        });
    });
}