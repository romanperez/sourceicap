﻿ClassIndexIcapMain.prototype.InstaladoSolicitado = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros) {
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    var rptInstaladoSolicitado = new CInstaladoSolicitado();
    generico.Inicializa2 = rptInstaladoSolicitado.Inicializa;
    generico.fnInstaladoSolicitado = rptInstaladoSolicitado.Reporte;
    generico.Inicializa(function () { generico.Inicializa2(); });
}
function CInstaladoSolicitado()
{

}
CInstaladoSolicitado.prototype.Inicializa = function ()
{
    var catalogo = this;
    catalogo.FechaIni = document.getElementById('FechaIni');
    catalogo.FechaFin = document.getElementById('FechaFin');
    catalogo.Proyecto = document.getElementById('Proyecto');
    catalogo.IdCliente = document.getElementById('IdCliente');
    catalogo.btnRptInstaladoSolicitado = document.getElementById('btnRptInstaladoSolicitado');
    catalogo.dvResultQuery = document.getElementById('dvResultQuery');
    $(catalogo.FechaIni).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $(catalogo.FechaFin).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    catalogo.SetUIBoostrap();
    catalogo.ICAP.BuildAutoCompletes();
    $(catalogo.btnRptInstaladoSolicitado).click(function ()
    {
        catalogo.fnInstaladoSolicitado($(this));
    });    
}
CInstaladoSolicitado.prototype.Reporte = function (btn)
{
    var catalogo = this;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    if (url == '' || url == undefined || url == null) return;
    var data = new Object();
    data.FechaIni = $(catalogo.FechaIni).val();
    data.FechaFin =  $(catalogo.FechaFin).val();
    data.Proyecto =  $(catalogo.Proyecto).val();
    data.IdCliente = $(catalogo.IdCliente).val();
    $(catalogo.dvResultQuery).html(catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax(
       {
           url: url,
           type: 'POST',
           data: JSON.stringify(data),
           contentType: "application/json",
           success: function (html)
           {
               var hayerror = catalogo.HasError(html);
               $(catalogo.dvResultQuery).html(html);
               if (!hayerror)
               {
                   catalogo.gridResult = document.getElementById('tblAllProyectosInstaladoSolicitado');
                   var grd = new InsumosByProyecto();
                   grd.CollapseGrid(catalogo.gridResult);
               }
           }
       });
}