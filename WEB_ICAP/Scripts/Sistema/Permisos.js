﻿ClassIndexIcapMain.prototype.PermisosInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{   
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.Permisos = new Permisos();
    generico.Permisos.ICAP = generico.ICAP;
    generico.Permisos.OnError = generico.OnError;
    generico.Permisos.Inicializa();
}
function Permisos()
{
    this.MenusSeleccionados = null;
}

Permisos.prototype.PermisosPerfil = function (id,perfil)
{    
    var permisos = this;
    var html = "";
    var url = $(permisos.dvPerfiles).attr('data-url');
    permisos.IdPerfil = id;   
    $(permisos.Tree).addClass('fondoLasecBodyLoading');
    html = $(permisos.Tree).html();
    $(permisos.Tree).html('');
    $(permisos.Tree).height(200);
    $.ajax({url: url + '/' + id,
            type: 'GET',            
            contentType: "application/json",
            success: function (respuesta)
            {
                if (respuesta.Error != null && respuesta.Error != undefined)
                {
                    permisos.OnError(respuesta.Error);
                }
                else
                {
                    $(permisos.Tree).html(html);
                    $(permisos.Tree).height('auto');
                    $(permisos.Tree).jstree("deselect_all");
                    $(respuesta.MenuVer).each(function ()
                    {                                              
                        var mnu = new String(this);
                        var id = mnu.split('|');
                        $(permisos.Tree).jstree(true).select_node(id[0]);                                                                                  
                    });
                    $(respuesta.MenuPermisos).each(function ()
                    {
                        var mnu = new String(this);
                        var id = mnu.split('|');
                        $(permisos.Tree).jstree(true).select_node(id[0]);
                    });
                    permisos.OldPermisos = respuesta;
                    $(permisos.Tree).removeClass('fondoLasecBodyLoading');
                    $(permisos.TitleMenu).html(perfil);
                    $(permisos.Tree).jstree('close_all');
                }
            },
            error: function (response) {
                permisos.ICAP.AddToBodyModal(response.responseText, true);
            }
    });
}
Permisos.prototype.FuncionalidadPerfiles = function ()
{
    var permisos = this;    
    $('#dvPerfiles').find('li.permisoPerfil').each(function ()
    {
        var id = $(this).attr('data-perfil-id');
        var name = $(this).attr('data-perfil-nombre');
        $(this).click(function (event)
        {
            permisos.PermisosPerfil(id, name);
        });
    });
}
Permisos.prototype.Inicializa = function ()
{
    var permisos = this;
    try
    {        
        permisos.Tree = document.getElementById('dvTreePermisos');
        permisos.Notificaciones = document.getElementById('dvNotificacionesPermisos');
        permisos.dvMenusPermisos = document.getElementById('dvMenusPermisos');
        permisos.url = $(permisos.dvMenusPermisos).attr("data-url");
        permisos.btnSavePermisos = document.getElementById('btnSavePermisos');
        permisos.dvPerfiles = document.getElementById("dvPerfiles");
        permisos.TitleMenu = document.getElementById('hTreeTitleMenu');
        permisos.FuncionalidadPerfiles();
        $(permisos.Tree).jstree(
        {
            "checkbox": {
                "keep_selected_style": false
            },
            "plugins": ["checkbox"]
        });
        $(permisos.Tree).on("changed.jstree", function (e, data)
        {            
            permisos.MenusSeleccionados = data.selected;
            
        });       
        $(permisos.btnSavePermisos).click(function () { permisos.Save();})
    }
    catch(eIni)
    {
        permisos.OnError(eIni);
    }
}
Permisos.prototype.BuildPermisos=function(element)
{
    var permisos = this;    
    var isPermiso = $('#' + element).attr('data-mnu-permiso');
    var idMenu = $('#' + element).attr('data-mnu-id');
    if (isPermiso == "1")
    {
        var idPermiso = $('#' + element).attr('data-mnu-idpermiso');
        permisos.NewPermisos.MenuPermisos.push(idMenu + "," + idPermiso);
    }
    else
    {
        permisos.NewPermisos.MenuVer.push(idMenu);
    }

}
Permisos.prototype.Save=function()
{
    var permisos = this;
    try
    {       
        if (permisos.IdPerfil == null || permisos.IdPerfil == undefined)
        {
            var msj = $(permisos.btnSavePermisos).attr('data-porfile-none');
            if (msj != null && msj != undefined)
            {
                permisos.OnError(msj);
            }
            
            return null;
        }
        if (permisos.MenusSeleccionados == null || permisos.MenusSeleccionados == undefined) return null;
        $(permisos.Tree).jstree("open_all");
        permisos.NewPermisos = new Object();
        permisos.NewPermisos.IdPerfil = permisos.IdPerfil;
        permisos.NewPermisos.MenuVer = new Array();
        permisos.NewPermisos.MenuPermisos = new Array();

        $(permisos.MenusSeleccionados).each(function ()
        {
            permisos.BuildPermisos(this);
        });
        var htmlMenus = $(permisos.Tree).html();
        $(permisos.Tree).html('');
        $(permisos.Tree).height(200);
        $(permisos.Tree).addClass('fondoLasecBodyLoading');
        $.ajax(
             {
                 url: permisos.url,
                 type: 'POST',
                 data: JSON.stringify(permisos.NewPermisos),
                 contentType: "application/json",
                 success: function (html)
                 {
                     $(permisos.Notificaciones).html(html);
                     $(permisos.Tree).html(htmlMenus);
                     $(permisos.Tree).removeClass('fondoLasecBodyLoading');
                     $(permisos.Tree).height('auto');
                 },
                 error: function (xhr, ajaxOptions, thrownError)
                 {                                      
                     $(permisos.Notificaciones).html(thrownError);
                 }
             });


    }
    catch(eIni)
    {
        permisos.OnError(eIni);
    }
}