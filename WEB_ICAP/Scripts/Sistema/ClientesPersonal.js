﻿ClassIndexIcapMain.prototype.MenusClientesPersonales = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros) {
    var tab = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    tab.MenuClientesPersonal = new MenuClientesPersonal();
    // tab.AfterEditar = tab.MenuInsumos.EsServicio;
    tab.Inicializa2 = tab.MenuClientesPersonal.Inicializa2;
    tab.LoadClientesPersonal = tab.MenuClientesPersonal.LoadClientesPersonal;
    tab.Inicializa(tab.Inicializa2);
}
function MenuClientesPersonal() {
}
MenuClientesPersonal.prototype.Inicializa2 = function (main) {
    var catalogo = main;
    catalogo.rptCargaMasivaClientePersonal = document.getElementById('rptCargaMasivaClientesPersonal');
    $(catalogo.rptCargaMasivaClientePersonal).click(function () {
        catalogo.LoadClientesPersonal($(this));
    });
}
MenuClientesPersonal.prototype.LoadClientesPersonal = function (btn) {
    var catalogo = this;
    try {
        var url = $(btn).attr('data-url-plantilla');
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        var fnLoadExcel = function (frm, btn2) {
            var url = $(btn).attr('data-url-uploadfile');
            var formData = new FormData(frm);
            if (title != null && wait != null) {
                catalogo.ReplaceBodyModal('<div class="dvCatalogosLoading">' + wait + '</div>');
            }
            $.ajax(
              {
                  url: url,
                  type: 'post',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (html) {
                      var hayerror = catalogo.HasError(html);
                      catalogo.ReplaceBodyModal(html);
                      if (!hayerror) {
                          catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.AfterNuevo();
                          catalogo.Save(title, wait);
                      }
                  }
              });
        };
        var fnAfterLoad = function (objMain, frm) {
            var input = document.getElementById('fileClientesPersonalXls');
            $(input).change(function () {
                var lbl = document.getElementById('lblClientesPersonalXlsSelect');
                $(lbl).html(($(this).val().split('\\').pop()));
            });
        };
        catalogo.PlantillaLoadFile(btn,
                                   url,
                                   'get',
                                   function (f, b) {
                                       fnLoadExcel(f, b);
                                   },
                                   function () {
                                       catalogo.ICAP.CloseModal();
                                   },
                                   function (c, frm) {
                                       fnAfterLoad(c, frm);
                                   }
                                   );
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
