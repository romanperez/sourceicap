﻿ClassIndexIcapMain.prototype.MenuCotizaciones = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.cotizaciones = new Cotizaciones();
    if (parametros != null) {
        if (parametros.Controlador != '' && parametros.Controlador != undefined) {
            generico.ControladorName = parametros.Controlador;
        }
        if (parametros.NameSaveButton != '' && parametros.NameSaveButton != undefined) {
            generico.NameSaveButton = parametros.NameSaveButton;
        }
        if (parametros.SearchAction != '' && parametros.SearchAction != undefined) {
            generico.SearchAction = parametros.SearchAction;
        }
    }
    generico.AfterEditar = generico.cotizaciones.changeModal;
    generico.AfterNuevo = generico.cotizaciones.changeModal;
    generico.AddConcepto = generico.cotizaciones.AddConcepto;
    generico.AddBtnsDelete = generico.cotizaciones.AddBtnsDelete;
    generico.AddBtnDetalleCapitulo = generico.cotizaciones.AddBtnDetalleCapitulo;
    generico.ExpInsumos = generico.cotizaciones.ExpInsumos;
    generico.GroupConceptos = generico.cotizaciones.GroupConceptos;
    generico.TotalConceptos = generico.cotizaciones.TotalConceptos;
    generico.ObiteneIvaPorcentaje = generico.cotizaciones.ObiteneIvaPorcentaje;
    generico.ActualizaTotal = generico.cotizaciones.ActualizaTotal;
    //generico.SetPorcentajeDescuentoGlobal = generico.cotizaciones.SetPorcentajeDescuentoGlobal;
    generico.SetMontoDescuentoGlobal = generico.cotizaciones.SetMontoDescuentoGlobal;
    generico.AplicaIva = generico.cotizaciones.AplicaIva;
    generico.AplicaDescuento = generico.cotizaciones.AplicaDescuento;
    generico.Save = generico.cotizaciones.SaveCotizacion;
    generico.SetUIControles = generico.cotizaciones.SetUIControles;
    generico.RealizaCalculos = generico.cotizaciones.RealizaCalculos;
    generico.CrearProyecto = generico.cotizaciones.CrearProyecto;
    generico.LoadCotizacion = generico.cotizaciones.LoadCotizacion;
    generico.GetCotizacion = generico.cotizaciones.GetCotizacion;
    generico.SetCotizacion = generico.cotizaciones.SetCotizacion;
    generico.SetTipoCambio = generico.cotizaciones.SetTipoCambio;
   // generico.QueryMoneda = generico.cotizaciones.QueryMoneda;    
    generico.ImprimirCotizacion = generico.cotizaciones.ImprimirCotizacion;
    generico.CopiarCotizacion = generico.cotizaciones.CopiarCotizacion;
    generico.PreviewCotizacion = generico.cotizaciones.PreviewCotizacion;
    generico.Inicializa2 = generico.cotizaciones.Inicializa;
    generico.GetPersonalCliente = generico.cotizaciones.GetPersonalCliente;
    generico.SetEventoListas = generico.cotizaciones.SetEventoListas;
    generico.AddPersonal = generico.cotizaciones.AddPersonal;
    generico.DelPersonal = generico.cotizaciones.DelPersonal;    
    generico.fnShowHideInsumos = generico.cotizaciones.fnShowHideInsumos;
    generico.Inicializa();
    generico.btnToolCrearProyecto = document.getElementById('btnToolCrearProyecto');
    generico.btnToolImprimeCotizacion = document.getElementById('btnToolImprimeCotizacion');
    generico.btnToolCopiarCotizacion = document.getElementById('btnToolCopiarCotizacion');
    generico.btnToolPreviewCotizacion = document.getElementById('btnToolPreviewCotizacion');

    generico.btnToolPreviewCotizacionAll = document.getElementById('btnToolPreviewCotizacionAll');
    generico.btnToolImprimeCotizacionAll = document.getElementById('btnToolImprimeCotizacionAll');


    $(generico.btnToolCrearProyecto).click(function ()
    {
        generico.CrearProyecto($(this));
    });
    generico.btnToolCargaCotizacion = document.getElementById('btnToolCargaCotizacion');
    $(generico.btnToolCargaCotizacion).click(function () {
        generico.LoadCotizacion($(this));
    });
    $(generico.btnToolImprimeCotizacion).click(function ()
    {
        generico.ImprimirCotizacion($(this));
    });
    $(generico.btnToolImprimeCotizacionAll).click(function () {
        generico.ImprimirCotizacion($(this));
    });
    $(generico.btnToolCopiarCotizacion).click(function ()
    {
        generico.CopiarCotizacion($(this));
    });
    $(generico.btnToolPreviewCotizacion).click(function ()
    {
        generico.PreviewCotizacion($(this));
    });
    $(generico.btnToolPreviewCotizacionAll).click(function () {
        generico.PreviewCotizacion($(this));
    });
}
function Cotizaciones()
{
    
}
Cotizaciones.prototype.GetPersonalCliente=function(tab)
{
    var catalogo = this;
    var IdCliente = parseInt($(catalogo.CotizacionesElement.controles.IdCliente).val(), 10);
    if (IdCliente <= 0) return;
    var IdClienteTab = parseInt($(tab).attr('data-IdCliente'));
    var url = $(tab).attr('data-url');
    var msjWait = $(tab).attr('data-msj-wait');
    if (url == null || $.trim(url) == '') return;
    if (IdClienteTab == IdCliente) return;
    $(catalogo.selAtencionCotizacion).html('<option selected="selected" value="0">' + msjWait + '</option>');
    $(catalogo.selContactoCotizacion).html('<option selected="selected" value="0">' + msjWait + '</option>');
    $(catalogo.ulAtencionCotizacion).find('li').each(function () {
        if (parseInt($(this).attr('data-id'), 10) == 0) {
            $(this).remove();
        }
    });
    $(catalogo.ulContactoCotizacion).find('li').each(function () {
        if (parseInt($(this).attr('data-id'), 10) == 0) {
            $(this).remove();
        }
    });
    $.ajax(
      {
          url: url + '/' + IdCliente,
          type: 'POST',
          success: function (personal)
          {
              var hayerror = catalogo.HasError(personal);
              if (hayerror) {
                  catalogo.SetInfo(personal);
              }
              else {
                  $(catalogo.selAtencionCotizacion).html(personal);
                  $(catalogo.selContactoCotizacion).html(personal);
                  $(tab).attr('data-IdCliente', IdCliente);
              }
          }
      });
}
Cotizaciones.prototype.fnShowHideInsumos = function ()
{
    var tblAllConceptos = document.getElementById('tblAllConceptos');
    $(tblAllConceptos).find('div.btnGridSmall').each(function () {
        var dv = document.getElementById($(this).attr('data-tr-destino'));
        $(dv).hide();
        $(this).click(function () {
            var dv = document.getElementById($(this).attr('data-tr-destino'));
            if ($(dv).is(':visible')) {
                $(this).removeClass('btnGridDetalleHide');
                $(this).addClass('btnGridDetalle');
                $(dv).hide();
            }
            else {
                $(this).removeClass('btnGridDetalle');
                $(this).addClass('btnGridDetalleHide');
                $(dv).show();
            }
        });
    });
}
Cotizaciones.prototype.PreviewCotizacion = function (btn,divTarget)
{
    var catalogo = this;
    try {
        var msjNone = $(btn).attr('data-msj-none');
        var msjWait = $(btn).attr('data-msj-wait');
        var url = $(btn).attr('data-url-plantilla');
        var title = $(btn).html();
        var obj = catalogo.GetObject();
        if (obj == null || obj.Id == null || obj.Id == undefined) {
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', msjNone, null, false, null, null);
            return
        }
        if (divTarget == null || divTarget == undefined) 
        {
            catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', msjWait);
        }
        else
        {
            $(divTarget).html(catalogo.GetLoadingInfoHtml(msjWait));
        }
        $.ajax(
          {
              url: url + '/' + obj.Id,
              type: 'get',
              success: function (html)
              {
                  var hayerror = catalogo.HasError(html);
                  if (divTarget == null || divTarget == undefined)
                  {
                      catalogo.ReplaceBodyModal(html);
                  }
                  else
                  {
                      $(divTarget).html(html);
                  }
                  if (!hayerror)
                  {
                      if (divTarget == null || divTarget == undefined)
                      {
                          catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.ICAP.LargeModal(true);                         
                      }
                      catalogo.fnShowHideInsumos();                      
                  }
              }

          });

    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.CopiarCotizacion = function (btn)
{
    var catalogo = this;
    try
    {
        var msjNone=$(btn).attr('data-msj-none');
        var msjWait = $(btn).attr('data-msj-wait');
        var url = $(btn).attr('data-msj-url');
        var title = $(btn).html();
        var obj = catalogo.GetObject();
        if (obj == null || obj.Id == null || obj.Id == undefined)
        {
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', msjNone, null, false, null, null);
            return
        }
        catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', msjWait);
            $.ajax(
              {
                  url: url + '/' + obj.Id,
                  type: 'get',
                  success: function (html)
                  {                                            
                      catalogo.ICAP.CloseModal();
                      $(catalogo.Notificaciones).html(html);
                      catalogo.ReloadCurrentPage();
                  }
              });       

    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.LoadCotizacion = function (btn)
{
    var catalogo = this;
    try
    {
        var url = $(btn).attr('data-url-plantilla');
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        var fnLoadExcel = function (frm,btn2)
        {
            var url = $(btn).attr('data-url-uploadfile');
            var formData = new FormData(frm);            
            if (title != null && wait != null)
            {
                catalogo.ReplaceBodyModal('<div class="dvCatalogosLoading">' + wait + '</div>');
            }
            $.ajax(
              {
                  url: url,
                  type: 'post',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (html)
                  {
                      var hayerror = catalogo.HasError(html);
                      catalogo.ReplaceBodyModal(html);
                      if (!hayerror)
                      {
                          catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.AfterNuevo();
                          catalogo.Save(title, wait);
                      }
                  }
              });
        };
        var fnAfterLoad = function (objMain, frm)
        {
            var input = document.getElementById('fileCotizacionXls');
            $(input).change(function ()
            {
                var lbl =document.getElementById('lblCotizacionXlsSelect');
                $(lbl).html(($(this).val().split('\\').pop()));
            });
        };
        catalogo.PlantillaLoadFile(btn,
                                   url,
                                   'get',
                                   function (f,b)
                                   {
                                       fnLoadExcel(f,b);
                                   },
                                   function ()
                                   {
                                       catalogo.ICAP.CloseModal();
                                   },
                                   function (c, frm)
                                   {
                                       fnAfterLoad(c,frm);
                                   }
                                   );
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.GroupConceptos = function (btn)
{
    var catalogo = this;
    try
    {
        var idCoti = $(catalogo.CotizacionesElement.controles.IdCotizacion).val();
        var ids = '';
        var url = $(btn).attr('data-url-group');        
        $(catalogo.TablaDetalles).find('tr.ConceptoCotizacion').each(function () {
            var obj = catalogo.ICAP.GetObject($(this));
            ids = ids + obj.IdConcepto + ",";
        });
        if (ids != null && ids != '') {
            var where = new Object();
            where.Valor = ids;
            catalogo.ICAP.SetLoading(dvTabConceptosCap, 250, true);
            url = url + '/' + idCoti;// + catalogo.QueryMoneda();
            $.ajax(
                {
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(where),
                    contentType: "application/json",
                    success: function (html)
                    {
                        $(catalogo.dvTabConceptosCap).html(html);
                        catalogo.ICAP.NotLoading(dvTabConceptosCap, true);
                        catalogo.AddBtnDetalleCapitulo(catalogo.dvTabConceptosCap,where);
                    }
                });
        }
        else {
            $(catalogo.dvTabConceptosCap).html('');
        }
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.ExpInsumos = function (btn)
{
    var catalogo = this;
    try
    {
        var ids = '';
        var idCoti = $(catalogo.CotizacionesElement.controles.IdCotizacion).val();        
        var url = $(btn).attr('data-url-insumos');     
        $(catalogo.TablaDetalles).find('tr.ConceptoCotizacion').each(function ()
        {
            var obj = catalogo.ICAP.GetObject($(this));
            ids = ids + obj.IdConcepto + ",";
        });
        if (ids != null && ids != '') {
            var where = new Object();
            where.Valor = ids;
            catalogo.ICAP.SetLoading(dvTabExpInsumos, 250, true);
            url = url + '/' + idCoti;// + catalogo.QueryMoneda();
            $.ajax(
                {
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(where),
                    contentType: "application/json",
                    success: function (html) {
                        $(catalogo.dvTabExpInsumos).html(html);
                        catalogo.ICAP.NotLoading(dvTabExpInsumos, true);
                    }
                });
        }
        else
        {
            $(catalogo.dvTabExpInsumos).html('');
        }
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.TotalConceptos = function (tbl, destino)
{
    var catalogo = this;
    try
    {
        var total = 0;        
        $(tbl).find('tr.ConceptoCotizacion').each(function ()
        {
            var obj = catalogo.ICAP.GetObject($(this));
            total =  parseFloat(total,10) + parseFloat(obj.Precio,10)
        });
        $(destino).html(total);
        $(destino).attr('data-value', total);        
        catalogo.ActualizaTotal(total);
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.AddBtnDetalleCapitulo = function (tbl,where)
{
    var catalogo = this;    
    var idCoti = $(catalogo.CotizacionesElement.controles.IdCotizacion).val();
    $(tbl).find('div.btnGridDetalle').each(function ()
    {
        $(this).click(function ()
        {
            var wheres = new Array();
            var where2 = new Object();
            var url = $(this).attr('data-url-detail');
            var tr = $(this).closest('tr');
            var btn = this;
            if (btn.ControlDestino == null) {
                var obj = catalogo.ICAP.GetObject(tr);
                where2.Valor = obj.IdCapitulo;
                where.NombreCampo = "IdConcepto";
                where2.NombreCampo = "IdCapitulo";
                wheres.push(where);
                wheres.push(where2);
                $(btn).removeClass('btnGridDetalle');
                $(btn).addClass('btnGridSmallLoading');
                url = url + '/' + idCoti;// + catalogo.QueryMoneda()
                $.ajax(
                  {
                      url: url,
                      type: 'POST',
                      data: JSON.stringify(wheres),
                      contentType: "application/json",
                      success: function (html)
                      {                         
                          $(tr).after(html);
                          $(btn).removeClass('btnGridSmallLoading');
                          $(btn).addClass('btnGridDetalleHide');
                          btn.ControlDestino = document.getElementById('trDetailCapitulo' + obj.IdCapitulo);                          
                      }
                  });
            }
            else
            {
                $(btn.ControlDestino).remove();
                $(btn).addClass('btnGridDetalle');
                $(btn).removeClass('btnGridDetalleHide');
                btn.ControlDestino = null;
            }
        });
    });

}
Cotizaciones.prototype.AddBtnsDelete = function (tbl)
{
    var catalogo = this;
    if (tbl.MovsDelete == null)
    {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');            
            var obj = catalogo.ICAP.GetObject(tr);
            if (parseInt(obj.IdCotizacionDetalle, 10) > 0)
            {
                tbl.MovsDelete.push(parseInt(obj.IdCotizacionDetalle, 10));
            }
            $(tr).remove();
            catalogo.TotalConceptos(tbl, catalogo.CotizacionesElement.controles.dvTotalConceptos);
        });
    });
    
}
Cotizaciones.prototype.AddConcepto = function (btn)
{
    var catalogo = this;
    try
    {
        //var existe = false;
        var url = $(btn).attr('data-url-plantilla');
        var msjFaltaInsumo = $(btn).attr('data-falta-concepto');
        var msjYaExiste = $(btn).attr('data-existe-concepto');
        var id = document.getElementById('idConceptoAddAux');
        var cliente = document.getElementById('IdCotiClienteAutoComplete');
        var idMoneda = $(catalogo.CotizacionesElement.controles.IdMoneda).val();
        if (id == null || id == undefined || id.ResultSelectionAutoComplete == null || id.ResultSelectionAutoComplete == undefined)
        {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjFaltaInsumo);
            return;
        }    
    /*
        $(catalogo.TablaDetalles).find('tr.ConceptoCotizacion').each(function ()
        {
            var obj = catalogo.ICAP.GetObject($(this));
            if (obj.IdConcepto == id.ResultSelectionAutoComplete.IdConcepto)
            {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjYaExiste);
                existe = true;
            }
        });
        if (existe) { return; }*/
        var cotizacion = catalogo.GetCotizacion();
        cotizacion.Conceptos = new Array();
        cotizacion.Conceptos.push(id.ResultSelectionAutoComplete);
    $.ajax(
     {
         url: url ,
         type: 'POST',
         data: JSON.stringify(cotizacion),
         contentType: "application/json",
         success: function (html)
         {
             $(catalogo.TablaDetalles).append(html);
             $(id).val('');
             $(cliente).val('');             
             catalogo.AddBtnsDelete(catalogo.TablaDetalles);
             catalogo.TotalConceptos(catalogo.TablaDetalles, catalogo.CotizacionesElement.controles.dvTotalConceptos);
         }
     });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.ObiteneIvaPorcentaje = function (datePicker)
{
    var catalogo = this;
    try
    {
        var url = datePicker.attr('data-url-ivaCambio');
        var tipo = new Object();
        tipo.FechaInicio = $(datePicker).val();
        $(datePicker).addClass('inputLoading');
        $.ajax(
        {
            url: url,
            type: 'POST',
            data: JSON.stringify(tipo),
            contentType: "application/json",
            success: function (tipo)
            {
                $(datePicker).removeClass('inputLoading');
                if (tipo.Error != null)
                {
                    catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, tipo.Error);
                    $(catalogo.CotizacionesElement.controles.IdIva).val(0);
                    //$(catalogo.CotizacionesElement.controles.IdTipoCambio).val(0);
                    //$(catalogo.CotizacionesElement.controles.Valor).val(0);
                    $(catalogo.CotizacionesElement.controles.Iva).val(0);
                }
                else
                {
                    $(catalogo.CotizacionesElement.controles.IdIva).val(tipo.IdIva);
                    //$(catalogo.CotizacionesElement.controles.IdTipoCambio).val(tipo.IdTipoCambio);
                    //$(catalogo.CotizacionesElement.controles.Valor).val(tipo.Valor);
                    $(catalogo.CotizacionesElement.controles.Iva).val(tipo.Porcentaje);
                    catalogo.CotizacionesElement.controles.Iva.Valor = parseFloat(tipo.Porcentaje, 10);
                    //catalogo.CotizacionesElement.controles.Valor.Valor = parseFloat(tipo.Valor, 10);
                    //catalogo.RealizaCalculos();
                }
            }
        });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.AplicaIva = function (iva)
{
    var catalogo = this;
    try
    {
       /* if (iva <= 0) return;
        var subtotal = parseFloat(catalogo.CotizacionesElement.controles.SubTotal.Valor, 10);
        var total = subtotal + ((subtotal * iva) / 100);

        $(catalogo.CotizacionesElement.controles.Total).val(total.toFixed(2));
        catalogo.CotizacionesElement.controles.Total.Valor = total.toFixed(2);*/
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
/*Cotizaciones.prototype.RealizaCalculos = function (btn)
{
    var catalogo = this;
    try
    {
        catalogo.TotalConceptos(catalogo.TablaDetalles, catalogo.CotizacionesElement.controles.dvTotalConceptos);
        catalogo.AplicaDescuento($(catalogo.CotizacionesElement.controles.DescuentoMonto).val());
        catalogo.AplicaIva($(catalogo.CotizacionesElement.controles.Iva).val());
        catalogo.SetTipoCambio(btn);
        
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}*/
Cotizaciones.prototype.AplicaDescuento = function (descuento)
{
    var catalogo = this;
    try
    {
       /* if (descuento <= 0) return;
        var subtotal = parseFloat(catalogo.CotizacionesElement.controles.SubTotal.Valor, 10);
        var total = subtotal - descuento;
        $(catalogo.CotizacionesElement.controles.SubTotal).val(total.toFixed(2));
        catalogo.CotizacionesElement.controles.SubTotal.Valor = total.toFixed(2);      */  
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
//Cotizaciones.prototype.SetPorcentajeDescuentoGlobal = function (txt) {
//    var catalogo = this;
//    try
//    {        
//        var subtotal = catalogo.CotizacionesElement.controles.SubTotal.Valor;
//        if (subtotal <= 0) return;
//        var porcentaje = parseFloat($(txt).val(), 10);
//        if (porcentaje > 100) {
//            porcentaje = 100;
//            $(catalogo.CotizacionesElement.controles.PorcentajeDescuento).val(porcentaje);
//            catalogo.CotizacionesElement.controles.PorcentajeDescuento.Valor = porcentaje;
//        }
//        var porcentaje = (porcentaje * subtotal) / 100;
//        if (porcentaje > subtotal) porcentaje = subtotal;
//        catalogo.CotizacionesElement.controles.DescuentoMonto.Valor = porcentaje.toFixed(2);
//        $(catalogo.CotizacionesElement.controles.DescuentoMonto).val(porcentaje.toFixed(2));
//       // catalogo.RealizaCalculos();

//    }
//    catch (eIni) {
//        catalogo.OnError(eIni);
//    }
//}
Cotizaciones.prototype.SetMontoDescuentoGlobal = function (txt) {
    var catalogo = this;
    try
    {
       /* var subtotal = catalogo.CotizacionesElement.controles.SubTotal.Valor;
        if (subtotal <= 0) return;
        var descuento = parseFloat($(txt).val(), 10);
        if (descuento > subtotal)
        {
            descuento = subtotal;
            $(catalogo.CotizacionesElement.controles.DescuentoMonto).val(descuento);
            catalogo.CotizacionesElement.controles.DescuentoMonto.Valor = descuento;
        }
        var porcentaje = (descuento * 100) / subtotal;
        if (porcentaje > 100) porcentaje = 100;*/
        //catalogo.CotizacionesElement.controles.PorcentajeDescuento.Valor = porcentaje.toFixed(2);
        //$(catalogo.CotizacionesElement.controles.PorcentajeDescuento).val(porcentaje.toFixed(2));
        //catalogo.RealizaCalculos();
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.ActualizaTotal = function (totalConceptos1)
{
    var catalogo = this;
    try
    {        
       /* var totalConceptos = totalConceptos1.toFixed(2);
        $(catalogo.CotizacionesElement.controles.SubTotal).val(totalConceptos);
        catalogo.CotizacionesElement.controles.SubTotal.Valor = totalConceptos;        
        $(catalogo.CotizacionesElement.controles.Total).val(totalConceptos);
        catalogo.CotizacionesElement.controles.Total.Valor = totalConceptos;*/
        //catalogo.AplicaIva(catalogo.CotizacionesElement.controles.Iva.Valor);
        //catalogo.AplicaDescuento(catalogo.CotizacionesElement.controles.DescuentoMonto.Valor);
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.SetUIControles = function ()
{
    var catalogo = this;
    try
    {      
        $(catalogo.CotizacionesElement.controles.Iva).attr('type', 'number');
        $(catalogo.CotizacionesElement.controles.Iva).removeClass('form-control');
        $(catalogo.CotizacionesElement.controles.Iva).addClass('my-form-control  SimboloPorcentaje');

        $(catalogo.CotizacionesElement.controles.Valor).attr('type', 'number');
        $(catalogo.CotizacionesElement.controles.Valor).removeClass('form-control');
        $(catalogo.CotizacionesElement.controles.Valor).addClass('my-form-control  SimboloMoneda');

        /*$(catalogo.CotizacionesElement.controles.PorcentajeDescuento).attr('type', 'number');
        $(catalogo.CotizacionesElement.controles.PorcentajeDescuento).removeClass('form-control');
        $(catalogo.CotizacionesElement.controles.PorcentajeDescuento).addClass('my-form-control  SimboloPorcentaje');*/

        $(catalogo.CotizacionesElement.controles.DescuentoMonto).attr('type', 'number');
        $(catalogo.CotizacionesElement.controles.DescuentoMonto).removeClass('form-control');
        $(catalogo.CotizacionesElement.controles.DescuentoMonto).addClass('my-form-control  SimboloMoneda');

       // $(catalogo.CotizacionesElement.controles.Total).attr('type', 'number');
        //$(catalogo.CotizacionesElement.controles.Total).removeClass('form-control');
        //$(catalogo.CotizacionesElement.controles.Total).addClass('my-form-control  SimboloMoneda');

       // $(catalogo.CotizacionesElement.controles.SubTotal).attr('type', 'number');
        //$(catalogo.CotizacionesElement.controles.SubTotal).removeClass('form-control');
       // $(catalogo.CotizacionesElement.controles.SubTotal).addClass('my-form-control  SimboloMoneda');


        $(catalogo.CotizacionesElement.controles.Exito).attr('type', 'number');
        $(catalogo.CotizacionesElement.controles.Exito).removeClass('form-control');
        $(catalogo.CotizacionesElement.controles.Exito).addClass('my-form-control  SimboloPorcentaje');

        $(catalogo.CotizacionesElement.controles.Utilidad).attr('type', 'number');
        $(catalogo.CotizacionesElement.controles.Utilidad).removeClass('form-control');
        $(catalogo.CotizacionesElement.controles.Utilidad).addClass('my-form-control  SimboloPorcentaje');

        
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}

Cotizaciones.prototype.ImprimirCotizacion = function (btn)
{
    var catalogo = this;
    try
    {        
        var obj = catalogo.GetObject();
        var none = $(btn).attr("data-msj-none");
        var wait = $(btn).attr("data-msj-wait");
        var url = $(btn).attr("data-msj-url");
        var urlDownload = $(btn).attr('data-url-rpt-dw');
        var title = $(btn).html();      
        //window.open(url + '/' + obj.Id, '_blank');
        if (obj == null) {
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
            return
        }
        // window.open(url + '/' + obj.Id, '_blank');    
        catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
        $.ajax(
      {
          url: url + '/' + obj.Id,
          type: 'GET',
          // data: JSON.stringify(oo),
          contentType: "application/json",
          success: function (response) {
              window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
              catalogo.ICAP.CloseModal();
          }
      });
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}

Cotizaciones.prototype.CrearProyecto = function (btn)
{
    var catalogo = this;
    try
    {
        var obj = catalogo.GetObject();
        var wait = $(btn).attr("data-msj-wait");
        var none = $(btn).attr("data-msj-none");
        var url = $(btn).attr("data-url-proyecto");
        var title = $(btn).html();
        if (obj == null)
        {
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
            return
        }
        catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
        $.ajax(
         {
             url: url + '/' + obj.Id,
             type: 'GET',
             contentType: 'application/json',
             success: function (html) {
                 catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                 catalogo.SetUIBoostrap();
                 $(catalogo.Notificaciones).html('');
                 catalogo.ICAP.BuildAutoCompletes();
                 var btnSave = document.getElementById('btnSaveProyectos');
                 var proyecto = new Proyectos(catalogo);
                 proyecto.LoadCatalogos();
                 $(btnSave).click(function ()
                 {                     
                     proyecto.SaveProyecto(title,wait,btnSave);
                 });
             }
         });
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Cotizaciones.prototype.Inicializa = function ()
{
    var catalogo = this;
    try
    {
        catalogo.btnAddConcepto = document.getElementById('btnAddDetalleConceptosCotizacion');
        catalogo.TablaDetalles = document.getElementById('tbdodyConceptosCotizacion');
        catalogo.tabExplosionInsumos = document.getElementById('tabExplosionInsumos');
        catalogo.tabConceptosCapitulos = document.getElementById('tabConceptosCapitulos');
        catalogo.dvNotificacionesConceptos = document.getElementById('dvNotificacionesValidacionConceptosCotizaciones');
        catalogo.dvTabExpInsumos = document.getElementById('dvTabExpInsumos');
        catalogo.dvTabConceptosCap = document.getElementById('dvTabConceptosCap');
        catalogo.tabDatosAdicionales = document.getElementById('tabDatosAdicionales');
        catalogo.ulAtencionCotizacion = document.getElementById('ulAtencionCotizacion');
        catalogo.ulContactoCotizacion = document.getElementById('ulContactoCotizacion');
        catalogo.selAtencionCotizacion = document.getElementById('selAtencionCotizacion');
        catalogo.selContactoCotizacion = document.getElementById('selContactoCotizacion');
        //catalogo.btnAplicaDescuentos = document.getElementById('btnAplicaDescuentos');
       
        catalogo.CotizacionesElement = new Object();
        catalogo.CotizacionesElement.controles = new Object();

        catalogo.CotizacionesElement.controles.IdCotizacion = document.getElementById('IdCotizacion');
        catalogo.CotizacionesElement.controles.IdCliente = document.getElementById('IdCliente');
        catalogo.CotizacionesElement.controles.Exito = document.getElementById('Exito');
        catalogo.CotizacionesElement.controles.IdMoneda = document.getElementById('IdMoneda');
        catalogo.CotizacionesElement.controles.Cotizacion = document.getElementById('Cotizacion');
        catalogo.CotizacionesElement.controles.Moneda = document.getElementById('IdMonedaCotizacionHeadAutoComplete');

        catalogo.CotizacionesElement.controles.dvTotalConceptos = document.getElementById('dvTotalConceptos');
        catalogo.CotizacionesElement.controles.Fecha = document.getElementById('Fecha');
        catalogo.CotizacionesElement.controles.IdIva = document.getElementById('IdIva');
        //catalogo.CotizacionesElement.controles.IdTipoCambio = document.getElementById('IdTipoCambio');
        //catalogo.CotizacionesElement.controles.Valor = document.getElementById('Valor');
        catalogo.CotizacionesElement.controles.Iva = document.getElementById('Iva');
        //catalogo.CotizacionesElement.controles.Total = document.getElementById('Total');
        //catalogo.CotizacionesElement.controles.SubTotal = document.getElementById('SubTotal');
        //catalogo.CotizacionesElement.controles.PorcentajeDescuento = document.getElementById('PorcentajeDescuento');
        catalogo.CotizacionesElement.controles.DescuentoMonto = document.getElementById('DescuentoMonto');
        catalogo.CotizacionesElement.controles.Utilidad = document.getElementById('Utilidad');

        catalogo.CotizacionesElement.controles.Solicitud = document.getElementById('Solicitud');
        catalogo.CotizacionesElement.controles.ProyectoInicial = document.getElementById('ProyectoInicial');
        catalogo.CotizacionesElement.controles.DescripcionTitulo = document.getElementById('DescripcionTitulo');
        catalogo.CotizacionesElement.controles.DescripcionCompleta = document.getElementById('DescripcionCompleta');
        catalogo.CotizacionesElement.controles.DepartamentoAtencion = document.getElementById('DepartamentoAtencion');
        catalogo.CotizacionesElement.controles.FechaEntrega = document.getElementById('FechaEntrega');
        catalogo.btnAtencionPlus = document.getElementById('btnAtencionCotizacionPlus');
        catalogo.btnAtencionMinus = document.getElementById('btnAtencionCotizacionMinus');
        catalogo.btnContactoPlus = document.getElementById('btnContactoCotizacionPlus');
        catalogo.btnContactoMinus = document.getElementById('btnContactoCotizacionMinus');

        if (catalogo.CotizacionesElement.controles.dvTotalConceptos != null) catalogo.CotizacionesElement.controles.dvTotalConceptos.Valor = 0;
        if (catalogo.CotizacionesElement.controles.Fecha != null) catalogo.CotizacionesElement.controles.Fecha.Valor = 0;
        if (catalogo.CotizacionesElement.controles.IdIva != null) catalogo.CotizacionesElement.controles.IdIva.Valor = 0;
        if (catalogo.CotizacionesElement.controles.Iva != null) catalogo.CotizacionesElement.controles.Iva.Valor = 0;
        if (catalogo.CotizacionesElement.controles.DescuentoMonto != null) catalogo.CotizacionesElement.controles.DescuentoMonto.Valor = 0;

        $(catalogo.btnAddConcepto).click(function ()
        {
            catalogo.AddConcepto($(this));
        });
        $(catalogo.tabExplosionInsumos).click(function ()
        {
            catalogo.ExpInsumos($(this));
        });      
        $(catalogo.tabConceptosCapitulos).click(function () {
            catalogo.GroupConceptos($(this));
        });        
        $(catalogo.CotizacionesElement.controles.Fecha).on('dp.change', function ()
        {
            catalogo.ObiteneIvaPorcentaje($(this));
        });                      
        $(catalogo.CotizacionesElement.controles.DescuentoMonto).change(function () {
            catalogo.SetMontoDescuentoGlobal($(this));
        });
        $(catalogo.tabDatosAdicionales).click(function ()
        {
            catalogo.GetPersonalCliente($(this));
        });

        $(catalogo.btnAtencionPlus).click(function () { catalogo.AddPersonal($(this)); });
        $(catalogo.btnContactoPlus).click(function () { catalogo.AddPersonal($(this)); });
        $(catalogo.btnAtencionMinus).click(function () { catalogo.DelPersonal($(this)); });
        $(catalogo.btnContactoMinus).click(function () { catalogo.DelPersonal($(this)); });
        if (catalogo.TablaDetalles != null) {
            catalogo.AddBtnsDelete(catalogo.TablaDetalles);
        }
        catalogo.SetUIControles();
        catalogo.SetUIBoostrap();
        catalogo.ICAP.BuildAutoCompletes();
    }
    catch (eIni)
    {
        //catalogo.OnError(eIni);
        alert(eIni);
    }
}
Cotizaciones.prototype.changeModal = function ()
{    
    var catalogo = this;
    catalogo.ICAP.LargeModal(true);
    catalogo.Inicializa2();
    catalogo.SetEventoListas(catalogo.ulAtencionCotizacion);
    catalogo.SetEventoListas(catalogo.ulContactoCotizacion);
}
Cotizaciones.prototype.SaveCotizacion = function (title, wait)
{
    var catalogo = this;
    var name = '#btnSave' + catalogo.NombreCatalogo;
    if (catalogo.NameSaveButton != null && catalogo.NameSaveButton != '' && catalogo.NameSaveButton != undefined)
    {
        name = '#'+catalogo.NameSaveButton
    }
    $('#btnSalirCotizacion').click(function ()
    {
        catalogo.ICAP.CloseModal();       
        catalogo.ReloadCurrentPage();        
    });
    $(name).click(function ()
    {
        var msjOK = $(this).attr('data-op-ok');
        var msjVacia = $(this).attr('data-cotizacion-vacia');
        var msjNoCliente = $(this).attr('data-cotizacion-nocliente');
        var msjTipoCambio = $(this).attr('data-cotizacion-tipocambio');
        var msjiva = $(this).attr('data-cotizacion-iva');
        var msjVaciaUbicacion = $(this).attr('data-ubicacion-empty');
        var cotizacion = catalogo.GetCotizacion(title, wait);
        if (catalogo.TablaDetalles != null) {
            $(catalogo.TablaDetalles.MovsDelete).each(function () {
                var obj = new Object();
                obj.IdCotizacionDetalle = this * -1;
                cotizacion.Conceptos.push(obj);
            });
        }
        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined) {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }        
        var form = document.getElementById(NombreFormulario);

        if (catalogo.ValidaSoloComplemento == null)
        {
            catalogo.ValidaSoloComplemento =false;
        }
            var valido = true;
            if (cotizacion.IdCliente <= 0)
            {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjNoCliente);
                valido = false;
            }
            if (cotizacion.IdIva <= 0 && !catalogo.ValidaSoloComplemento) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjiva);
                valido = false;
            }
        
            if (cotizacion.iNumConceptos <= 0 && !catalogo.ValidaSoloComplemento) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjVacia);
                valido = false;
            }
            $(cotizacion.Conceptos).each(function () {
                var obj = this;
                if (obj.Ubicacion == '') {
                    catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjVaciaUbicacion);
                    valido = false;
                }
            });
            if (valido)
            {
                var url = $(form).attr("action");
                var method = $(form).attr("method");
                if (title != null && wait != null) {
                    //catalogo.BuildWaitModal(title, wait);
                    catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                }

                $.ajax(
                  {
                      url: url,
                      type: method,
                      data: JSON.stringify(cotizacion),
                      contentType: "application/json",
                      success: function (html)
                      {
                          var hayerror = catalogo.HasError(html);
                          if (hayerror)
                          {
                              catalogo.SetInfo(html);
                          }
                          else
                          {
                              catalogo.ICAP.ReplaceBodyModal(html);
                              //catalogo.Inicializa();
                              catalogo.AfterEditar();
                              catalogo.Save(title, wait);
                              catalogo.SetInfo('<div class="alert alert-success" role="alert"> ' +
                                                                 '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                                 '<span aria-hidden="true">&times;</span></button> ' + msjOK + '</div>');
                          }
                         /* else {
                              catalogo.ICAP.CloseModal();
                              $(catalogo.Notificaciones).html(html);
                              catalogo.ReloadCurrentPage();
                          }*/
                      }
                     
                  });
            }
       
    });
}
Cotizaciones.prototype.GetCotizacion = function ()
{
    var catalogo = this;
    var cotizacion = new Object();
    cotizacion.IdCotizacion = $(catalogo.CotizacionesElement.controles.IdCotizacion).val();
    cotizacion.Cotizacion = $(catalogo.CotizacionesElement.controles.Cotizacion).val();
    cotizacion.DescuentoMonto = $(catalogo.CotizacionesElement.controles.DescuentoMonto).val();
    //cotizacion.PorcentajeDescuento = $(catalogo.CotizacionesElement.controles.PorcentajeDescuento).val();
    cotizacion.IdCliente = $(catalogo.CotizacionesElement.controles.IdCliente).val();
    cotizacion.IdIva = $(catalogo.CotizacionesElement.controles.IdIva).val();
    cotizacion.Fecha = $(catalogo.CotizacionesElement.controles.Fecha).val();
    cotizacion.Exito = $(catalogo.CotizacionesElement.controles.Exito).val();
    //cotizacion.IdTipoCambio = $(catalogo.CotizacionesElement.controles.IdTipoCambio).val();
    cotizacion.IdMoneda = $(catalogo.CotizacionesElement.controles.IdMoneda).val();
    cotizacion.Utilidad = parseFloat($(catalogo.CotizacionesElement.controles.Utilidad).val(), 10);
    cotizacion.Solicitud = $(catalogo.CotizacionesElement.controles.Solicitud).val();
    cotizacion.ProyectoInicial = $(catalogo.CotizacionesElement.controles.ProyectoInicial).val();
    cotizacion.DescripcionTitulo = $(catalogo.CotizacionesElement.controles.DescripcionTitulo).val();
    cotizacion.DescripcionCompleta = $(catalogo.CotizacionesElement.controles.DescripcionCompleta).val();
    cotizacion.DepartamentoAtencion = $(catalogo.CotizacionesElement.controles.DepartamentoAtencion).val();
    cotizacion.FechaEntrega = $(catalogo.CotizacionesElement.controles.FechaEntrega).val();
    cotizacion.PersonalClienteAtencion = new Array();
    cotizacion.PersonalClienteContacto = new Array();
    cotizacion.Conceptos = new Array();
    var iNumConceptos = 0;
    $(catalogo.TablaDetalles).find('tr.ConceptoCotizacion').each(function ()
    {
        var obj = catalogo.ICAP.GetObject($(this));
        cotizacion.Conceptos.push(obj);
        iNumConceptos++;
    });    
    $(catalogo.ulAtencionCotizacion).find('li').each(function () {
        var id = $(this).attr('data-id');
        var idClientePersonal = $(this).attr('data-idcliente');
        var personal = new Object();
        personal.IdCotizacionPersonalCliente = id;
        personal.IdPersonalCliente = idClientePersonal;
        if (parseInt(personal.IdCotizacionPersonalCliente, 10) == 0) {
            cotizacion.PersonalClienteAtencion.push(personal);
        }
    });
    $(catalogo.ulContactoCotizacion).find('li').each(function () {
        var id = $(this).attr('data-id');
        var idClientePersonal = $(this).attr('data-idcliente');
        var personal = new Object();
        personal.IdCotizacionPersonalCliente = id;
        personal.IdPersonalCliente = idClientePersonal;
        if (parseInt(personal.IdCotizacionPersonalCliente, 10) == 0) {
            cotizacion.PersonalClienteContacto.push(personal);
        }
    });
    $(catalogo.ulAtencionCotizacion.PersonalDelete).each(function () {
        var personal = new Object();
        personal.IdCotizacionPersonalCliente = this;
        cotizacion.PersonalClienteAtencion.push(personal);
    });
    $(catalogo.ulContactoCotizacion.PersonalDelete).each(function () {
        var personal = new Object();
        personal.IdCotizacionPersonalCliente = this;
        cotizacion.PersonalClienteContacto.push(personal);
    });
    cotizacion.iNumConceptos = iNumConceptos;    
    return cotizacion;
}
Cotizaciones.prototype.SetCotizacion = function (cotizacion)
{
    var catalogo = this;        
    $(catalogo.CotizacionesElement.controles.DescuentoMonto).val(cotizacion.DescuentoMonto);        
    $(catalogo.TablaDetalles).find('tr.ConceptoCotizacion').each(function ()
    {
    
    });    
}
Cotizaciones.prototype.SetTipoCambio = function (btn)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).show();
    /* var catalogo = this;
    var cotizacion = catalogo.GetCotizacion();
    var msjWait = $(btn).attr('data-msj-wait');   
    var url = $(btn).attr('data-url-conversion');
    if (msjWait != null)
    {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjWait);
    }
    $.ajax(
      {
          url: url,
          type: 'POST',
          data: JSON.stringify(cotizacion),
          contentType: "application/json",
          success: function (cotizacion)
          {                            
              if (cotizacion != null)
              {
                  catalogo.SetCotizacion(cotizacion);
              }
              $('#btnSave' + catalogo.NombreCatalogo).hide();
              $(catalogo.dvNotificacionesConceptos).html('');
          }
      });  */  
}
Cotizaciones.prototype.QueryMoneda = function ()
{
    var catalogo = this;
    //var IdTipoCambio = 0;//$(catalogo.CotizacionesElement.controles.IdTipoCambio).val();
    var idMoneda = $(catalogo.CotizacionesElement.controles.IdMoneda).val();
    var Moneda = $(catalogo.CotizacionesElement.controles.Moneda).val();
    if (idMoneda == null || parseInt(idMoneda, 10) <= 0 || isNaN(idMoneda))
    {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, 'ERROR MONEDA');
        return '';
    }
    if (Moneda == null || Moneda == '')
    {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, 'ERROR MONEDA');
        return '';
    }    
    return '?IdMoneda=' + idMoneda + '&Moneda=' + Moneda + '&IdTipoCambio=' + IdTipoCambio;    
}
Cotizaciones.prototype.DelPersonal = function (btn) {
    var catalogo = this;
    var origen = document.getElementById($(btn).attr('data-origen'));
    if (origen.PersonalDelete == null) {
        origen.PersonalDelete = new Array();
    }
    $(origen).find('li.personalActivo').each(function () {
        var id = parseInt($(this).attr('data-id'));
        if (id > 0) {
            origen.PersonalDelete.push(id * -1);
        }
        $(this).remove();
    });
}
Cotizaciones.prototype.AddPersonal = function (btn) {
    var catalogo = this;
    var origen = document.getElementById($(btn).attr('data-origen'));
    var destino = document.getElementById($(btn).attr('data-destino'));
    var dataMsjExiste = $(destino).attr('data-msj-existe');
    var op = $(origen).find(":selected");

    var idPersonal = parseInt($(op).attr('data-property-value'), 10);
    var Personal = $(op).val();
    if (idPersonal <= 0) return;
    var existe = false;
    $(destino).find("li").each(function () {
        if (parseInt($(this).attr('data-idcliente'), 10) == idPersonal) {
            existe = true;
        }
    });
    if (!existe) {
        $(destino).append('<li class="list-group-item" data-id="0" data-idcliente="' + idPersonal + '">' + Personal + '</li>');
        catalogo.SetEventoListas(destino);
    }
    else {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesConceptos, dataMsjExiste);
    }
}
Cotizaciones.prototype.SetEventoListas = function (ul) {
    $(ul).find('li').each(function () {
        $(this).unbind("click");
        $(this).click(function () {
            $(ul).find('li.personalActivo').each(function () {
                $(this).removeClass('personalActivo');
            });
            $(this).addClass('personalActivo');
        })
    });
}