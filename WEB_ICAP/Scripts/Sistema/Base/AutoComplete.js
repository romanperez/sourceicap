﻿function AutoComplete(elemento)
{
    var Auto = this;
    Auto.Inicializa(elemento);   
}
AutoComplete.prototype.Inicializa = function (elemento)
{
    var Auto = this;
    Auto.Elemento = elemento;
    Auto.GetInfo();
    Auto.CreaAutoComplete();
}
AutoComplete.prototype.GetInfo = function ()
{
    var Auto = this;
    Auto.url = $(Auto.Elemento).attr('data-auto-url');
    Auto.metod = $(Auto.Elemento).attr('data-auto-metod');
    Auto.minLength = $(Auto.Elemento).attr('data-auto-minLength');
    Auto.target = $(Auto.Elemento).attr('data-auto-target');
    Auto.propValue = $(Auto.Elemento).attr('data-auto-propValue');
    Auto.propDisplay = $(Auto.Elemento).attr('data-auto-propDisplay');
    Auto.campoSearch = $(Auto.Elemento).attr('data-auto-campoSearch');
    Auto.campoCondicion = $(Auto.Elemento).attr('data-auto-campoCondicion');
    Auto.omiteconcatenartabla = $(Auto.Elemento).attr('data-auto-omiteconcatenartabla');
    Auto.complemento = $(Auto.Elemento).attr("data-auto-complemento");
    Auto.multiple = $(Auto.Elemento).attr('data-auto-multiple');
    Auto.fnOnSelect = $(Auto.Elemento).attr('data-fn-onselect');
    Auto.fnBeforeSelect = $(Auto.Elemento).attr('data-fn-beforeSelect');    
}
AutoComplete.prototype.BuildBodyRequest = function (autoReference,valueSearch)
{
    var fnAuxProps = function (obj)
    {
        if (obj == null || obj == '')
        {
            obj = $(autoReference.Elemento).attr("data-auto-complemento");
        }
        if (obj != null && obj != '')
        {
            var elemento = document.getElementById(obj);
            var where = new Object();
            var pToEval = $(elemento).attr('data-auto2-property');
            where.NombreCampo = $(elemento).attr('data-auto2-nombrecampo');
            if (pToEval != null && pToEval != '')
            {
                where.Valor = $(elemento).attr(pToEval);
            }
            else
            {
                where.Valor = $(elemento).val();
            }
            
            where.Condicion = $(elemento).attr('data-auto2-condicion');
            var omite = $(elemento).attr('data-auto2-omiteconcatenar');
            if (omite == "1")
            { where.OmiteConcatenarTabla = true; }
            else
            { where.OmiteConcatenarTabla = false; }
            where.Operador = $(elemento).attr('data-auto2-operador');
            return where;
        }
        return null;
    }
    var Auto = autoReference;
    var searchArray = new Array();
    var search = new Object();
    search.NombreCampo = Auto.campoSearch;
    search.Valor = $.trim(valueSearch);
    search.Condicion = Auto.campoCondicion;
    if (Auto.omiteconcatenartabla == "1") {
        search.OmiteConcatenarTabla = true;
    }
    else {
        search.OmiteConcatenarTabla =false;
    }
    var otherOption = fnAuxProps(Auto.complemento);
    searchArray.push(search);
    if (otherOption != null)
    {
        searchArray.push(otherOption);
    }
    return JSON.stringify(searchArray);
}
AutoComplete.prototype.BuildMultiple = function (elemento,ui)
{
    var Auto = this;
    var prop = '';
    
    if (Auto.multiple == "1") 
    {
        var props = Auto.propDisplay.split(',');
        for (var c = 0; c < props.length; c++) {
            if (elemento != null) {
                prop = prop + ui.content[elemento][props[c]] + " ";
            }
            else {
                prop =prop+ ui.item[props[c]] + " ";
            }
        }
        prop = $.trim(prop);
    }
    else
    {
        if (elemento != null) {
            prop = ui.content[elemento][Auto.propDisplay];
        }
        else {
            prop =ui.item[Auto.propDisplay];
        }
    }    
    return prop;
}
AutoComplete.prototype.CreaAutoComplete = function ()
{
    var Auto = this;
    Auto.ValueOriginal = $('#' + Auto.target).val();   
    var SetValueUI = function (ui)
    {                
        if (ui == null || ui.content == null || ui.content.length <= 0) return;
        for (var elemento = 0; elemento < ui.content.length; elemento++)
        {
            ui.content[elemento].label = Auto.BuildMultiple(elemento,ui); //ui.content[elemento][Auto.propDisplay];
            ui.content[elemento].value = ui.content[elemento][Auto.propValue];
        }
    };
    var SetValueTarget = function (seleccion)
    {
        var elemento = document.getElementById(Auto.target);

        if (Auto.fnBeforeSelect != null && Auto.fnBeforeSelect != undefined && Auto.fnBeforeSelect != '')
        {
            Auto[Auto.fnBeforeSelect](Auto.ValueOriginal,seleccion);
        }
        $('#' + Auto.target).val(seleccion[Auto.propValue]);
        elemento.ResultSelectionAutoComplete = seleccion;
        if (Auto.fnOnSelect != null && Auto.fnOnSelect != undefined && Auto.fnOnSelect != '')
        {
            Auto[Auto.fnOnSelect](seleccion);
        }       
    }
    $(Auto.Elemento).autocomplete(
    {
        source: function (request, response)
        {           
            var Value = $(Auto.Elemento).val();
            var parametros = Auto.BuildBodyRequest(Auto, Value);
            $(Auto.Elemento).removeClass('autocompleteIcap');           
            $.ajax({
                type: Auto.metod,
                    url: Auto.url,
                    data: parametros,
                    success: response,
                    error: function (xhr, ajaxOptions, thrownError) 
                    {                        
                        $(Auto.Elemento).removeClass('ui-autocomplete-loading');
                        $(Auto.Elemento).addClass('autocompleteIcap');
                    },
                    contentType: 'application/json'});
        },       
        response: function (event, ui)
        {
            $('#' + Auto.target).val('');
            $(Auto.Elemento).removeClass('ui-autocomplete-loading');
            $(Auto.Elemento).addClass('autocompleteIcap');
            SetValueUI(ui);
        },
        minLength: Auto.minLength,
        select: function (event, ui)
        {            
            SetValueTarget(ui.item);
            $(Auto.Elemento).val(Auto.BuildMultiple(null,ui));//ui.item[Auto.propDisplay]);
            return false;
        },
        focus: function (event, ui)
        {
            event.preventDefault();
            $(Auto.Elemento).val(ui.item.label);
        }

    });    
}