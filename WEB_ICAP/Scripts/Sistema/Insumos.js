﻿ClassIndexIcapMain.prototype.MenusInsumos = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var tab = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    tab.MenuInsumos = new MenuInsumos();   
   // tab.AfterEditar = tab.MenuInsumos.EsServicio;
    tab.AfterNuevo = tab.MenuInsumos.NoEsServicio
    tab.EsServicio = tab.MenuInsumos.EsServicio;
    tab.Inicializa2 = tab.MenuInsumos.Inicializa2;
    tab.LoadInsumos = tab.MenuInsumos.LoadInsumos;
    tab.Inicializa(tab.Inicializa2);       
}
function MenuInsumos()
{
}
MenuInsumos.prototype.Inicializa2 = function (main)
{
    var catalogo =main;
    catalogo.rptCargaMasivaInsumo = document.getElementById('rptCargaMasivaInsumo');
    $(catalogo.rptCargaMasivaInsumo).click(function ()
    {
        catalogo.LoadInsumos($(this));
    });
}
MenuInsumos.prototype.LoadInsumos = function (btn)
{    
    var catalogo = this;
    try
    {
        var url = $(btn).attr('data-url-plantilla');
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        var fnLoadExcel = function (frm, btn2) {
            var url = $(btn).attr('data-url-uploadfile');
            var formData = new FormData(frm);
            if (title != null && wait != null) {
                catalogo.ReplaceBodyModal('<div class="dvCatalogosLoading">' + wait + '</div>');
            }
            $.ajax(
              {
                  url: url,
                  type: 'post',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (html) {
                      var hayerror = catalogo.HasError(html);
                      catalogo.ReplaceBodyModal(html);
                      if (!hayerror) {
                          catalogo.OnOffCheck();
                          catalogo.SetUIBoostrap();
                          catalogo.ICAP.BuildAutoCompletes();
                          catalogo.AfterNuevo();
                          catalogo.Save(title, wait);
                      }
                  }
              });
        };
        var fnAfterLoad = function (objMain, frm) {
            var input = document.getElementById('fileInsumosXls');
            $(input).change(function () {
                var lbl = document.getElementById('lblInsumosXlsSelect');
                $(lbl).html(($(this).val().split('\\').pop()));
            });
        };
        catalogo.PlantillaLoadFile(btn,
                                   url,
                                   'get',
                                   function (f, b) {
                                       fnLoadExcel(f, b);
                                   },
                                   function () {
                                       catalogo.ICAP.CloseModal();
                                   },
                                   function (c, frm) {
                                       fnAfterLoad(c, frm);
                                   }
                                   );
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
MenuInsumos.prototype.NoEsServicio = function ()
{
    var catalogo = this;
    try
    {
        catalogo.dvUnidadMedida = document.getElementById('dvSectionUnidadMedidaInsumo');
        catalogo.dvCosto = document.getElementById('dvSectionCostoInsumo');
        $(catalogo.dvCosto).hide();
        catalogo.EsServicio();
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
MenuInsumos.prototype.EsServicio = function ()
{
    var catalogo = this;
    try
    {
        var service = document.getElementById('EsServicio');
        var IdUnidad = document.getElementById('IdUnidad');
        var IdUnidadAutoComplete = document.getElementById('IdUnidadAutoComplete');
        var costo = document.getElementById('Costo');
        $(service).change(function ()
        {
            $(IdUnidad).val('');
            $(IdUnidadAutoComplete).val('');
            $(costo).val('');
            if ($(this).prop('checked'))
            {
                $(catalogo.dvUnidadMedida).hide();
                $(catalogo.dvCosto).show();
               
            }
            else
            {
                $(catalogo.dvCosto).hide();
                $(catalogo.dvUnidadMedida).show();                
            }
        });
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}