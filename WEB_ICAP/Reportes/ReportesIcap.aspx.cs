﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Controladores;
using System.IO;
using Negocio;
using Reportes;
namespace WEB_ICAP.Reportes
{
	public partial class ReportesIcap : System.Web.UI.Page
	{		
		protected void Page_Load(object sender, EventArgs e)
		{			
			byte[] file = null;
			EHandleHttpRequest hndlRequest = new EHandleHttpRequest();
			string nombreReporte = hndlRequest.GetQryString(Request, "Rpt");
			string parametros=hndlRequest.GetQryString(Request, "Id");
			string fileName = hndlRequest.GetQryString(Request, "NombreFile");		
			if(!IsPostBack)
			{
				switch(nombreReporte.ToUpper())
				{
					case "COTIZACION_PDF":
						file = RptCotizacion(Convert.ToInt32(parametros));						
						break;
					case "OT_ACTIVIDAD_PDF":
						file = RptImprimeRptOTActividad(Convert.ToInt32(parametros));						
						break;
					case "OT_GLOBAL_PDF":
						file = RptImprimeRptOTGlobal(Convert.ToInt32(parametros));						
						break;
					default:
						default1();
						break;
				}
				if(file != null && file.Length>0 )
				{
					Response.ContentType = "application/octet-stream";
					Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
					Response.OutputStream.Write(file, 0, file.Length);
					Response.Flush(); 
				}				
			}
		}
		protected byte[] RptCotizacion(int id)
		{			
			RptImprimeCotizacion cotizacion = new RptImprimeCotizacion();
			return cotizacion.GeneraReporte(id, ReportViewer1);
		}		
		protected byte[] RptImprimeRptOTActividad(int id)
		{			
			RptOTByActividad reporte = new RptOTByActividad();
			return reporte.GeneraReporte(id, ReportViewer1);
		}
		protected byte[] RptImprimeRptOTGlobal(int id)
		{			
			RptGlobalOT reporte = new RptGlobalOT();
			reporte.IdOrdenTrabajo = id;
			return reporte.GeneraReporte(this.ReportViewer1);			
		}		
		protected void default1()
		{
			List<IInsumos> Insumos = new List<IInsumos>();
			Insumos.Add(new CInsumos()
			{
				Codigo = "001",
				Descripcion = "D001",
				Costo = 10
			});
			Insumos.Add(new CInsumos()
			{
				Codigo = "002",
				Descripcion = "D002",
				Costo = 10223
			});
			Insumos.Add(new CInsumos()
			{
				Codigo = "002",
				Descripcion = "D002",
				Costo = 104
			});
			Insumos.Add(new CInsumos()
			{
				Codigo = "003",
				Descripcion = "D003",
				Costo = 100
			});

			var reportDataSource = new ReportDataSource
			{
				// Must match the DataSource in the RDLC
				Name = "Insumos",
				Value = Insumos
			};


			ReportViewer1.LocalReport.ReportPath = @"bin\RDLC\Insumos.rdlc";
			//System.IO.Path.Combine(Server.MapPath("~"),@"bin\RDLC\Insumos.rdlc");
			ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
			ReportViewer1.DataBind();
		}
	}
}