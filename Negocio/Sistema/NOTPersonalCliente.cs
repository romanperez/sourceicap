using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{  
    
    public partial class NOTPersonalCliente : NBase
    {
	public enum eFields {IdOTPersonalCliente=0,IdOrdenTrabajo,IdEstatus,IdPersonalCliente,Estatus}
        public NOTPersonalCliente(IEntidad entidad)
			: base(entidad)
	{
	}
	protected override IBase BuildDataEntity()
	{
	  DOTPersonalCliente otpersonal= new  DOTPersonalCliente();
	  DClientesPersonal personal = new DClientesPersonal();
	  DEstatus estatus = new DEstatus();	 
	  string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1} as NombreEstatus",estatus.Tabla,NEstatus.eFields.Nombre.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.Nombre.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.ApellidoPaterno.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.ApellidoMaterno.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.Puesto.ToString())
			  };
	  string[] condicionesJoin = new string[] 
			  { 
				   /*Join with ClientesPersonal*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,personal.Tabla,DBase.On, 
				   String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.IdPersonalCliente.ToString()),
					String.Format("{0}.{1}",otpersonal.Tabla,NOTPersonalCliente.eFields.IdPersonalCliente.ToString()))	,
					/*Join with Estatus*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,estatus.Tabla,DBase.On, 
				   String.Format("{0}.{1}",estatus.Tabla,NEstatus.eFields.IdEstatus.ToString()),
					String.Format("{0}.{1}",otpersonal.Tabla,NOTPersonalCliente.eFields.IdEstatus.ToString()))
			  };
	  otpersonal.SetConfigJoins(camposJoin, condicionesJoin);
	  return otpersonal;
	}		
	public NOTPersonalCliente() { }			
    }// NOTPersonalCliente ends.        
}// LibNegocio.Entidades ends.
