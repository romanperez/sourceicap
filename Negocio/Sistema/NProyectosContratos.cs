using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/03/2017 03:46:55 p. m.*/
namespace Negocio
{

	public partial class NProyectosContratos : NBase
	{
		public enum eFields
		{
			IdProyectoContrato=0,	IdProyecto	,IdContrato,	Estatus
		}
		public NProyectosContratos()
		{
		}
		public NProyectosContratos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DProyectoContratos();
		}
		public bool ObtieneInsumosSinPedido(int IdProyecto)
		{
			try
			{
				if(IdProyecto <= 0) return false;
				this.ClearWhere();
				this.AddWhere(new CPropsWhere()
				{
					Condicion = "=",
					NombreCampo = NProyectosContratos.eFields.IdProyecto.ToString(),
					Valor = IdProyecto,
					Prioridad=true
				});
				IEnumerable<IEntidad> res = this.Buscar();
				if(res != null && res.Count() > 0)
				{
					int[]Aids = (from tbl in res
							  select ((IProyectoContratos)tbl).IdContrato).ToArray();
					string ids = String.Join(",", Aids);
					NContratosDetallesInsumos2 insumos = new NContratosDetallesInsumos2();
					insumos.AddWhere(new CPropsWhere()
					{
						Condicion = "in",
						NombreCampo = NContratosDetallesInsumos2.eFields.IdContrato.ToString(),
						Valor =ids,
						Prioridad=true
					});
					IEnumerable<IEntidad> resInsumos = insumos.Buscar();
					//var enviaMail = resInsumos.Where(x => ((IContratosDetallesInsumos2)x).Pedido == null || ((IContratosDetallesInsumos2)x).Pedido == false);
					if(resInsumos != null && resInsumos.Count() > 0)
						return true;
				}				
			}
			catch(Exception er)
			{
				OnError(er);
			}
			return false;
		}
	}// NProyectosContratos ends.        
}// LibNegocio.Entidades ends.
