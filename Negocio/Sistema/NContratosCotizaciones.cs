using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 19/06/2017 05:00:00 p. m.*/
namespace Negocio
{

	public partial class NContratosCotizaciones : NBase
	{
		public enum eFields
		{
			IdContratoCotizacion = 0, IdContrato, IdCotizacion, Fecha
		}
		public NContratosCotizaciones()
		{
		}
		public NContratosCotizaciones(IEntidad entidad)
			: base(entidad)
		{			
		}
		protected override IBase BuildDataEntity()
		{			
			DContratosCotizaciones CC= new DContratosCotizaciones();
			DCotizaciones cotizaciones=new DCotizaciones();
			DContratos2 contratos = new DContratos2();

			string[] camposJoin = new string[]
			  {																				
				  String.Format("{0}.{1}",cotizaciones.Tabla,NCotizaciones.eFields.Cotizacion.ToString()),
				  String.Format("{0}.{1}",contratos.Tabla,NContratos.eFields.Contrato.ToString())				  
			  };
			string[] condicionesJoin = new string[] 
			  { 	
			 /*Join with Cotizaciones */
			String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cotizaciones.Tabla,DBase.On, 
			String.Format("{0}.{1}",cotizaciones.Tabla,NCotizaciones.eFields.IdCotizacion.ToString()),
			String.Format("{0}.{1}",CC.Tabla,NContratosCotizaciones.eFields.IdCotizacion.ToString())),
			/*Join with Contratos */
			String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,contratos.Tabla,DBase.On, 
			String.Format("{0}.{1}",contratos.Tabla,NContratos.eFields.IdContrato.ToString()),
			String.Format("{0}.{1}",CC.Tabla,NContratosCotizaciones.eFields.IdContrato.ToString()))
			  };
			CC.SetConfigJoins(camposJoin, condicionesJoin);
			return CC;
		}
	}// NContratosCotizaciones ends.        
}// LibNegocio.Entidades ends.
