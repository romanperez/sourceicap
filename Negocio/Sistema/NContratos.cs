using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{	
	///<summary>
	///Catalogo Contratos
	///</summary>
	public partial class NContratos : NBase
	{
		public enum eFields
		{
			IdContrato = 0, IdPedido, SubTotal, Total, PorcentajeDescuento, DescuentoMonto, IdEmpresa, IdCliente, IdUnidad, IdIva, IdTipoCambio, IdMoneda, Fecha, Contrato,
			IdUsuario,	IdUsuarioCostos
		}
		public NContratos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected virtual IBase BuildConfig(string table)
		{
			DContratos2 contrato = new DContratos2();			
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			DMonedas moneda = new DMonedas();
			DTipoCambios cambio = new DTipoCambios();
			DIvaValores iva = new DIvaValores();
			DClientes cliente = new DClientes();
			DProyectos proyecto =new DProyectos();
			DProyectoContratos proyectoContratos = new DProyectoContratos();
			DCotizaciones cotizacion = new DCotizaciones();
			DUsuarios usuario = new DUsuarios();
			DEmpleados empleado = new DEmpleados();
			contrato.Tabla = table;
			string[] camposJoin = new string[]
			  {																				
				  String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				  String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				  String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.RazonSocial.ToString()),
				  String.Format("{0}.{1} as Iva",iva.Tabla,NIvaValores.eFields.Porcentaje.ToString()),
				  String.Format("isnull({0}.{1},0) as {2}",cambio.Tabla,NTipoCambios.eFields.Valor.ToString(),NTipoCambios.eFields.Valor.ToString()),			
				  String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				  String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString()),
				  String.Format("{0}.{1} ",proyecto.Tabla,NProyectos.eFields.IdProyecto.ToString()),
				  String.Format("{0}.{1} ",proyecto.Tabla,NProyectos.eFields.Proyecto.ToString()),
				  String.Format("{0}.{1} ",cotizacion.Tabla,NCotizaciones.eFields.Cotizacion.ToString()),				  
				  String.Format("{0} AS NombreEmpleado",new NEmpleados().DataNombreEmpleado())
			  };
			string[] condicionesJoin = new string[] 
			  { 				   				 
				    /*Join with Monedas */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdMoneda.ToString())),
					/*Join with TipoCambio */
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,cambio.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cambio.Tabla,NTipoCambios.eFields.IdTipoCambio.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdTipoCambio.ToString())),
					/*Join with IVA */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,iva.Tabla,DBase.On, 
				   String.Format("{0}.{1}",iva.Tabla,NIvaValores.eFields.IdIva.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdIva.ToString())),
					/*Join with Clientes */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cliente.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdCliente.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdCliente.ToString())),
				  	/*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdEmpresa.ToString())),			 
					 /*Join with Unidades*/
					String.Format("{0} {1} {2} {3} = {4} {5}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format(" {0} {1}.{2} = {3}.{4}",DBase.AND,contrato.Tabla,NContratos.eFields.IdUnidad.ToString(),
					                                                 unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),
					/*Join with ProyectosContratos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,proyectoContratos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",proyectoContratos.Tabla,NProyectosContratos.eFields.IdContrato.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdContrato.ToString())),
					/*Join with Proyectos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,proyecto.Tabla,DBase.On, 
				   String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdProyecto.ToString()),
					String.Format("{0}.{1}",proyectoContratos.Tabla,NProyectosContratos.eFields.IdProyecto.ToString())),
					/*Join with Cotizaciones*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,cotizacion.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdCotizacion.ToString()),
					String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdCotizacion.ToString())),
					/*Join with Usuarios*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,usuario.Tabla,DBase.On, 
				   String.Format("{0}.{1}",usuario.Tabla,NUsuarios.eFields.IdUsuario.ToString()),
					String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdUsuario.ToString())),
					/*Join with Empleados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empleado.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdEmpleado.ToString()),
					String.Format("{0}.{1}",usuario.Tabla,NUsuarios.eFields.IdEmpleado.ToString()))
			  };
			contrato.SetConfigJoins(camposJoin, condicionesJoin);
			return contrato;
		}
		protected override IBase BuildDataEntity()
		{			
			DContratos2 contrato = new DContratos2();
			contrato = BuildConfig(DvwContratos2.VIEW_CONTRATOS2) as DContratos2;
			return contrato;
		}
		public NContratos()
		{
		}		
		public ICotizaciones CotizacionByProyecto(int idProyecto)
		{
			ICotizaciones cotizacion = null;
			try
			{
				int IdCotizacion = 0;
				IProyectos proyecto = null;								
				NProyectos buscaProyecto = new NProyectos();
				NCotizaciones buscaCotizacion = new NCotizaciones();
				proyecto = buscaProyecto.GetProyectoById(idProyecto);
				int.TryParse(proyecto.IdCotizacion.ToString(), out IdCotizacion);
				cotizacion = buscaCotizacion.GetCotizacionById(IdCotizacion);
				buscaCotizacion.LoadConceptos(cotizacion);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return cotizacion;
		}		
		protected int? GetIdTipoCambio(IContratos2 contrato)
		{
			if(contrato == null || contrato.IdEmpresa <= 0) return null;
			ITipoCambios cambio = new CTipoCambios();
			IMonedas peso = NConversiones.GetMonedaPesos(contrato.IdEmpresa);
			if(peso == null || peso.IdMoneda <= 0) return null;
			cambio.FechaInicio = contrato.Fecha;
			cambio.IdMonedaOrigen = contrato.IdMoneda;
			cambio.IdMonedaDestino = peso.IdMoneda;
			NTipoCambios ntipocambio = new NTipoCambios();
			cambio = ntipocambio.GetIdValorCambio(cambio) as CTipoCambios;
			if(cambio != null && cambio.IdTipoCambio > 0) return cambio.IdTipoCambio;
			return null;
		}		
		protected bool HasCambioCliente(IContratos2 contrato)
		{
			IContratos2 old = NContratos.GetById(contrato.IdContrato);
			if(old.IdCliente != contrato.IdCliente)
			{
				IEnumerable<IProyectoContratos> res= NProyectoContratos.GetByContrato(contrato.IdContrato);
				if(res != null && res.Count() > 0)
					return true;
					//falta validar que sim ya tiene un ContratosSinProyecto asignado NonSerializedAttribute permita CDataTipoCambio cliente
			}
			return false;
		}
		public bool ValidaContrato()
		{
			if(_data.Delete == 1) return true;
			if(_Entity != null)
			{
				IContratos2 contrato = _Entity as IContratos2;
				if(String.IsNullOrEmpty(contrato.MotivoModificacion))
				{
					contrato.IdUsuarioCostos = null;
				}
				/*bool PermiteCambios = ValidaAutorizado(contrato);
				if(!PermiteCambios)
				{
					return false;
				}*/
				contrato.IdTipoCambio = GetIdTipoCambio(contrato);
				if(contrato.IdTipoCambio == null || contrato.IdTipoCambio <= 0)
				{
					SetError(TraduceTexto("NContratoTipoCambioNovalido"));
					return false;
				}
				var noValid = contrato.Conceptos.Where(c => c.Cantidad <= 0 && c.IdContratosDetalles>=0);
				if(contrato.Conceptos == null || contrato.Conceptos.Count <= 0)
				{
					SetError(TraduceTexto("NContratoSinConceptos"));
					return false;
				}
				var ubicacionVacia = contrato.Conceptos.Where(x => String.IsNullOrEmpty(x.Ubicacion) && x.IdContratosDetalles >= 0);
				if(ubicacionVacia != null && ubicacionVacia.Count() > 0)
				{
					SetError(TraduceTexto("NConceptosDetalleNoUbicacion"));
					return false;
				}
				ubicacionVacia = contrato.Adicionales.Where(x => String.IsNullOrEmpty(x.Ubicacion) && x.IdContratosDetalles >= 0);
				if(ubicacionVacia != null && ubicacionVacia.Count() > 0)
				{
					SetError(TraduceTexto("NConceptosDetalleNoUbicacion"));
					return false;
				}

				if(noValid.Any())
				{
					SetError(TraduceTexto("NContratoConceptosZero"));
					return false;
				}
				if(contrato.IdCliente <= 0)
				{
					SetError(TraduceTexto("NContratoClienteNovalido"));
					return false;
				}				
				if(contrato.IdIva <= 0)
				{
					SetError(TraduceTexto("NContratoIvaNovalido"));
					return false;
				}
				if(contrato.CostosManuales != null && contrato.CostosManuales.Count > 0)
				{
					if(String.IsNullOrEmpty(contrato.MotivoModificacion))
					{
						SetError(TraduceTexto("NContratoElMotivoModificacionEmpty"));
						return false;
					}
				}
				if(contrato.IdContrato > 0)
				{
					bool CambioCliente = HasCambioCliente(contrato);
					if(CambioCliente)
					{
						SetError(TraduceTexto("NContratoCambioClienteNoValido"));
						return false;
					}
				}
				return true;
			}
			else
			{
				SetError(TraduceTexto("NegocioNContextEntidadVacia"));
				return false;
			}
			
		}
		//public bool ValidaAutorizado(IContratos2 contrato)
		//{
		//	bool EsValidoParaCambios = true;
		//	if(contrato.IdContrato <= 0)
		//	{
		//		EsValidoParaCambios= true;
		//	}
		//	else
		//	{
		//		CPropsWhere where = new CPropsWhere()
		//		{
		//			Prioridad = true,
		//			Condicion = "=",
		//			NombreCampo = NContratos.eFields.IdContrato.ToString(),
		//			Valor = contrato.IdContrato
		//		};
		//		this.ClearWhere();
		//		this.AddWhere(where);
		//		IEnumerable<IEntidad> res = this.Buscar();
		//		if(res != null && res.Count() > 0)
		//		{
		//			IContratos2 original = res.FirstOrDefault() as IContratos2;
		//			if(original.Aprobado)
		//			{
		//				SetError(TraduceTexto("NContratoAutorizado"));
		//				EsValidoParaCambios= false;
		//			}
		//		}
		//		else
		//		{
		//			EsValidoParaCambios= true;
		//		}
		//	}
		//	return EsValidoParaCambios;
		//}
		//public override bool ValidaDelete()
		//{
		//	bool PermiteCambios = ValidaAutorizado(_Entity as IContratos2);
		//	if(!PermiteCambios)
		//	{
		//		return false;
		//	}
		//	return true;
		//}
		public void ActualizaCostosInsumos()
		{
			IContratos2 contrato =null;			
		   contrato= GetById(( _Entity as IContratos2).IdContrato);
			contrato.CostosManuales = (_Entity as IContratos2).CostosManuales;
			(_data as DContratos2).ActualizaCostosManuales(contrato);
		}

		protected virtual bool ValidaDuplicado(bool isNew)
		{
			
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IContratos2", new DvwContratos2());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}

		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			if(ValidaContrato())
			{
				if(_data == null) return;
				_data.Save(_Entity);
				ActualizaCostosInsumos();
			}
		}
		public override void Update()
		{

			if(ValidaDuplicado(false)) return;	
			if(ValidaContrato())
			{
				if(_data == null) return;
				_data.Save(_Entity);
				ActualizaCostosInsumos();
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}		
		public override IEnumerable<IEntidad> Buscar()
		{
			IEnumerable<IEntidad> res = null;

			if(_data != null)
			{
				IEnumerable<IPropsWhere> where = _data.Where();
				_data = BuildConfig(DvwContratos2.VIEW_CONTRATOS2);
				_data.Tabla = DvwContratos2.VIEW_CONTRATOS2;
				_data.ClearWhere();
				if(where != null && where.Count() > 0)
				{
					_data.AddWhere(where.ToList());
				}				
			}
			res = _data.Select(_Entity);

			if(res != null && res.Count() > 0)
			{
				int[] Ids = res.Select(x => ((IContratos2)(x)).IdContrato).ToArray();
				string ids = String.Join(",", Ids);
				IEnumerable<IEntidad> conceptos = ConceptosByContrato(ids);
				IEnumerable<IEntidad> insumos = InsumosByContrato(ids);
				if(conceptos != null && conceptos.Count() > 0)
				{					
					res.ToList().ForEach(x =>
					{
						IMonedas peso = NConversiones.GetMonedaPesos(((IContratos2)(x)).IdEmpresa);
						IEnumerable<IEntidad> tmpConceptos = conceptos.Where(co => ((IContratosDetalles2)co).IdContrato == ((IContratos2)(x)).IdContrato);
						if(tmpConceptos != null && tmpConceptos.Count() > 0)
						{
							tmpConceptos.ToList().ForEach(i =>
							{
								IEnumerable<IEntidad> lstInsu = insumos.Where(co2 => ((IContratosDetallesInsumos2)co2).IdConcepto == ((IContratosDetalles2)i).IdConcepto &&
																									  ((IContratosDetallesInsumos2)co2).IdContratosDetalles == ((IContratosDetalles2)i).IdContratosDetalles);
								if(lstInsu != null && lstInsu.Count()>0)
								{
									((IContratosDetalles2)i).Insumos = lstInsu.Select(final => final as CContratosDetallesInsumos2).ToList();
								}
							});
							IEnumerable<CContratosDetalles2> ListConceptos = tmpConceptos.Select(final2 => final2 as CContratosDetalles2).Where(co => co.EsAdicional == false);
							IEnumerable<CContratosDetalles2> ListAdicionales = tmpConceptos.Select(final2 => final2 as CContratosDetalles2).Where(co => co.EsAdicional ==true);
							if(ListConceptos!= null && ListConceptos.Count()>0)
							{
								((IContratos2)(x)).Conceptos = ListConceptos.ToList();
								((IContratos2)(x)).Conceptos.ForEach(mo =>
								{
									mo.Moneda = peso.Moneda;
									mo.CodigoMoneda = peso.Codigo;
								});
							}
							if(ListAdicionales != null && ListAdicionales.Count() > 0)
							{
								((IContratos2)(x)).Adicionales = ListAdicionales.ToList();
								((IContratos2)(x)).Adicionales.ForEach(mo =>
								{
									mo.Moneda = peso.Moneda;
									mo.CodigoMoneda = peso.Codigo;
								});
							}
						}
					});
				}
			}
			return res;			
		}
		public IEnumerable<IEntidad> ConceptosByContrato(string IdsContratos)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();		
			where.Add(new CPropsWhere()
			{				
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NContratosDetalles2.eFields.IdContrato.ToString(),
				Valor = IdsContratos
			});
			NContratosDetalles2 conceptos = new NContratosDetalles2();
			conceptos.AddWhere(where);
			return conceptos.Buscar();
		}
		public IEnumerable<IEntidad> InsumosByContrato(string IdsContratos)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NContratosDetallesInsumos2.eFields.IdContrato.ToString(),
				Valor = IdsContratos
			});
			NContratosDetallesInsumos2 insumos = new NContratosDetallesInsumos2();
			insumos.AddWhere(where);
			return insumos.Buscar();
		}
		public IContratos2 GeneraFromProyecto(IContratos2 contrato)
		{
			DContratos2 dcontrato = new DContratos2();
			NProyectos pc = new NProyectos();
			IProyectos proyecto = pc.GetProyectoById(contrato.Proyecto.IdProyecto);			
			if(proyecto!=null && proyecto.Contratos != null && proyecto.Contratos.Count>0)
			{
				SetError(TraduceTexto("NProyectoYaGeneroContrato"));
				return contrato;
			}
			if(proyecto != null && (proyecto.IdCotizacion == null || proyecto.IdCotizacion<=0))
			{
				SetError(TraduceTexto("NContratosSinCotizcionBase"));
				return contrato;
			}
			return dcontrato.GeneraFromProyecto(contrato);			
		}
				
		/// <summary>
		/// Realiza la busqueda de los contratos pero en la vista vwContratos2
		/// </summary>
		/// <returns></returns>
		public override IPagina<Entity> Buscar<Entity>(string campoKey, int pagina, int registrosPP)
		{
			//DContratos2 all = new DContratos2();
			if(_data != null)
			{
				IEnumerable<IPropsWhere> where = _data.Where();
				_data = BuildConfig(DvwContratos2.VIEW_CONTRATOS2);
				_data.Tabla = DvwContratos2.VIEW_CONTRATOS2;
				_data.ClearWhere();
				if(where != null && where.Count() > 0)
				{
					_data.AddWhere(where.ToList());
				}
				//all.AddWhere(_data.Where());
			}
			//return all.GetIndexContratos<Entity>(pagina);
			return _data.Select<Entity>(_Entity, String.Format("{0}.{1}", DvwContratos2.VIEW_CONTRATOS2, campoKey), pagina, registrosPP);			
		}
		public IContratos2 GetDetalleContrato(int IdContrato)
		{
			DContratos2 all = new DContratos2();
			int idUserCostos = 0;
			if(_data != null)
			{
				all.AddWhere(_data.Where());
				all.AddWhere(new CPropsWhere()
				{
					NombreCampo = "Contratos2.IdContrato",
					Valor = IdContrato
				});
			}
			IContratos2 Contrato = NContratos.GetById(IdContrato);
			Contrato = all.GetDetalleContrato(Contrato);
			int.TryParse(Contrato.IdUsuarioCostos.ToString(), out idUserCostos);
			Contrato.UsuarioCostos = NUsuarios.GetById(idUserCostos)as CUsuarios;
			return Contrato;
		}
        public static int GetNumeroEstimacion(IEnumerable<IEstimaciones> Estimaciones)
        {
            if (Estimaciones == null || Estimaciones.Count() <= 0) return 1;
            return Estimaciones.Max(x => x.NumeroEstimacion) + 1;
        }
        public IContratos2 NewEstimacion(int IdContrato,IEstimaciones estimacion)
        {
            IContratos2 contrato = EstimacionesPorContrato(estimacion.IdContratosDetalles,estimacion.FechaInicio,estimacion.FechaFin);
            List<CEstimaciones> Estimaciones = new List<CEstimaciones>();
            contrato.Conceptos.ForEach(x =>
            {
                x.Estimaciones.ForEach(e =>
                {
                    Estimaciones.Add(e);
                });
            });
            Estimaciones = Estimaciones.OrderBy(x => x.NumeroEstimacion).ToList();
            estimacion.NumeroEstimacion = NContratos.GetNumeroEstimacion(Estimaciones);
            contrato.Conceptos.ForEach(x =>
            {
                IEstimaciones newEstimacion = new CEstimaciones();
                newEstimacion.IdEstimacion = 0;
                newEstimacion.IdContratosDetalles = x.IdContratosDetalles;
                newEstimacion.NumeroEstimacion = estimacion.NumeroEstimacion;
                newEstimacion.FechaInicio = estimacion.FechaInicio;
                newEstimacion.FechaFin = estimacion.FechaFin;
                newEstimacion.CantidadAcumuladaAnterior = x.Estimaciones.Sum(e => e.CantidadEstimada);
                newEstimacion.CantidadEjecutadaOT = x.CantidadEjecutada;  //
                newEstimacion.CantidadAcumuladaActual =x.Estimaciones.Sum(e => e.CantidadEstimada);
                newEstimacion.ImporteAcumuladoActual = newEstimacion.CantidadAcumuladaActual * x.Precio;
                newEstimacion.CantidadPorEstimar = x.CantidadPorEstimar;
                newEstimacion.ImportePorEstimar = x.ImportePorEstimar;
                newEstimacion.CantidadEstimada = x.CantidadPorEstimar;
                newEstimacion.ImporteEstimado= x.ImportePorEstimar;
                x.Estimaciones.Add(newEstimacion as CEstimaciones);
            });
            return contrato;
        }
		public IContratos2 EstimacionesPorContrato(int IdContrato,DateTime? FechaIni,DateTime? FechaFin)
		{
			DContratos2 all = new DContratos2();			
			IContratos2 Contrato = NContratos.GetById(IdContrato);
            Contrato = all.EstimacionesPorContrato(Contrato, FechaIni, FechaFin);			
			return Contrato;
		}        
		public IEnumerable<IEntidad> ContratosSinProyecto(IEnumerable<IPropsWhere> where)
		{
			IEnumerable<IEntidad> all = null;
			try
			{
				DContratos2 contrato = new DContratos2();
				IEnumerable<IContratos2> res=  contrato.ContratosSinProyecto(where);
				if(res != null && res.Count() > 0)
				{
					all= res.Select(x=>x as IEntidad);
				}				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return all;
		}
		public static void ToContratoDetalles(IEnumerable<IContratosDetalles2> detalles, IEnumerable<IConceptos> conceptos)
		{
			if(detalles == null || detalles.Count() <= 0) return;
			int idUnidadMedida = 0;
			detalles.ToList().ForEach(x =>
			{
				IEnumerable<IConceptos>concepto =conceptos.Where(i=>i.IdConcepto == x.IdConcepto);
				if(concepto!= null && concepto.Count()>0)
				{
					int.TryParse(concepto.FirstOrDefault().IdUnidadMedida.ToString(), out idUnidadMedida);
					x.Capitulo = concepto.FirstOrDefault().Capitulo;
					x.CodigoMoneda = concepto.FirstOrDefault().CodigoMoneda;
					x.Concepto = concepto.FirstOrDefault().Concepto;					
					x.Descripcion = concepto.FirstOrDefault().Descripcion;					
					x.IdConcepto = concepto.FirstOrDefault().IdConcepto;										
					x.IdMoneda = concepto.FirstOrDefault().IdMoneda;
					x.IdUnidadMedida = idUnidadMedida;
					x.Moneda = concepto.FirstOrDefault().Moneda;					
					x.Precio = concepto.FirstOrDefault().Precio;					
					x.UnidadMedida = concepto.FirstOrDefault().UnidadMedida;
					x.Insumos = new List<CContratosDetallesInsumos2>();
					concepto.FirstOrDefault().Insumos.ForEach(ii =>
					{											
						x.Insumos.Add(new CContratosDetallesInsumos2()
						{
							Cantidad=ii.Cantidad,
							Codigo=ii.Codigo,
							Costo=ii.CostoInsumo,
							Descripcion=ii.Descripcion,
							IdConcepto=ii.IdConcepto,							
							IdInsumo=ii.IdInsumo,							
							IdMoneda=ii.IdMoneda,
							Moneda=ii.Moneda,
							Unidad=ii.UnidadMedidaDescripcion

						});
					});
				}				
			});
		}
		public void ObtienePrecios(IContratos2 contrato)
		{
			try
			{
				DContratos2 data = new DContratos2();
				StringBuilder sb = new StringBuilder("");
				sb.Append("<Precios>");
				if(contrato.Conceptos != null && contrato.Conceptos.Count > 0)
				{
					contrato.Conceptos.ForEach(x =>
					{
						sb.Append(x.ToXml());
					});
				}
				sb.Append("</Precios>");
				IEnumerable<IContratosDetalles2> newPrecios= data.ObtienePrecios(contrato, sb.ToString());
				List<CContratosDetallesInsumos2> allInsumos = new List<CContratosDetallesInsumos2>();
				if(newPrecios != null && newPrecios.Count()>0)				
				{
					newPrecios.ToList().ForEach(x =>
					{
						x.Insumos.ForEach(i =>
						{
							allInsumos.Add(i);
						});
					});
					contrato.Conceptos.ForEach(c =>
					{
						IEnumerable<IContratosDetalles2> newConcepto = newPrecios.Where(x => x.IdConcepto == c.IdConcepto && x.EsAdicional == c.EsAdicional);
						if(newConcepto != null && newConcepto.Count() > 0)
						{
							c.Precio = newConcepto.FirstOrDefault().Precio;
							c.Insumos.ForEach(ii =>
							{
								IEnumerable<IContratosDetallesInsumos2> newInsumo = allInsumos.Where(x2 => x2.IdInsumo==ii.IdInsumo &&
																															x2.IdConcepto == ii.IdConcepto && 
									                                                                  x2.EsAdicional == ii.EsAdicional);
								if(newInsumo != null && newInsumo.Count() > 0)
								{
									ii.MotivoModificacion = newInsumo.FirstOrDefault().MotivoModificacion;
									ii.Costo = newInsumo.FirstOrDefault().Costo;
									ii.Codigo = newInsumo.FirstOrDefault().Codigo;
								}
							});
						}
					});
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
		}
		public static IContratos2 GetById(int IdContrato)
		{
			NContratos c = new NContratos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad=true,
				NombreCampo=NContratos.eFields.IdContrato.ToString(),
				Valor=IdContrato
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IContratos2;
			}
			return null;
		}
		public List<CPropsWhere> BuscaPreciosConceptos(IContratos2 entity)
		{
			List<int> conceptos = new List<int>();
			string ids = String.Empty;
			List<CPropsWhere> where = new List<CPropsWhere>();						
			if(entity != null)
			{
				if(entity.Conceptos != null && entity.Conceptos.Count() > 0)
				{
					entity.Conceptos.ForEach(x =>
					{
						conceptos.Add(x.IdConcepto);
					});
				}
				if(entity.Adicionales != null && entity.Adicionales.Count() > 0)
				{
					entity.Adicionales.ForEach(x =>
					{
						conceptos.Add(x.IdConcepto);
					});
				}					
				if(conceptos != null && conceptos.Count>0)
				{						
					ids = String.Join(",", conceptos.GroupBy(x => x).Select(grp => grp.Key).ToArray());
				}
				where.Add(new CPropsWhere()
					{
						Condicion="in",
						NombreCampo=NConceptos.eFields.IdConcepto.ToString(),
						Prioridad=true,
						Valor=ids
					});								
				return where;
			}
			return null;
		}
		#region Contratos_desde_vista_contratos2
		public IEnumerable<IvwContratos2> GetContratos(int IdProyecto)
		{
			NContratos c2 = new NContratos();
			DProyectoContratos pc = new DProyectoContratos();
			c2.AddWhere(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = String.Format("{0}.{1}", pc.Tabla, NProyectos.eFields.IdProyecto.ToString()),
				OmiteConcatenarTabla = true,
				Valor = IdProyecto,
				Condicion = "="
			});
			return c2.GetContratos();
		}
		public IEnumerable<IvwContratos2> GetContratos()
		{
			IEnumerable<IvwContratos2> contratos = null;
			try
			{
				DvwContratos2 datos = new DvwContratos2();
				DProyectoContratos ProyectoContratos = new DProyectoContratos();
				DMonedas moneda = new DMonedas();
				string[] camposJoin = new string[]
			  {																				
						
				  String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				  String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString())
				  
			  };
				string[] condicionesJoin = new string[] 
			  { 				   				 
				    /*Join with ProyectoContratos */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,ProyectoContratos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",ProyectoContratos.Tabla,NProyectoContratos.eFields.IdContrato.ToString()),
					String.Format("{0}.{1}",datos.Tabla,NContratos.eFields.IdContrato.ToString())),
					 /*Join with Monedas */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",datos.Tabla,NContratos.eFields.IdMoneda.ToString()))
			  };
				datos.SetConfigJoins(camposJoin, condicionesJoin);
				datos.AddWhere(_data.Where());				
				IEnumerable<IEntidad> res = datos.Select(new CvwContratos2());
				if(res != null && res.Count() > 0)
				{
					contratos = res.Select(x => x as IvwContratos2);
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return contratos;
		}
		#endregion
		public void GeneraCotizacionFromContrato(IContratos2 contrato)
		{
			if(contrato == null) return;
			contrato = NContratos.GetById(contrato.IdContrato);
			contrato.Cotizacion = contrato.Contrato;
			DContratos2 con = new DContratos2();
			NCotizaciones coti = new NCotizaciones();
			ICotizaciones cotizacionAux = new CCotizaciones();
			cotizacionAux.IdEmpresa=contrato.IdEmpresa;
			cotizacionAux.IdUnidad=contrato.IdUnidad;
			cotizacionAux.Cotizacion=contrato.Cotizacion;			
			coti.SetEntity(cotizacionAux);
			bool duplicado = coti.ValidaDuplicado(true);
			if(!duplicado)
			{
				con.GeneraCotizacionFromContrato(contrato);
			}
			else
			{
				this.SetError(coti.GetErrorMessage(), true);
			}
		}
		public IEnumerable<IContratosCotizaciones> CotizacionesGeneradas(IContratos2 contrato)
		{
			if(contrato == null || contrato.IdContrato <= 0) return null;
			IEnumerable<IEntidad> res = null;
			NContratosCotizaciones all = new NContratosCotizaciones();
			all.AddWhere(new CPropsWhere()
			{
				Condicion="=",
				NombreCampo=NContratosCotizaciones.eFields.IdContrato.ToString(),
				Prioridad=true,
				Valor=contrato.IdContrato
			});
			res = all.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IContratosCotizaciones);
			}
			return null;
		}
	}// NContratos ends.        
}// LibNegocio.Entidades ends.
