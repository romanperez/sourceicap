using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/03/2017 03:46:56 p. m.*/
namespace Negocio
{
	public partial class NContratosDetallesInsumos2 : NBase
	{
		public enum eFields
		{
			IdInsumoContrato = 0, IdContrato, IdConcepto, IdInsumo, Cantidad, Costo
		}
		public NContratosDetallesInsumos2()
		{
		}
		public NContratosDetallesInsumos2(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DContratosDetallesInsumos2 detalle = new DContratosDetallesInsumos2();
			DInsumos insumo = new DInsumos();
			DUnidadesMedida um = new DUnidadesMedida();
			DMonedas moneda = new DMonedas();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Codigo.ToString()),
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Descripcion.ToString()),
				String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.Unidad.ToString()),
				String.Format("{0}.{1} ",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Insumos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,insumo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdInsumo.ToString()),
					String.Format("{0}.{1}",detalle.Tabla,NContratosDetallesInsumos2.eFields.IdInsumo.ToString())),
					/*Join with Monedas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",detalle.Tabla,NInsumos.eFields.IdMoneda.ToString())),
					/*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,um.Tabla,DBase.On, 
				   String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdUnidad.ToString()))

			  };
			detalle.SetConfigJoins(camposJoin, condicionesJoin);
			return detalle;
		}		
	}// NContratosDetallesInsumos2 ends.        
}// LibNegocio.Entidades ends.