using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public class CUICatalogoProyectos : IUICatalogoProyectos
	{
		public virtual string Error
		{
			set;
			get;
		}
		public virtual IUICatalogo Catalogo
		{
			set;
			get;
		}
		public virtual List<CProyectosCostos> TotalPagina
		{
			set;
			get;
		}
		public virtual List<CProyectosCostos> Monedas
		{
			set;
			get;
		}
		public virtual List<CProyectosCostos> TotalGlobal
		{
			set;
			get;
		}
		public virtual double TGeneral
		{
			set;
			get;
		}
		public virtual double TPagina
		{
			set;
			get;
		}
		public CUICatalogoProyectos()
		{
			
		}
		public void CalculaCostos(IEnumerable<IProyectos> Proyectos)
		{
			try
			{
				TGeneral = 0;
				Catalogo = new CUICatalogo();
				Catalogo.Contenido = Proyectos; 
				GetAllMonedas();
				if(Proyectos != null && Proyectos.Count()>0)
				{
					//g.Totals = g.Total.ToString("c4");					
					var noConvertidos = Proyectos.Where(p => p.CostosGlobales != null && p.CostosGlobales.Count > 1);
					if(noConvertidos != null && noConvertidos.Count()>0)
					{
						noConvertidos.ToList().ForEach(x =>
						{
							Error+= x.Proyecto +",";
						});
					}
					Proyectos.ToList().ForEach(x =>
					{
						x.CostosGlobales.ForEach(y =>
						{
							y.Totals = y.Total.ToString("c4");
							TGeneral += y.Total;
						});
					});					
				}
				
			}
			catch(Exception e)
			{
				OnError(e);
			}
		}
		public CUICatalogoProyectos(IUICatalogo catalogo)
		{
			try
			{
				Catalogo = catalogo;
				GetAllMonedas();
			}
			catch(Exception eIni)
			{
				OnError(eIni);
			}
		}
		protected virtual void OnError(Exception error)
		{
			Error = error.Message;
		}
		protected virtual void GetAllMonedas()
		{
			if(Catalogo != null && Catalogo.Contenido != null && Catalogo.Contenido.Count() > 0)
			{
				List<CProyectosCostos> all = new List<CProyectosCostos>();
				List<CProyectosCostos> allGlobal = new List<CProyectosCostos>();
				Catalogo.Contenido.Where(c => ((IProyectos)(c)).EstatusProyecto != CProyectos.PROYECTO_RSMN_COSTOS).ToList().ForEach(x =>
				{
					IProyectos p = x as IProyectos;
					if(p != null)
					{
						if(p.CostosGlobales != null && p.CostosGlobales.Count > 0)
						{
							p.CostosGlobales.ForEach(m =>
							{
								all.Add(m);
							});
						}
					}
				});
				Catalogo.Contenido.Where(c => ((IProyectos)(c)).EstatusProyecto == CProyectos.PROYECTO_RSMN_COSTOS).ToList().ForEach(x =>
				{
					IProyectos p = x as IProyectos;
					if(p != null)
					{
						if(p.CostosGlobales != null && p.CostosGlobales.Count > 0)
						{
							p.CostosGlobales.ForEach(m =>
							{
								allGlobal.Add(m);
							});
						}
					}
				});
				if(all != null && all.Count() > 0)
				{


					TotalPagina = all.GroupBy(mm => new
					{
						Codigo = mm.Codigo,
						Moneda = mm.Moneda,
						IdMoneda = mm.IdMoneda
					}).Select(m => new CProyectosCostos()
					{
						Codigo = m.Key.Codigo,
						Moneda = m.Key.Moneda,
						IdMoneda = m.Key.IdMoneda,
						Total = m.Sum(x => x.Total),
					}).OrderBy(x => x.IdMoneda).ToList();

					if(allGlobal != null && allGlobal.Count() > 0)
					{
						TotalGlobal = allGlobal.GroupBy(mm => new
						{
							Codigo = mm.Codigo,
							Moneda = mm.Moneda,
							IdMoneda = mm.IdMoneda
						}).Select(m => new CProyectosCostos()
						{
							Codigo = m.Key.Codigo,
							Moneda = m.Key.Moneda,
							IdMoneda = m.Key.IdMoneda,
							Total = m.Sum(x => x.Total),
						}).OrderBy(x => x.IdMoneda).ToList();

						Monedas = allGlobal.GroupBy(mm => new
						{
							Codigo = mm.Codigo,
							Moneda = mm.Moneda,
							IdMoneda = mm.IdMoneda
						}).Select(m => new CProyectosCostos()
						{
							Codigo = m.Key.Codigo,
							Moneda = m.Key.Moneda,
							IdMoneda = m.Key.IdMoneda
						}).OrderBy(x => x.IdMoneda).ToList();
					}
				}
			}
		}
		public double ByMoneda(IProyectosCostos Moneda, IProyectos proyecto)
		{
			if(proyecto == null || proyecto.CostosGlobales == null || proyecto.CostosGlobales.Count() <= 0)
			{
				return 0;
			}
			var costo = proyecto.CostosGlobales.Where(i => i.IdMoneda == Moneda.IdMoneda);
			if(costo != null && costo.Count() > 0)
			{
				return costo.First().Total;
			}
			return 0;
		}
		public virtual double TotalPaginaByMoneda(IProyectosCostos Moneda)
		{
			if(Moneda == null || TotalPagina == null || TotalPagina.Count <= 0) return 0;
			var costo = TotalPagina.Where(t => t.IdMoneda == Moneda.IdMoneda);
			if(costo != null && costo.Count() > 0)
			{
				return costo.First().Total;
			}
			return 0;
		}
		public virtual double TotalGlobalByMoneda(IProyectosCostos Moneda)
		{
			if(Moneda == null || TotalGlobal == null || TotalGlobal.Count <= 0) return 0;
			var costo = TotalGlobal.Where(t => t.IdMoneda == Moneda.IdMoneda);
			if(costo != null && costo.Count() > 0)
			{
				return costo.First().Total;
			}
			return 0;
		}
	}// CUICatalogo ends.
}// Entidades ends.
