using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
/*Capa Entidades 16/11/2016 05:55:00 p. m.*/
namespace Entidades
{
	public class CUICatalogo:IUICatalogo
    {
		protected IUsuarios _Usuario;
		protected IEnumerable<IEntidad> _Contenido;
		protected IPaginaUI _PaginaInfo;
		protected IEnumerable<IPropsWhere> _FieldsSearch;
		protected IEnumerable<IPerfilAcciones> _Permisos;
		public virtual IEnumerable<IPropsWhere> FieldsSearch
		{
			set
			{
				_FieldsSearch = value;
			}
			get
			{
				return _FieldsSearch;
			}
		}
		public virtual IEnumerable<IEntidad> Contenido
		{
			set
			{
				_Contenido=value;
			}
			get
			{
				return _Contenido;
			}
		}
		public virtual IPaginaUI PaginaInfo
		{
			set
			{
				_PaginaInfo = value;
			}
			get
			{
				return _PaginaInfo;
			}
		}
		public virtual IEnumerable<IPerfilAcciones> Permisos
		{
			set
			{
				_Permisos = value;
			}
			get
			{
				return _Permisos;
			}
		}
		public virtual  string Nombre
		 {
			 get;
			 set;
		 }

		public virtual string ControladorName
		{
			get;
			set;
		}
		/*public virtual string Metodo
		{
			set;
			get;
		}
		public virtual int IdMenu
		{
			get;
			set;
		}	*/
		public virtual string controlesPersonalizados
		{
			set;
			get;
		}
		public virtual string NombreToolBar
		{
			set;
			get;
		}
		public virtual IUsuarios Usuario
		{
			get
			{
				return _Usuario;
			}
			set
			{
				_Usuario = value;
			}
		}
		public virtual int MenuCaller
		{
			set;
			get;
		}
		public CUICatalogo(){}
		public virtual TraduceTexto Traductor
		{
			set;
			get;
		}
        public string Empresas
        {
            get;
            set;
        }
        public virtual void SetPermisos(IEnumerable<IPerfilAcciones> permisos)	
		{
			this.Permisos = null;
			if(permisos == null || permisos.Count() <= 0) return;
			foreach(IPerfilAcciones permiso in permisos)
			{
				if(!String.IsNullOrEmpty(permiso.ParametrosMvc))
				{
					string[] valores = permiso.ParametrosMvc.Split('|');
					if(valores != null && valores.Count() > 0)
					{
						foreach(string valor in valores)
						{
							string[] propiedades = valor.Split(',');
							if(propiedades != null && propiedades.Count() >= 3)
							{
								string Name = propiedades[0].Trim();
								string value = propiedades[1].Trim();
								string valorTraducePropiedad = propiedades[2].Trim();
								if(value.ToLower() == "traduce")
								{
									value = this.Traductor(valorTraducePropiedad);
								}
								else
								{
									object valueAux = Datos.DBase.GetValue(this, value);
									if(valueAux != null)
									{
										value = (!String.IsNullOrEmpty(value)) ? String.Format("{0}{1}", valorTraducePropiedad, valueAux.ToString()) : valueAux.ToString();
									}
								}
								if(!String.IsNullOrEmpty(value))
								{
									permiso.Html = permiso.Html.Replace(String.Format("@{0}@", Name), value);
								}
							}
						}//foreach(string valor in valores) ends.
					}
				}//if(!String.IsNullOrEmpty(permiso.ParametrosMvc)) ends
			}//foreach(IPerfilAcciones permiso in permisos) ends.
			_Permisos = permisos;
		}//function SetPermisos ends.		
		public virtual bool HasPermiso(string name)
		{
			if(this._Permisos == null || _Permisos.Count() <= 0) return false;
			IEnumerable<IPerfilAcciones> permiso = _Permisos.Where(p => p.AccionNombre.ToLower() == name.ToLower());
			if(permiso != null && permiso.Count() > 0) return true;
			return false;
		}		
	 }// CUICatalogo ends.
}// Entidades ends.
