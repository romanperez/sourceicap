﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
namespace Negocio
{
	public class CModeloEstimaciones
	{
        public IContratos2 Contrato { set; get; }
        public int IdEstimacionShow
        {
            set;
            get;
        }
        public List<CEstimaciones> Estimaciones
        {
            set;
            get;
        }
       /* public double PorcentajeAmortizacionAnticipo { set; get; }
        public string DetallePorcentajeAmortizacionAnticipo { set; get; }*/
        public CModeloEstimaciones()
        {
        }
        public CModeloEstimaciones(int Estimacion,IContratos2 contrato)
		{
            IdEstimacionShow = (Estimacion<=0)? 1:Estimacion;
            Estimaciones = new List<CEstimaciones>();
            Contrato = contrato;
            IniciaEstimacionesUI(contrato);            
		}
        public CModeloEstimaciones(IContratos2 contrato)
        {             
            Estimaciones = new List<CEstimaciones>();
            Contrato = contrato;
            IniciaEstimacionesUI(contrato);
            IdEstimacionShow = NContratos.GetNumeroEstimacion(Estimaciones) - 1; 
        }
        public IEstimaciones EstimacionByContrato(int IdContratoDetalle)
        {
            if (IdEstimacionShow <= 0 || IdContratoDetalle <=0) return null;
            if (Estimaciones == null || Estimaciones.Count <= 0) return null;
            var res = Estimaciones.Where(x => x.NumeroEstimacion == IdEstimacionShow && x.IdContratosDetalles==IdContratoDetalle);
            if (res != null && res.Count() > 0)
            {
                return res.First() as IEstimaciones;
            }
            return null;
        }
        public IEstimaciones EstimacionActual()
        {
            if (IdEstimacionShow <= 0 ) return null;
            if (Estimaciones == null || Estimaciones.Count <= 0) return null;
            var res = Estimaciones.Where(x => x.NumeroEstimacion == IdEstimacionShow );
            if (res != null && res.Count() > 0)
            {
                return res.First() as IEstimaciones;
            }
            return null;
        }		
		public void IniciaEstimacionesUI(IContratos2 contrato)
		{
			if(contrato == null || contrato.Conceptos == null || contrato.Conceptos.Count <= 0) return;
			contrato.Conceptos.ForEach(x =>
			{
				x.Estimaciones.ForEach(e =>
				{
					Estimaciones.Add(e);					
				});
			});
			Estimaciones = Estimaciones.OrderBy(x => x.NumeroEstimacion).ToList();
            //this.PorcentajeAmortizacionGlobal();
			//Fechas = Estimaciones.GroupBy(x => x.FechaInicio).Select(x=>x.Key).ToList();
		}
        public IEnumerable<IEstimaciones> FiltroEstimaciones()
        {
            return from tbl in Estimaciones
                   group tbl by new
                   {
                       tbl.NumeroEstimacion,
                       tbl.FechaInicio,
                       tbl.FechaFin
                   } into g
                   select new CEstimaciones
                   {
                       NumeroEstimacion = g.First().NumeroEstimacion,
                       FechaInicio =g.First().FechaInicio,
                       FechaFin = g.First().FechaFin,
                   };
        }       
       /* public void PorcentajeAmortizacionGlobal()
        {
            if (Estimaciones == null || Estimaciones.Count <= 0) return;
            IEnumerable<IEstimaciones> estimacionPorcentaje = from tbl in Estimaciones
                                                               group tbl by new
                                                               {
                                                                   tbl.NumeroEstimacion,
                                                                   tbl.PorcentajeAmortizacionAnticipo,                                           
                                                               } into g
                                                               select new CEstimaciones
                                                               {
                                                                   NumeroEstimacion = g.First().NumeroEstimacion,
                                                                   PorcentajeAmortizacionAnticipo = g.First().PorcentajeAmortizacionAnticipo        
                                                               };            
            this.PorcentajeAmortizacionAnticipo = estimacionPorcentaje.Sum(x => x.PorcentajeAmortizacionAnticipo).Value;
            estimacionPorcentaje.ToList().ForEach(x => 
            { 
                this.DetallePorcentajeAmortizacionAnticipo += String.Format(" {0} {1} [{2}]% ",
                    CIdioma.TraduceCadena("IEstimacionesNumeroEstimacion"),
                    x.NumeroEstimacion,
                    x.PorcentajeAmortizacionAnticipo);
            });
            if (PorcentajeAmortizacionAnticipo > 100)
            {
                this.DetallePorcentajeAmortizacionAnticipo = String.Format("{0} = {1}%",
                    DetallePorcentajeAmortizacionAnticipo, PorcentajeAmortizacionAnticipo);
            }
        }*/
	}
}
