﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
namespace Negocio
{
	public class CModeloReporteProyecto
	{
		public virtual List<string> Contratos
		{
			set;
			get;
		}
		public virtual List<CContratosDetalles2> Conceptos
		{
			set;
			get;
		}
		public void BuildReporte(IEnumerable<IContratosDetalles2> conceptos)
		{
			if(conceptos == null && conceptos.Count() <= 0) return;
			Contratos = conceptos.GroupBy(mm => mm.Contrato).Select(x=>x.Key).ToList();
			Conceptos = conceptos.Select(x=> x as CContratosDetalles2).ToList();
		}
		public double GetCantidadByContrato(IContratosDetalles2 concepto)
		{
			return (concepto.CantidadProgramada > 0) ? concepto.CantidadProgramada : concepto.Cantidad;
		}
	}
}
