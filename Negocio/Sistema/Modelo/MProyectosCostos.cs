﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
namespace Negocio
{
	public class MProyectosCostos
	{
		public virtual CTipoCambios MonedaDestino
		{
			set;
			get;
		}
		public virtual int IdEmpresa
		{
			set;
			get;
		}
		public virtual int IdUnidad
		{
			set;
			get;
		}
		public virtual int Tipo
		{
			set;
			get;
		}
		public virtual List<CTipoCambios> TipoCambios
		{
			set;
			get;
		}
		public virtual List<CPropsWhere> Where
		{
			set;
			get;
		}
		public virtual int TotalPagina
		{
			set;
			get;
		}
		public virtual string TotalPaginas
		{
			set;
			get;
		}
		public virtual int TotalGeneral
		{
			set;
			get;		
		}
		public virtual string TotalGenerals
		{
			set;
			get;
		}
		public virtual List<CProyectos> Proyectos
		{
			set;
			get;
		}
		public virtual List<string> ColumnasHide
		{
			set;
			get;
		}

	}
}
