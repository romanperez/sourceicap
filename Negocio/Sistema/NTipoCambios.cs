using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/01/2017 02:07:44 p. m.*/
namespace Negocio
{

	public partial class NTipoCambios : NBase
	{
		public enum eFields
		{
			IdTipoCambio = 0, IdEmpresa, IdMonedaOrigen, IdMonedaDestino, FechaInicio, FechaFin, Valor, Estatus
		}
		public NTipoCambios()
		{
		}
		public NTipoCambios(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DTipoCambios tipoCambios = new DTipoCambios();
			DEmpresas empresas = new DEmpresas();
			DMonedas moneda = new DMonedas();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("M1.{0} AS MonedaOrigen",NMonedas.eFields.Moneda.ToString()),
				String.Format("M2.{0} AS MonedaDestino",NMonedas.eFields.Moneda.ToString()),
				String.Format("M1.{0} AS CodigoMonedaOrigen",NMonedas.eFields.Codigo.ToString()),
				String.Format("M2.{0} AS CodigoMonedaDestino",NMonedas.eFields.Codigo.ToString())
				
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresas.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",tipoCambios.Tabla,NTipoCambios.eFields.IdEmpresa.ToString()))	,
					/*Join with Monedas*/
					String.Format("{0} {1}  M1 {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("M1.{0}",NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",tipoCambios.Tabla,NTipoCambios.eFields.IdMonedaOrigen.ToString())),
					/*Join with Monedas*/
					String.Format("{0} {1}  M2 {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("M2.{0}",NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",tipoCambios.Tabla,NTipoCambios.eFields.IdMonedaDestino.ToString()))
			  };
			tipoCambios.SetConfigJoins(camposJoin, condicionesJoin);
			return tipoCambios;
		}
		public int GetIdValorCambio(ITipoCambios cambio, DateTime fecha)
		{
			return _data.fnGetIdValorCambio(cambio.IdEmpresa, fecha, cambio.IdMonedaOrigen, cambio.IdMonedaDestino);		
		}
		public bool VerificaFechas(ITipoCambios cambio, DateTime fecha)
		{
			int resultado1 = GetIdValorCambio(cambio, fecha);
			if(resultado1 != 0 ) 
			{
				if(resultado1 != cambio.IdTipoCambio )
				{
					SetError(String.Format("ID:{0} {1}",resultado1, TraduceTexto("NTipoCambioValorYaExiste")));
					return false;
				}
				
			}
			return true;
		}
		protected bool ValidaTipoCambio()
		{
			if(_data.Delete == 1) return true;
			bool validacion = true;
			if(_Entity != null)
			{
				ITipoCambios tipo = _Entity as ITipoCambios;
				if(tipo.FechaInicio > tipo.FechaFin)
				{
					SetError(TraduceTexto("NTipoCambioFechasIncorrectas"));
					return false;
				}
				if(tipo.Valor <= 0)
				{
					SetError(TraduceTexto("NTipoCambioValorIncorrectas"));
					return false;
				}
				validacion = this.VerificaFechas(tipo, tipo.FechaInicio);
				validacion = this.VerificaFechas(tipo, tipo.FechaFin);
				return validacion;
			}			
			return true;
		}
		protected void BeforeSave()
		{
			ITipoCambios tipo = _Entity as ITipoCambios;
			tipo.Valor = Math.Round(tipo.Valor, DBase.DECIMALES_OPERACIONES);
		}
		public override void Save()
		{
			if(ValidaTipoCambio())
			{
				if(_data == null) return;
				BeforeSave();
				_data.Save(_Entity);
			}
		}
		public override void Update()
		{

			if(ValidaTipoCambio())
			{
				if(_data == null) return;
				BeforeSave();
				_data.Save(_Entity);
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public ITipoCambios GetIdValorCambio(ITipoCambios Entity)
		{
			ITipoCambios tipo = null;
			try
			{
				int idTipo = this.GetIdValorCambio(Entity, Entity.FechaInicio);
				tipo = NTipoCambios.GetById(idTipo);
				if(tipo == null)
				{
					return null;
				}
			}
			catch(Exception eFecha)
			{
				OnError(eFecha);
			}
			return tipo;
		}
		public static ITipoCambios GetById(int id)
		{
			NTipoCambios tipo = new NTipoCambios();
			List<CPropsWhere> where = new List<CPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NTipoCambios.eFields.IdTipoCambio.ToString(),
				Valor = id
			});
			tipo.AddWhere(where);
			IEnumerable<IEntidad> res = tipo.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as ITipoCambios;
			}
			return null;
		}
	}// NTipoCambios ends.        
}// LibNegocio.Entidades ends.
