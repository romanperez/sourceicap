using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NOrdenesTrabajo : NBase
	{
		protected const int ID_DIAGRAMAS = 15;
		protected const int ID_DETALLLE_INSTALACION = 16;
		public delegate IOTDocumentos GetDocumentoById(int IdDocumento);
		public delegate DFile DownloadDocumento(IOTDocumentos documento);
		public enum eFields
		{
			IdOrdenTrabajo = 0, IdEmpresa	,IdProyecto,
			Comentarios, ComentariosCliente, Estatus,
			IdContrato, IdActividad, Folio, Ubicacion, 
			FechaInicio, FechaFin, IdEstatus, IdPrioridad,IdTipo
		}
		public NOrdenesTrabajo(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DOrdenesTrabajo ot= new DOrdenesTrabajo();
			DProyectos p = new DProyectos();
			DUnidades u =new DUnidades();
			DClientes cli = new DClientes();
			DEmpresas e = new DEmpresas();
			DActividades a = new DActividades();
			DEstatus es = new DEstatus();
			DOTContratos otContratos = new DOTContratos();
			DContratos2 contrato = new DContratos2();
			string[] camposJoin = new string[]
				  {																	
					  String.Format("{0}.{1}",e.Tabla,NEmpresas.eFields.Empresa.ToString()),
					  String.Format("{0}.{1}",u.Tabla,NUnidades.eFields.Unidad.ToString()),
					  String.Format("{0}.{1}",p.Tabla,NProyectos.eFields.Proyecto.ToString()),
					  String.Format("{0}.{1}",cli.Tabla,NClientes.eFields.IdCliente.ToString()),
					  String.Format("{0}.{1}",cli.Tabla,NClientes.eFields.RazonSocial.ToString()),
					  String.Format("{0}.{1} + ' ' +{2}.{3} AS {4}",
					               a.Tabla,
										NActividades.eFields.Actividad.ToString(),
										a.Tabla,
										NActividades.eFields.Descripcion.ToString(),
										NActividades.eFields.Actividad.ToString()),
					  String.Format("{0}.{1} as EstatusOT","e1",NEstatus.eFields.Nombre.ToString()),
					  String.Format("{0}.{1} as PrioridadOT","e2",NEstatus.eFields.Nombre.ToString()),
					  String.Format("{0}.{1} as TipoOT","e3",NEstatus.eFields.Nombre.ToString()),
					  String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.Contrato.ToString())
				  };
			string[] condicionesJoin = new string[] 
				  { 								  
					  /*Join with Proyectos*/						
					   String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,p.Tabla,DBase.On, 
					   String.Format("{0}.{1}",p.Tabla,NProyectos.eFields.IdProyecto.ToString()),
					   String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdProyecto.ToString())),
						/*Join with Unidades*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,u.Tabla,DBase.On, 
						String.Format("{0}.{1}",u.Tabla,NUnidades.eFields.IdUnidad.ToString()),
						String.Format("{0}.{1}",p.Tabla,NProyectos.eFields.IdUnidad.ToString()))	,
						/*Join with Empresas*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,e.Tabla,DBase.On, 
						String.Format("{0}.{1}",e.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
						String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdEmpresa.ToString())),
						/*Join with Clientes*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cli.Tabla,DBase.On, 
						String.Format("{0}.{1}",cli.Tabla,NClientes.eFields.IdCliente.ToString()),
						String.Format("{0}.{1}",p.Tabla,NProyectos.eFields.IdCliente.ToString())),
						/*Join with Actividades*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,a.Tabla,DBase.On, 
						String.Format("{0}.{1}",a.Tabla,NActividades.eFields.IdActividad.ToString()),
						String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdActividad.ToString())),
						/*Join with Estatus Estado OT*/
						String.Format("{0} {1} as e1 {2} {3} = {4}", DBase.InnerJoin,es.Tabla,DBase.On, 
						String.Format("{0}.{1}","e1",NEstatus.eFields.IdEstatus.ToString()),
						String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdEstatus.ToString())),
						/*Join with Estatus Prioridad OT*/
						String.Format("{0} {1} as e2 {2} {3} = {4}", DBase.InnerJoin,es.Tabla,DBase.On, 
						String.Format("{0}.{1}","e2",NEstatus.eFields.IdEstatus.ToString()),
						String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdPrioridad.ToString())),
						/*Join with Estatus Prioridad OT*/
						String.Format("{0} {1} as e3 {2} {3} = {4}", DBase.InnerJoin,es.Tabla,DBase.On, 
						String.Format("{0}.{1}","e3",NEstatus.eFields.IdEstatus.ToString()),
						String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdTipo.ToString())),
						/*Join with Estatus Contratos OT*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,otContratos.Tabla,DBase.On, 
						String.Format("{0}.{1}",otContratos.Tabla,NOTContratos.eFields.IdOrdenTrabajo.ToString()),
						String.Format("{0}.{1}",ot.Tabla,NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString())),
						/*Join with Contratos*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,contrato.Tabla,DBase.On, 
						String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdContrato.ToString()),
						String.Format("{0}.{1}",otContratos.Tabla,NOTContratos.eFields.IdContrato.ToString()))
				  };
			ot.SetConfigJoins(camposJoin, condicionesJoin);
			return ot;
		}
		public NOrdenesTrabajo()
		{
		}
		public IParametros PersonalAtencion()
		{
			return NParametros.GetByNombre("PREFIJO_OT_PERSONAL_ATENCION");
		}
		public IParametros PersonalContacto()
		{
			return NParametros.GetByNombre("PREFIJO_OT_PERSONAL_CONTACTO");
		}
		public IEstatus EstatusOTPersonalAtencion(int IdEmpresa)
		{
			NEstatus estatus = new NEstatus();
			IParametros pAtnecion = PersonalAtencion();
			IEnumerable<IEstatus> resAtencion = estatus.ByClasificacion(IdEmpresa, pAtnecion.Valor.ToString());
			if(resAtencion.Any())
			{
				return resAtencion.FirstOrDefault();
			}
			return new CEstatus();
		}
		public IEstatus EstatusOTPersonalContacto(int IdEmpresa)
		{
			NEstatus estatus = new NEstatus();
			IParametros pContacto = PersonalContacto();
			IEnumerable<IEstatus> resContacto = estatus.ByClasificacion(IdEmpresa, pContacto.Valor.ToString());
			if(resContacto.Any())
			{
				return resContacto.FirstOrDefault();
			}
			return new CEstatus();
		}
		protected virtual void SetDiagramas(IOrdenesTrabajo ot)
		{
			if(!ot.UpLoadFiles.Any()) return;
			if(String.IsNullOrEmpty(ot.ListaDiagramas)) return;
			string[] diagramas = ot.ListaDiagramas.Split(new char[]{','},StringSplitOptions.RemoveEmptyEntries);
			if(diagramas == null || diagramas.Count() <= 0) return;
			ot.UpLoadFiles.ForEach(x =>
			{
				if(x != null)
				{
					if(diagramas.Contains(x.Name))
					{

						ot.OTDiagramas.Add(new COTDocumentos()
						{
							Estatus = true,
							IdOrdenTrabajo = ot.IdOrdenTrabajo,
							IdTipoDocumento = NOrdenesTrabajo.ID_DIAGRAMAS,
							RutaDocumento = x.Url
						});
					}
				}
			});
		}
		protected virtual void SetDetallesInstalacion(IOrdenesTrabajo ot)
		{
			if(!ot.UpLoadFiles.Any()) return;
			if(String.IsNullOrEmpty(ot.ListaDetallesInstalacion)) return;						
			string[] detallesInstalacion = ot.ListaDetallesInstalacion.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			if(detallesInstalacion == null || detallesInstalacion.Count() <= 0) return;
			ot.UpLoadFiles.ForEach(x =>
			{
				if(x != null)
				{
					if(detallesInstalacion.Contains(x.Name))
					{

						ot.OTDetalleInstalacion.Add(new COTDocumentos()
						{
							Estatus = true,
							IdOrdenTrabajo = ot.IdOrdenTrabajo,
							IdTipoDocumento = NOrdenesTrabajo.ID_DETALLLE_INSTALACION,
							RutaDocumento = x.Url
						});
					}
				}
			});
		}
		protected virtual void SetTecnicos(IOrdenesTrabajo ot)
		{
			if(ot.OTPersonal == null || ot.OTPersonal.Count <= 0) return;
			ot.OTPersonal.ForEach(x =>
			{
				x.Estatus = true;
			});
		}
		protected string ListToString(IEnumerable<IOTPersonalCliente> lista, string separador)
		{
			if(lista == null || lista.Count() <= 0) return String.Empty;
			StringBuilder all = new StringBuilder("");
			int c = 0;
			foreach(IOTPersonalCliente persona in lista)
			{
				all.Append(persona.NombreCompleto);
				if(c < lista.Count() - 1)
				{
					all.Append(separador);
				}
				c++;
			}
			return all.ToString();
		}
		protected virtual void SetPersonal(IOrdenesTrabajo ot)
		{
			IEstatus resAtencion = EstatusOTPersonalAtencion(ot.IdEmpresa);
			IEstatus resContacto = EstatusOTPersonalContacto(ot.IdEmpresa);
			if(resContacto != null)
			{
				ot.PersonalClienteAtencion.ForEach(x =>
				{
					if(x!= null)
					{
						x.Estatus = true;
						x.IdEstatus = resAtencion.IdEstatus;
					}
					
				});
			}
			if(resContacto != null)
			{
				ot.PersonalClienteContacto.ForEach(x =>
				{
					if(x != null)
					{
						x.Estatus = true;
						x.IdEstatus = resContacto.IdEstatus;
					}
				});
			}		
		}
		protected virtual bool ValidaOT()
		{
			if(_data.Delete == 1) return true;
			bool valido = true;
			if(_Entity != null)
			{
				IOrdenesTrabajo OT = _Entity as IOrdenesTrabajo;
				SetTecnicos(OT);
				SetPersonal(OT);
				SetDiagramas(OT);
				SetDetallesInstalacion(OT);
				valido = ValidaFileUpload(OT);				
				if(OT.Contratos != null && OT.Contratos.Count > 1)
				{
					SetError(TraduceTexto("NOTSoloUnContrato"));					
					return false;
				}
				if(OT.OTConceptos != null && OT.OTConceptos.Count() > 0)
				{
					//Evitamos conceptos repetidos.
					IEnumerable<COTConceptos> all = OT.OTConceptos.Where(c => c.IdOTConcepto >= 0 && c.IdOTConcepto >= 0);
					var result = from item in all
									 group item by item.IdConcepto into g
									 select new CMovimientos
									 {
										 Tipo = g.Count()
									 };
					var repetidos = result.Where(x => x.Tipo > 1);
					if(repetidos != null && repetidos.Count() > 0)
					{
						SetError(TraduceTexto("NOTConceptoMultiple"));
						return false;
					}
					var result2 = from item in all
									  group item by item.IdConceptoClasificacion into g
									  select new CMovimientos
									  {
										  Tipo = g.Count()
									  };
					if(result2 != null && result2.Count() > 1)
					{
						SetError(TraduceTexto("NOTConceptosDistintasCL"));
						return false;
					}
				}
				else
				{
					SetError(TraduceTexto("NOTOrdenSinConceptos"));
					return false;
				}
				/*	IEnumerable<COTConceptos> all = OT.OTConceptos.Where(c=> c.IdOTConcepto>=0);
					if(all!=null && all.Count()>1)
					{
						SetError(TraduceTexto("NOTSoloUnConcepto"));
						return false;
					}					
					var news =  OT.OTConceptos.Where(c=> c.IdOTConcepto==0);
					if(news != null && news.Count()>0)
					{
						IEnumerable<IOTConceptos> saved = BuscaConceptos(OT);
						if(saved != null && saved.Count() >= 1)
						{
							SetError(TraduceTexto("NOTSoloUnConcepto"));
							return false;
						}
					}					
				}*/
				if(OT.IdActividad <= 0)
				{
					SetError(TraduceTexto("IOTSeleccioneActividad"));
					return false;
				}
				if(OT.IdTipo <= 0)
				{
					SetError(TraduceTexto("IOTSeleccioneTipo"));
					return false;
				}
				if(String.IsNullOrEmpty(OT.Folio))
				{
					SetError(TraduceTexto("IOTSeleccioneFolio"));
					return false;
				}
				if(String.IsNullOrEmpty(OT.Ubicacion))
				{
					SetError(TraduceTexto("IOTSeleccioneUbicacion"));
					return false;
				}
				if(OT.IdEstatus <= 0)
				{
					SetError(TraduceTexto("IOTSeleccioneEstatus"));
					return false;
				}
				if(OT.IdPrioridad <= 0)
				{
					SetError(TraduceTexto("IOTSeleccionePrioridad"));
					return false;
				}
			}
			return valido;
		}
		public override void Save()
		{
			if(ValidaOT())
			{
				if(_data == null) return;
				_data.Save(_Entity);				
			}
		}
		public override void Update()
		{
			if(ValidaOT())
			{
				if(_data == null) return;
				_data.Save(_Entity);				
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public virtual IPagina<Entity> BuscarIndex<Entity>(int pagina) where Entity : IEntidad, new()
		{
			DOrdenesTrabajo all = new DOrdenesTrabajo();
			if(_data != null)
			{
				all.AddWhere(_data.Where());
			}
			return all.IndexOrdenesTrabajo<Entity>(pagina);
		}
		protected string BuildIds(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			int[] idOts = (from tbl in ordenes
								select tbl.IdOrdenTrabajo).ToArray();
			return String.Join(",", idOts);
		}		
		public override IPagina<Entity> Buscar<Entity>(string campoKey, int pagina, int registrosPP)
		{
			campoKey = String.Format("{0}.{1}", new DOrdenesTrabajo().Tabla, campoKey);
			return base.Buscar<Entity>(campoKey, pagina, registrosPP);
		}
		public override IEnumerable<IEntidad> Buscar()
		{			
			IEnumerable<IEntidad> res= base.Buscar();
			if(res!=null && res.Count()>0)
			{				
				GetActividad(res.Select(x => x as IOrdenesTrabajo));
				GetClientesPersonal(res.Select(x => x as IOrdenesTrabajo));
				GetContratos(res.Select(x => x as IOrdenesTrabajo));
				GetPersonal(res.Select(x => x as IOrdenesTrabajo));
				GetDiagramas(res.Select(x => x as IOrdenesTrabajo));
				GetDetallesInstalacion(res.Select(x => x as IOrdenesTrabajo));
				GetConceptos(res.Select(x => x as IOrdenesTrabajo));
				return res;
			}
			return null;
		}
		public void GetActividad(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			int[] idOts = (from tbl in ordenes
								group tbl by tbl.IdActividad into g
								select  g.Key ).ToArray();
			string ids= String.Join(",", idOts);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NActividades.eFields.IdActividad.ToString(),
				Valor = ids
			});
			NActividades n = new NActividades();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IActividades> allActividades = null;
			if(res1.Any())
			{
				allActividades = res1.Select(x => x as IActividades);
				ordenes.ToList().ForEach(x =>
				{
					x.OTActividad= allActividades.Where(y => y.IdActividad == x.IdActividad).FirstOrDefault() as CActividades;
				});
			}
		}
		public void GetClientesPersonal(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			string ids = BuildIds(ordenes);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad=true,
				Condicion="in",
				NombreCampo=NOTPersonalCliente.eFields.IdOrdenTrabajo.ToString(),
				Valor=ids
			});
			NOTPersonalCliente n = new NOTPersonalCliente();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IOTPersonalCliente> personal = null;
			if(res1.Any())
			{
				personal = res1.Select(x => x as IOTPersonalCliente);
				ordenes.ToList().ForEach(x =>
				{
					IEstatus resAtencion = EstatusOTPersonalAtencion(x.IdEmpresa);
					IEstatus resContacto = EstatusOTPersonalContacto(x.IdEmpresa);
					IEnumerable<IOTPersonalCliente> atencion = personal.Where(p => p.IdOrdenTrabajo == x.IdOrdenTrabajo && p.IdEstatus == resAtencion.IdEstatus);
					IEnumerable<IOTPersonalCliente> contacto = personal.Where(p => p.IdOrdenTrabajo == x.IdOrdenTrabajo && p.IdEstatus == resContacto.IdEstatus);
					if(atencion.Any())
					{
						x.PersonalClienteAtencion = atencion.Select(aa => aa as COTPersonalCliente).ToList();
						x.ListClienteAtencion = ListToString(x.PersonalClienteAtencion, ",");
					}
					if(contacto.Any())
					{
						x.PersonalClienteContacto = contacto.Select(aa => aa as COTPersonalCliente).ToList();
						x.ListClienteContacto = ListToString(x.PersonalClienteContacto, ",");
					}
				});
			}						
		}
		public void GetContratos(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			string ids = BuildIds(ordenes);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NOTContratos.eFields.IdOrdenTrabajo.ToString(),
				Valor = ids
			});
			NOTContratos n = new NOTContratos();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IOTContratos> allContratos = null;
			if(res1.Any())
			{
				allContratos = res1.Select(x => x as IOTContratos);
				ordenes.ToList().ForEach(x =>
				{
					IEnumerable<IOTContratos> aux = allContratos.Where(y => y.IdOrdenTrabajo == x.IdOrdenTrabajo);
					if(aux.Any())
					{
						x.Contratos = aux.Select(y => y as COTContratos).ToList();
						x.Contrato = ((IOTContratos)x.Contratos.First()).Contrato;
					}

				});
			}			
		}
		public void GetPersonal(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			string ids = BuildIds(ordenes);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NOTContratos.eFields.IdOrdenTrabajo.ToString(),
				Valor = ids
			});
			NOTPersonal n = new NOTPersonal();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IOTPersonal> allPersonal = null;
			if(res1.Any())
			{
				allPersonal = res1.Select(x => x as IOTPersonal);
				ordenes.ToList().ForEach(x =>
				{
					IEnumerable<IOTPersonal> aux = allPersonal.Where(y => y.IdOrdenTrabajo == x.IdOrdenTrabajo);
					if(aux.Any())
					{
						x.OTPersonal = aux.Select(y => y as COTPersonal).ToList();
					}
				});
			}
		}
		public void GetDiagramas(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			string ids = BuildIds(ordenes);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NOTContratos.eFields.IdOrdenTrabajo.ToString(),
				Valor = ids
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NOTDocumentos.eFields.IdTipoDocumento.ToString(),
				Valor = NOrdenesTrabajo.ID_DIAGRAMAS,
				Operador=DBase.AND
			});
			NOTDocumentos n = new NOTDocumentos();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IOTDocumentos> allDiagramas = null;
			if(res1.Any())
			{
				allDiagramas = res1.Select(x => x as IOTDocumentos);
				ordenes.ToList().ForEach(x =>
				{
					IEnumerable<IOTDocumentos> aux = allDiagramas.Where(y => y.IdOrdenTrabajo == x.IdOrdenTrabajo);
					if(aux.Any())
					{
						x.OTDiagramas = aux.Select(y => y as COTDocumentos).ToList();
					}
				});
			}
		}
		public void GetDetallesInstalacion(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			string ids = BuildIds(ordenes);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NOTContratos.eFields.IdOrdenTrabajo.ToString(),
				Valor = ids
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NOTDocumentos.eFields.IdTipoDocumento.ToString(),
				Valor = NOrdenesTrabajo.ID_DETALLLE_INSTALACION,
				Operador = DBase.AND
			});
			NOTDocumentos n = new NOTDocumentos();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IOTDocumentos> allDetallesInstalacion = null;
			if(res1.Any())
			{
				allDetallesInstalacion = res1.Select(x => x as IOTDocumentos);
				ordenes.ToList().ForEach(x =>
				{
					IEnumerable<IOTDocumentos> aux = allDetallesInstalacion.Where(y => y.IdOrdenTrabajo == x.IdOrdenTrabajo);
					if(aux.Any())
					{
						x.OTDetalleInstalacion = aux.Select(y => y as COTDocumentos).ToList();
					}
				});
			}
		}

		public void GetConceptos(IEnumerable<IOrdenesTrabajo> ordenes)
		{
			const string ROOT = "<OT>{0}</OT>";
			const string PLANTILLA = "<ID>{0}</ID>";
			string[] idOts = (from tbl in ordenes
								select String.Format(PLANTILLA, tbl.IdOrdenTrabajo.ToString())).ToArray();
			string all = String.Join("", idOts);
			all = String.Format(ROOT, all);
			DOTConceptos OTS = new DOTConceptos();
			IEnumerable<IOTConceptos> allConceptos = OTS.GetConceptos(all);
			if(allConceptos!= null && allConceptos.Count()>0)
			{
				ordenes.ToList().ForEach(x =>
				{
					IEnumerable<IOTConceptos> aux = allConceptos.Where(y => y.IdOrdenTrabajo == x.IdOrdenTrabajo);
					if(aux!=null)
					{
						x.OTConceptos = aux.Select(y => y as COTConceptos).ToList();
					}
				});
			}			
		}
		protected virtual bool ValidaFileUpload(IOrdenesTrabajo orden)
		{
			if(!orden.UpLoadFiles.Any()) return true;
			bool validos = true;
			orden.UpLoadFiles.ForEach(x =>
			{
				if(!String.IsNullOrEmpty(x.Error))
				{
					SetError(x.Error, true);
					validos = false;
				}
			});
			return validos;
		}
		protected virtual IEnumerable<IOTConceptos> BuscaConceptos(IOrdenesTrabajo orden)
		{
			if(orden == null || orden.IdOrdenTrabajo <= 0) return null;
			return NOTConceptos.GetByIdOrdenTrabajo(orden.IdOrdenTrabajo);
		}
		public static IOrdenesTrabajo GetById(int IdOrdenTrabajo)
		{
			NOrdenesTrabajo o = new NOrdenesTrabajo();
			o.AddWhere(new CPropsWhere()
			{
				NombreCampo=NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString(),
				Condicion="=",
				Prioridad=true,
				Valor=IdOrdenTrabajo
			});
			IEnumerable<IEntidad> res= o.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.First() as IOrdenesTrabajo;
			}
			return null;
		}
		public static IEnumerable<IContratosDetallesInsumos2> AgrupaInsumos(IOrdenesTrabajo ot)
		{
			List<IContratosDetallesInsumos2> insumos = new List<IContratosDetallesInsumos2>();
			foreach(IOTConceptos concepto in ot.OTConceptos)
			{
				concepto.Insumos.ForEach(i =>
				{
					insumos.Add(i);
				});
			}
			if(insumos != null && insumos.Count() > 0)
			{
				return insumos.GroupBy(i => i.IdInsumo).Select(x => new CContratosDetallesInsumos2()
				{
					Cantidad=x.Sum(xs=> xs.Cantidad),
					CantidadProgramada = x.Sum(xs => xs.CantidadProgramada),
					Codigo = x.First().Codigo,
					Descripcion = x.First().Descripcion,
					Unidad = x.First().Unidad,
					CodigoMoneda = x.First().CodigoMoneda					
				});						 
			}
			return insumos;
		}
		public static List<DFile> ToListDFile(IEnumerable<IOTDocumentos> diagramas,GetDocumentoById getDoc,DownloadDocumento dwDoc)
		{
			List<DFile> files= new List<DFile>();
			if(diagramas == null || diagramas.Count() <= 0) return files;
			diagramas.ToList().ForEach(x =>
			{
				IOTDocumentos docAux = getDoc(x.IdOTDocumentos);
				if(docAux != null)
				{
					DFile fileAux = dwDoc(docAux);
					if(fileAux != null)
					{
						files.Add(fileAux);
					}
				}
			});
			return files;
		}
		public IEnumerable<IOrdenesTrabajo> GetOrdenesByProyecto(int IdProyecto)
		{
			this.ClearWhere();
			this.AddWhere(new CPropsWhere()
			{
				Condicion="=",
				NombreCampo=NOrdenesTrabajo.eFields.IdProyecto.ToString(),
				Prioridad=true,
				Valor=IdProyecto
			});
			IEnumerable<IEntidad> res = this.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IOrdenesTrabajo);
			}
			return null;
		}
		public IEnumerable<IOrdenesTrabajo> GetActividadesByContrato(int IdContrato,DateTime FechaInicial,DateTime FechaFinal)
		{
			NOTContratos contratos = new NOTContratos();
			contratos.AddWhere(new CPropsWhere()
			{
				Condicion="=",
				NombreCampo=NOTContratos.eFields.IdContrato.ToString(),
				Prioridad=true,
				Valor=IdContrato
			});
			IEnumerable<IEntidad> res = contratos.Buscar();
			if(res != null && res.Count() > 0)
			{
				int[] idOt = (from tbl in res
								 select ((IOTContratos)tbl).IdOrdenTrabajo).ToArray();
				if(idOt != null && idOt.Count() > 0)
				{
					NOrdenesTrabajo ordenes = new NOrdenesTrabajo();
					ordenes.AddWhere(new CPropsWhere()
					{
						Condicion = "in",
						NombreCampo =NOrdenesTrabajo.eFields.IdOrdenTrabajo.ToString(),
						Prioridad = true,
						Valor = String.Join(",",idOt),
						OmiteParametro=true
					});
					ordenes.AddWhere(new CPropsWhere()
					{
						Condicion = "between",
						NombreCampo =String.Format("cast({0} as date)",  NOrdenesTrabajo.eFields.FechaInicio.ToString()),
						Prioridad = true,
						Valor = new object[] { String.Format("'{0}'", FechaInicial.ToString("d")), 
							                   String.Format("'{0}'", FechaFinal.ToString("d")) },
                  OmiteConcatenarTabla=true,
						Operador =DBase.AND,
						OmiteParametro=true
					});
					IEnumerable<IEntidad> otsResult = ordenes.Buscar();
					if(otsResult != null && otsResult.Count() > 0)
					{
						otsResult.ToList().ForEach(x =>
						{
							IOrdenesTrabajo ot = x as IOrdenesTrabajo;
							if(ot.OTPersonal != null && ot.OTPersonal.Count() > 0)
							{
								ot.OTPersonal.ForEach(t =>
								{
									ot.ListTecnicos += t.NombreEmpleado + Environment.NewLine;
								});
							}
						});
						return otsResult.Select(x => x as IOrdenesTrabajo);
					}
					else
					{
						SetError(TraduceTexto("ViewMvcNoResult"));
					}
				}
			}
			return null;
		}
	}// NOrdenesTrabajo ends.     
}// LibNegocio.Entidades ends.
