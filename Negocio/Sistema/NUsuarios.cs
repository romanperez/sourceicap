using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Security.Cryptography;
/*Capa Negocio 16/11/2016 05:55:02 p. m.*/
namespace Negocio
{  
    ///<summary>
	 ///Catalogo de usuarios para el inicio de sesion.
	 ///</summary>
    public partial class NUsuarios : NBase
    {
		 protected bool IsDelete;
		 protected const string NO_REGISTRADO = "NUsuarios_NO_REGISTRADO";
		protected const string USUARIO_BAJA = "NUsuarios_NO_REGISTRADO";
		protected const string EMPLEADO_BAJA = "NUsuarios_NO_REGISTRADO";
		protected const string PERFIL_BAJA = "NUsuarios_NO_REGISTRADO";
		public enum eFields {IdUsuario=0,IdPerfil,Usuario,Clave,FechaRegistro,IdEmpleado}
		public NUsuarios(IEntidad entidad): base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DUsuarios usuario = new DUsuarios();
			DPerfiles perfil = new DPerfiles();
			DEmpleados empleado = new DEmpleados();
			DDepartamentos departamento = new DDepartamentos();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();		
			string[] camposJoin = new string[]
			{				
				String.Format("{0}.{1} AS Perfil",perfil.Tabla,NPerfiles.eFields.Nombre.ToString()),
				String.Format("{0} AS NombreEmpleado",new NEmpleados().DataNombreEmpleado()),
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1} as Departamento",departamento.Tabla,NDepartamentos.eFields.Nombre.ToString())
			};
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Perfiles*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,perfil.Tabla,DBase.On, 
				   String.Format("{0}.{1}",usuario.Tabla,NUsuarios.eFields.IdPerfil.ToString()),
					String.Format("{0}.{1}",perfil.Tabla,NPerfiles.eFields.IdPerfil.ToString())),
					/*Join with empleados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,empleado.Tabla,DBase.On, 
				   String.Format("{0}.{1}",usuario.Tabla,NUsuarios.eFields.IdEmpleado.ToString()),
					String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdEmpleado.ToString())),
					/*Join with Departamentos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,departamento.Tabla,DBase.On, 
				   String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdDepartamento.ToString()),
					String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdDepartamento.ToString())),
					/*Join with Unidad*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdUnidad.ToString())),
					/*Join with Empresa*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))

			  };
			usuario.SetConfigJoins(camposJoin, condicionesJoin);
			return usuario;
		}		
		public NUsuarios() { }
		public string ClaveMD5(string clave)
		{			 
			if(String.IsNullOrEmpty(clave)) return String.Empty;
			byte[] encodedPassword = new UTF8Encoding().GetBytes(clave);
			byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
			return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
		}
		public IUsuarios IniciarSesion()
		{
			IUsuarios user = null;
			try
			{
				user = _Entity as IUsuarios;
				InicializaError();
				if(_Entity == null) 
				{
					SetError(TraduceTexto("Sin datos no es posible iniciar sesion."), true);					
					return null;
				}				
				DUsuarios userData = new DUsuarios();
				user = userData.GetUserByLoggin(user);
				if(user == null)
				{
					SetError(TraduceTexto(NO_REGISTRADO), true);
					return null;
				}
				if(!String.IsNullOrEmpty(user.Clave))
				{
					SetError(TraduceTexto(NO_REGISTRADO), true);
				}
				if(!user.Estatus)
				{
					SetError(TraduceTexto(USUARIO_BAJA), true);
				}
				/*if(user.Data.Empleado == null || !user.Data.Empleado.Estatus)
				{
					SetError(TraduceTexto(EMPLEADO_BAJA), true);
				}*/
			}
			catch(Exception eLoggin)
			{
				OnError(eLoggin);
			}
			return user;
		}

		protected bool Valida(bool IsUpdate)
		{
			bool valid = true;
			IUsuarios user = _Entity as IUsuarios;
			if(!IsUpdate || (!String.IsNullOrEmpty(user.Clave) && !String.IsNullOrEmpty(user.ClaveConfirm))) 
			{
				if(user.Clave != user.ClaveConfirm)
				{
					valid = false;
					SetError(TraduceTexto("IUsuariosClavesNoCoinciden"));
				}
				if(!(user.Clave.Length >= 8 && user.Clave.Length <= 16))
				{
					valid = false;
					SetError(TraduceTexto("IUsuariosClaveLength"));
				}
				if(valid)
				{
					ToMD5();
				}
			}
			return valid;
		}
		public override bool ValidaSave()
		{			
			if(!IsDelete)
			{
				return this.Valida(false);
			}
			return true;
		}
		public override bool ValidaUpdate()
		{
			return this.Valida(true);
		}
		public override void Delete()
		{
			IsDelete = true;
			base.Delete();
		}
		protected void ToMD5()
		{
			if(_Entity != null)
			{
				((IUsuarios)_Entity).Clave = this.ClaveMD5(((IUsuarios)_Entity).Clave);
				((IUsuarios)_Entity).ClaveConfirm = this.ClaveMD5(((IUsuarios)_Entity).ClaveConfirm);
			}
		}
		public static IUsuarios GetById(int IdUsuario)
		{
			if(IdUsuario <= 0) return null;
			NUsuarios b = new NUsuarios();
			b.AddWhere(new CPropsWhere()
			{
				Condicion="=",
				Prioridad=true,
				NombreCampo = NUsuarios.eFields.IdUsuario.ToString(),
				Valor=IdUsuario
			});
			IEnumerable<IEntidad> res = b.Buscar();
			if(res != null && res.Count() > 0)
			{
				IUsuarios u = res.FirstOrDefault() as IUsuarios;
				u.Clave = String.Empty;
				u.ClaveConfirm = String.Empty;
				return u;
			}
			return null;
		}
		
   }// NUsuarios ends.        
}// LibNegocio.Entidades ends.
