using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 08/12/2016 05:26:43 p. m.*/
namespace Negocio
{  
    
    public partial class NSisLogErrores : NBase
    {
		 public enum eFields
		 {
			 Log_Error_Id = 0, Log_Fecha, Log_Archivo, Log_Funcion, Log_Linea, Log_Columna, Log_Pila, Log_Error, Log_Info
		 }
    public NSisLogErrores() { }
	public NSisLogErrores(IEntidad entidad)
			: base(entidad)
	{
	}
	protected override IBase BuildDataEntity()
	{
	  return new  DSisLogErrores();
	}				
    }// NSisLogErrores ends.        
}// LibNegocio.Entidades ends.
