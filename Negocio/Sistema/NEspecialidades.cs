using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{

	public partial class NEspecialidades : NBase
	{
		public enum eFields
		{
			IdEspecialidad = 0, IdEmpresa,IdGrado, Codigo, Nombre, Estatus
		}
		public NEspecialidades(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DEspecialidades especialidades = new DEspecialidades();
			DEmpresas empresas = new DEmpresas();
			DTecnicosGrados grado = new DTecnicosGrados();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} as Grado",grado.Tabla,NTecnicosGrados.eFields.Nombre.ToString()),
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresas.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",especialidades.Tabla,NEspecialidades.eFields.IdEmpresa.ToString())),	
					 /*Join with Grados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,grado.Tabla,DBase.On, 
				   String.Format("{0}.{1}",grado.Tabla,NTecnicosGrados.eFields.IdGrado.ToString()),
					String.Format("{0}.{1}",especialidades.Tabla,NEspecialidades.eFields.IdGrado.ToString()))
			  };
			especialidades.SetConfigJoins(camposJoin, condicionesJoin);
			return especialidades;
		}
		public IEnumerable<IPropsWhere> FiltroSoloEspecialidadesActivos(IEnumerable<IPropsWhere> whereOriginal)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NEspecialidades.eFields.Estatus.ToString(),
				Valor = true
			});			
			return base.CombineWithFiltro(where, whereOriginal);
		}
		public IEnumerable<IPropsWhere> FiltroByGradoId(int IdGrado)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NEspecialidades.eFields.IdGrado.ToString(),
				Valor = IdGrado
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NEspecialidades.eFields.Estatus.ToString(),
				Valor = true
			});	
			return where;
		}
		public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
		{
			entities.ToList().ForEach(
			x =>
			{
				_Entity = x;
				this.Save();
			});
			return entities;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IEspecialidades", new DEspecialidades());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;	
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public NEspecialidades()
		{
		}

        public static IEnumerable<IEspecialidades> GetEspecialidadesFromTable(System.Data.DataTable EspecialidadesTbl)
        {
            if (EspecialidadesTbl == null) return null;
            List<IEspecialidades> Especialidades = new List<IEspecialidades>();
            for (int rw = 0; rw < EspecialidadesTbl.Rows.Count; rw++)
            {
                Especialidades.Add(new CEspecialidades()
                {
                    Grado = EspecialidadesTbl.Rows[rw].Field<string>("GRADO"),
                    Codigo = EspecialidadesTbl.Rows[rw].Field<string>("CODIGO"),
                    Nombre = EspecialidadesTbl.Rows[rw].Field<string>("NOMBRE")
                });
            }
            return Especialidades;
        }

       public IEnumerable<IEntidad> SaveMasivo3(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((IEspecialidades)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<IEspecialidades> SaveMasivo2(IEnumerable<IEspecialidades> Especialidades, int IdEmpresa)
        {
            try
            {
                if (Especialidades == null || Especialidades.Count() <= 0) return null;

                DTecnicosGrados dT = new DTecnicosGrados();
                DEmpresas de = new DEmpresas();
                string[] Grados = Especialidades.Where(i => !(String.IsNullOrEmpty(i.Grado))).Select(i => String.Format("'{0}'", i.Grado)).ToArray();
                string InCondicion = String.Empty;
                IEnumerable<ITecnicosGrados> lstGrados = new List<ITecnicosGrados>();
                IEnumerable<IEntidad> Res = null;
                NTecnicosGrados nT = new NTecnicosGrados();

                List<IPropsWhere> where = new List<IPropsWhere>();
                CPropsWhere whereEmpresa = new CPropsWhere();
                whereEmpresa.Condicion = "=";
                whereEmpresa.OmiteConcatenarTabla = true;
                whereEmpresa.Prioridad = true;
                whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
                whereEmpresa.Valor = IdEmpresa;

                CPropsWhere whereCampo = new CPropsWhere();
                whereCampo.Condicion = "in";
                whereCampo.OmiteConcatenarTabla = true;
                whereCampo.Prioridad = true;

                //Buscando Grados
                InCondicion = String.Join(",", Grados);
                whereCampo.NombreCampo = String.Format("{0}.{1}", dT.Tabla, NTecnicosGrados.eFields.Nombre.ToString());
                whereCampo.Valor = InCondicion;
                where = new List<IPropsWhere>();
                where.Add(whereEmpresa);
                where.Add(whereCampo);
                nT.AddWhere(where);
                Res = nT.Buscar();
                if (Res != null && Res.Count() > 0)
                {
                    lstGrados = Res.Select(x => x as ITecnicosGrados);
                }


                Especialidades.ToList().ForEach(i =>
                {
                    var resF = (!String.IsNullOrEmpty(i.Grado)) ? lstGrados.Where(f => f.Nombre.ToLower() == i.Grado.ToLower() && f.IdEmpresa == IdEmpresa) : null;
                  
                    if (resF != null && resF.Count() > 0)
                    {
                        i.IdGrado = resF.FirstOrDefault().IdGrado;
                    }

                });


                Especialidades.ToList().ForEach(i =>
                {
                    i.IdEmpresa = IdEmpresa;
                    i.Estatus = true;
                });
                this.SaveMasivo3(Especialidades);
                Especialidades = Especialidades.Where(i => i.IdEspecialidad <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return Especialidades;
        }

	}// NEspecialidades ends.        
}// LibNegocio.Entidades ends.
