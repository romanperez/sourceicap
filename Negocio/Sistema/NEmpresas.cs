using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 21/12/2016 12:08:29 p. m.*/
namespace Negocio
{

	public partial class NEmpresas : NBase
	{
		
		protected IParametros _ParamDigitos;
		protected IParametros _ParamLetras;
		protected IParametros _ParamSeparador;
		public enum eFields
		{
			IdEmpresa = 0, Empresa, Direccion, Telefono, CorreoElectronico, SitioWeb, RFC, CodigoPostal, Estatus
		}
		public NEmpresas()
		{
		}
		public NEmpresas(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DEmpresas();
		}
		#region RFC
		protected bool Existe(string buscar, string buscaren)
		{
			char separador = _ParamSeparador.Valor.Trim()[0];
			int coincidencias = buscaren.Split(separador).Where(letra => letra == buscar).Count();
			if(coincidencias > 0) return true;
			else return false;
		}
		protected bool ValidaMoral12(string rfc)
		{
			string first = rfc.Substring(0, 3); // 3 letras 
			string second = rfc.Substring(3, 6);//6 digitos
			string third = rfc.Substring(6, 3);//3 alfanumericos
			bool valido = true;

			first.ToList().ForEach(c => valido = (Existe(c.ToString(), _ParamLetras.Valor) ? true : false));
			if(!valido) return false;

			second.ToList().ForEach(c => valido = (Existe(c.ToString(), _ParamDigitos.Valor) ? true : false));
			if(!valido) return false;

			third.ToList().ForEach(c => valido = (Existe(c.ToString(), _ParamLetras.Valor) || Existe(c.ToString(), _ParamDigitos.Valor) ? true : false));
			if(!valido) return false;

			return true;
		}
		protected bool ValidaFisica13(string rfc)
		{
			string first = rfc.Substring(0, 4); // 4 letras 
			string second = rfc.Substring(4, 6);//6 digitos
			string third = rfc.Substring(10, 3);//3 alfanumericos
			bool valido = true;

			first.ToList().ForEach(c => valido = (Existe(c.ToString(), _ParamLetras.Valor) ? true : false));
			if(!valido) return false;

			second.ToList().ForEach(c => valido = (Existe(c.ToString(), _ParamDigitos.Valor) ? true : false));
			if(!valido) return false;

			third.ToList().ForEach(c => valido = (Existe(c.ToString(), _ParamLetras.Valor) || Existe(c.ToString(), _ParamDigitos.Valor) ? true : false));
			if(!valido) return false;

			return true;
		}
		protected void LoadParametros()
		{
			IEnumerable<IParametros> _param = null;
			NParametros p = new NParametros();					
			p.AddWhere(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "contenga",
				NombreCampo = NParametros.eFields.Nombre.ToString(),
				Valor = "RFC_"
			});

			_param = p.Buscar().Select(r => (IParametros)r);
			if(_param != null && _param.Count() > 2)
			{
				_ParamDigitos = _param.Where(r => r.Nombre == "RFC_DIGITOS").First();
				_ParamLetras = _param.Where(r => r.Nombre == "RFC_LETRAS").First();
				_ParamSeparador = _param.Where(r => r.Nombre == "RFC_SEPARADOR").First();
			}
		}
		protected bool ValidaDatos()
		{
			if(_data.Delete == 1) return true;
			bool validos = true;
			IEmpresas empresa = _Entity as IEmpresas;
			validos = ValidaRfc(empresa.RFC);
			/*LoadParametros();
			if(_ParamDigitos == null || _ParamLetras == null || _ParamSeparador == null)
			{
				SetError(TraduceTexto("NEmpresaParametrosRFC"));
				return false;
			}
			empresa.RFC = empresa.RFC.ToLower();
			if(String.IsNullOrEmpty(empresa.RFC))
			{
				SetError(TraduceTexto("NEmpresaRFCNoValido"));
				return false;
			}

			if(empresa.RFC.Length != CEmpresas.LEN_MORAL && empresa.RFC.Length != CEmpresas.LEN_FISICA)
			{
				SetError(TraduceTexto("NEmpresaRFCNoValido"));
				return false;
			}
			_ParamLetras.Valor = _ParamLetras.Valor.ToString();
			if(empresa.RFC.Length == CEmpresas.LEN_MORAL)
			{
				if(!ValidaMoral12())
				{
					SetError(TraduceTexto("NEmpresaRFCNoValido"));
					return false;
				}
			}
			if(empresa.RFC.Length == CEmpresas.LEN_FISICA)
			{
				if(!ValidaFisica13())
				{
					SetError(TraduceTexto("NEmpresaRFCNoValido"));
					return false;
				}
			}
			empresa.RFC = empresa.RFC.ToUpper();*/
			return validos;
		}
		public bool ValidaRfc(string rfc)
		{
			if(String.IsNullOrEmpty(rfc))
			{
				SetError(TraduceTexto("NEmpresaRFCNoValido"));
				return false;
			}
			LoadParametros();
			if(_ParamDigitos == null || _ParamLetras == null || _ParamSeparador == null)
			{
				SetError(TraduceTexto("NEmpresaParametrosRFC"));
				return false;
			}
			rfc = rfc.ToLower();

			if(rfc.Length != CEmpresas.LEN_MORAL && rfc.Length != CEmpresas.LEN_FISICA)
			{
				SetError(TraduceTexto("NEmpresaRFCNoValido"));
				return false;
			}
			_ParamLetras.Valor = _ParamLetras.Valor.ToString();
			if(rfc.Length == CEmpresas.LEN_MORAL)
			{
				if(!ValidaMoral12(rfc))
				{
					SetError(TraduceTexto("NEmpresaRFCNoValido"));
					return false;
				}
			}
			if(rfc.Length == CEmpresas.LEN_FISICA)
			{
				if(!ValidaFisica13(rfc))
				{
					SetError(TraduceTexto("NEmpresaRFCNoValido"));
					return false;
				}
			}
			return true;
		}
		public override bool ValidaUpdate()
		{
			return ValidaDatos();
		}
		public override bool ValidaSave()
		{
			return ValidaDatos();
		}
		#endregion
	}// NEmpresas ends.        
}// LibNegocio.Entidades ends.
