using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 04/01/2017 10:37:33 a. m.*/
namespace Negocio
{

	public partial class NAcciones : NBase
	{
		public enum eFields
		{
			IdAccion = 0, AccionNombre, ParametrosJs, ParametrosMvc, Html, Estatus
		}
		public NAcciones()
		{
		}
		public NAcciones(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DAcciones();
		}
		public IAcciones FindByName(string name)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Condicion="=",
				NombreCampo=NAcciones.eFields.Estatus.ToString(),
				Valor=true,
				Prioridad=true			
			});
			where.Add(new CPropsWhere()
			{
				Condicion = "=",
				NombreCampo = NAcciones.eFields.AccionNombre.ToString(),
				Valor = name,
				Prioridad = true
			});
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<IEntidad> res = this.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IAcciones;
			}
			return null;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IAcciones", new DAcciones());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
	}// NAcciones ends.        
}// LibNegocio.Entidades ends.
