using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 10/04/2017 12:10:57 p. m.*/
namespace Negocio
{

	public partial class NClasificacionConceptos : NBase
	{
		public enum eFields
		{
			IdConceptoClasificacion = 0, IdEmpresa, Clasificacion, Descripicion
		}
		public NClasificacionConceptos()
		{
		}
		public NClasificacionConceptos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DClasificacionConceptos clasificacion = new DClasificacionConceptos();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",clasificacion.Tabla,NClasificacionConceptos.eFields.IdEmpresa.ToString()))
			};
			clasificacion.SetConfigJoins(camposJoin, condicionesJoin);
			return clasificacion;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IClasificacionConceptos", new DClasificacionConceptos());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}


        public static IEnumerable<IClasificacionConceptos> GetClasificacionConceptoFromTable(System.Data.DataTable ClasificacionConceptoTbl)
        {
            if (ClasificacionConceptoTbl == null) return null;
            List<IClasificacionConceptos> Clasificacion = new List<IClasificacionConceptos>();
            for (int rw = 0; rw < ClasificacionConceptoTbl.Rows.Count; rw++)
            {
                Clasificacion.Add(new CClasificacionConceptos()
                {
                    Clasificacion = ClasificacionConceptoTbl.Rows[rw].Field<string>("CLASIFICACION"),
                    Descripicion = ClasificacionConceptoTbl.Rows[rw].Field<string>("DESCRIPCION")
                });
            }
            return Clasificacion;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((IClasificacionConceptos)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<IClasificacionConceptos> SaveMasivo2(IEnumerable<IClasificacionConceptos> ClasificacionConcepto, int IdEmpresa)
        {
            try
            {
                if (ClasificacionConcepto == null || ClasificacionConcepto.Count() <= 0) return null;


                ClasificacionConcepto.ToList().ForEach(i =>
                {
                    i.IdEmpresa = IdEmpresa;
                });
                this.SaveMasivo(ClasificacionConcepto);
                ClasificacionConcepto = ClasificacionConcepto.Where(i => i.IdConceptoClasificacion <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return ClasificacionConcepto;
        }
	}// NClasificacionConceptos ends.        
}// LibNegocio.Entidades ends.
