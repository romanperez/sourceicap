using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 06/04/2017 03:52:28 p. m.*/
namespace Negocio
{

	public partial class NConversiones : NBase
	{
		public enum eFields
		{
			IdConversion = 0, IdMonedaOrigen, IdMonedaDestino, Operador
		}
		public NConversiones()
		{
		}
		public NConversiones(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DMonedas moneda = new DMonedas();
			DConversiones conversion = new DConversiones();
			string[] camposJoin = new string[]
			  {												
				String.Format("M1.{0} AS MonedaOrigen",NMonedas.eFields.Moneda.ToString()),
				String.Format("M2.{0} AS MonedaDestino",NMonedas.eFields.Moneda.ToString()),
				String.Format("M1.{0} AS CodigoMonedaOrigen",NMonedas.eFields.Codigo.ToString()),
				String.Format("M2.{0} AS CodigoMonedaDestino",NMonedas.eFields.Codigo.ToString())				
			  };
			string[] condicionesJoin = new string[] 
			  {
				/*Join with Monedas*/
					String.Format("{0} {1}  M1 {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("M1.{0}",NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",conversion.Tabla,NConversiones.eFields.IdMonedaOrigen.ToString())),
					/*Join with Monedas*/
					String.Format("{0} {1}  M2 {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("M2.{0}",NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",conversion.Tabla,NTipoCambios.eFields.IdMonedaDestino.ToString()))
					  };
			conversion.SetConfigJoins(camposJoin, condicionesJoin);
			return conversion;
		}
		public ITipoCambios ConvierteCantidad(ITipoCambios tipoCambio)
		{
			double cantidad = tipoCambio.Cantidad;
			//NTipoCambios cambio = new NTipoCambios();
			int IdMonedaOrigen = tipoCambio.IdMonedaOrigen;
			int IdMonedaDestino = tipoCambio.IdMonedaDestino;
			//tipoCambio.IdMonedaOrigen = IdMonedaDestino;
			//tipoCambio.IdMonedaDestino = IdMonedaOrigen;
			//int id = cambio.GetIdValorCambio(tipoCambio,tipoCambio.FechaInicio);			
			if(tipoCambio.IdTipoCambio <= 0 || tipoCambio == null)
			{
				SetError(TraduceTexto("NCotizacionesNoTipoCambioValido"), true);
				return null;
			}
			tipoCambio = NTipoCambios.GetById(tipoCambio.IdTipoCambio);
			tipoCambio.Cantidad = ConvierteCantidad(IdMonedaOrigen, IdMonedaDestino, cantidad, tipoCambio.Valor);
			return tipoCambio;
		}
		public double ConvierteCantidad(int IdMonedaOrigen, int IdMonedaDestino, double cantidad, double tipocambio)
		{
			if(_data == null) return 0;
			return _data.fnConvierteCantidad(IdMonedaOrigen, IdMonedaDestino, cantidad, tipocambio);			
		}
		public IConversiones GetById(int IdMonedaOrigen, int IdMonedaDestino)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NConversiones.eFields.IdMonedaOrigen.ToString(),
				Valor = IdMonedaOrigen
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NConversiones.eFields.IdMonedaDestino.ToString(),
				Operador = DBase.AND,
				Valor = IdMonedaDestino
			});
			NConversiones conversion = new NConversiones();
			conversion.AddWhere(where);
			IEnumerable<IEntidad> res= conversion.Buscar();
			if(res.Any())
			{
				return res.First() as IConversiones;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// Obtiene el registro de la moneda Correspondiente a PESOS MEXICANOS MXN.
		/// </summary>
		/// <param name="IdEmpresa">Identificador de la empresa.</param>
		/// <returns>Registro IMonedas [Pesos Mexicanos]</returns>
		public  static IMonedas GetMonedaPesos(int IdEmpresa)
		{
			DConversiones d = new DConversiones();
			int id = d.fnGetIdPeso(IdEmpresa);
			NMonedas moneda = new NMonedas();
			return moneda.GetById(id);
		}
	}// NConversiones ends.        
}// LibNegocio.Entidades ends.
