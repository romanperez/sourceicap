using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NOTConceptos : NBase
	{
		public enum eFields
		{
			IdOTConcepto = 0, IdOrdenTrabajo, IdContratosDetalles, IdConcepto, Cantidad
		}
		public NOTConceptos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DOTConceptos otconcepto = new DOTConceptos();
			DContratosDetalles2 detalle = new DContratosDetalles2();
			DConceptos concepto = new DConceptos();
			DUnidadesMedida medida = new DUnidadesMedida();
			DClasificacionConceptos clasificacion = new DClasificacionConceptos();
			string[] camposJoin = new string[]
				  {																	
					  String.Format("{0}.{1}",detalle.Tabla,NContratosDetalles2.eFields.Cantidad.ToString()),
					  String.Format("{0}.{1}",concepto.Tabla,NConceptos.eFields.Concepto.ToString()),
					  String.Format("{0}.{1}",concepto.Tabla,NConceptos.eFields.Descripcion.ToString()),
					  String.Format("{0}.{1} as UnidadMedida",medida.Tabla,NUnidades.eFields.Unidad.ToString()),
					  String.Format("{0}.{1}",clasificacion.Tabla,NClasificacionConceptos.eFields.Clasificacion.ToString()),
					  String.Format("{0}.{1}",clasificacion.Tabla,NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString())
				  };
			string[] condicionesJoin = new string[] 
				  { 								  
					  /*Join with ContratosDetalles2*/						
					   String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,detalle.Tabla,DBase.On, 
					   String.Format("{0}.{1}",detalle.Tabla,NContratosDetalles2.eFields.IdContratosDetalles.ToString()),
					   String.Format("{0}.{1}",otconcepto.Tabla,NOTConceptos.eFields.IdContratosDetalles.ToString())),
						/*Join with Conceptos*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,concepto.Tabla,DBase.On, 
						String.Format("{0}.{1}",concepto.Tabla,NConceptos.eFields.IdConcepto.ToString()),
						String.Format("{0}.{1}",otconcepto.Tabla,NOTConceptos.eFields.IdConcepto.ToString()))	,
						/*Join with UnidadesMedida*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,medida.Tabla,DBase.On, 
						String.Format("{0}.{1}",medida.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
						String.Format("{0}.{1}",concepto.Tabla,NConceptos.eFields.IdUnidadMedida.ToString())),
						/*Join with ClasificacionConceptos*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,clasificacion.Tabla,DBase.On, 
						String.Format("{0}.{1}",clasificacion.Tabla,NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString()),
						String.Format("{0}.{1}",concepto.Tabla,NConceptos.eFields.IdConceptoClasificacion.ToString()))
				  };
			otconcepto.SetConfigJoins(camposJoin, condicionesJoin);
			return otconcepto;
		}
		public NOTConceptos()
		{
		}
		public static IOTConceptos GetById(int id)
		{
			NOTConceptos c = new NOTConceptos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NOTConceptos.eFields.IdOTConcepto.ToString(),
				Valor = id
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IOTConceptos;
			}
			return null;
		}
		public static IEnumerable<IOTConceptos> GetByIdOrdenTrabajo(int IdOrdenTrabajo)
		{
			NOTConceptos c = new NOTConceptos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NOTConceptos.eFields.IdOrdenTrabajo.ToString(),
				Valor = IdOrdenTrabajo
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x=> x as IOTConceptos);
			}
			return null;
		}		
	}// NOTConceptos ends.        
}// LibNegocio.Entidades ends.
