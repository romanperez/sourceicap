using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{

	public partial class NClientesPersonal : NBase
	{
		public enum eFields
		{
			IdPersonalCliente = 0, IdCliente, Nombre, ApellidoPaterno, ApellidoMaterno, Puesto, Telefono, Estatus
		}
		public NClientesPersonal(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DClientesPersonal clientePersonal = new DClientesPersonal();
			DClientes cliente = new DClientes();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();

			string[] camposJoin = new string[]
			{				
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1} ",cliente.Tabla,NClientes.eFields.RazonSocial.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
			   /*Join with clientes*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cliente.Tabla,DBase.On, 
				String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdCliente.ToString()),
				String.Format("{0}.{1}",clientePersonal.Tabla,NClientesPersonal.eFields.IdCliente.ToString())),
				/*Join with Unidades*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				String.Format("{0}.{1}",unidad.Tabla,NClientes.eFields.IdUnidad.ToString()),
				String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdUnidad.ToString())),
				/*Join with Empresas*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			};
			clientePersonal.SetConfigJoins(camposJoin, condicionesJoin);
			return clientePersonal;
		}
		public NClientesPersonal()
		{
		}
		public static IEnumerable<IOTPersonalCliente> FindByiDOrdenTrabajo(int IdOrden)
		{
			COTPersonalCliente cp = new COTPersonalCliente();
			cp.IdOrdenTrabajo = IdOrden;
			NOTPersonalCliente p = new NOTPersonalCliente();
			p.AddWhere(NOTPersonalCliente.eFields.IdOrdenTrabajo.ToString(), "=");
			IEnumerable<IEntidad> res= p.Buscar();
			if(res.Any())
			{
				return res.Select(x => x as IOTPersonalCliente);
			}
			return null;
		}

        public static IEnumerable<IClientesPersonal> GetClientesPersonalFromTable(System.Data.DataTable ClientesPersonalTbl)
        {
            if (ClientesPersonalTbl == null) return null;
            List<IClientesPersonal> ClientesPersonal = new List<IClientesPersonal>();
            for (int rw = 0; rw < ClientesPersonalTbl.Rows.Count; rw++)
            {
                ClientesPersonal.Add(new CClientesPersonal()
                {   
                    Unidad = ClientesPersonalTbl.Rows[rw].Field<string>("UNIDAD"),
                    RazonSocial = ClientesPersonalTbl.Rows[rw].Field<string>("CLIENTE"),
                    Puesto = ClientesPersonalTbl.Rows[rw].Field<string>("PUESTO"),
                    Nombre = ClientesPersonalTbl.Rows[rw].Field<string>("NOMBRE"),
                    ApellidoPaterno = ClientesPersonalTbl.Rows[rw].Field<string>("APELLIDO_PATERNO"),
                    ApellidoMaterno = ClientesPersonalTbl.Rows[rw].Field<string>("APELLIDO_MATERNO"),
                    Telefono = ClientesPersonalTbl.Rows[rw].Field<string>("TELEFONO")
                });
            }
            return ClientesPersonal;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((IClientesPersonal)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<IClientesPersonal> SaveMasivo2(IEnumerable<IClientesPersonal> ClientesPersonal, int IdEmpresa)
        {
            try
            {
                if (ClientesPersonal == null || ClientesPersonal.Count() <= 0) return null;

                DUnidades du = new DUnidades();
                DClientes dC = new DClientes();
                DEmpresas de = new DEmpresas();
                string[] Unidades = ClientesPersonal.Where(i => !(String.IsNullOrEmpty(i.Unidad))).Select(i => String.Format("'{0}'", i.Unidad)).ToArray();
                string[] Clientes = ClientesPersonal.Where(i => !(String.IsNullOrEmpty(i.RazonSocial))).Select(i => String.Format("'{0}'", i.RazonSocial)).ToArray();
                string InCondicion = String.Empty;
                IEnumerable<IUnidades> lstUnidades = new List<IUnidades>();
                IEnumerable<IClientes> lstClientes = new List<IClientes>();
                IEnumerable<IEntidad> Res = null;
                NUnidades nU = new NUnidades();
                NClientes nC = new NClientes();
                

                List<IPropsWhere> where = new List<IPropsWhere>();
                CPropsWhere whereEmpresa = new CPropsWhere();
                whereEmpresa.Condicion = "=";
                whereEmpresa.OmiteConcatenarTabla = true;
                whereEmpresa.Prioridad = true;
                whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
                whereEmpresa.Valor = IdEmpresa;

                CPropsWhere whereCampo = new CPropsWhere();
                whereCampo.Condicion = "in";
                whereCampo.OmiteConcatenarTabla = true;
                whereCampo.Prioridad = true;

                //Buscando Unidades
                InCondicion = String.Join(",", Unidades);
                whereCampo.NombreCampo = String.Format("{0}.{1}", du.Tabla, NUnidades.eFields.Unidad.ToString());
                whereCampo.Valor = InCondicion;
                where = new List<IPropsWhere>();
                where.Add(whereEmpresa);
                where.Add(whereCampo);
                nU.AddWhere(where);
                Res = nU.Buscar();
                if (Res != null && Res.Count() > 0)
                {
                    lstUnidades = Res.Select(x => x as IUnidades);
                }

                //Buscando Clientes
                InCondicion = String.Join(",", Clientes);
                whereCampo.NombreCampo = String.Format("{0}.{1}", dC.Tabla, NClientes.eFields.RazonSocial.ToString());
                whereCampo.Valor = InCondicion;
                where = new List<IPropsWhere>();
                where.Add(whereEmpresa);
                where.Add(whereCampo);
                nC.AddWhere(where);
                Res = nC.Buscar();
                if (Res != null && Res.Count() > 0)
                {
                    lstClientes = Res.Select(x => x as IClientes);
                }


                ClientesPersonal.ToList().ForEach(i =>
                {
                    var resC = (!String.IsNullOrEmpty(i.RazonSocial)) ? lstClientes.Where(f => f.Unidad.ToLower() == i.Unidad.ToLower() && f.RazonSocial.ToLower() == i.RazonSocial.ToLower()) : null;

                    if (resC != null && resC.Count() > 0)
                    {
                        i.IdCliente = resC.FirstOrDefault().IdCliente;
                    }

                });

                ClientesPersonal.ToList().ForEach(i =>
                {
                    i.Estatus = true;
                });
                this.SaveMasivo(ClientesPersonal);
                ClientesPersonal = ClientesPersonal.Where(i => i.IdCliente <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return ClientesPersonal;
        }
	}// NClientesPersonal ends.        
}// LibNegocio.Entidades ends.
