using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{
	public partial class NContratosDetalles : NBase
	{
		public enum eFields
		{
			IdContratoDetalle = 0, Estatus, IdContrato, IdConcepto, Partida, Precio
		}
		public NContratosDetalles(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{

			DContratosDetalles contratoDetalles = new DContratosDetalles();
			DConceptos conceptos = new DConceptos();
			DCapitulos capitulo = new DCapitulos();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.Capitulo.ToString()),
				String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.Concepto.ToString()),
				String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.Descripcion.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Conceptos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,conceptos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdConcepto.ToString()),
					String.Format("{0}.{1}",contratoDetalles.Tabla,NCotizacionesDetalles.eFields.IdConcepto.ToString()))	,
					/*Join with Capitulos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,capitulo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.IdCapitulo.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdCapitulo.ToString()))
			  };
			contratoDetalles.SetConfigJoins(camposJoin, condicionesJoin);
			return contratoDetalles;
		}
		public NContratosDetalles()
		{
		}
		public IEnumerable<IContratosDetalles> GetDetalleContratoById(int idContrato)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			DConceptosInsumos entidad = new DConceptosInsumos();
			where.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NContratosDetalles.eFields.IdContrato.ToString(),
				Valor = idContrato
			});
			this.AddWhere(where);
			IEnumerable<IEntidad> res = this.Buscar();
			return (res != null && res.Count() >0)? res.Select(x=>x as IContratosDetalles): null;
		}
		public IEnumerable<IContratosDetalles> ToContratoDetalle(IEnumerable<ICotizacionesDetalles> detalles)
		{
			if(detalles == null || detalles.Count() <= 0) return null;
			return from tbl in detalles
					 select new CContratosDetalles()
					 {
						 Concepto = tbl.Concepto,
						 Descripcion = tbl.Descripcion,
						 IdConcepto = tbl.IdConcepto,
						 IdContrato = 0,
						 IdContratoDetalle = 0,
						 Partida = 0,
						 Precio = tbl.Precio
					 };
		}
	}// NContratosDetalles ends.        
}// LibNegocio.Entidades ends.
