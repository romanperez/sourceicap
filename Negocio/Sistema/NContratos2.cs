using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/03/2017 03:46:55 p. m.*/
namespace Negocio
{

	public partial class NContratos2 : NBase
	{

		public enum eFields
		{
			IdContrato = 0, IdPedido, SubTotal, Total, PorcentajeDescuento, DescuentoMonto, IdEmpresa, IdCliente, IdUnidad, IdIva, IdTipoCambio, IdMoneda, Fecha, Contrato
		}
		public NContratos2()
		{
		}
		public NContratos2(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DContratos2 contrato = new DContratos2();
			DEmpresas empresa = new DEmpresas();
			DUnidades unidad = new DUnidades();
			string[] camposJoin = new string[] { };
			string[] condicionesJoin = new string[] 
			{ 
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",contrato.Tabla,NDepartamentos.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),

            String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			};
			contrato.SetConfigJoins(camposJoin, condicionesJoin);
			return contrato;
		}
	}// NContratos2 ends.        
}// LibNegocio.Entidades ends.
