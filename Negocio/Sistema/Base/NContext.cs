﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
namespace Negocio
{
    public class NContext<Negocio> : IContext where Negocio : INegocio, new()
    {        
        protected Negocio _Negocio;		 
        public NContext()
        {
            _Negocio = new Negocio();
        }
        public NContext(Negocio negocio)
        {
            _Negocio = negocio;
        }
        public virtual void New(IEntidad Entity)
        {
            try
            {
                if (Entity != null)
                {
                    _Negocio.SetEntity(Entity);
                    _Negocio.Save();                    
                }
                else
                {
                    _Negocio.SetError(_Negocio.TraduceTexto("NContextEntidadVacia"));
                }
            }
            catch (Exception e)
            {
                OnError(e);
            }            
        }
		  public virtual IEnumerable<IEntidad> New(IEnumerable<IEntidad> Entities)
		  {
			  IEnumerable<IEntidad> resp = null;
			  try
			  {
				  if(Entities != null)
				  {					 
					  resp = _Negocio.SaveMasivo(Entities);
				  }
				  else
				  {
					  _Negocio.SetError(_Negocio.TraduceTexto("NContextEntidadVacia"));
				  }
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return resp;
		  }
        public virtual void Put(int id, IEntidad Entity)
        {
            try
            {
                if (id > 0 && Entity != null)
                {
                    _Negocio.SetEntity(Entity);
                    _Negocio.Update();                    
                }
                else
                {
                    _Negocio.SetError(_Negocio.TraduceTexto("NegocioNContextIdIncorecto"));
                }
            }
            catch (Exception e)
            {
                OnError(e);
            }           
        }
        public virtual void Delete(string key, int id)
        {
            try
            {
                if (id > 0 && !String.IsNullOrEmpty(key))
                {
                    _Negocio.SetEntity(this.GetById(key, id));
                    _Negocio.Delete();                    
                }
                else
                {
                    _Negocio.SetError(_Negocio.TraduceTexto("NegocioNContextIdIncorecto"));
                }
            }
            catch (Exception e)
            {
                OnError(e);
            }                        
        }
		  public virtual IEntidad BuscarScalar()
		  {
			  IEntidad Result = null;
			  try
			  {
				  Result = _Negocio.BuscarScalar();
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return Result;
		  }
        public virtual IEnumerable<IEntidad> Buscar()
        {
            IEnumerable<IEntidad> Result = null;
            try
            {
                Result = _Negocio.Buscar();
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return Result;
        }
		  public virtual IEnumerable<IEntidad> Buscar(int top)
		  {
			  IEnumerable<IEntidad> Result = null;
			  try
			  {
				  Result = _Negocio.Buscar(top);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return Result;
		  }
        public virtual IPagina<Entity> Buscar<Entity>(int pagina, string key) where Entity : IEntidad, new()
        {            
            return this.Buscar<Entity>(pagina,0,key);
        }
        public virtual IPagina<Entity> Buscar<Entity>(int pagina, int registroPP, string key) where Entity : IEntidad, new()
        {
            IPagina<Entity> Result = null;
            try
            {
                Result = _Negocio.Buscar<Entity>(key, pagina,registroPP);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return Result;
        }
        public virtual void Where(IEnumerable<IPropsWhere> where)
        {
            _Negocio.AddWhere(where);
        }
        public virtual IEntidad GetById(string keyId, int id)
        {
            IEntidad Entity=null;
            try
            {
                if (id > 0 && !String.IsNullOrEmpty(keyId))
                {
                    IPropsWhere where = new CPropsWhere()
                    {
                        Prioridad = true,
                        Condicion = "=",
                        NombreCampo = keyId,
                        Valor = id
                    };
                    _Negocio.AddWhere(where);
                    IEnumerable<IEntidad> Result= _Negocio.Buscar();
                    if (Result != null && Result.Count() > 0)
                    {
                        Entity = Result.First();
                    }
                    else
                    { _Negocio.SetError(_Negocio.TraduceTexto("NegocioNContextRegistroNoExiste")); }
                }
                else
                {
                    _Negocio.SetError(_Negocio.TraduceTexto("NegocioNContextIdIncorecto"));
                }
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return Entity;
        }
        public virtual bool HasError()
        {
            return _Negocio.HasError();
        }
        public virtual IError GetError()
        {
            return _Negocio.GetErrorDetail();
        }
		  #region Filtros
		  public IPropsWhere FiltroEmpresa(int IdEmpresa, string Campo)
		  {
			  return FiltroEmpresa(IdEmpresa, "=", Campo);
		  }
		  public IPropsWhere FiltroEmpresa(int IdEmpresa, string condicion, string Campo,bool OmiteConcatenarTabla)
		  {
			  if(IdEmpresa <= 0) return null;
			  IPropsWhere where = new CPropsWhere();
			  where.OmiteConcatenarTabla = OmiteConcatenarTabla;
			  where.Prioridad = true;
			  where.Condicion = condicion;
			  where.NombreCampo = Campo;
			  where.Valor = IdEmpresa;
			  return where;
		  }
		  public IPropsWhere FiltroEmpresa(int IdEmpresa,string condicion,string Campo)
		  {
			  return FiltroEmpresa(IdEmpresa, condicion, Campo, false);
		  }
		  public IEnumerable<IPropsWhere> Filtros(params IPropsWhere[] where)
		  {
			  if(where == null || where.Count() <= 0) return null;
			  List<IPropsWhere> _where=new List<IPropsWhere>();
			  foreach(IPropsWhere subwhere in where)
			  {
				  if(subwhere != null)
				  {
					  _where.Add(subwhere);
				  }
			  }			  
			  return _where;
		  }
		  public IEnumerable<IPropsWhere> Filtros(IEnumerable<IPropsWhere> where1, IEnumerable<IPropsWhere> where2)
		  {
			  return _Negocio.CombineWithFiltro(where1, where2);
		  }
		  #endregion
		  #region ManejoError
		  protected void OnError(Exception error)
        {
            _Negocio.OnError(error);
        }
        protected void OnError(string error)
        {
            this.OnError(new Exception(error));
        }
		  public virtual INegocio GetNegocio()
		  {
			  return _Negocio;
		  }

		/*public byte[]  ExportExcel(IExcel excelConfig, IEnumerable<IEntidad> Data)
		{
			try
			{				
            return excelConfig.MainExcel(Data);
			}
			catch(Exception eExcel)
			{
				OnError(eExcel);
			}
			return null;
		}*/
#endregion		 
		 #region Permisos
		  public virtual bool HasPermiso(string permiso,int Perfil, int Menu)
		  {
			  bool haspermiso = false;
			  try
			  {
				  NPerfilAcciones Findpermiso = new NPerfilAcciones();
				  return Findpermiso.HasPermiso(permiso, Perfil, Menu);
			  }
			  catch(Exception ePermiso)
			  {
				  OnError(ePermiso);
			  }
			  return haspermiso;
		  }
		 #endregion
	 }//class NContext ends.
}//namespace Negocio ends.
