﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Entidades;
using System.Reflection;
using System.Data.SqlClient;
using System.ComponentModel;
using Negocio;
using Datos;
using System.IO;
using PdfSharp;
using PdfSharp.Pdf;
using System.Net.Mail;
namespace Negocio
{
	public enum ETipoReporte
	{
		Excel=1,Pdf
	}
	public abstract class NBase:INegocio
	{		
		protected StringBuilder DuplicatedInfo;
		protected IBase _data;
		protected IEntidad _Entity;
		protected abstract IBase BuildDataEntity();
		public virtual IBase GetBuildDataEntity()
		{
			return BuildDataEntity();
		}
		public NBase()
		{
			_data = BuildDataEntity();
			_Entity = BuildEntity();
		}
		public NBase(IEntidad entidad)
		{
			_data = BuildDataEntity();
			_Entity = (entidad == null)? BuildEntity():entidad;
		}
		public void ClearEntity()
		{
			_Entity = null;
		}
		public void SetEntity(IEntidad Entity)
		{
			_Entity = Entity;
		}
		#region MASIVO
		public virtual IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
		{
			entities.ToList().ForEach(
			x =>
			{
				_Entity = x;
				this.Save();
			});
			return entities;
		}

		#endregion

		#region MANEJO_ERROR
		public virtual void ClearError()
		{
			if(_data == null)
			{
				_data = BuildDataEntity();
			}
			_data.ClearError();
		}
		public virtual void InicializaError()
		{
			if(_data == null)
			{
				_data = BuildDataEntity();
			}
			_data.InicializaError();
		}
        /// <summary>
        /// Agrega un error perosonalizado pero no se envia hacia la base de datos, sobreescribe mensajes de error previos.
        /// </summary>
        /// <param name="error">Error que queremos mostrar.</param>
		public virtual void SetError(string error)
		{			
			if(_data == null)
			{
				_data = BuildDataEntity();
			}
			_data.SetError(error);
			
		}
        /// <summary>
        /// Agrega un error perosonalizado pero no se envia hacia la base de datos.
        /// </summary>
        /// <param name="error">Error que queremos mostrar.</param>
        /// <param name="append">indicar true si queremos conservar errores anteriores.</param>
		public virtual void SetError(string error, bool append)
		{
			if(_data == null)
			{
				_data = BuildDataEntity();
			}
			_data.SetError(error,append);

		}
		public virtual void OnError(Exception eInterno)
		{
			try
			{
				if(_data == null)
				{
					_data = BuildDataEntity();
				}
				_data.OnError(eInterno);
			}
			catch(Exception erfatal)
			{
				throw new Exception(String.Format("Error fatal {0} {1}", eInterno.Message, erfatal.Message));
			}
		}
		public virtual string GetErrorMessage()
		{
			bool haserror = HasError();
			if(haserror)
			{
				return GetErrorDetail().Error;
			}
			return String.Empty;
		}
		public virtual IError GetErrorDetail()
		{
			if(_data != null)
			{
				return _data.GetError();				
			}
			return null;
		}
		public virtual bool HasError()
		{
			IError e = GetErrorDetail();
			if(e != null)
			{
				if(e.IdError>0 || !String.IsNullOrEmpty(e.Error))
				{
					return true;
				}
			}			
			return false;
		}		
#endregion
#region WHERE
		public virtual void AddWhere(string Nombre, string Condicion)
		{
			if(_data == null) return;
			_data.AddWhere(Nombre, Condicion);
		}
		public virtual void AddWhere(string Nombre, string condicion, string Valor)
		{
			if(_data == null) return;
			_data.AddWhere(Nombre, condicion, Valor);
		}
		public virtual void AddWhere(IPropsWhere propiedad)
		{
			if(_data == null) return;
			_data.AddWhere(propiedad);
		}
		public virtual void AddWhere(IEnumerable<IPropsWhere> propiedades)
		{
			if(_data == null) return;
			_data.AddWhere(propiedades.ToList());
		}
		public virtual void ClearWhere()
		{
			_data.ClearWhere();
		}
		public virtual  IEnumerable<IPropsWhere> FiltroActivos(string campo,IEnumerable<IPropsWhere> whereOriginal)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = campo,
				Valor = true
			});
			return this.CombineWithFiltro(where, whereOriginal);
		}
		public virtual IEnumerable<IPropsWhere> CombineWithFiltro(IEnumerable<IPropsWhere> filtro, IEnumerable<IPropsWhere> where)
		{
			List<IPropsWhere> newWhere = filtro.Concat(where).ToList();
			for(int c = 0; c < newWhere.Count(); c++)
			{
				if(c < newWhere.Count() - 1)
				{
					newWhere[c].Operador = DBase.AND;
				}
			}
			return newWhere;
		}
		public virtual IEnumerable<IPropsWhere> FiltrosEmpresaUnidad(int idEmpresa, int idUnidad)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();					
			where.Add(FiltrosEmpresa(idEmpresa));
			where.Add(FiltrosUnidad(idUnidad));
			return where;
		}
		public virtual IPropsWhere FiltrosEmpresa(int idEmpresa)
		{
			IPropsWhere where = new CPropsWhere();
			DEmpresas empresa = new DEmpresas();		
			string campoEmpresa = String.Format("{0}.{1}", empresa.Tabla, NEmpresas.eFields.IdEmpresa);			
			where.OmiteConcatenarTabla = true;
			where.Prioridad = true;
			where.Condicion = "=";
			where.NombreCampo = campoEmpresa;
			where.Valor = idEmpresa;
			return where;
		}
		public virtual IPropsWhere FiltrosUnidad(int idUnidad)
		{
			IPropsWhere where = new CPropsWhere();
			DUnidades unidad = new DUnidades();
			string campoUnidad = String.Format("{0}.{1}", unidad.Tabla, NUnidades.eFields.IdUnidad);			
			where=new CPropsWhere()
			{
				OmiteConcatenarTabla = true,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = campoUnidad,
				Valor = idUnidad

			};
			return where;
		}
#endregion
#region Buscar
		/// <summary>
		/// Realiza la busqueda en base a los parametros establecidos con el metodo AddWhere.
		/// </summary>
		/// <param name="top">Limite de registros</param>
		/// <returns></returns>
		public virtual IEnumerable<IEntidad> Buscar(int top)
		{
			if(_data == null) return null;
			if(_Entity == null) return null;
			return _data.Select(top,_Entity);
		}
       /// <summary>
		/// Realiza la busqueda en base a los parametros establecidos con el metodo AddWhere.
		/// </summary>
		/// <returns>Lista de entidades</returns>
		public virtual IEnumerable<IEntidad> Buscar()
		{
			if(_data == null) return null;
			if(_Entity == null) return null;
			return _data.Select(_Entity);
		}
		/// <summary>
		/// Realiza la busqueda en base a los parametros establecidos con el metodo AddWhere.
		/// </summary>
		/// <returns>Regresa el primer registro de la lista.</returns>
		public virtual IEntidad BuscarScalar()
		{
			IEnumerable<IEntidad> res = Buscar();
			if(res != null && res.Count() > 0) return res.FirstOrDefault();
			return null;
		}
		/// <summary>
		/// Realiza la busqueda en base a los parametros establecidos con el metodo AddWhere, y realiza una paginacion de los resultados.
		/// </summary>
		/// <typeparam name="Entity">Clase base de los objetos a buscar.</typeparam>
		/// <param name="campoKey">Campo por el cual se realiza la particion.</param>
		/// <param name="pagina">Pagina a cosnsultar.</param>
		/// <returns></returns>
		public virtual IPagina<Entity> Buscar<Entity>(string campoKey, int pagina) where Entity : IEntidad, new()
		{
			return Buscar<Entity>(campoKey,pagina,0);
		}
		/// <summary>
		/// Realiza la busqueda en base a los parametros establecidos con el metodo AddWhere, y realiza una paginacion de los resultados.
		/// </summary>
		/// <typeparam name="Entity">Clase base de los objetos a buscar.</typeparam>
		/// <param name="campoKey">Campo por el cual se realiza la particion.</param>
		/// <param name="pagina">Pagina a cosnsultar.</param>
		/// <param name="registrosPP">Numero de resgistros por Pagina.</param>
		/// <returns></returns>
		public virtual IPagina<Entity> Buscar<Entity>(string campoKey, int pagina, int registrosPP) where Entity : IEntidad, new()
		{
			if(_data == null) return null;
			return _data.Select<Entity>(_Entity, campoKey, pagina,registrosPP);
		}
#endregion
#region ALMACENAMIENTO
		public virtual IEntidad BuildEntity()
		{
			return (_data != null) ? _data.BuildEntity() : null;
		}
		public virtual  void Save()
		{
            if (ValidaSave())
            {
                if (_data == null) return;
                _data.Save(_Entity);
            }
		}
        public virtual void Update()
        {
            if (ValidaUpdate())
            {
                if (_data == null) return;
                _data.Save(_Entity);
            }
        }
		public virtual void Delete()
		{
            if (ValidaDelete())
            {
                if (_data == null) return;
                _data.Delete = 1;
                Save();
            }
		}
        public virtual bool ValidaSave() { return true; }
        public virtual bool ValidaUpdate() { return true; }        
        public virtual bool ValidaDelete() { return true; }

		
#endregion
#region ReflectionEvitarRegistrosDuplicados
		  public virtual IEnumerable<EntityTable> MetaDatos(IEntidad Entity, string Interface, string Table)
		  {
			  Type objType = Entity.GetType();
			  List<EntityTable> allConfig = new List<EntityTable>();
			  if(objType != null)
			  {
				  PropertyInfo[] properties = objType.GetInterface(Interface).GetProperties();
				  foreach(PropertyInfo propiedad in properties)
				  {
					  EntityTable Tabla = propiedad.GetCustomAttributes(typeof(EntityTable), true).FirstOrDefault() as EntityTable;
					  if(Tabla != null)
					  {
						  //where tbl == propiedad.Name
						  if(String.IsNullOrEmpty(Tabla.Campo))
						  {
							  Tabla.Campo = String.Format("{0}.{1}", Table, propiedad.Name);
						  }
						  if(String.IsNullOrEmpty(Tabla.Propiedad))
						  {
							  Tabla.Propiedad = propiedad.Name;
						  }
						  Tabla.Valor = DBase.GetValue(Entity, propiedad.Name);
						  allConfig.Add(Tabla);
					  }
				  }
			  }
			  return allConfig;
		  }//MetaDatos ends.
		  protected virtual IEnumerable<IPropsWhere> _BuildWhereByMeta(IEnumerable<EntityTable> keys)
		  {
			  if(keys == null || keys.Count() <= 0) return null;			  
			  List<CPropsWhere> where = new List<CPropsWhere>();
			  if(keys != null && keys.Count() > 0)
			  {
				  keys.ToList().ForEach(k =>
				  {
					  where.Add(new CPropsWhere()
					  {
						  Condicion = "=",
						  NombreCampo = k.Campo,
						  Prioridad = true,
						  OmiteConcatenarTabla = true,
						  Valor = k.Valor
					  });
				  });
			  }
			  return where;
		  }
		  protected virtual IEnumerable<IEntidad> _SearchByMeta(IEnumerable<IPropsWhere> where)
		  {
			  if(where != null && where.Count() > 0)
			  {
				  this.ClearWhere();
				  this.AddWhere(where);
				  return this.Buscar();
			  }
			  return null;
		  }
		  public virtual IEnumerable<IEntidad> FindByUniqueKey(IEnumerable<EntityTable> meta)
		  {
			  if(meta == null || meta.Count() <= 0) return null;
			  IEnumerable<EntityTable> keys = meta.Where(f => f.IsUnique == true);
			  IEnumerable<IPropsWhere> where = this._BuildWhereByMeta(keys);
			  return this._SearchByMeta(where);
		  }
		  public virtual IEnumerable<IEntidad> FindByPrimaryKey(IEnumerable<EntityTable> meta)
		  {
			  if(meta == null || meta.Count() <= 0) return null;
			  IEnumerable<EntityTable> keys = meta.Where(f => f.IsPrimary == true);
			  IEnumerable<IPropsWhere> where = this._BuildWhereByMeta(keys);
			  return this._SearchByMeta(where);
		  }
		  public virtual bool Compare(IEntidad origen, IEntidad destino, IEnumerable<EntityTable> meta)
		  {
			  IEnumerable<EntityTable> unique = meta.Where(x => x.IsUnique == true);
			  if(unique == null || unique.Count() <= 0) return false;
			  return true;
		  }
		  public virtual bool IsDuplicado(IEntidad Entity,bool IsNew, string Interface, IBase Table)
		  {
			  bool isDuplicado = false;
			  IEnumerable<EntityTable> meta = this.MetaDatos(Entity, Interface, Table.Tabla);
			  if(meta == null || meta.Count() <= 0) return false;
			  //se realizara un insert
			  if(IsNew)
			  {
				  IEnumerable<IEntidad> entityUnique = this.FindByUniqueKey(meta);
				  if(entityUnique != null && entityUnique.Count() > 0)
				  {
					  isDuplicado = true;
				  }				  
			  }
			  //se realizara un update
			  else
			  {
				  IEnumerable<IEntidad> entityKeys = this.FindByPrimaryKey(meta);
				  //existe mas de un registro con la misma primary key no se permite guardar otro registro
				  if(entityKeys != null && entityKeys.Count() > 1)
				  {
					  isDuplicado = true;
				  }
				  IEntidad OldEntity = entityKeys.FirstOrDefault();
				  //¿Hay cambios en los campos unique? si los hay,entonces debemos buscar que los nuevos campos unique existan actualmente.
				  bool hayCambio=false;
				  foreach(EntityTable field in meta.Where(x=>x.IsUnique==true))
				  {
					  object valor = DBase.GetValue(OldEntity, field.Propiedad);
					  if(valor.ToString() != field.Valor.ToString())
					  {
						  hayCambio=true;
					  }
				  }
				  //si los campos marcados como unique cambiaron entonces hay que validar que no existan para evitar duplicdad de registros
				  if(hayCambio)
				  {
					  IEnumerable<IEntidad> entityUnique = this.FindByUniqueKey(meta);
					  if(entityUnique != null && entityUnique.Count() > 0)
					  {
						  isDuplicado = true;
					  }
				  }

			  }
			  if(isDuplicado)
			  {
				  DuplicatedInfo = new StringBuilder("");
				  foreach(EntityTable field in meta.Where(x => x.IsUnique == true))
				  {
					  if(field.ShowResult)
					  {
						  DuplicatedInfo.Append(field.Valor.ToString());
						  DuplicatedInfo.Append(" ");
					  }
				  }
			  }
			  return isDuplicado;
		  }
#endregion

		  #region MULTIIDIOMA
		  /// <summary>
		///  Obtiene el texto en el recurso dependiendo del Idioma seleccionado si no lo encuentra se regresa el texto tal cual fue enviado a buscar.
		/// </summary>
		/// <param name="cadena">Texto a traducir.</param>
		/// <returns>Texto traducido.</returns>
		  public virtual string TraduceTexto(string cadena)
		{
			return CIdioma.TraduceCadena(cadena);
		}
#endregion
		#region PDF
		  public static Byte[] HtmlToPDF(string html)
		  {
			  Byte[] res = null;
			  using(MemoryStream ms = new MemoryStream())
			  {
				  PdfDocument pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);
				  pdf.Save(ms);
				  res = ms.ToArray();
			  }
			  return res;
		  }		  
		#endregion		
		#region  EnvioMail
		public virtual CEmail EnviarEmail(CEmail envioCorreo)
		{
			try
			{
				string []to = envioCorreo.To.Split(',');
				IEnumerable<IParametros> param_email = NParametros.GetByPrefijo("EMAIL_ICAP_");
				IParametros port = param_email.Where(p => p.Nombre == "EMAIL_ICAP_PORT").FirstOrDefault();
				IParametros host = param_email.Where(p => p.Nombre == "EMAIL_ICAP_HOST").FirstOrDefault();
				IParametros EnableSsl = param_email.Where(p => p.Nombre == "EMAIL_ICAP_SSL").FirstOrDefault();
				IParametros user = param_email.Where(p => p.Nombre == "EMAIL_ICAP_USR").FirstOrDefault();
				IParametros pass = param_email.Where(p => p.Nombre == "EMAIL_ICAP_CLAVE").FirstOrDefault();
				SmtpClient client = new SmtpClient();
				client.Port = Convert.ToInt32(port.Valor.ToString());
				client.Host = host.Valor.ToString();
				client.EnableSsl = (EnableSsl.Valor.ToString() == "0") ? false : true;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.UseDefaultCredentials = false;
				client.Credentials = new System.Net.NetworkCredential(user.Valor.ToString(), pass.Valor.ToString());
				MailMessage mm = new MailMessage();//(user.Valor.ToString(), envioCorreo.To, envioCorreo.Asunto, envioCorreo.Body);				
				mm.From=new MailAddress(user.Valor.ToString());
				to.ToList().ForEach(x =>{mm.To.Add(x);});
				mm.Subject = envioCorreo.Asunto;
				mm.Body = envioCorreo.Body;
				mm.BodyEncoding = UTF8Encoding.UTF8;
				mm.IsBodyHtml = envioCorreo.isHtml;
				mm.Priority = MailPriority.High;
				mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
				client.Send(mm);
				envioCorreo.Enviado = true;
			}
			catch(Exception e)
			{
				OnError(e);
				envioCorreo.Enviado = false;
			}
			return envioCorreo;
		}
		#endregion
		public static int ToInt(string data)
		{
			int iData = 0;
			int.TryParse(data, out iData);
			return iData;
		}
		public static double ToDouble(string data)
		{
			double iData = 0;
			double.TryParse(data, out iData);
			return iData;
		}
	}//NBase Clase ends.
} //Namspace ends.
