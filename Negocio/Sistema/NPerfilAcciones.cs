using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 04/01/2017 10:37:33 a. m.*/
namespace Negocio
{

	public partial class NPerfilAcciones : NBase
	{
		public enum eFields
		{			
			IdPerfilAcciones = 0, IdAccion, IdPerfil, IdMenu, Estatus
		}
		public NPerfilAcciones()
		{
		}
		public NPerfilAcciones(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{		
			DPerfilAcciones perfilAcciones = new DPerfilAcciones();
			DAcciones acciones = new DAcciones();
			
			string[] camposJoin = new string[]
			  {				
				String.Format("{0}.{1} ",acciones.Tabla,NAcciones.eFields.AccionNombre.ToString()),				
				String.Format("{0}.{1} ",acciones.Tabla,NAcciones.eFields.ParametrosJs.ToString()),
				String.Format("{0}.{1} ",acciones.Tabla,NAcciones.eFields.ParametrosMvc.ToString()),
				String.Format("{0}.{1} ",acciones.Tabla,NAcciones.eFields.Html.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Acciones*/
					String.Format("{0} {1} {2} {3} = {4} {5}", DBase.InnerJoin,acciones.Tabla,DBase.On, 
				   String.Format("{0}.{1}",acciones.Tabla,NAcciones.eFields.IdAccion.ToString()),
					String.Format("{0}.{1}",perfilAcciones.Tabla,NPerfilAcciones.eFields.IdAccion.ToString()),
					String.Format("{0} {1}.{2} = 1",DBase.AND,acciones.Tabla,NAcciones.eFields.Estatus.ToString()))
			  };
			perfilAcciones.SetConfigJoins(camposJoin, condicionesJoin);
			return perfilAcciones;
		}
		public virtual IEnumerable<IPropsWhere> FiltroIndex(int idMenu,IPerfiles perfil)
		{
			if(idMenu<=0 || perfil== null) return null;
			List<IPropsWhere> where = new List<IPropsWhere>();			
			where.Add(new CPropsWhere()
			{				
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NPerfilAcciones.eFields.Estatus.ToString(),
				Valor = 1,
				Operador=DBase.AND
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NPerfilAcciones.eFields.IdMenu.ToString(),
				Valor = idMenu,
				Operador=DBase.AND
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NPerfilAcciones.eFields.IdPerfil.ToString(),
				Valor = perfil.IdPerfil
			});
			return where;
		}
		public virtual bool HasPermiso(string accionName, int perfil, int menu)
		{
			NAcciones Findaccion = new NAcciones();
			IAcciones accion = Findaccion.FindByName(accionName);
			if(accion != null)
			{
				IPerfilAcciones permiso = this.FindByAccionPerfilMenu(accion.IdAccion, perfil, menu);
				if(permiso == null) return false;
				if(permiso.Estatus == false) return false;
				return true;
			}
			else
			{
				return false;
			}
		}
		public virtual IPerfilAcciones FindByAccionPerfilMenu(int idAccion,int idPerfil,int idMenu)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Condicion="=",
				NombreCampo=NPerfilAcciones.eFields.Estatus.ToString(),
				Valor=true,
				Prioridad=true			
			});
			where.Add(new CPropsWhere()
			{
				Condicion = "=",
				NombreCampo = NPerfilAcciones.eFields.IdAccion.ToString(),
				Valor = idAccion,
				Prioridad = true
			});
			where.Add(new CPropsWhere()
			{
				Condicion = "=",
				NombreCampo = NPerfilAcciones.eFields.IdPerfil.ToString(),
				Valor = idPerfil,
				Prioridad = true
			});
			where.Add(new CPropsWhere()
			{
				Condicion = "=",
				NombreCampo = NPerfilAcciones.eFields.IdMenu.ToString(),
				Valor = idMenu,
				Prioridad = true
			});
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<IEntidad> res = this.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IPerfilAcciones;
			}
			return null;
		}
	}// NPerfilAcciones ends.        
}// LibNegocio.Entidades ends.
