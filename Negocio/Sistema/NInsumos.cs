using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{
	public partial class NInsumos : NBase
	{
		public enum eFields
		{
			IdInsumo = 0,IdEmpresa,IdFamilia, IdUnidad, Codigo, Serie, Descripcion, IdMoneda, Precio, EsServicio, Estatus
		}
		public NInsumos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DInsumos insumo =  new DInsumos();
			DMonedas moneda = new DMonedas();
			DUnidadesMedida unidad = new DUnidadesMedida();
			DEmpresas empresa = new DEmpresas();
			DFamilias familia = new DFamilias();
			string[] camposJoin = new string[]
			  {				
				String.Format("{0}.{1} " ,unidad.Tabla,NUnidadesMedida.eFields.Unidad.ToString()),
				String.Format("{0}.{1} UnidadMedidaDescripcion" ,unidad.Tabla,NUnidadesMedida.eFields.Descripcion.ToString()),
				String.Format("{0}.{1} ",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} ",familia.Tabla,NFamilias.eFields.Familia.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdUnidad.ToString())),
					/*Join with Monedas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdMoneda.ToString())),
					 /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdEmpresa.ToString())),
					 /*Join with Familias*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,familia.Tabla,DBase.On, 
				   String.Format("{0}.{1}",familia.Tabla,NFamilias.eFields.IdFamilia.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdFamilia.ToString()))
			  };
			insumo.SetConfigJoins(camposJoin, condicionesJoin);
			return insumo;
		}
		public IEnumerable<IPropsWhere> FiltroSoloInsumosActivos(IEnumerable<IPropsWhere> whereOriginal)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();		
			where.Add(new CPropsWhere()
			{				
				Prioridad = true,
				Condicion = "=",
				NombreCampo =  NInsumos.eFields.EsServicio.ToString(),
				Valor = false
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NInsumos.eFields.Estatus.ToString(),
				Valor = true
			});
			return base.CombineWithFiltro(where, whereOriginal);
		}
		public IEnumerable<IPropsWhere> FiltroSoloInsumosServiciosActivos(IEnumerable<IPropsWhere> whereOriginal)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();			
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NInsumos.eFields.Estatus.ToString(),
				Valor = true
			});
			return base.CombineWithFiltro(where, whereOriginal);
		}
		protected virtual void ObtienenExistencias(string inCondition, IEnumerable<IInsumos> insumos)
		{
			NInsumosExistencias existencias = new NInsumosExistencias();
			existencias.AddWhere(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NInsumosExistencias.eFields.IdInsumo.ToString(),
				Valor = inCondition
			});
			existencias.AddWhere(new CPropsWhere()
			{
				Operador = DBase.AND,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NInsumosExistencias.eFields.Estatus.ToString(),
				Valor = true
			});
			IEnumerable<IInsumosExistencias> existencias2 = existencias.Buscar().Select(x => x as IInsumosExistencias);
			foreach(IInsumos insumo in insumos)
			{
				//solo para los insumos el costo se obtinene de la tabla movimientos
				if(!insumo.EsServicio)
				{
					IEnumerable<IInsumosExistencias> existencia = existencias2.Where(x => x.IdInsumo == insumo.IdInsumo).OrderByDescending(i=>i.UltimoMovimiento);
					if(existencia != null && existencia.Count() > 0)
					{
						insumo.Costo = existencia.First().Costo;
					}
				}				
				/*if(existencia != null && existencia.Count() > 0)
				{
					mov.CantidadExistenciaInsumo = existencia.FirstOrDefault().Cantidad;
				}*/
			}
		}
		/// <summary>
		/// Obtiene el costo de los insumos en base al registro de los movimientos [Tabla MovimientosDetalle]
		/// </summary>
		/// <param name="insumos">Lista de insumos</param>
		public void GetCostoInsumos(IEnumerable<IInsumos> insumos)
		{
			string ids = String.Empty;
			List<int> idInsumos = new List<int>();
			idInsumos = (from tbl in insumos
							 select tbl.IdInsumo).ToList();
			for(int c = 0; c < idInsumos.Count(); c++)
			{
				ids += idInsumos[c].ToString();
				if(c < idInsumos.Count() - 1)
				{
					ids += ",";
				}
			}
			ObtienenExistencias(ids, insumos);
		}
		public NInsumos()
		{
		}
		#region VALIDANDO_CAMBIOS
		protected bool HasExistencias(IInsumos insumo)
		{
			NInsumosExistencias existencias = new NInsumosExistencias();
			existencias.AddWhere(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NInsumosExistencias.eFields.IdInsumo.ToString(),
				Valor = insumo.IdInsumo
			});
			existencias.AddWhere(new CPropsWhere()
			{
				Operador = DBase.AND,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NInsumosExistencias.eFields.Estatus.ToString(),
				Valor = true
			});
			IEnumerable<IInsumosExistencias> existencias1 = existencias.Buscar().Select(x => x as IInsumosExistencias);
			if(existencias1 != null && existencias1.Count() > 0)
				return true;

			NMovimientosDetalles movs = new NMovimientosDetalles();
			movs.AddWhere(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NMovimientosDetalles.eFields.IdInsumo.ToString(),
				Valor = insumo.IdInsumo
			});
			IEnumerable<IMovimientosDetalles> existencias2 = movs.Buscar().Select(x => x as IMovimientosDetalles);
			if(existencias2 != null && existencias2.Count() > 0)
				return true;

			return false;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IInsumos", new DInsumos());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		protected virtual bool ValidaInsumo(bool update)
		{
			IInsumos insumo= _Entity as IInsumos;

			if(String.IsNullOrEmpty(insumo.Codigo))
			{
				SetError(TraduceTexto("RequiredIInsumosCodigo"));
				return false;
			}
				if(insumo.EsServicio && insumo.Costo <= 0)
				{
					SetError(TraduceTexto("IInsumoValidaServicioCosto"));
						return false;
				}
				if(!insumo.EsServicio && insumo.IdUnidad <= 0)
				{
					SetError(TraduceTexto("IInsumoValidaUnidadIncorrecta"));
					return false;
				}
				if(insumo.IdMoneda <= 0)
				{
					SetError(TraduceTexto("IInsumoValidaMonedaIncorrecta"));
					return false;
				}
				if(update)
				{
					if(insumo.Estatus == false)
					{
						if(HasExistencias(insumo))
						{
							SetError(TraduceTexto("IInsumoValidaTieneExistencias"));
							return false;
						}
					}
				}
				return true;			
		}
		public static IEnumerable<IInsumos> GetInsumosFromTable(System.Data.DataTable InsumosTbl)
		{			
			if(InsumosTbl == null) return null;
			List<IInsumos> Insumos = new List<IInsumos>();
			for(int rw = 0; rw < InsumosTbl.Rows.Count; rw++)
			{				
				Insumos.Add(new CInsumos()
				{
					Codigo = InsumosTbl.Rows[rw].Field<string>("CODIGO"),
					Serie = InsumosTbl.Rows[rw].Field<string>("SERIE"),
					Descripcion = InsumosTbl.Rows[rw].Field<string>("DESCRIPCION"),
					Familia = InsumosTbl.Rows[rw].Field<string>("FAMILIA"),
					UnidadMedidaDescripcion = InsumosTbl.Rows[rw].Field<string>("UNIDAD_MEDIDA"),
					CodigoMoneda = InsumosTbl.Rows[rw].Field<string>("MONEDA")
				});
			}
			return Insumos;
		}
		public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
		{
			entities.ToList().ForEach(
			x =>
			{				
				_Entity = x;
				this.Save();
				if(HasError())
				{
					((IInsumos)x).Empresa = this.GetErrorMessage();
					this.ClearError();
				}
			});
			return entities;
		}
		public IEnumerable<IInsumos> SaveMasivo2(IEnumerable<IInsumos> Insumos,int IdEmpresa)
		{
			try
			{
				if(Insumos == null || Insumos.Count() <= 0) return null;
				DFamilias df = new DFamilias();
				DUnidadesMedida du = new DUnidadesMedida();
				DMonedas dm = new DMonedas();
				DEmpresas de = new DEmpresas();
				string InCondicion = String.Empty;
				string[] Familias = Insumos.Where(i=> !(String.IsNullOrEmpty(i.Familia))).Select(i => String.Format("'{0}'", i.Familia)).ToArray();
				string[] Unidades = Insumos.Where(i => !(String.IsNullOrEmpty(i.UnidadMedidaDescripcion))).Select(i => String.Format("'{0}'", i.UnidadMedidaDescripcion)).ToArray();
				string[] Monedas = Insumos.Where(i => !(String.IsNullOrEmpty(i.CodigoMoneda))).Select(i => String.Format("'{0}'", i.CodigoMoneda)).ToArray();
				if(Familias != null) Familias = Familias.GroupBy(a => a).Select(g=>g.Key).ToArray();
				if(Unidades != null) Unidades = Unidades.GroupBy(a => a).Select(g => g.Key).ToArray();
				if(Monedas != null) Monedas = Monedas.GroupBy(a => a).Select(g => g.Key).ToArray();
				
				IEnumerable<IEntidad> Res = null;
				IEnumerable<IFamilias> lstFamilias = new List<IFamilias>();
				IEnumerable<IUnidadesMedida> lstUnidades = new List<IUnidadesMedida>();
				IEnumerable<IMonedas> lstMonedas =new List<IMonedas>();
				NFamilias nF = new NFamilias();
				NUnidadesMedida nM = new NUnidadesMedida();
				NMonedas nMo = new NMonedas();
				List<IPropsWhere> where = new List<IPropsWhere>();
				CPropsWhere whereEmpresa = new CPropsWhere();
				whereEmpresa.Condicion = "=";
				whereEmpresa.OmiteConcatenarTabla = true;
				whereEmpresa.Prioridad = true;
				whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
				whereEmpresa.Valor = IdEmpresa;

				CPropsWhere whereCampo = new CPropsWhere();
				whereCampo.Condicion = "in";
				whereCampo.OmiteConcatenarTabla = true;
				whereCampo.Prioridad = true;

				//Buscando Familias
				InCondicion = String.Join(",", Familias);
				whereCampo.NombreCampo = String.Format("{0}.{1}", df.Tabla, NFamilias.eFields.Familia.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nF.AddWhere(where);
				Res = nF.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstFamilias = Res.Select(x => x as IFamilias);
				}
				//Buscando Unidades de medida
				InCondicion = String.Join(",", Unidades);
				whereCampo.NombreCampo = String.Format("{0}.{1}", du.Tabla, NUnidadesMedida.eFields.Unidad.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nM.AddWhere(where);
				Res = nM.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstUnidades = Res.Select(x => x as IUnidadesMedida);
				}
				//Biscando Monedas
				InCondicion = String.Join(",", Monedas);
				whereCampo.NombreCampo = String.Format("{0}.{1}", dm.Tabla, NMonedas.eFields.Codigo.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nMo.AddWhere(where);
				Res = nMo.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstMonedas = Res.Select(x => x as IMonedas);
				}
				Insumos.ToList().ForEach(i =>
				{
					var resF =(!String.IsNullOrEmpty(i.Familia))? lstFamilias.Where(f => f.Familia.ToLower() == i.Familia.ToLower()):null;
					var resUM = (!String.IsNullOrEmpty(i.UnidadMedidaDescripcion)) ? lstUnidades.Where(u => u.Unidad.ToLower() == i.UnidadMedidaDescripcion.ToLower()) : null;
					var resMo = (!String.IsNullOrEmpty(i.CodigoMoneda)) ? lstMonedas.Where(m => m.Codigo.ToLower() == i.CodigoMoneda.ToLower()) : null;
					if(resF != null && resF.Count() > 0)
					{
						i.IdFamilia = resF.FirstOrDefault().IdFamilia;
					}
					if(resUM != null && resUM.Count() > 0)
					{
						i.IdUnidad = resUM.FirstOrDefault().IdUnidad;
					}
					if(resMo != null && resMo.Count() > 0)
					{
						i.IdMoneda = resMo.FirstOrDefault().IdMoneda;
					}
				});
				Insumos.ToList().ForEach(i =>{
					i.IdEmpresa = IdEmpresa;
					i.Estatus = true;
					if(i.Codigo == null) i.Codigo = String.Empty;
				});
			   this.SaveMasivo(Insumos);
				Insumos = Insumos.Where(i => i.IdInsumo <= 0);
			}
			catch(Exception e)
			{
				OnError(e);
			}

			return Insumos;
		}
		public override bool ValidaDelete()
		{
			if(HasExistencias(_Entity as IInsumos))
			{
				SetError(TraduceTexto("IInsumoValidaTieneExistencias"));
				return false;
			}
			return true;
		}

		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			if(ValidaInsumo(false))
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Update()
		{

			if(ValidaDuplicado(false)) return;	
			if(ValidaInsumo(true))
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		#endregion		
	}// NInsumos ends.        
}// LibNegocio.Entidades ends.
