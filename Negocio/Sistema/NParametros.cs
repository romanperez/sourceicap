using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{

	public partial class NParametros : NBase
	{
		public enum eFields
		{
			IdParametro = 0, Nombre, Valor, Estatus, Descripcion
		}
		public NParametros(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DParametros();
		}
		public NParametros()
		{
		}
		public static IParametros GetByNombre(string nombre)
		{
			CParametros p = new CParametros();
			p.Nombre = nombre;
			NParametros np = new NParametros(p);
			np.AddWhere(NParametros.eFields.Nombre.ToString(), "=");
			IEnumerable<IEntidad> res = np.Buscar();
			if(res.Any())
			{
				return res.FirstOrDefault() as IParametros;
			}
			else
			{
				return null;
			}
		}
		public static IEnumerable<IParametros> GetByPrefijo(string nombre)
		{
			CParametros p = new CParametros();
			p.Nombre = nombre;
			NParametros np = new NParametros(p);
			np.AddWhere(NParametros.eFields.Nombre.ToString(), "contenga");
			IEnumerable<IEntidad> res = np.Buscar();
			if(res.Any())
			{
				return res.Select(x => x as IParametros);
			}
			else
			{
				return null;
			}
		}

	}// NParametros ends.        
}// LibNegocio.Entidades ends.
