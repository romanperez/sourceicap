using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 21/12/2016 12:08:29 p. m.*/
namespace Negocio
{

	public partial class NDepartamentos : NBase
	{
		public enum eFields
		{
			IdDepartamento = 0, IdUnidad, Codigo, Nombre, Estatus
		}
		public NDepartamentos()
		{
		}
		public NDepartamentos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DDepartamentos departamento = new DDepartamentos();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),

            String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			};
			departamento.SetConfigJoins(camposJoin, condicionesJoin);
			return departamento;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IDepartamentos", new DDepartamentos());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}

        public static IEnumerable<IDepartamentos> GetDepartamentosFromTable(System.Data.DataTable DepartamentosTbl)
        {
            if (DepartamentosTbl == null) return null;
            List<IDepartamentos> Departamentos = new List<IDepartamentos>();
            for (int rw = 0; rw < DepartamentosTbl.Rows.Count; rw++)
            {
                Departamentos.Add(new CDepartamentos()
                {
                    Unidad = DepartamentosTbl.Rows[rw].Field<string>("UNIDAD"),
                    Codigo = DepartamentosTbl.Rows[rw].Field<string>("CODIGO"),
                    Nombre = DepartamentosTbl.Rows[rw].Field<string>("NOMBRE")
                });
            }
            return Departamentos;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((IDepartamentos)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<IDepartamentos> SaveMasivo2(IEnumerable<IDepartamentos> Departamentos, int IdEmpresa)
        {
            try
            {
                if (Departamentos == null || Departamentos.Count() <= 0) return null;

                DUnidades du = new DUnidades();
                DEmpresas de = new DEmpresas();
                string[] Unidades = Departamentos.Where(i => !(String.IsNullOrEmpty(i.Unidad))).Select(i => String.Format("'{0}'", i.Unidad)).ToArray();
                string InCondicion = String.Empty;
                IEnumerable<IUnidades> lstUnidades = new List<IUnidades>();
                IEnumerable<IEntidad> Res = null;
                NUnidades nU = new NUnidades();

                List<IPropsWhere> where = new List<IPropsWhere>();
                CPropsWhere whereEmpresa = new CPropsWhere();
                whereEmpresa.Condicion = "=";
                whereEmpresa.OmiteConcatenarTabla = true;
                whereEmpresa.Prioridad = true;
                whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
                whereEmpresa.Valor = IdEmpresa;

                CPropsWhere whereCampo = new CPropsWhere();
                whereCampo.Condicion = "in";
                whereCampo.OmiteConcatenarTabla = true;
                whereCampo.Prioridad = true;

                //Buscando Unidades
				InCondicion = String.Join(",", Unidades);
                whereCampo.NombreCampo = String.Format("{0}.{1}", du.Tabla, NUnidades.eFields.Unidad.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
                nU.AddWhere(where);
                Res = nU.Buscar();
				if(Res != null && Res.Count() > 0)
				{
                    lstUnidades = Res.Select(x => x as IUnidades);
				}

                Departamentos.ToList().ForEach(i =>
                {
                    var resF = (!String.IsNullOrEmpty(i.Unidad)) ? lstUnidades.Where(f => f.Unidad.ToLower() == i.Unidad.ToLower() && f.IdEmpresa == IdEmpresa) : null;

                    if (resF != null && resF.Count() > 0)
                    {
                        i.IdUnidad = resF.FirstOrDefault().IdUnidad;
                    }
                   
                });

                Departamentos.ToList().ForEach(i =>
                {
                    i.IdEmpresa = IdEmpresa;
                    i.Estatus = true;
                });
                this.SaveMasivo(Departamentos);
                Departamentos = Departamentos.Where(i => i.IdDepartamento <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return Departamentos;
        }
	}// NDepartamentos ends.        
}// LibNegocio.Entidades ends.
