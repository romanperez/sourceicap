using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
namespace Negocio
{

	public partial class NCotizaciones : NBase
	{		
		public enum eFields
		{
			IdCotizacion = 0, IdUnidad, IdUsuario, IdMoneda, IdTipoCambio, Total, Cotizacion,
			PorcentajeDescuento, DescuentoMonto, IdEmpresa, IdCliente, IdIva, Fecha, Exito, Importe, SubTotal, Iva,
			Solicitud, ProyectoInicial, DescripcionTitulo, DescripcionCompleta, DepartamentoAtencion
		}
		public NCotizaciones(IEntidad entidad)
			: base(entidad)
		{									
		}
		protected virtual IBase BuildConfig(string table)
		{			
			DCotizaciones cotizacion = new DCotizaciones();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			DProyectos proyecto = new DProyectos();
			DClientes cliente = new DClientes();
			DUsuarios usuario = new DUsuarios();
			DMonedas moneda = new DMonedas();
			DTipoCambios cambio = new DTipoCambios();
			DIvaValores iva = new DIvaValores();
			DEmpleados empleado = new DEmpleados();
			cotizacion.Tabla = table;
			string[] camposJoin = new string[]
			  {																				
				  String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				  String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				  String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.RazonSocial.ToString()),
				  String.Format("{0}.{1} as Iva",iva.Tabla,NIvaValores.eFields.Porcentaje.ToString()),
				  String.Format("isnull({0}.{1},0) as {2}",cambio.Tabla,NTipoCambios.eFields.Valor.ToString(),NTipoCambios.eFields.Valor.ToString()),			
				  String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				  String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString()),				  
				  String.Format("{0}.{1} ",proyecto.Tabla,NProyectos.eFields.Proyecto.ToString())
				  //String.Format("{0} AS NombreEmpleado",new NEmpleados().DataNombreEmpleado())
			  };
			string[] condicionesJoin = new string[] 
			  { 				   				 
				    /*Join with Monedas */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NContratos.eFields.IdMoneda.ToString())),
					/*Join with TipoCambio */
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,cambio.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cambio.Tabla,NTipoCambios.eFields.IdTipoCambio.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NContratos.eFields.IdTipoCambio.ToString())),
					/*Join with IVA */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,iva.Tabla,DBase.On, 
				   String.Format("{0}.{1}",iva.Tabla,NIvaValores.eFields.IdIva.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NContratos.eFields.IdIva.ToString())),
					/*Join with Clientes */
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cliente.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdCliente.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NContratos.eFields.IdCliente.ToString())),
				  	/*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NContratos.eFields.IdEmpresa.ToString())),			 
					 /*Join with Unidades*/
					String.Format("{0} {1} {2} {3} = {4} {5}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format(" {0} {1}.{2} = {3}.{4}",DBase.AND,cotizacion.Tabla,NContratos.eFields.IdUnidad.ToString(),
					                                                 unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),								
					/*Join with Proyectos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,proyecto.Tabla,DBase.On, 
				   String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdCotizacion.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdCotizacion.ToString()))
			  };
			cotizacion.SetConfigJoins(camposJoin, condicionesJoin);
			return cotizacion;
		}
		protected override IBase BuildDataEntity()	
		{
			DCotizaciones cotizacion = new DCotizaciones();
			cotizacion = BuildConfig(DCotizaciones.VW_COTIZACIONES) as DCotizaciones;
			return cotizacion;
		}
		public NCotizaciones()
		{
		}
		protected string GetWhereIn(string where)
		{
			string[] ids = where.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
			string inWhere = String.Empty;
			for(int c = 0; c < ids.Length; c++)
			{
				inWhere += ids[c];
				if(c < ids.Length - 1)
				{
					inWhere += ",";
				}
			}
			return inWhere;
		}
		public IEnumerable<IPropsWhere> InsumosByConcepto(string whereString)
		{
			string inWhere = GetWhereIn(whereString);
			List<IPropsWhere> where = new List<IPropsWhere>();
			DConceptosInsumos entidad = new DConceptosInsumos();
			where.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NConceptosInsumos.eFields.IdConcepto.ToString(),
				Valor = inWhere
			});
			return where;
		}
		public IEnumerable<IPropsWhere> InsumosByConceptoCotizacion(int idCotizacion)
		{
			//string inWhere = GetWhereIn(whereString);
			List<IPropsWhere> where = new List<IPropsWhere>();						
			where.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NCotizacionesDetallesInsumos.eFields.IdCotizacion.ToString(),
				Valor = idCotizacion			
			});
			return where;
		}
		//public IEnumerable<IPropsWhere> InsumosByCapitulo(IPropsWhere where)
		//{
		//	DConceptos concepto = new DConceptos();
		//	//IPropsWhere whereIds = where.Where(w => w.NombreCampo == "IdConcepto").FirstOrDefault();
		//	//IPropsWhere whereCap = where.Where(w => w.NombreCampo == "IdCapitulo").FirstOrDefault();
		//	List<IPropsWhere> filtro = new List<IPropsWhere>();			
		//	filtro.Add(new CPropsWhere()
		//	{
		//		OmiteConcatenarTabla = true,
		//		Prioridad = true,
		//		Condicion = "=",
		//		NombreCampo = String.Format("{0}.{1}", concepto.Tabla, NConceptos.eFields.IdCapitulo.ToString()),
		//		Valor = where.Valor
		//	});
		//	return filtro;
		//}
		public IEnumerable<IPropsWhere> InsumosByCapitulo(IEnumerable<IPropsWhere> where)
		{
			DConceptos concepto = new DConceptos();
			IPropsWhere whereIds = where.Where(w => w.NombreCampo == "IdConcepto").FirstOrDefault();
			IPropsWhere whereCap = where.Where(w => w.NombreCampo == "IdCapitulo").FirstOrDefault();
			List<IPropsWhere> filtro = new List<IPropsWhere>();
			filtro.Add(this.InsumosByConcepto(whereIds.Valor.ToString()).FirstOrDefault());
			filtro.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = true,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = String.Format("{0}.{1}", concepto.Tabla, NConceptos.eFields.IdCapitulo.ToString()),
				Valor = whereCap.Valor
			});
			return filtro;
		}
		public IEnumerable<IPropsWhere> InsumosByCapitulo(int idCotizacion,IEnumerable<IPropsWhere> where)
		{
			DCotizacionesDetallesInsumos concepto = new DCotizacionesDetallesInsumos();
			IPropsWhere whereIds = where.Where(w => w.NombreCampo == "IdConcepto").FirstOrDefault();
			IPropsWhere whereCap = where.Where(w => w.NombreCampo == "IdCapitulo").FirstOrDefault();
			List<IPropsWhere> filtro = new List<IPropsWhere>();
			filtro.Add(this.InsumosByConcepto(whereIds.Valor.ToString()).FirstOrDefault());
			filtro.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NCotizacionesDetallesInsumos.eFields.IdCotizacion.ToString(),
				Valor = idCotizacion
			});
			filtro.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = true,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NConceptos.eFields.IdCapitulo.ToString(),
				Valor = whereCap.Valor
			});
			return filtro;
		}
		public IEnumerable<IPropsWhere> ConceptosByCapitulo(string whereString)
		{
			string inWhere = GetWhereIn(whereString);
			List<IPropsWhere> where = new List<IPropsWhere>();
			DConceptos entidad = new DConceptos();
			where.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NConceptos.eFields.IdConcepto.ToString(),
				Valor = inWhere
			});			
			return where;
		}
		public IEnumerable<IPropsWhere> ConceptosByCapitulo(int idCotizacion)
		{			
			List<IPropsWhere> where = new List<IPropsWhere>();
			DConceptos entidad = new DConceptos();
			where.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NCotizacionesDetalles.eFields.IdCotizacion.ToString(),
				Valor = idCotizacion
			});
			return where;
		}
		public ICotizaciones GetCotizacionById(int idCotizacion)
		{
			ICotizaciones cotizacion = null;
			try
			{
				List<IPropsWhere> where = new List<IPropsWhere>();
				where.Add(new CPropsWhere()
				{

					Prioridad = true,
					Condicion = "=",
					NombreCampo = NCotizaciones.eFields.IdCotizacion.ToString(),
					Valor = idCotizacion
				});
				this.AddWhere(where);
				IEnumerable<IEntidad> res = this.Buscar();
				if(res != null && res.Count() > 0)
				{
					return res.FirstOrDefault() as ICotizaciones;
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return cotizacion;

		}
		public IEnumerable<IConceptos> GroupByCapitulo(IEnumerable<IConceptos> conceptos)
		{
			if(conceptos == null || conceptos.Count() <= 0) return null;
			IEnumerable<IConceptos> grupo = conceptos.GroupBy(l => l.IdCapitulo).Select(cl => new CConceptos
			{
				Capitulo = cl.First().Capitulo,
				Precio = cl.Sum(c => c.Precio),
				IdCapitulo = cl.First().IdCapitulo
			}).ToList();
			return grupo;													  
		}
		
		public void LoadConceptos(ICotizaciones cotizacion)
		{
			if(cotizacion == null) return;
			NCotizacionesDetalles conceptos = new NCotizacionesDetalles();
			IMonedas peso = NConversiones.GetMonedaPesos(cotizacion.IdEmpresa);
			List<CPropsWhere> where = new List<CPropsWhere>();
			where.Add(new CPropsWhere()
				{
					Prioridad = true,
					Condicion = "=",
					NombreCampo = NCotizacionesDetalles.eFields.IdCotizacion.ToString(),
					Valor = cotizacion.IdCotizacion
				});
			conceptos.AddWhere(where);
			cotizacion.Conceptos = conceptos.Buscar().Select(x => x as CCotizacionesDetalles).ToList();
			cotizacion.Conceptos.ToList().ForEach(x =>
			{
				x.IdMoneda = peso.IdMoneda;
				x.Moneda = peso.Moneda;
				x.CodigoMoneda = peso.Codigo;
			});			
		}
		public void LoadInsumos(ICotizaciones cotizacion)
		{			
			string id = String.Empty;
			List<CCotizacionesDetalles> allConceptos = cotizacion.Conceptos.Where(x => x.IdCotizacionDetalle == 0).ToList();
			if(allConceptos != null && allConceptos.Count()>0)
			{
				for(int c = 0; c < allConceptos.Count(); c++)
				{
					id = id + allConceptos[c].IdConcepto;
					if(c < allConceptos.Count() - 1)
					{
						id = id + ",";
					}					
				}
				NConceptosInsumos ci = new NConceptosInsumos();
				List<CPropsWhere> where = new List<CPropsWhere>();
				where.Add(new CPropsWhere()
				{
					Prioridad = true,
					Condicion = "in",
					NombreCampo = NConceptosInsumos.eFields.IdConcepto.ToString(),
					Valor = id
				});
				ci.AddWhere(where);
				IEnumerable<IEntidad> res = ci.Buscar();
				if(res != null && res.Count() > 0)
				{
					cotizacion.Insumos = new List<CCotizacionesDetallesInsumos>();
					foreach(IConceptosInsumos insumo in res)
					{
						cotizacion.Insumos.Add(new CCotizacionesDetallesInsumos()
						{
							Cantidad = insumo.Cantidad,
							Costo = insumo.CostoInsumo,
							Descripcion = insumo.Descripcion,
							IdCapitulo = insumo.IdCapitulo,
							IdConcepto = insumo.IdConcepto,
							IdInsumo = insumo.IdInsumo,
							IdMoneda = insumo.IdMoneda
						});
					}
				}
			}
			
			NCotizacionesDetallesInsumos saved = new NCotizacionesDetallesInsumos(new CCotizacionesDetallesInsumos()
			{
				IdCotizacion = cotizacion.IdCotizacion
			});
			saved.AddWhere(NCotizacionesDetallesInsumos.eFields.IdCotizacion.ToString(), "=");
			IEnumerable<IEntidad> e2 = saved.Buscar();
			if(e2 != null && e2.Count() > 0)
			{
				if(cotizacion.Insumos == null)
				{
					cotizacion.Insumos = new List<CCotizacionesDetallesInsumos>();					
				}
				foreach(ICotizacionesDetallesInsumos insumo in e2)
				{
					cotizacion.Insumos.Add(insumo as CCotizacionesDetallesInsumos);
				}		
			}
		}				
		public ICotizaciones GetOriginal(int idCotizacion)
		{
			//Obtenemos la cotizacion original
				ICotizaciones coti = new CCotizaciones();
				coti.IdCotizacion = idCotizacion;
				NCotizaciones buscar = new NCotizaciones(coti);
				buscar.AddWhere(NCotizaciones.eFields.IdCotizacion.ToString(), "=");
				IEnumerable<IEntidad> res = buscar.Buscar();
				if(res.Any())
				{
					return res.FirstOrDefault() as ICotizaciones;
				}
			return null;
		}		
		protected void SetPartidas(ICotizaciones cotizacion)
		{
			if(!cotizacion.Conceptos.Any()) return ;
			int sigPartida = cotizacion.Conceptos.Max(x => x.Partida);
			if(sigPartida < 0) sigPartida = 0;
			IEnumerable<ICotizacionesDetalles> noPartidas = cotizacion.Conceptos.Where(x => x.Partida <= 0);
			if(noPartidas.Any())
			{
				noPartidas.ToList().ForEach(x =>
				{
					x.Partida = ++sigPartida;
				});
			}
		}
		protected void MainCalculaCotizacion(ICotizaciones cotizacion)
		{									
			SetPartidas(cotizacion);					
		}
		protected int? GetIdTipoCambio(ICotizaciones coti)
		{
			if(coti == null || coti.IdEmpresa<=0) return null;			
			ITipoCambios cambio = new CTipoCambios();
			IMonedas peso = NConversiones.GetMonedaPesos(coti.IdEmpresa);
			if(peso == null || peso.IdMoneda <= 0) return null;
			cambio.FechaInicio = coti.Fecha;
			cambio.IdMonedaOrigen = coti.IdMoneda;
			cambio.IdMonedaDestino = peso.IdMoneda;
			NTipoCambios ntipocambio = new NTipoCambios();
			cambio = ntipocambio.GetIdValorCambio(cambio) as CTipoCambios;
			if(cambio!= null && cambio.IdTipoCambio>0) return cambio.IdTipoCambio;
			return null;
		}
		protected virtual void SetPersonal(ICotizaciones cotizacion)
		{
			NCotizacionesPersonalCliente personal = new NCotizacionesPersonalCliente();
			IEstatus resAtencion = personal.EstatusCotizacionPersonalAtencion(cotizacion.IdEmpresa);
			IEstatus resContacto = personal.EstatusCotizacionPersonalContacto(cotizacion.IdEmpresa);
			if(resContacto != null)
			{
				cotizacion.PersonalClienteAtencion.ForEach(x =>
				{
					if(x != null)
					{
						x.IdEstatus = resAtencion.IdEstatus;
					}
				});
			}
			if(resContacto != null)
			{
				cotizacion.PersonalClienteContacto.ForEach(x =>
				{
					if(x != null)
					{						
						x.IdEstatus = resContacto.IdEstatus;
					}
				});
			}
		}
		public virtual bool ValidaDuplicado(bool isNew)
		{
			DCotizaciones cotizacionTable = new DCotizaciones();
			cotizacionTable.Tabla =DCotizaciones.VW_COTIZACIONES;
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "ICotizaciones",cotizacionTable);
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		protected bool ValidaCotizacion()
		{
			if(_data.Delete == 1) return true;			
			if(_Entity != null)
			{
				ICotizaciones cotizacion = _Entity as ICotizaciones;
				SetPersonal(cotizacion);
				cotizacion.IdTipoCambio = GetIdTipoCambio(cotizacion);
				if(cotizacion.IdTipoCambio == null || cotizacion.IdTipoCambio <= 0)
				{
					SetError(TraduceTexto("NCotizacionTipoCambioNovalido"));
					return false;
				}
				if(cotizacion.IdMoneda <= 0)
				{
					SetError(TraduceTexto("NCotizacionMonedaNovalido"));
					return false;
				}
				if(cotizacion.IdIva <= 0)
				{
					SetError(TraduceTexto("NCotizacionIvaNovalido"));
					return false;
				}
				//if(cotizacion.IdTipoCambio <= 0 )
				//{
				//	SetError(TraduceTexto("NCotizacionTipoCambioNovalido"));
				//	return false;
				//}
				if(cotizacion.IdCliente <= 0)
				{
					SetError(TraduceTexto("NCotizacionClienteNovalido"));
					return false;
				}
				if(cotizacion.Conceptos== null || cotizacion.Conceptos.Count() <= 0)
				{
					SetError(TraduceTexto("ICotizacionSinConceptos"));
					return false;
				}
				var cantidadesZero = cotizacion.Conceptos.Where(x => x.Cantidad <= 0 && x.IdCotizacionDetalle>=0);
				if(cantidadesZero != null && cantidadesZero.Count()>0)
				{
					SetError(TraduceTexto("ICotizacionConceptosCantidadesZero"));
					return false;
				}
				var ubicacionVacia = cotizacion.Conceptos.Where(x => String.IsNullOrEmpty( x.Ubicacion) && x.IdCotizacionDetalle >= 0);
				if(ubicacionVacia != null && ubicacionVacia.Count() > 0)
				{
					SetError(TraduceTexto("NConceptosDetalleNoUbicacion"));
					return false;
				}
				var result = from item in cotizacion.Conceptos
								 group item by new
								 {
									 item.IdConcepto,
									 item.Ubicacion
								 } into g
								 select new CMovimientos
								 {
									 Tipo = g.Count()
								 };
				var repetidos = result.Where(x => x.Tipo > 1);
				if(repetidos != null && repetidos.Count() > 0)
				{
					SetError(TraduceTexto("IMovimientosExisteConcepto"));
					return false;
				}
				MainCalculaCotizacion(cotizacion);				
			}
		
			return true;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			if(ValidaCotizacion())
			{
				if(_data == null) return;
				_data.Save(_Entity);
				if(((ICotizaciones)_Entity).IdCotizacion == -1)
				{
					SetError(String.Format("{0} {1}", CIdioma.TraduceCadena("NConceptosNoHayTipoCambio"), ((ICotizaciones)_Entity).Empresa));
				}
			}
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;	
			if(ValidaCotizacion())
			{
				if(_data == null) return;
				_data.Save(_Entity);
				if(((ICotizaciones)_Entity).IdCotizacion == -1)
				{
					SetError(String.Format("{0} {1}", CIdioma.TraduceCadena("NConceptosNoHayTipoCambio"), ((ICotizaciones)_Entity).Empresa));
				}
			}
		}
		public override bool ValidaDelete()
		{
			bool bResp = true;
			if(_Entity != null)
			{
				ICotizaciones cotizacion = _Entity as ICotizaciones;
				bResp = IsValidModificaciones(cotizacion.IdCotizacion);
			}			
			return bResp;
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}		
		public IConceptos ToConcepto(ICotizacionesDetalles detalle)
		{
			CConceptos concepto = new CConceptos();
			concepto.Capitulo =detalle.Capitulo;
			concepto.Concepto = detalle.Concepto ;
			concepto.Descripcion =detalle.Descripcion;
			concepto.Precio =detalle.Precio;
		   concepto.IdConcepto= detalle.IdConcepto;
			concepto.Moneda = detalle.Moneda;
			return concepto;
		}
		/*protected int ToInt(string data)
		{
			int iData = 0;
			int.TryParse(data, out iData);
			return iData;
		}*/
		public void GetConceptosNombre(ICotizaciones cotizacion,System.Data.DataTable datos)
		{
			if(datos == null) return;
			cotizacion.Conceptos = new List<CCotizacionesDetalles>();			
			for(int rw =0;rw<datos.Rows.Count;rw++)
			{
				cotizacion.Conceptos.Add(new CCotizacionesDetalles()
				{
					Concepto = datos.Rows[rw].Field<string>("B1 CLAVE"),
					Descripcion =datos.Rows[rw].Field<string>("B5 DESCRIPCIÓN"),
					Cantidad =ToDouble(datos.Rows[rw].Field<string>("B7 CANTIDAD").ToString()),
					Partida = ToInt(datos.Rows[rw].Field<string>("B2 PARTIDA").ToString()),
					Ubicacion = datos.Rows[rw].Field<string>("B8 UBICACIÓN"),
					UnidadMedida = datos.Rows[rw].Field<string>("B6 UNIDAD/MEDIDA")
				});
			}
		}
		public ICotizaciones RealizaConversionMonedas(ICotizaciones cotizacion)
		{			
			return cotizacion;
		}
		
		public static string GetCampoId()
		{
			//DCotizaciones coti = new DCotizaciones();
			//return String.Format("{0}.{1}", coti.Tabla, NCotizaciones.eFields.IdCotizacion.ToString());
			return NCotizaciones.eFields.IdCotizacion.ToString();
		}
		public static ICotizaciones CopiaCotizacion(ICotizaciones cotizacion)
		{
			cotizacion.IdCotizacion = 0;
			cotizacion.Conceptos.ForEach(x =>
			{
				x.IdCotizacionDetalle = 0;
				x.IdCotizacion = 0;
			});
			cotizacion.Insumos.ForEach(x =>
			{
				x.IdCotizacion = 0;
				x.IdInsumoCotizacion = 0;
			});
			cotizacion.Cotizacion = String.Format("{0}Copia", cotizacion.Cotizacion);
			cotizacion.IsCopy = true;
			return cotizacion;
		}
		public static double GetUtilidad(double cantidad, double utilidad)
		{
			if(utilidad <= 0) return cantidad;
			return cantidad+( (cantidad * utilidad) / 100);
		}
		public override IPagina<Entity> Buscar<Entity>(string campoKey, int pagina, int registrosPP)
		{
			/*DCotizaciones all = new DCotizaciones();
			if(_data != null)
			{
				all.AddWhere(_data.Where());
			}
			return all.GetIndexCotizaciones<Entity>(pagina);*/
			//////			
			if(_data != null)
			{
				IEnumerable<IPropsWhere> where = _data.Where();
				_data = BuildConfig(DCotizaciones.VW_COTIZACIONES);
				_data.Tabla = DCotizaciones.VW_COTIZACIONES;
				_data.ClearWhere();
				if(where != null && where.Count() > 0)
				{
					_data.AddWhere(where.ToList());
				}				
			}
			IPagina<Entity> pag= _data.Select<Entity>(_Entity, String.Format("{0}.{1}", DCotizaciones.VW_COTIZACIONES, campoKey), pagina, registrosPP);

			if(pag != null && pag.Contenido !=null && pag.Contenido.Count() > 0)
			{
				GetUsuario(pag.Contenido.Select(x => x as ICotizaciones));
				GetClientePersonal(pag.Contenido.Select(x => x as ICotizaciones));
				return pag;
			}
			return null;
		}
		public ICotizaciones GetDetalleCotizacion(int IdCotizacion)
		{
			DCotizaciones all = new DCotizaciones();
			/*if(_data != null)
			{
				all.AddWhere(_data.Where());
				all.AddWhere(new CPropsWhere()
				{
					NombreCampo = "Cotizaciones.IdCotizacion",
					Valor = IdCotizacion
				});
			}*/

			ICotizaciones Cotizacion = GetCotizacionById(IdCotizacion);
			Cotizacion = all.GetDetalleCotizacion(Cotizacion);
			/*if(Cotizacion != null)
			{
				Cotizacion.SubTotal = Cotizacion.Conceptos.Sum(c => c.Precio * c.Cantidad);
			}*/
			return Cotizacion;
		}
		
		public override IEnumerable<IEntidad> Buscar()
		{
			IEnumerable<IEntidad> res = base.Buscar();
			if(res != null && res.Count() > 0)
			{
				GetUsuario(res.Select(x => x as ICotizaciones));
				GetClientePersonal(res.Select(x => x as ICotizaciones));
				return res;
			}
			return null;
		}
		protected string BuildIds(IEnumerable<ICotizaciones> cotizaciones)
		{
			int[] idOts = (from tbl in cotizaciones
								select tbl.IdCotizacion).ToArray();
			return String.Join(",", idOts);
		}
		protected string ListToString(IEnumerable<ICotizacionesPersonalCliente> lista, string separador)
		{
			if(lista == null || lista.Count() <= 0) return String.Empty;
			StringBuilder all = new StringBuilder("");
			int c = 0;
			foreach(ICotizacionesPersonalCliente persona in lista)
			{
				all.Append(persona.NombreCompleto);
				if(c < lista.Count() - 1)
				{
					all.Append(separador);
				}
				c++;
			}
			return all.ToString();
		}
		protected void GetClientePersonal(IEnumerable<ICotizaciones> cotizaciones)
		{
			string ids = BuildIds(cotizaciones);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NCotizacionesPersonalCliente.eFields.IdCotizacion.ToString(),
				Valor = ids
			});
			NCotizacionesPersonalCliente personalClienteCotizacion = new NCotizacionesPersonalCliente();			
			personalClienteCotizacion.AddWhere(where);
			IEnumerable<IEntidad> res1 = personalClienteCotizacion.Buscar();
			IEnumerable<ICotizacionesPersonalCliente> personal = null;			
			if(res1.Any())
			{
				personal = res1.Select(x => x as ICotizacionesPersonalCliente);
				cotizaciones.ToList().ForEach(x =>
				{
					IEstatus resAtencion = personalClienteCotizacion.EstatusCotizacionPersonalAtencion(x.IdEmpresa);
					IEstatus resContacto = personalClienteCotizacion.EstatusCotizacionPersonalContacto(x.IdEmpresa);
					IEnumerable<ICotizacionesPersonalCliente> atencion = personal.Where(p => p.IdCotizacion == x.IdCotizacion && p.IdEstatus == resAtencion.IdEstatus);
					IEnumerable<ICotizacionesPersonalCliente> contacto = personal.Where(p => p.IdCotizacion == x.IdCotizacion && p.IdEstatus == resContacto.IdEstatus);
					if(atencion.Any())
					{
						x.PersonalClienteAtencion = atencion.Select(aa => aa as CCotizacionesPersonalCliente).ToList();
						x.ListClienteAtencion = ListToString(x.PersonalClienteAtencion, ",");
					}
					if(contacto.Any())
					{
						x.PersonalClienteContacto = contacto.Select(aa => aa as CCotizacionesPersonalCliente).ToList();
						x.ListClienteContacto = ListToString(x.PersonalClienteContacto, ",");
					}
				});
			}	
		}
		protected void GetUsuario(IEnumerable<ICotizaciones> cotizaciones)
		{
			int[] idUsers = (from tbl in cotizaciones
								group tbl by tbl.IdUsuario into g
								select g.Key).ToArray();
			string ids = String.Join(",", idUsers);
			NUsuarios n = new NUsuarios();
			n.AddWhere(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NUsuarios.eFields.IdUsuario.ToString(),
				Valor = ids
			});
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IUsuarios> allUsers = null;
			if(res1!= null && res1.Count()>0)
			{
				allUsers = res1.Select(x => x as IUsuarios);
				cotizaciones.ToList().ForEach(x =>
				{
					x.Usuario = allUsers.Where(y => y.IdUsuario == x.IdUsuario).FirstOrDefault() as CUsuarios;
				});
			}
		}
		public bool IsValidModificaciones(int IdCotizacion)
		{
			bool IsValid = true;
			try
			{				
				NProyectos p = new NProyectos();
				IProyectos proyecto = p.GetProyectoByIdCotizacion(IdCotizacion);
				if(proyecto != null && proyecto.Contratos != null && proyecto.Contratos.Count > 0)
				{
					SetError(TraduceTexto("NProyectoYaGeneroContrato"));
					IsValid = false;
				}
				if(p.HasError())
				{
					SetError(p.GetErrorMessage(), true);
					IsValid = false;
				}				
			}
			catch(Exception e)
			{
				OnError(e);
				IsValid = false;
			}
			return IsValid;
		}
	}// NCotizaciones ends.        
}// LibNegocio.Entidades ends.
