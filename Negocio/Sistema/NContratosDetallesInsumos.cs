using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 21/02/2017 09:48:12 a. m.*/
namespace Negocio
{  
    
    public partial class NContratosDetallesInsumos : NBase
    {
	public enum eFields {IdInsumoContrato=0,IdContrato,IdConcepto,IdInsumo,Cantidad,Costo}
    public NContratosDetallesInsumos() { }
	public NContratosDetallesInsumos(IEntidad entidad)
			: base(entidad)
	{
	}
	protected override IBase BuildDataEntity()
	{
	  return new  DContratosDetallesInsumos();
	}				
    }// NContratosDetallesInsumos ends.        
}// LibNegocio.Entidades ends.
