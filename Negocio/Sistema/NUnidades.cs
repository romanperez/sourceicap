using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 21/12/2016 12:08:29 p. m.*/
namespace Negocio
{

	public partial class NUnidades : NBase
	{
		public enum eFields
		{
			IdUnidad = 0, IdEmpresa, Unidad, Direccion, Telefono, Estatus
		}
		public NUnidades()
		{
		}
		public NUnidades(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			};
			unidad.SetConfigJoins(camposJoin, condicionesJoin);
			return unidad;
		}
		public IEnumerable<IPropsWhere> FiltroIndex(IUsuarios user, string valor)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NUnidades.eFields.IdEmpresa.ToString(),
				Valor = valor
			});
			return where;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IUnidades", new DUnidades());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}

        public static IEnumerable<IUnidades> GetUnidadesFromTable(System.Data.DataTable UnidadesTbl)
        {
            if (UnidadesTbl == null) return null;
            List<IUnidades> Unidades = new List<IUnidades>();
            for (int rw = 0; rw < UnidadesTbl.Rows.Count; rw++)
            {
                Unidades.Add(new CUnidades()
                {
                    Unidad = UnidadesTbl.Rows[rw].Field<string>("UNIDAD"),
                    Direccion = UnidadesTbl.Rows[rw].Field<string>("DIRECCION"),
                    Telefono = UnidadesTbl.Rows[rw].Field<string>("TELEFONO")
                });
            }
            return Unidades;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((IUnidades)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<IUnidades> SaveMasivo2(IEnumerable<IUnidades> Unidades, int IdEmpresa)
        {
            try
            {
                if (Unidades == null || Unidades.Count() <= 0) return null;


                Unidades.ToList().ForEach(i =>
                {
                    i.IdEmpresa = IdEmpresa;
                    i.Estatus = true;
                });
                this.SaveMasivo(Unidades);
                Unidades = Unidades.Where(i => i.IdUnidad <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return Unidades;
        }
	}// NUnidades ends.        
}// LibNegocio.Entidades ends.
