using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NEstatus : NBase
	{
		public enum eFields
		{
			IdEstatus = 0, IdEmpresa, Clasificacion, Nombre, Estatus
		}
		public NEstatus(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DEstatus estatus = new DEstatus();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())				
			  };
			string[] condicionesJoin = new string[] 
			  { 				  
					 /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",estatus.Tabla,NEstatus.eFields.IdEmpresa.ToString()))					 
			  };
			estatus.SetConfigJoins(camposJoin, condicionesJoin);
			return estatus;
		}
		public NEstatus()
		{
		}
		public IEnumerable<IEstatus> ByClasificacion(int IdEmpresa, string clasificacion)
		{
			if(String.IsNullOrEmpty(clasificacion) || IdEmpresa <= 0) return null;
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NEstatus.eFields.Clasificacion.ToString(),
				Condicion = "=",
				Valor = clasificacion
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NEstatus.eFields.IdEmpresa.ToString(),
				Condicion = "=",
				Valor = IdEmpresa
			});
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				NombreCampo = NEstatus.eFields.Estatus.ToString(),
				Condicion = "=",
				Valor = true
			});
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<IEntidad> res = this.Buscar();
			if(res != null) return res.Select(x => x as IEstatus);
			else return null;
		}

		public IEnumerable<IEstatus> OTTipo(int IdEmpresa)
		{
			IParametros param = NParametros.GetByNombre("PREFIJO_OT_TIPO");
			if(param == null || IdEmpresa <= 0) return null;
			return this.ByClasificacion(IdEmpresa, param.Valor.ToString());
		}
		public IEnumerable<IEstatus> OTEstado(int IdEmpresa)
		{
			IParametros param = NParametros.GetByNombre("PREFIJO_OT_ESTATUS");
			if(param == null || IdEmpresa <= 0) return null;
			return this.ByClasificacion(IdEmpresa, param.Valor.ToString());
		}
		public IEnumerable<IEstatus> ProyectoEstado(int IdEmpresa)
		{
			IParametros param = NParametros.GetByNombre("PREFIJO_PROYECTO_ESTATUS");
			if(param == null || IdEmpresa <= 0) return null;
			return this.ByClasificacion(IdEmpresa, param.Valor.ToString());
		}
		public IEnumerable<IEstatus> OTPrioridad(int IdEmpresa)
		{
			IParametros param = NParametros.GetByNombre("PREFIJO_OT_URGENCIA");
			if(param == null || IdEmpresa <= 0) return null;
			return this.ByClasificacion(IdEmpresa, param.Valor.ToString());
		}
	}// NEstatus ends.        
}// LibNegocio.Entidades ends.
