using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NIvaValores : NBase
	{
		public enum eFields
		{
			IdIva = 0, IdEmpresa, Porcentaje, FechaInicio, FechaFin, Estatus
		}
		public NIvaValores(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DIvaValores IvaValores = new DIvaValores();
			DEmpresas empresas = new DEmpresas();

			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.Empresa.ToString())
				
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresas.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",IvaValores.Tabla,NIvaValores.eFields.IdEmpresa.ToString()))						
			  };
			IvaValores.SetConfigJoins(camposJoin, condicionesJoin);
			return IvaValores;
		}
		public NIvaValores()
		{
		}
		protected bool VerificaFechas(IIvaValores cambio, DateTime fecha)
		{			
			int resultado1 = _data.fnGetIdValorIva(cambio.IdEmpresa, fecha);
			if(resultado1 != 0)
			{	
				if(resultado1 != cambio.IdIva)
				{
					SetError(String.Format("ID:{0} {1}",resultado1, TraduceTexto("NIvaFechasYaExisten")));					
					return false;
				}				
			}
			return true;
		}
		protected bool ValidaIvaValores()
		{
			if(_data.Delete == 1) return true;
			bool validacion = true;
			if(_Entity != null)
			{
				IIvaValores ivaval = _Entity as IIvaValores;
				if(ivaval.FechaInicio > ivaval.FechaFin)
				{
					SetError(TraduceTexto("NIvaFechasIncorrectas"));
					return false;
				}
				if(ivaval.Porcentaje <= 0)
				{
					SetError(TraduceTexto("NIvaPorcentajeIncorrecto"));
					return false;
				}
				validacion = this.VerificaFechas(ivaval, ivaval.FechaInicio);
				validacion = this.VerificaFechas(ivaval, ivaval.FechaFin);
				return validacion;
			}

			return true;
		}

		public override void Save()
		{
			if(ValidaIvaValores())
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Update()
		{

			if(ValidaIvaValores())
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public static IIvaValores GetById(int id)
		{
			NIvaValores iva = new NIvaValores();
			List<CPropsWhere> where = new List<CPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NIvaValores.eFields.IdIva.ToString(),
				Valor = id
			});
			iva.AddWhere(where);
			IEnumerable<IEntidad> res = iva.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IIvaValores;
			}
			return null;
		}		
		public IIvaValores GetIvaByFecha(IIvaValores Entity)
		{
			IIvaValores iva = null;
			try
			{
				int idIva = _data.fnGetIdValorIva(Entity.IdEmpresa, Entity.FechaInicio);
				iva = NIvaValores.GetById(idIva);
				if(iva == null)
				{					
					return null;
				}
			}
			catch(Exception eFecha)
			{
				OnError(eFecha);
			}
			return iva;
		}
	}// NIvaValores ends.        
}// LibNegocio.Entidades ends.
