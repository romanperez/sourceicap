using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 03/10/2017 11:24:13 a. m.*/
namespace Negocio
{

	public partial class NEstimaciones : NBase
	{
		protected IContratos2 _Contrato;
		public enum eFields
		{
            IdEstimacion = 0, IdContratosDetalles, NumeroEstimacion, FechaInicio, FechaFin, CantidadEstimada, ImporteEstimado
		}
		public NEstimaciones()
		{
		}
		public NEstimaciones(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DEstimaciones();
		}
		public IEnumerable<IContratosDetalles2> EstimacionesPorContratoIndex(int IdProyecto,string contratos,DateTime Inicial,DateTime Final)
		{
			DProyectos p = new DProyectos();
			return p.EstimacionesPorContratoIndex(IdProyecto,contratos,Inicial,Final);
		}
		protected IEstimaciones ActualizaEstimacion(IEstimaciones estimacion)
		{
			NEstimaciones busca = new NEstimaciones();
			busca.AddWhere(new CPropsWhere()
			{
				Condicion="=",				
				OmiteConcatenarTabla=true,
				NombreCampo=NEstimaciones.eFields.FechaInicio.ToString(),
				//NombreCampo=String.Format("CAST({0} AS DATE)", NEstimaciones.eFields.Fecha.ToString()),
				//OmiteParametro=true,
				Valor=estimacion.FechaInicio.ToShortDateString(),
				Prioridad=true
			});
			busca.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				NombreCampo = NEstimaciones.eFields.IdContratosDetalles.ToString(),
				Valor = estimacion.IdContratosDetalles,
				Prioridad = true
			});
			IEnumerable<IEntidad> res = busca.Buscar();			
			if(res != null && res.Count() == 1)
			{
				IEstimaciones auxEstimacion = res.FirstOrDefault() as IEstimaciones;
				auxEstimacion.CantidadEstimada += estimacion.CantidadEstimada;
				return auxEstimacion;
			}
			if(res != null && res.Count() > 1)
			{
				SetError(String.Format("{0} {1}", TraduceTexto("IEstimacionFechaExistente"), estimacion.FechaInicio.ToString("d")), true);
				return null;
			}
			return estimacion;
		}
		public void ObtieneCantidadProgramada(int IdContrato,DateTime FechaIni,DateTime FechaFin)
		{
            NContratos NContrato = new NContratos();
            _Contrato = NContrato.EstimacionesPorContrato(IdContrato, FechaIni, FechaFin);                        
		}
        
		public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
		{
			entities.ToList().ForEach(
			 x =>
			 {
				 _Entity = x;
				 _data.Delete = 0;
				 if(((IEstimaciones)_Entity).IdEstimacion==0)
				 {
					 if(((IEstimaciones)_Entity).CantidadEstimada>0)
					 {
						 Save();
					 }
					 
				 }
				 if(((IEstimaciones)_Entity).IdEstimacion > 0)
				 {					 
					 Update();
				 }
				 if(((IEstimaciones)_Entity).IdEstimacion < 0)
				 {
					 ((IEstimaciones)_Entity).IdEstimacion = ((IEstimaciones)_Entity).IdEstimacion * -1;
					 ((IEstimaciones)_Entity).FechaInicio = DateTime.Now;
					 _data.Delete = 1;
					 Delete();
				 }
			 });
			return entities;
		}
        public static string ToString(IEstimaciones estimacion)
        {
            if (estimacion == null) return String.Empty;
            return string.Format("{0} {1} {2}", estimacion.NumeroEstimacion, estimacion.FechaInicio.ToString("d"), estimacion.FechaFin.ToString("d"));
        }
        public IEnumerable<IEstimaciones> ReporteEstimacionesCantidades(IEstimaciones entity)
        {
            IBase data = BuildDataEntity();
            return (data as DEstimaciones).ReporteEstimacionesCantidades(entity);
        }
        public IEnumerable<IEstimacionesResumenEconomico> ReporteEstimacionesResumenEconomico(IEstimacionesResumenEconomico entity)
        {
            IBase data = BuildDataEntity();
            return (data as DEstimaciones).ReporteEstimacionesResumenEconomico(entity);
        }

        public static IEstimaciones GetById(int IdEstimacion)
        {
            NEstimaciones iva = new NEstimaciones();
            List<CPropsWhere> where = new List<CPropsWhere>();
            where.Add(new CPropsWhere()
            {
                Prioridad = true,
                Condicion = "=",
                NombreCampo = NEstimaciones.eFields.IdEstimacion.ToString(),
                Valor = IdEstimacion
            });
            iva.AddWhere(where);
            IEnumerable<IEntidad> res = iva.Buscar();
            if (res != null && res.Count() > 0)
            {
                return res.FirstOrDefault() as IEstimaciones;
            }
            return null;
        }
        public bool ValidaPeriodoEstimacion(IEstimaciones estimacion)
        {
            int idEstimacion1=0;
            int idEstimacion2 = 0;
            idEstimacion1 = _data.fnValidaPeriodoEstimacion(estimacion.IdContratosDetalles, estimacion.FechaInicio);
            idEstimacion2 = _data.fnValidaPeriodoEstimacion(estimacion.IdContratosDetalles, estimacion.FechaFin);
            if (idEstimacion1>0 || idEstimacion2>0)
            {
                IEstimaciones existeA = NEstimaciones.GetById(idEstimacion1);
                IEstimaciones existeB = NEstimaciones.GetById(idEstimacion2);
                string esti1 = NEstimaciones.ToString(existeA);
                string esti2 = NEstimaciones.ToString(existeB);
                if (!String.IsNullOrEmpty(esti1))
                {
                    SetError(TraduceTexto("IEstimacionPeriodoYaExiste") + " " + esti1, true);
                }
                if (!String.IsNullOrEmpty(esti2))
                {
                    SetError(TraduceTexto("IEstimacionPeriodoYaExiste") + " " + esti2, true);
                }
                return false;                
            }
            if (estimacion.FechaInicio > estimacion.FechaFin)
            {
                SetError(TraduceTexto("IEstimacionRevisarPeriodoEstimacion"));
                return false;                
            }
            return true;            
        }
		public bool ValidaEstimacion(bool isNew)
		{
			if(_data.Delete == 1) return true;
			bool isvalid=true;
			IEstimaciones estimacion = _Entity as IEstimaciones;
			/*if(isNew)
			{
				estimacion = ActualizaEstimacion(estimacion);
				if(estimacion == null)
				{
					isvalid = false;
				}
				else
				{
					SetEntity(estimacion);
				}
			}*/
			if(_Contrato != null && _Contrato.Conceptos != null && _Contrato.Conceptos.Count()>0)
			{
				IContratosDetalles2 concepto = null;
				var detalle = _Contrato.Conceptos.Where(c => c.IdContratosDetalles == estimacion.IdContratosDetalles);
				if(detalle != null && detalle.Count() > 0)
				{
					concepto = detalle.First();
				}
				if(concepto != null)
				{
					if(estimacion.CantidadEstimada > concepto.CantidadEjecutada)
					{
                        SetError(String.Format("{0} {1} [{2}]", TraduceTexto("IEstimacionCantidadvsCantidadEjecutada"), estimacion.CantidadEstimada, concepto.CantidadEjecutada), true);
						return false;
					}
				}
			}
			return isvalid;
		}
		public override bool ValidaSave()
		{
			return ValidaEstimacion(true);
		}
		public override bool ValidaUpdate()
		{
			return ValidaEstimacion(false);
		}
	}// NEstimaciones ends.        
}// LibNegocio.Entidades ends.
