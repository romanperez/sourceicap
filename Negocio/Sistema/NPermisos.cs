using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{  
    
    public partial class NPermisos : NBase
    {		 
        public NPermisos(IEntidad entidad): base(entidad){}
		  public NPermisos()
		  {
		  }			
		  protected override IBase BuildDataEntity()
		  {
				return new  DPermisos();
		  }
		  public override IEnumerable<IEntidad> Buscar()
		  {
			  List<IPermisos> allPermisos = new List<IPermisos>();
			  IPermisos permiso = _Entity as IPermisos;
			  NMenus menus = new NMenus();
			  permiso.Menus =menus.GetAllMenus().Select(x => x as CMenus).ToList();
			  menus.GetMenuIdioma(permiso.Menus);
			  permiso.Perfiles = _GetAllPerfiles().Select(x => x as CPerfiles).ToList();
			  permiso.Acciones = _GetAllAcciones().Select(x => x as CAcciones).ToList();
			  allPermisos.Add(permiso);
			  return allPermisos;
		  }
		  protected IEnumerable<IPerfiles> _GetAllPerfiles()
		  {
			  CPerfiles perfil = new CPerfiles();
			  perfil.Estatus = true;
			  NPerfiles perfiles = new NPerfiles(perfil);
			  perfiles.AddWhere(NPerfiles.eFields.Estatus.ToString(), "=");
			  return perfiles.Buscar().Select(p => p as IPerfiles);			  
		  }
		  protected IEnumerable<IAcciones> _GetAllAcciones()
		  {
			  CAcciones accion = new CAcciones();
			  accion.Estatus = true;
			  NAcciones acciones = new NAcciones(accion);
			  acciones.AddWhere(NAcciones.eFields.Estatus.ToString(), "=");
			  return acciones.Buscar().Select(p => p as IAcciones);
		  }
		  public CPermisosJs GetPermisos(int IdPerfil)
		  {
			  List<IPropsWhere> wherePM = new List<IPropsWhere>();
			  List<IPropsWhere> wherePA = new List<IPropsWhere>();
			  wherePM.Add(new CPropsWhere()
			  {
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = NPerfilMenus.eFields.IdPerfil.ToString(),
				  Valor = IdPerfil
			  });
			  wherePM.Add(new CPropsWhere()
			  {
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = NPerfilMenus.eFields.Estatus.ToString(),
				  Valor = 1
			  });
			  wherePA.Add(new CPropsWhere()
			  {
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = NPerfilAcciones.eFields.IdPerfil.ToString(),
				  Valor = IdPerfil
			  });
			  wherePA.Add(new CPropsWhere()
			  {
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = NPerfilAcciones.eFields.Estatus.ToString(),
				  Valor = 1
			  });
			  return ((DPermisos)(_data)).GetPermisos(IdPerfil,wherePM,wherePA);	
		  }
		  protected void ActualizaPermisos()
		  {
			  IPermisosJs _permisos = _Entity as IPermisosJs;
			  if(_permisos.MenuPermisos != null && _permisos.MenuPermisos.Count() > 0)
			  {
				  _permisos.MenuPermisos = _permisos.MenuPermisos.Distinct().ToArray();
			  }
			  if(_permisos.MenuVer != null && _permisos.MenuVer.Count() > 0)
			  {
				  _permisos.MenuVer = _permisos.MenuVer.Distinct().ToArray();
			  }			  
		  }
		  public override void Update()
		  {
			  if(ValidaUpdate())
			  {
				  if(_data == null) return;
				  ActualizaPermisos();
				  _data.Save(_Entity);
			  }
		  }		 
    }// NTecnicos ends.        
}// LibNegocio.Entidades ends.
