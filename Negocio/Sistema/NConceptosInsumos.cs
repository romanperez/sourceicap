using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{

	public partial class NConceptosInsumos : NBase
	{
		public enum eFields
		{
			IdConceptoInsumo = 0, IdConcepto, IdMoneda,IdInsumo, Cantidad
		}
		public NConceptosInsumos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DConceptosInsumos detail = new DConceptosInsumos();
			DInsumos insumo = new DInsumos();
			DMonedas moneda = new DMonedas();
			DConceptos conceptos = new DConceptos();
			DUnidadesMedida um = new DUnidadesMedida();
			string[] camposJoin = new string[]
			  {								
				  String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdCapitulo.ToString()),
				  String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Codigo.ToString()),
				  String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Descripcion.ToString()),				  
				  String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				  String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.Unidad.ToString()),
				  String.Format("{0}.{1} UnidadMedidaDescripcion" ,um.Tabla,NUnidadesMedida.eFields.Descripcion.ToString()),				
				  String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Conceptos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,conceptos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdConcepto.ToString()),
					String.Format("{0}.{1}",detail.Tabla,NConceptosInsumos.eFields.IdConcepto.ToString())),	 
				  /*Join with Insumos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,insumo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdInsumo.ToString()),
					String.Format("{0}.{1}",detail.Tabla,NConceptosInsumos.eFields.IdInsumo.ToString())),					
					/*Join with Monedas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",detail.Tabla,NConceptosInsumos.eFields.IdMoneda.ToString())),
					/*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,um.Tabla,DBase.On, 
				   String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdUnidad.ToString()))
			  };					
			detail.SetConfigJoins(camposJoin, condicionesJoin);
			return detail;
		}
		public NConceptosInsumos()
		{
		}
		public IEnumerable<IConceptosInsumos> ObtieneDetallesById(int id)
		{
			CConceptosInsumos cd = new CConceptosInsumos();
			cd.IdConcepto = id;
			NConceptosInsumos detalles = new NConceptosInsumos(cd);
			detalles.AddWhere(NConceptosInsumos.eFields.IdConcepto.ToString(), "=");
			return detalles.Buscar().Select(b => b as CConceptosInsumos).ToList();			
		}
		public IConceptosInsumos ToConceptoInsumo(IInsumos insumo)
		{
			double costo = 0;
			IConceptosInsumos ci = new CConceptosInsumos();
			ci.Codigo = insumo.Codigo;
			ci.Descripcion = insumo.Descripcion;
			ci.IdInsumo = insumo.IdInsumo;
			ci.Moneda = insumo.Moneda;
			ci.CodigoMoneda = insumo.CodigoMoneda;
			ci.Unidad = insumo.Unidad;
			ci.UnidadMedidaDescripcion = insumo.UnidadMedidaDescripcion;
			double.TryParse(insumo.Costo.ToString(), out costo);
			ci.CostoInsumo = costo;
			ci.IdMoneda = insumo.IdMoneda;
			return ci;
		}
		//public IEnumerable<IConceptosInsumos> ConvierteInsumos(CDataTipoCambio info,ITipoCambios cambio,IEnumerable<IConceptosInsumos> insumos)
		//{			
		//	if(info.IdMoneda <= 0 || info.IdTipoCambio <= 0 || cambio.IdMonedaOrigen <= 0)
		//	{
		//		throw new Exception("ICotizacionSeleccioneMoneda");
		//	}
		//	List<IConceptosInsumos> NewInsumos = new List<IConceptosInsumos>();
		//	foreach(IConceptosInsumos insumo in insumos)
		//	{
		//		if(insumo.IdMoneda != info.IdMoneda)
		//		{
		//			if(info.IdMoneda != cambio.IdMonedaOrigen)
		//			{
		//				insumo.CostoInsumo = insumo.CostoInsumo * cambio.Valor;
		//				insumo.Moneda = info.Moneda;
		//			}
		//			else
		//			{
		//				insumo.CostoInsumo = insumo.CostoInsumo / cambio.Valor;
		//				insumo.Moneda = info.Moneda;
		//			}
		//		}
		//		NewInsumos.Add(insumo);
		//	}
		//	return NewInsumos;
		//}
	}// NConceptosInsumos ends.        
}// LibNegocio.Entidades ends.
