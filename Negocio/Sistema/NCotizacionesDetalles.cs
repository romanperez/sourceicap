using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{  
    
    public partial class NCotizacionesDetalles : NBase
    {
	public enum eFields {IdCotizacionDetalle=0,IdCotizacion,IdConcepto,IdInsumo,Cantidad,Precio,PorcentajeDescuento,DescuentoMonto}
        public NCotizacionesDetalles(IEntidad entidad)
			: base(entidad)
	{
	}
	protected override IBase BuildDataEntity()
	{	  
	  DCotizacionesDetalles detalle = new DCotizacionesDetalles();
	  DConceptos conceptos = new DConceptos();
	  DCapitulos capitulo = new DCapitulos();
	  DUnidadesMedida um = new DUnidadesMedida();
	  string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.IdCapitulo.ToString()),
				String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.Capitulo.ToString()),				
				String.Format("{0}.{1} as UnidadMedida",um.Tabla,NUnidadesMedida.eFields.Unidad.ToString())				
			  };
	  string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Conceptos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,conceptos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdConcepto.ToString()),
					String.Format("{0}.{1}",detalle.Tabla,NCotizacionesDetalles.eFields.IdConcepto.ToString()))	,
					/*Join with Capitulos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,capitulo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.IdCapitulo.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdCapitulo.ToString())),
					/*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,um.Tabla,DBase.On, 
				   String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdUnidadMedida.ToString()))
			  };
	  detalle.SetConfigJoins(camposJoin, condicionesJoin);
	  return detalle;
	}		
	public NCotizacionesDetalles() { }
	protected virtual bool ValidaDetalle()
	{
		ICotizacionesDetalles detalle = _Entity as ICotizacionesDetalles;
		detalle.Precio=Math.Round(detalle.Precio,DCotizaciones.NUM_DECIMALES);
		return true;
	}
	public override bool ValidaSave()
	{
		return ValidaDetalle();
	}
	public override bool ValidaUpdate()
	{
		return ValidaDetalle();
	}
    }// NCotizacionesDetalles ends.        
}// LibNegocio.Entidades ends.
