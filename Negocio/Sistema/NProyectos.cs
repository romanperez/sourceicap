using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{
	public partial class NProyectos : NBase
	{
		public bool OmiteDetalle;
		public enum eFields
		{
			IdProyecto = 0, IdCliente, IdEmpresa, IdUnidad, IdCotizacion, Proyecto, Descripcion, Ubicacion, FechaInicial, FechaFinal, IdEstatus
		}
		public NProyectos(IEntidad entidad)
			: base(entidad)
		{
		}		
		protected override IBase BuildDataEntity()
		{
			DProyectos proyecto = new DProyectos();
			DCotizaciones cotizacion = new DCotizaciones();
			DClientes cliente = new DClientes();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			DEstatus estatus = new DEstatus();
			string[] camposJoin = new string[]
				  {																	
					String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.RazonSocial.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
					String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.Cotizacion.ToString()),
					String.Format("{0}.{1} as EstatusProyecto",estatus.Tabla,NEstatus.eFields.Nombre.ToString())
				  };
			string[] condicionesJoin = new string[] 
				  { 			
					   /*Join with Cotizaciones*/
					   String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,cotizacion.Tabla,DBase.On, 
					   String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdCotizacion.ToString()),
					   String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdCotizacion.ToString())),
					  /*Join with Clientes*/						
					   String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cliente.Tabla,DBase.On, 
					   String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdCliente.ToString()),
					   String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdCliente.ToString())),
						/*Join with Unidades*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,unidad.Tabla,DBase.On, 
						String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
						String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdUnidad.ToString()))	,
						/*Join with Empresas*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,empresa.Tabla,DBase.On, 
						String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
						String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdEmpresa.ToString())),
						/*Join with Estatus*/
						String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,estatus.Tabla,DBase.On, 
						String.Format("{0}.{1}",estatus.Tabla,NEstatus.eFields.IdEstatus.ToString()),
						String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdEstatus.ToString()))
				  };
			proyecto.SetConfigJoins(camposJoin, condicionesJoin);
			return proyecto;
		}		
		public NProyectos()
		{
		}
		public IProyectos GetProyectoById(int idProyecto)
		{
			IProyectos proyecto = null;
			try
			{
				List<IPropsWhere> where = new List<IPropsWhere>();
				where.Add(new CPropsWhere()
				{

					Prioridad = true,
					Condicion = "=",
					NombreCampo = NProyectos.eFields.IdProyecto.ToString(),
					Valor = idProyecto
				});
				this.AddWhere(where);
				IEnumerable<IEntidad> res = this.Buscar();
				if(res != null && res.Count() > 0)
				{
					return res.FirstOrDefault() as IProyectos;
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return proyecto;
		}
		public IProyectos GetProyectoByIdCotizacion(int IdCotizacion)
		{
			IProyectos proyecto = null;
			try
			{
				NProyectos p = new NProyectos();
				p.AddWhere(new CPropsWhere()
				{
					Condicion = "=",
					NombreCampo = NProyectos.eFields.IdCotizacion.ToString(),
					Prioridad = true,
					Valor = IdCotizacion
				});
				IEnumerable<IEntidad> res = p.Buscar();
				if(res != null && res.Count() > 0)
				{
					proyecto = res.FirstOrDefault() as IProyectos;
				}				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return proyecto;
		}
		public virtual IPagina<Entity> BuscarIndex<Entity>(int pagina,bool permiso) where Entity : IEntidad, new()
		{
			DProyectos all = new DProyectos();
			if(_data != null)
			{
				all.AddWhere(_data.Where());
			}
			return all.GetProyectos<Entity>(pagina,permiso);
		}
		public override IEnumerable<IEntidad> Buscar()
		{
			IEnumerable<IEntidad> res= base.Buscar();
			IEnumerable<IProyectoContratos> cp = null;
			if(res != null && res.Count() > 0 && !OmiteDetalle)
			{
				IEnumerable<IEntidad> aux = _GetContratosByIds(res.Select(x => ((IProyectos)x).IdProyecto).ToArray());
				if(aux != null && aux.Count()>0)
				{
					cp = aux.Select(x => x as IProyectoContratos);
				}
				res.ToList().ForEach(x =>
				{
					if(cp != null && cp.Count() > 0)
					{
						IEnumerable<IProyectoContratos> cpAux = cp.Where(c => c.IdProyecto == ((IProyectos)x).IdProyecto);
						if(cpAux != null && cpAux.Count() > 0)
						{
							((IProyectos)x).Contratos = cpAux.Select(con => con as CProyectoContratos).ToList();
						}
					}
					if(((IProyectos)x).Contratos != null && ((IProyectos)x).Contratos.Count() > 0)
					{
						((IProyectos)x).Empresa = ((IProyectos)x).Contratos.FirstOrDefault().Empresa;
						((IProyectos)x).Unidad = ((IProyectos)x).Contratos.FirstOrDefault().Unidad;
						((IProyectos)x).RazonSocial = ((IProyectos)x).Contratos.FirstOrDefault().RazonSocial;
					}
				});
			}
			
			return res;
		}
		public IEnumerable<IEntidad> _GetContratosByIds(int[] ids)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{

				Prioridad = true,
				Condicion = "in",
				NombreCampo = NProyectoContratos.eFields.IdProyecto.ToString(),
				Valor = String.Join(",", ids)
			});
			where.Add(new CPropsWhere()
			{

				Operador=DBase.AND,
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NProyectoContratos.eFields.Estatus.ToString(),
				Valor =true
			});
			NProyectoContratos cp = new NProyectoContratos();
			cp.AddWhere(where);
			return cp.Buscar();
		}
		public void AsignaCotizacion(IProyectos proyecto)
		{			
			IProyectos original = this.GetProyectoById(proyecto.IdProyecto);
			NProyectos s = new NProyectos();
			s.AddWhere(new CPropsWhere()
			{
				Valor=proyecto.IdCotizacion,
				NombreCampo=NProyectos.eFields.IdCotizacion.ToString(),
				Prioridad=true,
				Condicion="="
			});
			IEnumerable<IEntidad> res = s.Buscar();
			if(res != null && res.Count() > 0)
			{
				IProyectos pp = res.FirstOrDefault()as IProyectos;
				if(pp == null) pp = new CProyectos();
				SetError(String.Format("{0} {1}",TraduceTexto("IProyectoCotizacionYaExiste"),pp.Proyecto),true);				
			}
			if(original != null && original.IdCotizacion != null)
			{
				SetError(TraduceTexto("IProyectoCotizacionYaTieneCotizacion"), true);
			}
			if(!this.HasError())
			{
				original.IdCotizacion = proyecto.IdCotizacion;
				_data.Save(original);	
			}
		}
		public void SolicitarInsumosByProyecto(IProyectos proyecto,int IdUsuario)
		{
			try
			{
				DProyectos p = new DProyectos();
				p.SolicitarInsumosByProyecto(proyecto, IdUsuario);
			}
			catch(Exception e)
			{
				OnError(e);
			}
		}
		public IEnumerable<IvwContratos2> InsumosByProyecto(int proyecto)
		{
			return this.InsumosByProyecto(new CProyectos()
			{
				IdProyecto = proyecto
			});
		}
		public IEnumerable<IvwContratos2> InsumosByProyectoInsumos(int proyecto)
		{
			IEnumerable<IvwContratos2> result = this.InsumosByProyecto(proyecto);
			List<CContratosDetallesInsumos2> allInsumos = new List<CContratosDetallesInsumos2>();			
			result.ToList().ForEach(co =>
			{
				if(co.Conceptos != null && co.Conceptos.Count > 0)
				{
					co.Conceptos.ForEach(con =>
					{						
						if(con.Insumos != null && con.Insumos.Count > 0)
						{
							con.Insumos.ForEach(ii =>
							{
								allInsumos.Add(ii);
							});
						}
					});
				}
			});
			var consolidadInsumos = allInsumos.GroupBy(i => new
			{
				IdInsumo = i.IdInsumo,
				Codigo = i.Codigo,
				Descripcion = i.Descripcion,
				Unidad = i.Unidad,
				CantidadPorConcepto = i.CantidadPorConcepto,
			}).Select(i => new CContratosDetallesInsumos2()
			{
				IdInsumo = i.Key.IdInsumo,
				Codigo = i.Key.Codigo,
				Descripcion = i.Key.Descripcion,
				Unidad = i.Key.Unidad,				
				Cantidad = i.Sum(x => x.Cantidad),
				CantidadPorConcepto=i.Key.CantidadPorConcepto,
				CantidadEjecutada = i.Sum(x => x.CantidadEjecutada),
				CantidadProgramada = i.Sum(x => x.CantidadProgramada),
				CantidadSolicitada = i.Sum(x => x.CantidadSolicitada),
				CantidadEnviada = i.Sum(x => x.CantidadEnviada),
				CantidadRecibida = i.Sum(x => x.CantidadRecibida),
				SalidaAlmacen = i.Sum(x => x.SalidaAlmacen)
			}).OrderBy(i=>i.Codigo);
			List<IvwContratos2> ByConcepto = new List<IvwContratos2>();
			List<CContratosDetalles2> Concepto = new List<CContratosDetalles2>();
			Concepto.Add(new CContratosDetalles2()
			{
				IdConcepto=0,
				Concepto= TraduceTexto("ReporteByConceptoConceptoUnico"),
				Insumos = consolidadInsumos.ToList()
			});
			ByConcepto.Add(new CvwContratos2()
			{
				Contrato = TraduceTexto("ReporteByConceptoContratoUnico"),
				Fecha = DateTime.Now,
				Conceptos =Concepto
			});
			return ByConcepto;
		}
		public IEnumerable<IvwContratos2> InsumosByProyectoConceptos(int proyecto)
		{
			IEnumerable<IvwContratos2> result = this.InsumosByProyecto(proyecto);
			List<CContratosDetallesInsumos2> allInsumos = new List<CContratosDetallesInsumos2>();
			List<CContratosDetalles2> allConceptos = new List<CContratosDetalles2>();
			result.ToList().ForEach(co =>
			{
				if(co.Conceptos != null && co.Conceptos.Count > 0)
				{
					co.Conceptos.ForEach(con =>
					{
						allConceptos.Add(con);
						if(con.Insumos != null && con.Insumos.Count > 0)
						{
							con.Insumos.ForEach(ii =>
							{
								allInsumos.Add(ii);
							});
						}
					});										
				}
			});
			var consolidada = allConceptos.GroupBy(c => new
			{
				Concepto = c.Concepto,
				IdConcepto = c.IdConcepto				
			}).Select(c => new CContratosDetalles2()
			{
				IdConcepto=c.Key.IdConcepto,
				Concepto = c.Key.Concepto,						
				IdContratosDetalles=c.Key.IdConcepto,
				Ubicacion=String.Join(",",c.Select(x=>x.Ubicacion)),
				Cantidad = c.Sum(x=>x.Cantidad),
				CantidadProgramada = c.Sum(x=>x.CantidadProgramada),
				CantidadEjecutada = c.Sum(x=>x.CantidadEjecutada),
				Ots=c.Sum(x=>x.Ots)				
			});
			var consolidadInsumos = allInsumos.GroupBy(i => new
			{
				IdConcepto = i.IdConcepto,
				Codigo =i.Codigo,
				Descripcion=i.Descripcion,
				Unidad=i.Unidad			,
				CantidadPorConcepto = i.CantidadPorConcepto
			}).Select(i => new CContratosDetallesInsumos2()
			{
				IdConcepto=i.Key.IdConcepto,
				Codigo=i.Key.Codigo,
				Descripcion=i.Key.Descripcion,
				Unidad =i.Key.Unidad,
				CantidadPorConcepto = i.Key.CantidadPorConcepto,
				Cantidad = i.Sum(x=>x.Cantidad),				
				CantidadEjecutada = i.Sum(x => x.CantidadEjecutada),
				CantidadProgramada = i.Sum(x => x.CantidadProgramada)	,
				CantidadSolicitada = i.Sum(x => x.CantidadSolicitada),
				CantidadEnviada = i.Sum(x => x.CantidadEnviada),
				CantidadRecibida = i.Sum(x => x.CantidadRecibida),
				SalidaAlmacen = i.Sum(x => x.SalidaAlmacen)
			});
			List<CContratosDetalles2> alConceptos2 =consolidada.ToList();
			alConceptos2.ForEach(co =>
			{
				var aux= consolidadInsumos.Where(i => i.IdConcepto == co.IdConcepto);
				if(aux != null && aux.Count() > 0)
				{
					co.Insumos = aux.OrderBy(x=>x.Codigo).ToList();
				}
			});
			List<IvwContratos2> ByConcepto = new List<IvwContratos2>();
			ByConcepto.Add(new CvwContratos2()
			{
				Contrato=TraduceTexto("ReporteByConceptoContratoUnico"),
				Fecha=DateTime.Now,
				Conceptos = alConceptos2
			});
			return ByConcepto;
		}
		public IEnumerable<IvwContratos2> InsumosByProyecto(IProyectos proyecto)
		{
			try
			{
				DProyectos p = new DProyectos();
				return p.InsumosByProyecto(proyecto);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return null;
		}

        public IEnumerable<IContratosDetalles2> ConceptosPorContratos(int idProyecto, string contratos)
        {
            try
            {
                DProyectos p = new DProyectos();
                return p.ConceptosPorContratos(idProyecto, contratos);
            }
            catch (Exception e)
            {
                OnError(e);
            }
            return null;
        }
		  public IEnumerable<IContratosDetalles2> ConceptosPorContratosInstalado(int idProyecto, string contratos)
		  {
			  try
			  {
				  DProyectos p = new DProyectos();
				  return p.ConceptosPorContratosInstalado(idProyecto, contratos);
			  }
			  catch(Exception e)
			  {
				  OnError(e);
			  }
			  return null;
		  }

          public IEnumerable<IContratosDetalles2> ConceptosPorContratosSolicitadoInstalado(int idProyecto, string contratos, DateTime fechaInicial, DateTime fechaFinal)
          {
              try
              {
                  DProyectos p = new DProyectos();
                  return p.ConceptosPorContratosSolicitadoInstalado(idProyecto, contratos, fechaInicial, fechaFinal);
              }
              catch (Exception e)
              {
                  OnError(e);
              }
              return null;
          }

          public IEnumerable<IContratosDetalles2> ConceptosPorContratosSolicitadoEstado(int idProyecto, string contratos)
          {
              try
              {
                  DProyectos p = new DProyectos();
                  return p.ConceptosPorContratosSolicitadoEstado(idProyecto, contratos);
              }
              catch (Exception e)
              {
                  OnError(e);
              }
              return null;
          }

          public IEnumerable<IContratosDetalles2> ConceptosPorContratosSolicitadoArea(int idProyecto, string ubicaciones)
          {
              try
              {
                  DProyectos p = new DProyectos();
                  return p.ConceptosPorContratosSolicitadoArea(idProyecto, ubicaciones);
              }
              catch (Exception e)
              {
                  OnError(e);
              }
              return null;
          }
          public IEnumerable<IContratosDetalles2> ConceptosPorContratosInstaladoArea(int idProyecto, string ubicaciones)
          {
              try
              {
                  DProyectos p = new DProyectos();
                  return p.ConceptosPorContratosInstaladoArea(idProyecto, ubicaciones);
              }
              catch (Exception e)
              {
                  OnError(e);
              }
              return null;
          }

          public IEnumerable<IContratosDetalles2> ConceptosSolicitadoEstadoArea(int idProyecto, string contratos)
          {
              try
              {
                  DProyectos p = new DProyectos();
                  return p.ConceptosSolicitadoEstadoArea(idProyecto, contratos);
              }
              catch (Exception e)
              {
                  OnError(e);
              }
              return null;
          }

          public IEnumerable<IContratosDetalles2> ConceptosSolicitadoInstaladoArea(int idProyecto, string contratos, DateTime fechaInicial, DateTime fechaFinal)
          {
              try
              {
                  DProyectos p = new DProyectos();
                  return p.ConceptosSolicitadoInstaladoArea(idProyecto, contratos, fechaInicial, fechaFinal);
              }
              catch (Exception e)
              {
                  OnError(e);
              }
              return null;
          }

		protected IEnumerable<IContratosDetalles2> _InsumosByProyectoFechaConcepto(IProyectos proyecto)
		{
			try
			{
				DProyectos p = new DProyectos();
				return p.InsumosByProyectoFecha(proyecto.IdProyecto,proyecto.FechaInicial,proyecto.FechaFinal);
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return null;
		}
		public IEnumerable<IContratosDetalles2> SolicitudInsumosInstaladoFechaFiltro(IProyectos proyecto)
		{
			try
			{
				return _InsumosByProyectoFechaConcepto(proyecto);			
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return null;
		}
		public IEnumerable<IContratosDetalles2> InstaladoFechaConceptos(IProyectos proyecto)
		{
			try
			{
				IEnumerable<IContratosDetalles2> conceptos = _InsumosByProyectoFechaConcepto(proyecto);
				if(conceptos != null)
				{
					var consolidada = conceptos.GroupBy(c => new
					{
						Concepto = c.Concepto,
						Descripcion=c.Descripcion,
						IdConcepto = c.IdConcepto
					}).Select(c => new CContratosDetalles2()
					{
						IdConcepto = c.Key.IdConcepto,
						Concepto = c.Key.Concepto,
						Descripcion=c.Key.Descripcion,
						IdContratosDetalles = c.Key.IdConcepto,
						Ubicacion = String.Join(",", c.Select(x => x.Ubicacion)),
						Cantidad = c.Sum(x => x.Cantidad),
						CantidadProgramada = c.Sum(x => x.CantidadProgramada),
						CantidadEjecutada = c.Sum(x => x.CantidadEjecutada),						
						Ots = c.Sum(x => x.Ots)						
					});
					if(consolidada != null)
					{
						return consolidada.Select(x=>x as IContratosDetalles2);
					}
				}				
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return null;
		}
		public IEnumerable<IContratosDetallesInsumos2> InstaladoFechaInsumos(IProyectos proyecto)
		{
			try
			{
				List<CContratosDetallesInsumos2> allInsumos = new List<CContratosDetallesInsumos2>();
				IEnumerable<IContratosDetalles2> conceptos = _InsumosByProyectoFechaConcepto(proyecto);
				conceptos.ToList().ForEach(co =>
				{				
					if(co.Insumos != null && co.Insumos.Count > 0)
					{
						co.Insumos.ForEach(ii =>
						{
							allInsumos.Add(ii);
						});
					}					
				});
				if(allInsumos != null)
				{
					var consolidadInsumos = allInsumos.GroupBy(i => new
					{
						IdConcepto = i.IdConcepto,
						Codigo = i.Codigo,
						Descripcion = i.Descripcion,
						Unidad = i.Unidad,
						CantidadPorConcepto = i.CantidadPorConcepto
					}).Select(i => new CContratosDetallesInsumos2()
					{
						IdConcepto = i.Key.IdConcepto,
						Codigo = i.Key.Codigo,
						Descripcion = i.Key.Descripcion,
						Unidad = i.Key.Unidad,
						CantidadPorConcepto = i.Key.CantidadPorConcepto,
						Cantidad = i.Sum(x => x.Cantidad),
						CantidadEjecutada = i.Sum(x => x.CantidadEjecutada),
						CantidadProgramada = i.Sum(x => x.CantidadProgramada),
						CantidadSolicitada = i.Sum(x => x.CantidadSolicitada),
						CantidadEnviada = i.Sum(x => x.CantidadEnviada),
						CantidadRecibida = i.Sum(x => x.CantidadRecibida),
						SalidaAlmacen =  i.Sum(x => x.SalidaAlmacen)
					});
					if(consolidadInsumos != null)
					{
						return consolidadInsumos.Select(x => x as IContratosDetallesInsumos2).OrderBy(x=>x.Codigo);
					}
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return null;
		}

		public IEnumerable<IProyectos> ConsultaInstaladoSolicitado(CFiltrosInstaladoSolicitado filtros)
		{
			IEnumerable<IProyectos> Result = null;
			List<CPropsWhere> where = new List<CPropsWhere>();
			CPropsWhere fecha= new CPropsWhere();
			CPropsWhere proyecto = new CPropsWhere();
			CPropsWhere cliente = new CPropsWhere();
			fecha.OmiteParametro = true;
			fecha.Prioridad=true;
			fecha.Condicion = "between";
			fecha.OmiteConcatenarTabla = true;
			fecha.NombreCampo = "CAST(Proyectos.FechaInicial AS date)";
			fecha.Valor = new object[] { String.Format("'{0}'", filtros.FechaIni.ToString("d")), 
				                          String.Format("'{0}'", filtros.FechaFin.ToString("d")) };
			if(!String.IsNullOrEmpty(filtros.Proyecto))
			{
				fecha.OmiteParametro = true;
				proyecto.Prioridad = true;
				proyecto.Condicion = "contenga";
				proyecto.NombreCampo = NProyectos.eFields.Proyecto.ToString();
				proyecto.Valor = filtros.Proyecto;
			}
			else
			{
				proyecto = null; 
			}
			if(filtros.IdCliente>0)
			{
				cliente.Prioridad = true;
				cliente.Condicion = "=";
				cliente.NombreCampo = NProyectos.eFields.IdCliente.ToString();
				cliente.Valor = filtros.IdCliente;
			}
			else
			{
				cliente = null;
			}
			where.Add(fecha);
			if(proyecto != null) where.Add(proyecto);
			if(cliente != null) where.Add(cliente);
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<IEntidad> res = base.Buscar();
			if(res != null && res.Count() > 0)
			{
				Result= res.Select(x => x as IProyectos);
				Result.ToList().ForEach(p =>
				{
					var res1 = this.InsumosByProyecto(p);
					if(res1 != null && res.Count() > 0)
					{
						p.vwContratos = res1.Select(x => x as CvwContratos2).ToList();
					}
				});
			}
			return Result;
		}
		protected void BuildWhereCostos(MProyectosCostos Modelo)
		{
			this.ClearWhere();			
			switch(Modelo.Tipo)
			{
				case 1: //Muestra el listado pero en base a la unidad (Proyectos por unidad)	
					this.AddWhere(FiltrosEmpresaUnidad(Modelo.IdEmpresa, Modelo.IdUnidad));
					break;
				case 2: //Muestra el listado pero en base a la empresa(Proyectos por empresa)
					this.AddWhere(FiltrosEmpresa(Modelo.IdEmpresa));
					break;
			}
			if(Modelo != null && Modelo.Where != null && Modelo.Where.Count > 0)
			{
				foreach(IPropsWhere where in Modelo.Where)
				{					
					if(where.Valor != null && !String.IsNullOrEmpty(where.NombreCampo))
					{
						if(where.ValidaConsultaMVC(this.BuildEntity()))
						{
							/*if(!String.IsNullOrEmpty(where.Plantilla))
							{
								where.Valor = String.Format(where.Plantilla, where.Valor.ToString());
							}*/
							this.AddWhere(where);
						}
						
					}
				}
			}
		}
		protected IEnumerable<IProyectos> ObtieneProyectosParaCostos(MProyectosCostos Model)
		{
			this.OmiteDetalle = true;
			BuildWhereCostos(Model);
			/*this.ClearWhere();
			if(Model != null && Model.Where != null && Model.Where.Count > 0)
			{
				foreach(IPropsWhere where in Model.Where)
				{
					if(where.Valor != null && !String.IsNullOrEmpty(where.NombreCampo))
					{
						this.AddWhere(where);
					}
				}
			}			 
			switch(Model.Tipo)
			{
				case 1: //Muestra el listado pero en base a la unidad (Proyectos por unidad)	
					this.AddWhere(FiltrosEmpresaUnidad(Model.IdEmpresa, Model.IdUnidad));
					break;
				case 2: //Muestra el listado pero en base a la empresa(Proyectos por empresa)
					this.AddWhere(FiltrosEmpresa(Model.IdEmpresa));
					break;
			}*/
			IEnumerable<IEntidad> res = this.Buscar();
			if(res == null || res.Count() <= 0) return null;
			return res.Select(x=>x as IProyectos);
		}
		protected void ObtieneCostosProyectos(IEnumerable<IProyectos> Proyectos)
		{
			DProyectos dProyectosCostos = new DProyectos();
			dProyectosCostos.MainCostosProyectos(Proyectos); //Obtenemos los costos de los contratos que forman al proyecto.
		}
		protected IEnumerable<IProyectos> ConvierteImportes(MProyectosCostos Modelo,IEnumerable<IProyectos> Proyectos)
		{
			if(Modelo == null || Modelo.MonedaDestino == null || Modelo.MonedaDestino.IdMonedaOrigen <= 0) return null;
			NMonedas nmoneda = new NMonedas();			
			IMonedas moneda = nmoneda.GetById(Modelo.MonedaDestino.IdMonedaOrigen);
			Modelo.MonedaDestino.CodigoMonedaDestino = moneda.Codigo;
			Modelo.MonedaDestino.MonedaDestino = moneda.Moneda;
			ITipoCambios tipoCambioDestino = null;
			List<IProyectos> ResultadoFinal = new List<IProyectos>();
			if(Proyectos != null && Proyectos.Count() > 0 && Modelo.MonedaDestino != null)
			{
				Proyectos.ToList().ForEach(p =>
				{
					IProyectos pConverido = this.BuildEntity() as IProyectos;
					if(pConverido != null)
					{
						pConverido.IdProyecto = p.IdProyecto;
						pConverido.CostosGlobales = p.CostosGlobales;
						if(pConverido.CostosGlobales != null && pConverido.CostosGlobales.Count > 0)
						{
							pConverido.CostosGlobales.ForEach(g =>
							{
								if(g.IdMoneda != Modelo.MonedaDestino.IdMonedaOrigen)
								{

									g.Codigo = moneda.Codigo;
									g.Moneda = moneda.Moneda;
									var destino = Modelo.TipoCambios.Where(pp => pp.IdMonedaDestino == g.IdMoneda);
									if(destino != null && destino.Count() > 0)
									{
										tipoCambioDestino = destino.FirstOrDefault();
									}
									if(tipoCambioDestino != null)
									{
										g.Total = g.Total / tipoCambioDestino.Valor;
										g.IdMoneda = moneda.IdMoneda;										
									}
								}
							});
							pConverido.CostosGlobales = pConverido.CostosGlobales.GroupBy(mm => new
							{
								Codigo = mm.Codigo,
								Moneda = mm.Moneda,
								IdMoneda = mm.IdMoneda
							}).Select(m => new CProyectosCostos()
							{
								Codigo = m.Key.Codigo,
								Moneda = m.Key.Moneda,
								IdMoneda = m.Key.IdMoneda,
								Total = m.Sum(x => x.Total),
							}).OrderBy(x => x.IdMoneda).ToList();							
						}
						ResultadoFinal.Add(pConverido);
					}
				});
			}
			return ResultadoFinal;
		}
		public IEnumerable<IProyectos> ConvierteImportes(MProyectosCostos Modelo,
			                                              bool hasPermiso)
		{
			if(Modelo == null) return null;
			IEnumerable<ITipoCambios> tipoCambios = Modelo.TipoCambios.Where(t => t.Valor > 0 && t.IdMonedaDestino > 0 && t.IdMonedaOrigen > 0);
			if(tipoCambios == null || tipoCambios.Count() <= 0) return null;
			Modelo.TipoCambios = tipoCambios.Select(x=>x as CTipoCambios).ToList();
			IEnumerable<IProyectos> ListadoProyectos = ObtieneProyectosParaCostos(Modelo);
			ObtieneCostosProyectos(ListadoProyectos);
			ListadoProyectos = ConvierteImportes(Modelo,ListadoProyectos);
			return ListadoProyectos;			
		}

     

		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IProyectos", new DProyectos());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		protected bool ValidaProyecto()
		{
			if(_data.Delete == 1) return true;
			if(_Entity != null)
			{
				IProyectos proyecto = _Entity as IProyectos;
				if(proyecto.Contratos != null && proyecto.Contratos.Count>0)
				{					
					var newProyectos = proyecto.Contratos.Where(x => x.IdProyectoContrato >= 0);
					if(newProyectos != null && newProyectos.Count() > 0)
					{
						proyecto.IdCliente = newProyectos.First().IdCliente;
						var diferentes = newProyectos.Where(x => x.IdCliente != proyecto.IdCliente );
						if(diferentes.Any())
						{
							SetError(TraduceTexto("IProyectoClientesDiferentes"));
							return false;
						}
					}
				}
				if(proyecto.IdCliente<=0)
				{
					SetError(TraduceTexto("IProyectoSeleccioneUnCliente"));
					return false;
				}
				if(String.IsNullOrEmpty(proyecto.Proyecto))
				{
					SetError(TraduceTexto("IProyectoSinProyecto"));
					return false;
				}
			}
			return true;
		}
		public virtual IEnumerable<IProyectos> GetReporteCostos(MProyectosCostos Modelo,bool permiso)
		{
			if(!permiso) return null;
			BuildWhereCostos(Modelo);
			IPagina<CProyectos> _pagina = BuscarIndex<CProyectos>(0, permiso);
			if(_pagina != null)
			{
				IEnumerable<IProyectos> costosConversion = ConvierteImportes(Modelo, permiso);
				return DesagrupaProyectos(_pagina.Contenido, costosConversion,Modelo.MonedaDestino);
			}
			return null;
		}
		protected IEnumerable<IProyectos> DesagrupaProyectos(IEnumerable<IProyectos> Proyectos, 
																			  IEnumerable<IProyectos> Conversion,
																			ITipoCambios MonedaDestino)
		{
			if(Proyectos == null || Proyectos.Count() <= 0) return null;
			List<IProyectos> proyectos = new List<IProyectos>();
			Proyectos.ToList().ForEach(x =>
			{
				if(x.CostosGlobales != null && x.CostosGlobales.Count > 0)
				{
					x.CostosGlobales.ForEach(c =>
					{
						IProyectos pp = BuildEntity() as IProyectos;
						pp.IdProyecto =x.IdProyecto;
						pp.Proyecto = x.Proyecto;
						pp.Descripcion = x.Descripcion;
						pp.RazonSocial = x.RazonSocial;
						pp.Cotizacion = x.Cotizacion;
						pp.Ubicacion = x.Ubicacion;
						pp.FechaInicial = x.FechaInicial;
						pp.FechaFinal = x.FechaFinal;
						pp.MonedaCosto = c.Codigo;
						pp.CostoProyecto = c.Total;
						if(Conversion != null && Conversion.Count() > 0)
						{
							var convertido = Conversion.Where(cx => cx.IdProyecto == pp.IdProyecto);
							if(convertido!= null && convertido.Count()>0)
							{
								pp.MonedaConversion = String.Format(TraduceTexto("IProyectosTotalConversion") + " {0} ", MonedaDestino.CodigoMonedaDestino);
								if(convertido.FirstOrDefault().CostosGlobales != null && convertido.FirstOrDefault().CostosGlobales.Count>0)
								{
									var existe = proyectos.Where(xx => xx.IdProyecto == pp.IdProyecto);
									if(!(existe!=null && existe.Count()>0))
									{
										pp.MonedaConversionCosto = convertido.FirstOrDefault().CostosGlobales.FirstOrDefault().Total;
									}									
								}								
							}
						}
						proyectos.Add(pp);
					});					
				}
			});
			return proyectos;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			if(ValidaProyecto())
			{
				if(_data == null) return;
				_data.Save(_Entity);				
			}
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;	
			if(ValidaProyecto())
			{
				if(_data == null) return;
				_data.Save(_Entity);				
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}		
	}// NProyectos ends.        
}// LibNegocio.Entidades ends.