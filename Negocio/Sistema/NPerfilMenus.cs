using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 04/01/2017 10:37:33 a. m.*/
namespace Negocio
{
	public partial class NPerfilMenus : NBase
	{
		public enum eFields
		{
			IdPerfilModulo = 0, IdPerfil, IdMenu, Estatus
		}
		public NPerfilMenus()
		{
		}
		public NPerfilMenus(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DPerfilMenus();
		}
	}// NPerfilMenus ends.        
}// LibNegocio.Entidades ends.
