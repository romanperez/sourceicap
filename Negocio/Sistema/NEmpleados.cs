using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{      
    public partial class NEmpleados : NBase
    {
		  public enum eFields {IdEmpleado=0,IdDepartamento,Nombre,ApellidoPaterno,ApellidoMaterno,
		  Matricula, Email, Telefono, Celular, Estatus
		  }
		  public NEmpleados(){}
		  public NEmpleados(IEntidad entidad): base(entidad){}
		  protected override IBase BuildDataEntity()
		  {
			  DEmpleados empleado = new DEmpleados();
			  DDepartamentos departamento = new DDepartamentos();
			  DUnidades unidad = new DUnidades();
			  DEmpresas empresa=new DEmpresas();
			  string[] camposJoin = new string[]
			  {
				String.Format("{0}.{1} AS Departamento{2}",departamento.Tabla,NDepartamentos.eFields.Nombre.ToString(),NDepartamentos.eFields.Nombre.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.Unidad.ToString())
			  };
			  string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Departamento*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,departamento.Tabla,DBase.On, 
				   String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdDepartamento.ToString()),
					String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdDepartamento.ToString())),
					/*Join with Unidades*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),
					/*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()))
			  };
			  empleado.SetConfigJoins(camposJoin, condicionesJoin);
			  return empleado;
		  }
		  public virtual IEnumerable<IPropsWhere> FiltroIndex(IUsuarios usuario, string valor)
		  {
			  List<IPropsWhere> where = new List<IPropsWhere>();
			  DEmpresas emp = new DEmpresas();
			  where.Add(new CPropsWhere()
			  {
				  OmiteConcatenarTabla = true,
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = String.Format("{0}.{1}", emp.Tabla, NEmpresas.eFields.IdEmpresa.ToString()),
				  Valor = valor
			  });
			  return where;
		  }	
	 	 public string DataNombreEmpleado()
		 {
			 DEmpleados empleado = new DEmpleados();	
			 string Nombre = String.Format("{0}.{1}", empleado.Tabla, NEmpleados.eFields.Nombre.ToString());
			 string APaterno = String.Format("{0}.{1}", empleado.Tabla, NEmpleados.eFields.ApellidoPaterno.ToString());
			 string AMaterno = String.Format("{0}.{1}", empleado.Tabla, NEmpleados.eFields.ApellidoMaterno.ToString());
			 return String.Format("{0} + ' ' + {1} + ' ' + {2} ", Nombre, APaterno, AMaterno);			 
		 }
		 protected virtual bool ValidaDuplicado(bool isNew)
		 {
			 bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IEmpleados", new DEmpleados());
			 if(isDuplicado)
			 {
				 SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			 }
			 return isDuplicado;
		 }
		 protected bool ValidaCaracteres(string Cadena,string[]NoPermitidos)
		 {
			 bool valido = true;
			 Cadena.ToList().ForEach(c =>
			 {
				 int coincidencias = NoPermitidos.Where(letra => letra[0] == c).Count();
				 if(coincidencias > 0) valido=false;
			 });
			 return valido;
		 }
		 protected bool ValidaEmpleado()
		 {
			 try
			 {
				 //EMPLEADOS_NO_VALID
				 bool NombreValido = true;
				 IParametros no_valid = NParametros.GetByNombre("EMPLEADOS_NO_VALID");
				 IParametros separador = NParametros.GetByNombre("RFC_SEPARADOR");
				 IEmpleados empleado = _Entity as IEmpleados;
				 string[] noValidos = no_valid.Valor.Split(separador.Valor[0]);
				 if(noValidos != null && noValidos.Count() > 0)
				 {
					 NombreValido = ValidaCaracteres(empleado.NombreCompleto(), noValidos);
					 if(!NombreValido)
					 {
						 SetError(String.Format(TraduceTexto("NEmpleadosNombreIncorrecto"),empleado.NombreCompleto()));
						 return false;
					 }
				 }

			 }
			 catch(Exception e)
			 {
				 OnError(e);
			 }
			 return true;
		 }
		 public override bool ValidaSave()
		 {
			 return ValidaEmpleado();
		 }
		 public override bool ValidaUpdate()
		 {
			 return ValidaEmpleado();
		 }
		 public override void Save()
		 {
			 if(ValidaDuplicado(true)) return;
			 base.Save();
		 }
		 public override void Update()
		 {
			 if(ValidaDuplicado(false)) return;
			 base.Update();
		 }
		 public override void Delete()
		 {
			 if(ValidaDelete())
			 {
				 if(_data == null) return;
				 _data.Delete = 1;
				 _data.Save(_Entity);
			 }
		 }

         public static IEnumerable<IEmpleados> GetEmpleadosFromTable(System.Data.DataTable EmpleadosTbl)
         {
             if (EmpleadosTbl == null) return null;
             List<IEmpleados> Departamentos = new List<IEmpleados>();
             for (int rw = 0; rw < EmpleadosTbl.Rows.Count; rw++)
             {
                 Departamentos.Add(new CEmpleados()
                 {
                     Unidad = EmpleadosTbl.Rows[rw].Field<string>("UNIDAD"),
                     DepartamentoNombre = EmpleadosTbl.Rows[rw].Field<string>("DEPARTAMENTO"),
                     Nombre = EmpleadosTbl.Rows[rw].Field<string>("NOMBRE"),
                     ApellidoPaterno = EmpleadosTbl.Rows[rw].Field<string>("APELLIDO_PATERNO"),
                     ApellidoMaterno = EmpleadosTbl.Rows[rw].Field<string>("APELLIDO_MATERNO"),
                     Matricula = EmpleadosTbl.Rows[rw].Field<string>("MATRICULA"),
                     Email = EmpleadosTbl.Rows[rw].Field<string>("CORREO_ELECTRONICO"),
                     Telefono = EmpleadosTbl.Rows[rw].Field<string>("TELEFONO_OFICINA"),
                     Celular = EmpleadosTbl.Rows[rw].Field<string>("TELEFONO_MOVIL"),
                 });
             }
             return Departamentos;
         }


         public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
         {
             entities.ToList().ForEach(
             x =>
             {
                 _Entity = x;
                 this.Save();
                 if (HasError())
                 {
                     ((IEmpleados)x).Empresa = this.GetErrorMessage();
                     this.ClearError();
                 }
             });
             return entities;
         }

         public IEnumerable<IEmpleados> SaveMasivo2(IEnumerable<IEmpleados> Empleados, int IdEmpresa)
         {
             try
             {
                 if (Empleados == null || Empleados.Count() <= 0) return null;

                 DUnidades du = new DUnidades();
                 DDepartamentos dd = new DDepartamentos();
                 DEmpresas de = new DEmpresas();
                 string[] Unidades = Empleados.Where(i => !(String.IsNullOrEmpty(i.Unidad))).Select(i => String.Format("'{0}'", i.Unidad)).ToArray();
                 string[] Departamentos = Empleados.Where(i => !(String.IsNullOrEmpty(i.Unidad))).Select(i => String.Format("'{0}'", i.DepartamentoNombre)).ToArray();
                 string InCondicion = String.Empty;
                 IEnumerable<IUnidades> lstUnidades = new List<IUnidades>();
                 IEnumerable<IDepartamentos> lstDepartamentos = new List<IDepartamentos>();
                 IEnumerable<IEntidad> Res = null;
                 NUnidades nU = new NUnidades();
                 NDepartamentos nD = new NDepartamentos();

                 List<IPropsWhere> where = new List<IPropsWhere>();
                 CPropsWhere whereEmpresa = new CPropsWhere();
                 whereEmpresa.Condicion = "=";
                 whereEmpresa.OmiteConcatenarTabla = true;
                 whereEmpresa.Prioridad = true;
                 whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
                 whereEmpresa.Valor = IdEmpresa;

                 CPropsWhere whereCampo = new CPropsWhere();
                 whereCampo.Condicion = "in";
                 whereCampo.OmiteConcatenarTabla = true;
                 whereCampo.Prioridad = true;

                 //Buscando Unidades
                 InCondicion = String.Join(",", Unidades);
                 whereCampo.NombreCampo = String.Format("{0}.{1}", du.Tabla, NUnidades.eFields.Unidad.ToString());
                 whereCampo.Valor = InCondicion;
                 where = new List<IPropsWhere>();
                 where.Add(whereEmpresa);
                 where.Add(whereCampo);
                 nU.AddWhere(where);
                 Res = nU.Buscar();
                 if (Res != null && Res.Count() > 0)
                 {
                     lstUnidades = Res.Select(x => x as IUnidades);
                 }

                 //Buscando Departamentos
                 InCondicion = String.Join(",", Departamentos);
                 whereCampo.NombreCampo = String.Format("{0}.{1}", dd.Tabla, NDepartamentos.eFields.Nombre.ToString());
                 whereCampo.Valor = InCondicion;
                 where = new List<IPropsWhere>();
                 where.Add(whereEmpresa);
                 where.Add(whereCampo);
                 nD.AddWhere(where);
                 Res = nD.Buscar();
                 if (Res != null && Res.Count() > 0)
                 {
                     lstDepartamentos = Res.Select(x => x as IDepartamentos);
                 }

                 Empleados.ToList().ForEach(i =>
                 {
                     var resF = (!String.IsNullOrEmpty(i.Unidad)) ? lstUnidades.Where(f => f.Unidad.ToLower() == i.Unidad.ToLower() && f.IdEmpresa == IdEmpresa) : null;
                     var resD = (!String.IsNullOrEmpty(i.DepartamentoNombre)) ? lstDepartamentos.Where(f => f.Nombre.ToLower() == i.DepartamentoNombre.ToLower() && f.IdEmpresa == IdEmpresa) : null;

                     if (resF != null && resF.Count() > 0)
                     {
                         i.IdUnidad = resF.FirstOrDefault().IdUnidad;
                     }

                     if (resD != null && resD.Count() > 0)
                     {
                         i.IdDepartamento = resD.FirstOrDefault().IdDepartamento;
                     }

                 });

                 Empleados.ToList().ForEach(i =>
                 {
                     i.IdEmpresa = IdEmpresa;
                     i.Estatus = true;
                 });
                 this.SaveMasivo(Empleados);
                 Empleados = Empleados.Where(i => i.IdEmpleado <= 0);
             }
             catch (Exception e)
             {
                 OnError(e);
             }

             return Empleados;
         }
    }// NEmpleados ends.        
}// LibNegocio.Entidades ends.
