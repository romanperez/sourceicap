using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/01/2017 02:07:44 p. m.*/
namespace Negocio
{

	public partial class NCapitulos : NBase
	{
		public enum eFields
		{
			IdCapitulo = 0, IdEmpresa, Capitulo, Estatus
		}
		public NCapitulos()
		{
		}
		public NCapitulos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DCapitulos capitulo = new DCapitulos();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.IdEmpresa.ToString()))
			};
			capitulo.SetConfigJoins(camposJoin, condicionesJoin);
			return capitulo;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "ICapitulos", new DCapitulos());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
	}// NCapitulos ends.        
}// LibNegocio.Entidades ends.
