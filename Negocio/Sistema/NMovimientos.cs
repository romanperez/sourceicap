using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{
    public partial class NMovimientos : NBase
    {		 
        public enum eFields
        {
			  IdMovimiento = 0, IdEstatus, Fecha, IdAlmacen, IdAlmacenDestino, IdUsuario, Estatus,Tipo
        }
        public enum eTipoMovimientos
        {
            Entrada = 1, Salida, Traspaso
        }
        public NMovimientos(IEntidad entidad)
            : base(entidad)
        {
        }
        protected override IBase BuildDataEntity()
		  {
		  //	 DMovimientos movimiento = new DMovimientos();			  
		  //	 DAlmacenes almacen = new DAlmacenes();
			  
		  //	 string[] camposJoin = new string[]
		  //	{								
		  //	 String.Format("{0}.{1}",almacen.Tabla,NAlmacenes.eFields.IdUnidad.ToString())
		  //	};
		  //	 string[] condicionesJoin = new string[] 
		  //	{ 
		  //		 /*Join with Unidades*/
		  //		 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,almacen.Tabla,DBase.On, 
		  //		 String.Format("{0}.{1}",almacen.Tabla,NAlmacenes.eFields.IdAlmacen.ToString()),
		  //		 String.Format("{0}.{1}",movimiento.Tabla,NMovimientos.eFields.IdAlmacen.ToString()))					
		  //	};
		  //	 movimiento.SetConfigJoins(camposJoin, condicionesJoin);
		  //	 return movimiento;
			   DMovimientos movimiento = new DMovimientos();	 
			   DAlmacenes Almacenes = new DAlmacenes();
				DAlmacenes AlmaceneDestino = new DAlmacenes();
				DUnidades Unidades =new DUnidades();
				DEmpresas Empresas = new DEmpresas();
				DUsuarios Usuario = new DUsuarios();
				string[] camposJoin = new string[]
			  	{								
			  		String.Format("{0}.{1}",Almacenes.Tabla,NAlmacenes.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",Almacenes.Tabla,NAlmacenes.eFields.Almacen.ToString()),
					String.Format("{0}.{1}",Unidades.Tabla,NUnidades.eFields.Unidad.ToString()),
					String.Format("{0}.{1}",Usuario.Tabla,NUsuarios.eFields.Usuario.ToString()),
					String.Format("{0}.{1} as AlmacenDestino","alm2",NAlmacenes.eFields.Almacen.ToString()),
					"[dbo].[fnMovimientosContratos](vwProyecto.IdProyecto,movimientos.IdMovimiento) as Contrato",
					"vwProyecto.Proyecto"
				};
				string[] condicionesJoin = new string[] 
			  	{ 
			  		 /*Join with Almacenes*/
			  		 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,Almacenes.Tabla,DBase.On, 
			  		 String.Format("{0}.{1}",Almacenes.Tabla,NAlmacenes.eFields.IdAlmacen.ToString()),
			  		 String.Format("{0}.{1}",movimiento.Tabla,NMovimientos.eFields.IdAlmacen.ToString())),
					  /*Join with Almacenes*/
			  		 String.Format("{0} {1} alm2 {2} {3} = {4}", DBase.LeftJoin,AlmaceneDestino.Tabla,DBase.On, 
			  		 String.Format("{0}.{1}","alm2",NAlmacenes.eFields.IdAlmacen.ToString()),
			  		 String.Format("{0}.{1}",movimiento.Tabla,NMovimientos.eFields.IdAlmacenDestino.ToString())),
					 /*Join with Unidades*/
			  		 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,Unidades.Tabla,DBase.On, 
			  		 String.Format("{0}.{1}",Unidades.Tabla,NUnidades.eFields.IdUnidad.ToString()),
			  		 String.Format("{0}.{1}",Almacenes.Tabla,NAlmacenes.eFields.IdUnidad.ToString())),
					 /*Join with Empresas*/
			  		 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,Empresas.Tabla,DBase.On, 
			  		 String.Format("{0}.{1}",Empresas.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
			  		 String.Format("{0}.{1}",Unidades.Tabla,NUnidades.eFields.IdEmpresa.ToString())),
					 /*Join with Usuarios*/
			  		 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,Usuario.Tabla,DBase.On, 
			  		 String.Format("{0}.{1}",Usuario.Tabla,NUsuarios.eFields.IdUsuario.ToString()),
			  		 String.Format("{0}.{1}",movimiento.Tabla,NMovimientos.eFields.IdUsuario.ToString())),
					 " left join (select p.IdProyecto,IdMovimiento ,p.Proyecto "  +
				    " from MovimientosContratos mc "+
				    " inner join Proyectos p on p.IdProyecto = mc.IdProyecto " +
				    " group by p.IdProyecto,IdMovimiento ,p.Proyecto)vwProyecto on vwProyecto.IdMovimiento= " + movimiento.Tabla +"."+ NMovimientos.eFields.IdMovimiento.ToString()
			  	};
				movimiento.SetConfigJoins(camposJoin, condicionesJoin);
			  return movimiento;
        }
        public NMovimientos()
        {
        }
		  public IEnumerable<IMovimientos> BuscaMovimiento(CFiltrosMovimiento Entity)
		  {
			  if (_data != null)
				  return ((DMovimientos)(_data)).BuscaMovimiento(Entity);
			  else return null;
		  }
        public IPagina<CMovimientos> GetPageMovimientosByTipo(int TipoMovimiento, int Pagina, IEnumerable<CPropsWhere> where)
        {
            //if (_data != null)
            //   return ((DMovimientos)(_data)).GetPageMovimientosByTipo(TipoMovimiento, Pagina, where);
            //else return null;
			  List<CPropsWhere> where2 = new List<CPropsWhere>();
			  where2.Add(new CPropsWhere()
			  {
				  Prioridad = true,
				  NombreCampo = NMovimientos.eFields.Tipo.ToString(),
				  Valor = TipoMovimiento,
				  Operador = DBase.AND,
				  Condicion = "="
			  });
			  where.ToList().ForEach(x =>
			  {
				  x.Prioridad = true;
				  x.Operador = DBase.AND;
				  x.Condicion = "=";
				  x.OmiteConcatenarTabla = true;
				  where2.Add(x);
			  });
			  this.ClearWhere();
			  this.AddWhere(where2);
			  return this.Buscar<CMovimientos>("Movimientos." + NMovimientos.eFields.IdMovimiento.ToString(),Pagina);
        }
		  protected virtual IEnumerable<IInsumosExistencias> _ObtienenExistencias(int idAlmacen, string inCondition)
		  {
			   NInsumosExistencias existencias = new NInsumosExistencias();
			  existencias.AddWhere(new CPropsWhere()
			  {
				  Prioridad = true,
				  Condicion = "in",
				  NombreCampo = NInsumosExistencias.eFields.IdInsumo.ToString(),
				  Valor = inCondition
			  });
			  existencias.AddWhere(new CPropsWhere()
			  {
				  Operador = DBase.AND,
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = NInsumosExistencias.eFields.Estatus.ToString(),
				  Valor = true
			  });
			  return existencias.Buscar().Select(x => x as IInsumosExistencias);
		  }
		  public virtual void ObtienenExistencias(int idAlmacen, string inCondition, List<CMovimientosDetalles> DetalleMovimientos,bool SetCostos)
		  {			  
			  IEnumerable<IInsumosExistencias> existencias2 = _ObtienenExistencias(idAlmacen, inCondition);
			  foreach(CMovimientosDetalles mov in DetalleMovimientos)
			  {
				  IEnumerable<IInsumosExistencias> existencia = existencias2.Where(x => x.IdInsumo == mov.IdInsumo && x.IdAlmacen == idAlmacen);
				  if(existencia != null && existencia.Count() > 0)
				  {
					  mov.CantidadExistenciaInsumo = existencia.FirstOrDefault().Cantidad;
					  if(SetCostos)
					  {
						  mov.Precio = existencia.FirstOrDefault().Costo;
					  }
				  }
			  }
		  }
		  protected string GetInIdsInsumos(IMovimientos movimiento)
		  {
			  List<int> idInsumos = new List<int>();
			  (movimiento).DetalleMovimientos.ToList().ForEach(x => idInsumos.Add(x.IdInsumo));
			  string ids = String.Empty;
			  for(int c = 0; c < idInsumos.Count(); c++)
			  {
				  ids += idInsumos[c].ToString();
				  if(c < idInsumos.Count() - 1)
				  {
					  ids += ",";
				  }
			  }
			  return ids;
		  }
		  public IMovimientos ObtieneDetalles(IMovimientos movimiento,bool cargaExistencias)
		  {
			   CMovimientosDetalles cd = new CMovimientosDetalles();
				cd.IdMovimiento=movimiento.IdMovimiento;
				NMovimientosDetalles detalles = new NMovimientosDetalles(cd);
				detalles.AddWhere(NMovimientosDetalles.eFields.IdMovimiento.ToString(), "=");
				(movimiento).DetalleMovimientos = detalles.Buscar().Select(b => b as CMovimientosDetalles).ToList();
				if(cargaExistencias && (movimiento).DetalleMovimientos != null && (movimiento).DetalleMovimientos.Count()>0)
				{
					List<int> idInsumos = new List<int>();
					movimiento.DetalleMovimientos.ToList().ForEach(x => idInsumos.Add(x.IdInsumo));
					string ids = GetInIdsInsumos(movimiento);
					if(!String.IsNullOrEmpty(ids))
					{
						ObtienenExistencias(movimiento.IdAlmacen, ids, movimiento.DetalleMovimientos,false);
					}
				}
				return movimiento;
		  }
		  protected bool ValidaMovimientos()
		  {
			  if(_data.Delete == 1) return true;
			  if(_Entity != null)
			  {
				  if(((IMovimientos)_Entity).IdAlmacen <= 0)
				  {
					  SetError(TraduceTexto("NMovimientosNoAlmacen"));
					  return false;
				  }
				  if(((IMovimientos)_Entity).DetalleMovimientos != null && ((IMovimientos)_Entity).DetalleMovimientos.Count() > 0)
				  {
					 var result = from item in ((IMovimientos)_Entity).DetalleMovimientos
										group item by item.IdInsumo into g
									  select new CMovimientos {										
										Tipo = g.Count()};
					 var repetidos = result.Where(x => x.Tipo > 1);
					 /*if(repetidos != null && repetidos.Count() > 0)
					 {
						 SetError(TraduceTexto("NMovimientosInsumosRepetidoMovimiento"));
						 return false;
					 }*/

				  }
				  if(((IMovimientos)_Entity).Tipo == (int)NMovimientos.eTipoMovimientos.Entrada)
				  {
					  if(((IMovimientos)_Entity).DetalleMovimientos != null && ((IMovimientos)_Entity).DetalleMovimientos.Count() > 0)
					  {
						  IEnumerable<IMovimientosDetalles> noValidos = ((IMovimientos)_Entity).DetalleMovimientos.Where(x => (x.Precio <= 0 || x.Cantidad <= 0) 
																																						&& x.IdMovimientoDetalle>0);
						  if(noValidos != null && noValidos.Count() > 0)
						  {
							  SetError(TraduceTexto("NMovimientosPrecioCantidadesZero"));
							  return false;
						  }
					  }
				  }
				  if(((IMovimientos)_Entity).Tipo == (int)NMovimientos.eTipoMovimientos.Traspaso)
				  {
					  if(((IMovimientos)_Entity).IdAlmacenDestino == null)						  
					  {
						 SetError(TraduceTexto("NMovimientosNoAlmacen"));
						 return false;
					  }
					  if(((IMovimientos)_Entity).IdAlmacenDestino == ((IMovimientos)_Entity).IdAlmacen)
					  {
						  SetError(TraduceTexto("NMovimientosAlmacenEqualAlmacenDestino"));
						  return false;
					  }
					  if(!ValidaCantidadesSolicitadas(((IMovimientos)_Entity)))
					  {						 
						  return false;
					  }
				  }
				  if(((IMovimientos)_Entity).Tipo == (int)NMovimientos.eTipoMovimientos.Salida || 
					  ((IMovimientos)_Entity).Tipo == (int)NMovimientos.eTipoMovimientos.Traspaso)
				  {
					  if(((IMovimientos)_Entity).DetalleMovimientos != null && ((IMovimientos)_Entity).DetalleMovimientos.Count() > 0)
					  {
						  string ids = GetInIdsInsumos(_Entity as IMovimientos);
						  ObtienenExistencias(((IMovimientos)_Entity).IdAlmacen, ids, ((IMovimientos)_Entity).DetalleMovimientos,false);
						  IEnumerable<IMovimientosDetalles> noValidos = ((IMovimientos)_Entity).DetalleMovimientos.Where(x => (x.Cantidad <= 0 || x.Cantidad <= 0)
																																						 && x.IdMovimientoDetalle >= 0);
						  if(noValidos != null && noValidos.Count() > 0)
						  {
							  SetError(TraduceTexto("NMovimientosPrecioCantidadesZero"));
							  return false;
						  }
						  noValidos = ((IMovimientos)_Entity).DetalleMovimientos.Where(x => (x.Cantidad > x.CantidadExistenciaInsumo)
																											&& x.IdMovimientoDetalle >= 0);
						  if(noValidos != null && noValidos.Count() > 0)
						  {
							  SetError(TraduceTexto("NMovimientosCantidadMayorAExistencias"));
							  return false;
						  }
					  }
				  }
			  }
			  return true;
		  }
        
		  public override void Save()
		  {			  
			  if(ValidaMovimientos())
			  {
				  if(_data == null) return;
				  _data.Save(_Entity);
			  }
		  }
		  public override void Update()
		  {
			  
			  if(ValidaMovimientos())
			  {
				  if(_data == null) return;
				  _data.Save(_Entity);
				  ActualizaSolicitudes(_Entity as IMovimientos);
			  }
		  }
		  public override void Delete()
		  {			  
			  if(ValidaDelete())
			  {
				  if(_data == null) return;
				  _data.Delete = 1;
				  _data.Save(_Entity);
			  }
		  }
		  public bool EsMovimientoValidoEditar(IMovimientos Entity)
		  {
			  int diasTolerancia = 1;
			  double diasDiferencia = (DateTime.Now - Entity.Fecha).TotalDays;
			  return diasTolerancia > diasDiferencia;
		  }
		  public void SetSolicituInsumoAsociada(IMovimientos Entity)
		  {
			  Entity.TieneSolicituInsumoAsociada = this.TieneSolicitudInsumo(Entity);
		  }
		  public void SetDatosUnidadesAlmacenes(IMovimientos Entity)
		  {
			  try
			  {
				  if(Entity == null) return ;

				  Entity.Unidad = String.Empty;
				  Entity.Almacen = String.Empty;
				  Entity.IdUnidadDestino = 0;
				  Entity.UnidadDestino = String.Empty;
				  Entity.AlmacenDestino = String.Empty;
				  IAlmacenes almOrigen = null;
				  IUnidades uniOrigen = null;
				  IAlmacenes almDestino = null;
				  IUnidades uniDestino = null;
				  IEnumerable<IEntidad> Res = null;
				  NAlmacenes buscaAlmacenes = new NAlmacenes();
				  NUnidades buscaUnidades = new NUnidades();
				  if(Entity.IdAlmacen > 0)
				  {
					  buscaAlmacenes.AddWhere(new CPropsWhere()
					  {
						  Prioridad = true,
						  Condicion = "=",
						  NombreCampo = NAlmacenes.eFields.IdAlmacen.ToString(),
						  Valor = Entity.IdAlmacen 
					  });
					  Res = buscaAlmacenes.Buscar();
					  if(Res != null && Res.Count() > 0)
					  {
						  almOrigen = Res.FirstOrDefault() as IAlmacenes;
						  Entity.Almacen = almOrigen.Almacen;
						  buscaUnidades.AddWhere(new CPropsWhere()
						  {
							  Prioridad = true,
							  Condicion = "=",
							  NombreCampo = NUnidades.eFields.IdUnidad.ToString(),
							  Valor = almOrigen.IdUnidad
						  });
						  Res = buscaUnidades.Buscar();
						  if(Res != null && Res.Count() > 0)
						  {
							  uniOrigen = Res.FirstOrDefault() as IUnidades;
							  Entity.Unidad = uniOrigen.Unidad;
						  }
					  }
				  }
				  if(Entity.IdAlmacenDestino != null)
				  {
					  if(Entity.IdAlmacenDestino > 0)
					  {
						  buscaAlmacenes.ClearWhere();
						  buscaAlmacenes.AddWhere(new CPropsWhere()
						  {
							  Prioridad = true,
							  Condicion = "=",
							  NombreCampo = NAlmacenes.eFields.IdAlmacen.ToString(),
							  Valor = Entity.IdAlmacenDestino
						  });
						  Res = buscaAlmacenes.Buscar();
						  if(Res != null && Res.Count() > 0)
						  {
							  almDestino = Res.FirstOrDefault() as IAlmacenes;
							  Entity.AlmacenDestino = almDestino.Almacen;
							  buscaUnidades.ClearWhere();
							  buscaUnidades.AddWhere(new CPropsWhere()
							  {
								  Prioridad = true,
								  Condicion = "=",
								  NombreCampo = NUnidades.eFields.IdUnidad.ToString(),
								  Valor = almDestino.IdUnidad
							  });
							  Res = buscaUnidades.Buscar();
							  if(Res != null && Res.Count() > 0)
							  {
								  uniDestino = Res.FirstOrDefault() as IUnidades;
								  Entity.IdUnidadDestino = uniDestino.IdUnidad;
								  Entity.UnidadDestino = uniDestino.Unidad;
							  }
						  }
					  }
				  }
			  }
			  catch(Exception eBusqueda)
			  {
				  OnError(eBusqueda);
			  }
		  }
		  public IEnumerable<IMovimientos> MovimientosByProyecto(int IdProyecto)
		  {
			  if(IdProyecto <= 0) return null;
			  return MovimientosByProyecto(new CProyectos()
			  {
				  IdProyecto = IdProyecto
			  });
		  }
		  public IEnumerable<IMovimientos> MovimientosByProyecto(IProyectos Proyecto)
		  {
			  DMovimientos m = new DMovimientos();
			  return m.MovimientosByProyecto(Proyecto);
		  }
		  public bool ValidaCantidadesSolicitadas(IMovimientos movimiento)
		  {
			  bool isValid = true;

			  movimiento.DetalleMovimientos.ForEach(md =>
			  {
				  if(md.IdMovimientoDetalle > 0)
				  {
					  IMovimientosContratos movContrato = NMovimientosContratos.GetByIdMovimientoDetalle(md.IdMovimientoDetalle);
					  if(movContrato != null)
					  {
						  int IdPedidoDetalle = 0;
						  int.TryParse(movContrato.IdPedidoDetalle.ToString(), out IdPedidoDetalle);
						  IPedidosDetalles pedidoDetalle = NPedidosDetalles.GetById(IdPedidoDetalle);
						  if(md.Cantidad > pedidoDetalle.Cantidad)
						  {
							  SetError(String.Format("{0} {1} {2}", TraduceTexto("NMovimientoCantidadSolicitadaNoRespetada"), md.Cantidad, pedidoDetalle.Cantidad), true);
							  isValid = false;
						  }
					  }
				  }
			  });
			  return isValid;			 
		  }
		  public bool TieneSolicitudInsumo(IMovimientos movimiento)
		  {
			  if(movimiento == null || movimiento.IdMovimiento <= 0) return false;
			  var res=NMovimientosContratos.GetByIdMovimiento(movimiento.IdMovimiento);
			  if(res != null && res.Count() > 0) return true;
			  return false;
		  }
		  public void ActualizaSolicitudes(IMovimientos movimiento)
		  {
			  if(TieneSolicitudInsumo(movimiento))
			  {
				  NPedidos pedido = new NPedidos();
				  pedido.ActualizaSolicitudInsumos(movimiento);
			  }
		  }
		  public static IMovimientos GetById(int IdMovimiento)
		  {
			  NMovimientosDetalles c = new NMovimientosDetalles();
			  c.AddWhere(new CPropsWhere()
			  {
				  Condicion = "=",
				  Prioridad = true,
				  NombreCampo = NMovimientosDetalles.eFields.IdMovimiento.ToString(),
				  Valor = IdMovimiento
			  });
			  IEnumerable<IEntidad> res = c.Buscar();
			  if(res != null && res.Count() > 0)
			  {
				  return res.FirstOrDefault() as IMovimientos;
			  }
			  return null;
		  }
    }// NMovimientos ends.        
}// LibNegocio.Entidades ends.
