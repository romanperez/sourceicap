using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{
	///<summary>
	///Catalogo Conceptos
	///</summary>
	public partial class NConceptos : NBase
	{
		public bool ValidaConceptoVacio;
		public enum eFields
		{
			IdConcepto = 0, IdEmpresa, IdUnidadMedida, IdConceptoClasificacion, IdCapitulo, Precio, Concepto, Descripcion, Estatus
		}
		public NConceptos()
			: base()
		{
			ValidaConceptoVacio = true;
		}
		public NConceptos(IEntidad entidad)
			: base(entidad)
		{
			ValidaConceptoVacio = true;
		}
		protected override IBase BuildDataEntity()
		{
			DConceptos conceptos = new DConceptos();
			DEmpresas empresas = new DEmpresas();
			DCapitulos capitulo = new DCapitulos();
			DUnidadesMedida um = new DUnidadesMedida();
			DClasificacionConceptos clasificacion = new DClasificacionConceptos();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.Capitulo.ToString()),
				String.Format("{0}.{1} as UnidadMedida",um.Tabla,NUnidadesMedida.eFields.Unidad.ToString()),
				String.Format("{0}.{1} ",clasificacion.Tabla,NClasificacionConceptos.eFields.Clasificacion.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresas.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdEmpresa.ToString()))	,
					/*Join with Capitulos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,capitulo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.IdCapitulo.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdCapitulo.ToString())),
					/*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,um.Tabla,DBase.On, 
				   String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdUnidadMedida.ToString())),
					/*Join with ClasificacionConceptos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,clasificacion.Tabla,DBase.On, 
				   String.Format("{0}.{1}",clasificacion.Tabla,NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdConceptoClasificacion.ToString()))
			  };
			conceptos.SetConfigJoins(camposJoin, condicionesJoin);
			return conceptos;
		}		
		protected virtual bool ValidaConceptos()
		{
			 if(_data.Delete == 1) return true;
			 if(_Entity != null)
			 {
				 if(((IConceptos)_Entity).IdCapitulo <= 0)
				 {
					 SetError(TraduceTexto("IMovimientosCapituloNecesario"));
					 return false;
				 }
				 /*if(((IConceptos)_Entity).Precio <= 0)
				 {
					 SetError(TraduceTexto("NConceptosInsumosZero"));
					 return false;				 
				 }*/
				 if(ValidaConceptoVacio && (((IConceptos)_Entity).Insumos == null || ((IConceptos)_Entity).Insumos.Count <= 0))
				 {
					 SetError(TraduceTexto("NConceptosInsumosVacios"));
					 return false;
				 }
				 if(ValidaConceptoVacio)
				 {
					 IEnumerable<IConceptosInsumos> noValidos = ((IConceptos)_Entity).Insumos.Where(x => (x.Cantidad <= 0 && x.IdConceptoInsumo >= 0));
					 if(noValidos != null && noValidos.Count() > 0)
					 {
						 SetError(TraduceTexto("NConceptosInsumosZero"));
						 return false;
					 }
				 }
			 }
			 return true;
		}		
		public IEnumerable<ICotizacionesDetalles> FindConceptos(int idEmpresa,bool AddNoRegistrados,IEnumerable<ICotizacionesDetalles> conceptos)
		{			
			if(conceptos == null || conceptos.Count() <= 0) return null;
			List<IPropsWhere> where = new List<IPropsWhere>();
			string[] array = conceptos.Select(x => String.Format("'{0}'", x.Concepto)).ToArray();
			string all = String.Join(",",array);
			where.Add(new CPropsWhere()
			{
				OmiteConcatenarTabla = false,
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NConceptos.eFields.Concepto.ToString(),
				Valor = all
			});
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<IEntidad> res = this.Buscar();
			if(res != null &&res.Count()>0)
			{
				IConceptos first = res.FirstOrDefault() as IConceptos;
				int idCapitulo = 0;
				IMonedas moneda = NConversiones.GetMonedaPesos(first.IdEmpresa); /*Por default todos los conceptos originalmente son pesos*/
				res.ToList().ForEach(x =>
				{
					ICotizacionesDetalles aux = conceptos.Where(y => y.Concepto == ((IConceptos)(x)).Concepto).FirstOrDefault();
					if(aux != null)
					{						
						int.TryParse(((IConceptos)(x)).IdCapitulo.ToString(), out idCapitulo);
						aux.Capitulo = ((IConceptos)(x)).Capitulo;
						aux.IdCapitulo = idCapitulo;
						aux.IdConcepto = ((IConceptos)(x)).IdConcepto;
						aux.Descripcion= ((IConceptos)(x)).Descripcion;
						aux.Precio= ((IConceptos)(x)).Precio;						
						aux.UnidadMedida = ((IConceptos)(x)).UnidadMedida;
						aux.IdMoneda = moneda.IdMoneda;
						aux.Moneda = moneda.Moneda;
						aux.CodigoMoneda = moneda.Codigo;
					}
				});
			//	return conceptos.Where(x=>x.IdConcepto>0);				
			}
			//else return null;
			if(conceptos != null)
			{
				if(AddNoRegistrados)
				{
					IEnumerable<ICotizacionesDetalles> noSaved = conceptos.Where(x => x.IdConcepto <= 0);
					NConceptos news =new  NConceptos();
					news.ValidaConceptoVacio = false;
					IConceptos newConcepto = null;
					noSaved.ToList().ForEach(cc =>
					{
						IUnidadesMedida um = NUnidadesMedida.GetByName(cc.UnidadMedida);
						newConcepto = new CConceptos();
						newConcepto.Concepto = cc.Concepto;
						newConcepto.Descripcion = cc.Descripcion;
						newConcepto.IdEmpresa = idEmpresa;
						newConcepto.Estatus = true;
						newConcepto.Fecha = DateTime.Now;
						if(um != null)
						{
							newConcepto.IdUnidadMedida = um.IdUnidad;
						}
						news.SetEntity(newConcepto);
						news.Save();
						if(news.HasError())
						{
							SetError(String.Format("[{0}] {1}",newConcepto.Concepto, news.GetErrorMessage()), true);
						}
					});
				}
				return conceptos;
			}
			else
			{
				return null;
			}
		}
		public IConceptos ObtienePrecioConcepto(IConceptos concepto)
		{
			StringBuilder sb = new StringBuilder("");
			sb.Append("<Precios>");
			sb.Append(concepto.ToXml());
			sb.Append("</Precios>");
			DContratos2 data = new DContratos2();
			DConceptos dcon = new DConceptos();
			IContratos2 c2 = new CContratos2();
			c2.IdEmpresa = concepto.IdEmpresa;
			c2.Fecha = concepto.Fecha;
			c2.Utilidad = 0;
			IEnumerable<IContratosDetalles2> newPrecios = data.ObtienePrecios(c2, sb.ToString());
			if(newPrecios != null && newPrecios.Count() > 0)
			{
				IEnumerable<IContratosDetalles2> newPrecio = newPrecios.Where(x => x.IdConcepto == concepto.IdConcepto);
				if(newPrecio != null && newPrecio.Count() > 0)
				{
					concepto.Precio = newPrecio.FirstOrDefault().Precio;
					if(concepto.Precio <= 0)
					{
						newPrecio.FirstOrDefault().Insumos.ForEach(i =>
						{
							concepto.Concepto += i.MotivoModificacion + " "; 
						});
					}
				}
			}
			dcon.ActualizaPrecio(concepto);
			return concepto;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IConceptos", new DConceptos());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));				
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;			
			if(ValidaConceptos())
			{
				if(_data == null) return;
				_data.Save(_Entity);
				ObtienePrecioConcepto(_Entity as IConceptos);
				if(((IConceptos)_Entity).Precio <=0)
				{
					SetError(String.Format("{0} {1}", CIdioma.TraduceCadena("NConceptosNoHayTipoCambio"), ((IConceptos)_Entity).Concepto));
				}
			}
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			if(ValidaConceptos())
			{
				if(_data == null) return;
				_data.Save(_Entity);
				ObtienePrecioConcepto(_Entity as IConceptos);
				if(((IConceptos)_Entity).Precio <= 0)
				{
					SetError(String.Format("{0} {1}", CIdioma.TraduceCadena("NConceptosNoHayTipoCambio"), ((IConceptos)_Entity).Concepto));
				}
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		/// <summary>
		/// Convierte el precio de los conceptos[Precio en pesos por default] al tipo de cambio indicado.
		/// </summary>
		/// <param name="conceptos">Lista de conceptos.</param>
		/// <returns>Conceptos con precios en dolares.</returns>
		public IEnumerable<IConceptos> ConceptosToDollar(ITipoCambios tipo, IEnumerable<IConceptos> conceptos)
		{
			List<IConceptos> newConceptos = new List<IConceptos>();
			if(tipo.IdMonedaOrigen<=0)
			{
				throw new Exception("ICotizacionSeleccioneMoneda");
			}
			foreach(IConceptos concepto in conceptos)
			{
				concepto.Moneda = tipo.MonedaOrigen;
				concepto.Precio = concepto.Precio / tipo.Valor;
				newConceptos.Add(concepto);
			}
			return newConceptos;
		}
		public IEnumerable<IConceptos> ConceptosToPesos(ITipoCambios tipo, IEnumerable<IConceptos> conceptos)
		{
			List<IConceptos> newConceptos = new List<IConceptos>();
			if(tipo.IdMonedaOrigen <= 0)
			{
				throw new Exception("ICotizacionSeleccioneMoneda");
			}
			foreach(IConceptos concepto in conceptos)
			{
				concepto.Moneda = tipo.MonedaOrigen;
				concepto.Precio = concepto.Precio * tipo.Valor; //////////////////
				newConceptos.Add(concepto);
			}
			return newConceptos;
		}
		/// <summary>
		/// Convierte el precio de los conceptos[Precio en pesos por default] al tipo de cambio indicado.
		/// </summary>
		/// <param name="conceptos">Lista de conceptos.</param>
		/// <returns>Conceptos con precios en dolares.</returns>
		public ICotizacionesDetalles ConceptosToDollar(ITipoCambios tipo, ICotizacionesDetalles concepto)
		{
			if(tipo.IdMonedaOrigen <= 0)
			{
				throw new Exception("ICotizacionSeleccioneMoneda");
			}		
			concepto.Moneda = tipo.MonedaOrigen;
			concepto.Precio = concepto.Precio / tipo.Valor;			
			return concepto;
		}
		protected void SetPesos(IEnumerable<IEntidad> conceptos)
		{
			IConceptos concepto = conceptos.First() as IConceptos;
			IMonedas pesos = NConversiones.GetMonedaPesos(concepto.IdEmpresa);
			conceptos.ToList().ForEach(x =>
			{
				((IConceptos)x).IdMoneda = pesos.IdMoneda;
				((IConceptos)x).Moneda = pesos.Moneda;
				((IConceptos)x).CodigoMoneda = pesos.Codigo;
			});
		}
		public override IPagina<Entity> Buscar<Entity>(string campoKey, int pagina, int registrosPP)
		{			
			IPagina<Entity> res= base.Buscar<Entity>(campoKey, pagina, registrosPP);
			if(res.Contenido!=null)
			{
				SetPesos(res.Contenido.Select(x=> x as IEntidad));
				List<IPropsWhere> where = GetIds(res.Contenido.Select(x => x as IConceptos));
				GetInsumos(where, res.Contenido.Select(x => x as IConceptos));
				res.Contenido.ToList().ForEach(x =>
				{
					ValidaConcepto(x as IConceptos);
				});
			}
			return res;
		}
		protected void ValidaConcepto(IConceptos concepto)
		{
			StringBuilder sb = new StringBuilder(String.Empty);
			concepto.EsValido = true;
			if( concepto.IdUnidadMedida==null || concepto.IdUnidadMedida <= 0 )
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IConceptoUnidadMedidaObligatoria"));
				sb.Append("<br>");
			}
			if(concepto.IdConceptoClasificacion ==null || concepto.IdConceptoClasificacion <= 0)
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IConceptoClasificacionObligatoria"));
				sb.Append("<br>");
			}
			if(String.IsNullOrEmpty(concepto.Descripcion))
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IConceptoDescripcionObligatoria"));
				sb.Append("<br>");
			}
			if(concepto.Estatus==false)
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IConceptoEstatusInactivo"));
				sb.Append("<br>");
			}
			if(concepto.IdCapitulo == null || concepto.IdCapitulo<=0)
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IMovimientosCapituloNecesario"));
				sb.Append("<br>");
			}
			if(concepto.Precio <= 0)
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IConceptoPrecioObligatorio"));
				sb.Append("<br>");
			}
			if(concepto.Insumos ==null || concepto.Insumos.Count()<=0)
			{
				concepto.EsValido = false;
				sb.Append(TraduceTexto("IConceptoInsumosObligatorios"));
				sb.Append("<br>");
			}
			if(!concepto.EsValido)
			{
				concepto.MotivoNoValido = sb.ToString();
			}
		}
		public override IEnumerable<IEntidad> Buscar(int top)
		{
			IEnumerable<IEntidad> res= base.Buscar(top);
			if(res != null)
			{
				SetPesos(res);	
			}
			return res;
		}
		public List<IPropsWhere> GetIds(IEnumerable<IConceptos> conceptos)
		{
			int[] idOts = (from tbl in conceptos
								group tbl by tbl.IdConcepto into g
								select g.Key).ToArray();
			string ids = String.Join(",", idOts);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NConceptosInsumos.eFields.IdConcepto.ToString(),
				Valor = ids
			});
			return where;
		}
		public void GetInsumos(IEnumerable<IPropsWhere> where, IEnumerable<IConceptos> conceptos)
		{
			NConceptosInsumos n = new NConceptosInsumos();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IConceptosInsumos> allInsumos = null;
			if(res1 != null)
			{
				allInsumos = res1.Select(x => x as IConceptosInsumos);
				conceptos.ToList().ForEach(x =>
				{
					IEnumerable<IConceptosInsumos> insumoByConcepto = allInsumos.Where(y => y.IdConcepto == x.IdConcepto);
					if(insumoByConcepto!= null && insumoByConcepto.Count()>0)
					{
						x.Insumos = insumoByConcepto.Select(x2=>x2 as CConceptosInsumos).ToList();
					}
				});
			}
		}
		public override IEnumerable<IEntidad> Buscar()
		{
			IEnumerable<IEntidad> res = base.Buscar();			
			if(res != null && res.Count() > 0)
			{
				IMonedas pesos= NConversiones.GetMonedaPesos((res.FirstOrDefault() as CConceptos).IdEmpresa);
				List<IPropsWhere> where = GetIds(res.Select(x=>x as IConceptos));
				GetInsumos(where, res.Select(x => x as IConceptos));
				res.ToList().ForEach(x =>
				{
					((IConceptos)(x)).IdMoneda=pesos.IdMoneda;
					((IConceptos)(x)).Moneda =pesos.Moneda;
					((IConceptos)(x)).CodigoMoneda = pesos.Codigo;
				});
				return res;
			}
			return null;
		}
		public static IEnumerable<IConceptos> GetConceptosFromTable(DataTable ConceptosTbl)
		{

			if(ConceptosTbl == null) return null;
			IEnumerable<CConceptos> Conceptos = null;
			List<IConceptos> ConceptosAux = new List<IConceptos>();
			List<IConceptosInsumos> InsumosAux = new List<IConceptosInsumos>();
			for(int rw = 0; rw < ConceptosTbl.Rows.Count; rw++)
			{
				IConceptos cAux = new CConceptos();
				IConceptosInsumos iAux = new CConceptosInsumos();
				cAux.Concepto = ConceptosTbl.Rows[rw].Field<string>("CONCEPTO");
				cAux.Descripcion = ConceptosTbl.Rows[rw].Field<string>("DESCRIPCION");
				cAux.Capitulo = ConceptosTbl.Rows[rw].Field<string>("CAPITULO");
				cAux.UnidadMedida = ConceptosTbl.Rows[rw].Field<string>("UNIDAD_MEDIDA");
				cAux.Clasificacion = ConceptosTbl.Rows[rw].Field<string>("CLASIFICACION");				
				iAux.Codigo = ConceptosTbl.Rows[rw].Field<string>("INSUMO_CODIGO");
				iAux.ConceptoPadre = cAux.Concepto;
				iAux.CostoInsumo = NBase.ToDouble(ConceptosTbl.Rows[rw].Field<string>("INSUMO_COSTO"));
				iAux.Cantidad = NBase.ToDouble(ConceptosTbl.Rows[rw].Field<string>("INSUMO_CANTIDAD"));
				ConceptosAux.Add(cAux);
				InsumosAux.Add(iAux);
			}
			if(ConceptosAux != null && ConceptosAux.Count > 0)
			{
				var Conceptos2 = from tbl in ConceptosAux
								group tbl by new
								{
									tbl.Concepto,
									tbl.Descripcion,
									tbl.Capitulo,
									tbl.UnidadMedida,
									tbl.Clasificacion
								} into gp
								select new CConceptos()
								{
									Concepto = gp.Key.Concepto,
									Descripcion = gp.Key.Descripcion,
									Capitulo = gp.Key.Capitulo,
									UnidadMedida = gp.Key.UnidadMedida,
									Clasificacion = gp.Key.Clasificacion
								};
				Conceptos = Conceptos2.ToList();
				if(Conceptos != null && Conceptos.Count() > 0)
				{
					foreach(IConceptos c in  Conceptos)
					{
						IEnumerable<IConceptosInsumos> res= InsumosAux.Where(i => i.ConceptoPadre == c.Concepto);
						c.Insumos = (res != null && res.Count()>0)? res.Select(i=>i as CConceptosInsumos).ToList():null;
					};
				}
			}
			return Conceptos;
		}		
		public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
		{
			entities.ToList().ForEach(
			x =>
			{
				try
				{
					_Entity = x;
					this.ValidaConceptoVacio = true;
					this.Save();
					if(HasError())
					{
						((IConceptos)x).Empresa = this.GetErrorMessage();
						this.ClearError();
					}
				}
				catch(Exception eSave)
				{
					((IConceptos)x).Empresa = eSave.Message;
				}
			});
			return entities;
		}
		public virtual IEnumerable<IConceptos>SaveMasivo2(IEnumerable<IConceptos> Conceptos,int IdEmpresa)
		{
			try
			{
				if(Conceptos == null || Conceptos.Count() <= 0) return null;
				
				DCapitulos ca = new DCapitulos();				
				DUnidadesMedida du = new DUnidadesMedida();
				DClasificacionConceptos cla = new DClasificacionConceptos();
				DInsumos ins =new DInsumos();				
				DEmpresas de = new DEmpresas();
				string InCondicion = String.Empty;
				string[] Capitulos = Conceptos.Where(i => !(String.IsNullOrEmpty(i.Capitulo))).Select(i => String.Format("'{0}'", i.Capitulo)).ToArray();
				string[] Unidades = Conceptos.Where(i => !(String.IsNullOrEmpty(i.UnidadMedida))).Select(i => String.Format("'{0}'", i.UnidadMedida)).ToArray();
				string[] Clasificaciones = Conceptos.Where(i => !(String.IsNullOrEmpty(i.Clasificacion))).Select(i => String.Format("'{0}'", i.Clasificacion)).ToArray();
				string[] Insumos = null;
				List<string> lstInusmos = new List<string>(); 
				Conceptos.ToList().ForEach(c=>
				{
					c.Insumos.ForEach(i =>
					{
						lstInusmos.Add(String.Format("'{0}'", i.Codigo));
					});
				});
				Insumos =(lstInusmos!=null && lstInusmos.Count()>0)? lstInusmos.ToArray():null;
				if(Capitulos != null) Capitulos = Capitulos.GroupBy(a => a).Select(g => g.Key).ToArray();
				if(Unidades != null) Unidades = Unidades.GroupBy(a => a).Select(g => g.Key).ToArray();
				if(Clasificaciones != null) Clasificaciones = Clasificaciones.GroupBy(a => a).Select(g => g.Key).ToArray();
				if(Insumos != null) Insumos = Insumos.GroupBy(a => a).Select(g => g.Key).ToArray();

				IEnumerable<IEntidad> Res = null;
				IEnumerable<ICapitulos> lstCapitulos = new List<ICapitulos>();
				IEnumerable<IUnidadesMedida> lstUnidades = new List<IUnidadesMedida>();
				IEnumerable<IClasificacionConceptos> lstClasificaciones = new List<IClasificacionConceptos>();
				IEnumerable<IInsumos> lstInsumos = new List<IInsumos>();

				NCapitulos nC = new NCapitulos();
				NUnidadesMedida nUm = new NUnidadesMedida();
				NClasificacionConceptos nCla = new NClasificacionConceptos();
				NInsumos nIns = new NInsumos();

				List<IPropsWhere> where = new List<IPropsWhere>();
				CPropsWhere whereEmpresa = new CPropsWhere();
				whereEmpresa.Condicion = "=";
				whereEmpresa.OmiteConcatenarTabla = true;
				whereEmpresa.Prioridad = true;
				whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
				whereEmpresa.Valor = IdEmpresa;

				CPropsWhere whereCampo = new CPropsWhere();
				whereCampo.Condicion = "in";
				whereCampo.OmiteConcatenarTabla = true;
				whereCampo.Prioridad = true;

				//Buscando Capitulos
				InCondicion = String.Join(",", Capitulos);
				whereCampo.NombreCampo = String.Format("{0}.{1}", ca.Tabla, NCapitulos.eFields.Capitulo.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nC.AddWhere(where);
				Res = nC.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstCapitulos = Res.Select(x => x as ICapitulos);
				}
				//Buscando Unidades de medida
				InCondicion = String.Join(",", Unidades);
				whereCampo.NombreCampo = String.Format("{0}.{1}", du.Tabla, NUnidadesMedida.eFields.Unidad.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nUm.AddWhere(where);
				Res = nUm.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstUnidades = Res.Select(x => x as IUnidadesMedida);
				}
				//Buscando Clasificaciones
				InCondicion = String.Join(",", Clasificaciones);
				whereCampo.NombreCampo = String.Format("{0}.{1}", cla.Tabla, NClasificacionConceptos.eFields.Clasificacion.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nCla.AddWhere(where);
				Res = nCla.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstClasificaciones = Res.Select(x => x as IClasificacionConceptos);
				}
				//Buscando Insumos
				InCondicion = String.Join(",", Insumos);
				whereCampo.NombreCampo = String.Format("{0}.{1}", ins.Tabla, NInsumos.eFields.Codigo.ToString());
				whereCampo.Valor = InCondicion;
				where = new List<IPropsWhere>();
				where.Add(whereEmpresa);
				where.Add(whereCampo);
				nIns.AddWhere(where);
				Res = nIns.Buscar();
				if(Res != null && Res.Count() > 0)
				{
					lstInsumos = Res.Select(x => x as IInsumos);
				}
				//cargamos costos de insumos				
				if(lstInsumos != null && lstInsumos.Count()>0)
				{
					nIns.GetCostoInsumos(lstInsumos);
				}								
				Conceptos.ToList().ForEach(c =>
				{
					var resCap = (!String.IsNullOrEmpty(c.Capitulo)) ? lstCapitulos.Where(cap => cap.Capitulo.ToLower().Trim() == c.Capitulo.ToLower().Trim()) : null;
					var resUM = (!String.IsNullOrEmpty(c.UnidadMedida)) ? lstUnidades.Where(u => u.Unidad.ToLower().Trim() == c.UnidadMedida.ToLower().Trim()) : null;
					var resCla = (!String.IsNullOrEmpty(c.Clasificacion)) ? lstClasificaciones.Where(m => m.Clasificacion.ToLower().Trim() == c.Clasificacion.ToLower().Trim()) : null;
					if(resCap != null && resCap.Count() > 0)
					{
						c.IdCapitulo = resCap.FirstOrDefault().IdCapitulo;
					}
					if(resUM != null && resUM.Count() > 0)
					{
						c.IdUnidadMedida= resUM.FirstOrDefault().IdUnidad;
					}
					if(resCla != null && resCla.Count() > 0)
					{
						c.IdConceptoClasificacion = resCla.FirstOrDefault().IdConceptoClasificacion;
					}
					if(c.Insumos != null && c.Insumos.Count() > 0)
					{
						c.Insumos.ForEach(ii =>
						{
							var iAux = lstInsumos.Where(fi => fi.Codigo.ToLower() == ii.Codigo.ToLower());
							if(iAux != null && iAux.Count() > 0)
							{
								ii.IdInsumo = iAux.FirstOrDefault().IdInsumo;
								ii.IdMoneda = iAux.FirstOrDefault().IdMoneda;
								double costoDb=0;
								double.TryParse(iAux.FirstOrDefault().Costo.ToString(),out costoDb);
								//dejamos el costo del archivo como principal solo en caso de que sea zero lo reemplazamos por el de base de datos
								if(ii.CostoInsumo <= 0)
								{
									ii.CostoInsumo = costoDb;
								}
							}
						});
					}
				});
				Conceptos.ToList().ForEach(c =>
				{
					c.IdEmpresa = IdEmpresa;
					c.Fecha = DateTime.Now;
					c.Estatus = true;
					if(c.Concepto == null) c.Concepto = String.Empty;					
					IEnumerable<CConceptosInsumos> noZero =c.Insumos.Where(z => z.IdInsumo > 0);
					c.Insumos = (noZero != null && noZero.Count() > 0) ? noZero.ToList() : null;
				});
				this.SaveMasivo(Conceptos);
				Conceptos= Conceptos.Where(c => c.IdConcepto <= 0 || !String.IsNullOrEmpty(c.Empresa));
			}
			catch(Exception e)
			{
				OnError(e);
			}

			return Conceptos;
		}
	}// NConceptos ends.        
}// LibNegocio.Entidades ends.
