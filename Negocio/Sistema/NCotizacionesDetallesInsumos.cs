using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 21/02/2017 09:48:12 a. m.*/
namespace Negocio
{

	public partial class NCotizacionesDetallesInsumos : NBase
	{
		public enum eFields
		{
			IdInsumoCotizacion = 0, IdCotizacion, IdConcepto, IdInsumo, Cantidad, Costo
		}
		public NCotizacionesDetallesInsumos()
		{
		}
		public NCotizacionesDetallesInsumos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DCotizacionesDetallesInsumos cotiinsumo= new DCotizacionesDetallesInsumos();
			DCotizaciones cotizacion = new DCotizaciones();
			DInsumos insumo = new DInsumos();
			DMonedas moneda = new DMonedas();
			DConceptos conceptos = new DConceptos();
			DUnidadesMedida um = new DUnidadesMedida();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdCapitulo.ToString()),
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Codigo.ToString()),
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Descripcion.ToString()),
				String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdMoneda.ToString()),
				String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.Unidad.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Cotizaciones*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cotizacion.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdCotizacion.ToString()),
					String.Format("{0}.{1}",cotiinsumo.Tabla,NCotizacionesDetallesInsumos.eFields.IdCotizacion.ToString())),	 
				  /*Join with Conceptos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,conceptos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdConcepto.ToString()),
					String.Format("{0}.{1}",cotiinsumo.Tabla,NCotizacionesDetallesInsumos.eFields.IdConcepto.ToString())),	 
				  /*Join with Insumos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,insumo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdInsumo.ToString()),
					String.Format("{0}.{1}",cotiinsumo.Tabla,NCotizacionesDetallesInsumos.eFields.IdInsumo.ToString())),					
					/*Join with Monedas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdMoneda.ToString())),
					/*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,um.Tabla,DBase.On, 
				   String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdUnidad.ToString()))
			  };
			cotiinsumo.SetConfigJoins(camposJoin, condicionesJoin);
			return cotiinsumo;
		}
	}// NCotizacionesDetallesInsumos ends.        
}// LibNegocio.Entidades ends.
