using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 08/12/2016 05:26:43 p. m.*/
namespace Negocio
{  
    
    public partial class NPerfiles : NBase
    {
	public enum eFields {IdPerfil=0,Nombre,Descripcion,Estatus}
    public NPerfiles() { }
	public NPerfiles(IEntidad entidad)
			: base(entidad)
	{
	}
	protected override IBase BuildDataEntity()
	{
	  return new  DPerfiles();
	}				
    }// NPerfiles ends.        
}// LibNegocio.Entidades ends.
