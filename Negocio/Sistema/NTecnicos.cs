using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{
	public partial class NTecnicos : NBase
	{
		public enum eFields
		{
			IdTecnico = 0, IdEmpleado, IdEspecialidad, IdGrado
		}
		public NTecnicos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DTecnicos tecnicos = new DTecnicos();
			DEmpleados empleado = new DEmpleados();
			DEspecialidades especialidad = new DEspecialidades();
			DTecnicosGrados grado = new DTecnicosGrados();
			DDepartamentos departamento = new DDepartamentos();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();			
			string[] camposJoin = new string[]
			{								
					String.Format("{0} as NombreEmpleado",new NEmpleados().DataNombreEmpleado()),
					String.Format("{0}.{1} as NombreEspecialidad",especialidad.Tabla,NEspecialidades.eFields.Nombre.ToString()),
					String.Format("{0}.{1} as NombreGrado",grado.Tabla,NTecnicosGrados.eFields.Nombre.ToString()),
					String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empleados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empleado.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdEmpleado.ToString()),
					String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdEmpleado.ToString())),					
					/*Join with Especialidades*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,especialidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",especialidad.Tabla,NEspecialidades.eFields.IdEspecialidad.ToString()),
					String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdEspecialidad.ToString())),	
					/*Join with TecnicosGrados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,grado.Tabla,DBase.On, 
					String.Format("{0}.{1}",grado.Tabla,NTecnicosGrados.eFields.IdGrado.ToString()),
					String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdGrado.ToString())),

					/*Join with Departamentos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,departamento.Tabla,DBase.On, 
				   String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdDepartamento.ToString()),
					String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdDepartamento.ToString())),
					/*Join with Unidades*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",departamento.Tabla,NDepartamentos.eFields.IdUnidad.ToString())),
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			  };
			tecnicos.SetConfigJoins(camposJoin, condicionesJoin);
			return tecnicos;
		}
		//protected virtual IBase BuildDataEntityIndex()
		//{			
		//	NEmpleados empleado = new NEmpleados();
		//	DEmpleados dempleado = empleado.GetBuildDataEntity() as DEmpleados;
		//	DTecnicos tecnicos = new DTecnicos();
		//	string[] camposJoin =dempleado.CamposJoin();
		//	string[] condicionesJoin = dempleado.CondicionesJoin();
		//	List<string> newCondicionesJoin= condicionesJoin.ToList();
		//	List<string> newCamposJoin = camposJoin.ToList();
		//	newCondicionesJoin.Add( /*Join with Tecnicos*/
		//			String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin, tecnicos.Tabla, DBase.On,
		//			String.Format("{0}.{1}", tecnicos.Tabla, NTecnicos.eFields.IdEmpleado.ToString()),
		//			String.Format("{0}.{1}", dempleado.Tabla, NEmpleados.eFields.IdEmpleado.ToString())));
		//	newCamposJoin.Add("count(*) as Matricula");
		//	dempleado.SetConfigJoins(newCamposJoin.ToArray(), newCondicionesJoin.ToArray());
		//	return dempleado;
		//}
		public virtual IPagina<Entity> BuscarIndex<Entity>(int pagina) where Entity:IEntidad, new()
		{			
			DEmpleados all = new DEmpleados();
			if(_data != null)
			{
				all.AddWhere(_data.Where());
			}
			return all.GetAllTecnicos<Entity>(pagina);			
		}
		/*public string CampoIdEmpleado()
		{			
			DEmpleados empleado = new DEmpleados();
			return String.Format("{0}.{1}", empleado.Tabla, NEmpleados.eFields.IdEmpleado);
		}*/
		public NTecnicos()
		{
			
		}
		public virtual bool ValidaTecnico(bool isNuevo)
		{
			if(_data.Delete == 1) return true;
			if(_Entity != null)
			{
				ITecnicos tecnico = _Entity as ITecnicos;
				if(tecnico.Especialidades == null || tecnico.Especialidades.Count() <= 0)
				{
					SetError(TraduceTexto("ITecnicosSinEspecialidades"));
					return false;
				}
				else
				{
					if(isNuevo)
					{
						ITecnicos tec2 = this.GetTecnicoByIdEmpleado(tecnico.IdEmpleado);
						if(tec2 != null && tec2.Especialidades != null && tec2.Especialidades.Count()>0)
						{
							tecnico.Especialidades = tecnico.Especialidades.Concat(tec2.Especialidades).ToList();
						}						
					}					
					var result = from item in tecnico.Especialidades
									 where item.IdTecnico>=0
									 group item by item.IdEspecialidad into g									 
									 select new CEspecialidades
									 {
										 IdTecnico = g.Count()
									 };
					var repetidos = result.Where(x => x.IdTecnico > 1);
					if(repetidos != null && repetidos.Count() > 0)
					{
						SetError(TraduceTexto("ITecnicosEspecialidadRepetida"));
						return false;
					}					
				}
			}
			return true;
		}
		public override void Save()
		{
			if(ValidaTecnico(true))
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Update()
		{

			if(ValidaTecnico(false))
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public ITecnicos GetTecnicoByIdEmpleado(int IdEmpleado)		
		{			
			IPropsWhere where = new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NTecnicos.eFields.IdEmpleado.ToString(),
				Valor = IdEmpleado
			};
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<ITecnicos> Result = this.Buscar().Select(x => x as ITecnicos);
			ITecnicos tecnicoGroup = new CTecnicos();
			tecnicoGroup.Especialidades = new List<CEspecialidades>();
			foreach(ITecnicos t in Result)
			{
				tecnicoGroup.Especialidades.Add(new CEspecialidades()
				{
					IdEspecialidad = t.IdEspecialidad,
					Nombre = t.NombreEspecialidad,
					IdTecnico = t.IdTecnico,
					IdGrado=t.IdGrado,
					Grado=t.NombreGrado
				});
			}
			tecnicoGroup.IdEmpleado = IdEmpleado;
			if(Result != null && Result.Count()>0)
			{				
				tecnicoGroup.IdEmpresa = Result.FirstOrDefault().IdEmpresa;
				tecnicoGroup.Empresa = Result.FirstOrDefault().Empresa;
				tecnicoGroup.NombreEmpleado = Result.FirstOrDefault().NombreEmpleado;
			}			
			return tecnicoGroup;
		}
		public override IEnumerable<IEntidad> Buscar(int top)
		{
			if(_data==null)return null;
			DTecnicos tecnicos = new DTecnicos();
			tecnicos.AddWhere(_data.Where());
			return tecnicos.CustomTop(top);
		}


        public static IEnumerable<ITecnicos> GetTecnicosFromTable(System.Data.DataTable TecnicosTbl)
        {
            if (TecnicosTbl == null) return null;
            List<ITecnicos> Tecnicos = new List<ITecnicos>();
            for (int rw = 0; rw < TecnicosTbl.Rows.Count; rw++)
            {
                Tecnicos.Add(new CTecnicos()
                {
                    NombreEmpleado = TecnicosTbl.Rows[rw].Field<string>("MATRICULA"),
                    NombreGrado = TecnicosTbl.Rows[rw].Field<string>("GRADO"),
                    NombreEspecialidad = TecnicosTbl.Rows[rw].Field<string>("ESPECIALIDAD")
                });
            }
            return Tecnicos;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((ITecnicos)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<ITecnicos> SaveMasivo2(IEnumerable<ITecnicos> Tecnicos, int IdEmpresa)
        {
            try
            {
                if (Tecnicos == null || Tecnicos.Count() <= 0) return null;

                DEmpleados dE = new DEmpleados();
                DEspecialidades dd = new DEspecialidades();
                DEmpresas de = new DEmpresas();
                string[] Empleados = Tecnicos.Where(i => !(String.IsNullOrEmpty(i.NombreEmpleado))).Select(i => String.Format("'{0}'", i.NombreEmpleado)).ToArray();
                string[] Especialidades = Tecnicos.Where(i => !(String.IsNullOrEmpty(i.NombreEspecialidad))).Select(i => String.Format("'{0}'", i.NombreEspecialidad)).ToArray();
                string InCondicion = String.Empty;
                IEnumerable<IEmpleados> lstEmpleados = new List<IEmpleados>();
                IEnumerable<IEspecialidades> lstEspecialidades = new List<IEspecialidades>();
                IEnumerable<IEntidad> Res = null;
                NEmpleados nE = new NEmpleados();
                NEspecialidades nD = new NEspecialidades();

                List<IPropsWhere> where = new List<IPropsWhere>();
                CPropsWhere whereEmpresa = new CPropsWhere();
                whereEmpresa.Condicion = "=";
                whereEmpresa.OmiteConcatenarTabla = true;
                whereEmpresa.Prioridad = true;
                whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
                whereEmpresa.Valor = IdEmpresa;

                CPropsWhere whereCampo = new CPropsWhere();
                whereCampo.Condicion = "in";
                whereCampo.OmiteConcatenarTabla = true;
                whereCampo.Prioridad = true;

                //Buscando Empleados
                InCondicion = String.Join(",", Empleados);
                whereCampo.NombreCampo = String.Format("{0}.{1}", dE.Tabla, NEmpleados.eFields.Matricula.ToString());
                whereCampo.Valor = InCondicion;
                where = new List<IPropsWhere>();
                where.Add(whereEmpresa);
                where.Add(whereCampo);
                nE.AddWhere(where);
                Res = nE.Buscar();
                if (Res != null && Res.Count() > 0)
                {
                    lstEmpleados = Res.Select(x => x as IEmpleados);
                }

                //Buscando Especialidades
                InCondicion = String.Join(",", Especialidades);
                whereCampo.NombreCampo = String.Format("{0}.{1}", dd.Tabla, NEspecialidades.eFields.Nombre.ToString());
                whereCampo.Valor = InCondicion;
                where = new List<IPropsWhere>();
                where.Add(whereEmpresa);
                where.Add(whereCampo);
                nD.AddWhere(where);
                Res = nD.Buscar();
                if (Res != null && Res.Count() > 0)
                {
                    lstEspecialidades = Res.Select(x => x as IEspecialidades);
                }

                Tecnicos.ToList().ForEach(i =>
                {
                    var resF = (!String.IsNullOrEmpty(i.NombreEmpleado)) ? lstEmpleados.Where(f => f.Matricula.ToLower() == i.NombreEmpleado.ToLower() && f.IdEmpresa == IdEmpresa) : null;
                    var resD = (!String.IsNullOrEmpty(i.NombreEspecialidad)) ? lstEspecialidades.Where(f => f.Nombre.ToLower() == i.NombreEspecialidad.ToLower() && f.IdEmpresa == IdEmpresa && f.Grado == i.NombreGrado) : null;

                    if (resF != null && resF.Count() > 0)
                    {
                        i.IdEmpleado = resF.FirstOrDefault().IdEmpleado;
                    }

                    if (resD != null && resD.Count() > 0)
                    {
                        i.IdEspecialidad = resD.FirstOrDefault().IdEspecialidad;
                        i.IdGrado = resD.FirstOrDefault().IdGrado;

                        if (i.Especialidades == null)
                            i.Especialidades = new List<CEspecialidades>();

                        i.Especialidades.Add(new CEspecialidades { IdEspecialidad = i.IdEspecialidad, IdGrado = i.IdGrado });
                    }

                    
                });

                Tecnicos.ToList().ForEach(i =>
                {
                    i.IdEmpresa = IdEmpresa;
                });
                this.SaveMasivo(Tecnicos);
                Tecnicos = Tecnicos.Where(i => i.IdTecnico <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return Tecnicos;
        }
	}// NTecnicos ends.        
}// LibNegocio.Entidades ends.