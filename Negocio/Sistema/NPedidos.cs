using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{
	public partial class NPedidos : NBase
	{
		public enum eFields
		{
			IdPedido = 0,IdProyecto,IdContrato, IdUnidad, FechaSolicitado, FechaLlegada, Estatus
		}
		public NPedidos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DPedidos pedido = new DPedidos();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			DProyectos proyecto = new DProyectos();
			DContratos2 contrato = new DContratos2();

			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),				
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} ",proyecto.Tabla,NProyectos.eFields.Proyecto.ToString()),
				String.Format("{0}.{1} ",contrato.Tabla,NContratos.eFields.Contrato.ToString())
				//String.Format("{0}.{1} ",contrato.Tabla,NContratos.eFields.Contrato.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				/*Join Unidades*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
				String.Format("{0}.{1}",pedido.Tabla,NPedidos.eFields.IdUnidad.ToString())),
				/*Join Empresas*/
            String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString())),

				/*Join Proyectos*/
            String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,proyecto.Tabla,DBase.On, 
				String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdProyecto.ToString()),
				String.Format("{0}.{1}",pedido.Tabla,NPedidos.eFields.IdProyecto.ToString()))	,									
				/*Join Contratos*/
            String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,contrato.Tabla,DBase.On, 
				String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdContrato.ToString()),
				String.Format("{0}.{1}",pedido.Tabla,NPedidos.eFields.IdContrato.ToString()))										
			};
			pedido.SetConfigJoins(camposJoin, condicionesJoin);
			return pedido;
		}
		public NPedidos()
		{
		}
		public virtual bool ValidaPedido()
		{
			if(_data.Delete == 1) return true;
			if(_Entity != null)
			{
				IPedidos pedido = _Entity as IPedidos;
				if(pedido.Detalle == null || pedido.Detalle.Count() <= 0)
				{
					SetError(TraduceTexto("NPedidosDetalleVacio"));
					return false;
				}
				else
				{
					var result = from item in pedido.Detalle
									 group item by item.IdInsumo into g
									 select new CInsumos
									 {
										 Costo = g.Count()
									 };
					var repetidos = result.Where(x => x.Costo > 1);
					if(repetidos != null && repetidos.Count() > 0)
					{
						SetError(TraduceTexto("NPedidoInsumoDuplicado"));
						return false;
					}
					IEnumerable<IPedidosDetalles> zero = pedido.Detalle.Where(x => x.Cantidad <= 0);
					if(zero != null && zero.Count() > 0)
					{
						SetError(TraduceTexto("NPedidoCantidadesZero"));
						return false;
					}
					if(pedido.FechaLlegada != null)
					{
						if(pedido.FechaSolicitado.Date > pedido.FechaLlegada.Value.Date)
						{
							SetError(TraduceTexto("NPedidoFechaLlgadaIncorrecta"));
							return false;
						}
					}
				}
			}
			return true;
		}
		public override void Save()
		{
			if(ValidaPedido())
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Update()
		{

			if(ValidaPedido())
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public static string GetCampoId()
		{
			DPedidos pedido = new DPedidos();
			return String.Format("{0}.{1}", pedido.Tabla, NPedidos.eFields.IdPedido.ToString());
		}
		public IEnumerable<IPedidos> PedidosByProyecto(int idproyecto)
		{
			if(idproyecto <= 0) return null;
			return PedidosByProyecto(new CProyectos()
			{
				IdProyecto = idproyecto
			});
		}
		public IEnumerable<IPedidos> PedidosByProyecto(IProyectos proyecto)
		{
			DPedidos p = new DPedidos();			
			return p.PedidosByProyecto(proyecto);			
		}
		public IEnumerable<IPedidosDetalles> PedidosByProyectoMail(IProyectos proyecto)
		{
			DPedidos p = new DPedidos();
			return p.PedidosByProyectoMail(proyecto);
		}

		public IEnumerable<IPedidosDetalles> ExistenciasVSPedidoByProyecto(int IdProyecto, int IdAlmacen)
		{
			DPedidos p = new DPedidos();
			return p.ExistenciasVSPedidoByProyecto(IdProyecto,IdAlmacen);
		}

		public void RecibeInsumosByProyecto(CModelTraspasos Pedido,int idUsuario)
		{
			bool hayErrores = false;
			int[] Aids = null;
			string ids="";
			IPedidos iPedido=null;
			List<CMovimientosDetalles> existenciasOriginales= new List<CMovimientosDetalles>();
			if(Pedido.Pedidos!=null && Pedido.Pedidos.First().Detalle != null  )
			{ 
				Aids= (from tbl in Pedido.Pedidos.First().Detalle
								select tbl.IdInsumo).ToArray();
				ids = String.Join(",", Aids);
				iPedido = Pedido.Pedidos.First();
			}
			NMovimientos nt = new NMovimientos();
			CMovimientos traspaso = new CMovimientos();
			traspaso.Tipo =(int)NMovimientos.eTipoMovimientos.Traspaso;
			traspaso.Fecha=DateTime.Now;
			traspaso.IdAlmacen = Pedido.Movimiento.IdAlmacen;
			traspaso.IdAlmacenDestino=Pedido.Movimiento.IdAlmacenDestino;
			traspaso.Estatus=true;
			traspaso.IdUsuario=idUsuario;
			traspaso.DetalleMovimientos = new List<CMovimientosDetalles>();
			foreach(IPedidosDetalles detalle in iPedido.Detalle)
			{
				CMovimientosDetalles movDet = new CMovimientosDetalles();
				if(detalle.CantidadTraspasar > 0)
				{
					movDet.IdMovimientoDetalle =  detalle.IdPedidoDetalle;
					movDet.IdInsumo = detalle.IdInsumo;
					movDet.Cantidad = detalle.CantidadTraspasar;
					movDet.IdPedidoDetalle = detalle.IdPedidoDetalle;
					traspaso.DetalleMovimientos.Add(movDet);
				}
			}
			nt.ObtienenExistencias(traspaso.IdAlmacen, ids, traspaso.DetalleMovimientos, true);
			traspaso.DetalleMovimientos.ForEach(x => existenciasOriginales.Add(new CMovimientosDetalles()
			{
				IdInsumo=x.IdInsumo,
				CantidadExistenciaInsumo = x.CantidadExistenciaInsumo
			}));

		  //Detectamos los insumos a los cuales se les esta enviando mas cantidad de la solicitada.
		  var insumosNoSolicitados = iPedido.Detalle.Where(x=>x.CantidadTraspasar>x.Cantidad);
		  if(insumosNoSolicitados != null && insumosNoSolicitados.Count()>0)
		  {
			  insumosNoSolicitados.ToList().ForEach(x =>
			  {
				  if(x.IdPedidoDetalle > 0)
				  {
					  x.IdPedidoDetalle = x.IdPedidoDetalle * -1;
				  }
			  });
			  hayErrores = true;
		  }
			//Se realiza el calculo de los insumos
			foreach(IMovimientosDetalles detalle in traspaso.DetalleMovimientos)
			{
				detalle.CantidadExistenciaInsumo = detalle.CantidadExistenciaInsumo - detalle.Cantidad;
				var actualiza = traspaso.DetalleMovimientos.Where(x => x.IdInsumo == detalle.IdInsumo &&
																					  x.IdMovimientoDetalle != detalle.IdMovimientoDetalle &&
																					  x.IdMovimientoDetalle != 0);
				if(actualiza != null && actualiza.Count()>0)
				{
					actualiza.ToList().ForEach(x =>
					{
						x.CantidadExistenciaInsumo = detalle.CantidadExistenciaInsumo;
					});
				}
				detalle.IdMovimientoDetalle = 0;
			}
			//Se determina que insumos no cuentan con stock y se envia la lista de aquellos con  un id negativo para especificar que
			//contiene un error al especificar un id negativo
			var InsumoNegativos = traspaso.DetalleMovimientos.Where(x => x.CantidadExistenciaInsumo < 0);
			if(InsumoNegativos != null && InsumoNegativos.Count() > 0)
			{
				InsumoNegativos.ToList().ForEach(x =>
				{
					var errores = Pedido.Pedidos.First().Detalle.Where(i => i.IdInsumo == x.IdInsumo);
					if(errores != null && errores.Count() > 0)
					{
						errores.ToList().ForEach(e =>
						{
							e.IdPedidoDetalle = (e.IdPedidoDetalle > 0) ? e.IdPedidoDetalle * -1 : e.IdPedidoDetalle;
							e.CantidadTraspasar = existenciasOriginales.Where(ex => ex.IdInsumo == e.IdInsumo).First().CantidadExistenciaInsumo;
						});
					}
				});
				hayErrores = true;
			}
			//Aquellos insumos que cuenten con stock suficiente solo enviamos la cantidad existente para informar al usuario cuanta cantidad hay en almacen.
			var InsumoNoNegativos = traspaso.DetalleMovimientos.Where(x => x.CantidadExistenciaInsumo >= 0);
			if(InsumoNoNegativos != null && InsumoNegativos.Count() > 0)
			{
				InsumoNoNegativos.ToList().ForEach(x =>
				{
					var NoErrores = Pedido.Pedidos.First().Detalle.Where(i => i.IdInsumo == x.IdInsumo);
					if(NoErrores != null && NoErrores.Count() > 0)
					{
						NoErrores.ToList().ForEach(e =>
						{
							e.CantidadTraspasar = existenciasOriginales.Where(ex => ex.IdInsumo == e.IdInsumo).First().CantidadExistenciaInsumo;
						});
					}
				});
			}
		
			if(hayErrores) return;

			NMovimientos NTraspaso = new NMovimientos(traspaso);
			NTraspaso.Save();
			if(NTraspaso.HasError())
			{
				SetError(NTraspaso.GetErrorMessage(), true);
				return;
			}			
			//Actualizamos cantidades Solicitudes
			traspaso.DetalleMovimientos.ForEach(x =>
			{
				IPedidosDetalles detalle = NPedidosDetalles.GetById(x.IdPedidoDetalle);
				IPedidosDetalles detalleContrato = iPedido.Detalle.Where(dp => dp.IdPedidoDetalle == x.IdPedidoDetalle).FirstOrDefault();
				NPedidosDetalles pd = new NPedidosDetalles(detalle);
				if(detalle != null && detalle.IdPedidoDetalle > 0)
				{
					detalle.CantidadEnviada = detalle.CantidadEnviada2 + x.Cantidad;
					detalle.IdConcepto = detalleContrato.IdConcepto;
					pd.Save();
					if(pd.HasError())
					{
						NTraspaso.SetError(pd.GetErrorMessage(), true);
					}
					if(detalleContrato != null && detalleContrato.IdContrato > 0)
					{
						int IdProyectoAux = 0;
						int IdConceptoAux = 0;
						int.TryParse(iPedido.IdProyecto.ToString(), out IdProyectoAux);
						int.TryParse(detalleContrato.IdConcepto.ToString(), out IdConceptoAux);
						NMovimientosContratos mc = new NMovimientosContratos(new CMovimientosContratos()
						{
							IdMovimientoContrato = 0,														
							IdProyecto =IdProyectoAux,// iPedido.IdProyecto,
							IdContrato = detalleContrato.IdContrato,
							IdConcepto = IdConceptoAux,//detalleContrato.IdConcepto,
							IdInsumo = detalle.IdInsumo,
							IdMovimiento = traspaso.IdMovimiento,
							IdPedidoDetalle = (detalle.IdPedidoDetalle>0)? detalle.IdPedidoDetalle :(int?)null,
							IdMovimientoDetalle=x.IdMovimientoDetalle
						});
						mc.Save();
						if(mc.HasError())
						{
							NTraspaso.SetError(mc.GetErrorMessage(), true);
						}
					}
				}
				//iPedido.IdProyecto;
				//detalle.IdContrato				
			});
			int idalmacenSalida = 0;
			int.TryParse(Pedido.Movimiento.IdAlmacenDestino.ToString(), out idalmacenSalida);
			if(idalmacenSalida > 0)
			{
				CMovimientos salida = new CMovimientos();
				salida.Tipo = (int)NMovimientos.eTipoMovimientos.Salida;
				salida.Fecha = DateTime.Now;
				salida.IdAlmacen = idalmacenSalida;
				salida.IdAlmacenDestino = null;
				salida.Estatus = true;
				salida.IdUsuario = idUsuario;
				salida.DetalleMovimientos = new List<CMovimientosDetalles>();
				traspaso.DetalleMovimientos.ForEach(x =>
				{
					x.IdMovimiento = 0;
					x.IdMovimientoDetalle = 0;
					salida.DetalleMovimientos.Add(x);
				});
				NTraspaso.SetEntity(salida);
				NTraspaso.Save();
				if(NTraspaso.HasError())
				{
					SetError(NTraspaso.GetErrorMessage(), true);
					return; 
					//aki me kede debe realizar traspaso,actualizacion de Pedidos,asociar el traspaso con el proyecto/CContratos2 
						//realizar La salida,asociar La salida con el proyecto/contrato
				}
				salida.DetalleMovimientos.ForEach(x =>
				{					
					IPedidosDetalles detalleContrato = iPedido.Detalle.Where(dp => dp.IdPedidoDetalle == x.IdPedidoDetalle).FirstOrDefault();
					int IdProyectoAux2 = 0;
					int IdConceptoAux2 = 0;
					int.TryParse(iPedido.IdProyecto.ToString(), out IdProyectoAux2);
					int.TryParse(detalleContrato.IdConcepto.ToString(), out IdConceptoAux2);
					NMovimientosContratos mc = new NMovimientosContratos(new CMovimientosContratos()
						{
							IdMovimientoContrato = 0,														
							IdProyecto =IdProyectoAux2, //iPedido.IdProyecto,
							IdContrato = detalleContrato.IdContrato,
							IdConcepto = IdConceptoAux2,//detalleContrato.IdConcepto,
							IdInsumo = x.IdInsumo,
							IdMovimiento = salida.IdMovimiento,
							IdMovimientoTraspaso = traspaso.IdMovimiento,
							IdMovimientoDetalle = x.IdMovimientoDetalle,
							IdPedidoDetalle =detalleContrato.IdPedidoDetalle
						});
					mc.Save();
					if(mc.HasError())
					{
						NTraspaso.SetError(mc.GetErrorMessage(), true);
					}
				});
			}//if ends.
		}
		
		public void ActualizaSolicitudInsumos(IMovimientos movimiento)
			{
				NPedidosDetalles nPedido=new NPedidosDetalles();				
				movimiento.DetalleMovimientos.ForEach(x=>
				{
					//Actualizando la solicitud de insumos
					IMovimientosContratos mov=NMovimientosContratos.GetByIdMovimientoDetalle(x.IdMovimientoDetalle);
					if(mov!=null)
					{
						int id =0;
						int.TryParse(mov.IdPedidoDetalle.ToString(),out id);
						IPedidosDetalles pedidoDetalle = NPedidosDetalles.GetById(id);
						pedidoDetalle.CantidadEnviada=x.Cantidad;						
						nPedido.SetEntity(pedidoDetalle);
						nPedido.Save();
					}					
				});
				IEnumerable<IMovimientosContratos> salida = NMovimientosContratos.GetByIdMovimientoTraspaso(movimiento.IdMovimiento);
				NMovimientosDetalles NmovDetail=new NMovimientosDetalles();
				if(salida!= null && salida.Count()>0)
				{
					salida.ToList().ForEach(x=>
					{
						int idPedidoDetalle =0;
						int idMovDetalle=0;
						int.TryParse(x.IdPedidoDetalle.ToString(),out idPedidoDetalle);
						int.TryParse(x.IdMovimientoDetalle.ToString(),out idMovDetalle);
						IPedidosDetalles pedido = NPedidosDetalles.GetById(idPedidoDetalle);						
						IMovimientosDetalles movimientoDetalle=NMovimientosDetalles.GetById(idMovDetalle);
						movimientoDetalle.Cantidad = pedido.CantidadEnviada2;
						NmovDetail.SetEntity(movimientoDetalle);
						NmovDetail.Save();						
					});
				}
			}				
	}// NPedidos ends.        
}// LibNegocio.Entidades ends.



//falta actualizar el pedido y la salida cuando sea modifica el traspaso