using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NInsumosExistencias : NBase
	{
		public enum eFields
		{
			IdExistencia = 0, IdAlmacen, IdInsumo, Cantidad, Estatus
		}
		public NInsumosExistencias(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DInsumosExistencias existencias = new DInsumosExistencias();
			DInsumos insumo = new DInsumos();
			DAlmacenes almacen = new DAlmacenes();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			DMonedas moneda = new DMonedas();
			string[] camposJoin = new string[]
			{				
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Codigo.ToString()),
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Serie.ToString()),
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Descripcion.ToString()),
				String.Format("{0}.{1}",almacen.Tabla,NAlmacenes.eFields.Almacen.ToString()),
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} ",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString()),
			};			
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Insumos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,insumo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdInsumo.ToString()),
					String.Format("{0}.{1}",existencias.Tabla,NInsumosExistencias.eFields.IdInsumo.ToString())),
						/*Join with Monedas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdMoneda.ToString())),
					/*Join with Alamacenes*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,almacen.Tabla,DBase.On, 
				   String.Format("{0}.{1}",almacen.Tabla,NAlmacenes.eFields.IdAlmacen.ToString()),
					String.Format("{0}.{1}",existencias.Tabla,NInsumosExistencias.eFields.IdAlmacen.ToString())),
					/*Join with Unidades*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",almacen.Tabla,NAlmacenes.eFields.IdUnidad.ToString())),
					/*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			  };
			existencias.SetConfigJoins(camposJoin, condicionesJoin);
			return existencias;
		}
		public NInsumosExistencias()
		{
		}
		//public IEnumerable<IInsumosExistencias> GetInsumosServicion(int idEmpresa)
		//{

		//}
	}// NInsumosExistencias ends.        
}// LibNegocio.Entidades ends.
