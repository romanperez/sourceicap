using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{

	public partial class NPedidosDetalles : NBase
	{
		public enum eFields
		{
			IdPedidoDetalle = 0, IdPedido, IdInsumo, Cantidad, CantidadEnviada
		}
		public NPedidosDetalles(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DPedidosDetalles pedidoDetalle = new DPedidosDetalles();
			DUnidadesMedida unidad = new DUnidadesMedida();
			DInsumos insumos = new DInsumos();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",insumos.Tabla,NInsumos.eFields.Codigo.ToString()),
				String.Format("{0}.{1}",insumos.Tabla,NInsumos.eFields.Serie.ToString()),
				String.Format("{0}.{1}",insumos.Tabla,NInsumos.eFields.Descripcion.ToString()),
				String.Format("{0}.{1} UnidadMedidaDescripcion" ,unidad.Tabla,NUnidadesMedida.eFields.Descripcion.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Insumos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,insumos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",insumos.Tabla,NInsumos.eFields.IdInsumo.ToString()),
					String.Format("{0}.{1}",pedidoDetalle.Tabla,NPedidosDetalles.eFields.IdInsumo.ToString())),
					  /*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",insumos.Tabla,NInsumos.eFields.IdUnidad.ToString()))
			  };
			pedidoDetalle.SetConfigJoins(camposJoin, condicionesJoin);
			return pedidoDetalle;
		}
		public NPedidosDetalles()
		{
		}
		public List<CPedidosDetalles> GetDetalleByPedido(IPedidos pedido)
		{
			IPedidosDetalles pd = new CPedidosDetalles();
			pd.IdPedido = pedido.IdPedido;
			NPedidosDetalles findPedido = new NPedidosDetalles(pd);
			findPedido.AddWhere(NPedidosDetalles.eFields.IdPedido.ToString(), "=");
			IEnumerable<IEntidad> res = findPedido.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as CPedidosDetalles).ToList();
			}
			return null;
		}
		public static IPedidosDetalles GetById(int IdPedidoDetalle)
		{
			NPedidosDetalles c = new NPedidosDetalles();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NPedidosDetalles.eFields.IdPedidoDetalle.ToString(),
				Valor = IdPedidoDetalle
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IPedidosDetalles;
			}
			return null;
		}
		public static IEnumerable<IPedidosDetalles> GetByIds(string Ids)
		{
			NPedidosDetalles c = new NPedidosDetalles();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "in",
				Prioridad = true,
				NombreCampo = NPedidosDetalles.eFields.IdPedidoDetalle.ToString(),
				Valor = Ids
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x=>x  as IPedidosDetalles);
			}
			return null;
		}
		public IPedidosDetalles ToDetallePedido(IInsumos insumo)
		{
			IPedidosDetalles pedido = new CPedidosDetalles();
			pedido.IdInsumo = insumo.IdInsumo;
			pedido.Codigo = insumo.Codigo;
			pedido.Descripcion = insumo.Descripcion;
			return pedido;
		}
		public void ConfirmaRecepcion(IEnumerable<IPedidosDetalles> pedidos)
		{
			try
			{
				pedidos.ToList().ForEach(p =>
				{
					this.ClearWhere();
					this.AddWhere(new CPropsWhere()
					{
						NombreCampo = NPedidosDetalles.eFields.IdPedidoDetalle.ToString(),
						Valor = p.IdPedidoDetalle,
						Prioridad = true,
						Condicion = "="
					});
					IEnumerable<IEntidad> res = this.Buscar();
					if(res != null)
					{
						IPedidosDetalles newCantidadRecibida = res.FirstOrDefault() as IPedidosDetalles;
						newCantidadRecibida.CantidadRecibida = p.CantidadRecibida;
						this.SetEntity(newCantidadRecibida);
						this.Save();
					}
				});
			}
			catch(Exception e)
			{
				OnError(e);
			}
		}
	}// NPedidosDetalles ends.        
}// LibNegocio.Entidades ends.
