using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NMonedas : NBase
	{
		public enum eFields
		{
			IdMoneda = 0, IdEmpresa, Moneda,Codigo, Estatus, Predeterminada
		}
		public NMonedas(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DMonedas moneda = new DMonedas();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdEmpresa.ToString()))
			  };
			moneda.SetConfigJoins(camposJoin, condicionesJoin);
			return moneda;
		}
		public NMonedas()
		{
		}		
		public IMonedas GetById(int IdMoneda)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NMonedas.eFields.IdMoneda.ToString(),
				Valor = IdMoneda
			});
			this.ClearWhere();
			this.AddWhere(where);
			IEnumerable<IEntidad> res = this.Buscar();
			if(res.Any())
			{
				return res.First() as IMonedas;
			}
			else
			{
				return null;
			}
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IMonedas", new DMonedas());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;	
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
	}// NMonedas ends.        
}// LibNegocio.Entidades ends.
