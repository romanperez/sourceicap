using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{
	///<summary>
	///Catalogo almacenes
	///</summary>
	public partial class NAlmacenes : NBase
	{
		public enum eFields
		{
			IdAlmacen = 0, IdUnidad, Codigo, Almacen, Estatus
		}
		public NAlmacenes(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DAlmacenes almacen = new DAlmacenes();			
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",almacen.Tabla,NDepartamentos.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),

            String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()))
			};
			almacen.SetConfigJoins(camposJoin, condicionesJoin);
			return almacen;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IAlmacenes", new DAlmacenes());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public NAlmacenes()
		{
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;	
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;	
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
	}// NAlmacenes ends.        
}// LibNegocio.Entidades ends.
