using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{

	public partial class NUnidadesMedida : NBase
	{
		public enum eFields
		{
			IdUnidad = 0, IdEmpresa, Unidad, Descripcion, Estatus
		}
		public NUnidadesMedida(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DUnidadesMedida unidad = new DUnidadesMedida();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidadesMedida.eFields.IdEmpresa.ToString()))
			  };
			unidad.SetConfigJoins(camposJoin, condicionesJoin);
			return unidad;
		}
		public NUnidadesMedida()
		{
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IUnidadesMedida", new DUnidadesMedida());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));			
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;			
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;			
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
		public static IUnidadesMedida GetByName(string nombre)
		{
			CUnidadesMedida um = new CUnidadesMedida();
			um.Unidad = nombre;
			NUnidadesMedida np = new NUnidadesMedida(um);
			np.AddWhere(NUnidadesMedida.eFields.Unidad.ToString(), "=");
			IEnumerable<IEntidad> res = np.Buscar();
			if(res.Any())
			{
				return res.FirstOrDefault() as IUnidadesMedida;
			}
			else
			{
				return null;
			}
			
		}
	}// NUnidadesMedida ends. 
      
}// LibNegocio.Entidades ends.
