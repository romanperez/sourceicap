using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using Idiomas;
using System.Resources;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{
    public partial class NMenus : NBase
    {
        public const string PREFIJO_RESOURCE = "Menu";
        public enum eFields { IdMenu = 0, IdMenuPadre, Menu, Titulo, ToolTip, Url, Orden, Estatus }
        public NMenus(IEntidad entidad) : base(entidad) { }
        protected override IBase BuildDataEntity()
        {
            return new DMenus();
        }
        public NMenus()
            : base(new CMenus())
        {
        }
        public IEnumerable<IMenus> GetAllMenus()
        {
            return GetAllMenus(null, false);
        }
        public IEnumerable<IMenus> GetAllMenus(CPerfiles perfil)
        {
            return GetAllMenus(perfil, true);
        }
        public IEnumerable<IMenus> GetAllMenus(CPerfiles perfil, bool validaPerfil)
        {
            IEnumerable<IMenus> result = null;
            try
            {
                if (perfil == null && validaPerfil)
                {
                    OnError(new Exception(TraduceTexto("NMenusNoUserSession")));
                    return result;
                }
                result = this._GetMenuByPerfil(perfil);
                if (result != null && result.Count() > 0)
                {
                    IEnumerable<IMenus> Root = result.Where(m => m.IdMenuPadre == 0);
                    if (Root != null && Root.Count() > 0)
                    {
                        OrdenaMenus(Root.ToArray(), result.ToArray());
                        result = Root;
                    }
                }
            }
            catch (Exception er)
            {
                OnError(er);
            }
            return result;
        }
        protected void OrdenaMenus(IMenus[] root, IMenus[] AllMenus)
        {
            if (root == null || root.Count() <= 0) return;
            if (AllMenus == null || AllMenus.Count() <= 0) return;
            foreach (IMenus mnu in root)
            {
                mnu.SetSubMenus(AllMenus.Where(m => m.IdMenuPadre == mnu.IdMenu));
                OrdenaMenus(mnu.GetSubMenus().ToArray(), AllMenus);
            }
        }
        public void GetMenuIdioma(IEnumerable<IMenus> Menus)
        {
            if (Menus == null || Menus.Count() <= 0) return;
            foreach (IMenus mnu in Menus)
            {
                string buscar = String.Format("{0}{1}", PREFIJO_RESOURCE, mnu.Menu);
                string texto = TraduceTexto(buscar);
                if (texto != buscar)
                {
                    mnu.Titulo = texto;
                }
                GetMenuIdioma(mnu.SubMenus);
            }
        }
        protected IEnumerable<IMenus> _GetMenuByPerfil(IPerfiles perfil)
        {
            if (this._data != null)
            {
                return ((DMenus)(_data)).GetMenuByPerfil(perfil);
            }
            else
            {
                return null;
            }
        }
		  protected virtual bool ValidaDuplicado(bool isNew)
		  {
			  bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IMenus", new DMenus());
			  if(isDuplicado)
			  {
				  SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			  }
			  return isDuplicado;
		  }
		  public override void Save()
		  {
			  if(ValidaDuplicado(true)) return;
			  base.Save();
		  }
		  public override void Update()
		  {
			  if(ValidaDuplicado(false)) return;
			  base.Update();
		  }
		  public override void Delete()
		  {
			  if(ValidaDelete())
			  {
				  if(_data == null) return;
				  _data.Delete = 1;
				  _data.Save(_Entity);
			  }
		  }
        //public override bool ValidaDelete()
        //{
        //    SetError("No puedesn borrorlo dije..!!");
        //    return false;
        //}
    }// NMenus ends.        
}// LibNegocio.Entidades ends.
