using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 23/06/2017 01:59:21 p. m.*/
namespace Negocio
{

	public partial class NMovimientosContratos : NBase
	{
		public enum eFields
		{
			IdMovimientoContrato=0,	IdProyecto,	IdContrato,	IdConcepto,	IdInsumo,	IdMovimiento,	IdMovimientoDetalle,	IdPedidoDetalle,	IdMovimientoTraspaso
		}
		public NMovimientosContratos()
		{
		}
		public NMovimientosContratos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DMovimientosContratos();
		}
		public static IMovimientosContratos GetById(int IdMovimientoContrato)
		{
			NMovimientosContratos c = new NMovimientosContratos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad=true,
				NombreCampo = NMovimientosContratos.eFields.IdMovimientoContrato.ToString(),
				Valor = IdMovimientoContrato
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IMovimientosContratos;
			}
			return null;
		}
		public static IEnumerable<IMovimientosContratos> GetByIdMovimientoTraspaso(int IdMovimiento)
		{
			NMovimientosContratos c = new NMovimientosContratos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NMovimientosContratos.eFields.IdMovimientoTraspaso.ToString(),
				Valor = IdMovimiento
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IMovimientosContratos);
			}
			return null;
		}
		public static IEnumerable<IMovimientosContratos> GetByIdMovimiento(int IdMovimiento)
		{
			NMovimientosContratos c = new NMovimientosContratos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NMovimientosContratos.eFields.IdMovimiento.ToString(),
				Valor = IdMovimiento
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IMovimientosContratos);
			}
			return null;			
		}
		public static IMovimientosContratos GetByIdMovimientoDetalle(int IdMovimientoDetalle)
		{
			NMovimientosContratos c = new NMovimientosContratos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NMovimientosContratos.eFields.IdMovimientoDetalle.ToString(),
				Valor = IdMovimientoDetalle
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IMovimientosContratos).FirstOrDefault();
			}
			return null;
		}
		public static IEnumerable<IMovimientosContratos> GetByMovimientosDetalle(string ids)
		{
			NMovimientosContratos c = new NMovimientosContratos();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "in",
				Prioridad = true,
				NombreCampo = NMovimientosContratos.eFields.IdMovimientoDetalle.ToString(),
				Valor = ids
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IMovimientosContratos);
			}
			return null;
		}

	}// NMovimientosContratos ends.        
}// LibNegocio.Entidades ends.
