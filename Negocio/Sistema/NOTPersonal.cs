using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{  
    
    public partial class NOTPersonal : NBase
    {
	public enum eFields {IdOTPersonal=0,IdOrdenTrabajo,IdTecnico,Estatus}
        public NOTPersonal(IEntidad entidad)
			: base(entidad)
	{
	}
	protected override IBase BuildDataEntity()
	{		
		DOTPersonal personal= new DOTPersonal();
		DTecnicos tecnicos = new DTecnicos();
		DEmpleados empleado = new DEmpleados();
		DEspecialidades especialidad = new DEspecialidades();
		DTecnicosGrados grado = new DTecnicosGrados();
		
		string[] camposJoin = new string[]
			{								
				String.Format("{0}.{1} as NombreGrado",grado.Tabla,NTecnicosGrados.eFields.Nombre.ToString()),	
				String.Format("{0}.{1} as NombreEspecialidad",especialidad.Tabla,NEspecialidades.eFields.Nombre.ToString()),
				String.Format("{0} as NombreEmpleado",new NEmpleados().DataNombreEmpleado())
			};
		string[] condicionesJoin = new string[] 
			  { 
				  /*Join with	Tecnicos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,tecnicos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdTecnico.ToString()),
					String.Format("{0}.{1}",personal.Tabla,NOTPersonal.eFields.IdTecnico.ToString())), 
				  /*Join with Empleados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empleado.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empleado.Tabla,NEmpleados.eFields.IdEmpleado.ToString()),
					String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdEmpleado.ToString())),					
					/*Join with Especialidades*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,especialidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",especialidad.Tabla,NEspecialidades.eFields.IdEspecialidad.ToString()),
					String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdEspecialidad.ToString())),	
					/*Join with TecnicosGrados*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,grado.Tabla,DBase.On, 
					String.Format("{0}.{1}",grado.Tabla,NTecnicosGrados.eFields.IdGrado.ToString()),
					String.Format("{0}.{1}",tecnicos.Tabla,NTecnicos.eFields.IdGrado.ToString()))
			  };
		personal.SetConfigJoins(camposJoin, condicionesJoin);
		return personal;
		
	}		
	public NOTPersonal() { }			
    }// NOTPersonal ends.        
}// LibNegocio.Entidades ends.
