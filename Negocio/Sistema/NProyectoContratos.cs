using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/03/2017 03:46:56 p. m.*/
namespace Negocio
{

	public partial class NProyectoContratos : NBase
	{
		public enum eFields
		{
			IdProyectoContrato = 0, IdProyecto, IdContrato, Estatus
		}
		public NProyectoContratos()
		{
		}
		public NProyectoContratos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DProyectoContratos proyecto = new DProyectoContratos();
			DContratos2 contrato = new DContratos2();
			DClientes cliente = new DClientes();
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
		 		  {												
		 			String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.Contrato.ToString()),
		 			String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
		 			String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
		 			String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.RazonSocial.ToString()),
					String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdCliente.ToString())
		 		  };
			string[] condicionesJoin = new string[] 
		 		  { 			
		 				/*Join with Contratos*/
		 				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,contrato.Tabla,DBase.On, 
		 				String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdContrato.ToString()),
		 				String.Format("{0}.{1}",proyecto.Tabla,NProyectoContratos.eFields.IdContrato.ToString())),
		 			  /*Join with Clientes*/						
		 				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cliente.Tabla,DBase.On, 
		 				String.Format("{0}.{1}",cliente.Tabla,NClientes.eFields.IdCliente.ToString()),
		 				String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdCliente.ToString())),
		 				/*Join with Unidades*/
		 				String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,unidad.Tabla,DBase.On, 
		 				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
		 				String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdUnidad.ToString()))	,
		 				/*Join with Empresas*/
		 				String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,empresa.Tabla,DBase.On, 
		 				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
		 				String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdEmpresa.ToString()))
		 		  };
			proyecto.SetConfigJoins(camposJoin, condicionesJoin);
			return proyecto;
		}
		public static IEnumerable<IProyectoContratos> GetByContrato(int IdContrato)
		{
			NProyectoContratos b = new NProyectoContratos();
			b.AddWhere(new CPropsWhere()
			{
				Prioridad=true,
				NombreCampo = NProyectoContratos.eFields.IdContrato.ToString(),
				Condicion="=",
				Valor=IdContrato
			});
			IEnumerable<IEntidad> res = b.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as IProyectoContratos);
			}
			return null;
		}
	}// NProyectoContratos ends.        
}// LibNegocio.Entidades ends.
