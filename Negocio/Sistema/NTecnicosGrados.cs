using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:48 p. m.*/
namespace Negocio
{

	public partial class NTecnicosGrados : NBase
	{
		public enum eFields
		{
			IdGrado = 0, IdEmpresa, Codigo, Nombre, Estatus
		}
		public NTecnicosGrados(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DTecnicosGrados tecnicoGrado = new DTecnicosGrados();
			DEmpresas empresas = new DEmpresas();

			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.Empresa.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Empresas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresas.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresas.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",tecnicoGrado.Tabla,NTecnicosGrados.eFields.IdEmpresa.ToString())),					
			  };
			tecnicoGrado.SetConfigJoins(camposJoin, condicionesJoin);
			return tecnicoGrado;
		}
		public IEnumerable<IPropsWhere> FiltroSoloGradosActivos(IEnumerable<IPropsWhere> whereOriginal)
		{
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "=",
				NombreCampo = NEspecialidades.eFields.Estatus.ToString(),
				Valor = true
			});
			return base.CombineWithFiltro(where, whereOriginal);
		}
		public NTecnicosGrados()
		{
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "ITecnicosGrados", new DTecnicosGrados());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}

        public static IEnumerable<ITecnicosGrados> GetUnidadesFromTable(System.Data.DataTable GradosTbl)
        {
            if (GradosTbl == null) return null;
            List<ITecnicosGrados> Unidades = new List<ITecnicosGrados>();
            for (int rw = 0; rw < GradosTbl.Rows.Count; rw++)
            {
                Unidades.Add(new CTecnicosGrados()
                {
                    Codigo = GradosTbl.Rows[rw].Field<string>("CODIGO"),
                    Nombre = GradosTbl.Rows[rw].Field<string>("NOMBRE")
                });
            }
            return Unidades;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((ITecnicosGrados)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<ITecnicosGrados> SaveMasivo2(IEnumerable<ITecnicosGrados> Grados, int IdEmpresa)
        {
            try
            {
                if (Grados == null || Grados.Count() <= 0) return null;


                Grados.ToList().ForEach(i =>
                {
                    i.IdEmpresa = IdEmpresa;
                    i.Estatus = true;
                });
                this.SaveMasivo(Grados);
                Grados = Grados.Where(i => i.IdGrado <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return Grados;
        }
	}// NTecnicosGrados ends.        
}// LibNegocio.Entidades ends.
