using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NMovimientosDetalles : NBase
	{
		public enum eFields
		{
			IdMovimientoDetalle = 0, IdMovimiento, IdInsumo, Cantidad, Precio, Observacion
		}
		public NMovimientosDetalles(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DMovimientosDetalles detail = new DMovimientosDetalles();
			DInsumos insumo = new DInsumos();
			DMonedas moneda = new DMonedas();
			DUnidadesMedida unidad = new DUnidadesMedida();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Codigo.ToString()),
				String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.Descripcion.ToString()),
				String.Format("{0}.{1} " ,unidad.Tabla,NUnidadesMedida.eFields.Unidad.ToString()),
				String.Format("{0}.{1} UnidadMedidaDescripcion" ,unidad.Tabla,NUnidadesMedida.eFields.Descripcion.ToString()),
				String.Format("{0}.{1} ",moneda.Tabla,NMonedas.eFields.Moneda.ToString()),
				String.Format("{0}.{1} as CodigoMoneda",moneda.Tabla,NMonedas.eFields.Codigo.ToString()),
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Insumos*/			
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,insumo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdInsumo.ToString()),
					String.Format("{0}.{1}",detail.Tabla,NMovimientosDetalles.eFields.IdInsumo.ToString())),
					 /*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdUnidad.ToString())),
					/*Join with Monedas*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,moneda.Tabla,DBase.On, 
				   String.Format("{0}.{1}",moneda.Tabla,NMonedas.eFields.IdMoneda.ToString()),
					String.Format("{0}.{1}",insumo.Tabla,NInsumos.eFields.IdMoneda.ToString()))
			  };
			detail.SetConfigJoins(camposJoin, condicionesJoin);
			return detail;
		}
		public static IMovimientosDetalles GetById(int IdMovimientoDetalle)
		{
			NMovimientosDetalles c = new NMovimientosDetalles();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NMovimientosDetalles.eFields.IdMovimientoDetalle.ToString(),
				Valor = IdMovimientoDetalle
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IMovimientosDetalles;
			}
			return null;
		}
		public NMovimientosDetalles()
		{
		}		
	}// NMovimientosDetalles ends.        
}// LibNegocio.Entidades ends.
