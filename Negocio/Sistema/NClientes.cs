using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
using System.Data;
/*Capa Negocio 15/11/2016 04:27:46 p. m.*/
namespace Negocio
{
	///<summary>
	///Catalogo clientes
	///</summary>
	public partial class NClientes : NBase
	{
		public enum eFields
		{
			IdCliente = 0, IdUnidad, RazonSocial, NombreComercial, Rfc, Telefono, Direccion, CodigoPostal, Estatus
		}
		public NClientes(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DClientes clientes= new  DClientes();
			DUnidades unidad=new DUnidades();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin=new string[]
			{
				String.Format("{0}.{1} ",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				/*Join with Unidades*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				String.Format("{0}.{1}",clientes.Tabla,NClientes.eFields.IdUnidad.ToString()),
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString())),
				/*Join with Empresas*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()))
			};
			clientes.SetConfigJoins(camposJoin,condicionesJoin);
			return clientes;
		}
		public NClientes()
		{
		}			
		protected bool ValidaRfc(string rfc)
		{
			NEmpresas emp = new NEmpresas();
			bool valido =emp.ValidaRfc(rfc);
			if(emp.HasError())
			{
				SetError(emp.GetErrorMessage());
			}
			return valido;
		}
		protected bool ValidaDatos()
		{
			if(_data.Delete == 1) return true;
			bool validos = true;
			IClientes cliente = _Entity as IClientes;
			validos = ValidaRfc(cliente.Rfc);			
			return validos;
		}
		public override bool ValidaUpdate()
		{
			return ValidaDatos();
		}
		public override bool ValidaSave()
		{
			return ValidaDatos();
		}


        public static IEnumerable<IClientes> GetClientesFromTable(System.Data.DataTable ClientesTbl)
        {
            if (ClientesTbl == null) return null;
            List<IClientes> Clientes = new List<IClientes>();
            for (int rw = 0; rw < ClientesTbl.Rows.Count; rw++)
            {
                Clientes.Add(new CClientes()
                {
                    Unidad = ClientesTbl.Rows[rw].Field<string>("UNIDAD"),
                    RazonSocial = ClientesTbl.Rows[rw].Field<string>("RAZON_SOCIAL"),
                    NombreComercial = ClientesTbl.Rows[rw].Field<string>("NOMBRE_COMERCIAL"),
                    Rfc = ClientesTbl.Rows[rw].Field<string>("RFC"),
                    Telefono = ClientesTbl.Rows[rw].Field<string>("TELEFONO"),
                    Direccion = ClientesTbl.Rows[rw].Field<string>("DIRECCION"),
                    CodigoPostal = Convert.ToInt32(ClientesTbl.Rows[rw].Field<string>("CODIGO_POSTAL"))
                });
            }
            return Clientes;
        }

        public override IEnumerable<IEntidad> SaveMasivo(IEnumerable<IEntidad> entities)
        {
            entities.ToList().ForEach(
            x =>
            {
                _Entity = x;
                this.Save();
                if (HasError())
                {
                    ((IClientes)x).Empresa = this.GetErrorMessage();
                    this.ClearError();
                }
            });
            return entities;
        }

        public IEnumerable<IClientes> SaveMasivo2(IEnumerable<IClientes> Clientes, int IdEmpresa)
        {
            try
            {
                if (Clientes == null || Clientes.Count() <= 0) return null;

                DUnidades du = new DUnidades();
                DEmpresas de = new DEmpresas();
                string[] Unidades = Clientes.Where(i => !(String.IsNullOrEmpty(i.Unidad))).Select(i => String.Format("'{0}'", i.Unidad)).ToArray();
                string InCondicion = String.Empty;
                IEnumerable<IUnidades> lstUnidades = new List<IUnidades>();
                IEnumerable<IEntidad> Res = null;
                NUnidades nU = new NUnidades();
                NDepartamentos nD = new NDepartamentos();

                List<IPropsWhere> where = new List<IPropsWhere>();
                CPropsWhere whereEmpresa = new CPropsWhere();
                whereEmpresa.Condicion = "=";
                whereEmpresa.OmiteConcatenarTabla = true;
                whereEmpresa.Prioridad = true;
                whereEmpresa.NombreCampo = String.Format("{0}.{1}", de.Tabla, NEmpresas.eFields.IdEmpresa.ToString());
                whereEmpresa.Valor = IdEmpresa;

                CPropsWhere whereCampo = new CPropsWhere();
                whereCampo.Condicion = "in";
                whereCampo.OmiteConcatenarTabla = true;
                whereCampo.Prioridad = true;

                //Buscando Unidades
                InCondicion = String.Join(",", Unidades);
                whereCampo.NombreCampo = String.Format("{0}.{1}", du.Tabla, NUnidades.eFields.Unidad.ToString());
                whereCampo.Valor = InCondicion;
                where = new List<IPropsWhere>();
                where.Add(whereEmpresa);
                where.Add(whereCampo);
                nU.AddWhere(where);
                Res = nU.Buscar();
                if (Res != null && Res.Count() > 0)
                {
                    lstUnidades = Res.Select(x => x as IUnidades);
                }

              
                Clientes.ToList().ForEach(i =>
                {
                    var resF = (!String.IsNullOrEmpty(i.Unidad)) ? lstUnidades.Where(f => f.Unidad.ToLower() == i.Unidad.ToLower() && f.IdEmpresa == IdEmpresa) : null;
                
                    if (resF != null && resF.Count() > 0)
                    {
                        i.IdUnidad = resF.FirstOrDefault().IdUnidad;
                    }

                });

                Clientes.ToList().ForEach(i =>
                {
                    i.Estatus = true;
                });
                this.SaveMasivo(Clientes);
                Clientes = Clientes.Where(i => i.IdCliente <= 0);
            }
            catch (Exception e)
            {
                OnError(e);
            }

            return Clientes;
        }
	}// NClientes ends.        
}// LibNegocio.Entidades ends.
