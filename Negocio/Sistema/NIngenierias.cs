using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NIngenierias : NBase
	{
		public enum eFields
		{
			IdIngenieria = 0, IdProyecto, Ingenieria, Fecha, Estatus
		}
		public NIngenierias(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{			
			DIngenierias ingenieria = new DIngenierias();
			DProyectos proyecto = new DProyectos();
			DCotizaciones cotizacion = new DCotizaciones();			
			DUnidades unidad = new DUnidades();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.Proyecto.ToString()),
				String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.Unidad.ToString()),
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())
				
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Proyectos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,proyecto.Tabla,DBase.On, 
				   String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdProyecto.ToString()),
					String.Format("{0}.{1}",ingenieria.Tabla,NIngenierias.eFields.IdProyecto.ToString())),
					 /*Join with Cotizaciones*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,cotizacion.Tabla,DBase.On, 
				   String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdCotizacion.ToString()),
					String.Format("{0}.{1}",proyecto.Tabla,NProyectos.eFields.IdCotizacion.ToString())),
					 /*Join with Unidades*/
					 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,unidad.Tabla,DBase.On, 
				   String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",cotizacion.Tabla,NCotizaciones.eFields.IdUnidad.ToString())),
					 /*Join with Empresas*/
					 String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",unidad.Tabla,NUnidades.eFields.IdEmpresa.ToString())),
			  };
			ingenieria.SetConfigJoins(camposJoin, condicionesJoin);

			return ingenieria;
		}
		public NIngenierias()
		{
		}
		public IIngenierias GetIngenieriaById(int idIngenieria)
		{		
			IIngenierias ingenieria = null;
			try
			{
				List<IPropsWhere> where = new List<IPropsWhere>();
				where.Add(new CPropsWhere()
				{
					 
				  Prioridad = true,
				  Condicion = "=",
				  NombreCampo = NIngenierias.eFields.IdIngenieria.ToString(),
				  Valor = idIngenieria
				});
				 this.AddWhere(where);
				 IEnumerable<IEntidad> res = this.Buscar();
				if(res != null && res.Count() > 0)
				{
					return res.FirstOrDefault() as IIngenierias;
				}
			}
			catch(Exception e)
			{
				OnError(e);
			}
			return ingenieria;
		
		}
	}// NIngenierias ends.        
}// LibNegocio.Entidades ends.
