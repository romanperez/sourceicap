using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 05/05/2017 01:04:01 p. m.*/
namespace Negocio
{

	public partial class NOTConceptosEjecucion : NBase
	{
		public enum eFields
		{
			IdOTConceptoEjecucion = 0, IdOTConcepto, /*CantidadProgramada,*/ CantidadEjecutada, Fecha
		}
		public NOTConceptosEjecucion()
		{
		}
		public NOTConceptosEjecucion(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DOTConceptosEjecucion();
		}
		public static List<COTConceptosEjecucion> GetEjecucionesByIdOTConcepto(IOTConceptos concepto)		
		{
			if(concepto == null) return null;
			NOTConceptosEjecucion exe = new NOTConceptosEjecucion();
			exe.AddWhere(new CPropsWhere()
			{
				Condicion="=",
				NombreCampo=NOTConceptosEjecucion.eFields.IdOTConcepto.ToString(),
				Prioridad=true,
				Valor=concepto.IdOTConcepto
			});
			IEnumerable<IEntidad> res = exe.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.Select(x => x as COTConceptosEjecucion).ToList();
			}
			return null;
		}
		public static IEnumerable<IOTConceptos> GetOTConceptos(int IdOrdenTrabajo)
		{
			IOrdenesTrabajo ot = new COrdenesTrabajo();
			ot.IdOrdenTrabajo = IdOrdenTrabajo;
			NOrdenesTrabajo n = new NOrdenesTrabajo();
			List<IOrdenesTrabajo> all = new List<IOrdenesTrabajo>();
			all.Add(ot);
			n.GetConceptos(all);
			return ot.OTConceptos;
		}
		public static double GetTotalEjecucionesByIdOTConcepto(IOTConceptos concepto)
		{
			/*IOrdenesTrabajo ot = new COrdenesTrabajo();
			ot.IdOrdenTrabajo = concepto.IdOrdenTrabajo;
			NOrdenesTrabajo n = new NOrdenesTrabajo();
			List<IOrdenesTrabajo> all = new List<IOrdenesTrabajo>();
			all.Add(ot);
			n.GetConceptos(all);
			IEnumerable<IOTConceptos> res = ot.OTConceptos.Where(x => x.IdConcepto == concepto.IdConcepto);
			if(res != null && res.Count()>0)
			{
				return res.Sum(x => x.CantidadEjecutada);
			}
			return 0;*/
			IEnumerable<IOTConceptos> res = NOTConceptosEjecucion.GetOTConceptos(concepto.IdOrdenTrabajo);
			if(res != null && res.Count()>0)
			{
				return res.Where(c=>c.IdContratosDetalles==concepto.IdContratosDetalles).Sum(x => x.CantidadEjecutada);
			}
			return 0;
		}
		public static double GetTotalProgramacionesByIdOTConcepto(IOTConceptos concepto)
		{			
			IEnumerable<IOTConceptos> res = NOTConceptosEjecucion.GetOTConceptos(concepto.IdOrdenTrabajo);
			if(res != null && res.Count() > 0)
			{
				return res.Sum(x => x.CantidadProgramada);
			}
			return 0;
		}
		public static IOTConceptosEjecucion GetById(int id)
		{
			NOTConceptosEjecucion c = new NOTConceptosEjecucion();
			c.AddWhere(new CPropsWhere()
			{
				Condicion = "=",
				Prioridad = true,
				NombreCampo = NOTConceptosEjecucion.eFields.IdOTConceptoEjecucion.ToString(),
				Valor = id
			});
			IEnumerable<IEntidad> res = c.Buscar();
			if(res != null && res.Count() > 0)
			{
				return res.FirstOrDefault() as IOTConceptosEjecucion;
			}
			return null;
		}
		protected virtual bool ValidaEjecucion()
		{
			if(_data.Delete == 1) return true;
			double cantidadEjecutada = 0;
			double cantidadProgramada = 0;
			bool valido = true;
			if(_Entity != null)
			{
				IOrdenesTrabajo orden = null;
				IOTConceptosEjecucion ejecucion= _Entity as IOTConceptosEjecucion;
				IOTConceptos otconcepto =NOTConceptos.GetById(ejecucion.IdOTConcepto);
				IOTConceptosEjecucion update = NOTConceptosEjecucion.GetById(ejecucion.IdOTConceptoEjecucion);
				if(otconcepto != null)
				{
					orden = NOrdenesTrabajo.GetById(otconcepto.IdOrdenTrabajo);
					otconcepto = orden.OTConceptos.Where(c => c.IdOTConcepto == ejecucion.IdOTConcepto).FirstOrDefault();
					if(otconcepto != null)
					{
						cantidadEjecutada = otconcepto.CantidadEjecutada; //NOTConceptosEjecucion.GetTotalEjecucionesByIdOTConcepto(otconcepto);
						cantidadProgramada = otconcepto.CantidadProgramada; //NOTConceptosEjecucion.GetTotalProgramacionesByIdOTConcepto(otconcepto);
					}
				}								
				cantidadEjecutada += ejecucion.CantidadEjecutada;
				cantidadProgramada += ejecucion.CantidadProgramada;
				if(update != null)
				{
					/*Si actualizamos un registro ls csntidad se contabiliza con respecto a la nueva cantidad*/
					cantidadEjecutada -= update.CantidadEjecutada;
					cantidadProgramada -= update.CantidadProgramada;
					/*Si hay una ejecucion ya no se permite cambiar la cantidad programada*/
					if(update.CantidadEjecutada>0)//cantidadEjecutada > 0)
					{
						ejecucion.CantidadProgramada = update.CantidadProgramada;
					}					
				}
				if(cantidadProgramada > otconcepto.Cantidad)
				{
					SetError(TraduceTexto("NOTConceptosEjecucionProgramacionesAlcanzadas"));
					return false;
				}
				if(cantidadEjecutada > otconcepto.Cantidad)
				{
					SetError(TraduceTexto("NOTConceptosEjecucionEjecucionesAlcanzadas"));
					return false;
				}
				if(cantidadProgramada <= 0 && cantidadEjecutada > 0)
				{
					SetError(TraduceTexto("NOTConceptosEjecucionPorgramadaZero"));
					return false;
				}
				if(orden != null)				
				{
					if(!(ejecucion.Fecha.Date >= orden.FechaInicio.Date && ejecucion.Fecha.Date <= orden.FechaFin.Date))
					{
						SetError(String.Format(TraduceTexto("NOTConceptosFechaIncorrecta"),orden.FechaInicio.ToString("d"),orden.FechaFin.ToString("d")));
						return false;
					}
				}
			}
			return valido;
		}
		public override void Save()
		{
			if(ValidaEjecucion())
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Update()
		{
			if(ValidaEjecucion())
			{
				if(_data == null) return;
				_data.Save(_Entity);
			}
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
	}// NOTConceptosEjecucion ends.        
}// LibNegocio.Entidades ends.
