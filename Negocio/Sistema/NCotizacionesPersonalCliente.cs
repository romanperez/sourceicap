using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 02/06/2017 11:10:20 a. m.*/
namespace Negocio
{

	public partial class NCotizacionesPersonalCliente : NBase
	{
		public enum eFields
		{
			IdCotizacionPersonalCliente = 0, IdCotizacion, IdEstatus, IdPersonalCliente
		}
		public NCotizacionesPersonalCliente()
		{
		}
		public NCotizacionesPersonalCliente(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DCotizacionesPersonalCliente Cotipersonal = new DCotizacionesPersonalCliente();
			DClientesPersonal personal = new DClientesPersonal();
			DEstatus estatus = new DEstatus();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1} as NombreEstatus",estatus.Tabla,NEstatus.eFields.Nombre.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.Nombre.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.ApellidoPaterno.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.ApellidoMaterno.ToString()),
				String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.Puesto.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with ClientesPersonal*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,personal.Tabla,DBase.On, 
				   String.Format("{0}.{1}",personal.Tabla,NClientesPersonal.eFields.IdPersonalCliente.ToString()),
					String.Format("{0}.{1}",Cotipersonal.Tabla,NOTPersonalCliente.eFields.IdPersonalCliente.ToString()))	,
					/*Join with Estatus*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,estatus.Tabla,DBase.On, 
				   String.Format("{0}.{1}",estatus.Tabla,NEstatus.eFields.IdEstatus.ToString()),
					String.Format("{0}.{1}",Cotipersonal.Tabla,NOTPersonalCliente.eFields.IdEstatus.ToString()))
			  };
			Cotipersonal.SetConfigJoins(camposJoin, condicionesJoin);
			return Cotipersonal;
		}
		public IParametros PersonalAtencion()
		{
			return NParametros.GetByNombre("PREFIJO_OT_PERSONAL_ATENCION");
		}
		public IParametros PersonalContacto()
		{
			return NParametros.GetByNombre("PREFIJO_OT_PERSONAL_CONTACTO");
		}
		public IEstatus EstatusCotizacionPersonalAtencion(int IdEmpresa)
		{
			NEstatus estatus = new NEstatus();
			IParametros pAtnecion = PersonalAtencion();
			IEnumerable<IEstatus> resAtencion = estatus.ByClasificacion(IdEmpresa, pAtnecion.Valor.ToString());
			if(resAtencion.Any())
			{
				return resAtencion.FirstOrDefault();
			}
			return new CEstatus();
		}
		public IEstatus EstatusCotizacionPersonalContacto(int IdEmpresa)
		{
			NEstatus estatus = new NEstatus();
			IParametros pContacto = PersonalContacto();
			IEnumerable<IEstatus> resContacto = estatus.ByClasificacion(IdEmpresa, pContacto.Valor.ToString());
			if(resContacto.Any())
			{
				return resContacto.FirstOrDefault();
			}
			return new CEstatus();
		}
	}// NCotizacionesPersonalCliente ends.        
}// LibNegocio.Entidades ends.
