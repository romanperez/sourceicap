using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:45 p. m.*/
namespace Negocio
{
	///<summary>
	///Catalogo actividades
	///</summary>
	public partial class NActividades : NBase
	{
		public enum eFields
		{
			IdActividad = 0, IdEmpresa, IdEspecialidad, IdConceptoClasificacion, Actividad, Descripcion, ProcedimientoSeguridad, AnalisisRiesgo, Estatus
		}
		public NActividades(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DActividades actividad = new DActividades();
			DEmpresas empresa = new DEmpresas();
			DEspecialidades especialidad = new DEspecialidades();
			DClasificacionConceptos clasificacion = new DClasificacionConceptos();
			string[] camposJoin = new string[]
			{				
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString()),
				String.Format("{0}.{1} as Especialidad",especialidad.Tabla,NEspecialidades.eFields.Nombre.ToString()),
			   String.Format("{0}.{1} ",clasificacion.Tabla,NClasificacionConceptos.eFields.Clasificacion.ToString())
			};
			string[] condicionesJoin = new string[] 
			{ 
				/*Join with Unidades*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
				String.Format("{0}.{1}",actividad.Tabla,NActividades.eFields.IdEmpresa.ToString())),
				/*Join with Especialidades*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,especialidad.Tabla,DBase.On, 
				String.Format("{0}.{1}",especialidad.Tabla,NEspecialidades.eFields.IdEspecialidad.ToString()),
				String.Format("{0}.{1}",actividad.Tabla,NActividades.eFields.IdEspecialidad.ToString())),
				/*Join with ClasificacionConceptos*/
				String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,clasificacion.Tabla,DBase.On, 
				String.Format("{0}.{1}",clasificacion.Tabla,NClasificacionConceptos.eFields.IdConceptoClasificacion.ToString()),
				String.Format("{0}.{1}",actividad.Tabla,NActividades.eFields.IdConceptoClasificacion.ToString()))
			};
			actividad.SetConfigJoins(camposJoin, condicionesJoin);
			return actividad;
		}
		public NActividades()
		{
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IActividades", new DActividades());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
	}// NActividades ends.        
}// LibNegocio.Entidades ends.
