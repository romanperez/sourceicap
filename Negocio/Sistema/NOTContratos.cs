using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 26/04/2017 01:51:07 p. m.*/
namespace Negocio
{

	public partial class NOTContratos : NBase
	{
		public enum eFields
		{
			IdContratoOrden = 0, IdOrdenTrabajo, IdContrato
		}
		public NOTContratos()
		{
		}
		public NOTContratos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DOTContratos contrato = new DOTContratos();
			DContratos2 co2 = new DContratos2();
			string[] camposJoin = new string[]
				  {																	
					  String.Format("{0}.{1}",co2.Tabla,NContratos.eFields.Contrato.ToString())
				  };
			string[] condicionesJoin = new string[] 
				  { 								  
					  /*Join with Contratos*/						
					   String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,co2.Tabla,DBase.On, 
					   String.Format("{0}.{1}",co2.Tabla,NContratos.eFields.IdContrato.ToString()),
					   String.Format("{0}.{1}",contrato.Tabla,NOTContratos.eFields.IdContrato.ToString()))
				  };
			contrato.SetConfigJoins(camposJoin, condicionesJoin);
			return contrato;			
		}
	}// NOTContratos ends.        
}// LibNegocio.Entidades ends.
