using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 26/01/2017 04:27:09 p. m.*/
namespace Negocio
{

	public partial class NFamilias : NBase
	{
		public enum eFields
		{
			IdFamilia = 0, Familia, Descripcion, Estatus, IdEmpresa
		}
		public NFamilias()
		{
		}
		public NFamilias(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			DFamilias familia = new DFamilias();
			DEmpresas empresa = new DEmpresas();
			string[] camposJoin = new string[]
			{
				String.Format("{0}.{1} ",empresa.Tabla,NEmpresas.eFields.Empresa.ToString())			
			};
			string[] condicionesJoin = new string[] 
			{ 				
            String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,empresa.Tabla,DBase.On, 
				   String.Format("{0}.{1}",empresa.Tabla,NEmpresas.eFields.IdEmpresa.ToString()),
					String.Format("{0}.{1}",familia.Tabla,NFamilias.eFields.IdEmpresa.ToString()))
			};
			familia.SetConfigJoins(camposJoin, condicionesJoin);
			return familia;
		}
		protected virtual bool ValidaDuplicado(bool isNew)
		{
			bool isDuplicado = this.IsDuplicado(_Entity, isNew, "IFamilias", new DFamilias());
			if(isDuplicado)
			{
				SetError(String.Format(TraduceTexto("NBaseRegistroDuplicado"), this.DuplicatedInfo.ToString()));
			}
			return isDuplicado;
		}
		public override void Save()
		{
			if(ValidaDuplicado(true)) return;
			base.Save();
		}
		public override void Update()
		{
			if(ValidaDuplicado(false)) return;
			base.Update();
		}
		public override void Delete()
		{
			if(ValidaDelete())
			{
				if(_data == null) return;
				_data.Delete = 1;
				_data.Save(_Entity);
			}
		}
	}// NFamilias ends.        
}// LibNegocio.Entidades ends.
