using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 15/11/2016 04:27:47 p. m.*/
namespace Negocio
{

	public partial class NOTDocumentos : NBase
	{
		public enum eFields
		{
			IdOTDocumentos = 0, IdOrdenTrabajo, IdTipoDocumento, RutaDocumento, Estatus
		}
		public NOTDocumentos(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{
			return new DOTDocumentos();
		}
		public NOTDocumentos()
		{
		}

	}// NOTDocumentos ends.        
}// LibNegocio.Entidades ends.
