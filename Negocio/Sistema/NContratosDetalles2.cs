using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos;
/*Capa Negocio 30/03/2017 03:46:55 p. m.*/
namespace Negocio
{

	public partial class NContratosDetalles2 : NBase
	{
		public enum eFields
		{
			IdContratosDetalles = 0, IdContrato, IdConcepto,Partida, Precio, PorcentajeDescuento, DescuentoMonto, Cantidad
		}
		public NContratosDetalles2()
		{
		}
		public NContratosDetalles2(IEntidad entidad)
			: base(entidad)
		{
		}
		protected override IBase BuildDataEntity()
		{		
			DContratosDetalles2 contratoDetalles = new DContratosDetalles2();
			DConceptos conceptos = new DConceptos();
			DCapitulos capitulo = new DCapitulos();
			DUnidadesMedida um = new DUnidadesMedida();
			DContratos2 contrato = new DContratos2();
			string[] camposJoin = new string[]
			  {								
				String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.Capitulo.ToString()),				
				String.Format("{0}.{1} as UnidadMedida",um.Tabla,NUnidadesMedida.eFields.Unidad.ToString()),				
				String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.Contrato.ToString())
			  };
			string[] condicionesJoin = new string[] 
			  { 
				   /*Join with Conceptos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,conceptos.Tabla,DBase.On, 
				   String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdConcepto.ToString()),
					String.Format("{0}.{1}",contratoDetalles.Tabla,NContratosDetalles2.eFields.IdConcepto.ToString()))	,
					/*Join with Capitulos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,capitulo.Tabla,DBase.On, 
				   String.Format("{0}.{1}",capitulo.Tabla,NCapitulos.eFields.IdCapitulo.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdCapitulo.ToString())),
						/*Join with UnidadesMedida*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.LeftJoin,um.Tabla,DBase.On, 
				   String.Format("{0}.{1}",um.Tabla,NUnidadesMedida.eFields.IdUnidad.ToString()),
					String.Format("{0}.{1}",conceptos.Tabla,NConceptos.eFields.IdUnidadMedida.ToString())),
						/*Join with Contratos*/
					String.Format("{0} {1} {2} {3} = {4}", DBase.InnerJoin,contrato.Tabla,DBase.On, 
				   String.Format("{0}.{1}",contrato.Tabla,NContratos.eFields.IdContrato.ToString()),
					String.Format("{0}.{1}",contratoDetalles.Tabla,NContratosDetalles2.eFields.IdContrato.ToString()))
			  };
			contratoDetalles.SetConfigJoins(camposJoin, condicionesJoin);
			return contratoDetalles;
		}
		public IEnumerable<IPropsWhere> GetConceptosByIdContrato(IEnumerable<IContratosDetalles2> contratos)
		{
			if(contratos == null || contratos.Count()<0) return null;
			int[] ids = contratos.Select(x => x.IdContrato).ToArray();
			string sids = String.Join(",", ids);
			List<CPropsWhere> where = new List<CPropsWhere>();
			where.Add(new CPropsWhere()
			{
				NombreCampo = NContratosDetalles2.eFields.IdContrato.ToString(),				
				Prioridad = true,
				Valor = sids,
				Condicion = "in"
			});
			return where;
		}
		public static IOTConceptos ToOTConcepto(IContratosDetalles2 detalle)
		{
			if(detalle == null) return null;
			int IdConceptoClasificacion = 0;
			int.TryParse(detalle.InfoConcepto.IdConceptoClasificacion.ToString(), out IdConceptoClasificacion);
			return new COTConceptos()
			{
				Cantidad=detalle.Cantidad,
				IdConcepto=detalle.IdConcepto,
				Concepto=detalle.Concepto,
				Contrato = detalle.Contrato,
				IdContratosDetalles=detalle.IdContratosDetalles,
				UnidadMedida=detalle.UnidadMedida,
				Descripcion=detalle.Descripcion,
				InfoConcepto = detalle.InfoConcepto,
				Ubicacion =detalle.Ubicacion,
				Clasificacion=detalle.InfoConcepto.Clasificacion,
				IdConceptoClasificacion = IdConceptoClasificacion,//detalle.InfoConcepto.IdConceptoClasificacion,
				Partida =detalle.Partida,
				EsAdicional=detalle.EsAdicional
			};
		}
		public override IEnumerable<IEntidad> Buscar()
		{
			IEnumerable<IEntidad> res= base.Buscar();
			if(res != null && res.Count() > 0)
			{
				GetConcepto(res.Select(x => x as IContratosDetalles2));
				return res;
			}
			return null;
		}
		protected string BuildIds(IEnumerable<IContratosDetalles2> conceptos)
		{
			int[] ids = (from tbl in conceptos
								select tbl.IdConcepto).ToArray();
			return String.Join(",", ids);
		}
		protected void GetConcepto(IEnumerable<IContratosDetalles2> conceptos)
		{
			string ids = BuildIds(conceptos);
			List<IPropsWhere> where = new List<IPropsWhere>();
			where.Add(new CPropsWhere()
			{
				Prioridad = true,
				Condicion = "in",
				NombreCampo = NConceptos.eFields.IdConcepto.ToString(),
				Valor = ids
			});
			NConceptos n = new NConceptos();
			n.AddWhere(where);
			IEnumerable<IEntidad> res1 = n.Buscar();
			IEnumerable<IConceptos> allConceptos = null;
			if(res1.Any())
			{
				allConceptos = res1.Select(x => x as IConceptos);
				conceptos.ToList().ForEach(x =>
				{
					IEnumerable<IConceptos> aux = allConceptos.Where(y => y.IdConcepto == x.IdConcepto);
					if(aux!= null && aux.Count()>0)
					{
						x.InfoConcepto = aux.Select(y => y as CConceptos).FirstOrDefault();
					}

				});
			}			
		}
	}// NContratosDetalles2 ends.        
}// LibNegocio.Entidades ends.
