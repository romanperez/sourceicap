﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Entidades;
using Datos;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Data;
namespace Negocio
{
	
   public class NExcel<Entidad> : IExcel where Entidad : IEntidad, new()
	{
		public virtual string InterfaceName
		{
			set;
			get;
		}
		public virtual string FileOutPut
		{
			set;
			get;
		}
		public virtual string NameSheet
		{
			set;
			get;
		}
		public virtual string FileName
		{
			set;
			get;
		}
		Entidad _Plantilla;
		public NExcel()
		{
			Inicializa();
		}
		protected void Inicializa()
		{
			_Plantilla = new Entidad();
		}
		public IEnumerable<IPropsWhere> GetColumnsHeaders()
		{
			if(String.IsNullOrEmpty(InterfaceName))
			{
				throw new Exception("NO INTERFACE DETECTED.");
			}
			string[] DefaultFieldsOmitir = _Plantilla.GetFieldsExcelExportOmitir();//Obtenemos los campos que no deseamos exportar.
			string[] DefaultFields = _Plantilla.GetFieldsExcelExport();//Si no es nulo omitimos reflection y cosntruimos los campos manualmente.
			List<IPropsWhere> Propiedades = new List<IPropsWhere>();
			if(DefaultFields == null)
			{
				Type objType = _Plantilla.GetType();
				if(objType != null)
				{
					PropertyInfo[] properties = objType.GetInterface(InterfaceName).GetProperties();
					foreach(PropertyInfo propiedad in properties)
					{
						DisplayAttribute displayAttribute = propiedad.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault() as DisplayAttribute;
						if(displayAttribute != null)
						{
							bool add = true;
							if(DefaultFieldsOmitir != null)
							{
								IEnumerable<string> existe = from tbl in DefaultFieldsOmitir
																	  where tbl == propiedad.Name
																	  select tbl;
								if(existe != null && existe.Count() > 0)
								{
									add = false;
								}
							}
							if(add)
							{
								Propiedades.Add(new CPropsWhere()
								{
									NombreCampo = propiedad.Name, /*Nombre del campo*/
									Condicion = CIdioma.TraduceCadena(displayAttribute.Name)/*Texto a mostrar en ui*/
								});
							}
						}
					}
				}
			}//if(DefaultFIelds == null) ends.
			else
			{
				DefaultFields.ToList().ForEach(x => Propiedades.Add(new CPropsWhere()
				{
					NombreCampo = x.Split('|')[0], /*Nombre del campo*/
					Condicion = CIdioma.TraduceCadena(x.Split('|')[1]) /*Texto a mostrar en ui*/
				}));
			}
			return Propiedades;
		}

		protected MemoryStream ExcelToMemoryStream()
		{
			MemoryStream memoryStream = new MemoryStream();
			SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook);
			return memoryStream;

		}
		protected MemoryStream ExcelToMemoryStream(byte []xls)
		{
			MemoryStream memoryStream = new MemoryStream(xls);

			byte[] dd = ReadStream(memoryStream);
			SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook);
			return memoryStream;

		}
		public  byte[] ReadStream(Stream input)
		{			
			input.Position = 0;
			using(var memoryStream = new MemoryStream())
			{
				input.CopyTo(memoryStream);
				return memoryStream.ToArray();
			}
		}		
		public DataTable MainByteToExcel(byte[] xls)
		{
			int rError = 0;
			if(xls == null || xls.Length <= 0) return null;
			DataTable dtXlsInfo = new DataTable();
			Stream xlsBytes = new MemoryStream(xls);			
			using(SpreadsheetDocument doc = SpreadsheetDocument.Open(xlsBytes, false))
			{				
				Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
				Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
				IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
				
				foreach(Row row in rows)
				{					
					if(row.RowIndex.Value == 1)
					{
						foreach(Cell cell in row.Descendants<Cell>())
						{
							dtXlsInfo.Columns.Add(GetValue(doc, cell));
						}
					}
					else
					{
						//Add rows to DataTable.
						dtXlsInfo.Rows.Add();
						int i = 0;
						try
						{							
							/*foreach(Cell cell in row.Descendants<Cell>())
							{
								dtXlsInfo.Rows[dtXlsInfo.Rows.Count - 1][i] = GetValue(doc, cell);
								i++;
							}*/
							IEnumerable<Cell> cells = SpreedsheetHelper.GetRowCells(row);
							foreach(Cell cell in cells)
							{
								dtXlsInfo.Rows[dtXlsInfo.Rows.Count - 1][i] = GetValue(doc, cell);
								i++;
							}

						}
						catch(Exception)
						{
							//int rowe = dtXlsInfo.Rows.Count - 1;
							//int TotalRows = rows.Count();
							rError++;
							if(rError >= 6) break;
						}
					}
				}					
			}
			return dtXlsInfo;
		}
		protected string GetValue(SpreadsheetDocument doc, Cell cell)
		{
			/*string value = cell.CellValue.InnerText;
			if(cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
			{
				return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
			}
			return value;*/
			SharedStringTablePart stringTablePart = doc.WorkbookPart.SharedStringTablePart;
			if(cell.CellValue == null)
			{
				return "";
			}
			string value = cell.CellValue.InnerXml;
			if(cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
			{
				return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
			}
			else
			{
				return value;
			}
		}
		public byte[] MainExcel(IEnumerable<IEntidad> datos)
		{
			IEnumerable<IPropsWhere> Columns = GetColumnsHeaders();
			if(Columns == null || Columns.Count() <= 0) return null;
			MemoryStream memoryStream = ExcelToMemoryStream();
			using(var workbook = SpreadsheetDocument.Create(/*FileOutPut*/memoryStream, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
			{
				var workbookPart = workbook.AddWorkbookPart();
				workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
				workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();
				var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
				var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
				sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);
				DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
				string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);
				uint sheetId = 1;
				if(sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
				{
					sheetId =sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
				}

				DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet()
				{
					Id = relationshipId,
					SheetId = sheetId,
					Name = NameSheet
				};
				sheets.Append(sheet);
				DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();				
				foreach(IPropsWhere propiedad in Columns)
				{
					DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
					cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
					cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(propiedad.Condicion);
					headerRow.AppendChild(cell);
				}
				sheetData.AppendChild(headerRow);
				foreach(IEntidad data in datos)
				{
					DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
					foreach(IPropsWhere propiedad in Columns)
					{
						DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
						cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
						object valorr = DBase.GetValue(data, propiedad.NombreCampo);
						if(valorr != null)
						{
							cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(valorr.ToString());
						}
						newRow.AppendChild(cell);
					}
					sheetData.AppendChild(newRow);
				}
			}//Using Ends.
			return ReadStream(memoryStream);
		}//MainExcel Ends.
	}//NExcel Ends.	
}
public class SpreedsheetHelper
{
	///<summary>returns an empty cell when a blank cell is encountered
	///</summary>
	public static IEnumerable<Cell> GetRowCells(Row row)
	{
		int currentCount = 0;

		foreach(DocumentFormat.OpenXml.Spreadsheet.Cell cell in
			 row.Descendants<DocumentFormat.OpenXml.Spreadsheet.Cell>())
		{
			string columnName = GetColumnName(cell.CellReference);

			int currentColumnIndex = ConvertColumnNameToNumber(columnName);

			for(; currentCount < currentColumnIndex; currentCount++)
			{
				yield return new DocumentFormat.OpenXml.Spreadsheet.Cell();
			}

			yield return cell;
			currentCount++;
		}
	}

	/// <summary>
	/// Given a cell name, parses the specified cell to get the column name.
	/// </summary>
	/// <param name="cellReference">Address of the cell (ie. B2)</param>
	/// <returns>Column Name (ie. B)</returns>
	public static string GetColumnName(string cellReference)
	{
		// Match the column name portion of the cell name.
		var regex = new System.Text.RegularExpressions.Regex("[A-Za-z]+");
		var match = regex.Match(cellReference);

		return match.Value;
	}

	/// <summary>
	/// Given just the column name (no row index),
	/// it will return the zero based column index.
	/// </summary>
	/// <param name="columnName">Column Name (ie. A or AB)</param>
	/// <returns>Zero based index if the conversion was successful</returns>
	/// <exception cref="ArgumentException">thrown if the given string
	/// contains characters other than uppercase letters</exception>
	public static int ConvertColumnNameToNumber(string columnName)
	{
		var alpha = new System.Text.RegularExpressions.Regex("^[A-Z]+$");
		if(!alpha.IsMatch(columnName)) throw new ArgumentException();

		char[] colLetters = columnName.ToCharArray();
		Array.Reverse(colLetters);

		int convertedValue = 0;
		for(int i = 0; i < colLetters.Length; i++)
		{
			char letter = colLetters[i];
			int current = i == 0 ? letter - 65 : letter - 64; // ASCII 'A' = 65
			convertedValue += current * (int)Math.Pow(26, i);
		}

		return convertedValue;
	}
}