﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptImprimeCotizacion : ReporteBase
	{
		public byte[] GeneraReporte(int IdCotizacion, ReportViewer Reporte)
		{
			string url = /*Controladores.MVC.MvcBase.GetBaseUrl()*/ base.GetURLWebService() + "Cotizaciones/GetDetalleCotizacion/" + IdCotizacion;
			RestSharp.IRestResponse response = Peticion(url, null, RestSharp.Method.GET);// Controladores.MVC.MvcBase.BuildPeticionService(url, null, RestSharp.Method.GET);
			ICotizaciones coti = new CCotizaciones();
			List<ICotizaciones> all = new List<ICotizaciones>();
			IEmpleados empleado = new CEmpleados();
			List<IEmpleados> allEmpleados = new List<IEmpleados>();
			int IdEmpleado = 0;
			if(response != null)
			{
				coti = CBase._ToJson<CCotizaciones>(response.Content);
				if(coti != null)
				{
					if(!String.IsNullOrEmpty(coti.FechaEntrega))
					{
						coti.FechaEntrega = coti.FechaEntrega.Trim();
					}
					all.Add(coti);
					int.TryParse(coti.Usuario.IdEmpleado.ToString(), out IdEmpleado);
					empleado = GetEmpleado(IdEmpleado);
					if(empleado != null)
					{
						allEmpleados.Add(empleado);
					}					
				}
			}
			var reportDataSource = new ReportDataSource
			{
				Name = "Cotizacion",
				Value = all
			};
			var reportDataSource1 = new ReportDataSource
			{
				Name = "Conceptos",
				Value = coti.Conceptos
			};
			var reportDataSource2 = new ReportDataSource
			{
				Name = "Empleado",
				Value = allEmpleados
			};
			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "Cotizacion.rdlc");
			Reporte.LocalReport.DataSources.Add(reportDataSource);
			Reporte.LocalReport.DataSources.Add(reportDataSource1);
			Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(Reporte);
		}
	
	}
}
