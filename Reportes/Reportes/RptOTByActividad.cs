﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptOTByActividad : ReporteBase
	{
		public byte[] GeneraReporte(int IdOrdenTrabajo, ReportViewer Reporte)
		{
			string url = /*Controladores.MVC.MvcBase.GetBaseUrl()*/base.GetURLWebService() + "OrdenesTrabajo/Get/" + IdOrdenTrabajo;
			RestSharp.IRestResponse response = Peticion(url, null, RestSharp.Method.GET);// Controladores.MVC.MvcBase.BuildPeticionService(url, null, RestSharp.Method.GET);
			IOrdenesTrabajo ot = new COrdenesTrabajo();
			List<IOrdenesTrabajo> ordenes = new List<IOrdenesTrabajo>();
			List<IOTPersonal> tecnicos = new List<IOTPersonal>();
			List<IEmpresas> allEmpresas = new List<IEmpresas>();
			IEmpresas empresa = null;
			if(response != null)
			{
				ot = CBase._ToJson<COrdenesTrabajo>(response.Content);
				if(ot != null)
				{
					ordenes.Add(ot);
					if(ot.OTPersonal != null && ot.OTPersonal.Count > 0)
					{
						ot.OTPersonal.ForEach(x => tecnicos.Add(x));
					}
					empresa = GetEmpresaById(ot.IdEmpresa);
					if(empresa != null)
					{
						allEmpresas.Add(empresa);
					}
				}

			}
			var dsOT = new ReportDataSource
			{
				Name = "OT",
				Value = ordenes
			};
			//var dsTecnicos = new ReportDataSource
			//{
			//	Name = "Tecnicos",
			//	Value = tecnicos
			//};
			var dsEmpresa = new ReportDataSource
			{
				Name = "Empresa",
				Value = allEmpresas
			};
			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "OTActividades.rdlc"); 
			Reporte.LocalReport.DataSources.Add(dsOT);
			//Reporte.LocalReport.DataSources.Add(dsTecnicos);
			Reporte.LocalReport.DataSources.Add(dsEmpresa);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(Reporte);
		}
		public byte[] GeneraReporte(IEnumerable<IOrdenesTrabajo> Ordenes, ReportViewer Reporte, DateTime fecha)
		{

			List<IEmpresas> empresas =new List<IEmpresas>();
			empresas.Add(GetEmpresaById(Ordenes.FirstOrDefault().IdEmpresa));
		
			var dsOT = new ReportDataSource
			{
				Name = "OT",
				Value = Ordenes
			};			
			var dsEmpresa = new ReportDataSource
			{
				Name = "Empresa",
				Value = empresas
			};
			ReportParameter[] parametros = new ReportParameter[1];
			parametros[0] = new ReportParameter("FechaHoy",fecha.ToString("d"), false);
			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "OTActividades.rdlc");
			Reporte.LocalReport.SetParameters(parametros);
			Reporte.LocalReport.DataSources.Add(dsOT);			
			Reporte.LocalReport.DataSources.Add(dsEmpresa);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(Reporte);
		}
	}
}
