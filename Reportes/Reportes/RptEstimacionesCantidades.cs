﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
    public class RptEstimacionesCantidades : ReporteBase
	{
        public byte[] GeneraReporte(ETipoReporte TipoReporte, IEnumerable<IEstimaciones> Estimaciones)
		{			
			ReportViewer Reporte = new ReportViewer();			
			
			var reportDataSource2 = new ReportDataSource
			{
                Name = "Estimaciones",
                Value = Estimaciones
			};
            Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "EstimacionesCantidades.rdlc");			
            Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(TipoReporte.ToString(), Reporte);
		}
	}
}
