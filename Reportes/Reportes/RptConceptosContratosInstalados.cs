﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptConceptosContratosInstalados : ReporteBase
	{

		public byte[] GeneraReporte(ETipoReporte TipoReporte,
											 IEnumerable<IContratosDetalles2> ConceptosContratos, string nombreProyecto)
		{
			if(ConceptosContratos == null || ConceptosContratos.Count() <= 0) return null;

			ReportViewer Reporte = new ReportViewer();

			var reportDataSource2 = new ReportDataSource
			{
				Name = "ConceptosContratos",
				Value = ConceptosContratos
			};
			List<ReportParameter> parametros = new List<ReportParameter>();
			parametros.Add(new ReportParameter("NombreProyecto", nombreProyecto));
			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ConceptosContratosInstalados2.rdlc");
			Reporte.LocalReport.SetParameters(parametros);
			Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(TipoReporte.ToString(), Reporte);
		}

	}
}
