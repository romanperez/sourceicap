﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
namespace Reportes
{
	public class ReporteBase
	{
		public delegate RestSharp.IRestResponse BuildPeticionService(string url, string body, RestSharp.Method Verb);
		public string UrlBase
		{
			set;
			get;
		}
		public BuildPeticionService Peticion
		{
			set;
			get;
		}
		protected const string PATH_LASEC_LOGO = @"Reportes\LasecLogo.jpg";
		protected const string PATH_BECKER_LOGO = @"Reportes\BeckerLogo.jpg";
		protected const string PATH_BASE = @"bin\RDLC";
		public virtual string urlWebService
		{
			set;
			get;
		}
		protected string GetURLWebService()
		{
			return UrlBase;//Controladores.MVC.MvcBase.GetBaseUrl();
		}
		protected byte[] ReporteToByteArray(string FormatoSalida,ReportViewer reporte)
		{
			Warning[] warnings;
			string[] streamids;
			string mimeType;
			string encoding;
			string filenameExtension;
			return reporte.LocalReport.Render(FormatoSalida, null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
		}
		protected byte[] ReporteToByteArray(ReportViewer reporte)
		{
			return ReporteToByteArray("PDF",reporte);		
		}
		protected IEmpresas GetEmpresaById(int IdEmpresa)
		{
			if(IdEmpresa <= 0) return null;
			string url = GetURLWebService() + "Empresas/Get/" + IdEmpresa;
			RestSharp.IRestResponse response = Peticion(url, null, RestSharp.Method.GET); //Controladores.MVC.MvcBase.BuildPeticionService(url, null, RestSharp.Method.GET);
			IEmpresas empresa = null;
			if(response != null)
			{
				empresa = CBase._ToJson<CEmpresas>(response.Content);
			}
			return empresa;
		}
		protected IEmpleados GetEmpleado(int IdEmpleado)
		{
			if(IdEmpleado <= 0) return null;
			string url = GetURLWebService() + "Empleados/Get/" + IdEmpleado;
			RestSharp.IRestResponse response = Peticion(url, null, RestSharp.Method.GET); //Controladores.MVC.MvcBase.BuildPeticionService(url, null, RestSharp.Method.GET);
			IEmpleados empleado = null;
			if(response != null)
			{
				empleado = CBase._ToJson<CEmpleados>(response.Content);
			}
			return empleado;
		}
		protected IOTDocumentos GetDocumentoById(int IdDocumento)
		{
			if(IdDocumento <= 0) return null;
			string url = GetURLWebService() + "OTDocumentos/Get/" + IdDocumento;
			RestSharp.IRestResponse response = Peticion(url, null, RestSharp.Method.GET);// Controladores.MVC.MvcBase.BuildPeticionService(url, null, RestSharp.Method.GET);
			IOTDocumentos documento = null;
			if(response != null)
			{
				documento = CBase._ToJson<COTDocumentos>(response.Content);
			}
			return documento;
		}
		protected DFile DownloadDocumento(IOTDocumentos documento)
		{

			if(documento == null) return null;
			DFile doc = new DFile();
			doc.Url = documento.RutaDocumento;
			string url = GetURLWebService() + "OrdenesTrabajo/GetFile";
			string body = CBase._ToJson(doc);
			RestSharp.IRestResponse response = Peticion(url, body, RestSharp.Method.POST);// Controladores.MVC.MvcBase.BuildPeticionService(url, body, RestSharp.Method.POST);
			if(response != null)
			{
				doc = CBase._ToJson<DFile>(response.Content);
				if(doc != null && !String.IsNullOrEmpty(doc.ContenidoString))
				{
					doc.Contenido = Convert.FromBase64String(doc.ContenidoString);
					doc.Name = System.IO.Path.GetFileName(doc.Url);
				}
			}
			return doc;
		}
		protected List<DFile> GetLogos(string Name)
		{		
			List<DFile> all = new List<DFile>();
			IOTDocumentos doc = new COTDocumentos();
			doc.RutaDocumento = Name;
			DFile fil = DownloadDocumento(doc);
			all.Add(fil);
			return all;
		} 
		protected List<DFile> GetLogoLasec()
		{
			return GetLogos(ReporteBase.PATH_LASEC_LOGO);
		}
		protected List<DFile> GetLogoBecker()
		{
			return GetLogos(ReporteBase.PATH_BECKER_LOGO);
		}
		protected ReportDataSource dsLogoLasec(string nombreLogo)
		{
			ReportDataSource dsListLogo = new ReportDataSource
			{
				Name = nombreLogo,
				Value = GetLogoLasec()
			};
			return dsListLogo;
		}
		protected ReportDataSource dsLogoBecker(string nombreLogo)
		{
			ReportDataSource dsListLogo = new ReportDataSource
			{
				Name = nombreLogo,
				Value = GetLogoBecker()
			};
			return dsListLogo;
		}
	}
}
