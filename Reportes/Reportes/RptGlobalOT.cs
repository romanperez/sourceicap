﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptGlobalOT:ReporteBase
	{
		public virtual int IdOrdenTrabajo
		{
			set;
			get;
		}
		public virtual CParamRptOTGlobal Parametros
		{
			set;
			get;
		}
		public byte[] GeneraReporte(ReportViewer Reporte)
		{
			string url = base.GetURLWebService() + "OrdenesTrabajo/Get/" + IdOrdenTrabajo;
			RestSharp.IRestResponse response = Peticion(url, null, RestSharp.Method.GET);// Controladores.MVC.MvcBase.BuildPeticionService(url, null, RestSharp.Method.GET);
			IOrdenesTrabajo ot = new COrdenesTrabajo();
			List<IOrdenesTrabajo> ordenes = new List<IOrdenesTrabajo>();
			List<IOTPersonal> tecnicos = new List<IOTPersonal>();
			List<IOTConceptos> allConceptos = new List<IOTConceptos>();
			List<IEmpresas> allEmpresas = new List<IEmpresas>();
			List<IContratosDetallesInsumos2> allInsumos = new List<IContratosDetallesInsumos2>();
			List<DFile> OTDiagramas = new List<DFile>();
			List<DFile> OTDetallesInstalacion = new List<DFile>();
			IEmpresas empresa = null;
			bool bShowDiagramas=true;
			bool bShowDetallesInstalacion = true;

			if(response != null)
			{
				ot = CBase._ToJson<COrdenesTrabajo>(response.Content);
				if(ot != null)
				{
					ordenes.Add(ot);
					if(ot.OTPersonal != null && ot.OTPersonal.Count > 0)
					{
						ot.OTPersonal.ForEach(x => tecnicos.Add(x));
					}
					if(ot.OTConceptos != null && ot.OTConceptos.Count > 0)
					{
						ot.OTConceptos.ForEach(x => allConceptos.Add(x));
					}
					empresa = GetEmpresaById(ot.IdEmpresa);
					if(empresa != null)
					{
						allEmpresas.Add(empresa);
					}
				}
				OTDiagramas = NOrdenesTrabajo.ToListDFile(ot.OTDiagramas, this.GetDocumentoById, this.DownloadDocumento);
				OTDetallesInstalacion = NOrdenesTrabajo.ToListDFile(ot.OTDetalleInstalacion, this.GetDocumentoById, this.DownloadDocumento);
				allInsumos = NOrdenesTrabajo.AgrupaInsumos(ot).ToList();
				if(OTDiagramas == null || OTDiagramas.Count <= 0) bShowDiagramas = false;
				if(OTDetallesInstalacion == null || OTDetallesInstalacion.Count <= 0) bShowDetallesInstalacion = false;
			}
			var dsOT = new ReportDataSource
			{
				Name = "OT",
				Value = ordenes
			};
			var dsTecnicos = new ReportDataSource
			{
				Name = "Tecnicos",
				Value = tecnicos
			};
			var dsEmpresa = new ReportDataSource
			{
				Name = "Empresa",
				Value = allEmpresas
			};
			var dsConceptos = new ReportDataSource
			{
				Name = "Conceptos",
				Value = allConceptos
			};
			var dsInsumos = new ReportDataSource
			{
				Name = "Insumos",
				Value = allInsumos
			};
			var dsDiagramas = new ReportDataSource
			{
				Name = "Diagramas",
				Value = OTDiagramas
			};
			var dsDetallesInstalacion = new ReportDataSource
			{
				Name = "DetallesInstalacion",
				Value = OTDetallesInstalacion
			};
			string personal = "";
			if(ot != null && ot.OTPersonal != null)
			{
				ot.OTPersonal.ForEach(x =>
				{
					personal += "-" + x.NombreEmpleado + Environment.NewLine; 
				});
			}
			ReportParameter[] parametros = new ReportParameter[7]; 
			parametros[0] = new ReportParameter("PConcepto",Parametros.Concepto.ToString(), false);
			parametros[1] = new ReportParameter("PPartida", Parametros.Partida.ToString(), false);
			parametros[2] = new ReportParameter("PInsumos", Parametros.Insumos.ToString(), false);
			parametros[3] = new ReportParameter("PAnalisis", Parametros.Analisis.ToString(), false);
			parametros[4] = new ReportParameter("PPersonal", personal, false);
			parametros[5] = new ReportParameter("PDiagramas", bShowDiagramas.ToString(), false);
			parametros[6] = new ReportParameter("PDetallesInstalacion",bShowDetallesInstalacion.ToString(), false);	
		
			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "OTGlobal.rdlc");
			Reporte.LocalReport.EnableExternalImages = true;
			Reporte.ProcessingMode = ProcessingMode.Local;
			Reporte.LocalReport.SetParameters(parametros);
			Reporte.LocalReport.DataSources.Add(dsOT);
			Reporte.LocalReport.DataSources.Add(dsTecnicos);
			Reporte.LocalReport.DataSources.Add(dsEmpresa);
			Reporte.LocalReport.DataSources.Add(dsConceptos);
			Reporte.LocalReport.DataSources.Add(dsInsumos);
			Reporte.LocalReport.DataSources.Add(dsDiagramas);
			Reporte.LocalReport.DataSources.Add(dsDetallesInstalacion);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Logos"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Logos2"));
			Reporte.LocalReport.Refresh();
			return base.ReporteToByteArray(Reporte);
		}///GeneraReporte ends.	
	}//RptGlobalOT ends.
}//namespace ends.
