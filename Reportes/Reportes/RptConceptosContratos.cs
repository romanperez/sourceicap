﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
    public class RptConceptosContratos : ReporteBase
	{
		public byte[] GeneraReporte(ETipoReporte TipoReporte, 
			                         IEnumerable<IContratosDetalles2> ConceptosContratos, string nombreProyecto, int tipo)
		{
			if(ConceptosContratos == null || ConceptosContratos.Count() <= 0) return null;
		
			ReportViewer Reporte = new ReportViewer();			
			
			var reportDataSource2 = new ReportDataSource
			{
				Name = "ConceptosContratos",
                Value = ConceptosContratos
			};
			List<ReportParameter> parametros = new List<ReportParameter>();
            parametros.Add(new ReportParameter("NombreProyecto", nombreProyecto));

            if (tipo == 1)
            Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ConceptosContratos.rdlc");
            if (tipo == 2)
                Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ConceptosContratosSolicitadoEstado.rdlc");
            if (tipo == 3)
                Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ConceptosSolicitadoArea.rdlc");
            if (tipo == 4)
                Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ConceptosInstaladoArea.rdlc");
            if (tipo == 5)
                Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ConceptosSolicitadoEstadoArea.rdlc");

			Reporte.LocalReport.SetParameters(parametros);
            Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(TipoReporte.ToString(), Reporte);
		}
	}
}
