﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptProyectosCostos : ReporteBase
	{
		public string MonedaDestino
		{
			set;
			get;
		}
		public string FechaTipoCambio
		{
			set;
			get;
		}
		public byte[] GeneraReporte(ETipoReporte TipoReporte, 
			                         IEnumerable<IProyectos> Proyectos,
			                         IEnumerable<ITipoCambios>tipoCambios,
											 MProyectosCostos Modelo)
		{
			if(Proyectos == null || Proyectos.Count() <=0) return null;
			string colConversionMoneda = Proyectos.FirstOrDefault().MonedaConversion;
			ReportViewer Reporte = new ReportViewer();			
			var reportDataSource = new ReportDataSource
			{
				Name = "Proyectos",
				Value = Proyectos
			};
			var reportDataSource2 = new ReportDataSource
			{
				Name = "TipoCambios",
				Value = tipoCambios
			};
			List<ReportParameter> parametros = new List<ReportParameter>();
			parametros.Add(new ReportParameter("colConversionMoneda", colConversionMoneda, false));
			parametros.Add(new ReportParameter("MonedaDestino", MonedaDestino, false));
			parametros.Add(new ReportParameter("FechaTipoCambio", FechaTipoCambio, false));


			if(Modelo.ColumnasHide != null && Modelo.ColumnasHide.Count > 0)
			{
				Modelo.ColumnasHide.ForEach(c =>
				{
					parametros.Add(new ReportParameter(c, "False", false));
				});
			}

			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "ProyectosCostos.rdlc");
			Reporte.LocalReport.SetParameters(parametros);
			Reporte.LocalReport.DataSources.Add(reportDataSource);
			Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(TipoReporte.ToString(), Reporte);
		}
	
	}
}
