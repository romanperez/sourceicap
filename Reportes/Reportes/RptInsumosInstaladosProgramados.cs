﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptInsumosInstaladosProgramados : ReporteBase
	{
		public byte[] GeneraReporte(ETipoReporte TipoReporte,
											IEnumerable<IContratosDetallesInsumos2> ConceptosContratos, string nombreProyecto,int tipo)
		{
			if(ConceptosContratos == null || ConceptosContratos.Count() <= 0) return null;
		
			ReportViewer Reporte = new ReportViewer();			
			
			var reportDataSource2 = new ReportDataSource
			{
				Name = "Insumos",
             Value = ConceptosContratos
			};
			List<ReportParameter> parametros = new List<ReportParameter>();
            parametros.Add(new ReportParameter("NombreProyecto", nombreProyecto));

            if (tipo == 1)
					Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "InsumosInstaladosProgramados.rdlc");
            if (tipo == 2)
					Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "InsumosInstaladosProgramadosFecha.rdlc");           

			Reporte.LocalReport.SetParameters(parametros);
         Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(TipoReporte.ToString(), Reporte);
		}
	}
}
