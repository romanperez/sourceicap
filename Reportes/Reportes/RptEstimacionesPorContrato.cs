﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Negocio;
using Microsoft.Reporting.WebForms;
namespace Reportes
{
	public class RptEstimacionesPorContrato : ReporteBase
	{
        public byte[] GeneraReporteEstimacionesCantidades(ETipoReporte TipoReporte,
                                                          IContratos2 parametrosContrato,
                                                          IEnumerable<IEstimaciones> Estimaciones)
        {
            ReportViewer Reporte = new ReportViewer();

            var reportDataSource2 = new ReportDataSource
            {
                Name = "Estimaciones",
                Value = Estimaciones
            };
            int NumeroEstimacion= Estimaciones.Max(x=>x.NumeroEstimacion);
            IEnumerable<IEstimaciones> ultimas = Estimaciones.Where(x => x.NumeroEstimacion == NumeroEstimacion);
            IEstimaciones estimacion =Estimaciones.Where(x=>x.NumeroEstimacion == NumeroEstimacion).FirstOrDefault();
            List<ReportParameter> parametros = new List<ReportParameter>();            
            parametros.Add(new ReportParameter("TotalImporteEstimar",ultimas.Sum(x=>x.ImportePorEstimar).ToString("n4")));
            parametros.Add(new ReportParameter("TotalImporteContratado", ultimas.Sum(x=>x.ImporteContratado).ToString("n4")));
            parametros.Add(new ReportParameter("TituloReporteA", "Infraestructura y Comunicaciones IT"));
            parametros.Add(new ReportParameter("ResponsableEmpresa",String.Format("{0} {1} {2}",
                                               parametrosContrato.Usuario.NombreEmpleado,
                                               Environment.NewLine, 
                                               parametrosContrato.Empresa)));
            parametros.Add(new ReportParameter("ResponsableCliente",String.Format("{0} {1} {2}",
                                                parametrosContrato.ContactoCliente,
                                                Environment.NewLine,
                                                parametrosContrato.RazonSocial)));
            parametros.Add(new ReportParameter("Proyecto",parametrosContrato.Proyecto.Proyecto));
            parametros.Add(new ReportParameter("Periodo",String.Format("{0} al {1}",
                                                          estimacion.FechaInicio.ToString("d"),estimacion.FechaFin.ToString("d"))));
            parametros.Add(new ReportParameter("OrdenCompra",parametrosContrato.Contrato));
            parametros.Add(new ReportParameter("Fecha",DateTime.Now.ToString("d")));
            parametros.Add(new ReportParameter("Estimacion",estimacion.NumeroEstimacion.ToString()));
            parametros.Add(new ReportParameter("DocumentoB", "Resumen volumetría estimaciones No.:"));
            parametros.Add(new ReportParameter("Documento", "RESUMEN VOLUMETRIA ESTIMACIONES"));
            parametros.Add(new ReportParameter("ClienteTituloC",parametrosContrato.ContactoCliente));
            parametros.Add(new ReportParameter("ClienteTituloB",""));
            parametros.Add(new ReportParameter("ClienteTituloA", parametrosContrato.RazonSocial));
            Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "EstimacionesCantidades.rdlc");
            Reporte.LocalReport.SetParameters(parametros);
            Reporte.LocalReport.DataSources.Add(reportDataSource2);
            Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
            //Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
            return ReporteToByteArray(TipoReporte.ToString(), Reporte);
        }
        public byte[] GeneraReporteEstimacionesResumenEconomico(ETipoReporte TipoReporte,
                                                                IContratos2 parametrosContrato,
                                                                IEnumerable<CEstimacionesResumenEconomico> Estimaciones)
        {
            ReportViewer Reporte = new ReportViewer();

            var reportDataSource2 = new ReportDataSource
            {
                Name = "Estimaciones",
                Value = Estimaciones
            };
            int NumeroEstimacion = Estimaciones.Max(x => x.NumeroEstimacion);
            IEnumerable<IEstimaciones> ultimas = Estimaciones.Where(x => x.NumeroEstimacion == NumeroEstimacion);
            IEstimacionesResumenEconomico estimacion = Estimaciones.Where(x => x.NumeroEstimacion == NumeroEstimacion).FirstOrDefault();
            List<ReportParameter> parametros = new List<ReportParameter>();
            parametros.Add(new ReportParameter("TotalImporteEstimar", ultimas.Sum(x => x.ImportePorEstimar).ToString("n4")));
            parametros.Add(new ReportParameter("TotalImporteContratado", ultimas.Sum(x => x.ImporteContratado).ToString("n4")));
            parametros.Add(new ReportParameter("TituloReporteA", "Infraestructura y Comunicaciones IT"));
            parametros.Add(new ReportParameter("ResponsableEmpresa", String.Format("{0} {1} {2}",
                                               parametrosContrato.Usuario.NombreEmpleado,
                                               Environment.NewLine,
                                               parametrosContrato.Empresa)));
            parametros.Add(new ReportParameter("ResponsableCliente", String.Format("{0} {1} {2}",
                                                parametrosContrato.ContactoCliente,
                                                Environment.NewLine,
                                                parametrosContrato.RazonSocial)));
            parametros.Add(new ReportParameter("Proyecto", parametrosContrato.Proyecto.Proyecto));
            parametros.Add(new ReportParameter("Periodo", String.Format("{0} al {1}",
                                                          estimacion.FechaInicio2.Value.ToString("d"), estimacion.FechaFin2.Value.ToString("d"))));
            parametros.Add(new ReportParameter("OrdenCompra", parametrosContrato.Contrato));
            parametros.Add(new ReportParameter("Fecha", DateTime.Now.ToString("d")));
            parametros.Add(new ReportParameter("Estimacion", estimacion.NumeroEstimacion.ToString()));
            parametros.Add(new ReportParameter("DocumentoB", "Resumen económico estimaciones No.:"));
            parametros.Add(new ReportParameter("Documento", "RESUMEN ECONOMICO ESTIMACIONES"));
            parametros.Add(new ReportParameter("ClienteTituloC", parametrosContrato.ContactoCliente));
            parametros.Add(new ReportParameter("ClienteTituloB", ""));
            parametros.Add(new ReportParameter("ClienteTituloA", parametrosContrato.RazonSocial));
            Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "EstimacionesResumenEconomico.rdlc");
            Reporte.LocalReport.SetParameters(parametros);
            Reporte.LocalReport.DataSources.Add(reportDataSource2);
            Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));            
            return ReporteToByteArray(TipoReporte.ToString(), Reporte);
        }
        public byte[] GeneraReporte(ETipoReporte TipoReporte,
											IEnumerable<IContratosDetalles2> Estimaciones,
											string nombreProyecto)
		{
			if(Estimaciones == null || Estimaciones.Count() <= 0) return null;
			ReportViewer Reporte = new ReportViewer();
			var reportDataSource2 = new ReportDataSource
			{
				Name = "Estimaciones",
				Value = Estimaciones
			};
			List<ReportParameter> parametros = new List<ReportParameter>();
			parametros.Add(new ReportParameter("NombreProyecto", nombreProyecto));
			Reporte.LocalReport.ReportPath = System.IO.Path.Combine(PATH_BASE, "EstimacionesPorContrato.rdlc");
			Reporte.LocalReport.SetParameters(parametros);
			Reporte.LocalReport.DataSources.Add(reportDataSource2);
			Reporte.LocalReport.DataSources.Add(base.dsLogoLasec("Lasec"));
			Reporte.LocalReport.DataSources.Add(base.dsLogoBecker("Becker"));
			return ReporteToByteArray(TipoReporte.ToString(), Reporte);
		}
	}
}