﻿function Acceso()
{
    this.Usuario = document.getElementById('Usuario');
    this.Clave = document.getElementById('Clave');
    this.Idiomas = document.getElementById('opAllIdiomas');
    this.FormLoggin = document.getElementById('frmLoggin');
    this.Inicializa();
}
Acceso.prototype.Inicializa=function()
{
    var Loggin = this;
    $('input[type="text"]').addClass("form-control");
    $('input[type="password"]').addClass("form-control");
    $(Loggin.Usuario).val('');
    $(Loggin.Clave).val('');
    $(Loggin.Idiomas).change(function () {
        var id = parseInt($(this).val(), 10);
        var url = $(this).attr('data-url-idioma')
        if (id > 0) {
            $(Loggin.FormLoggin).attr('action', url + '/' + id);          
            $(Loggin.FormLoggin).submit();
        }
    });    
    $('#dvBackGround').height($(window).height());
    this.Usuario.focus();
}