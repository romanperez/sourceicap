﻿ClassIndexIcapMain.prototype.MenusInsumos = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var tab = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    tab.MenuInsumos = new MenuInsumos();   
   // tab.AfterEditar = tab.MenuInsumos.EsServicio;
    tab.AfterNuevo = tab.MenuInsumos.NoEsServicio
    tab.EsServicio = tab.MenuInsumos.EsServicio;
    tab.Inicializa();       
}
function MenuInsumos()
{
}
MenuInsumos.prototype.NoEsServicio = function ()
{
    var catalogo = this;
    try
    {
        catalogo.dvUnidadMedida = document.getElementById('dvSectionUnidadMedidaInsumo');
        catalogo.dvCosto = document.getElementById('dvSectionCostoInsumo');
        $(catalogo.dvCosto).hide();
        catalogo.EsServicio();
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
MenuInsumos.prototype.EsServicio = function ()
{
    var catalogo = this;
    try
    {
        var service = document.getElementById('EsServicio');
        var IdUnidad = document.getElementById('IdUnidad');
        var IdUnidadAutoComplete = document.getElementById('IdUnidadAutoComplete');
        var costo = document.getElementById('Costo');
        $(service).change(function ()
        {
            $(IdUnidad).val('');
            $(IdUnidadAutoComplete).val('');
            $(costo).val('');
            if ($(this).prop('checked'))
            {
                $(catalogo.dvUnidadMedida).hide();
                $(catalogo.dvCosto).show();
               
            }
            else
            {
                $(catalogo.dvCosto).hide();
                $(catalogo.dvUnidadMedida).show();                
            }
        });
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}