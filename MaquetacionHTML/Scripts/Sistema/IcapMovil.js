﻿function ICAPM()
{

}
ICAPM.prototype.Loggin = function ()
{
    var icap = this;
    $('#btnLoggin').click(function () { icap.GetOrdenes($(this)); });
}
ICAPM.prototype.fnSimulaSincronizacion = function (btn)
{
    var text = $('#lblTargetMnu1').html();
    $('#lblTargetMnu1').html('Por favor espere estoy descargando sus ordenes de trabajo.');
    $(btn).attr('class', 'fa fa-circle-o-notch fa-spin fa-2x fa-fw');
    var url = $('#lblTargetMnu1').attr('data-url');
    $('#liDwnOt').removeClass('list-mnu');
    $('#liDwnOt').addClass('list-mnu-wait');
    $.ajax(
     {
         url: url,
         type: 'GET',
         success: function (html) {
             $('#lblTargetMnu1').html(html);
             $(btn).attr('class', 'fa fa-globe fa-2x mnuOp');
             $('#liDwnOt').addClass('list-mnu');
             $('#liDwnOt').removeClass('list-mnu-wait');
         }
     });
}
ICAPM.prototype.fnSalir = function ()
{
    window.location = 'Index/Index';
}
ICAPM.prototype.Back = function ()
{
    $('#dvbacktomenu').click(function ()
    {
        $('#dvMnuPrincipal').show();
        $('#dvMnuPrincipalResult').html('');
    });
    $('#btnLoggin').click(function ()
    {
        $('#infosave').html(' <div class="alert alert-success" role="alert">La informacion se guardo correctamente.</div>');
    });
}
ICAPM.prototype.Menu=function()
{
    var icap = this;
    $('#allMenu').find('i.mnuOp').each(function ()
    {
        $(this).click(function ()
        {
            var url = $(this).attr('data-url');
            var fn = $(this).attr('data-fn');
            if (url == '') {
                icap[fn]($(this));
            }
            else
            {
                $('#dvMnuPrincipal').hide();
                $('#dvMnuPrincipalResult').html('Por favor espere estoy cargando la informacion');
                $.ajax(
                {
                    url: url,
                    type: 'GET',
                    success: function (html)
                    {                       
                        $('#dvMnuPrincipalResult').html(html);
                        icap.FuncionalidadToolbar();
                        icap.Back();
                    }
                });
            }
        });
    });
}
ICAPM.prototype.GetOrdenes = function (btn)
{
    var icap = this;
    var url = $(btn).attr('data-url');    
    $.ajax(
       {
           url: url ,
           type: 'GET',          
           success: function (html)
           {              
               $('#dvAllContenido').html(html);               
               icap.Menu();
           }
       });

}
ICAPM.prototype.FuncionalidadToolbar = function ()
{
    var icap = this;
    var btnAllInsumos = document.getElementById('btnAllInsumos');
    var btnAllActividades = document.getElementById('btnAllActividades');
    $(btnAllInsumos).click(function ()
    {
        icap.OcultaTablaAct(icap);
    });
    $(btnAllActividades).click(function () {
        icap.OcultaTablaIns(icap);
    });
    icap.LoadModal();
    icap.Rechazar();
    icap.InsumosByOT();
    icap.SaveAvance();
    icap.VerDocumentos();
}
ICAPM.prototype.SaveAvance = function ()
{
    var main = this;
    $('#btnSaveOne').click(function ()
    {
        main.ShowModal('Avance', main.MensajeConfirmaAcance() + main.GetFooter(), '', null, true, null, null);
    });
}
ICAPM.prototype.OcultaTablaAct = function (main)
{
    var ins = document.getElementById('dvAllInsumos');
    var act = document.getElementById('dvAllOts');
    $(ins).show();
    $(act).hide();
}
ICAPM.prototype.OcultaTablaIns = function (main) {
    var ins = document.getElementById('dvAllInsumos');
    var act = document.getElementById('dvAllOts');
    $(ins).hide();
    $(act).show();
   
}
ICAPM.prototype.ShowDocumentos = function ()
{
    window.open("/Procedimientos/Analisis.pdf", "_blank");
}
ICAPM.prototype.InsumosByOT = function () {
    var main = this;
    $('#tablaOrdenesTrabajovwAdministrador').find('td.dvdetail').each(function () {
        $(this).click(function ()
        {
            var id = $(this).attr('data-target-div');
            if ($('#' + id).is(":visible")) {
                $('#' + id).hide();
            }
            else {
                $('#' + id).show();
            }
        });
    });
}
ICAPM.prototype.VerDocumentos = function ()
{
    var main = this;
    $('#tablaOrdenesTrabajovwAdministrador').find('td.dvDownLoadFile').each(function () {
        $(this).click(function ()
        {
            main.ShowDocumentos();
        });
    });
}
ICAPM.prototype.Rechazar = function ()
{
    var main = this;
    $('#tablaOrdenesTrabajovwAdministrador').find('td.dvRejectOT').each(function ()
    {
        $(this).click(function ()
        {
            var tr=$(this).closest('tr');
            main.ShowModal('Rechazar', main.ConfirmaRechazar() + main.GetFooter(), '', null, true, null, null);
            $('#btnRechazaOrden').click(function ()
            {
                $(tr).remove();
                main.CloseModal();
            });
        });
    });
}
ICAPM.prototype.MensajeConfirmaAcance = function () {
    return '<div class="form-group col-md-12" style="color:#000!important;"><label>El avance se actualizo correctamente.</label> ' +
        '</div>';
}
ICAPM.prototype.ConfirmaRechazar = function ()
{
    return '<div class="form-group col-md-12" style="color:#000!important;"><label>Indique el motivo de rechazo</label> ' +
        '<input type="text" class="form-control"/></div>' + 
        '<div class="form-group col-md-12"> ' +
	 '<span id="btnRechazaOrden" class="btn btn-primary btn-right">Rechazar</span></div>';
}
ICAPM.prototype.GetFooter = function () {
    return ' <div class="modal-footer"></div>';
}
ICAPM.prototype.LoadModal = function ()
{
    var icap = this;
    var url = $('#idModalMode').attr('data-url-modal');
    if (url != null)
    {
        icap.modalElement = document.getElementById('idModalMode');
        $.ajax(
            {
                url: url,
                type: "GET",
                success: function (html)
                {
                    $(icap.modalElement).hide();
                    $(icap.modalElement).html(html);
                }
            });
    }   
}
ICAPM.prototype.ShowModal = function (Titulo, mensaje, tipo, muestraFoot, fnAccept, fnOnModalEnds)
{
    var md = new ClassIndexIcapMain();
    md.IcapModalGlobal = this.modalElement;
    md.ShowModal(Titulo, mensaje, tipo, muestraFoot, fnAccept, fnOnModalEnds);
}
ICAPM.prototype.CloseModal = function ()
{
    var md = new ClassIndexIcapMain();
    md.IcapModalGlobal = this.modalElement;
    md.CloseModal();    
}