﻿ClassIndexIcapMain.prototype.EmpresasInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.Empresa = new Empresas();
    generico.Empresa.ICAP = generico.ICAP;
    generico.Empresa.OnError = generico.OnError;
    generico.Inicializa();
    generico.fnCambiaImagen = generico.Empresa.fnCambiaImagen;
    generico.Save = generico.Empresa.Save;
}
function Empresas(catalogoGenrico)
{
    var empresa = this;
    this.catalogoGenrico = catalogoGenrico;       
}
Empresas.prototype.GetInfoFile = function (form)
{
    var empresa = this;
    try
    {        
        var formData = new FormData(form);
        return formData;
    }
    catch (error)
    {
       empresa.OnError(error);
    }
}
Empresas.prototype.fnCambiaImagen = function (btn,dv)
{
    var divParent = $(btn).closest('div');
    $(divParent).hide();
    $(dv).css("visibility", "visible");
    $(dv).show();
}
Empresas.prototype.Save = function (title, wait)
{
    var catalogo = this;
    this.btnEmpresaLogo = document.getElementById('btnEmpresaLogo');
    var dv = document.getElementById('_viewLogoEmpresa');
    if (this.btnEmpresaLogo != null && this.btnEmpresaLogo != undefined)
    {
        $(dv).hide();
        $(this.btnEmpresaLogo).click(function () { catalogo.fnCambiaImagen($(this),dv); });
    }
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {       
        var form = document.getElementById('formularioEditarNew' + catalogo.NombreCatalogo);
        if ($(form).valid())
        {
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            var data = catalogo.Empresa.GetInfoFile(form);
            if (title != null && wait != null)
            {
                //catalogo.BuildWaitModal(title, wait);
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
            }
            $.ajax(
              {
                  url: url,
                  type: method,
                  data: data,
                  contentType: false,
                  processData: false,
                  success: function (html)
                  {
                      var hayerror = catalogo.HasError(html);
                      if (hayerror) {
                          catalogo.SetInfo(html);
                      }
                      else {
                          $(catalogo.Notificaciones).html(html);
                          catalogo.ICAP.CloseModal();
                          catalogo.ReloadCurrentPage(function () {
                              catalogo.Aftersave(title, html);
                          });
                      }
                  }
              });
        }
    });
}