﻿ClassIndexIcapMain.prototype.MenuProyectos = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros) {
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.proyectos = new Proyectos(generico);
    if (parametros != null)
    {
        if (parametros.Controlador != '' && parametros.Controlador != undefined)
        {
            generico.ControladorName = parametros.Controlador;
        }
        if (parametros.NameSaveButton != '' && parametros.NameSaveButton != undefined) {
            generico.NameSaveButton = parametros.NameSaveButton;
        }
        if (parametros.SearchAction != '' && parametros.SearchAction != undefined)
        {
            generico.SearchAction = parametros.SearchAction;
        }
    }
    generico.EventNuevo = generico.proyectos.EventNuevo;
    generico.AfterEditar = generico.proyectos.changeModal;
    generico.AfterNuevo = generico.proyectos.changeModal;
    generico.gridDelete = generico.proyectos.AddBtnsDelete;
    generico.AddContrato = generico.proyectos.AddContrato;
    generico.Save = generico.proyectos.SaveProyectoContratos;
    generico.CreaContratoFromProyecto = generico.proyectos.CreaContratoFromProyecto;
    generico.EnviaContrato = generico.proyectos.EnviaContrato;
    generico.AdministraProyecto = generico.proyectos.AdministraProyecto;
    generico.Inicializa2 = generico.proyectos.Inicializa;
    generico.LoadCatalogos = generico.proyectos.LoadCatalogos;
    generico.DetallesProyecto = generico.proyectos.DetallesProyecto;
    generico.ShowDetalles = generico.proyectos.ShowDetalles;
    generico.Inicializa();
    generico.btnToolCrearProyectoContrato = document.getElementById('btnToolCrearProyectoContrato');
    generico.btnToolAdministrarProyecto = document.getElementById('btnToolAdministrarProyecto');
    generico.btnToolDetallesProyecto = document.getElementById('btnToolDetallesProyecto');
    $(generico.btnToolCrearProyectoContrato).click(function () {
        generico.CreaContratoFromProyecto($(this));
    });
    $(generico.btnToolAdministrarProyecto).click(function () {
        generico.AdministraProyecto($(this));
    });
    $(generico.btnToolDetallesProyecto).click(function ()
    {
        generico.DetallesProyecto($(this));
    });
}
function Proyectos(catalogo)
{
    this.catalogo = catalogo;   
}
Proyectos.prototype.ShowDetalles = function ()
{
    var catalogo = this;
    var btnCoti = document.getElementById('spnShowCotizacionProyecto');
    var btnPrintCoti = document.getElementById('spnPrintCotizacionProyecto');
    var dvCotizacionDetail = document.getElementById('dvCotizacionProyectoDetail');
    var dvContratoProyectoDetail = document.getElementById('dvContratoProyectoDetail');
    var tBodyAllContratosDetalleProyecto = document.getElementById('tBodyAllContratosDetalleProyecto');
    var fnShowCotizacion = function (btn)
    {
        var id = parseInt($(btn).attr('data-id'));
        var url = $(btn).attr('data-url');
        var msjWait = $(btn).attr('data-msj-wait');
        if (isNaN(id) || id <= 0) return;
        if (url == null || url == undefined || url == '') return;
        $(dvCotizacionDetail).html(catalogo.GetLoadingInfoHtml(msjWait));
        $.ajax({
            url: url + '/' + id,
            type: "get",
            success: function (html)
            {
                var coti = new Cotizaciones();
                $(dvCotizacionDetail).html(html);
                coti.fnShowHideInsumos();
            }
        });
    };
    var fnPrintCotizacion = function (btn)
    {
        var id = parseInt($(btn).attr('data-id'));
        var url = $(btn).attr('data-url');
        var msjWait = $(btn).attr('data-msj-wait');
        if (isNaN(id) || id <= 0) return;
        if (url == null || url == undefined || url == '') return;
        window.open(url + '/' + id, '_blank');

    }
    var fnPreviewContrato=function (tr)
    {
        var url = $(dvContratoProyectoDetail).attr('data-url');
        var msjWait = $(dvContratoProyectoDetail).attr('data-msj-wait');
        var contrato = catalogo.ICAP.GetObject(tr);
        if (url == null || url == undefined || url == '') return;
        if (contrato == null || contrato == undefined) return;
        $(dvContratoProyectoDetail).html(catalogo.GetLoadingInfoHtml(msjWait));
        $.ajax({
            url: url + '/' + contrato.IdContrato,
            type: "get",
            success: function (html)
            {                
                var contrato = new Contratos();
                $(dvContratoProyectoDetail).html(html);
                contrato.fnShowHideInsumos();
            }
        });
    }
    $(tBodyAllContratosDetalleProyecto).find('i.dvPreview').each(function ()
    {
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');
            fnPreviewContrato(tr);
        });
    });
    $(btnCoti).click(function ()
    {
        fnShowCotizacion($(this));
    });
    $(btnPrintCoti).click(function () {
        fnPrintCotizacion($(this));
    });    
}
Proyectos.prototype.DetallesProyecto = function (btn)
{
    var catalogo = this;
    var url = $(btn).attr('data-url-proyecto');
    var none = $(btn).attr('data-msj-none');
    var wait = $(btn).attr('data-msj-wait');
    var obj = catalogo.GetObject();   
    if (obj == null)
    {
        catalogo.ICAP.ShowModal($(btn).html() + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    if(url ==null || url=='' || url ==undefined)
    {
        return;
    }
    catalogo.BuildWaitModal($(btn).html(), wait);    
    $.ajax({
            url: url + '/' + obj.Id,
            type: "get",                 
            success: function (html)
            {
                var hayerror = catalogo.HasError(html);
                catalogo.ICAP.ReplaceBodyModal(html);
                if (!hayerror) 
                {
                    catalogo.ICAP.LargeModal(true);
                    catalogo.ShowDetalles();
                }                
            }
        });
}
Proyectos.prototype.AddBtnsDelete = function (tbl)
{
    var catalogo = this;
    if (tbl.MovsDelete == null) {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function () {
            var tr = $(this).closest('tr');
            var obj = catalogo.ICAP.GetObject(tr);
            if (parseInt(obj.IdProyectoContrato, 10) > 0) {
                tbl.MovsDelete.push(parseInt(obj.IdProyectoContrato, 10));
            }
            $(tr).remove();
        });
    });
}
Proyectos.prototype.Inicializa = function ()
{
    var catalogo = this;
    try
    {
        catalogo.btnAddContrato2 = document.getElementById('btnAddContrato2');
        catalogo.tblContratos = document.getElementById('tBodyAllContratos');
        catalogo.dvNotificaciones = document.getElementById('dvNotificacionesValidacionPProyectos');
        catalogo.IdEstatus = document.getElementById('IdEstatus');
        $(catalogo.btnAddContrato2).click(function () { catalogo.AddContrato($(this)); });
        $(catalogo.btnAddContrato2).tooltip();
        if (catalogo.tblContratos != null)
        {
            catalogo.gridDelete(catalogo.tblContratos);
        }
        catalogo.LoadCatalogos();
    }
    catch (eIni) {alert(eIni);}
}
Proyectos.prototype.changeModal = function ()
{
    var catalogo = this;
    //catalogo.ICAP.LargeModal(true);
    catalogo.Inicializa2();
}
Proyectos.prototype.AdministraProyecto = function (btn)
{
    var catalogo = this;
    try
    {
        var div = catalogo.ICAP.GetDivContenidoActivo();
        var dvParentAll = $(div).parent();
        var dvAux = null;
        var url = $(btn).attr('data-url-submenu');
        var loaded = $(btn).attr('data-loaded');
        var msjWait = $(btn).attr('data-msj-wait');
        var none = $(btn).attr('data-msj-none');
        var obj = catalogo.GetObject();
        if (obj == null) {
            catalogo.ICAP.ShowModal($(btn).html() + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
            return
        }
        dvAux = $(dvParentAll).find('div.dvAuxAdministraProyecto')[0];
        if (dvAux == null || dvAux == undefined) {
            dvAux = document.createElement('div');
            $(dvAux).addClass('dvAuxAdministraProyecto');
            $(dvParentAll).append(dvAux);            
        }
        else
        {
            $(dvAux).show();            
        }
        $(div).hide();
        $(dvAux).html(catalogo.GetInfoHtml(msjWait));        
        $.ajax(
             {
                 url: url + '/' + obj.Id,
                 type: "get",                 
                 success: function (html)
                 {
                     $(dvAux).html(html);
                     var hayerror = catalogo.HasError(html);
                     if (!hayerror)
                     {
                         var adm = new AdministraProyectos(catalogo);
                     }
                     
                 }
             });       
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Proyectos.prototype.SaveProyecto = function (title, wait, btn) {
    var catalogo = this.catalogo;
    try {
        var form = document.getElementById('formularioEditarNewProyectos');
        if ($(form).valid()) {
            var url = $(btn).attr("data-url-save");
            var data = $(form).serialize();
            $('#dvNotificacionesValidacionPProyectos').html(catalogo.GetLoadingInfoHtml(wait));
            $.ajax(
              {
                  url: url,
                  type: "Post",
                  data: data,
                  contentType: "application/x-www-form-urlencoded",
                  success: function (html)
                  {
                      var hayerror = catalogo.HasError(html);
                      if (hayerror)
                      {
                          $('#dvNotificacionesValidacionPProyectos').html(html);
                      }
                      else
                      {
                          catalogo.ICAP.CloseModal();
                          catalogo.ReloadCurrentPage(function ()
                          {
                              catalogo.Aftersave(title, html);
                          });
                      }
                  }
              });
        }
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Proyectos.prototype.SaveProyectoContratos = function (title, wait)
{
    var catalogo = this;
    try
    {           
        var name = '#btnSave' + catalogo.NombreCatalogo;
        if (catalogo.NameSaveButton != null && catalogo.NameSaveButton != '' && catalogo.NameSaveButton != undefined)
        {
            name = '#' + catalogo.NameSaveButton
        }

        $(name).click(function ()
        {
            var form = document.getElementById('formularioEditarNewProyectos');
            if ($(form).valid()) {
                //var url = $($(this)).attr("data-url-save");

                var url = $(form).attr("action");
                var method = $(form).attr("method");

                var ObjProyecto = new Object();
               
                ObjProyecto.IdProyecto = $('#IdProyecto').val();
                ObjProyecto.IdCotizacion = $('#IdCotizacion').val();
                ObjProyecto.Proyecto = $('#Proyecto').val();
                ObjProyecto.Descripcion = $('#Descripcion').val();
                ObjProyecto.Ubicacion = $('#Ubicacion').val();
                ObjProyecto.FechaInicial = $('#FechaInicial').val();
                ObjProyecto.FechaFinal = $('#FechaFinal').val();
                ObjProyecto.IdEstatus = parseInt($('#IdEstatus').val(), 10);
                ObjProyecto.IdCliente = parseInt($('#IdCliente').val(), 10);
                if (catalogo.tblContratos != null)
                {
                    ObjProyecto.Contratos = new Array();
                    $(catalogo.tblContratos).find('tr').each(function () {
                        var obj = catalogo.ICAP.GetObject($(this));
                        ObjProyecto.Contratos.push(obj);
                    });
                    if (catalogo.tblContratos.MovsDelete != null) {
                        $(catalogo.tblContratos.MovsDelete).each(function () {
                            var obj = new Object();
                            obj.IdProyectoContrato = this * -1;
                            ObjProyecto.Contratos.push(obj);
                        });
                    }
                }
                if (title != null && wait != null)
                {
                    // catalogo.BuildWaitModal(title, wait);
                    catalogo.SetInfo(catalogo.GetInfoHtml(wait));                   
                }
                $.ajax(
                  {
                      url: url,
                      type: method,
                      data: JSON.stringify(ObjProyecto),
                      contentType: "application/json",
                      success: function (html)
                      {
                          var hayerror = catalogo.HasError(html);
                          if (hayerror) {
                              catalogo.SetInfo(html);
                          }
                          else {
                              catalogo.ICAP.CloseModal();
                              catalogo.ReloadCurrentPage(function () {
                                  catalogo.Aftersave(title, html);
                              });
                          }
                      }
                     
                  });
            }
        });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}

Proyectos.prototype.EventNuevo = function (btn)
{
    var catalogo = this;
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).html();
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    $.ajax(
      {
          url: catalogo.ControladorName + '/New/0',
          type: 'GET',
          success: function (html) {
              catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
              catalogo.OnOffCheck();
              catalogo.SetUIBoostrap();
              $(catalogo.Notificaciones).html('');
              catalogo.ICAP.BuildAutoCompletes();
              catalogo.Save(title, wait);
              if (catalogo.AfterNuevo != null && catalogo.AfterNuevo != undefined) {
                  catalogo.AfterNuevo();
              }
          }
          , error: function (response) {
              catalogo.ICAP.AddToBodyModal(response.responseText, true);
          }
      });
}

Proyectos.prototype.AddContrato = function (btn)
{
    var catalogo = this;
    try
    {       
        var url = $(btn).attr('data-url-plantilla');
        var msjFalta = $(btn).attr("data-falta-contrato");
        var msjExiste = $(btn).attr("data-contrato-exists");
        var id = document.getElementById('hddnIdContratoAux');
        var contratoText = document.getElementById('IdContratos2AutoComplete');
        var existe = false;
        if (id == null || id == undefined || id.ResultSelectionAutoComplete == null || id.ResultSelectionAutoComplete == undefined) {
            catalogo.ShowErrorInModal(catalogo.dvNotificaciones, msjFalta);
            return;
        }      
        $(catalogo.tblContratos).find('tr.rwProyectoContrato').each(function () 
        {
            var obj = catalogo.ICAP.GetObject($(this));
            if (obj.IdContrato == id.ResultSelectionAutoComplete.IdContrato)
            {
                existe = true;
            }
        });        
        if (existe)
        {
            catalogo.ShowErrorInModal(catalogo.dvNotificaciones, msjExiste);
            return;
        }
        $.ajax(
        {
            url: url,
            type: 'POST',
            data: JSON.stringify(id.ResultSelectionAutoComplete),
            contentType: "application/json",
            success: function (html) {
                $(catalogo.tblContratos).append(html);
                $(id).val('');
                $(contratoText).val('');
                catalogo.gridDelete(catalogo.tblContratos);
            }
            , error: function (response) {
                catalogo.ICAP.AddToBodyModal(response.responseText, true);
            }
        });
    }
    catch (eIni) { catalogo.OnError(eIni); }
}

Proyectos.prototype.CreaContratoFromProyecto = function (btn) {
    var catalogo = this;
    try
    {
        var obj = catalogo.GetObject();        
        var url = $(btn).attr('data-url-proyecto');
        var wait = $(btn).attr('data-msj-wait');
        catalogo.BuildWaitModal(catalogo.TituloCatalogo, wait);
        $.ajax(
               {
                   url: url + '/' + obj.Id,
                   type: 'get',
                   success: function (html)
                   {
                       catalogo.ICAP.ShowModal(catalogo.TituloCatalogo, html, null, false, null, null);
                       catalogo.OnOffCheck();
                       catalogo.SetUIBoostrap();
                       $(catalogo.Notificaciones).html('');
                       catalogo.ICAP.BuildAutoCompletes();
                       $('#btnGeneraContrato').click(function () { catalogo.EnviaContrato($(this), wait); })
                   }, error: function (response) {                       
                       catalogo.ICAP.ReplaceBodyModal(response.responseText, true);
                   }

               });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Proyectos.prototype.EnviaContrato = function (btn, wait)
{
    var catalogo = this;
    var obj = catalogo.GetObject();    
    var url = $(btn).attr('data-url-save');
    var nombreContrato = document.getElementById('Contrato');
    var contrato = new Object();
    contrato.Proyecto = new Object();
    contrato.Proyecto.IdProyecto = obj.Id;
    contrato.Contrato = $(nombreContrato).val();
    contrato.IdEstatus = parseInt($(catalogo.IdEstatus).val(), 10);
    catalogo.BuildWaitModal(catalogo.TituloCatalogo, wait);
    $.ajax(
           {
               url: url ,
               type: 'POST',
               data: JSON.stringify(contrato),
               contentType: "application/json",
               success: function (html)
               {                           
                   catalogo.ICAP.CloseModal();
                   catalogo.ReloadCurrentPage(function () {
                       catalogo.Aftersave('', html);
                   });
               }, error: function (response) {
                   catalogo.ICAP.ReplaceBodyModal(response.responseText, true);
               }
           });
}
Proyectos.prototype.LoadCatalogos = function () {
    var catalogo = this;
    catalogo.IdEstatus = document.getElementById('IdEstatus');
    var url1 = $(catalogo.IdEstatus).attr('data-url-estatus');  
    var msjWait1 = $(catalogo.IdEstatus).attr('data-msj-wait');    
    var EstatusIni = parseInt($(catalogo.IdEstatus).attr('data-idEstatusOt'), 10);   
    $(catalogo.IdEstatus).html('<option selected="selected" value="0">' + msjWait1 + '</option>');
    $.ajax(
    {
        url: url1,
        type: 'get',
        success: function (html) {
            $(catalogo.IdEstatus).html(html);
            $(catalogo.IdEstatus).find('option').each(function () {
                if (parseInt($(this).val(), 10) == EstatusIni) {
                    $(this).prop('selected', true);
                }
            });
        }
        , error: function (response) {
            catalogo.ICAP.AddToBodyModal(response.responseText, true);
        }
    });  
}