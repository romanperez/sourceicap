﻿ClassIndexIcapMain.prototype.ConceptosInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.Conceptos = new Conceptos();
    generico.Inicializa2 = generico.Conceptos.IniciaConceptos;
    generico.AddInsumo = generico.Conceptos.AddInsumo;
    generico.EventNuevo = generico.Conceptos.NuevoConcepto;
    generico.Save = generico.Conceptos.SaveConcepto;
    generico.EventEditar = generico.Conceptos.EventEditar;
    generico.Inicializa();
   
}
function Conceptos()
{
}
Conceptos.prototype.ShowErrorInModal = function (elemento, error)
{
    $(elemento).html('<div  class="alert alert-danger col-md-12" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" >&times;</span></button><spa> ' + error + '</span></div>');
    $(elemento).show();
}
Conceptos.prototype.EventEditar = function (btn)
{
    var catalogo = this;
    var obj = catalogo.GetObject();
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");
    var title = $(btn).html();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    $.ajax(
     {
         url: catalogo.ControladorName + '/Editar/' + obj.Id,
         type: 'GET',
         contentType: 'application/json',
         success: function (html)
         {
             var hayerror = catalogo.HasError(html);
             if (hayerror) {
                 catalogo.SetInfo(html);
             }
             else {
                 catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                 catalogo.ICAP.LargeModal(true);
                 catalogo.SetUIBoostrap();
                 $(catalogo.Notificaciones).html('');
                 catalogo.ICAP.BuildAutoCompletes();
                 catalogo.Save(title, wait);
                 catalogo.Inicializa2();
             }
         }       
     });
}
Conceptos.prototype.AddBtnsDelete = function (tbl)
{
    if (tbl.MovsDelete == null)
    {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function () {
            var tr = $(this).closest('tr');
            var HtmlId = $(tr).find('td[data-property="IdConceptoInsumo"]');
            var id = $(HtmlId).attr("data-property-value");
            if (parseInt(id, 10) > 0) {
                tbl.MovsDelete.push(parseInt(id, 10));
            }
            $(tr).remove();
        });
    });
}
Conceptos.prototype.NuevoConcepto = function (btn)
{
    var catalogo = this;
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).html();
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    $.ajax(
      {
          url: catalogo.ControladorName + '/New',
          type: 'GET',
          success: function (html)
          {
              var hayerror = catalogo.HasError(html);
              if (hayerror) {
                  catalogo.SetInfo(html);
              }
              else {
                  catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                  catalogo.ICAP.LargeModal(true);
                  catalogo.SetUIBoostrap();
                  $(catalogo.Notificaciones).html('');
                  catalogo.ICAP.BuildAutoCompletes();
                  catalogo.Save(title, wait);
                  catalogo.Inicializa2();
              }
          }
          ,
          error: function (response) {
              catalogo.ICAP.ReplaceBodyModal(response.responseText, true);
          }
      });
}
Conceptos.prototype.AddInsumo = function (btn)
{
    var catalogo = this;
    try
    {
        
        var msjFaltaInsumo = $(btn).attr('data-falta-insumo');
        var id = document.getElementById('IdInsumoAuxConcepto');
        var Descripcion = document.getElementById('IdInsumoAutoComplete');
        if (id == null || id == undefined || id.ResultSelectionAutoComplete == null || id.ResultSelectionAutoComplete== undefined)
        {
            catalogo.Conceptos.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjFaltaInsumo);
            return;
        }
        var url = $(btn).attr('data-url-plantilla');
        $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(id.ResultSelectionAutoComplete),
             contentType: "application/json",
             success: function (html)
             {
                 var hayerror = catalogo.HasError(html);                 
                 $(catalogo.TablaDetalles).append(html);
                 $(id).val('');
                 $(Descripcion).val('');
                 if (!hayerror) {
                     catalogo.Conceptos.AddBtnsDelete(catalogo.TablaDetalles);
                 }
             }
         });
    }
    catch (ee) {
        catalogo.OnError(ee);
    }
}
Conceptos.prototype.IniciaConceptos = function()
{
    var catalogo = this;
    try
    {
        catalogo.btnAddInsumo = document.getElementById('btnAddDetalleConceptos');
        catalogo.dvNotificacionesConceptos = document.getElementById('dvNotificacionesValidacionConceptos');
        catalogo.TablaDetalles = document.getElementById('tbdodyConceptosInsumos');
        $(catalogo.btnAddInsumo).click(function () { catalogo.AddInsumo($(this)); });
        catalogo.Conceptos.AddBtnsDelete(catalogo.TablaDetalles);
        //catalogo.Precio = document.getElementById('Precio');
        //$(catalogo.Precio).attr('type', 'number');
        //$(catalogo.Precio).removeClass('form-control');
        //$(catalogo.Precio).addClass('my-form-control TxtCantidades SimboloMoneda');

        $(catalogo.btnAddInsumo).tooltip();
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
}

Conceptos.prototype.SaveConcepto = function (title, wait)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {        
        catalogo.IdConcepto = document.getElementById('IdConcepto');
        catalogo.Concepto = document.getElementById('Concepto');
        catalogo.Descripcion = document.getElementById('Descripcion');
        catalogo.Estatus = document.getElementById('Estatus');
        catalogo.IdCapitulo = document.getElementById('IdCapitulo');
        catalogo.IdUnidadMedida = document.getElementById('IdUnidadMedida');
        catalogo.IdClasificacion = document.getElementById('IdConceptoClasificacion')
        var msjCapituloNecesario = $(this).attr('data-capitulo-vacio');
        var msjCantidadesMayoresZero = $(this).attr('data-cant-zero');
        var msjInsumosRepetidos = $(this).attr('data-insumo-repetido');
        var msjMovimientoVacio = $(this).attr('data-movimiento-vacio');       

        var isValido = true;
        var Mov = new Movimientos();
        var form = document.getElementById('formularioEditarNewConceptos');
        if ($(form).valid())
        {
            var InsumoDetalles = new Array();
            $(catalogo.TablaDetalles).find('tr.ConcetptosInsumos').each(function ()
            {
                var obj = Mov.GetObject($(this));
                var existe = false;
                if (parseFloat(obj.Cantidad, 10) <= 0 || parseFloat(obj.CostoInsumo,10) <=0) {
                    catalogo.Conceptos.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjCantidadesMayoresZero);
                    isValido = false;
                }
                if (InsumoDetalles.length > 0)
                {
                    $(InsumoDetalles).each(function ()
                    {
                        if (this.IdInsumo == obj.IdInsumo)
                        {
                            catalogo.Conceptos.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjInsumosRepetidos);
                            isValido = false;
                            existe = true;
                            //alert($(this).html());
                            // $(this).addClass("trRepetido");
                        }
                    });
                }
                if (!existe)
                {
                    InsumoDetalles.push(obj);
                }
                else
                {
                   $(this).addClass("trRepetido");
                }
            });
            if (parseFloat($(catalogo.IdCapitulo).val(), 10) <= 0)
            {
                catalogo.Conceptos.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjCapituloNecesario);
                isValido = false;
            }
            /*if (parseFloat($(catalogo.Precio).val(), 10) <= 0)
            {
                catalogo.Conceptos.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjCantidadesMayoresZero);
                isValido = false;
            }*/
            if (InsumoDetalles.length <= 0) {
                catalogo.Conceptos.ShowErrorInModal(catalogo.dvNotificacionesConceptos, msjMovimientoVacio);
                isValido = false;
            }
            $(catalogo.TablaDetalles.MovsDelete).each(function () {
                var movNegativo = new Object();
                movNegativo.IdConceptoInsumo = this * -1;
                InsumoDetalles.push(movNegativo);
            });
            var newMov = new Object();
            newMov.IdConcepto = $(catalogo.IdConcepto).val();
            newMov.Concepto = $(catalogo.Concepto).val();
            newMov.Descripcion = $(catalogo.Descripcion).val();
            newMov.Estatus = $(catalogo.Estatus).prop('checked');            
            //newMov.Precio = parseFloat($(catalogo.Precio).val(), 10);
            newMov.IdCapitulo = parseFloat($(catalogo.IdCapitulo).val(), 10);
            newMov.IdUnidadMedida = parseFloat($(catalogo.IdUnidadMedida).val(), 10);
            newMov.IdConceptoClasificacion = parseFloat($(catalogo.IdClasificacion).val(), 10);
            newMov.Insumos = InsumoDetalles;
            var url = $(form).attr("action");
            var method = $(form).attr("method");          
            if (isValido)
            {               
                //catalogo.InfoInModal(catalogo.dvNotificacionesConceptos,wait);
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                $.ajax(
                 {
                     url: url,
                     type: method,
                     data: JSON.stringify(newMov),
                     contentType: "application/json",
                     success: function (html)
                     {
                         var hayerror = catalogo.HasError(html);
                         if (hayerror) {
                             catalogo.SetInfo(html);
                         }
                         else {
                             $(catalogo.Notificaciones).html(html);
                             catalogo.ICAP.CloseModal();
                             catalogo.ReloadCurrentPage(function () {
                                 catalogo.Aftersave(title, html);
                             });
                         }
                     }
                 });
            }
        }
    });
}
