﻿function AdministraProyectos(catalogo)
{    
    if (catalogo != null) {
        this.Inicializa(catalogo);
    }
}

AdministraProyectos.prototype.MovimientosPedidos = function ()
{
    var catalogo = this.catalogo;
    var tblPedidos = document.getElementById('tblPedidosByProyecto');
    var tblMovimientos = document.getElementById('tblMovimientosByProyecto');
    var btnSearchInsumoInPM = document.getElementById('btnSearchInsumoInPM');
    var btnConfirmaRecepcion = document.getElementById('btnConfirmaRecepcion');
    var dvNotificaciones = document.getElementById('dvNotificacionesMovimientosPedidos');
    var fnTablaFunctional = function (tbl)
    {
        $(tbl).find('td.tdDetailContenido').each(function () { $(this).hide(); });
        $(tbl).find('td.dvdetail').each(function () {
            $(this).click(function () {
                var div = document.getElementById($(this).attr('data-target-div'));
                if (div != null) {
                    if ($(div).is(":visible")) {
                        $(div).hide();
                        $(this).find('>div>i').removeClass("fa-minus");
                        $(this).find('>div>i').addClass("fa-plus");
                    }
                    else {
                        $(div).show();
                        $(this).find('>div>i').removeClass("fa-plus");
                        $(this).find('>div>i').addClass("fa-minus");
                    }
                }
            });
        });
    };
    var fnSearchInsumo = function (btn)
    {
        var msjPedido = $(btn).attr('data-pedido-resultt');
        var msjMovimiento = $(btn).attr('data-movimiento-result');
        var selInsumo = document.getElementById('IdInsumoBuscarP_M');
        var resPedido= fnSearchInTable(selInsumo.ResultSelectionAutoComplete, tblPedidos);
        var resMovimiento = fnSearchInTable(selInsumo.ResultSelectionAutoComplete, tblMovimientos);

        var resultAll = resPedido.Find +' ' + msjPedido + ' ' + resPedido.Cantidad + ' ' + resPedido.UM + '<br/>';
        resultAll += resMovimiento.Find + ' ' + msjMovimiento + ' ' + resMovimiento.Cantidad + ' ' + resMovimiento.UM;
        $('#resultSearch').show();
        $('#resultSearch').html(resultAll);
    };
    var fnSearchInTable = function (Insumo,tbl)
    {
        var result = new Object();
        result.Find = 0;
        result.Cantidad = 0;
        result.UM = '';
        $(tbl).find('tr.rwDetalleInsumo').each(function ()
        {
            var insumo = catalogo.ICAP.GetObject($(this));
            if (insumo.IdInsumo == Insumo.IdInsumo) {
                $(this).addClass('trActivo');
                result.Find++;
                result.Cantidad += parseInt(insumo.Cantidad, 10);
                result.UM = insumo.UnidadMedidaDescripcion;
            }
            else {
                $(this).removeClass('trActivo');
            }
        });
        $(tbl).find('td.tdDetailContenido').each(function () {
            $(this).find('>div>i').removeClass("fa-plus");
            $(this).find('>div>i').addClass("fa-minus");
            $(this).show();
        });
        return result;
    }
    var fnConfirmaRecepcion=function(btn)
    {
        var pedido = new Pedidos();
        pedido.catalogo = catalogo;
        pedido.Notificaciones = dvNotificaciones;
        pedido.ConfirmaRecepcion(btn, tblPedidos);
    };
    $('#resultSearch').hide();
    catalogo.ICAP.LargeModal(true);
    catalogo.ICAP.BuildAutoCompletes();
    fnTablaFunctional(tblPedidos);
    fnTablaFunctional(tblMovimientos);
    $(btnSearchInsumoInPM).click(function () { fnSearchInsumo($(this)); });
    $(btnConfirmaRecepcion).click(function () { fnConfirmaRecepcion($(this)); })
}
AdministraProyectos.prototype.AfterInicializaCatalogo = function (catalogo)
{
    catalogo.Grid = document.getElementById('tablaContratosvwAdministrador');
    catalogo.GridClick(catalogo);    
}
AdministraProyectos.prototype.PreviewCotizacion=function(btn)
{       
    var coti = new Cotizaciones();
    var catalogoaux = this.catalogo;
    catalogoaux.Preview = coti.PreviewCotizacion;
    catalogoaux.fnShowHideInsumos = coti.fnShowHideInsumos;
    catalogoaux.GetObject = function () {
        var obj = new Object();
        obj.Id = parseInt($(btn).attr('data-cotizacion'), 10);
        if (obj.Id <= 0 || isNaN(obj.Id)) {
            obj.Id = null;
        }
        return obj;
    };
    catalogoaux.Preview(btn);
}
AdministraProyectos.prototype.PrintCotizacion = function (btn)
{        
    var coti = new Cotizaciones();
    var catalogoaux = this.catalogo;
    catalogoaux.ImprimirCotizacion = coti.ImprimirCotizacion;    
    catalogoaux.GetObject = function () {
            var obj = new Object();
            obj.Id = parseInt($(btn).attr('data-cotizacion'), 10);
            if (obj.Id <= 0 || isNaN(obj.Id)) {
                obj.Id = null;
            }
            return obj;
        };    
    catalogoaux.ImprimirCotizacion(btn);
}
AdministraProyectos.prototype.ReloadContratos=function()
{
    var adm = this;
    var catalogo = adm.catalogo;    
    adm.LoadAllContratos(adm.btnAllContratos);   
}

AdministraProyectos.prototype.MainContratos = function ()
{       var adm = this;
    var catalogo = adm.catalogo;        var contrato = new ClassIndexIcapMain();
    adm.tablaContratosvwAdministrador = document.getElementById('tablaContratosvwAdministrador');
    adm.btnToolActualizarContratos = document.getElementById('btnToolActualizarContratos');
    
    if (adm.btnToolActualizarContratos != null)
    {
        $(adm.btnToolActualizarContratos).click(function ()
        {
            adm.LoadAllContratos(adm.btnAllContratos);
        });
    }
    var mnu = catalogo.GetMenuByNombre('Contratos');
    contrato.MenuContratos(catalogo.ICAP,
                           $(mnu).attr('data-name-mnu'),
                           $(mnu).attr('data-name-mnu'),
                           null,
                           $(mnu).attr('data-url-mnu'),
                           $(mnu).attr('data-id-menu'),
                           {
                               EventNuevo: adm.EventNuevoContrato,
                               Proyecto: adm.Proyecto,
                               postFijo: 'vwAdministrador',
                               AfterDelete: function () { adm.ReloadContratos(); },
                               AfterExit: function () { adm.ReloadContratos(); }
                           });
}
AdministraProyectos.prototype.AfterDeleteOT = function (title, html)
{
    var adm = this;
    var catalogo = adm.catalogo;
    var hayerror = catalogo.HasError(html);

    if (!hayerror) {
        adm.LoadAllOrdenesTrabajo(adm.btnShowAllOT);
    }
    else {
        $(adm.dvAdmProyectoBody).html(html);
    }
}

AdministraProyectos.prototype.MainOrdenesTrabajo = function ()
{
    var adm = this;
    var catalogo = adm.catalogo;    var OTS = new ClassIndexIcapMain();        
    var mnu = catalogo.GetMenuByNombre('OrdenesTrabajo');
    adm.btnToolActualizarOT =document.getElementById('btnToolActualizarOT');
    OTS.MenuOT(catalogo.ICAP,
                           $(mnu).attr('data-name-mnu'),
                           $(mnu).attr('data-name-mnu'),
                           null,
                           $(mnu).attr('data-url-mnu'),
                           $(mnu).attr('data-id-menu'),
                           {
                              // EventNuevo: adm.EventNuevoContrato,
                              // Proyecto: adm.Proyecto,
                               postFijo: 'vwAdministrador',
                               AfterDelete: function (title, html)
                               {
                                   adm.AfterDeleteOT(title, html);
                               },
                               AfterExit: function (title, html)
                               {
                                   adm.AfterDeleteOT(title, html);
                               }
                           });
    $(adm.btnToolActualizarOT).click(function () { adm.LoadAllOrdenesTrabajo(adm.btnShowAllOT); });
}
AdministraProyectos.prototype.EventNuevoContrato = function (btn)
{
    var catalogo = this;
    var wait = $(btn).attr("data-msj-wait");
    var title = $(btn).html();
    var url = catalogo.ControladorName + '/NewProyecto';
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);   
  
    $.ajax({
        url: url,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(catalogo.Proyecto),
        success: function (html)
        {
            var hayerror = catalogo.HasError(html);
            catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
            $(catalogo.Notificaciones).html('');
            if (!hayerror) {
                catalogo.OnOffCheck();
                catalogo.SetUIBoostrap();
                catalogo.ICAP.BuildAutoCompletes();
                catalogo.Save(title, wait);
                if (catalogo.AfterNuevo != null && catalogo.AfterNuevo != undefined) {
                    catalogo.AfterNuevo();
                }
            }
        }
    });
}
AdministraProyectos.prototype.LoadAllContratos = function (btn)
{
    var adm = this;
    var catalogo=adm.catalogo;
    var msjWait = $(adm.dvAdmProyectoBody).attr('data-msj-wait');
    var msjNone =$(adm.dvAdmProyectoBody).attr('data-msj-none');
    var url = $(btn).attr('data-url');   
    var Id = parseInt($(adm.IdProyecto).val(), 10);
    if(Id>0)
    {
        $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
        $.ajax(
             {
                 url: url + '/' + Id,
                 type: 'get',           
                 success: function (html)
                 {                     
                     var hayerror = catalogo.HasError(html);
                     $(adm.dvAdmProyectoBody).html(html);
                     if (!hayerror)
                     {
                         adm.MainContratos();
                     }                 
                 }
             });
    }
    else
    {
        catalogo.ShowErrorInModal(adm.dvAdmProyectoBody,msjNone)
    }
}
AdministraProyectos.prototype.LoadAllOrdenesTrabajo = function (btn)
{    
    var adm = this;
    var catalogo = adm.catalogo;
    var msjWait = $(adm.dvAdmProyectoBody).attr('data-msj-wait');
    var msjNone = $(adm.dvAdmProyectoBody).attr('data-msj-none');
    var url = $(btn).attr('data-url');
    var Id = parseInt($(adm.IdProyecto).val(), 10);
    if (Id > 0) {
        $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
        $.ajax(
             {
                 url: url + '/' + Id,
                 type: 'get',
                 success: function (html) {
                     var hayerror = catalogo.HasError(html);
                     $(adm.dvAdmProyectoBody).html(html);
                     if (!hayerror) {
                         adm.MainOrdenesTrabajo();
                     }
                 }
             });
    }
    else {
        catalogo.ShowErrorInModal(adm.dvAdmProyectoBody, msjNone)
    }
}
AdministraProyectos.prototype.SetCotizacion = function (IdProyecto, msjWait)
{
    var adm = this;
    var catalogo = adm.catalogo;
    var notificaciones = document.getElementById('dvNotificacionesAsignaCotizacion');
    var btnSaveAsignaCotizacionProyecto = document.getElementById('btnSaveAsignaCotizacionProyecto');
    var fnSetCotizacion = function (btn)
    {
        var form = document.getElementById('formularioEditarNewProyectos');
        var url = $(btn).attr('data-url-save');
        $(notificaciones).html(catalogo.GetInfoHtml(msjWait));
        var data = $(form).serialize();
        $.ajax(
             {
                 url: url,
                 type: 'POST',
                 data: data,
                 contentType: "application/x-www-form-urlencoded",
                 success: function (html)
                 {
                     var hayerror = catalogo.HasError(html);
                     $(notificaciones).html(html);
                     if (!hayerror)                     
                     {
                         catalogo.ICAP.CloseModal();                        
                     }                    
                 }
             });
    }
    $(btnSaveAsignaCotizacionProyecto).click(function ()
    {
        fnSetCotizacion($(this))
    });
}
AdministraProyectos.prototype.AsignarCotizacion = function (btn)
{
    var adm = this;
    var catalogo = adm.catalogo;
    var msjNone
    var url = $(btn).attr('data-url-plantilla');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    var msjWait = $(btn).attr('data-msj-wait');
    if (isNaN(id) || id <= 0) return;
    catalogo.BuildWaitModal($(btn).html(), msjWait);
    $.ajax(
           {
               url: url + '/' + id,
               type: 'get',
               success: function (html)
               {
                   var hayerror = catalogo.HasError(html);                  
                   catalogo.ReplaceBodyModal(html);
                   if (!hayerror)
                   {
                       catalogo.SetUIBoostrap();
                       catalogo.ICAP.BuildAutoCompletes();
                       adm.SetCotizacion(id, msjWait);
                   }
               }
           });
    
}
AdministraProyectos.prototype.ShowNewOTPlantilla = function (btn)
{
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    if (url == null || url == undefined || url == '') return;
    var ot = new Object();
    var contrato = new Object();
    
    ot.IdProyecto = $('#IdProyecto').val();
    contrato.IdContrato = $('#IdContrato').val();
    ot.Contratos = new Array();
    ot.Contratos.push(contrato);
    catalogo.BuildWaitModal($(btn).html(), msjWait);
    var fnOtFuncionalidad = function ()
    {        
        var catalogo2 = catalogo;
        var orden = new OT();        
        catalogo2 = orden.SetEventos(catalogo2, orden);
        catalogo2.NombreCatalogo = "OrdenesTrabajo";
        catalogo2.SetUIBoostrap();
        catalogo2.ICAP.BuildAutoCompletes();               
        catalogo2.Save($(btn).html(), msjWait);
        catalogo2.AfterEditar();
        catalogo2.AfterExit = function () { adm.LoadAllOrdenesTrabajo(adm.btnShowAllOT); };
    }
    $.ajax(
         {
             url: url ,
             type: 'post',
             data: JSON.stringify(ot),
             contentType: "application/json",
             success: function (html)
             {
                 var hayerror = catalogo.HasError(html);
                 catalogo.ReplaceBodyModal(html);
                 if (!hayerror)
                 {
                     fnOtFuncionalidad();
                 }
             }
         });
}
AdministraProyectos.prototype.NewOT = function ()
{
    var adm = this;
    var btnNew = document.getElementById('btnNewOT');    
    $(btnNew).click(function ()
    {
        adm.ShowNewOTPlantilla($(this));
    });
}
AdministraProyectos.prototype.InsumosByProyecto = function (btn)
{
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (id <= 0) return;
    $(adm.dvAdmProyectoBody).html(catalogo.GetLoadingInfoHtml(msjWait));
    $.ajax(
         {
             url: url + '/' + id,
             type: 'get',
             success: function (html) {
                 var hayerror = catalogo.HasError(html);
                 $(adm.dvAdmProyectoBody).html(html);
                 if (!hayerror)
                 {
                     var winInsumos = new InsumosByProyecto();
                     winInsumos.CollapseGrid(document.getElementById('tAllContratosInsumos'))
                 }
             }
         });
}
AdministraProyectos.prototype.NewOtByContrato = function (btn)
{
    var adm = this;
    catalogo = adm.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    var id = parseInt($(btn).attr('data-proyecto'), 10);
    if (isNaN(id) || id <= 0) return;
    if (url == null || url == undefined || url == '') return;
    catalogo.BuildWaitModal($(btn).html(), msjWait);
    $.ajax(
           {
               url: url + '/' + id,
               type: 'get',
               success: function (html)
               {
                   var hayerror = catalogo.HasError(html);
                   catalogo.ReplaceBodyModal(html);
                   if (!hayerror)
                   {
                       catalogo.SetUIBoostrap();
                       adm.NewOT();
                      /* catalogo.ICAP.BuildAutoCompletes();
                       adm.SetCotizacion(id, msjWait);*/
                   }
               }
           });

}
AdministraProyectos.prototype.Inicializa = function (catalogo)
{
    var adm = this;
    this.catalogo = catalogo;    
    adm.IdProyecto=document.getElementById('IdProyecto');
    adm.dvAdmProyectoBody = document.getElementById('dvAdmProyectoBody');
    adm.btnViewCotizacion = document.getElementById('btnViewCotizacion');
    adm.btnPrintCotizacion = document.getElementById('btnPrintCotizacion');
    adm.btnAsignarCotizacion = document.getElementById('btnAsignarCotizacion');
    adm.btnAllContratos = document.getElementById('btnAllContratos');
    adm.btnShowAllOT = document.getElementById('btnShowAllOT');
    adm.tblSingleProyecto = document.getElementById('tblSingleProyecto');
    adm.btnAddNewOtByContrato = document.getElementById('btnAddNewOtByContrato');
    adm.btnInsumosByProyecto = document.getElementById('btnInsumosByProyecto');
      
    if (adm.btnViewCotizacion != null)
    {
        $(adm.btnViewCotizacion).click(function ()
        {
            adm.PreviewCotizacion($(this));
        });
    }
    if (adm.btnPrintCotizacion != null) {
        $(adm.btnPrintCotizacion).click(function () {
            adm.PrintCotizacion($(this));
        });
    }
    if (adm.btnAllContratos != null)
    {
        $(adm.btnAllContratos).click(function ()
        {
            adm.LoadAllContratos($(this));
        });
    }
    if (adm.btnShowAllOT != null) {
        $(adm.btnShowAllOT).click(function () {
            adm.LoadAllOrdenesTrabajo($(this));
        });
    }
    var tr = $(adm.tblSingleProyecto).find('tr:first');
    if (tr != null && tr != undefined)
    {       
        adm.Proyecto = adm.catalogo.ICAP.GetObject($(tr));
    }
    $(adm.btnAsignarCotizacion).click(function ()
    {
        adm.AsignarCotizacion($(this));
    });
    $(adm.btnAddNewOtByContrato).click(function ()
    {
        adm.NewOtByContrato($(this));
    });
    $(adm.btnInsumosByProyecto).click(function ()
    {
        adm.InsumosByProyecto($(this));
    });
}
