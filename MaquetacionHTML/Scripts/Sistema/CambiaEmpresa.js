﻿ClassIndexIcapMain.prototype.CambiaEmpresaInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.CambiaE = new CambiaEmpresa();
    generico.CambiaE.ICAP = generico.ICAP;
    generico.CambiaE.OnError = generico.OnError;   
}
function CambiaEmpresa()
{
    this.Inicia();
}
CambiaEmpresa.prototype.Inicia = function ()
{
    var cambio = this;
    try
    {        
        cambio.cmbEmpresas = document.getElementById('cmbEmpresas');
        cambio.cmbUnidad = document.getElementById('cmbUnidades');
        cambio.cmbDepartamento = document.getElementById('cmbDepartamentos');
        cambio.btnCambiaEmpresa = document.getElementById('btnCambiaEmpresa');
        $(cambio.cmbEmpresas).change(function () {
            cambio.CambiaEmpresa($(this).val());
        });
        /*$(cambio.cmbUnidad).change(function ()
        {
            cambio.CambiaUnidad($(this).val());
        });*/
        $(cambio.btnCambiaEmpresa).click(function () {
            cambio.EnviaCambio();
        });
    }
    catch (err)
    {
        cambio.OnError(err);
    }
}
CambiaEmpresa.prototype.EnviaCambio = function ()
{
    var cambio = this;
    try{
        var url = $(cambio.btnCambiaEmpresa).attr('data-url');
        var urlIni = $(cambio.btnCambiaEmpresa).attr('data-url-inicio');
        var msjNoValidEmpresa = $(cambio.btnCambiaEmpresa).attr('data-empresa-novalida');
        var formData = new FormData();
        var iEmpresa = $(cambio.cmbEmpresas).val();
        var iUnidad = $(cambio.cmbUnidad).val();
        var iDepartamento = $(cambio.cmbDepartamento).val();
        if (iEmpresa == null || iEmpresa == undefined ) 
            return;
        formData.append("IdEmpresa", iEmpresa);
        formData.append("IdUnidad", iUnidad);
        formData.append("IdDepartamento", iDepartamento);
        if (iEmpresa <= 0)
        {
            cambio.ICAP.ShowModal($(cambio.btnCambiaEmpresa).html(), msjNoValidEmpresa, null, false, null, null);
            return;
        }
        var msjWait = $(cambio.cmbEmpresas).attr('data-msj-wait');
        cambio.ICAP.ShowModal($(cambio.btnCambiaEmpresa).html(), msjWait, null, false, null, null);        
        $.ajax({
            url: url + '/' + 1,
            type: 'POST',
            contentType: "application/json",
            data: formData,
            contentType: false,
            processData: false,
            success: function (respuesta)
            {            
                window.location = urlIni;
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(thrownError);
            }
        });
    }
    catch (err)
    {
        cambio.OnError(err);
    }
}
CambiaEmpresa.prototype.CambiaEmpresa = function (id)
{
    var cambio = this;
    try
    {
        if (id == null || id == undefined || id <= 0) return;
        var url = $(cambio.cmbEmpresas).attr('data-url');
        var msjWait = $(cambio.cmbEmpresas).attr('data-msj-wait');
        $(cambio.cmbUnidad).html('<option selected="selected" value="0">' + msjWait + '</option>');
        $.ajax({
            url: url + '/' + id,
            type: 'GET',
            contentType: "application/json",
            success: function (respuesta)
            {
                $(cambio.cmbUnidad).html('');
                $(cambio.cmbUnidad).append(respuesta);
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(thrownError);
            }
        });
    }
    catch (err) {
        cambio.OnError(err);
    }
}
CambiaEmpresa.prototype.CambiaUnidad = function (id)
{
    var cambio = this;
    try{
        if (id == null || id == undefined || id <= 0) return;
        var url = $(cambio.cmbUnidad).attr('data-url');
        $.ajax({
            url: url + '/' + id,
            type: 'GET',
            contentType: "application/json",
            success: function (respuesta)
            {            
                $(cambio.cmbDepartamento).html('');
                $(cambio.cmbDepartamento).append(respuesta);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
    catch (err) {
        cambio.OnError(err);
    }
}