﻿ClassIndexIcapMain.prototype.PedidosInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    generico.pedidos = new Pedidos();
    generico.AfterEditar = generico.pedidos.changeModal;
    generico.AfterNuevo = generico.pedidos.changeModal;
    generico.GetDetallePedido = generico.pedidos.GetDetallePedido;
    generico.AddBtnsDelete = generico.pedidos.AddBtnsDelete;
    generico.AddInsumo = generico.pedidos.AddInsumo;
    generico.Save = generico.pedidos.SavePedido;
    generico.Iniciliza2 = generico.pedidos.Iniciliza2;
    generico.fnGridDetalle = generico.pedidos.fnGridDetalle;
    generico.AfterSearch2 = generico.pedidos.fnGridDetalle;
    generico.AfterSearch = function () { this.fnGridDetalle(this); };
    generico.Inicializa(generico.Iniciliza2);
}
function Pedidos()
{
    
}
Pedidos.prototype.GetDetallePedido = function (btn)
{
    var catalogo = this;
    try
    {
        var url = $(btn).attr('data-url-detail');        
        var tr = $(btn).closest('tr');
        var obj = catalogo.ICAP.GetObject(tr);   
        var ControlDestino = document.getElementById('trDetailPedido' + obj.IdPedido);
        if (ControlDestino == null)
        {                   
            $(btn).removeClass('btnGridDetalle');
            $(btn).addClass('btnGridSmallLoading');
            $.ajax(
              {
                  url: url + '/' + obj.IdPedido,
                  type: 'get',                 
                  contentType: "application/json",
                  success: function (html)
                  {                    
                      $(tr).after(html);
                      $(btn).removeClass('btnGridSmallLoading');
                      $(btn).addClass('btnGridDetalleHide');                     
                  }
              });
        }
        else
        {
            $(ControlDestino).remove();
            $(btn).addClass('btnGridDetalle');
            $(btn).removeClass('btnGridDetalleHide');
            ControlDestino = null;
        }
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Pedidos.prototype.Iniciliza2 = function (main)
{
    var catalogo = main;
    try
    {
        catalogo.fnGridDetalle(main);
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }
}
Pedidos.prototype.fnGridDetalle = function (main)
{
    var catalogo = main;
    try
    {
        $('#tablaPedidos').find('div.btnGridDetalle').each(function () {
            $(this).click(function () { catalogo.GetDetallePedido($(this)); });
        });
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Pedidos.prototype.AddInsumo = function (btn)
{    
    var catalogo = this;
    var url = $(btn).attr('data-url-plantilla');
    var obj = document.getElementById('IdInsumoPedidoAux');
    if (obj != null)
    {
        var insumo = obj.ResultSelectionAutoComplete;
        if (insumo != null)
        {
            $.ajax(
         {
             url: url ,
             type: 'POST',
             data: JSON.stringify(insumo),
             contentType: "application/json",
             success: function (html)
             {
                 $(obj).val('');
                 $(catalogo.IdInsumoPedidoAutoComplete).val('');
                 $(catalogo.tblInsumos).append(html);
                 catalogo.AddBtnsDelete(catalogo.tblInsumos);
             }
         });
        }
    }
}
Pedidos.prototype.AddBtnsDelete = function (tbl)
{
    var catalogo = this;
    if (tbl.MovsDelete == null) {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');
            var obj = catalogo.ICAP.GetObject(tr);
            if (parseInt(obj.IdPedidoDetalle, 10) > 0)
            {
                tbl.MovsDelete.push(parseInt(obj.IdPedidoDetalle, 10));
            }
            $(tr).remove();            
        });
    });

}
Pedidos.prototype.changeModal = function ()
{
    var catalogo = this;
    try
    {
        catalogo.btnAddDetallePedido = document.getElementById('btnAddDetallePedido');
        catalogo.tblInsumos = document.getElementById('tblInsumosByPedido');
        catalogo.IdInsumoPedidoAutoComplete = document.getElementById('IdInsumoPedidoAutoComplete');
        catalogo.dvNotificacionesValidacionPedidos = document.getElementById('dvNotificacionesValidacionPedidos');
        catalogo.FechaSolicitado = document.getElementById('FechaSolicitado');
        catalogo.FechaLlegada = document.getElementById('FechaLlegada');
        catalogo.IdPedido = document.getElementById('IdPedido');
        catalogo.IdProyecto = document.getElementById('IdProyecto');
        catalogo.IdContrato = document.getElementById('IdContrato');

        catalogo.Estatus = document.getElementById('Estatus');

        $(catalogo.btnAddDetallePedido).click(function ()
        {
            catalogo.AddInsumo($(this));
        });
        catalogo.ICAP.LargeModal(true);
        catalogo.AddBtnsDelete(catalogo.tblInsumos);
    }
    catch (eIni)
    {
        catalogo.OnError(eIni);
    }    
}
Pedidos.prototype.SavePedido = function (title, wait)
{
    var catalogo = this;
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {
        var msjVacio = $(this).attr('data-pedido-vacio');
        var msjInsumoRepetido = $(this).attr('data-insumo-duplicado');
        var msjZero = $(this).attr('data-insumo-zero');        
        
        var pedido = new Object();
        pedido.IdPedido = $(catalogo.IdPedido).val();
        pedido.FechaSolicitado = $(catalogo.FechaSolicitado).val();
        pedido.FechaLlegada = $(catalogo.FechaLlegada).val();
        pedido.IdProyecto = $(catalogo.IdProyecto).val();
        pedido.IdContrato = $(catalogo.IdContrato).val();
        //pedido.Estatus = $(Estatus).val();
        pedido.Detalle = new Array();
        var iNumPedidos = 0;
        var pedidosZero = false;
        var hayRepetidos = false;
        if (catalogo.tblInsumos == null || catalogo.tblInsumos == undefined)
        {
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionPedidos, msjVacio);
            return;
        }
        $(catalogo.tblInsumos).find('tr').each(function ()
        {
            var obj = catalogo.ICAP.GetObject($(this));
            if (obj.Cantidad <= 0)
            {
                pedidosZero = true;
            }
            if (pedido.Detalle.length > 0)
            {
                $(pedido.Detalle).each(function ()
                {
                    if (this.IdInsumo == obj.IdInsumo)
                    {
                        hayRepetidos = true;
                    }
                });
            }
            if (!hayRepetidos)
            {
                pedido.Detalle.push(obj);
                iNumPedidos++;
            }           
        });
        $(catalogo.tblInsumos.MovsDelete).each(function () {
            var obj = new Object();
            obj.IdPedidoDetalle = this * -1;
            pedido.Detalle.push(obj);
        });
        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined) {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }
        var form = document.getElementById(NombreFormulario);
        if ($(form).valid())
        {
            var valido = true;            
            if (pedido.Detalle.length <= 0)
            {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionPedidos, msjVacio);
                valido = false;
            }
            if (hayRepetidos) {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionPedidos, msjInsumoRepetido);
                valido = false;
            }
            if (pedidosZero)
            {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionPedidos, msjZero);
                valido = false;
            }
            if (iNumPedidos <= 0)
            {
                catalogo.ShowErrorInModal(catalogo.dvNotificacionesValidacionPedidos, msjVacio);
                valido = false;
            }
            if (valido)
            {
                var url = $(form).attr("action");
                var method = $(form).attr("method");
                if (title != null && wait != null)
                {
                    // catalogo.BuildWaitModal(title, wait);
                    catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                }
                $.ajax(
                  {
                      url: url,
                      type: method,
                      data: JSON.stringify(pedido),
                      contentType: "application/json",
                      success: function (html)
                      {
                          var hayerror = catalogo.HasError(html);
                          if (hayerror) {
                              catalogo.SetInfo(html);
                          }
                          else {
                              $(catalogo.Notificaciones).html(html);
                              catalogo.ICAP.CloseModal();
                              var after = new Object();
                              after.afterload = 1;
                              after.fn = function () { catalogo.fnGridDetalle(catalogo); };
                              catalogo.ReloadCurrentPage(after);
                          }
                      }
                     
                  });
            }
        }
    });
}
Pedidos.prototype.TraspasoFromPedido = function (btn)
{
    var main = this;
    var catalogo = this.Catalogo;
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url');
    if (url == '' || url == undefined) return;
    catalogo.BuildWaitModal($(btn).html(), msjWait);
    $.ajax({   url: url ,
        type: 'GET',
        contentType: "application/json",
        success: function (html)
        {
            catalogo.ReplaceBodyModal(html);
            var hayerror = catalogo.HasError(html);
            if (!hayerror)
            {
                catalogo.SetUIBoostrap();
                catalogo.ICAP.BuildAutoCompletes();
                catalogo.ICAP.LargeModal(true);
                main.CreaTraspaso(msjWait, function () { main.AfterPlantillaTraspaso(); });
            }
        }        
    });
}
Pedidos.prototype.EntradaFromPedido = function (btn) {
    var main = this;
    var catalogo = this.Catalogo;
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url');
    if (url == '' || url == undefined) return;
    catalogo.BuildWaitModal($(btn).html(), msjWait);
    $.ajax({
        url: url,
        type: 'GET',
        contentType: "application/json",
        success: function (html) {
            catalogo.ReplaceBodyModal(html);
            var hayerror = catalogo.HasError(html);
            if (!hayerror) {
                catalogo.SetUIBoostrap();
                catalogo.ICAP.BuildAutoCompletes();
                catalogo.ICAP.LargeModal(true);
                main.CreaEntrada(msjWait, function () { main.AfterPlantillaEntrada(); });
            }
        }
    });
}
Pedidos.prototype.fnDeleteRow = function (tbl)
{
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');
            $(tr).remove();
        });
    });
}
Pedidos.prototype.AfterPlantillaEntrada = function ()
{
    var adm = this;
    var catalogo = adm.Catalogo;
    adm.IdAlmacen = document.getElementById('IdAlmacen');
    adm.tblInsumos = document.getElementById('tbdodyMovimientoDetalleEntradaFromProyecto');
    adm.IdProyecto = document.getElementById('IdProyecto');
    adm.dvNotificaciones = document.getElementById('dvNotificacionesValidacion');
    adm.btnSaveEntrada = document.getElementById('btnSaveEntrada');
    adm.Fecha = document.getElementById('Fecha');
    var msjWait = $(adm.IdAlmacen).attr('data-msj-wait');
    var espere = catalogo.GetLoadingInfoHtml(msjWait);
    var AllInsumos = new Array();
    var fnMapeaExistencia = function (insumos, grid)
    {
        $(insumos).each(function ()
        {
            var insumo = this;
            var tr = $(grid).find('tr[data-insumo-id="' + insumo.IdInsumo + '"]');
            if(tr!=null)
            {
                $(tr).find('div.CantidadExistenciaAlmacen').each(function ()
                {
                    $(this).html(insumo.Cantidad);
                });
                $(tr).find('input.cantidadEnviarEntrada').each(function () {
                    $(this).val(insumo.CantidadEnviada);
                });
            }
        });
    };
   
    var fnRevisaExistencias = function (btn)
    {
        var url = $(btn).attr('data-url');
       
        var detalle = new Object();
        detalle.IdPedido = parseInt($(adm.IdProyecto).val(),10);
        detalle.IdPedidoDetalle = parseInt($(adm.IdAlmacen).val(), 10);
        $(adm.dvNotificaciones).html(catalogo.GetLoadingInfoHtml(msjWait));
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(detalle),
            success: function (response)
            {
                if (response.Error != null) {
                    catalogo.ShowErrorInModal(adm.dvNotificaciones, response.Error);
                }
                else
                {
                    $(adm.dvNotificaciones).html('');
                    fnMapeaExistencia(response, adm.tblInsumos);                    
                }
            }
        });
    };
    $(adm.IdAlmacen).on('change', function () { fnRevisaExistencias($(this)); });
    adm.fnDeleteRow(adm.tblInsumos);
    $(adm.btnSaveEntrada).click(function () { adm.SaveEntrada($(this), espere); });
    $(adm.Fecha).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
}
Pedidos.prototype.AfterPlantillaTraspaso = function ()
{    
    var adm = this;
    var catalogo = adm.Catalogo;
    adm.IdAlmacen = document.getElementById('IdAlmacen');
    adm.IdAlmacenDestino = document.getElementById('IdAlmacenDestino');
    adm.IdProyecto = document.getElementById('IdProyecto');
    adm.dvNotificacionesValidacion = document.getElementById('dvNotificacionesValidacion');
    adm.tblInsumos = document.getElementById('tbdodyMovimientoDetalleTraspasosFromProyecto');
    adm.btnSaveTraspasos = document.getElementById('btnSaveTraspasos');
    var fnRevisaExistencias = function (btn)
    {          
        $(adm.tblInsumos).find('input.cantidadtraspasar').each(function ()
        {
            $(this).val($(this).attr('max') - $(this).attr('data-enviada'));
        });
        
    };
    var msjWait = $(adm.IdAlmacen).attr('data-msj-wait');
    var espere = catalogo.GetLoadingInfoHtml(msjWait);
    adm.ErroresInOperacion = $(btnSaveTraspasos).attr('data-error-operacion');
    adm.OperacionOK = $(btnSaveTraspasos).attr('data-ok-operacion');
    $(adm.IdAlmacen).on('change', function () { fnRevisaExistencias($(this)); });
    $(adm.btnSaveTraspasos).click(function () { adm.SaveTraspaso($(this), espere); });
    adm.fnDeleteRow(adm.tblInsumos);
}
Pedidos.prototype.EvaluaResultado = function (result)
{
    var adm = this;
    var catalogo = adm.Catalogo;
    var hayErrores = false;
    $(adm.tblInsumos).find('tr').each(function ()
    {
        $(this).removeClass('trActivo');
    });
    $(result.Pedidos[0].Detalle).each(function ()
    {
        var insumo = this;
        var id = parseInt(insumo.IdPedidoDetalle, 0)
        if (id < 0)
        {
            id = id * -1;
            var tr = 'trIdPedido' + id;
            $('#' + tr).addClass('trActivo');
            hayErrores = true;
        }        
        var prefijo = $('#CantidadTraspasar' + id).attr('data-title-cantidad');
        $('#CantidadTraspasar' + id).attr('title',prefijo + ' ' + insumo.CantidadTraspasar);
        $('#CantidadTraspasar' + id).tooltip();        
    });
    if (hayErrores) {
        catalogo.ShowErrorInModal(adm.dvNotificacionesValidacion, adm.ErroresInOperacion);
    }
    else {
        catalogo.InfoInModal(adm.dvNotificacionesValidacion, adm.OperacionOK);
        catalogo.ICAP.CloseModal();
    }
}
Pedidos.prototype.SaveEntrada = function (btn, espere) {
    var adm = this;
    var catalogo = adm.Catalogo;
    var url = $(btn).attr('data-url');
    var MovEntrada = new Object();
    MovEntrada.IdAlmacen = parseInt($(adm.IdAlmacen).val(), 10);
    MovEntrada.IdUnidad = parseInt($(adm.IdAlmacen).attr('data-id'), 10);
    MovEntrada.Fecha = $(adm.Fecha).val();
    MovEntrada.DetalleMovimientos = new Array();
    var insumos = new Array();    
    $(adm.tblInsumos).find('tr').each(function () {
        var insumo = catalogo.ICAP.GetObject($(this));
        insumos.push(insumo);
    });    
    MovEntrada.DetalleMovimientos = insumos;

    if (MovEntrada.IdAlmacen <= 0)
    {
        var msj = $(btn).attr('data-no-almacen');
        catalogo.ShowErrorInModal(adm.dvNotificaciones, msj);
        return;
    }

    $(adm.dvNotificaciones).html(espere);
    $.ajax(
           {
               url: url,
               type: 'POST',
               data: JSON.stringify(MovEntrada),
               contentType: "application/json",
               success: function (html)
               {
                   var hayerror = catalogo.HasError(html);

                   if (hayerror) {
                       $(adm.dvNotificaciones).html(html);
                   }
                   else {
                       catalogo.ICAP.CloseModal();
                       catalogo.ReloadCurrentPage();
                   }
               }
           });

}
Pedidos.prototype.SaveTraspaso = function (btn, espere)
{
    var adm = this;
    var catalogo = adm.Catalogo;
    var url = $(btn).attr('data-url');    
    var Modelo = new Object();
    Modelo.Movimiento = new Object();
    Modelo.Movimiento.IdAlmacen = $(adm.IdAlmacen).val();
    Modelo.Movimiento.IdAlmacenDestino = $(adm.IdAlmacenDestino).val();
    Modelo.Pedidos = new Array();
    var insumos = new Array();
    var pedido = new Object();
    $(adm.tblInsumos).find('tr').each(function ()
    {
        var insumo = catalogo.ICAP.GetObject($(this));
        insumos.push(insumo);
    });
    pedido.IdProyecto = $(adm.IdProyecto).val();
    pedido.Detalle = insumos;
    Modelo.Pedidos.push(pedido);
    $(adm.dvNotificacionesValidacion).html(espere);      
    $.ajax(
           {
               url: url,
               type: 'POST',
               data: JSON.stringify(Modelo),
               contentType: "application/json",
               success: function (respuesta)
               {                   
                   if (respuesta.IdError != null) {
                       catalogo.ShowErrorInModal(adm.dvNotificacionesValidacion, respuesta.Error);
                   }
                   else
                   {                       
                       $(adm.dvNotificacionesValidacion).html('');
                       adm.EvaluaResultado(respuesta);
                   }
               }
           });
    
}
Pedidos.prototype.CreaTraspaso = function (msjWait,afterLoad)
{
    var btnCreaTraspaso = document.getElementById('btnCreaTraspaso');
    var IdProyecto = document.getElementById('IdProyecto');
    var adm = this;
    var catalogo = adm.Catalogo;
    var url = $(btnCreaTraspaso).attr('data-url');
    $(btnCreaTraspaso).click(function ()
    {
        if (url != null && url != '' && IdProyecto.ResultSelectionAutoComplete != null)
        {
            catalogo.BuildWaitModal($(btnCreaTraspaso).html(), msjWait);
            $.ajax(
            {
                url: url ,
                type: 'POST',
                data: JSON.stringify(IdProyecto.ResultSelectionAutoComplete),
                contentType: "application/json",
                success: function (html)
                {
                    catalogo.ReplaceBodyModal(html);
                    var hayerror = catalogo.HasError(html);
                    if (!hayerror)
                    {
                        catalogo.SetUIBoostrap();
                        catalogo.ICAP.BuildAutoCompletes();
                        catalogo.ICAP.LargeModal(true);
                        //adm.AfterPlantillaTraspaso();
                        afterLoad();
                    }
                }
            });
        }
    });
}

Pedidos.prototype.CreaEntrada = function (msjWait, afterLoad) {
    this.CreaTraspaso(msjWait, afterLoad);
}

Pedidos.prototype.ConfirmaRecepcion = function (btn,grid)
{
    var pedido = this;
    var catalogo = this.catalogo;
    var detalles = new Array();
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    $(grid).find('tr.rwDetalleInsumo').each(function ()
    {
        var obj = catalogo.ICAP.GetObject($(this));
        var recibida = parseFloat(obj.CantidadRecibida, 10);
        if (!isNaN(recibida) && recibida > 0) {
            detalles.push(obj);
        }
    });    
    $(pedido.Notificaciones).html(catalogo.GetLoadingInfoHtml(msjWait));
    console.log(detalles);
    $.ajax({
        url: url,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(detalles),
        success: function (html)
        {
            var hayerror = catalogo.HasError(html);           
            $(pedido.Notificaciones).html(html);           
        }
    });
}
