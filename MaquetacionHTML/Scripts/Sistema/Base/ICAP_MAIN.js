﻿function ClassIndexIcapMain()
{
    
}
ClassIndexIcapMain.prototype.OnError = function (e)
{
    var main=this;
    try
    {        
        main.ShowModal('Error', e, null, false, null, null);
    }
    catch (e)
    {
        alert('Error en el manejador de errores ' + e);
    }
}
ClassIndexIcapMain.prototype.Inicializa = function ()
{
    var main = this;    
    main.dvMenu = document.getElementById('mnuIcapMain');
    main.dvmnuIcon = document.getElementById('mnuIcon');
    main.dvmnuIcapContenido = document.getElementById('mnuIcapContenido');
    main.dvHeadIcap = document.getElementById('dvHeadIcap'); 
    main.dvTabulador = document.getElementById('dvTabulador');/*---------->*/
    main.dvPageParentIcap = document.getElementById('dvPageParentIcap');/*---------->*/
    main.dvmnuIcon.MuestraMenu = 1;
    main.IcapModalGlobal = document.getElementById('IcapModalGlobal');
    main.IcapModalGlobalMode = document.getElementById('IcapModalGlobalMode');
    main.btnIdRefreshAll = document.getElementById('btnIdRefreshAll');
    main.btnIdLogOut = document.getElementById('btnIdLogOut');

    $(main.btnIdRefreshAll).click(function ()
    {
        main.ReloadMenu();
    });
    $(main.btnIdLogOut).click(function ()
    {
        main.LogOut($(this));
    });
    $(main.dvmnuIcon).click(function ()
    {
        if (main.dvmnuIcon.MuestraMenu == 1)
        {            
            main.ShowMenu(main);
        }
        else
        {           
            main.HideMenu(main);
        }
    });
    $(main.dvmnuIcapContenido).height($(window).height() - $(main.dvHeadIcap).height());// - $(main.dvTabulador).height());
    $(main.dvMenu).height($(window).height() - $(main.dvHeadIcap).height());// - $(main.dvTabulador).height());   
    main.SetHeigthScreen(main, main.dvPageParentIcap);
    $(main.IcapModalGlobal).hide();
    $(main.IcapModalGlobalMode).hide();
    main.GetFunctionalityMenu();
    main.LoadModal();      
}
ClassIndexIcapMain.prototype.SetHeigthScreen = function (main,obj)
{
    $(obj).height($(main.dvmnuIcapContenido).height() - $(main.dvTabulador).height() - 5);
}
ClassIndexIcapMain.prototype.ShowMenu = function (main)
{
   // $(main.dvmnuIcapContenido).width($(document).width() - $(main.dvMenu).width());
    $(main.dvMenu).show(1000);
    main.dvmnuIcon.MuestraMenu = 0;
}
ClassIndexIcapMain.prototype.HideMenu = function (main)
{
   // $(main.dvmnuIcapContenido).width($(document).width());
    $(main.dvMenu).hide(1000);
    main.dvmnuIcon.MuestraMenu = 1;
}
ClassIndexIcapMain.prototype.GetFunctionalityMenu = function ()
{
    $(function () {
        $('li.liAux').each(function () {
            var li = this;
            var childs = $(this).attr('data-has-items');
            var imgArrow = $(this).attr('data-img-arrow');
            var mnuPadre = $(this).attr('data-id-menu-padre');
            if (childs == "True") {
                $(this).addClass('has-sub');
                $(this).find('>a').css("background-image", " url('" + imgArrow + "')");
            }
            var img = $(this).attr('data-img');
            if (img != "")
            {
                if (mnuPadre == "0") {
                    $(this).addClass('mnuimg');
                   
                }
                else
                    {
                        $(this).addClass('mnuimgSmall');                        
                }
                $(this).css("background-image", " url('" + img + "')");
                
            }
        });
        $('a[data-url-mnu!=""]').each(function () {
            var url = $(this).attr('data-url-mnu');
            var mnuName = $(this).attr('data-name-mnu');
            var mnuTitle = $(this).attr('data-title-mnu');
            var mnuParametros = $(this).attr('data-parametros-mnu');
            var mnuImgTab = $(this).attr('data-img-tab');
            var mnuId = $(this).attr('data-id-menu');            
            $(this).click(function (event) {
                if (url != "") {
                    var grl = new General();
                    var icap = grl.GetICAP();
                    if (icap != null) {icap.ShowIcapMenu(url, mnuName, mnuTitle, mnuParametros, mnuImgTab, mnuId);}
                }
            });
        });
    })
}
ClassIndexIcapMain.prototype.LogOut = function (btn)
{
    var main = this;
    try
    {
        var url1 = $(btn).attr('data-url');
        var url2 = $(btn).attr('data-url-ini');
        $.ajax({
            url: url1,
            type: 'GET',
            contentType: "application/json",            
            success: function (respuesta)
            {
                window.location = url2;
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(thrownError);
            }
        });
    }
    catch (eGrl) { main.OnError(eGrl); }
}

ClassIndexIcapMain.prototype.ReloadMenu = function ()
{   
    var main = this;
    try
    {
        var tabActive = $(main.dvTabulador).find('div.dvTavActive');
        if (tabActive.length >0)
        {
            var nameMenu = $(tabActive[0]).attr('data-name-mnu');
            var aLink = $(main.dvMenu).find("a[data-name-mnu='" + nameMenu + "']");
            main.ShowIcapMenu($(aLink[0]).attr('data-url-mnu'),
                              $(aLink[0]).attr('data-name-mnu'),
                              $(aLink[0]).attr('data-title-mnu'),
                              $(aLink[0]).attr('data-parametros-mnu'),
                              $(aLink[0]).attr('data-img-tab'),
                              $(aLink[0]).attr('data-id-menu'));
        }
    }
    catch (eGrl) { main.OnError(eGrl); }
}
ClassIndexIcapMain.prototype.GetDivContenidoActivo = function ()
{
    var main = this;
    var divContenidoActivo = null;
    $(main.dvTabulador).find('div.dvTavActive').each(function ()
    {
        var title = $(this).attr('data-name-mnu');
        divContenidoActivo = document.getElementById('dvPageContent' + title);       
    });
    return divContenidoActivo;
}
ClassIndexIcapMain.prototype.ShowIcapMenu = function (url, mnuNombre, mnuTitle,mnuParametros,imgTab,idMenu)
{
    var main = this;
    try
    {        
        var showTab = function (tab,tabTitle)
        {           
            $(main.dvTabulador).find('div.dvTab').each(function ()
            {
                $(this).removeClass('dvTavActive')
            });
            $(main.dvPageParentIcap).find('div.dvTabContenido').each(
               function ()
               {
                   $(this).hide();
                  
               });
            $(tabTitle).addClass('dvTavActive')
            $(tab).show();
        }
        var TabContentName = 'dvPageContent' + mnuNombre;
        var TabTitleName = 'dvPageTitle' + mnuNombre;
        main.HideMenu(main);
        $(main.dvmnuIcapContenido).removeClass("fondoLasecBody");
        var dvTabContent = $('#' + TabContentName);
        if (dvTabContent[0] == null)
        {
            $(main.dvPageParentIcap).append("<div id='" + TabContentName + "' class='dvTabContenido'></div>");
            dvTabContent = $('#' + TabContentName);
        }        
        if (dvTabContent == null) return;
        var dvTabTitle = $('#' + TabTitleName);
        if (dvTabTitle[0] == null) {
            //$(main.dvTabulador).append("<div id='" + TabTitleName + "'" + "data-name-mnu='" + mnuNombre + "'" + " class='dvTab col-md-1'>" + mnuTitle + "<span>&times;</span></div>");
            $(main.dvTabulador).append("<div id='" + TabTitleName + "'" + "data-name-mnu='" + mnuNombre + "'" + " class='dvTab'>" + mnuTitle + "<span>&times;</span></div>");
            dvTabTitle = $('#' + TabTitleName);
            $(dvTabTitle).find('>span').click(function () {
                //
                $(dvTabTitle).removeClass('dvTavActive');
                $(dvTabTitle).hide();
                $(dvTabContent).hide();
                window.event.cancelBubble = true;
            });            
            if (imgTab != null && imgTab != '')
            {
                $(dvTabTitle).css("background-image", " url('" + imgTab + "')");                
                //$(dvTabTitle).css("padding-left", "300px!important;");
            }
            else
            {
                $(dvTabTitle).css("padding-left", "0px!important;");
            }
        }
        else {
            $(dvTabTitle).show();
        }
        if (dvTabTitle != null) {
            $(dvTabTitle).click(function ()
            {
               // 
                showTab(dvTabContent, dvTabTitle);
            });
        }
        showTab(dvTabContent, dvTabTitle);
        $(dvTabContent).html('');
        main.SetLoading(dvTabContent);
        //$(dvTabContent).addClass("ContenidoIcap");
        //$(dvTabContent).addClass("fondoLasecBodyLoading");
        $(dvTabContent).load(url, function (responseText, textStatus, http)
        {            
            //$(dvTabContent).removeClass("fondoLasecBodyLoading");
            //$(dvTabContent).addClass("fondoLasecBodyComplete");
            main.NotLoading(dvTabContent);
            main.Menu = new Object();
            main.Menu.showIcon = true;
            main.Menu.imgTab = imgTab;
            if (mnuParametros != null && mnuParametros!= "")
            {
                var objParametros = eval('(' + mnuParametros + ')');
                if (objParametros.function != null)
                {                    
                    main[objParametros.function](main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, objParametros);
                }
            }
            else
            {                
                var tab = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url,idMenu);                
                tab.Inicializa();
            }            
            main.SetHeigthScreen(main, main.dvPageParentIcap);
            
        });   

      //  var _in = document.getElementById('srptDinamic');

       // $(main.dvmnuIcapContenido).removeClass("fondoLasecBodyLoading");
      //  $(main.dvmnuIcapContenido).addClass("fondoLasecBodyComplete");
        
        //main.SetLoading(main.dvmnuIcapContenido);
        //$(main.dvmnuIcapContenido).load(url, function (responseText, textStatus, http)
        //{
        //    $(main.dvmnuIcapContenido).removeClass("fondoLasecBodyLoading");
        //    $(main.dvmnuIcapContenido).addClass("fondoLasecBodyComplete");
        //    var tab = new Catalogos(main, mnuNombre, main.dvmnuIcapContenido);
        //    tab.Inicializa();            
        //});       
    }
    catch (eGrl) {main.OnError(eGrl);}
}


ClassIndexIcapMain.prototype.SetLoading= function (objeto,y,clear)
{   
    if (y == null || y == undefined) { $(objeto).height(500); }
    if (parseInt(y, 10) <= 0) { $(objeto).height(500); }
    if (parseInt(y, 10) > 0) { $(objeto).height(y); }
    $(objeto).addClass("fondoLasecBodyLoading");
    if (clear)
    {
        $(objeto).html('');
    }
}
ClassIndexIcapMain.prototype.NotLoading = function (objeto,auto)
{
    $(objeto).removeClass("fondoLasecBodyLoading");
    $(objeto).addClass("fondoLasecBodyComplete");
    if (auto)
    {
        $(objeto).css('height', 'auto');
    }
    //this.SetHeigthScreen(this, objeto);
}
ClassIndexIcapMain.prototype.LoadModal = function ()
{
    var main = this;
          
        var url = $(main.IcapModalGlobal).attr('data-url-modal');
        if (url != null) 
        {           
            var modalElement = main.IcapModalGlobal;
            $.ajax(
                {
                    url: url,
                    type: "GET",
                    success: function (html)
                    {                        
                        $(main.IcapModalGlobal).html(html);                                         
                    }              
            });
        }
        else { main.OnError('No fue posible encontrar la url para la ventana de notificaciones.');}
  
}
ClassIndexIcapMain.prototype.AddToBodyModal = function (mensaje,inicio)
{
    var main = this;    
    try
    {
        var modalElement = main.IcapModalGlobal;      
        var pModalBody = $(modalElement).find('[id ="pModalBodyGlobal"]');
        if (inicio)
        {
            $(pModalBody[0]).prepend(mensaje);
        }
        else { $(pModalBody[0]).append(mensaje); }
    }
    catch (e) { main.OnError(e); }
}
ClassIndexIcapMain.prototype.ReplaceBodyModal = function (mensaje)
{
    var main = this;
    try {
        var modalElement = main.IcapModalGlobal;
        var pModalBody = $(modalElement).find('[id ="pModalBodyGlobal"]');
        $(pModalBody[0]).html(mensaje);
    }
    catch (e) { main.OnError(e); }
}
ClassIndexIcapMain.prototype.CloseModal = function ()
{
    var main = this;
    var modalElement = main.IcapModalGlobal;
    var dvModal = $(modalElement).find('[id ="dvModalMessageGlobal"]');
    $(function ()
    {
        $(dvModal[0]).modal('toggle');
        main.LargeModal(false);
    });
}
ClassIndexIcapMain.prototype.GetModal = function () {
    var main = this;
    var modalElement = main.IcapModalGlobal;
    var dvModal = $(modalElement).find('[id ="dvModalMessageGlobal"]');
    return dvModal;
}
ClassIndexIcapMain.prototype.LargeModal = function (onOff)
{
    var main = this;
    var modalElement = main.IcapModalGlobal;
    var dvModal = $(modalElement).find('[id ="dvModalMessageGlobal"]').find('div[role="document"]');
    $(function ()
    {
        if (onOff)
            $(dvModal[0]).addClass('modal-lg');
        else
            $(dvModal[0]).removeClass('modal-lg');
    });
}
ClassIndexIcapMain.prototype.IconTitleBar = function (dvTitle)
{
    var main = this;    
    try
    {        
        if (main.Menu == null || main.Menu == undefined) return;
        if (main.Menu.showIcon == null || main.Menu.showIcon == undefined || main.Menu.showIcon == false) return;
        if (main.Menu.imgTab == null || main.Menu.imgTab == undefined || main.Menu.imgTab == '') return;
        
        $(dvTitle).css("background-image", " url('" + main.Menu.imgTab + "')");
        $(dvTitle).addClass("imgModalIcon");
    }
    catch (e) { main.OnError(e); }
}
ClassIndexIcapMain.prototype.ModalSize = function (x,y)
{
    var main = this;
    var modalElement = main.IcapModalGlobal;    
    var dvModal = $(modalElement).find('[id ="dvModalMessageGlobal"]');
    var dvBody = $(dvModal).find('div.modal-content');
    if (parseInt(y, 10) > 0) {$(dvBody).height(y);}
    if (parseInt(x, 10) > 0) $(dvBody).width(x);
}
ClassIndexIcapMain.prototype.ChangeBodyModal = function ( body)
{
    var main = this;    
    try
    {
        var modalElement = main.IcapModalGlobal;       
        var pModalBody = $(modalElement).find('[id ="pModalBodyGlobal"]');       
        $(pModalBody[0]).html(body);
    }
    catch (e) { main.OnError(e); }
}
ClassIndexIcapMain.prototype.GetModalBody =function()
{
    var main = this;    
    try
    {        
        var modalElement = main.IcapModalGlobal;
        var pModalBody = $(modalElement).find('[id ="pModalBodyGlobal"]');
        return pModalBody;
    }
    catch (e) { main.OnError(e); }
}

ClassIndexIcapMain.prototype.ShowModal = function (Titulo,mensaje, tipo,muestraFoot, fnAccept,fnOnModalEnds) 
{
    var main = this;    
    try
    {        
        var modalElement = main.IcapModalGlobal;
        $(modalElement).show();
        var dvModal = $(modalElement).find('[id ="dvModalMessageGlobal"]');               
        var pModalBody = $(modalElement).find('[id ="pModalBodyGlobal"]');
        var pModalFooter = $(modalElement).find('[id="pModalFooterGlobal"]');
        var title = $(modalElement).find('[id ="myModalLabel"]');
       

        $(title[0]).text(Titulo);
        if (tipo == 'error') 
        {
            $(pModalBody[0]).addClass("alert alert-danger");
        }
        $(pModalBody[0]).html(mensaje);
        if (fnAccept != null && fnAccept != undefined)
        {
            var btn = $(modalElement).find('[id ="btnModalAccept"]');
            $(btn).click(fnAccept);
        }
        if (!muestraFoot)
        {
            $(pModalFooter).hide();
        }
        $(dvModal[0]).modal({
            backdrop: 'static',
            keyboard: false
        });        
        $(dvModal[0]).draggable({
            handle: ".modal-header"
        });
        /*$(dvModal[0]).resizable({
            //alsoResize: ".modal-dialog",
            //minHeight: 150
        });*/
        
        if (fnOnModalEnds != null && fnOnModalEnds != undefined)
        {
            fnOnModalEnds();
        }
        main.LargeModal(false);
        main.IconTitleBar($(title[0]));
    }
    catch (e) { main.OnError(e); }    
}
ClassIndexIcapMain.prototype.BuildAutoCompletes = function ()
{
    var main = this;    
    try
    {
        $('input.autocompleteIcap').each(function ()
        {
            var auto = new AutoComplete($(this));            
        });
    }
    catch (e) { main.OnError(e); }
}
ClassIndexIcapMain.prototype.ShowModalMode = function (titulo,mensaje)
{
    var main = this;    
    try
    {
        var modalElement = main.IcapModalGlobalMode;
        $(modalElement).html(mensaje);
        $(modalElement).attr("title", titulo);           
        $(modalElement).dialog(
            {
            modal: true,
            create: function (event, ui)
            {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close span", widget).removeClass("ui-icon-closethick").addClass("ui-icon-DialogICAPCloseButton");
            }
            ,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    catch (e) { main.OnError(e); }
}
function OnErrorGlobalICAP(error)
{
    try
    {
        var index = new ClassIndexIcapMain();
        index.OnError(error);
    }
    catch(error2)
    {
        alert(error)
    }
}

ClassIndexIcapMain.prototype.GetAtributos = function (obj) {
    var ob = new Object();
    ob.prop = $(obj).attr('data-property');
    ob.val = $(obj).attr('data-property-value');
    return ob;
}
ClassIndexIcapMain.prototype.GetObject = function (elemento) {
    try {
        var catalogo = this;
        var tr = elemento;
        if (tr != null) {
            var entidadJSON = new Object();
            var objeto = catalogo.GetAtributos(tr);
            if (objeto != null) {
                entidadJSON[objeto.prop] = objeto.val;
            }
            $(tr).find('td').each(function () {
                var isDinamic = $(this).attr('data-object-value');
                if (isDinamic != null && isDinamic != '') {
                    var PropiedadObjeto = $(this).attr('data-property');
                    var Propiedad = $(this).attr('data-object-prop');
                    var Control = $('#' + isDinamic).val();
                    if (PropiedadObjeto != undefined) {
                        entidadJSON[PropiedadObjeto] = Control;
                    }
                }
                else {
                    var ob1 = catalogo.GetAtributos($(this));
                    if (ob1 != null) {
                        if (ob1 != undefined) {
                            entidadJSON[ob1.prop] = ob1.val;
                        }
                    }
                }
            });
            return entidadJSON;
        }
        return null;
    }
    catch (e) { OnErrorGlobalICAP(e); }
}