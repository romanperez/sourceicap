﻿ClassIndexIcapMain.prototype.MovimientosInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{   
    var generico = new Catalogos(main, parametros.Catalogo, mnuTitle, dvTabContent, url, idMenu);
    generico.ControladorName = parametros.Controlador;
    generico.btnAddInsumo = parametros.btnAddInsumo;
    generico.InputInfoInsumo = parametros.InputInfoInsumo;
    generico.InputInfoInsumoAutoComplete = parametros.InputInfoInsumoAutoComplete    
    generico.NombreFormulario = parametros.NombreFormulario;
    generico.NombreTablaDetalles = parametros.NombreTablaDetalles;
    generico.movimientos = new Movimientos(parametros.TipoMovimiento);
    generico.movimientos.ICAP = generico.ICAP;
    generico.movimientos.OnError = generico.OnError;
    generico.movimientos.BuildWaitModal = generico.BuildWaitModal;
    generico.TipoMovimiento = parametros.TipoMovimiento;
    generico.UrlPlantillaNuevo = '/NewMovimientoEntrada';
    generico.EventNuevo = generico.movimientos.EventNuevo;    
    generico.EventEditar = generico.movimientos.EventEditar;
    generico.Save = generico.movimientos.EventSave;
    generico.NuevaEntrada = generico.movimientos.NuevaEntrada;
    generico.LoadDetailsMovs = generico.movimientos.LoadDetailsMovs;
    generico.AfterPaginador = generico.movimientos.ReloadCurrentPage2;
    generico.Iniciliza2 = generico.movimientos.Inicializa2;
    generico.SearchAction = 'SearchEntradas';
    generico.Inicializa(generico.Iniciliza2);
    //generico.LoadDetailsMovs();
    var btnEntradaFromPedido = document.getElementById('btnEntradaFromPedido');
    $(btnEntradaFromPedido).click(function () {
        var entrada = new Pedidos();
        entrada.idMenu = idMenu;
        entrada.Catalogo = generico;
        entrada.EntradaFromPedido($(this));
    });
}
ClassIndexIcapMain.prototype.MovimientosInicializacionSalidas = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, parametros.Catalogo, mnuTitle, dvTabContent, url, idMenu);
    generico.ControladorName = parametros.Controlador;
    generico.btnAddInsumo = parametros.btnAddInsumo;
    generico.InputInfoInsumo = parametros.InputInfoInsumo;
    generico.InputInfoInsumoAutoComplete = parametros.InputInfoInsumoAutoComplete    
    generico.NombreFormulario = parametros.NombreFormulario;
    generico.NombreTablaDetalles = parametros.NombreTablaDetalles;
    generico.movimientos = new Movimientos(parametros.TipoMovimiento);
    generico.movimientos.ICAP = generico.ICAP;
    generico.movimientos.OnError = generico.OnError;
    generico.movimientos.BuildWaitModal = generico.BuildWaitModal;
    generico.TipoMovimiento = parametros.TipoMovimiento;
    generico.UrlPlantillaNuevo = '/NewMovimientoSalida';
    generico.EventNuevo = generico.movimientos.EventNuevo;
    generico.EventEditar = generico.movimientos.EventEditarSalida;
    generico.Save = generico.movimientos.EventSaveSalida;
    generico.NuevaEntrada = generico.movimientos.NuevaEntrada;
    generico.LoadDetailsMovs = generico.movimientos.LoadDetailsMovs;
    generico.AfterPaginador = generico.movimientos.ReloadCurrentPage2;
    generico.SearchAction = 'SearchSalidas';
    generico.Iniciliza2 = generico.movimientos.Inicializa2;
    generico.Inicializa(generico.Iniciliza2);
    //generico.LoadDetailsMovs();
}
ClassIndexIcapMain.prototype.MovimientosInicializacionTraspasos = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros) {
    var generico = new Catalogos(main, parametros.Catalogo, mnuTitle, dvTabContent, url, idMenu);
    generico.ControladorName = parametros.Controlador;
    generico.btnAddInsumo = parametros.btnAddInsumo;
    generico.InputInfoInsumo = parametros.InputInfoInsumo;
    generico.InputInfoInsumoAutoComplete = parametros.InputInfoInsumoAutoComplete
    generico.NombreFormulario = parametros.NombreFormulario;
    generico.NombreTablaDetalles = parametros.NombreTablaDetalles;
    generico.movimientos = new Movimientos(parametros.TipoMovimiento);
    generico.movimientos.ICAP = generico.ICAP;
    generico.movimientos.OnError = generico.OnError;
    generico.movimientos.BuildWaitModal = generico.BuildWaitModal;
    generico.TipoMovimiento = parametros.TipoMovimiento;
    generico.UrlPlantillaNuevo = '/NewMovimientoTraspaso/' + idMenu;
    generico.EventNuevo = generico.movimientos.EventNuevo;
    generico.EventEditar = generico.movimientos.EventEditarTraspaso;
    generico.Save = generico.movimientos.EventSaveTraspaso;
    generico.NuevaEntrada = generico.movimientos.NuevaEntrada;
    generico.SetFuncionCombos = generico.movimientos.SetFuncionCombos;
    generico.MuestraAlmacenes = generico.movimientos.MuestraAlmacenes;
    generico.LoadDetailsMovs = generico.movimientos.LoadDetailsMovs;
    generico.AfterPaginador = generico.movimientos.ReloadCurrentPage2;
    generico.SearchAction = 'SearchTraspaso';
    generico.Iniciliza2 = generico.movimientos.Inicializa2;
    generico.Inicializa(generico.Iniciliza2);
   // generico.LoadDetailsMovs();

    var btnRealizaEnvioInsumos = document.getElementById('btnRealizaEnvioInsumos');
    $(btnRealizaEnvioInsumos).click(function ()
    {
        var pedido = new Pedidos();
        pedido.idMenu = idMenu;
        pedido.Catalogo = generico;
        pedido.TraspasoFromPedido($(this));
    });
}
ClassIndexIcapMain.prototype.BuscaMovimientoInicializacion = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros)
{
    var generico = new Catalogos(main, parametros.Catalogo, mnuTitle, dvTabContent, url, idMenu);
    var buscador = new Movimientos();
    buscador.catalogo = generico;
    buscador.InicializaBuscador();
}
function Movimientos(tipo, title, mnuNombre)
{
    var movimiento = this;
    this.TipoMovimiento = tipo;
    this.TituloCatalogo = title;
    this.NombreCatalogo = mnuNombre;    
    
}
Movimientos.prototype.InicializaBuscador = function ()
{
    var buscador = this;
    var catalogo = buscador.catalogo;
    buscador.FechaIni = document.getElementById('FechaIni');
    buscador.FechaFin = document.getElementById('FechaFin');
    buscador.Tipo = document.getElementById('Tipo');
    buscador.Proyecto = document.getElementById('Proyecto');
    buscador.Contrato = document.getElementById('Contrato');
    buscador.IdUsuario = document.getElementById('IdUsuario');
    buscador.IdUnidadOrigen = document.getElementById('IdUnidadOrigen');
    buscador.IdAlmacenOrigen = document.getElementById('IdAlmacenOrigen');
    buscador.IdUnidadDestino = document.getElementById('IdUnidadDestino');
    buscador.IdAlmacenDestino = document.getElementById('IdAlmacenDestino');

    buscador.dvResultQuery = document.getElementById('dvResultQuery');
    buscador.btnBuscaMovimiento = document.getElementById('btnBuscaMovimiento');

    $(buscador.FechaIni).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $(buscador.FechaFin).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    catalogo.SetUIBoostrap();        
    catalogo.ICAP.BuildAutoCompletes();
    $(buscador.btnBuscaMovimiento).click(function () { buscador.BuscaMovimiento($(this), buscador.dvResultQuery); });
}
Movimientos.prototype.BuscaMovimiento=function(btn,target)
{
    var buscador = this;
    var catalogo = buscador.catalogo;
    var url = $(btn).attr('data-url');
    var msjWait = $(btn).attr('data-msj-wait');
    if (url == '' || url == null || url == undefined) return;
    $(target).html(catalogo.GetLoadingInfoHtml(msjWait));
    var parametros = new Object();

    parametros.FechaIni= $(buscador.FechaIni).val();
    parametros.FechaFin= $(buscador.FechaFin).val();
    parametros.Tipo= $(buscador.Tipo).val();
    parametros.Proyecto= $(buscador.Proyecto).val();
    parametros.COntrato=$(buscador.Contrato).val();
    parametros.IdUsuario= $(buscador.IdUsuario).val();
    parametros.IdUnidadOrigen= $(buscador.IdUnidadOrigen).val();
    parametros.IdAlmacenOrigen= $(buscador.IdAlmacenOrigen).val();
    parametros.IdUnidadDestino =$(buscador.IdUnidadDestino).val();
    parametros.IdAlmacenDestino = $(buscador.IdAlmacenDestino).val();

    $.ajax(
    {
        url: url,
        type: 'POST',
        data: JSON.stringify(parametros),
        contentType: "application/json",
        success: function (html)
        {
            var hayerror = catalogo.HasError(html);
            $(target).html(html);
            if (!hayerror)
            {              
                buscador.Grid = document.getElementById('tblMovimientosResultAll');
                buscador.LoadDetailsMovs();               
            }
            
        }
    });

}
Movimientos.prototype.ReloadCurrentPage2 = function ()
{
    var catalogo = this;    
    catalogo.LoadDetailsMovs();
}
Movimientos.prototype.Inicializa2 = function (main)
{
    var catalogo = main;
    try {
        catalogo.LoadDetailsMovs(main);
    }
    catch (eIni) {
        catalogo.OnError(eIni);
    }
}
Movimientos.prototype.LoadDetailsMovs = function ()
{
    var catalogo = this;
    try
    {       
        $(catalogo.Grid).find(/*'div.btnGridSmall'*/'div.btnGridSmallDetalle').each(function ()
        {
            $(this).click(function ()
            {                          
                var tr = $(this).closest('tr');
                var td = $(this).closest('td');
                var attrId = $(tr).attr('data-property-value');
                var url = $(tr).attr('data-url-movs');
                var Id = parseInt(attrId, 10);
                var idTrDetalle = $(tr).attr('idDetalles');
                if (idTrDetalle == null || idTrDetalle == '') {
                    //$(td).html('');              
                    $(td).addClass('btnGridSmallLoading');
                    $.ajax({
                        url: url + '/' + Id,
                        type: 'GET',
                        contentType: "application/json",
                        success: function (respuesta) {
                            var trDin = document.createElement('tr');
                            var sId = 'trDetail' + Id;
                            $(trDin).attr('id', sId);
                            $(trDin).html(respuesta);
                            $(tr).after(trDin);
                            $(tr).attr('idDetalles', sId);
                            $(td).removeClass('btnGridSmallLoading');
                            //alert($(td).html());
                            $(td).find('i').each(function () {
                                $(this).removeClass('fa-plus');
                                $(this).addClass('fa-minus');
                            });
                        }
                    });
                }
                else
                {
                    $(td).find('i').each(function () {
                        $(this).removeClass('fa-minus');
                        $(this).addClass('fa-plus');                        
                    });
                    $('#' + idTrDetalle).remove();
                    $(tr).attr('idDetalles','');
                }
            });
        });
    }
    catch (ee) {
        catalogo.OnError(ee);
    }
}
Movimientos.prototype.MuestraAlmacenes = function (cmb,cmbDestino)
{
    var catalogo = this;
    try
    {
        var id = $(cmb).val();
        if (id == null || id == undefined || id <= 0) return;
        var url = $(cmb).attr('data-url');
        var msjWait = $(cmb).attr('data-msj-wait');
        $(cmbDestino).html('<option selected="selected" value="0">'+msjWait+'</option>');        
        $.ajax(
        {
            url: url + '/' + id,
            type: 'GET',
            contentType: "application/json",
            success: function (respuesta)
            {
                $(cmbDestino).html('');
                $(cmbDestino).append(respuesta);
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(thrownError);
            }
        });
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
}
Movimientos.prototype.SetFuncionCombos = function ()
{
    var catalogo = this;
    try
    {
        var form = document.getElementById(catalogo.NombreFormulario);        
        var cmbUnidadOrigen = $(form).find('select[id="IdUnidadOrigen"]'); 
        var cmbAlmacenOrigen = $(form).find('select[id="IdAlmacen"]');
        var cmbUnidadDestino = $(form).find('select[id="IdUnidadDestino"]');
        var cmbAlmacenDestino = $(form).find('select[id="IdAlmacenDestino"]');
        if (cmbUnidadOrigen != null)
        {
            $(cmbUnidadOrigen).change(function ()
            {
                catalogo.MuestraAlmacenes($(this), cmbAlmacenOrigen);
            });
        }
        if (cmbUnidadDestino != null)
        {
            catalogo.AlmacenDestino = cmbAlmacenDestino;
            $(cmbUnidadDestino).change(function ()
            {
                catalogo.MuestraAlmacenes($(this), cmbAlmacenDestino);
            });
        }
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
}
Movimientos.prototype.AddBtnsDelete = function (tbl)
{
    if (tbl.MovsDelete == null)
    {
        tbl.MovsDelete = new Array();
    }
    $(tbl).find('div.btnDelete').each(function ()
    {
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');
            var HtmlId = $(tr).find('td[data-property="IdMovimientoDetalle"]');
            var id = $(HtmlId).attr("data-property-value");
            if (parseInt(id, 10) > 0)
            {
                tbl.MovsDelete.push(parseInt(id, 10));
            }
            $(tr).remove();
        });
    });
}
Movimientos.prototype.ShowErrorInModal = function (elemento,error)
{
    $(elemento).html('<div  class="alert alert-danger col-md-12" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" >&times;</span></button><spa> ' + error + '</span></div>');
    $(elemento).show();
}
Catalogos.prototype.SetFuncionalidadBotones = function ()
{
    var catalogo = this;
    var movim = new Movimientos();
    catalogo.TablaDetalles = document.getElementById(catalogo.NombreTablaDetalles);
    catalogo.btnAddDetalleEntrada = document.getElementById(catalogo.btnAddInsumo);
    movim.AddBtnsDelete(catalogo.TablaDetalles);
    $(catalogo.btnAddDetalleEntrada).tooltip();    
    $(this.btnAddDetalleEntrada).click(function ()
    {
        catalogo.AddDetalle($(this));
    });   
}

Catalogos.prototype.AddDetalle = function (btn)
{
    var catalogo = this;
    try
    {
        var msjFaltaInsumo = $(btn).attr('data-falta-insumo');
        var mov1 = new Movimientos();
        var id = document.getElementById(catalogo.InputInfoInsumo);
        var Descripcion = document.getElementById(catalogo.InputInfoInsumoAutoComplete);     
        if (id == null || id == undefined ||
            Descripcion == null || Descripcion == undefined ||
            $(id).val() == null ||$(id).val() == undefined ||
            $(Descripcion).val() == null || $(Descripcion).val() == undefined||
             $(id).val() == '' || $(Descripcion).val() == '')
        {                                    
            mov1.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjFaltaInsumo);
            return;
        }      
        var url = $(btn).attr('data-url-plantilla');
        $.ajax(
         {
             url: url,
             type: 'POST',
             data: JSON.stringify(id.ResultSelectionAutoComplete),
             contentType: "application/json",            
             success: function (html)
             {
                 var movim = new Movimientos();
                 $(catalogo.TablaDetalles).append(html);
                 $(id).val('');
                 $(Descripcion).val('');
                 movim.AddBtnsDelete(catalogo.TablaDetalles);
             }
         });
    }
    catch (ee)
    {
        catalogo.OnError(ee);
    }
    
}
Movimientos.prototype.NuevaEntrada = function ()
{                                         
    var form = document.getElementById(this.NombreFormulario);
    var fecha = $(form).find('input[id="Fecha"]');         
    this.Fecha = fecha;
    this.IdAlmacen = $(form).find('select[id="IdAlmacen"]');
    this.Tipo = $(form).find('input[id="Tipo"]');
    this.IdMovimiento = $(form).find('input[id="IdMovimiento"]');
    $(fecha).datepicker();
    this.NotificacionModalEntrada = document.getElementById('dvNotificacionesValidacion');
    $(this.NotificacionModalEntrada).hide();
}

Movimientos.prototype.EditarEntrada = function ()
{
    var form = document.getElementById(this.NombreFormulario);
    var fecha = $(form).find('input[id="Fecha"]');   
    this.Fecha = fecha;
    this.IdAlmacen = $(form).find('select[id="IdAlmacen"]');
    this.Tipo = $(form).find('input[id="Tipo"]');
    $(fecha).datepicker();
}

Movimientos.prototype.EventEditar = function (btn)
{    
    var catalogo = this;
    var obj = catalogo.GetObject();
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");
    var title = $(btn).html();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    //catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    catalogo.SetInfo(catalogo.GetInfoHtml(wait));
    $.ajax(
     {
         url: catalogo.ControladorName + '/Editar/' + obj.Id,
         type: 'GET',
         contentType: 'application/json',
         success: function (html)
         {             
             var hayerror = catalogo.HasError(html);             
             catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
             $(catalogo.Notificaciones).html('');
             if (!hayerror) {
                 catalogo.ICAP.LargeModal(true);
                 catalogo.SetUIBoostrap();
                 catalogo.NuevaEntrada();                
                 catalogo.SetFuncionalidadBotones();
                 catalogo.ICAP.BuildAutoCompletes();
                 catalogo.Save(title, wait);
             }
         }
     });
}
Movimientos.prototype.EventEditarTraspaso = function (btn)
{
    var catalogo = this;
    var obj = catalogo.GetObject();
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");
    var title = $(btn).html();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    //catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    catalogo.SetInfo(catalogo.GetInfoHtml(wait));
    var formData = new FormData();
    var idMenu = parseInt(catalogo.IdMenu, 10);
    var idMovimiento = parseInt(obj.Id, 10);
    formData.append("idMenu", idMenu);
    formData.append("idMovimiento", idMovimiento);
    $.ajax(
     {
         url: catalogo.ControladorName + '/EditarTraspaso',
         type: 'Post',         
         data: formData,
         contentType: false,
         processData: false,
         success: function (html)
         {
             catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
             $(catalogo.Notificaciones).html('');
             var hayerror = catalogo.HasError(html);
             if (!hayerror) {
                 catalogo.ICAP.LargeModal(true);
                 catalogo.SetUIBoostrap();
                 catalogo.NuevaEntrada();
                 catalogo.SetFuncionalidadBotones();
                 catalogo.ICAP.BuildAutoCompletes();
                 catalogo.Save(title, wait);
                 if (catalogo.SetFuncionCombos != null && catalogo.SetFuncionCombos != undefined) {
                     catalogo.SetFuncionCombos();
                 }
             }
         }
     });
}
Movimientos.prototype.EventEditarSalida = function (btn) {
    var catalogo = this;
    var obj = catalogo.GetObject();
    var wait = $(btn).attr("data-msj-wait");
    var none = $(btn).attr("data-msj-none");
    var title = $(btn).html();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    //catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    catalogo.SetInfo(catalogo.GetInfoHtml(wait));
    $.ajax(
     {
         url: catalogo.ControladorName + '/EditarSalida/' + obj.Id,
         type: 'GET',
         contentType: 'application/json',
         success: function (html)
         {
             var hayerror = catalogo.HasError(html);
             catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
             $(catalogo.Notificaciones).html('');
             if (!hayerror)
             {
                 catalogo.ICAP.LargeModal(true);
                 catalogo.SetUIBoostrap();
                 catalogo.NuevaEntrada();
                 catalogo.SetFuncionalidadBotones();
                 catalogo.ICAP.BuildAutoCompletes();
                 catalogo.Save(title, wait);
             }
         }
     });
}
Movimientos.prototype.EventNuevo = function (btn)
{
    var catalogo = this;
    try
    {                              
        var wait = $(btn).attr("data-msj-wait");
        var title = $(btn).html();
        //catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);       
        catalogo.SetInfo(catalogo.GetInfoHtml(wait));
        $.ajax(
          {
              url: catalogo.ControladorName + catalogo.UrlPlantillaNuevo,
              type: 'get',
              contentType: "application/json",            
              success: function (html)
              {                  
                  catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', html, null, false, null, null);
                  var hayerror = catalogo.HasError(html);
                  if (!hayerror)
                  {
                      catalogo.ICAP.LargeModal(true);
                      catalogo.SetUIBoostrap();
                      catalogo.NuevaEntrada();
                      catalogo.SetFuncionalidadBotones();
                      $(catalogo.Notificaciones).html('');
                      catalogo.ICAP.BuildAutoCompletes();
                      catalogo.Save(title, wait);
                      if (catalogo.SetFuncionCombos != null && catalogo.SetFuncionCombos != undefined) {
                          catalogo.SetFuncionCombos();
                      }
                  }
              }
          });
    }
    catch (e)
    {
        catalogo.OnError(e);
    }
}
Movimientos.prototype.EventSaveTraspaso = function (title, wait)
{
    var catalogo = this;    
    $('#btnSave' + catalogo.NombreCatalogo).click(function () {
        var msjCantidadesMayoresZero = $(this).attr('data-cant-zero');
        var msjExistenciasMayoresZero = $(this).attr('data-exist-zero');
        var msjExistenciasMenorCantidad = $(this).attr('data-exist-cant');
        var msjInsumosRepetidos = $(this).attr('data-insumo-repetido');
        var msjMovimientoVacio = $(this).attr('data-movimiento-vacio');
        var msjSeleccionaAlmacen = $(this).attr('data-no-almacen');
        var msjAlmacenEqual = $(this).attr('data-almacen-equal');
        var isValido = true;
        var Mov = new Movimientos();
        var form = document.getElementById(catalogo.NombreFormulario);
        if ($(form).valid()) {
            var InsumoDetalles = new Array();
            $(catalogo.TablaDetalles).find('tr.movimientoDetalle').each(function () {

                var obj = Mov.GetObject($(this));
                var existe = false;
                if (parseFloat(obj.Cantidad, 10) <= 0) {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjCantidadesMayoresZero);
                    isValido = false;
                }
                if (parseFloat(obj.CantidadExistenciaInsumo, 10) <= 0) {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjExistenciasMayoresZero);
                    isValido = false;
                }
                if (parseFloat(obj.CantidadExistenciaInsumo, 10) < parseFloat(obj.Cantidad, 10)) {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjExistenciasMenorCantidad);
                    isValido = false;
                }
               /* if (InsumoDetalles.length > 0) {
                    $(InsumoDetalles).each(function () {
                        if (this.IdInsumo == obj.IdInsumo) {
                            Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjInsumosRepetidos);
                            isValido = false;
                            existe = true;
                        }
                    });
                }*/
                if (!existe) {
                    InsumoDetalles.push(obj);
                }
            });
            if (InsumoDetalles.length <= 0) {
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjMovimientoVacio);
                isValido = false;
            }
            $(catalogo.TablaDetalles.MovsDelete).each(function () {
                var movNegativo = new Object();
                movNegativo.IdMovimientoDetalle = this * -1;
                InsumoDetalles.push(movNegativo);
            });
            var newMov = new Object();
            newMov.IdMovimiento = $(catalogo.IdMovimiento).val();            
            newMov.Tipo = $(catalogo.Tipo).val();
            newMov.Fecha = $(catalogo.Fecha).val();
            newMov.IdAlmacen = $(catalogo.IdAlmacen).val();
            newMov.IdAlmacenDestino = $(catalogo.AlmacenDestino).val();
            newMov.DetalleMovimientos = InsumoDetalles;
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            if (parseInt(newMov.IdAlmacen, 10) <= 0 ||
                parseInt(newMov.IdAlmacenDestino, 10) <= 0 ) {
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjSeleccionaAlmacen);
                isValido = false;
            }
            if ( parseInt(newMov.IdAlmacenDestino, 10) == parseInt(newMov.IdAlmacen, 10))
            {                
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjAlmacenEqual);
                isValido = false;
            }
            if (isValido)
            {
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                $.ajax(
                 {
                     url: url,
                     type: method,
                     data: JSON.stringify(newMov),
                     contentType: "application/json",
                     success: function (html)
                     {
                         var hayerror = catalogo.HasError(html);
                         if (hayerror) {
                             catalogo.SetInfo(html);
                         }
                         else {
                             $(catalogo.Notificaciones).html(html);
                             catalogo.ICAP.CloseModal();
                             catalogo.ReloadCurrentPage({
                                 afterload: 1, fn:
                                      function () {
                                          catalogo.Aftersave(title, html);
                                          catalogo.LoadDetailsMovs();
                                      }
                             });
                         }
                     }
                 });
            }
        }
    });
}
Movimientos.prototype.EventSaveSalida = function (title, wait)
{
    var catalogo = this;    
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {        
        var msjCantidadesMayoresZero = $(this).attr('data-cant-zero');
        var msjExistenciasMayoresZero = $(this).attr('data-exist-zero');
        var msjExistenciasMenorCantidad = $(this).attr('data-exist-cant');
        var msjInsumosRepetidos = $(this).attr('data-insumo-repetido');
        var msjMovimientoVacio = $(this).attr('data-movimiento-vacio');
        var msjSeleccionaAlmacen = $(this).attr('data-no-almacen');

        var isValido = true;
        var Mov = new Movimientos();
        var form = document.getElementById(catalogo.NombreFormulario);
        if ($(form).valid())
        {
            var InsumoDetalles = new Array();            
            $(catalogo.TablaDetalles).find('tr.movimientoDetalle').each(function ()
            {

                var obj = Mov.GetObject($(this));
                var existe = false;
                if (parseFloat(obj.Cantidad, 10) <= 0 )
                {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjCantidadesMayoresZero);
                    isValido = false;
                }
                if (parseFloat(obj.CantidadExistenciaInsumo, 10) <= 0) {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjExistenciasMayoresZero);
                    isValido = false;
                }
                if (parseFloat(obj.CantidadExistenciaInsumo, 10) < parseFloat(obj.Cantidad, 10)) {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjExistenciasMenorCantidad);
                    isValido = false;
                }
                if (InsumoDetalles.length > 0) {
                    $(InsumoDetalles).each(function () {
                        if (this.IdInsumo == obj.IdInsumo) {
                            Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjInsumosRepetidos);
                            isValido = false;
                            existe = true;
                        }
                    });
                }
                if (!existe) {
                    InsumoDetalles.push(obj);
                }
            });
            if (InsumoDetalles.length <= 0)
            {
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjMovimientoVacio);
                isValido = false;
            }
            $(catalogo.TablaDetalles.MovsDelete).each(function ()
            {
                var movNegativo = new Object();
                movNegativo.IdMovimientoDetalle = this * -1;
                InsumoDetalles.push(movNegativo);
            });
            var newMov = new Object();
            newMov.IdMovimiento = $(catalogo.IdMovimiento).val();
            newMov.Tipo = $(catalogo.Tipo).val();
            newMov.Fecha = $(catalogo.Fecha).val();
            newMov.IdAlmacen = $(catalogo.IdAlmacen).val();
            newMov.DetalleMovimientos = InsumoDetalles;
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            if (parseInt(newMov.IdAlmacen, 10) <= 0) {
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjSeleccionaAlmacen);
                isValido = false;
            }
            if (isValido)
            {
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                $.ajax(
                 {
                     url: url,
                     type: method,
                     data: JSON.stringify(newMov),
                     contentType: "application/json",
                     success: function (html)
                     {
                         var hayerror = catalogo.HasError(html);
                         if (hayerror) {
                             catalogo.SetInfo(html);
                         }
                         else {
                             $(catalogo.Notificaciones).html(html);
                             catalogo.ICAP.CloseModal();
                             catalogo.ReloadCurrentPage({ afterload:1,fn:
                                 function () {
                                     catalogo.Aftersave(title, html);
                                     catalogo.LoadDetailsMovs();
                                 }
                             });
                         }
                     }
                 });
            }
        }
    });
}

Movimientos.prototype.EventSave = function (title, wait)
{
    var catalogo = this;   
    
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {     
        var msjCantidadesMayoresZero = $(this).attr('data-cant-precios-zero');
        var msjInsumosRepetidos = $(this).attr('data-insumo-repetido');
        var msjMovimientoVacio = $(this).attr('data-movimiento-vacio');
        var msjSeleccionaAlmacen = $(this).attr('data-no-almacen');

        var isValido = true;
        var Mov = new Movimientos();
        var form = document.getElementById(catalogo.NombreFormulario);
        if ($(form).valid())
        {
            var InsumoDetalles = new Array();
            $(catalogo.TablaDetalles).find('tr.movimientoDetalle').each(function ()
            {
                var trRoot = $(this);
                var obj = Mov.GetObject($(this));      
                var existe = false;
                if (parseFloat(obj.Precio,10) <= 0 || parseFloat(obj.Cantidad,10) <= 0)
                {
                    Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjCantidadesMayoresZero);
                    isValido = false;                    
                }
                if (InsumoDetalles.length > 0)
                {
                    $(InsumoDetalles).each(function ()
                    {
                        if (this.IdInsumo == obj.IdInsumo)
                        {
                            Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjInsumosRepetidos);
                            isValido = false;
                            existe = true;
                            $(trRoot).addClass('trActivo');
                        }
                    });
                }
                if (!existe) {
                    InsumoDetalles.push(obj);
                }
            });
            if (InsumoDetalles.length <= 0) {
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjMovimientoVacio);
                isValido = false;
            }
            $(catalogo.TablaDetalles.MovsDelete).each(function ()
            {
                var movNegativo = new Object();
                movNegativo.IdMovimientoDetalle = this * -1;
                InsumoDetalles.push(movNegativo);
            });
            var newMov = new Object();
            newMov.IdMovimiento = $(catalogo.IdMovimiento).val();
            newMov.Tipo = $(catalogo.Tipo).val();
            newMov.Fecha = $(catalogo.Fecha).val();
            newMov.IdAlmacen = $(catalogo.IdAlmacen).val();
            newMov.DetalleMovimientos = InsumoDetalles;
            var url = $(form).attr("action");
            var method = $(form).attr("method");
            if (parseInt(newMov.IdAlmacen, 10) <= 0)
            {
                Mov.ShowErrorInModal(catalogo.NotificacionModalEntrada, msjSeleccionaAlmacen);
                isValido = false;
            }            
            if (isValido)
            {
                catalogo.SetInfo(catalogo.GetInfoHtml(wait));
                $.ajax(
                 {
                     url: url,
                     type: method,
                     data: JSON.stringify(newMov),
                     contentType: "application/json",
                     success: function (html)
                     {
                         var hayerror = catalogo.HasError(html);
                         if (hayerror) {
                             catalogo.SetInfo(html);
                         }
                         else {
                             $(catalogo.Notificaciones).html(html);
                             catalogo.ICAP.CloseModal();
                             catalogo.ReloadCurrentPage({
                                 afterload: 1, fn:
                                     function () {
                                         catalogo.Aftersave(title, html);
                                         catalogo.LoadDetailsMovs();
                                     }
                             });
                         }
                     }
                 });
            }
        }
    });
}
Movimientos.prototype.GetAtributos = function (obj)
{
    var ob = new Object();
    ob.prop = $(obj).attr('data-property');
    ob.val = $(obj).attr('data-property-value');
    return ob;
}
Movimientos.prototype.GetObject = function (elemento)
{
    try
    {
        var catalogo = this;
        var tr = elemento;
        if (tr != null)
        {
            var entidadJSON = new Object();
            var objeto = catalogo.GetAtributos(tr);
            if (objeto != null)
            {
                entidadJSON[objeto.prop] = objeto.val;
            }
            $(tr).find('td').each(function ()
            {                
                var isDinamic = $(this).attr('data-object-value');                
                if (isDinamic!= null && isDinamic != '')
                {
                    var PropiedadObjeto = $(this).attr('data-property');
                    var Propiedad = $(this).attr('data-object-prop');
                    var Control = $('#' + isDinamic).val();
                    if (PropiedadObjeto != undefined) {
                        entidadJSON[PropiedadObjeto] = Control;
                    }
                }
                else {
                    var ob1 = catalogo.GetAtributos($(this));
                    if (ob1 != null) {
                        if (ob1 != undefined) {
                            entidadJSON[ob1.prop] = ob1.val;
                        }
                    }
                }
            });
            return entidadJSON;
        }
        return null;
    }
    catch (e) { alert(e); }
}