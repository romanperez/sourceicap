﻿ClassIndexIcapMain.prototype.MenuOT = function (main, mnuNombre, mnuTitle, dvTabContent, url, idMenu, parametros) {
    var generico = new Catalogos(main, mnuNombre, mnuTitle, dvTabContent, url, idMenu);
    ot = new OT();
    generico = ot.SetEventos(generico, ot);
    if (parametros != null)
    {
        if (parametros.postFijo != null) { generico.postFijo = parametros.postFijo; }
        if (parametros.AfterDelete != null) { generico.Aftersave = parametros.AfterDelete; }
        if (parametros.AfterExit != null) { generico.AfterExit = parametros.AfterExit;}
    }
    generico.Inicializa();
    generico.rptByActividades = document.getElementById('rptByActividades');
    generico.rptGral = document.getElementById('rptGral');
    $(generico.rptByActividades).click(function ()
    {
        //generico.RptOrdenTrabajo($(this)); 
        generico.ConfiguraReporte($(this));
    });
    $(generico.rptGral).click(function ()
    {
        generico.ConfiguraReporte($(this));
    });
}
function OT()
{

}
OT.prototype.SetEventos = function (catalogo, ot)
{
    catalogo.AfterEditar = ot.changeModal2;
    catalogo.AfterNuevo = ot.changeModal;
    catalogo.AddPersonal = ot.AddPersonal;
    catalogo.AddContrato = ot.AddContrato;
    catalogo.DelPersonal = ot.DelPersonal;
    catalogo.DelContrato = ot.DelContrato;
    catalogo.AddTecnico = ot.AddTecnico;
    catalogo.LoadCatalogos = ot.LoadCatalogos;
    catalogo.SetEventoDelete = ot.SetEventoDelete;
    catalogo.LoadAllTecnicos = ot.LoadAllTecnicos;
    catalogo.SetTRActivos = ot.SetTRActivos;
    catalogo.GetTRActivos = ot.GetTRActivos;
    catalogo.GetOT = ot.GetOT;
    catalogo.AddPlantillaDiagrama = ot.AddPlantillaDiagrama;
    catalogo.SeEvntoChangeFiles = ot.SeEvntoChangeFiles;
    catalogo.GetContratos = ot.GetContratos;
    catalogo.GetClientePersonal = ot.GetClientePersonal;
    catalogo.SetEventoListas = ot.SetEventoListas;
    catalogo.ToForm = ot.ToForm;
    catalogo.ObjectToForm = ot.ObjectToForm;
    catalogo.LoadFiles = ot.LoadFiles;
    catalogo.GetNameFiles = ot.GetNameFiles;
    catalogo.DownloadDocument = ot.DownloadDocument;
    catalogo.DeleteDocument = ot.DeleteDocument;
    catalogo.VistaPrevia = ot.VistaPrevia;
    catalogo.GetConceptosByContratros = ot.GetConceptosByContratros;
    catalogo.PlantillaConcepto = ot.PlantillaConcepto;
    catalogo.PlantillaEjecucion = ot.PlantillaEjecucion;
    catalogo.AplicaTipoOrden = ot.AplicaTipoOrden;
    catalogo.RptOrdenTrabajo = ot.RptOrdenTrabajo;
    catalogo.Save = ot.SaveOT;
    catalogo.Inicializa2 = ot.Inicializa;
    catalogo.ConfiguraReporte = ot.ConfiguraReporte;
    return catalogo;
}
OT.prototype.RptOrdenTrabajo = function (btn,data)
{
    var catalogo = this;
    var wait = $(btn).attr('data-msj-wait');
    var none = $(btn).attr('data-msj-none');
    var url = $(btn).attr('data-url-rpt');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var title = $(btn).html();
    var obj = catalogo.GetObject();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }      
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    $.ajax(
       {
           url: url + '/' + obj.Id,
           type: 'POST',
           data: data,
           contentType: "application/x-www-form-urlencoded",
           success: function (response)
           {
               if (response.IdError != null)
               {
                   catalogo.ICAP.ReplaceBodyModal(catalogo.ReturnErrorHtml(response.Error));
               }
               else
               {
                   window.location = urlDownload + '?IdReporte=' + response.Id + '&NombreArchivo=' + response.Nombre;
                   catalogo.ICAP.CloseModal();
               }
           }
       });

}
OT.prototype.ConfiguraReporte = function (btn)
{
    var catalogo = this;
    var wait = $(btn).attr('data-msj-wait');
    var none = $(btn).attr('data-msj-none');
    var url = $(btn).attr('data-url-rpt');
    var urlDownload = $(btn).attr('data-url-rpt-dw');
    var urlPlantilla = $(btn).attr('data-url-plantilla');
    var title = $(btn).html();
    var obj = catalogo.GetObject();
    if (obj == null) {
        catalogo.ICAP.ShowModal(title + ' (' + catalogo.TituloCatalogo + ')', none, null, false, null, null);
        return
    }
    var fnGeneraReporte = function (btnOriginal)
    {
        var btn = document.getElementById('btnBldRptGlobal');
        $(btn).click(function ()
        {
            var data = $('#formularioConfigProyecto').serialize();
            catalogo.RptOrdenTrabajo(btnOriginal, data);
        });
    }
    catalogo.BuildWaitModal(title + ' (' + catalogo.TituloCatalogo + ')', wait);
    $.ajax(
       {
           url: urlPlantilla + '/' + obj.Id,
           type: 'GET',     
           contentType: "application/json",
           success: function (html)
           {
               catalogo.ICAP.ReplaceBodyModal(html);
               var hayerror = catalogo.HasError(html);
               if (!hayerror)
               {
                   catalogo.SetUIBoostrap();                   
                   catalogo.ICAP.BuildAutoCompletes();
                   fnGeneraReporte(btn);
               }
           }
       });
}
OT.prototype.changeModal = function ()
{
    var catalogo = this;
    catalogo.ICAP.LargeModal(true);
    catalogo.Inicializa2();
}
OT.prototype.changeModal2 = function () {
    var catalogo = this;    
    catalogo.ICAP.LargeModal(true);
    catalogo.Inicializa2();
    //catalogo.GetContratos();
    catalogo.GetClientePersonal();
    catalogo.SetEventoListas(catalogo.ulAtencion);
    catalogo.SetEventoListas(catalogo.ulContacto);
    //catalogo.SetEventoListas(catalogo.ulOTContratos);
}
OT.prototype.Inicializa = function ()
{
    var catalogo = this;
    try
    {
        catalogo.dvNotificacionesOT = document.getElementById('dvNotificacionesValidacionOT');
        catalogo.IdOTActividadAutoComplete = document.getElementById('IdOTActividadAutoComplete');
        catalogo.IdOrdenTrabajo = document.getElementById('IdOrdenTrabajo');
        catalogo.IdActividad = document.getElementById('IdActividad');
        catalogo.FechaInicio = document.getElementById('FechaInicio');
        catalogo.Folio = document.getElementById('Folio');
        //catalogo.IdContrato = document.getElementById('IdContrato');
        catalogo.IdProyecto = document.getElementById('IdProyecto');
        catalogo.IdCliente = document.getElementById('RazonSocial');
        catalogo.FechaFin = document.getElementById('FechaFin');
        catalogo.Ubicacion = document.getElementById('Ubicacion');
        catalogo.atencion = document.getElementById('selAtencionOT');
        catalogo.contacto = document.getElementById('selContactoOT');
        catalogo.btnAtencionPlus = document.getElementById('btnAtencionPlus');
        catalogo.btnAtencionMinus = document.getElementById('btnAtencionMinus');
        catalogo.btnContactoPlus = document.getElementById('btnContactoPlus');
        catalogo.btnContactoMinus = document.getElementById('btnContactoMinus');        
        catalogo.btnContratoPlus = document.getElementById('btnContratoPlus');
        catalogo.btnContratoMinus = document.getElementById('btnContratoMinus');
        catalogo.IdEstatus = document.getElementById('IdEstatus');
        catalogo.IdPrioridad = document.getElementById('IdPrioridad');
        catalogo.IdTipo = document.getElementById('IdTipo');
        catalogo.Comentarios = document.getElementById('Comentarios');
        catalogo.ComentariosCliente = document.getElementById('ComentariosCliente');
        catalogo.ulAtencion = document.getElementById('ulAtencionOT');
        catalogo.ulContacto = document.getElementById('ulContactoOT');
        //catalogo.ulOTContratos = document.getElementById('ulOTContratos');
        catalogo.IdContrato = document.getElementById('IdContrato');
        catalogo.btnAddTecnicoPlus = document.getElementById('btnAddTecnicoPlus');
        catalogo.tblOTAllPersonal = document.getElementById('tblOTAllPersonal');
        catalogo.btnLoadAllTecnicos = document.getElementById('btnLoadAllTecnicos');
        catalogo.dvOTDiagramas = document.getElementById('dvOTDiagramas');
        catalogo.dvOTDetallesInstalacion = document.getElementById('dvOTDetallesInstalacion');
        catalogo.tabConceptos = document.getElementById('tabConceptos');
        catalogo.selAllConceptos = document.getElementById('selAllConceptos');
        catalogo.btnAddDiagramaPlus = document.getElementById('btnAddDiagramaPlus');
        catalogo.btnAddDetalleInstalacionPlus = document.getElementById('btnAddDetalleInstalacionPlus');
        catalogo.btnDownloadDiagrama = document.getElementById('btnDownloadDiagrama');
        catalogo.btnDelDiagrama = document.getElementById('btnDelDiagrama');
        catalogo.btnDownloadDetalleInstalacion = document.getElementById('btnDownloadDetalleInstalacion');
        catalogo.btnDelDetalleInstalacion = document.getElementById('btnDelDetalleInstalacion');
        catalogo.btnPreviewDetalleInstalacion = document.getElementById('btnPreviewDetalleInstalacion');
        catalogo.btnPreviewDiagrama = document.getElementById('btnPreviewDiagrama');
        catalogo.btnAddConcepto = document.getElementById('btnAddConcepto');
        catalogo.tblAllOTConceptos = document.getElementById('tblAllOTConceptos');
        catalogo.btnEjcutarConcepto = document.getElementById('btnEjcutarConcepto');
        catalogo.dvEjecucionConceptos = document.getElementById('dvEjecucionConceptos');
        catalogo.dvAllTblConceptos = document.getElementById('dvAllTblConceptos');
        catalogo.dvListAllConceptos = document.getElementById('dvListAllConceptos');
       


        $(catalogo.FechaInicio).datetimepicker({ locale: 'es' });
        $(catalogo.FechaFin).datetimepicker({ locale: 'es' });
        $(catalogo.btnAtencionPlus).click(function () { catalogo.AddPersonal($(this)); });
        $(catalogo.btnContactoPlus).click(function () { catalogo.AddPersonal($(this)); });
        $(catalogo.btnAtencionMinus).click(function () { catalogo.DelPersonal($(this)); });
        $(catalogo.btnContactoMinus).click(function () { catalogo.DelPersonal($(this)); });
        $(catalogo.btnContratoPlus).click(function () { catalogo.AddContrato($(this)); });
        $(catalogo.btnContratoMinus).click(function () { catalogo.DelContrato($(this)); });
        $(catalogo.btnAddTecnicoPlus).click(function () { catalogo.AddTecnico($(this)); });
        $(catalogo.btnLoadAllTecnicos).click(function () { catalogo.LoadAllTecnicos($(this)); });
        $(catalogo.btnAddDiagramaPlus).click(function () { catalogo.AddPlantillaDiagrama($(this)); });
        $(catalogo.btnAddDetalleInstalacionPlus).click(function () { catalogo.AddPlantillaDiagrama($(this)); });
        $(catalogo.btnDownloadDiagrama).click(function () { catalogo.DownloadDocument($(this)); });
        $(catalogo.btnDelDiagrama).click(function () { catalogo.DeleteDocument($(this)); });
        $(catalogo.btnDownloadDetalleInstalacion).click(function () { catalogo.DownloadDocument($(this)); });
        $(catalogo.btnDelDetalleInstalacion).click(function () { catalogo.DeleteDocument($(this)); });        
        $(catalogo.btnPreviewDetalleInstalacion).click(function () { catalogo.VistaPrevia($(this)); });
        $(catalogo.btnPreviewDiagrama).click(function () { catalogo.VistaPrevia($(this)); });        
        $(catalogo.tabConceptos).click(function () { catalogo.GetConceptosByContratros($(this)); });
        if (catalogo.btnAddConcepto != null)
        {
            $(catalogo.btnAddConcepto).click(function () { catalogo.PlantillaConcepto($(this)); });
        }
        $(catalogo.btnEjcutarConcepto).click(function () { catalogo.PlantillaEjecucion($(this)); });
        if(catalogo.IdTipo != null)
        {
            $(catalogo.IdTipo).change(function ()
            {
                catalogo.AplicaTipoOrden($(this));
            });
        }       
        catalogo.SetEventoDelete(catalogo.tblOTAllPersonal);
        catalogo.SetEventoDelete(catalogo.tblAllOTConceptos);
        catalogo.LoadCatalogos();        
        catalogo.SeEvntoChangeFiles(catalogo.dvOTDiagramas);
        catalogo.SeEvntoChangeFiles(catalogo.dvOTDetallesInstalacion);
    }
    catch (eIni)
    {
        alert(eIni);
    }
}
OT.prototype.AplicaTipoOrden = function (sel)
{
    var catalogo = this;
    var cerrada = $(sel).attr('data-cerrada');
    var selecionada = $(sel).find(":selected");
    var tipo = new String($(selecionada).text()).toLowerCase();
    $(catalogo.tblAllOTConceptos).html('');
    //$(catalogo.Ubicacion).val('');
    $(catalogo.IdOTActividadAutoComplete).val('');
    $(catalogo.IdActividad).val('');
    if ($.trim(tipo) == cerrada)
    {
        $(catalogo.IdOTActividadAutoComplete).attr('data-auto-complemento', 'selAllConceptos');
       // $(catalogo.Ubicacion).val('');
       // $(catalogo.Ubicacion).attr('readonly', 'readonly');
    }
    else
    {
        $(catalogo.IdOTActividadAutoComplete).removeAttr('data-auto-complemento');
       // $(catalogo.Ubicacion).val('');
       // $(catalogo.Ubicacion).removeAttr('readonly');
    }
}
OT.prototype.PlantillaEjecucion = function (btn)
{
    var catalogo = this;
    var url = $(btn).attr('data-url-load');
    var msjWait = $(btn).attr('data-msj-wait');
    var tr = $(catalogo.tblAllOTConceptos).find('tr.trActivo');
    var concepto = catalogo.ICAP.GetObject($(tr));
    $(catalogo.dvEjecucionConceptos).html('<div class="alert alert-info" role="alert">' + msjWait + '</div>');
    var fnCancelar = function (btn) {
        var url = $(btn).attr('data-url-reload');
        if (catalogo.btnAddConcepto != null) {
            $(catalogo.btnAddConcepto).show();
        }
        $(catalogo.dvAllTblConceptos).show();
        $(catalogo.dvListAllConceptos).show();
        $(catalogo.dvEjecucionConceptos).html('');
        fnHideBotones(false);

        var Idorden = parseInt($(catalogo.IdOrdenTrabajo).val(), 10);
        if (Idorden > 0) {
            $(catalogo.dvAllTblConceptos).html('<div class="alert alert-info" role="alert">' + msjWait + '</div>');
            $.ajax(
            {
                url: url + '/' + Idorden,
                type: 'get',
                success: function (html) {
                    $(catalogo.dvAllTblConceptos).html(html);
                    catalogo.tblAllOTConceptos = document.getElementById('tblAllOTConceptos');
                    if (catalogo.tblAllOTConceptos != null) {
                        catalogo.SetTRActivos(catalogo.tblAllOTConceptos, false);
                    }
                }
            });
        }
    };
    var fnEjecutar = function (btn,concepto)
    {
        //var url = $(btn).attr('data-url-reload');
        var url = $(btn).attr('data-url-exe');
        var ejecucion = new Object();
        ejecucion.CantidadEjecutada = $(catalogo.ExeEjecutada).val();
        ejecucion.CantidadProgramada = $(catalogo.ExeProgramada).val();
        ejecucion.Fecha = $(catalogo.ExeFecha).val();
        ejecucion.IdOTConcepto = concepto.IdOTConcepto;
        if (catalogo.btnExeEjecutar.TRSelected != null)
        {
            ejecucion.IdOTConceptoEjecucion = catalogo.btnExeEjecutar.TRSelected.IdOTConceptoEjecucion;
            $(catalogo.btnExeEjecutar.TR).find('td').each(function()
            {
                if($(this).attr('data-property')=='Fecha')
                {
                    $(this).html(ejecucion.Fecha);
                }
                if($(this).attr('data-property')=='CantidadEjecutada')
                {
                    $(this).html(ejecucion.CantidadEjecutada);
                }
            });          
        }

        $.ajax(
        {
            url: url,
            type: 'POST',
            data: JSON.stringify(ejecucion),
            contentType: "application/json",
            success: function (html)
            {
                var hayerror = catalogo.HasError(html);
                $(catalogo.dvEjecucionConceptos).prepend(html);
                if (!hayerror) {
                    $(catalogo.btnExeCancelar).click();
                }
            }
        });
    }
    var fnSelectRwEjecucion = function (tr)
    {
        var trEjecutado = catalogo.ICAP.GetObject($(tr));
        catalogo.btnExeEjecutar.TR = tr;
        catalogo.btnExeEjecutar.TRSelected = trEjecutado;
        $(catalogo.ExeFecha).val(trEjecutado.Fecha);
        $(catalogo.ExeEjecutada).val(trEjecutado.CantidadEjecutada);
        $(catalogo.ExeProgramada).val(trEjecutado.CantidadProgramada);
    }
    var fnHideBotones=function(hide)
    {
        if (hide) {
            $('#btnSalirOT').hide();
            $('#btnSaveOrdenesTrabajo').hide();
        }
        else {
            $('#btnSalirOT').show();
            $('#btnSaveOrdenesTrabajo').show();
        }
    }
    $(catalogo.dvAllTblConceptos).hide();
    $(catalogo.dvListAllConceptos).hide();
    if (btn.btnAddConcepto != null) {
        $(catalogo.btnAddConcepto).hide();
    }
    $.ajax(
     {
         url: url + '/' + concepto.IdOTConcepto,
         type: 'get',
         success: function (html)
         {
             $(catalogo.dvEjecucionConceptos).html(html);
             catalogo.ExeFecha = document.getElementById('ExeFecha');             
             catalogo.btnExeEjecutar = document.getElementById('btnExeEjecutar');
             catalogo.btnExeCancelar = document.getElementById('btnExeCancelar');
             catalogo.ExeEjecutada = document.getElementById('ExeEjecutada');
             catalogo.ExeProgramada = document.getElementById('ExeProgramada');
             catalogo.tblEjecucionConceptos = document.getElementById('tblEjecucionConceptos');
             $(catalogo.ExeFecha).datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
             $(catalogo.btnExeCancelar).click(function () { fnCancelar($(this)); });
             $(catalogo.btnExeEjecutar).click(function () { fnEjecutar($(this), concepto); });
             catalogo.OnSelectRow(catalogo.tblEjecucionConceptos, false, function (tr) { fnSelectRwEjecucion(tr); });
             fnHideBotones(true);
         }
     });
}
OT.prototype.PlantillaConcepto = function (btn)
{
    var catalogo = this;
    var url = $(btn).attr('data-url-load');
    var msjWait = $(btn).attr('data-msj-wait');
    var msjSinTipo=$(btn).attr('data-sin-tipo');
    var auxTr = document.createElement('tr');    
    var idConcepto = 0;
    if (catalogo.selAllConceptos != null)
    {
      idConcepto= parseInt($(catalogo.selAllConceptos).val(), 10);
    }
    if (catalogo.tblAllOTConceptos.InEspera == 1) return;
    if (idConcepto <= 0) return;
    var IdTipo = 0;
    if (catalogo.IdTipo != null)
    {
        IdTipo = parseInt($(catalogo.IdTipo).val(), 10);
    }
    if (IdTipo <= 0)
    {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjSinTipo);
        return;
    }
    var fnSetDatos = function (concepto)
    {
        var iExiste = 0;
        $(catalogo.Ubicacion).val(concepto.Ubicacion);
        $(selAllConceptos).attr('data-property-valor', concepto.IdConceptoClasificacion);
        $(catalogo.IdOTActividadAutoComplete).val('');
        $(catalogo.IdActividad).val('');

        $(catalogo.tblAllOTConceptos).find('tr').each(function ()
        {
            var tr = catalogo.ICAP.GetObject($(this));
            if (tr.IdConcepto == concepto.IdConcepto)
            {
                iExiste++;
            }
        });
        if (iExiste > 1) return true;
        return false;
    }
    $(auxTr).html('<td colspan="7"><b>' + msjWait + '</b></td>');
    $(catalogo.tblAllOTConceptos).append(auxTr);
    catalogo.tblAllOTConceptos.InEspera = 1;
    $.ajax(
      {
          url: url + '/' + idConcepto,
          type: 'get',          
          success: function (html)
          {
              var hayerror = catalogo.HasError(html);
              if (!hayerror)
              {
                  $(auxTr).remove();
                  catalogo.tblAllOTConceptos.InEspera = 0;
                  $(catalogo.tblAllOTConceptos).append(html);
                  catalogo.SetEventoDelete(catalogo.tblAllOTConceptos);
                  catalogo.GetTRActivos(null, catalogo.tblAllOTConceptos);
                  var tr = $(catalogo.tblAllOTConceptos).find('tr:last');
                  var concepto = catalogo.ICAP.GetObject($(tr));
                  var existe = fnSetDatos(concepto);
                  if (existe)
                  {
                      var msjExiste = $(catalogo.tblAllOTConceptos).attr('data-concepto-existe');
                      catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjExiste);
                      $(tr).remove();
                  }
              }
          }
      });


}
OT.prototype.GetConceptosByContratros = function (tab)
{
    var catalogo = this;
    var tabBody = document.getElementById($(tab).attr('aria-controls'));
    var AllContratos = new Array();
    var msjWait =(catalogo.selAllConceptos!=null)? $(catalogo.selAllConceptos).attr('data-msj-wait'):"";
    var url = (catalogo.selAllConceptos!=null)? $(catalogo.selAllConceptos).attr('data-url-load'):"";
   /* $(catalogo.ulOTContratos).find('li').each(function ()
    {
        var idContrato = parseInt($(this).attr('data-contrato'), 10);
        if (idContrato > 0)
        {
            var conAux = new Object();
            conAux.IdContrato = idContrato;
            AllContratos.push(conAux);
        }
    });*/    
    var conAux = new Object();
    conAux.IdContrato = parseInt($(catalogo.IdContrato).attr('data-id'), 10);
    if (conAux.IdContrato <= 0 || isNaN(conAux.IdContrato ))
    {
        conAux.IdContrato = parseInt($(catalogo.IdContrato).val(), 10);
    }
    AllContratos.push(conAux);
    if (catalogo.selAllConceptos != null)
    {
        $(catalogo.selAllConceptos).html('<option selected="selected" value="0">' + msjWait + '</option>');
    }
    
    catalogo.SetTRActivos(catalogo.tblAllOTConceptos, false);         
    if ($.trim(url) == "") return;

    $.ajax(
     {
         url: url,
         type: 'POST',
         data: JSON.stringify(AllContratos),
         contentType: "application/json",
         success: function (html)
         {
             $(catalogo.selAllConceptos).html(html);
         }
     });
}
OT.prototype.SeEvntoChangeFiles = function (elemento)
{
    $(elemento).find('input.diagrama').each(function ()
    {
        $(this).change(function ()
        {
            var label = $(this).parent().find('label');            
            $(label).html(($(this).val().split('\\').pop()));
        });
    });
}
OT.prototype.GetNameFiles = function (elemento)
{
    var catalogo = this;
    var destino = '';
    $(elemento).find('input.diagrama').each(function ()
    {
        if ($(this)[0].files[0] != null)
        {    
            destino = destino + $(this)[0].files[0].name + ",";
        }
    });
    return destino;
}
OT.prototype.LoadFiles = function (form,elemento,destino)
{
    var catalogo = this;
    destino = '';
    $(elemento).find('input.diagrama').each(function ()
    {
        if ($(this)[0].files[0] != null)
        {
            form.append($(this)[0].files[0].name, $(this)[0].files[0]);
            destino = destino + $(this)[0].files[0].name + ",";
        }
    });
    return form;
}
OT.prototype.ObjectToForm = function (frm,obj,propiedad)
{
    var iCont = 0;
    $(obj).each(function () {
        for (var p in this) {
            frm.append(propiedad + '[' + iCont + '].' + p, this[p]);
        }
        iCont++;
    });
}
OT.prototype.ToForm = function (obj)
{
    var catalogo=this;
    var data = new FormData();
    obj.ListaDiagramas = catalogo.GetNameFiles(catalogo.dvOTDiagramas);
    obj.ListaDetallesInstalacion = catalogo.GetNameFiles(catalogo.dvOTDetallesInstalacion);
    for (var prop in obj)
    {
        switch (prop)
        {
            case 'Contratos':
                if (obj.Contratos.length > 0)
                {
                    catalogo.ObjectToForm(data, obj.Contratos, 'Contratos');                   
                }
                break;
            case 'PersonalClienteAtencion':
                if (obj.PersonalClienteAtencion.length > 0) {
                    catalogo.ObjectToForm(data, obj.PersonalClienteAtencion, 'PersonalClienteAtencion');
                }
                break;
            case 'PersonalClienteContacto':
                if (obj.PersonalClienteContacto.length > 0) {
                    catalogo.ObjectToForm(data, obj.PersonalClienteContacto, 'PersonalClienteContacto');
                }
                break;
            case 'OTPersonal':
                if (obj.OTPersonal.length > 0) {
                    catalogo.ObjectToForm(data, obj.OTPersonal, 'OTPersonal');
                }
                break;
            case 'OTConceptos':
                if (obj.OTConceptos.length > 0) {
                    catalogo.ObjectToForm(data, obj.OTConceptos, 'OTConceptos');
                }
                break;
            default:
                data.append(prop, obj[prop]);
                break;
        }        
    }
    data = catalogo.LoadFiles(data, catalogo.dvOTDiagramas);
    data = catalogo.LoadFiles(data, catalogo.dvOTDetallesInstalacion);
    return data;
}

OT.prototype.VistaPrevia = function (btn)
{
    var msjWait = $(btn).attr('data-msj-wait');
    var url = $(btn).attr('data-url-plantilla');
    var target =document.getElementById($(btn).attr('data-target'));
    var op = document.getElementById($(btn).attr('data-origen'));
    var idDocumento = parseInt($(op).val(), 10);    
    $(target).html('<div class="alert alert-info" role="alert">' + msjWait + '</div>');
    $.ajax({
        url: url + '/' + idDocumento,
        type: 'get',
        success: function (html)
        {
            $(target).html(html);           
        }
    });
}
OT.prototype.DownloadDocument = function (btn)
{
    var op = document.getElementById($(btn).attr('data-origen'));
    var idDocumento = parseInt($(op).val(), 10);
    var url = $(btn).attr('data-url-dwn');
    window.open(url + '/' + idDocumento, '_blank');
}
OT.prototype.DeleteDocument = function (btn)
{
    var op = document.getElementById($(btn).attr('data-origen'));
    var idDocumento = parseInt($(op).val(), 10);
    var selecionada = $(op).find(":selected");
    var url = $(btn).attr('data-url-del');
    var target = document.getElementById($(btn).attr('data-target'));
    $.ajax({
        url: url + '/' + idDocumento,
        type: 'post',
        success: function (html)
        {
            $(target).prepend(html);
            $(selecionada).remove();
        }
    });
}

OT.prototype.AddPlantillaDiagrama = function (btn)
{
    var catalogo = this;

    var url = $(btn).attr('data-url-plantilla');
    var div =document.getElementById($(btn).attr('data-destino'));
    var msjWait = $(btn).attr('data-msj-wait');
    var id = 0;
    var msj = document.createElement('div');
    var target = document.getElementById($(btn).attr('data-target'));
    $(div).find('div.diagrama').each(function () { id++ });
    $(msj).html('<div class="alert alert-info" role="alert">' + msjWait + '</div>');
    $(target).prepend(msj);
    $.ajax({
        url: url+ '/' + id,
        type: 'get',
        success: function (html)
        {
            $(msj).remove();
            $(div).append(html);
            catalogo.SeEvntoChangeFiles(div);
        }
    });
}
OT.prototype.SetTRActivos = function (tbl,multiple)
{
    var catalogo = this;
    catalogo.OnSelectRow(tbl,multiple);
}
OT.prototype.GetTRActivos = function (btn,tbl)
{
    var catalogo = this;
    $(tbl).find('tr.trActivo').each(function ()
    {
        $(this).removeClass('trActivo');
        $(this).unbind("click");
        $(catalogo.tblOTAllPersonal).append($(this));
    });
}
OT.prototype.SelectTR = function (tbl) {
    var catalogo = this;
    $(tbl).find('tr').each(function ()
    {
        $(this).removeClass('trActivo');
        $(this).unbind("click");
        $(catalogo.tblOTAllPersonal).append($(this));
    });
}
OT.prototype.LoadAllTecnicos = function (btn)
{
    var catalogo = this;
    var url = $(btn).attr('data-url-load-tecnicos');
    var msjWait = $(btn).attr('data-msj-wait');
    var dv1 = document.getElementById($(btn).attr('data-dvdata'));
    var dv2 = document.getElementById($(btn).attr('data-dvdata-aux'));
    $(dv1).hide();
    $(dv2).show();
    $(dv2).html('<div class="alert alert-info form-group col-md-12" role="alert">' + msjWait + '</div>');
    $.ajax({
        url: url,
        type: 'get',       
        success: function (html)
        {
            $(dv2).removeClass("dvHidden");
            $(dv2).addClass("dvAllTecnicos");
            $(dv2).html(html);
            catalogo.SetTRActivos($('#tblAllTecnicosByEmpresa'));
            $('#btnAddTecnicosSeleccionados').click(function ()
            {
                catalogo.GetTRActivos($(this), $('#tblAllTecnicosByEmpresa'));
                $(dv2).hide();
                $(dv1).show();
            });

        }
    });
}
OT.prototype.GetOT = function ()
{
    var catalogo = this;
    var ot = new Object();
    ot.numConceptos = 0;   
    ot.IdOrdenTrabajo = parseInt($(catalogo.IdOrdenTrabajo).val(), 10);    
    ot.IdProyecto = parseInt($(catalogo.IdProyecto).val(), 10);
    //ot.IdContrato = parseInt($(catalogo.IdContrato).val(), 10);
    ot.IdActividad = parseInt($(catalogo.IdActividad).val(), 10);
    ot.Folio = $(catalogo.Folio).val();
    ot.Ubicacion = $(catalogo.Ubicacion).val();
    ot.FechaInicio = $(catalogo.FechaInicio).val();
    ot.FechaFin = $(catalogo.FechaFin).val();
    ot.IdEstatus = parseInt($(catalogo.IdEstatus).val(), 10);
    ot.IdPrioridad = parseInt($(catalogo.IdPrioridad).val(), 10);
    ot.IdTipo = parseInt($(catalogo.IdTipo).val(), 10);
    ot.Comentarios = $(catalogo.Comentarios).val();
    ot.ComentariosCliente = $(catalogo.ComentariosCliente).val();
    ot.PersonalClienteAtencion = new Array();
    ot.PersonalClienteContacto = new Array();
    ot.OTConceptos = new Array();
    ot.Contratos = new Array();
    ot.OTPersonal = new Array();
    ot.ListaDiagramas ="";
    ot.ListaDetallesInstalacion = "";

    $(catalogo.ulAtencion).find('li').each(function ()
    {
        var id = $(this).attr('data-id');
        var idClientePersonal = $(this).attr('data-idcliente');
        var personal = new Object();
        personal.IdOTPersonalCliente = id;
        personal.IdPersonalCliente = idClientePersonal;
        if (parseInt(personal.IdOTPersonalCliente, 10) == 0)
        {
            ot.PersonalClienteAtencion.push(personal);
        }        
    });
    $(catalogo.ulContacto).find('li').each(function ()
    {
        var id = $(this).attr('data-id');
        var idClientePersonal = $(this).attr('data-idcliente');
        var personal = new Object();
        personal.IdOTPersonalCliente = id;
        personal.IdPersonalCliente = idClientePersonal;        
        if (parseInt(personal.IdOTPersonalCliente, 10) == 0) {
            ot.PersonalClienteContacto.push(personal);
        }
    });

  /*  $(catalogo.ulOTContratos).find('li').each(function () {
        var id = $(this).attr('data-id');
        var idContrato= $(this).attr('data-contrato');
        var contrato = new Object();
        contrato.IdContratoOrden = id;
        contrato.IdContrato = idContrato;
        if (parseInt(contrato.IdContratoOrden, 10) == 0) {
            ot.Contratos.push(contrato);
        }
    });*/
    var contratoA = new Object();
    contratoA.IdContratoOrden = $(catalogo.IdContrato).attr('data-idcontratoorden');
    contratoA.IdContrato = parseInt($(catalogo.IdContrato).attr('data-id'), 10);
    if (contratoA.IdContrato <= 0 || isNaN(contratoA.IdContrato))
    {
        contratoA.IdContrato = parseInt($(catalogo.IdContrato).val(), 10);
    }
    ot.Contratos.push(contratoA);


    $(catalogo.tblOTAllPersonal).find('tr').each(function ()
    {       
        var tecnico = catalogo.ICAP.GetObject($(this));
        if (tecnico.IdOTPersonal == 0)
        {
            ot.OTPersonal.push(tecnico);
        }
    });

    $(catalogo.tblAllOTConceptos).find('tr').each(function () {
        var concepto = catalogo.ICAP.GetObject($(this));
        //if (concepto.IdOTConcepto == 0)
        //{
            ot.OTConceptos.push(concepto);
            ot.numConceptos++;
        //}
    });

    $(catalogo.ulAtencion.PersonalDelete).each(function ()
    {
        var personal = new Object();
        personal.IdOTPersonalCliente = this;        
        ot.PersonalClienteAtencion.push(personal);
    });
    $(catalogo.ulContacto.PersonalDelete).each(function () {
        var personal = new Object();
        personal.IdOTPersonalCliente = this;
        ot.PersonalClienteContacto.push(personal);
    });
   /* $(catalogo.ulOTContratos.ContratosDelete).each(function () {
        var contrato = new Object();
        contrato.IdContratoOrden = this;
        ot.Contratos.push(contrato);
    });*/
    $(catalogo.tblOTAllPersonal.DeletePersonal).each(function ()
    {
        var tecnico = new Object();
        tecnico.IdOTPersonal = this;
        ot.OTPersonal.push(tecnico);
    });
    $(catalogo.tblAllOTConceptos.DeletePersonal).each(function () {
        var contrato = new Object();
        contrato.IdOTConcepto = this;
        ot.OTConceptos.push(contrato);
    });
    return ot;    
}
OT.prototype.LoadCatalogos = function()
{
    var catalogo = this;
    var url1 = $(catalogo.IdEstatus).attr('data-url-estatus');
    var url2 = $(catalogo.IdPrioridad).attr('data-url-estatus');
    var url3 = $(catalogo.IdTipo).attr('data-url-estatus');

    var msjWait1 = $(catalogo.IdEstatus).attr('data-msj-wait');
    var msjWait2 = $(catalogo.IdPrioridad).attr('data-msj-wait');
    var msjWait3 = $(catalogo.IdTipo).attr('data-msj-wait');

    var EstatusIni = parseInt($(catalogo.IdEstatus).attr('data-idEstatusOt'),10);
    var PrioiridadIni = parseInt($(catalogo.IdPrioridad).attr('data-idEstatusOt'), 10);
    var TipoIni = parseInt($(catalogo.IdTipo).attr('data-idEstatusOt'), 10);

    $(catalogo.IdEstatus).html('<option selected="selected" value="0">' + msjWait1 + '</option>');
    $(catalogo.IdPrioridad).html('<option selected="selected" value="0">' + msjWait2 + '</option>');
    $(catalogo.IdTipo).html('<option selected="selected" value="0">' + msjWait3 + '</option>');

    $.ajax(
    {
        url: url1,
        type: 'get',
        success: function (html)
        {
            $(catalogo.IdEstatus).html(html);
            $(catalogo.IdEstatus).find('option').each(function ()
            {               
                if (parseInt($(this).val(), 10) == EstatusIni)
                {
                    $(this).prop('selected', true);
                }
                
            });
        }
    });
    $.ajax(
   {
       url: url2,
       type: 'get',
       success: function (html) {
           $(catalogo.IdPrioridad).html(html);
           $(catalogo.IdPrioridad).find('option').each(function ()
           {
               if (parseInt($(this).val(), 10) == PrioiridadIni) {
                   $(this).prop('selected', true);
               }
           });
       }
   });
    if (url3 != null && url3 != undefined) {
        $.ajax(
     {
         url: url3,
         type: 'get',
         success: function (html) {
             $(catalogo.IdTipo).html(html);
             $(catalogo.IdTipo).find('option').each(function () {
                 if (parseInt($(this).val(), 10) == TipoIni) {
                     $(this).prop('selected', true);
                 }
             });
         }
     });
    }
}
OT.prototype.DelPersonal = function (btn)
{
    var catalogo = this;
    var origen = document.getElementById($(btn).attr('data-origen'));
    if (origen.PersonalDelete == null)
    {
        origen.PersonalDelete = new Array();
    }
    $(origen).find('li.personalActivo').each(function ()
    {
        var id = parseInt($(this).attr('data-id'));
        if (id > 0)
        {
            origen.PersonalDelete.push(id*-1);
        }
        $(this).remove();        
    });        
}
OT.prototype.SetEventoListas = function (ul)
{
    $(ul).find('li').each(function () {
        $(this).unbind("click");
        $(this).click(function () {
            $(ul).find('li.personalActivo').each(function () {
                $(this).removeClass('personalActivo');
            });
            $(this).addClass('personalActivo');
        })
    });
}
OT.prototype.AddPersonal = function (btn) {
    var catalogo = this;
    var origen = document.getElementById($(btn).attr('data-origen'));
    var destino = document.getElementById($(btn).attr('data-destino'));
    var dataMsjExiste = $(destino).attr('data-msj-existe');
    var op = $(origen).find(":selected");

    var idPersonal = parseInt($(op).attr('data-property-value'), 10);
    var Personal = $(op).val();
    if (idPersonal <= 0) return;
    var existe = false;
    $(destino).find("li").each(function () {
        if (parseInt($(this).attr('data-idcliente'), 10) == idPersonal) {
            existe = true;
        }
    });
    if (!existe) {
        $(destino).append('<li class="list-group-item" data-id="0" data-idcliente="' + idPersonal + '">' + Personal + '</li>');
        catalogo.SetEventoListas(destino);
    }
    else {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, dataMsjExiste);
    }
}
OT.prototype.DelContrato = function (btn) {
    var catalogo = this;
    var origen = document.getElementById($(btn).attr('data-origen'));
    if (origen.ContratosDelete == null) {
        origen.ContratosDelete = new Array();
    }
    $(origen).find('li.personalActivo').each(function () {
        var id = parseInt($(this).attr('data-id'));
        if (id > 0) {
            origen.ContratosDelete.push(id * -1);
        }
        $(this).remove();
    });
}
OT.prototype.AddContrato = function (btn)
{
    var catalogo = this;
    var origen = document.getElementById($(btn).attr('data-origen'));
    var destino = document.getElementById($(btn).attr('data-destino'));
    var dataMsjExiste = $(destino).attr('data-msj-existe');
    var op = $(origen).find(":selected");

    //var idPersonal = parseInt($(op).attr('data-property-value'), 10);
    var Id = $(op).val();
    var Contrato = $.trim($(op).text());
    if (Id <= 0) return;
    var existe = false;
    $(destino).find("li").each(function () {
        if (parseInt($(this).attr('data-contrato'), 10) == Id) {
            existe = true;
        }
    });
    if (!existe) {
        $(destino).append('<li class="list-group-item" data-id="0" data-contrato="' + Id + '">' + Contrato + '</li>');
        catalogo.SetEventoListas(destino);
    }
    else {
        catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, dataMsjExiste);
    }
}
OT.prototype.SetEventoDelete = function (tbl)
{
    if (tbl.DeletePersonal == null)
    {
        tbl.DeletePersonal = new Array();
    }
    $(tbl).find('div.btnRemoveTR').each(function ()
    {
        $(this).unbind("click");
        $(this).click(function ()
        {
            var tr = $(this).closest('tr');
            var id = $(this).attr('data-param');
            if (parseInt(id, 10) > 0) {
                tbl.DeletePersonal.push(id * -1);
            }
            $(tr).remove();
        });
        
    });
}
OT.prototype.AddTecnico = function(btn)
{
    var catalogo = this;
    if (catalogo.tblOTAllPersonal.InEspera == 1) return;
    var url = $(btn).attr('data-url-plantilla');
    var msjWait = $(btn).attr('data-msj-wait');
    var auxTr = document.createElement('tr');
    var idTecnicoAux = document.getElementById('idTecnicoAux');
    $(auxTr).html('<td colspan="4"><b>' + msjWait + '</b></td>');
    $(catalogo.tblOTAllPersonal).append(auxTr);
    if (idTecnicoAux.ResultSelectionAutoComplete == null)
    {
        $(auxTr).remove();
        return;
    }
    catalogo.tblOTAllPersonal.InEspera = 1;
    $.ajax(
      {
          url: url,
          type: 'POST',          
          data: JSON.stringify(idTecnicoAux.ResultSelectionAutoComplete),
          contentType: "application/json",
          success: function (tecnico)
          {              
              $(auxTr).remove();
              catalogo.tblOTAllPersonal.InEspera = 0;              
              $(catalogo.tblOTAllPersonal).append(tecnico);
              $(idTecnicoAux).val('');
              idTecnicoAux.ResultSelectionAutoComplete = null;
              $('#IdOTTecnicoAutoComplete').val('');
              catalogo.SetEventoDelete(catalogo.tblOTAllPersonal);
          }
      });

}

OT.prototype.GetContratos = function ()
{
    var catalogo = this;
    try
    {        
        var url = $(catalogo.IdContrato).attr('data-url-proyecto');
        var msjWait = $(catalogo.IdContrato).attr('data-msj-wait');
        var id = $(catalogo.IdProyecto).val();       
        $(catalogo.IdContrato).html('<option selected="selected" value="0">' + msjWait + '</option>');
        $.ajax(
        {
            url: url + '/' + id,
            type: 'POST',                        
            success: function (html)
            {                                
                $(catalogo.IdContrato).html(html);
            }
        });
    }
    catch (eIni)
    {
        alert(eIni);
    }
}
OT.prototype.GetClientePersonal = function ()
{
    var catalogo = this;
    try
    {
        var url = $(catalogo.IdCliente).attr('data-url');
        var msjWait = $(catalogo.IdContrato).attr('data-msj-wait');
        var idCliente = $(catalogo.IdCliente).attr('data-property-value');
        var ulAtencion = document.getElementById('ulAtencionOT');
        var ulContacto = document.getElementById('ulContactoOT');

        $(catalogo.atencion).html('<option selected="selected" value="0">' + msjWait + '</option>');
        $(catalogo.contacto).html('<option selected="selected" value="0">' + msjWait + '</option>');
        $(ulAtencion).find('li').each(function ()
        {
            if (parseInt($(this).attr('data-id'), 10) == 0)
            {
                $(this).remove();
            }
        });
        $(ulContacto).find('li').each(function () {
            if (parseInt($(this).attr('data-id'), 10) == 0) {
                $(this).remove();
            }
        });        
        $.ajax(
       {
           url: url +'/' + idCliente,
           type: 'POST',          
           success: function (personal)
           {
               $(catalogo.atencion).html(personal);
               $(catalogo.contacto).html(personal);
           }
       });
    }
    catch (eIni) {
        alert(eIni);
    }
}

OT.prototype.SaveOT = function (title, wait)
{
    var catalogo = this;
    $('#btnSalirOT').click(function ()
    {
        catalogo.ICAP.CloseModal();
        if (catalogo.AfterExit != null)
        {
            catalogo.AfterExit(title, '');
        }
        else {
            catalogo.ReloadCurrentPage();
        }
    });
    $('#btnSave' + catalogo.NombreCatalogo).click(function ()
    {
        var ot = catalogo.GetOT();
        var msjOK = $(this).attr('data-op-ok');
        var NombreFormulario = catalogo.NombreFormulario;
        if (NombreFormulario == null || NombreFormulario == undefined)
        {
            NombreFormulario = 'formularioEditarNew' + catalogo.NombreCatalogo
        }
        var form = document.getElementById(NombreFormulario);        
        var url = $(form).attr("action");
        var method = $(form).attr("method");
            
        var allOT = catalogo.ToForm(ot);
        var valido = true;
        //var msjOneConcepto = $(this).attr('data-solo-concepto');
        /*if (ot.numConceptos > 1)
        {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjOneConcepto);
        }*/
        var msjSinActividad = $(this).attr('data-Actividad-vacia');
        var msjIdTipo   = $(catalogo.IdTipo).attr('data-empty-ms');
        var msjFolio = $(catalogo.Folio).attr('data-empty-ms');
        var msjIdContrato = $(catalogo.IdContrato).attr('data-empty-ms');
        var msjUbicacion = $(catalogo.Ubicacion).attr('data-empty-ms');
        var msjIdEstatus = $(catalogo.IdEstatus).attr('data-empty-ms');
        var msjIdPrioridad = $(catalogo.IdPrioridad).attr('data-empty-ms');

        if (parseInt(ot.IdTipo, 10) <= 0)
        {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjIdTipo);
        }
        if (parseInt(ot.IdActividad, 10) <= 0 || isNaN(ot.IdActividad))
        {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjSinActividad);
        }
        if ($.trim(ot.Folio) =='') {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjFolio);
        }
        if (parseInt(ot.IdContrato, 10) <= 0) {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjIdContrato);
        }
        if ($.trim(ot.Ubicacion)=='') {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjUbicacion);
        }
        if (parseInt(ot.IdEstatus, 10) <= 0) {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjIdEstatus);
        }
        if (parseInt(ot.IdPrioridad, 10) <= 0) {
            valido = false;
            catalogo.ShowErrorInModal(catalogo.dvNotificacionesOT, msjIdPrioridad);
        }

        if (valido)
        {           
            $(catalogo.dvNotificacionesOT).html(catalogo.GetLoadingInfoHtml(wait));
            $.ajax(
                 {
                     url: url,
                     type: method,
                     data: allOT,
                     contentType: false,
                     processData: false,
                     success: function (html)
                     {
                         var hayerror = catalogo.HasError(html);                         
                         if (!hayerror) 
                         {
                             catalogo.ICAP.ReplaceBodyModal(html);
                             catalogo.AfterEditar();
                             catalogo.Save(title, wait);                                                        
                             $('#dvNotificacionesValidacionOT').html('<div class="alert alert-success" role="alert"> ' +
                                                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                                                '<span aria-hidden="true">&times;</span></button> ' + msjOK + '</div>');
                         }
                         else
                         {                            
                             $(catalogo.dvNotificacionesOT).html(html);
                         }
                     }                    
                 });
        }
    });
}
AutoComplete.prototype.OnSelectProyecto = function (seleccion)
{
    var form = document.getElementById('formularioEditarNewOrdenesTrabajo');
    if (form != null)
    {
        var myOt = new OT();
        var cliente = $(form).find('input[id="RazonSocial"]');
        myOt.IdContrato = document.getElementById('IdContrato');
        myOt.IdProyecto = document.getElementById('IdProyecto');
        myOt.atencion = document.getElementById('selAtencionOT');
        myOt.contacto = document.getElementById('selContactoOT');
        myOt.IdCliente = cliente;
        $(cliente).val(seleccion.RazonSocial);
        $(cliente).attr('data-property-value', seleccion.IdCliente);
        myOt.GetContratos();
        myOt.GetClientePersonal();
    }
}
AutoComplete.prototype.OnSelectActividad = function (seleccion)
{
    var form = document.getElementById('formularioEditarNewOrdenesTrabajo');
    if (form != null) {
        var myOt = new OT();
        var target = $(form).find('div[id="dvActividadAux"]');
        var url = $(target).attr('data-url-plantilla');
        var msjWait = $(target).attr('data-msj-wait');
        $(target).html('<div class="alert alert-info" role="alert">' + msjWait + '</div>');
        $.ajax({
            url: url,
            type: 'post',
            data: JSON.stringify(seleccion),
            contentType: "application/json",
            success: function (html)
            {
                $(target).html(html);
            }
        });
    }
}
