﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MaquetacionHTML.Controllers
{
    public class IndexController : Controller
    {
        //
        // GET: /Index/

        public ActionResult Index()
        {
            return View();
        }
		  [HttpGet]
		  public ActionResult OTLista()
		  {
			  return PartialView();
		  }
		  [HttpGet]
		  public ActionResult FakeDescarga()
		  {
			  System.Threading.Thread.Sleep(5000);
			  return PartialView();
		  }
		  [HttpGet]
		  public ActionResult MisOrdenes()
		  {
			  return PartialView();
		  }
		  [HttpGet]
		  public ActionResult IniciarOrden()
		  {
			  return PartialView();
		  }
		  [HttpGet]
		  public ActionResult Modal()
		  {
			  return PartialView();
		  }
					  		  
    }
}
